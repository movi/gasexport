﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text.RegularExpressions;
using Northwoods.Go;

namespace GExGoDiagram.SvgParser
{
    public class GoSvgPlaceHolder : GoDrawing
    {
        public GoSvgPlaceHolder()
        {
            Init();
        }

        private void Init()
        {
            var pathTypes = new byte[]
            {0, 3, 3, 3, 3, 3, 3, 1, 1, 3, 3, 3, 3, 3, 131, 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 131};
            
            var points = new[]
            {
                new PointF {X = 27.375f, Y = 0f},
                new PointF {X = 22.252f, Y = 0f},
                new PointF {X = 17.434f, Y = 2.003f},
                new PointF {X = 13.812f, Y = 5.625f},
                new PointF {X = 7.1095f, Y = 12.327f},
                new PointF {X = 6.2992f, Y = 24.922f},
                new PointF {X = 12.031f, Y = 32.562f},
                new PointF {X = 27.375f, Y = 54.75f},
                new PointF {X = 42.719f, Y = 32.594f},
                new PointF {X = 48.474f, Y = 24.923f},
                new PointF {X = 47.64f, Y = 12.327f},
                new PointF {X = 40.938f, Y = 5.625002f},
                new PointF {X = 37.316f, Y = 2.003002f},
                new PointF {X = 32.497f, Y = 1.907349E-06f},
                new PointF {X = 27.375f, Y = 1.907349E-06f},
                new PointF {X = 27.5625f, Y = 14f},
                new PointF {X = 30.3195f, Y = 14f},
                new PointF {X = 32.5625f, Y = 16.243f},
                new PointF {X = 32.5625f, Y = 19f},
                new PointF {X = 32.5625f, Y = 21.757f},
                new PointF {X = 30.3195f, Y = 24f},
                new PointF {X = 27.5625f, Y = 24f},
                new PointF {X = 24.8055f, Y = 24f},
                new PointF {X = 22.5625f, Y = 21.757f},
                new PointF {X = 22.5625f, Y = 19f},
                new PointF {X = 22.5625f, Y = 16.243f},
                new PointF {X = 24.8055f, Y = 14f},
                new PointF {X = 27.5625f, Y = 14f}
            };

            Selectable = false;
            Data = new GoDrawingData(pathTypes, points);
        }
    }
}
