﻿using GEx.Iface;

namespace GExGoDiagram
{
    /// <summary>
    /// Реализация IDiagramFactory для Norhwoods GoDiagram
    /// </summary>
    public class GoDiagramFactory : GenericDiagramFactory<GazExportGoNode, GazExportLink, RasterLayerGoView>{};

    /*
    public class GoDiagramFactory : IDiagramFactory
    {
        INodeView IDiagramFactory.CreateNode()
        {
            var node = new GazExportGoNode();
            return node;
        }


        IConnectionView IDiagramFactory.CreateConnection()
        {
            var link = new GazExportLink();
            return link;
        }

        INetworkView IDiagramFactory.CreateCanvas()
        {
            var view = new RasterLayerGoView();
            return view;
        }
    }
    */ 
}
