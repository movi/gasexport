using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using GEx.Iface;
using Northwoods.Go;

namespace GExGoDiagram
{
    public class RasterLayerGoView : GoView, INetworkView
    {
        public RasterLayerGoView()
        {
            DoExtraInit();
        }

        /// <summary>
        /// ��������� �������� �� �����,
        /// ��������� ������� ���������� ��������
        /// </summary>
        /// <param name="underlayer"></param>
        public void SetUnderlayer(Bitmap underlayer)
        {
            DragsRealtime = true;
            ExternalDragDropsOnEnter = true;

            ArrowMoveLarge = 10F;
            ArrowMoveSmall = 1F;

            var bitmap = new Bitmap(underlayer, _map.Size);
            using (var ms = new MemoryStream())
            {
                bitmap.Save(ms, ImageFormat.Png);
                MapUnderlayer = ms.ToArray();
            }
            SetMap(bitmap);
        }

        private Image _img;
        private bool _isBackgroundImageVisible;
        
        /// <summary>
        /// ������������� ��������� ��������
        /// </summary>
        public bool IsBackGroundImageVisible
        {
            set
            {
                _isBackgroundImageVisible = value;
            }
        }

        /// <summary>
        /// 
        /// ��� ���. �������� ��������� GoView:
        /// ��������, ������������� ����� � ��.
        /// 
        /// </summary>
        private void DoExtraInit()
        {
            DragsRealtime = true;
            ExternalDragDropsOnEnter = true;

            ArrowMoveLarge = 10F;
            ArrowMoveSmall = 1F;

            var img = map.underlayer;
            SetMap(new Bitmap(img));
        }

        public void AddToNetwork(INodeView nodeView)
        {
            var go = nodeView as GazExportGoNode;
            if (go != null)
            {
                Document.Add(go);
            }
        }

        void INetworkView.AddToNetwork(IConnectionView connectionView)
        {
            var link = connectionView as GazExportLink;
//            link.Brush = new SolidBrush(Color.Red);
//            link.Pen = new Pen(Color.Red, 12);
            if (link != null)
            {
                Document.Add(link);
            }
        }

        void INetworkView.AddToNetwork(INodeView nodeView)
        {
            Document.Add(nodeView as GoObject);
        }

        System.Collections.Generic.IEnumerable<INodeView> INetworkView.EnumerateNodes()
        {
            return this.Document.OfType<INodeView>();
        }

        System.Collections.Generic.IEnumerable<IConnectionView> INetworkView.EnumerateLinks()
        {
            return this.Document.OfType<IConnectionView>();
        }

        /// <summary>
        /// ������ ����� GoDocument
        /// GoDocument - �������� ������ ��� �������� ����������� GoView
        /// </summary>
        void INetworkView.ClearDoc()
        {
            Document = new GoDocument();
            DoExtraInit();
        }

        /// <summary>
        /// ������� ����� (edje)
        /// </summary>
        void INetworkView.ClearLinks()
        {
            var list = this.Document.OfType<GoLink>().ToList();
            foreach (var goLink in list)
            {
                Document.Remove(goLink);
            }
        }

        /// <summary>
        /// ������� ����
        /// </summary>
        /// <param name="dtonode"></param>
        void INetworkView.RemoveNodeAsSelectedForPath(DTO_Node dtonode)
        {
            if (dtonode == null) return;

            var node = Document.OfType<INodeView>().FirstOrDefault(x => x.NodeDocument.ID == dtonode.ID);
            if (node == null) return;

            (node as GazExportGoNode).SetAsPlaceHolder(false);
        }

        public void SetNodeAsSelectedForPath(DTO_Node dtonode)
        {
            if (dtonode == null) return;

            var node = Document.OfType<INodeView>().FirstOrDefault(x => x.NodeDocument.ID == dtonode.ID);
            if (node == null) return;

            (node as GazExportGoNode).SetAsPlaceHolder(true);
        }

        void INetworkView.SetPosition(float x, float y)
        {
            DocExtentCenter =
                new PointF(x, y);
        }

        /// <summary>
        /// �������� ����� �������� �� �������� �� ���������, �������� ������ � ������ ��������� ��������
        /// </summary>
        public byte[] MapUnderlayer { get; set; }

        private Bitmap _map;
        private Graphics bmpGfx;
        private IntPtr hBitmap;

        public void DrawStretch(IntPtr hBitmap, Graphics srcGfx, Graphics destGfx,
            Rectangle srcRect, Rectangle destRect)
        {
            IntPtr pTarget = destGfx.GetHdc();
            IntPtr pSource = CreateCompatibleDC(pTarget);
            IntPtr pOrig = SelectObject(pSource, hBitmap);
            SetStretchBltMode(pTarget, 0x04);
            if (!StretchBlt(pTarget, destRect.X, destRect.Y, destRect.Width, destRect.Height,
                pSource, srcRect.X, srcRect.Y, srcRect.Width, srcRect.Height,
                TernaryRasterOperations.SRCCOPY))
                throw new Win32Exception(Marshal.GetLastWin32Error());

            IntPtr pNew = SelectObject(pSource, pOrig);
            DeleteDC(pSource);
            destGfx.ReleaseHdc(pTarget);
        }

        [DllImport("gdi32.dll", EntryPoint = "SelectObject")]
        private static extern System.IntPtr SelectObject(
            [In()] System.IntPtr hdc,
            [In()] System.IntPtr h);

        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        private static extern bool DeleteDC(IntPtr hdc);

        [DllImport("gdi32.dll", EntryPoint = "DeleteObject")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool DeleteObject(
            [In()] System.IntPtr ho);

        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        private static extern IntPtr CreateCompatibleDC(IntPtr hdc);

        [DllImport("gdi32.dll")]
        private static extern bool StretchBlt(IntPtr hdcDest, int nXOriginDest, int nYOriginDest,
            int nWidthDest, int nHeightDest,
            IntPtr hdcSrc, int nXOriginSrc, int nYOriginSrc, int nWidthSrc, int nHeightSrc,
            TernaryRasterOperations dwRop);

        [DllImport("gdi32.dll")]
        private static extern int ExcludeClipRect(
            IntPtr hdc, int nLeftRect, int nTopRect, int nRightRect, int nBottomRect);

        [DllImport("user32.dll")]
        private static extern int FillRect(IntPtr hDC, [In] ref RECT lprc, IntPtr hbr);

        [DllImport("gdi32.dll")]
        private static extern IntPtr CreatePen(int fnPenStyle, int nWidth, int crColor);

        [DllImport("gdi32.dll")]
        private static extern IntPtr CreateSolidBrush(int crColor);

        /// <summary>
        /// SetStretchBltMode
        /// </summary>
        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        private static extern bool SetStretchBltMode(IntPtr hObject, int nStretchMode);


        public enum TernaryRasterOperations : uint
        {
            SRCCOPY = 0x00CC0020
            //there are many others but we don't need them for this purpose, omitted for brevity
        }
        
        /// <summary>
        /// ����� ���������� ����� ��� ��������� ��������, ��� 
        /// OnPaint - � ������� GoView ��� ��� ����������
        /// ����������� ����, ������ ��� ������ �������������
        /// </summary>
        /// <param name="g"></param>
        /// <param name="clipRect"></param>
        protected override void PaintPaperColor(Graphics g, RectangleF clipRect)
        {
            if (!_isBackgroundImageVisible)
            {
                base.PaintPaperColor(g,clipRect);
                return;
            }

            var canvasSize = new Size((int)_map.Width, (int)_map.Height);
            var viewRectWidth = Width;
            var viewRectHeight = Height;
            var zoom = DocScale;
            var pt = new Point((int)DocPosition.X, (int)DocPosition.Y);

            if (_map == null) return;

            Rectangle srcRect, distRect;
            //if (CanvasSize.Width * zoom < viewRectWidth && CanvasSize.Height * zoom < viewRectHeight)
            //    srcRect = new Rectangle(0, 0, CanvasSize.Width, CanvasSize.Height);  // view all image
            //else 
            srcRect = new Rectangle(pt, new Size((int)(viewRectWidth / zoom), (int)(viewRectHeight / zoom))); // view a portion of image

          //  Graphics g = e.Graphics;

            Rectangle mapRect = srcRect;//whatever zoom/pan logic you implemented.

            Rectangle windowRect = new Rectangle(0, 0, viewRectWidth, viewRectHeight); // view window width and height


            int left = -mapRect.Left;
            int right = -mapRect.Right;
            int top = mapRect.Top + mapRect.Bottom;
            int bottom = mapRect.Bottom;

            ;// size.Width

            DrawStretch(
                hBitmap,
                bmpGfx,
                g,
                mapRect,
                windowRect);

            var drawnRect = new RECT();
            var winRect = new RECT(windowRect);

            bool needToBleach = false;
            var viewPoint = ConvertDocToView(pt);

            if ((pt.X < 0 || pt.Y < 0)
                ||(pt.X > (canvasSize.Width - (int)(windowRect.Right / DocScale)) || pt.Y > (canvasSize.Height - (int)(windowRect.Right / DocScale))))
            {
                needToBleach = true;
            }

            if (needToBleach)
            {
                drawnRect.Left = 10;
                drawnRect.Top = 10;
                drawnRect.Right = canvasSize.Width - viewPoint.X-10;
                drawnRect.Bottom = canvasSize.Height - viewPoint.Y-10;
                
                //FullRegion
                var fullrect = new Rectangle(pt.X-10, pt.Y-10, (int)(windowRect.Right / DocScale)+10, (int)(windowRect.Right / DocScale)+10);
                var rgn = new Region(fullrect);
                var path = new GraphicsPath();
                path.AddRectangle(drawnRect);
                rgn.Exclude(path);
                g.FillRegion(Brushes.White, rgn);
            }
        }

        private void SetMap(Bitmap bmp)
        {
            _map = bmp;
            //dispose/delete any previous caches
            if (bmpGfx != null) bmpGfx.Dispose();
            if (hBitmap != null) DeleteObject(hBitmap);
            if (bmp == null) return;
            //cache the new HBitmap and Graphics.
            bmpGfx = Graphics.FromImage(_map);
            hBitmap = _map.GetHbitmap();
        }
    }
}