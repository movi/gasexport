using System;
using System.Drawing;
using System.Windows.Forms;
using Northwoods.Go;

namespace GExGoDiagram.GoTools
{
    /// <summary>
    /// ����������� � ������������.
    /// </summary>
    public class DragViewTool : GoToolZooming
    {
        protected enum Mode
        {
            Drag,
            Zoom
        }

        protected Mode _mode;

        #region Fields

        /// <summary>
        /// ��������� ��������� ���������
        /// </summary>
        [NonSerialized]
        protected PointF _init_location;

        [NonSerialized]
        bool _activate;

        Cursor _old_cursor;

        #endregion //Fields

        #region Constructor

        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="view">���</param>
        public DragViewTool(GoView view)
            : base(view)
        {
            _activate = false;
        }

        #endregion //Constructor

        #region Functions

        protected SizeF CalculateDelta()
        {
            PointF point1 = FirstInput.ViewPoint;
            PointF point2 = LastInput.ViewPoint;
            return new SizeF(point2.X - point1.X, point2.Y - point1.Y);
        }

        #endregion //Functions

        #region Overrides

        public override bool CanStart()
        {
            GoObject obj =
                View.PickObject(true, false, FirstInput.DocPoint, true);

            if (obj != null && View.Selection.Contains(obj))
                return false;
            
            //��������� ��������� ������������ ����
            if (obj != null && LastInput.Buttons == MouseButtons.Right)
                return false;
            
            return true;
        }

        public override void Start()
        {
            _old_cursor = View.Cursor;

            _init_location = View.DocPosition;
            _activate = true;

            if (LastInput.Buttons == MouseButtons.Left)
                _mode = Mode.Drag;
            if (LastInput.Buttons == MouseButtons.Right)
                _mode = Mode.Zoom;
        }

        public override void DoMouseDown()
        {
            if (_mode == Mode.Drag)
            {
                SizeF delta = CalculateDelta();
                if (delta.IsEmpty)
                {
                    StopTool();
                    return;
                }
            }
            base.DoMouseDown();
        }

        public override void DoMouseMove()
        {
            if (!_activate) return;

            if (_mode == Mode.Drag)
            {
                if (_mode == Mode.Drag) View.Cursor = Cursors.Hand;
                SizeF delta = CalculateDelta();

                float scale = View.DocScale;

                View.DocPosition = new PointF(_init_location.X - delta.Width / scale,
                    _init_location.Y - delta.Height / scale);
            }
            if (_mode == Mode.Zoom)
            {
                if (_mode == Mode.Zoom) View.Cursor = Cursors.Cross;
                base.DoMouseMove();
            }
        }

        public override void DoMouseUp()
        {
            var delta = CalculateDelta();
            if (delta.IsEmpty)
            {
                GoObject obj = View.PickObject(true, false, FirstInput.DocPoint, true);
                
                //����� ������������ ��������� ��� �������� (����� ������� ������� ������ ������ - �� ��� ����� ��������� �
                //�������� ���� ��������������
                if (obj != null && !View.Selection.Contains(obj))
                {
                    //TODO:������� �� ������ 
                    View.Selection.Clear();
                    View.Selection.Add(obj);
                }
            }

            if (_activate)
            {
                if (_mode == Mode.Zoom)
                {
                    Box = ComputeRubberBandBox();
                    DoRubberBand(Box);
                }
            }

            StopTool();
        }


        public override void Stop()
        {
            if (_old_cursor != null)
                View.Cursor = _old_cursor;

            _activate = false;

            base.Stop();
        }

        #endregion //Overrides
    }
}
