﻿using System;
using System.Drawing;
using System.Windows.Forms;
using GEx.Iface;
using Northwoods.Go;

//using GEx.Domain;

namespace GExGoDiagram.GoTools
{
    public delegate void OnAddToScheme(GazExportGoNode node);

    public class CreateTool : GoTool
    {
        public Action<GazExportGoNode> OnCreateNewNode { get; set; }

        public Action ExitTool;


        //private GasExportObjectType _nodeType;
        private DTO_Node_type _nodeType;
        
        /// <summary>
        /// CreateTool создает узлы определенного типа
        /// </summary>
        public DTO_Node_type NodeType
        {
            get
            {
                return _nodeType;
            }

            set
            {
                _nodeType = value;
                _prototype.SetNodeType(_nodeType);
            }
        }

        private GazExportGoNode _prototype;

        public CreateTool(GoView view)
            : base(view)
        {
            _prototype = new GazExportGoNode();
        }

        public override bool CanStart()
        {
            return false; // cannot be run as a modeless tool -- must assign GoView.Tool
        }

        public override void Start()
        {
            View.CursorName = "crosshair";
            RectangleF extent = View.DocExtent; // show the initial prototype offscreen
            Prototype.Position = new PointF(extent.Right + 10, extent.Bottom + 10);
            View.Layers.Default.Add(Prototype);
        }

        public override void Stop()
        {
            Prototype.Remove(); // from view layer
            View.CursorName = "default";
        }

        public override void DoMouseMove()
        {
            Prototype.Location = LastInput.DocPoint;
            if (View.Document.IsUnoccupied(Prototype.Bounds, null))
                View.CursorName = "crosshair";
            else
                View.CursorName = "not-allowed";
        }

        public override void DoMouseUp()
        {
            Prototype.Location = LastInput.DocPoint;

            //CreateTool реагирует только на левую кнопку мыши
            //
            //Пока не договорились о другом - все остальные кнопки (правая, средняя, боковые, какие угодно еще)
            //отменяют действие этого Tool и возвращают стандартный Tool
            if (LastInput.MouseEventArgs.Button != MouseButtons.Left)
            {
                if (ExitTool != null)
                    ExitTool();
                return;
            }

            if (!IsBeyondDragSize() && View.Document.IsUnoccupied(Prototype.Bounds, null))
            {
                StartTransaction();
                var obj = View.Document.AddCopy(Prototype, Prototype.Location);
                var goNode = obj as GazExportGoNode;

                goNode.Text = "Новый узел";//Если эту строчку закоменить то будет Эксепшн из-за Label=null
                
                //Устанавливаем Label в начальную позицию
                
                //TODO: нужно устанавливать в 0,0 - данные цифры - временно
                ((INodeView)goNode).LabelX = ((INodeView)goNode).X - 50;
                ((INodeView)goNode).LabelY = ((INodeView)goNode).Y - 75;

          
                goNode.AdjustFont();

                if (OnCreateNewNode != null)
                {
                    OnCreateNewNode(goNode);
                }

                TransactionResult = "inserted object";
                StopTransaction();
            }
        }

        public GoObject Prototype
        {
            get { return _prototype; }
        }

        public override void DoKeyDown()
        {
            if (LastInput.Key == System.Windows.Forms.Keys.Delete)
            {
                if (this.Selection.Count > 0)
                { 
                
                }
            }
            base.DoKeyDown();
        }
    }
}
