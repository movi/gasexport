﻿using System.Drawing;
using Northwoods.Go;

namespace GExGoDiagram.GoTools
{
    public class GExToolManager : GoToolManager
    {
        public GExToolManager(GoView v) : base(v)
        {
        }

        public override void DoMouseWheel()
        {
            //base.View.DoWheel(base.LastInput);

            // mva
            // завершить редактирование любых элементов на схеме, прежде чем приступать к масштабированию,
            // иначе это приводит к ошибке в Win 7 (предположительно, в Висте - тоже) при одновременном масштабировании и редактировании надписей на дугах
            // (если установлен фокус в редактирование надписи)
            //
            // Ошибка (System.ArgumentException: Parameter is not valid. at System.Drawing.Graphics.CheckErrorStatus(Int32 status)
            base.View.DoEndEdit();

            GoInputEventArgs evt = base.LastInput;
            PointF tf1 = View.DocPosition;
            View.DocScale *= 1f + (evt.Delta/2400f);
            PointF tf2 = View.ConvertViewToDoc(LastInput.ViewPoint);
            View.DocPosition =
                new PointF((tf1.X + evt.DocPoint.X) - tf2.X, (tf1.Y + evt.DocPoint.Y) - tf2.Y);
        }
    }
}

