﻿using GEx.Iface;
using Northwoods.Go;

namespace GExGoDiagram
{
    /// <summary>
    /// Wrapper for GoPort
    /// </summary>
    public class GazExportPort : IPort
    {
        private GoPort _inner;

        public GazExportPort(GoPort port)
        {
            _inner = port;
        }

        internal IGoPort GetGoPort()
        {
            return _inner;
        }
    }
}
