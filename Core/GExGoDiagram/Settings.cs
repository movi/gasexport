﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GExGoDiagram
{
    public static class Settings
    {
        /// <summary>
        /// Hint события смены признака IsOutput для GoNode
        /// </summary>
        public static readonly int IsOutputChangedSubHint = 100000;
    }
}
