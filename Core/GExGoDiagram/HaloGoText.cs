﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using GEx.Iface;
using Northwoods.Go;

namespace GExGoDiagram
{
    /// <summary>
    /// GoText с обводкой
    /// </summary>
    public class HaloGoText : GoText, ILabelView
    {
        /// <summary>
        /// Использовать обводку или нет
        /// </summary>
        public bool UseHalo { get; set; }

        private INodeView _parent;
        INodeView ILabelView.Parent
        {
            get
            {
                return _parent; 
                
            }
        }


        public override bool OnSingleClick(GoInputEventArgs evt, GoView view)
        {
            return false;    
        }

        public override bool OnDoubleClick(GoInputEventArgs evt, GoView view)
        {
            if (!CanEdit()) return false;
            if (!view.CanEditObjects()) return false;

            if (evt.Shift || evt.Control) return false;
            DoBeginEdit(view);

            return true;

        }


        public HaloGoText(INodeView node)
        {
            _parent = node;
        }

        public Color OutlineForeColor { get; set; }
        public float OutlineWidth { get; set; }

        internal static Pen NewPen(Color c, float w)
        {
            return new Pen(c, w);
        }

        private void DrawText(Graphics g, GoView view, string text)
        {
            RectangleF bounds = Bounds;
            Color textColor = TextColor;
            Brush brush4 = (textColor == Color.Black) ? new SolidBrush(Color.Black) : new SolidBrush(TextColor);
            PaintText(text, g, view, bounds, brush4);

            if (textColor != Color.Black)
            {
                brush4.Dispose();
            }
        }

        public override void Paint(Graphics g, GoView view)
        {
            if (!PaintGreek(g, view))
            {
                RectangleF bounds = Bounds;
                if (!TransparentBackground)
                {
                    if (Shadowed)
                    {
                        SizeF shadowOffset = GetShadowOffset(view);
                        Brush shadowBrush = GetShadowBrush(view);
                        GoShape.DrawRectangle(g, view, null, shadowBrush, bounds.X + shadowOffset.Width, bounds.Y + shadowOffset.Height, bounds.Width, bounds.Height);
                    }
                    Color backgroundColor = BackgroundColor;
                    Brush brush = (backgroundColor == Color.White) ? new SolidBrush(Color.White) : new SolidBrush(BackgroundColor);
                    GoShape.DrawRectangle(g, view, null, brush, bounds.X, bounds.Y, bounds.Width, bounds.Height);
                    if (backgroundColor != Color.White)
                    {
                        brush.Dispose();
                    }
                }
                string text = Text;
                float width = 1f;
                if (view != null)
                {
                    width /= 1.0f;
                }
                if (Shadowed && TransparentBackground)
                {
                    RectangleF rect = bounds;
                    SizeF ef4 = GetShadowOffset(view);
                    rect.X += ef4.Width;
                    rect.Y += ef4.Height;
                    if (Bordered)
                    {
                        Pen shadowPen = GetShadowPen(view, width);
                        GoShape.DrawRectangle(g, view, shadowPen, null, rect.X - (width / 2f), rect.Y, rect.Width + width, rect.Height);
                    }
                    if (text.Length > 0)
                    {
                        Brush textbrush = GetShadowBrush(view);
                        PaintText(text, g, view, rect, textbrush);
                    }
                }
                Color textColor = TextColor;
                if (Bordered)
                {
                    Pen pen = NewPen(textColor, width);
                    GoShape.DrawRectangle(g, view, pen, null, bounds.X - (width / 2f), bounds.Y, bounds.Width + width, bounds.Height);
                    pen.Dispose();
                }
                if (text.Length > 0)
                {
                    DrawText(g, view, text);
                }
            }
        }
        private float getLineHeight(Font font)
        {
            return font.GetHeight();
        }
        private int FindFirstLineBreak(string str, int start)
        {
            int nextline = 0;
            return FindFirstLineBreak(str, start, ref nextline);
        }

        private static readonly char[] myNewlineArray = { '\r', '\n' };

        private int FindFirstLineBreak(string str, int start, ref int nextline)
        {
            int num = str.IndexOfAny(myNewlineArray, start);
            if (num >= 0)
            {
                if (((str[num] == '\r') && ((num + 1) < str.Length)) && (str[num + 1] == '\n'))
                {
                    nextline = num + 2;
                    return num;
                }
                nextline = num + 1;
            }
            return num;
        }
        private float getLineLeading(Font font)
        {
            FontFamily fontFamily = font.FontFamily;
            FontStyle style = font.Style;
            float size = font.Size;
            int lineSpacing = fontFamily.GetLineSpacing(style);
            int emHeight = fontFamily.GetEmHeight(style);
            float num4 = (size * (lineSpacing - emHeight)) / emHeight;
            return ((num4 * 2f) / 3f);
        }

        private float computeHeight(Graphics g, Font font, float maxw)
        {
            string text = Text;
            float num = getLineHeight(font);
            if (text.Length == 0)
            {
                myNumLines = 1;
                return num;
            }
            if (!Multiline)
            {
                int length = FindFirstLineBreak(text, 0);
                if (length >= 0)
                {
                    text = text.Substring(0, length);
                }
            }
            StringFormat fmt = getStringFormat(null);
            float num3 = 0f;
            myNumLines = 0;
            int start = 0;
            int nextline = 0;
            bool flag = false;
            while (!flag)
            {
                var num5 = FindFirstLineBreak(text, start, ref nextline);
                if (num5 == -1)
                {
                    num5 = text.Length;
                    flag = true;
                }
                if (start <= num5)
                {
                    string str = text.Substring(start, num5 - start);
                    if (str.Length > 0)
                    {
                        if (Wrapping)
                        {
                            SizeF area = new SizeF(maxw, 1E+09f);
                            int lines = 0;
                            SizeF ef2 = measureString(str, g, font, fmt, area, out lines);
                            num3 += ef2.Height;
                            myNumLines += lines;
                        }
                        else
                        {
                            num3 += num;
                            myNumLines++;
                        }
                    }
                    else
                    {
                        num3 += num;
                        myNumLines++;
                    }
                }
                start = nextline;
            }
            return num3;
        }
        private float getStringWidth(string str, Graphics g, Font font, StringFormat fmt)
        {
            return g.MeasureString(str, font, new PointF(), fmt).Width;
        }

        private float computeWidth(Graphics g, Font font)
        {
            string text = Text;
            if (text.Length == 0)
            {
                return 0f;
            }

            StringFormat myStandardStringFormat = _myStandardStringFormat;
            if (Multiline)
            {
                float num = 0f;
                int start = 0;
                bool flag = false;
                int nextline = 0;
                while (!flag)
                {
                    int num4 = FindFirstLineBreak(text, start, ref nextline);
                    if (num4 == -1)
                    {
                        num4 = text.Length;
                        flag = true;
                    }
                    string str = text.Substring(start, num4 - start);
                    float num5 = getStringWidth(str, g, font, myStandardStringFormat);
                    if (Wrapping && (num5 > WrappingWidth))
                    {
                        return WrappingWidth;
                    }
                    if (num5 > num)
                    {
                        num = num5;
                    }
                    start = nextline;
                }
                return num;
            }
            int length = FindFirstLineBreak(text, 0);
            if (length >= 0)
            {
                text = text.Substring(0, length);
            }
            float num7 = getStringWidth(text, g, font, myStandardStringFormat);
            if (Wrapping && (num7 > WrappingWidth))
            {
                return WrappingWidth;
            }
            return num7;
        }
        private bool fitsInBox(Graphics g, Font font, RectangleF rect)
        {
            float num = computeWidth(g, font);
            if (rect.Width < num)
            {
                return false;
            }
            float num2 = computeHeight(g, font, rect.Width);
            if (rect.Height < num2)
            {
                return false;
            }
            return true;
        }
        private Font makeFont(string name, float size, FontStyle style)
        {
            byte gdiCharSet = (byte)GdiCharSet;
            Font font = null;
            try
            {
                font = new Font(name, size, style, GraphicsUnit.Point, gdiCharSet);
            }
            catch (Exception)
            {
            }
            return font;
        }
        private Font findLargestFont(Graphics g, RectangleF rect, float minfsize, float maxfsize)
        {
            if (minfsize <= 0f)
            {
                minfsize = 0.01f;
            }
            string name = Font.Name;
            FontStyle style = Font.Style;
            float size = 10f;
            Font font = null;
            while (((size <= maxfsize) && fitsInBox(g, font = makeFont(name, size, style), rect)) && (font != null))
            {
                font.Dispose();
                size++;
            }
            if (font != null)
            {
                font.Dispose();
            }
            for (size -= 0.1f; (!fitsInBox(g, font = makeFont(name, size, style), rect) && (size > (minfsize + 0.1f))) && (font != null); size -= 0.1f)
            {
                font.Dispose();
            }
            return font;
        }
        internal static RectangleF IntersectionRect(RectangleF a, RectangleF b)
        {
            float x = Math.Max(a.X, b.X);
            float y = Math.Max(a.Y, b.Y);
            float num3 = Math.Min(a.X + a.Width, b.X + b.Width);
            float num4 = Math.Min(a.Y + a.Height, b.Y + b.Height);
            return new RectangleF(x, y, Math.Max(0f, num3 - x), Math.Max(0f, num4 - y));
        }

        private void PaintText(string str, Graphics g, GoView view, RectangleF rect, Brush textbrush)
        {
            if (str.Length != 0)
            {
                Font oldFont = Font;
                if (oldFont != null)
                {
                    Font newFont = null;
                    float num = getLineHeight(oldFont);
                    bool clipping = Clipping;
                    Region clip = null;
                    Region region2 = null;
                    if (clipping)
                    {
                        RectangleF ef = IntersectionRect(rect, g.ClipBounds);
                        clip = g.Clip;
                        region2 = new Region(ef);
                        g.Clip = region2;
                    }
                    if (!Multiline)
                    {
                        int length = FindFirstLineBreak(str, 0);
                        if (length >= 0)
                        {
                            str = str.Substring(0, length);
                        }
                    }
                    StringFormat fmt = getStringFormat(view);
                    if (view.IsPrinting && AutoResizes)
                    {
                        newFont = findLargestFont(g, Bounds, oldFont.Size - 1f, oldFont.Size);
                        oldFont = newFont;
                    }
                    float num3 = -getLineLeading(oldFont);
                    int start = 0;
                    int nextline = -1;
                    bool flag2 = false;
                    while (!flag2)
                    {
                        int num5 = FindFirstLineBreak(str, start, ref nextline);
                        if (num5 == -1)
                        {
                            num5 = str.Length;
                            flag2 = true;
                        }
                        if (start <= num5)
                        {
                            string str2 = str.Substring(start, num5 - start);
                            if (str2.Length > 0)
                            {
                                RectangleF ef2 = new RectangleF(rect.X, rect.Y + num3, rect.Width, (rect.Height - num3) + 0.01f);
                                drawString(str2, g, view, oldFont, textbrush, ef2, fmt);
                                if (Wrapping)
                                {
                                    int lines = 0;
                                    SizeF ef3 = measureString(str2, g, oldFont, fmt, new SizeF(ef2.Width, ef2.Height), out lines);
                                    num3 += ef3.Height;
                                }
                                else
                                {
                                    num3 += num;
                                }
                            }
                            else
                            {
                                num3 += num;
                            }
                        }
                        start = nextline;
                    }
                    if (newFont != null)
                    {
                        newFont.Dispose();
                    }
                    if (clipping && (clip != null))
                    {
                        g.Clip = clip;
                    }
                    if (region2 != null)
                    {
                        region2.Dispose();
                    }
                }
            }
        }
        private static StringFormat MakeStandardStringFormat()
        {
            //StringFormat format;
            //return new StringFormat(StringFormat.GenericTypographic) { FormatFlags = format.FormatFlags | StringFormatFlags.MeasureTrailingSpaces, FormatFlags = format.FormatFlags & ~StringFormatFlags.LineLimit };

            var stringFormat = new StringFormat(StringFormat.GenericTypographic);
            stringFormat.FormatFlags |= StringFormatFlags.MeasureTrailingSpaces;
            stringFormat.FormatFlags &= ~StringFormatFlags.LineLimit;
            return stringFormat;
        }

        private static StringFormat _myStandardStringFormat = MakeStandardStringFormat();

        private StringFormat myStringFormat;
        private int myNumLines = 1;

        private StringFormat getStringFormat(GoView view)
        {
            if (myStringFormat == null)
            {
                myStringFormat = new StringFormat(_myStandardStringFormat);
            }
            myStringFormat.Trimming = StringTrimming;
            if (StringTrimming == StringTrimming.None)
            {
                myStringFormat.FormatFlags &= ~StringFormatFlags.LineLimit;
            }
            else
            {
                myStringFormat.FormatFlags |= StringFormatFlags.LineLimit;
            }
            int alignment = Alignment;
            if (alignment <= 0x10)
            {
                switch (alignment)
                {
                    case 1:
                        goto Label_00BF;

                    case 4:
                    case 8:
                        goto Label_00CD;
                }
            }
            else if (alignment <= 0x40)
            {
                switch (alignment)
                {
                    case 0x20:
                        goto Label_00BF;

                    case 0x40:
                        goto Label_00CD;
                }
            }
            else if (alignment == 0x80)
            {
                goto Label_00BF;
            }
            myStringFormat.Alignment = StringAlignment.Near;
            goto Label_00D9;
        Label_00BF:
            myStringFormat.Alignment = StringAlignment.Center;
            goto Label_00D9;
        Label_00CD:
            myStringFormat.Alignment = StringAlignment.Far;
        Label_00D9:
            if (isRightToLeft(view))
            {
                myStringFormat.FormatFlags |= StringFormatFlags.DirectionRightToLeft;
            }
            else
            {
                myStringFormat.FormatFlags &= ~StringFormatFlags.DirectionRightToLeft;
            }
            if (Wrapping)
            {
                myStringFormat.FormatFlags &= ~StringFormatFlags.NoWrap;
            }
            else
            {
                myStringFormat.FormatFlags |= StringFormatFlags.NoWrap;
            }
            return myStringFormat;
        }

        internal bool isRightToLeft(GoView view)
        {
            if (RightToLeftFromView && (view != null))
            {
                return (view.RightToLeft == System.Windows.Forms.RightToLeft.Yes);
            }
            return RightToLeft;
        }
        
        
        /// <summary>
        /// Весь текст отрисовки вытащен через Reflector
        /// Изменения по отрисовке обводки находятся здесь и только здесь.
        /// Текст с обводкой рисуется через DrawPath и FillPath
        /// </summary>
        private void drawString(string str, Graphics g, GoView view, Font font, Brush br,
            RectangleF rect, StringFormat fmt)
        {
            //var boldFont = new Font(font.FontFamily, font.Size*1.3f);

            //var b = new SolidBrush(Color.White);
            //g.DrawString(str, boldFont, b, rect, fmt);

            //g.DrawString(str, font, br, rect, fmt);

            //   RectangleF bounds = this.Bounds;
            OutlineForeColor = Color.White;
    OutlineWidth = font.Size/5;
            GraphicsState before = g.Save();

            using (GraphicsPath gp = new GraphicsPath())
            using (Pen outline = new Pen(OutlineForeColor, OutlineWidth) { LineJoin = LineJoin.Round })
            {
                Color ForeColor = Color.Black;
                using (Brush foreBrush = new SolidBrush(ForeColor))
                {
                    Color textColor = TextColor;
                    Brush brush = (textColor == Color.Black)
                        ? new SolidBrush(Color.Black)
                        : new SolidBrush(TextColor);
                    if (UseHalo)
                    {
                    
                        //hack, подгоняющий gp.AddString к размеру graphics.DrawString
                        float emSize = g.DpiY*font.Size/72;
                        gp.AddString(str, font.FontFamily, (int) font.Style,
                            emSize, rect, fmt);
                        //   g.ScaleTransform(1/1.3f, 1/1.35f);
                        g.SmoothingMode = SmoothingMode.HighQuality;
                        g.DrawPath(outline, gp);
                        g.FillPath(foreBrush, gp);
                    }
                    else
                    {
                        g.DrawString(str, font, br, rect, fmt);
                    }
                    if (textColor != Color.Black)
                    {
                        brush.Dispose();
                    }
                }
            }
            g.Restore(before);
        }

        private SizeF measureString(string str, Graphics g, Font font, StringFormat fmt, SizeF area, out int lines)
        {
            int charactersFitted;
            return g.MeasureString(str, font, area, fmt, out charactersFitted, out lines);
        }
    }
}
