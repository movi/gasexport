﻿using System;
using System.Configuration;
using System.Drawing;
using System.Windows.Forms;
using GEx.Iface;
using GExGoDiagram.SvgParser;
using Northwoods.Go;

//using GEx.Domain;

namespace GExGoDiagram
{
    /// <summary>
    /// Основной узел с меткой для прототипа
    /// (пока со стандартной меткой)
    /// 
    /// GoBasicNode - просто и со вкусом - 
    /// Icon нет, зато:
    /// - стандартный кружок с заливкой
    /// - есть лейбла
    /// - есть порт (отключаем)
    /// </summary>
    public class GazExportGoNode : GoIconicNode, INodeView
    {
        private Func<DTO_Node, bool> _chooseFirstPoint;
        private Action<DTO_Node> _chosenFirstPoint;
        private Action<DTO_Node> _chooseSecondPoint;
        private Action<DTO_Node> _removePoint;

        /// <summary>
        /// Объект-узел из доменной модели
        /// </summary>
        private DTO_Node _nodeDocument;

        private DTO_Node_type _nodeType;

        private void RefreshColor()
        {
            Shape.Brush = new SolidBrush(GetColor(_nodeType));
        }

        public GazExportGoNode()
        {
            Init();
        }

        public override GoContextMenu GetContextMenu(GoView view)
        {
            var cm = new GoContextMenu(view);
            cm.MenuItems.Add("Первая точка маршрута", OnClickSetFirstPoint);
            cm.MenuItems.Add("Конечная точка маршрута", OnClickSetSecondPoint);
            cm.MenuItems.Add("Удалить точку из маршрута", OnClickRemovePoint);
            if (_nodeType == DTO_Node_type.Border
                //добавим немного дублирования - код для бордера и VTP должен быть одинаков
                ||_nodeType == DTO_Node_type.VirtualTradingPoint)
            {
                MenuItem itm = new MenuItem("Пункт сдачи", OnClickIsOutput) { Checked = _nodeDocument.IsOutput };
                cm.MenuItems.Add(itm);
            }
            return cm;
        }

        private void OnClickIsOutput(object sender, EventArgs e)
        {
            _nodeDocument.IsOutput = !_nodeDocument.IsOutput;

            Changed(Settings.IsOutputChangedSubHint, 0, 0, Bounds, 1, 1, Bounds);

            RefreshColor();
        }

        private void OnClickSetFirstPoint(object sender, EventArgs eventArgs)
        {
            ChooseThisAsAFirstPoint();
        }

        private void ChooseThisAsAFirstPoint()
        {
            if (_chooseFirstPoint != null)
            {

                bool isChosen = true;
                
                foreach (Delegate d in _chooseFirstPoint.GetInvocationList())
                    isChosen &= (bool)d.DynamicInvoke(_nodeDocument);

                if (!isChosen)
                {
                    MessageBox.Show(String.Format("{0} не может быть выбран в качестве первой точки", _nodeDocument.Name), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                
                if (_chosenFirstPoint != null)
                {
                    //узел успешно выбран, бросаем дальше
                    _chosenFirstPoint(_nodeDocument);
                }
                
                SetAsPlaceHolder(true);
            }
        }

        public void SetAsPlaceHolder(bool isSelectedForPath)
        {
            var lx = ((INodeView)this).LabelX;
            var ly = ((INodeView)this).LabelY;

            Icon = isSelectedForPath ? _selectedForPath.Copy() : _usual.Copy();
            
            ((INodeView)this).LabelX = lx;
            ((INodeView)this).LabelY = ly;
            RefreshColor();
        }

        private void OnClickSetSecondPoint(object sender, EventArgs e)
        {
            if (!this._nodeDocument.IsOutput)
            {
                MessageBox.Show(String.Format("{0} не может быть выбран в качестве второй точки", _nodeDocument.Name), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (_chooseSecondPoint != null)
                _chooseSecondPoint(_nodeDocument);
            
            SetAsPlaceHolder(true);
        }

        private void OnClickRemovePoint(object sender, EventArgs e)
        {
            var dialogResult = MessageBox.Show(string.Format("Удалить точку: {0}", _nodeDocument.Name), @"Удаление точки маршрута", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult != DialogResult.Yes) return;

            if (_removePoint != null)
                _removePoint(_nodeDocument);
            SetAsPlaceHolder(false);
        }

        protected override GoText CreateLabel(string name)
        {
            var label = //base.CreateLabel(name);//
                new HaloGoText(this);
            
            label.Text = name; 
            label.Editable = true;
            label.Selectable = true;
            label.EditableWhenSelected = false;
            
            bool useHalo;
            Boolean.TryParse(ConfigurationManager.AppSettings["UseLabelHalo"], out useHalo);
            label.UseHalo = useHalo;
            return label;
        }

        /// <summary>
        /// Надпись, в общем соответствующая объекту "Label" (подпись),
        /// связанному с узлом.
        /// </summary>
        /// <remarks>
        /// Переопределяем стандартное поведение, чтобы
        /// при удалении объекта надписи оставалась возможность
        /// редактировать надпись (Text никогда не возвращает null)        
        /// </remarks>
        public override string Text
        {
            get
            {
                if (Label != null)
                {
                    return Label.Text;
                }
                return "";
            }
            set
            {
                if (value == null)
                {
                    Remove(Label);
                }
                else if (Label == null)
                {
                    Label = CreateLabel(value);
                }
                else
                {
                    Label.Text = value;
                }
            }
        }

        private float startOffsetY = 50;
        
        private GoShape _usual;
        private GoShape _selectedForPath;

        private void Init()
        {
            this.DraggableLabel = true;

            _usual = new GoBasicNode().Shape.Copy() as GoShape;
            _selectedForPath = new GoSvgPlaceHolder();

            Icon = _usual;
            Icon.Selectable = false;

            RefreshColor();
            //Shape.PenColor = Color.Aquamarine;
            //Shape.PenWidth = 2;


            Width = float.Parse(ConfigurationManager.AppSettings["NodeWidth"]);

            Height = float.Parse(ConfigurationManager.AppSettings["NodeHeight"]); //40;
            Port = CreatePort();
            Port.Center = Icon.Center;
            //Port.Visible = false;
            _goPortWrapper = new GazExportPort(Port);

            DraggableLabel = true;
        }

        protected override GoPort CreatePort()
        {
            return new GoPort { Style = GoPortStyle.Ellipse, FromSpot = 0, ToSpot = 0, Size = _defaultPortSize, Visible = false};
        }

        private GazExportPort _goPortWrapper;

        private Color GetColor(DTO_Node_type nodeType)
        {
            Color defaultColor = Color.Crimson;

            switch (nodeType)
            {
                case DTO_Node_type.Border:
                    return GetBorderColor(); 
                case DTO_Node_type.VirtualTradingPoint:
                    return GetVTPColor();
                case DTO_Node_type.Output:
                    return Color.Chartreuse;
                case DTO_Node_type.RussianGasInput:
                    return Color.Coral;
                case DTO_Node_type.UGS:
                    return Color.Crimson;
                default:
                    return defaultColor;
            }
        }

        private Color GetBorderColor()
        {
            return _nodeDocument != null && _nodeDocument.IsOutput ? Color.DarkGreen : Color.BlueViolet;
        }
        
        private Color GetVTPColor()
        {
            return _nodeDocument != null && _nodeDocument.IsOutput ? Color.Aqua : Color.LightCoral;
        }

        private PointF _position;
        private SizeF _defaultPortSize = new SizeF(7f, 7f);


        public IPort GetPort()
        {
            return _goPortWrapper;
        }

        internal void AdjustFont()
        {
            Label.Editable = true;
            Label.FontSize = float.Parse(ConfigurationManager.AppSettings["NodeFontSize"]);
            Label.Alignment = TopCenter;
        }

        float INodeView.X
        {
            get
            {
                return Icon.Location.X;
            }
            set
            {
                var point = Icon.Location;
                point.X = value;

                Icon.Location = point;
            }
        }

        float INodeView.Y
        {
            get
            {
                return Icon.Location.Y;
            }
            set
            {
                var point = Icon.Location;
                point.Y = value;

                Icon.Location = point;
            }
        }

        IPort INodeView.GetPort()
        {
            throw new NotImplementedException();
        }

        string INodeView.Name
        {
            get { return Text; }
            set { Text = value; }
        }

        DTO_Node INodeView.NodeDocument
        {
            get
            {
                return _nodeDocument;
            }
            set
            {
                _nodeDocument = value;
                ChangeNodeType(value.Type);
            }
        }

        private void ChangeNodeType(DTO_Node_type value)
        {
            if (_nodeDocument != null)
                _nodeDocument.Type = value;

            _nodeType = value;
            RefreshColor();
        }

        private DTO_Node_type GetNodeType()
        {
            if (_nodeDocument != null)
                return _nodeDocument.Type;

            return _nodeType;
        }

        DTO_Node_type INodeView.NodeType
        {
            get
            {
                return GetNodeType();
            }
            set
            {
                _nodeType = value;
                ChangeNodeType(value);
            }
        }

        event Func<DTO_Node, bool> INodeView.OnChoosingFirstPoint
        {
            add { _chooseFirstPoint += value; }
            remove { _chooseFirstPoint -= value; }
        }

        event Action<DTO_Node> INodeView.ChooseSecondPoint
        {
            add { _chooseSecondPoint += value; }
            remove { _chooseSecondPoint -= value; }
        }

        event Action<DTO_Node> INodeView.RemovePoint
        {
            add { _removePoint += value; }
            remove { _removePoint -= value; }
        }

        void INodeView.SetLabelFontSize(float size)
        {
            Label.FontSize = size;
        }

        internal void SetNodeType(DTO_Node_type nodeType)
        {
            ChangeNodeType(nodeType);
        }


        float INodeView.LabelX
        {
            get
            {
                return Label.Location.X;
            }
            set
            {
                var point = Label.Location;
                point.X = value;

                Label.Location = point;
            }
        }

        float INodeView.LabelY
        {
            get
            {
                return Label.Location.Y;
            }
            set
            {
                var point = Label.Location;
                point.Y = value;

                Label.Location = point;
            }
        }


        event Action<DTO_Node> INodeView.OnChosenFirstPoint
        {
            add { _chosenFirstPoint+=value; }
            remove { _chosenFirstPoint -= value; }
        }
    }
}
