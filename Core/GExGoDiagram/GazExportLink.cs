﻿using System.Configuration;
using System.Drawing;
using GEx.Iface;
using Northwoods.Go;

namespace GExGoDiagram
{
    public class GazExportLink : GoLink, IConnectionView
    {
        private Pen _defaultPen;
        private Pen _selectedPen;
        public GazExportLink()
        {
            //var width = //20f /2;
            var width = float.Parse(ConfigurationManager.AppSettings["LinkWidth"]);
            
            Brush =
                new SolidBrush(Color.DarkBlue)
                {
                    
                };
            _defaultPen = new Pen(Color.DarkBlue, width)
            {
                EndCap = System.Drawing.Drawing2D.LineCap.AnchorMask
            };
            _selectedPen = _defaultPen.Clone() as Pen;
            _selectedPen.Color = Color.DarkRed;

            Pen = _defaultPen;
            ToArrow = true;
            
            var coeff = 3;
            this.ToArrowLength = this.ToArrowLength * coeff;
            this.ToArrowWidth = this.ToArrowWidth * coeff;
            this.ToArrowShaftLength = this.ToArrowShaftLength * coeff;
            Selectable = false;
            Editable = false;
        }

        public IPort PortFrom
        {
            get { return this.FromPort as GazExportPort; }
            set { FromPort = (value as GazExportPort).GetGoPort(); }
        }

        public IPort PortTo
        {
            get { return this.ToPort as GazExportPort; }
            set { ToPort = (value as GazExportPort).GetGoPort(); }
        }

        public INodeView NodeFrom
        {
            get { return this.FromNode as GazExportGoNode; }
            set { FromPort = (value as GazExportGoNode).Port; }
        }

        public INodeView NodeTo
        {
            get { return this.ToNode as GazExportGoNode; }
            set { ToPort = (value as GazExportGoNode).Port; }
        }

        public void Select()
        {
            Pen = _selectedPen;
        }

        public void DeSelect()
        {
            Pen = _defaultPen;
        }
    }
}
