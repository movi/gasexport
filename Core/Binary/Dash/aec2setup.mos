(!*******************************************************
  * Mosel Example programs                              *
  * ======================                              *
  *                                                     *
  * file aec2setup.mos                                  *
  * ``````````````````                                  *
  *  Configuration program for AEC2 package:            *
  *  + generate the aid,ask and acf files               *
  *  + create SSh secret keys                           *
  *  + create/update security group                     *
  *  + create AMI with Xpress installed                 *
  *                                                     *
  * (c) 2012 Fair Isaac Corporation                     *
  *     author: Y. Colombani                            *
  *******************************************************!)

model aec2setup
uses 'aec2'
version 0.0.2

declarations
 is_windows=getsysinfo(SYS_NAME)="Windows"
 cfg:text
 images:list of EC2image
 SSH_CMD,SCP_CMD:text
 platform:string
end-declarations

forward procedure check_env
forward procedure read_aid
forward procedure read_endpoint
forward procedure read_ask
forward procedure read_secgrp
forward procedure create_image
forward procedure create_pass
forward procedure prepare_xp
forward function secgrp_is_ok(sgrp:string, port:integer):boolean
forward function extract_archive(pkg:text,lic:text,xmod:text):boolean
forward procedure create_intscr
forward procedure ask(qn:string,resp:text,qm:boolean)
forward procedure ask(qn:string,resp:text)
forward procedure askqm(qn:string,resp:text)
forward procedure select_file(what:string,qn:string,delft:string,resp:text,filter:string)

aec2_verbose:=true

if is_windows then
 SSH_CMD:="mplink -batch -ssh"
 SCP_CMD:="mpscp -batch"
else
 SSH_CMD:="ssh -o StrictHostKeyChecking=no"
 SCP_CMD:="scp -o StrictHostKeyChecking=no"
end-if

writeln("*** AEC2 Mosel Package configuration tool ***")
writeln

check_env

select_file("Configuration files","Configuration file name to create/update","aec2.acf",cfg,"*.acf")
if loadAEC2Config(string(cfg)) then
 writeln("Loading of '",cfg,"' succeeded!")
else
 writeln("Loading of '",cfg,"' failed")
 writeln("Let's generate a new configuration file.")
end-if

writeln("\n- Security Credentials (1)")
if aec2_aidfile="" or not isValidPrivateKey then
 read_aid
end-if
writeln(">> Using '",aec2_aidfile,"' for the access key.")

writeln("\n- End Point selection")
read_endpoint

writeln("\n- Security Credentials (2)")
if aec2_askfile="" then
 read_ask
end-if
writeln(">> Using '",aec2_askfile,"' for the SSH key pair.")

writeln("\n- Connection settings")
writeln("AEC2 Instances support direct connection to an xprmsrv server")
writeln("or secure connections through an ssh tunnel.")
ans:=text(aec2_xsrvport)
askqm("Which port should the xprmsrv server use (0 to disable xprmsrv)",ans)
aec2_xsrvport:=parseint(ans,1)
if aec2_xsrvport<=0 then
 aec2_xsrvport:=0
 aec2_connmode:="ssh"
 writeln(">> 'xprmsrv' disabled; connection through ssh only")
else
 ans:=text(aec2_connmode)
 askqm("What should be the default connection mode (ssh or xprmsrv)",ans)
 aec2_connmode:=string(ans)
 writeln(">> 'xprmsrv' listening on port ",aec2_xsrvport,"; default mode is ",aec2_connmode)
end-if

writeln("\n- Selection/creation of a security group")
read_secgrp
writeln(">> Using '",aec2_secgrp,"' for the security group.")

writeln("\n- Selection/creation of an AMI")
if aec2_image="" then
 images:=[]
else
 images:=getImages(aec2_image,"","")
end-if
if images.size=0 then
 ans:=text("yes")
 askqm("No Image is defined. Would you like to create a new one with xpress installed",ans)
 if ans="yes" then
  create_image
 end-if
end-if
writeln(">> Using AMI '",aec2_image,"'.")

writeln("\n- Instance type")
writeln("Instances can be of one of these types:")
writeln("t1.micro, m1.small, m1.large, m1.xlarge, c1.medium, c1.xlarge")
writeln("m2.xlarge, m2.2xlarge, m2.4xlarge, cc1.4xlarge, cg1.4xlarge")
ans:=aec2_type
askqm("What type should be used for instances started by AEC2",ans)
aec2_type:=string(ans)
writeln(">> Instances started by AEC2 will be of type ",aec2_type)


writeln("\n")
ans:=text("yes")
askqm("Would you like to save/update this configuration in '"+cfg+"'",ans)
if ans="yes" then
 saveAEC2Config(string(cfg))
 writeln(">> Configuration saved to '",cfg,"'")
end-if

! Check the required programs are available
!------------------------------------------
procedure check_env
 declarations
  failed:set of string
  l:text
 end-declarations

 savdir:=getparam("workdir")
 setparam("workdir",getparam("tmpdir"))
 fopen("dum",F_OUTPUT)
 writeln("bla")
 fclose(F_OUTPUT)
 fopen("tmp:rep",F_OUTPUT)
 if is_windows then
  writeln ! necessary (not sure why)
  system("mplink -V")
  system("mpscp -V")
  system("pcmdgen -V")
  fclose(F_OUTPUT)
  fopen("tmp:rep",F_INPUT)
  failed+={"mplink","mpscp","pcmdgen"}
  while(readtextline(l)>0)
   if findtext(l,"plink:",1)=1 then
    failed-={"mplink"}
   elif findtext(l,"pscp:",1)=1 then
    failed-={"mpscp"}
   elif findtext(l,"PuTTYgen",1)<>0 then
    failed-={"pcmdgen"}
   end-if
  fclose(F_INPUT)
 else
  system("/bin/sh -c 'ssh -V 2>&1'")
  if getsysstat<>0 then failed+={"ssh"}; end-if
  system("scp dum dum2")
  if getsysstat<>0 then failed+={"scp"}; end-if
  fdelete("dum2")
  fclose(F_OUTPUT)
 end-if
 fdelete("dum")
 fdelete("tmp:rep")
 setparam("workdir",savdir)
 if failed.size>0 then
  writeln("This model requires external programs for its processing.")
  writeln("The following program(s) seem to be missing:")
  forall(p in failed)
   write("\t",p)
  writeln("\nAborting")
  exit(1)
 end-if
end-procedure

! Get and save Access Credentials
!--------------------------------
procedure read_aid
 declarations
  aid,key,kf:text
 end-declarations

 writeln("A Secret Access Key is required to send queries")
 select_file("Access Key files","Access Key file name","aec2.aid",kf,"*.aid")
 loadPrivateKey(string(kf))
 if aec2_aidfile<>kf or not isValidPrivateKey then
  writeln("Loading of this Access key file failed.")
  writeln("A key can be created from the AWS web site:")
  writeln("- go to 'Your Account' and select 'Security Credentials'")
  writeln("- scroll down to 'Access Credentials' and select 'Access Keys'")
  writeln("- then locate an active Access Key in the 'Your Access Keys' list")
  writeln("- if the list is empty, click 'Create a New Access Key'")
  repeat
   repeat
    write("What is your Access Key ID? ");fflush
    i:=readtextline(aid)
    trim(aid)
   until aid.size>0
   repeat
    write("What is your Secret Access Key? ");fflush
    i:=readtextline(key)
    trim(key)
   until key.size>0
   setPrivateKey(aid,key)
  until isValidPrivateKey
   
  ask("Access key file name to save this key",kf)
  savePrivateKey(string(kf))
 end-if
end-procedure

! Select the End point to use
!----------------------------
procedure read_endpoint
 declarations
  lsep:list of text
 end-declarations

 currep:=getEndPoint
 setEndPoint("ec2.amazonaws.com")
 lsep:=getEndPoints("")
 forall(l in lsep)
  if l=currep then
   setEndPoint(currep)
   break
  end-if

 writeln("Queries are sent to an 'End Point'")
 writeln("The available End Points are:")
 forall(l in lsep)
  writeln("\t",l)
 repeat
  currep:=getEndPoint
  askqm("Which End Point would you like to use",currep)
  forall(l in lsep)
   if l=currep then
    setEndPoint(currep)
    break
   end-if
 until currep=getEndPoint
end-procedure

! Read/generate/save SSH key pair
!--------------------------------
procedure read_ask
 declarations
  kpf,ans:text
 end-declarations

 writeln("A key pair is required to login into instances")
 select_file("Key Pair files","Key Pair file name","aec2.ask",kpf,"*.ask")
 if not selectSSHKeyFile(string(kpf)) then
  writeln("Loading of this Key Pair file failed.")
  ans:=text("yes")
  askqm("Would you like to create a new Key Pair",ans)
  if ans<>"yes" then
   writeln("Aborting.")
   exit(1)
  end-if
  ans:=text("xpress")
  ask("Key Pair name",ans)
  ask("Key Pair file name",kpf)
  aec2_verbose:=false
  deleteKeyPair(string(ans))
  aec2_verbose:=true
  if createSSHKeyFile(string(kpf),string(ans)) then
   writeln("Key creation succeeded!")
   if not selectSSHKeyFile(string(kpf)) then
    writeln("Loading of newly created keyfile failed - aborting")
    exit(1)
   end-if
  else
   writeln("Key creation failed. Aborting.")
   exit(1)
  end-if
 end-if
end-procedure

! Select or generate a security group
!------------------------------------
procedure read_secgrp
 declarations
  ans:text
 end-declarations

 if not secgrp_is_ok(aec2_secgrp,22) or
    (aec2_xsrvport>0 and not secgrp_is_ok(aec2_secgrp,aec2_xsrvport)) then
  writeln("Instances are run under a 'security group' that defines an access policy.")
  writeln("AEC2 requires the security group to allow connections to the SSH port")
  if aec2_xsrvport>0 then
   writeln("and to port ",aec2_xsrvport," (for xprmsrv).")
  end-if
  writeln("The current group (",aec2_secgrp,") does not seem to authorise this type of access.")
  ans:=text("no")
  askqm("Would you like to keep this setting anyway",ans)
  if ans="no" then
   asg:=getAllSecurityGroups
   writeln("The following security groups are currently available:")
   forall(s in asg) writeln("\t",s)
   ans:=getfirst(asg)
   ask("Select the group to use or give the identifier for a new group",ans)
   aec2_secgrp:=string(ans)
   if or(s in asg) s=ans then
    if not secgrp_is_ok(aec2_secgrp,22) then
     writeln("This group does not seem to authorise SSH access.")
     ans:=text("yes")
     askqm("Would you like to add the necessary rule",ans)
     need_update:=ans="yes"
    end-if
    if aec2_xsrvport>0 and not secgrp_is_ok(aec2_secgrp,aec2_xsrvport) then
     writeln("This group does not seem to authorise access to port ",aec2_xsrvport)
     ans:=text("yes")
     askqm("Would you like to add the necessary rule",ans)
     need_update_x:=ans="yes"
    end-if
   else
    createSecurityGroup(aec2_secgrp,"Xpress for AEC2")
    if aec2_status div 100 <> 2 then
     writeln("Failed to create security policy - aborting")
     exit(1)
    else
     need_update:=true
     need_update_x:=aec2_xsrvport>0
    end-if
   end-if
   if need_update then
    addSecurityIngress(aec2_secgrp,"tcp",22..22,["0.0.0.0/0"])
    if aec2_status div 100 <> 2 then
     writeln("Failed to update security policy - aborting")
     exit(1)
    end-if
   end-if
   if need_update_x then
    addSecurityIngress(aec2_secgrp,"tcp",aec2_xsrvport..aec2_xsrvport,["0.0.0.0/0"])
    if aec2_status div 100 <> 2 then
     writeln("Failed to update security policy - aborting")
     exit(1)
    end-if
   end-if
  end-if
 end-if
end-procedure

! Select or generate an AMI
!--------------------------
procedure create_image
 declarations
  ans:text
 end-declarations

 ans:=""
 images:=getImages("","","Xpress for AEC2")
 if images.size>0 then
  writeln("\nThe following AMIs are available:")
  forall(i in images)
   writeln(i.id,"(",i.name,") - '",i.description,"'")
  ask("Type the Id of one of these images or hit enter to create a new one",ans)
 end-if
 if ans="" then
  prepare_xp
  create_intscr
  images:=getImages("","","Amazon Linux AMI "+platform+" EBS")
  if images.size<1 then
   writeln("Internal error - cannot find 'Amazon Linux AMI ",platform," EBS'. Aborting")
   exit(1)
  end-if
  aec2_type:="t1.micro"
  image:=getfirst(images)
  writeln("Ready to start an instance from image ",image.id," (",image.description,")")
  ans:="yes"
  ask("Please confirm image creation",ans)
  if ans<>"yes" then
   writeln("Aborting.")
   exit(1)
  end-if
  writeln("Starting instance.")
  aec2_image:=image.id
  inst:=runInstance
  write("Waiting for instance to be ready."); fflush
  if not waitInstanceReady(inst,300,true) then
   writeln("\nInstance failed to start - aborting")
   terminateInstance(inst) ! in case connection failing on a running instance
   exit(1)
  end-if
  writeln("\nCopying files...")
  tocopy:=text(" ")+expandpath(gettmpdir+"/xpressmp.tgz")+" "+
                 expandpath(gettmpdir+"/doinstall")+" "+
                 expandpath(gettmpdir+"/doinstall.expect")
  system(SCP_CMD+" -i "+getKeyFile+tocopy+" ec2-user@"+inst.ipaddr+":/tmp")
  writeln("Executing installation script...")
  system(SSH_CMD+" -i "+getKeyFile+" ec2-user@"+inst.ipaddr+" expect /tmp/doinstall.expect")
  writeln("Testing installation:")
  system(SSH_CMD+" -i "+getKeyFile+" ec2-user@"+inst.ipaddr+" mosel -s -c info")
  if getsysstat<>0 then
   writeln("Installation failed (license problem?)")
   writeln("Shutting down instance and aborting")
   terminateInstance(inst)
   exit(1)
  else
   writeln("Installation successful.")
   ans:=text("xpress-")+text(currentdate)+text("-")+text(currenttime div 1000)
   ask("Name of the AMI to be created",ans)
   writeln("Creating image '",ans,"'...")
   aec2_image:=string(createImage(inst,string(ans),"Xpress for AEC2 - "+platform+" EBS"))
   if aec2_image="" then
    writeln("Creation of the AMI failed - aborting")
    terminateInstance(inst)
    exit(1)
   else
    writeln("Creation successful.")
    writeln("The new image ID is ",aec2_image)
    write("Waiting for operation to complete.");fflush
    repeat
     sleep(10000)
     write(".");fflush
     images:=getImages(aec2_image,"","")
     if images.size<1 then
      next
     else
      image:=getfirst(images)
     end-if
    until image.snapshot<>"" and image.available
    writeln("\nNew AMI is now available.")
    terminateInstance(inst)
   end-if
  end-if
 else
  aec2_image:=string(ans)
 end-if
end-procedure

! Return a random number between 0 and m-1
!-----------------------------------------
function random(m:integer):integer
  returned:=integer(round((m*random)+0.5))-1
end-function

! Generate a random password
!---------------------------
procedure create_pass
 declarations
  pass:text
 end-declarations
 forall(i in 1..16)
  case random(3) of
   0:setchar(pass,i,random(10)+48)  ! digit
   1:setchar(pass,i,random(26)+65)  ! upper case letter
   else
     setchar(pass,i,random(26)+97)  ! lower case letter
  end-case
 aec2_xsrvpass:=string(pass)
end-procedure

! Prepare the Xpress package for the AEC2 instance
!-------------------------------------------------
procedure prepare_xp
 declarations
  pkg,lic:text
 end-declarations

 writeln("\nTo install Xpress on an Amazon instance an Xpress package")
 writeln("for Linux is required. The corresponding file name should")
 writeln("be of the form 'xp*_linuxrh*_setup.tar'")
 writeln("In addition to the software, a valid license file is also")
 writeln("necessary. A license file is usually named 'xpauth.xpr'")
 select_file("Xpress package","package file name","",pkg,"xp*_linux*_setup.tar")
 if pkg="" or bittest(getfstat(pkg),SYS_TYP)<>SYS_REG then
  writeln("Package not found - aborting.")
  exit(1)
 else
  platform:=if(findtext(pkg,"_linux_x86_64",1)>0,"x86_64","i386")
  select_file("License","license file name","",lic,"*.xpr")
  if lic="" or bittest(getfstat(string(lic)),SYS_TYP)<>SYS_REG then
   writeln("License not found - aborting.")
   exit(1)
  else
   writeln("Preparing archive...")
   create_pass
   if not extract_archive(pkg,lic,"") then
    writeln("Failed to prepare the archive - aborting")
    exit(1)
   else
    writeln("Done.")
   end-if
  end-if 
 end-if 
end-procedure

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

! Check whether the provided security group allow a given port
!-------------------------------------------------------------
function secgrp_is_ok(sgrp:string, port:integer):boolean
 rules:=describeSecurityGroup(sgrp)
 forall(r in rules)
  if (port in r.ports) and (or(s in r.sources) s="0.0.0.0/0") then
   returned:=true
  end-if
end-function

! Extract and repackage the Xpress standard distribution
!-------------------------------------------------------
function extract_archive(pkg:text,lic:text,xmod:text):boolean
 declarations
  lsf:list of text
 end-declarations
 tarlist(0,pkg,lsf,"xp*_linux*.tar.gz")
 returned:=(getsysstat=0 and lsf.size=1)
 if returned then
  tararc:=string(getfirst(lsf))
  makedir(gettmpdir+"/xpressmp")
  untar(0,pkg,gettmpdir,tararc)
  untar(0,"zlib.gzip:tmp:"+tararc,gettmpdir+"/xpressmp",
      "version.txt;bin/mosel;bin/xprmsrv;"+ !"bin/xpserver;"+
      "lib/libKalis.so|libxprm_rt.so*|libxprm_mc.so*|libxprl*|libxprs.so*|libxslp.so*;dso/*")
  fdelete("tmp:"+tararc)
  if xmod<>"" then
   if pathmatch(xmod,"*.tgz") or pathmatch(xmod,"*.tar.gz") then
    untar(0,"zlib.gzip:"+xmod,gettmpdir+"/xpressmp/dso","*.dso")
   else
    untar(0,xmod,gettmpdir+"/xpressmp/dso","*.dso")
   end-if
  end-if
  fcopy(lic,"tmp:xpressmp/xpauth.xpr")

  fopen("tmp:/xpressmp/bin/xpenv",F_OUTPUT+F_BINARY)
  writeln("export XPRESSDIR=/opt/xpressmp")
  writeln("export XPRESS=${XPRESSDIR}")
  writeln("export LD_LIBRARY_PATH=${XPRESSDIR}/lib:${LD_LIBRARY_PATH}")
  writeln("export PATH=${XPRESSDIR}/bin:${PATH}")
  writeln("export MOSEL_RESTR=NoExecWDOnly")
  fclose(F_OUTPUT)

  fopen("tmp:xpressmp/xprmsrv.cfg",F_OUTPUT+F_BINARY)
  writeln("MOSEL_RESTR=NoExecWDOnly")
  writeln("[xpress]")
  writeln(" XPRESSDIR=/opt/xpressmp")
  writeln(" XPRESS=${XPRESSDIR}")
  writeln(" MOSEL_CWD=/home/ec2-user")
  writeln(" PASS=",aec2_xsrvpass)
  fclose(F_OUTPUT)

  fopen("tmp:xpressmp/bin/xpressinit",F_OUTPUT+F_BINARY)
  writeln("#!/bin/bash")
  writeln("#")
  writeln("# Init file for xpress AMI")
  writeln("#")
  writeln("# chkconfig: 2345 25 25")
  writeln("# description:  Setup and start Xpress server (xprmsrv)")
  writeln("#")
  writeln
  writeln("# source function library")
  writeln(". /etc/rc.d/init.d/functions")
  writeln
  writeln("RETVAL=0")
  writeln("start() {")
  writeln("        echo \"Starting Xpress services:\"")
  writeln("	UDATA=`GET http://169.254.169.254/latest/user-data`")
  writeln("	XPORT=`echo $UDATA | sed -n -e 's/XPORT=\\([1-9][0-9]\\+\\).*/\\1/p'`")
  writeln("	if [ \"$XPORT\" != \"\" ]; then")
  writeln("	 echo -n $\"xprmsrv on port ${XPORT}:\"")
  writeln("	 /opt/xpressmp/bin/xprmsrv -d -u ec2-user -g ec2-user -p $XPORT /opt/xpressmp/xprmsrv.cfg && success || failure")
  writeln("	 echo")
  writeln("	fi")
  writeln("}")
  writeln("stop () {")
  writeln('        echo -n $"Stopping Xpress services: "')
  writeln("	killall -q xprmsrv")
  writeln("	success")
  writeln("        echo")
  writeln("}")
  writeln
  writeln('case "$1" in')
  writeln("        start)")
  writeln("                start")
  writeln("                ;;")
  writeln("        stop)")
  writeln("                stop")
  writeln("                ;;")
  writeln("        restart)")
  writeln("                stop")
  writeln("                start")
  writeln("                ;;")
  writeln("        *)")
  writeln("                echo $\"Usage: $0 {start|stop|restart}\"")
  writeln("                RETVAL=1")
  writeln("esac")
  writeln("exit $RETVAL")
  fclose(F_OUTPUT)
  lsf:=[]
  findfiles(SYS_RECURS,lsf,gettmpdir,"xpressmp/*")
  newtar(0,"zlib.gzip:tmp:xpressmp.tgz",gettmpdir,lsf)
  removefiles(SYS_RECURS,gettmpdir,"xpressmp/*")
  removedir(gettmpdir+"/xpressmp")
 ! else returned:=false
 end-if
end-function

procedure create_intscr
 fopen("tmp:doinstall.expect",F_OUTPUT+F_BINARY)
 writeln("# 'sudo' command cannot be called from a script because")
 writeln("# it requires a tty: to avoid the problem we use 'expect'...")
 writeln("spawn -noecho sudo sh /tmp/doinstall")
 writeln("interact")
 writeln("wait")
 fclose(F_OUTPUT)

 fopen("tmp:doinstall",F_OUTPUT+F_BINARY)

 writeln("#!/bin/bash")
 writeln("umask 022")
 writeln("cd /opt")
 writeln("tar xzf /tmp/xpressmp.tgz")
 writeln("chown -R root:root /opt/xpressmp")
 writeln("chmod -R 0755 /opt/xpressmp")
 writeln("/sbin/ldconfig -n /opt/xpressmp/lib")
 writeln("cp /opt/xpressmp/bin/xpressinit /etc/rc.d/init.d")
 writeln("chmod +x /etc/rc.d/init.d/xpressinit")
 writeln("chkconfig --add xpressinit")
 writeln("echo '. /opt/xpressmp/bin/xpenv' >>/home/ec2-user/.bashrc")
 writeln("rm -f /tmp/xpressmp.tgz /tmp/doinstall /tmp/doinstall.expect")

 fclose(F_OUTPUT)

end-procedure

! Read answer to a question with a default value
!-----------------------------------------------
procedure ask(qn:string,resp:text,qm:boolean)
 declarations
  l:text
 end-declarations

 write(qn,"[",resp,if(qm,"]? ","]: "))
 fflush
 if readtextline(l)>0 then
  trim(l)
  if l.size>0 then
   resp:=l
  end-if
 end-if
end-procedure

procedure ask(qn:string,resp:text)
 ask(qn,resp,false)
end-procedure

procedure askqm(qn:string,resp:text)
 ask(qn,resp,true)
end-procedure

! Display a list of files and ask for a selection
!------------------------------------------------
procedure select_file(what:string,qn:string,deflt:string,resp:text,filter:string)
 declarations
  flst:list of text
 end-declarations
 findfiles(flst,filter)
 if flst.size>1 then
  writeln("Available ",what,":")
  forall(i in flst)
   write("\t",i)
  writeln
 end-if
 resp:=if(flst.size>0,getfirst(flst),text(deflt))
 ask(qn,resp)
end-procedure

end-model
