@echo off

set RVAL=0

set INSTALLDIR=%~dp0
set XPSERVER=%INSTALLDIR%xpserver.exe

if %1_ == start_ goto LMGRD_START
if %1_ == stop_ goto LMGRD_STOP
if %1_ == starts_ goto LMGRD_STARTS
if %1_ == stops_ goto LMGRD_STOPS
if %1_ == reread_ goto LMGRD_REREAD
if %1_ == install_ goto LMGRD_INSTALL
if %1_ == uninst_ goto LMGRD_UNINST

echo Usage: %0 start - start the standalone license manager
echo        %0 stop - stop the standalone license manager
echo        %0 install - install the license manager as a service (NT or 2000)
echo        %0 uninst - uninstall the license manager service (NT or 2000)
echo        %0 starts - start the license manager service
echo        %0 stops - stop the license manager service
goto END

:LMGRD_START
set RET=START_LABEL
set FILE=xpauth.xpr
goto FIND_FILE
:START_LABEL
echo Running %XPSERVER% -xpress "%RESULT%" -logfile "%TEMP%\xprl_server.log" -bg
%XPSERVER% -xpress "%RESULT%" -logfile "%TEMP%\xprl_server.log" -bg
set RVAL=%ERRORLEVEL%
if %RVAL == 0 goto LMGRD_STARTED
goto END

:LMGRD_STARTED
echo License manager started
goto END

:LMGRD_STOP
echo Running %XPSERVER% -stop -logfile "%TEMP%\xprl_server.log"
%XPSERVER% -stop -logfile "%TEMP%\xprl_server.log"
set RVAL=%ERRORLEVEL%
rem -q 
goto END

:LMGRD_REREAD
echo reread not implemented - sorry.
set RVAL=255
goto END

:LMGRD_INSTALL
echo Running %XPSERVER% -service install
%XPSERVER% -service install
set RVAL=%ERRORLEVEL%
goto END

:LMGRD_UNINST
echo Running %XPSERVER% -service remove
%XPSERVER% -service remove
set RVAL=%ERRORLEVEL%
goto END

:LMGRD_STARTS
echo %XPSERVER% -service start
%XPSERVER% -service start
set RVAL=%ERRORLEVEL%
goto END

:LMGRD_STOPS
echo %XPSERVER% -service stop
%XPSERVER% -service stop
set RVAL=%ERRORLEVEL%
goto END

rem Subroutine to find a file
:FIND_FILE
if %RET%_ == _ goto ERR_NORET
if %FILE%_ == _ goto ERR_NOFILE

if not exist "%XPRESSDIR%\bin\%FILE%" goto TRY_INSTALLDIR
set RESULT=%XPRESSDIR%\bin
echo Using %XPRESSDIR%\bin\%FILE%
goto %RET%

:TRY_INSTALLDIR
if not exist "%INSTALLDIR%%FILE%" goto TRY_DEFAULT
set RESULT=%INSTALLDIR%.
echo Using %INSTALLDIR%%FILE%
goto %RET%

:TRY_DEFAULT
if not exist "c:\xpressmp\bin\%FILE%" goto ERR_NOTFOUND
set RESULT=c:\xpressmp\bin
echo Using c:\xpressmp\bin\%FILE%
goto %RET%


rem Error messages

:ERR_NOTFOUND
if %FILE%_ == xpauth.xpr_ goto ERR_NOLIC
if "%XPRESSDIR%" == "" goto NOTFOUND_NODIR

echo Cannot find the file %FILE%. Please ensure that it is in the
echo %XPRESSDIR%\bin directory.
set RVAL=128
goto END

:NOTFOUND_NODIR
echo Cannot find the file %FILE%. Please set the XPRESSDIR variable to point to
echo your Xpress-MP installation directory.
set RVAL=128
goto END

:ERR_NOLIC
if "%XPRESSDIR%" == "" goto NOLIC_NODIR

echo Cannot find the license file, xpauth.xpr. Please obtain one from Dash and put
echo it into the %XPRESSDIR%\bin directory.
set RVAL=128
goto END

:NOLIC_NODIR
echo Cannot find the license file, xpauth.xpr. Please set the XPRESSDIR variable
echo to point to your Xpress-MP installation directory.
set RVAL=128
goto END

:ERR_NORET
echo Internal error - no return path specified
set RVAL=128
goto END

:ERR_NOFILE
echo Internal error - no file specified
set RVAL=128
goto END

:END
pause
exit /B %RVAL%
