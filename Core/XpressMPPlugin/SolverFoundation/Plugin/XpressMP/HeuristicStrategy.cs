﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public enum HeuristicStrategy
    {
        Automatic = -1,
        Basic = 1,
        Enhanced = 2,
        Extensive = 3,
        None = 0
    }
}

