﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    internal enum SolverState
    {
        Start,
        Solving,
        Solved,
        Aborting,
        Aborted,
        Disposing,
        Disposed
    }
}

