﻿namespace SolverFoundation.Plugin.XpressMP
{
    using Microsoft.SolverFoundation.Common;
    using Microsoft.SolverFoundation.Services;
    using Optimizer;
    using System;

    internal class XpressMPGoal : ILinearGoal
    {
        private bool goalInObjective;
        private int goalVID;
        private bool isEnabled;
        private bool isMinimize;
        private SolverFoundation.Plugin.XpressMP.Bounds originalGoalRowBounds;
        private int priority;
        private XpressMPSolver solver;

        internal XpressMPGoal(XpressMPSolver solver, int goalVID, bool isMinimize, int priority)
        {
            this.solver = solver;
            this.isEnabled = true;
            this.goalVID = goalVID;
            this.isMinimize = isMinimize;
            this.priority = priority;
        }

        internal void MoveGoalFromXPRSObjective()
        {
            try
            {
                if (!this.goalInObjective)
                {
                    throw new MsfException("Goal is not in the objective!");
                }
                if ((this.solver.State == SolverState.Solving) || (this.solver.State == SolverState.Aborting))
                {
                    throw new MsfException("Cannot move objective back into matrix while solve in progress");
                }
                XPRSprob xpressProb = this.solver.XpressProb;
                int rowIndex = this.solver.VIDs.GetRowIndex(this.Index);
                int qElems = xpressProb.QElems;
                if (qElems > 0)
                {
                    int[] numArray = new int[xpressProb.Cols + 1];
                    int[] numArray2 = new int[qElems];
                    double[] numArray3 = new double[qElems];
                    xpressProb.GetMQObj(numArray, numArray2, numArray3, qElems, 0, xpressProb.Cols - 1);
                    for (int i = 0; i < xpressProb.Cols; i++)
                    {
                        for (int j = numArray[i]; j < numArray[i + 1]; j++)
                        {
                            xpressProb.ChgQRowCoeff(rowIndex, i, numArray2[j], numArray3[j]);
                        }
                    }
                }
                this.solver.SetBounds(this.Index, this.originalGoalRowBounds.lower, this.originalGoalRowBounds.upper);
                XpressMPSolver.RemoveXPRSprobObjective(xpressProb);
                this.goalInObjective = false;
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
        }

        internal void MoveGoalToXPRSObjective()
        {
            try
            {
                Rational indeterminate;
                if (this.goalInObjective)
                {
                    throw new MsfException("Goal is already in the objective!");
                }
                XPRSprob xpressProb = this.solver.XpressProb;
                int rowIndex = this.solver.VIDs.GetRowIndex(this.Index);
                XpressMPSolver.RemoveXPRSprobObjective(xpressProb);
                this.originalGoalRowBounds = new SolverFoundation.Plugin.XpressMP.Bounds(this.solver, this.Index);
                int maxcoeffs = xpressProb.GetRows(null, null, null, 0, rowIndex, rowIndex);
                int[] numArray = new int[maxcoeffs + 1];
                double[] numArray2 = new double[maxcoeffs + 1];
                int[] numArray3 = new int[2];
                xpressProb.GetRows(numArray3, numArray, numArray2, maxcoeffs, rowIndex, rowIndex);
                numArray[maxcoeffs] = -1;
                if (!this.solver.UserSolution.TryGetValue(this.Index, out indeterminate))
                {
                    indeterminate = Rational.Indeterminate;
                }
                if (indeterminate == Rational.Indeterminate)
                {
                    numArray2[maxcoeffs] = 0.0;
                }
                else
                {
                    numArray2[maxcoeffs] = XpressMPSolver.XpressMPDouble(indeterminate);
                }
                xpressProb.ChgObj(maxcoeffs + 1, numArray, numArray2);
                int num3 = xpressProb.GetQRowQMatrixTriplets(rowIndex, null, null, null);
                if (num3 > 0)
                {
                    int[] numArray4 = new int[num3];
                    int[] numArray5 = new int[num3];
                    double[] dqe = new double[num3];
                    xpressProb.GetQRowQMatrixTriplets(rowIndex, numArray4, numArray5, dqe);
                    for (int i = 0; i < num3; i++)
                    {
                        xpressProb.ChgQObj(numArray4[i], numArray5[i], dqe[i]);
                    }
                    xpressProb.DelQMatrix(rowIndex);
                }
                xpressProb.ChgRowType(1, new int[] { rowIndex }, new char[] { 'N' });
                this.goalInObjective = true;
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
        }

        public bool Enabled
        {
            get
            {
                return this.isEnabled;
            }
            set
            {
                this.isEnabled = value;
            }
        }

        public int Index
        {
            get
            {
                return this.goalVID;
            }
        }

        public object Key
        {
            get
            {
                return this.solver.GetKeyFromIndex(this.goalVID);
            }
        }       

        public bool Minimize
        {
            get
            {
                return this.isMinimize;
            }
            set
            {
                this.isMinimize = value;
            }
        }

        public int Priority
        {
            get
            {
                return this.priority;
            }
            set
            {
                this.priority = value;
            }
        }
    }
}

