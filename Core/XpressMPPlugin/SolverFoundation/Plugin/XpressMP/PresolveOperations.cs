﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    [Flags]
    public enum PresolveOperations
    {
        DualReductions = 8,
        DuplicateColumnRemoval = 0x20,
        DuplicateRowRemoval = 0x40,
        ForcingRowRemoval = 4,
        LinearlyDependantRowRemoval = 0x1000,
        NoAdvancedIPReductions = 0x800,
        NoIntegerVariableAndSOSDetection = 0x2000,
        NoIPReductions = 0x200,
        NoSemiContinuousReductions = 0x400,
        RedundantRowRemoval = 0x10,
        SingletonColumnRemoval = 1,
        SingletonRowRemoval = 2,
        StrongDualReductions = 0x80,
        VariableEliminations = 0x100
    }
}

