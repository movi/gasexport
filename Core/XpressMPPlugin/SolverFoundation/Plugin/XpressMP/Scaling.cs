﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("Microsoft.Naming", "CA1714:FlagsEnumsShouldHavePluralNames"), Flags]
    public enum Scaling
    {
        ByMaxElemNotGeoMean = 0x20,
        ColumnScaling = 2,
        CurtisReid = 0x10,
        ExcludeQuadraticFromScaleFactor = 0x80,
        Maximum = 8,
        ObjectiveScaling = 0x40,
        RowScaling = 1,
        RowScalingAgain = 4
    }
}

