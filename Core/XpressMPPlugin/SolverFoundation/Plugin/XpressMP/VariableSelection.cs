﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public enum VariableSelection
    {
        Automatic = -1,
        DownPseudoCost = 5,
        MaximumUpDownPseudoCosts = 4,
        MaximumUpDownPseudoCostsPlusTwiceMinimum = 3,
        MinimumUpDownPseudoCosts = 1,
        SumupDownPseudoCosts = 2,
        UpPseudoCost = 6
    }
}

