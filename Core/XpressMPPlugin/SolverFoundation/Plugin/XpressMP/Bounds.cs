﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    internal struct Bounds
    {
        public double lower;
        public double upper;
        public Bounds(double lb, double ub)
        {
            this.lower = lb;
            this.upper = ub;
        }

        public Bounds(XpressMPSolver solver, int vid)
        {
            solver.GetBounds(vid, out this.lower, out this.upper);
        }
    }
}

