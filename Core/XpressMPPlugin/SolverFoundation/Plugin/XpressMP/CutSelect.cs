﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("Microsoft.Naming", "CA1714:FlagsEnumsShouldHavePluralNames"), Flags]
    public enum CutSelect
    {
        Clique = 0x73f,
        Cover = 0x79f,
        Default = -1,
        DisableCutRows = 0x471f,
        Flowpath = 0xf1f,
        GubCover = 0x871f,
        Implication = 0x171f,
        LiftAndProject = 0x271f,
        Mir = 0x75f
    }
}

