﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    internal struct VID
    {
        public object key;
        public VIDType type;
        public int modelIndex;
    }
}

