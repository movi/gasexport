﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public enum CutStrategy
    {
        Aggressive = 3,
        Conservative = 1,
        Default = -1,
        Moderate = 2,
        None = 0
    }
}

