﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("Microsoft.Naming", "CA1714:FlagsEnumsShouldHavePluralNames"), Flags]
    public enum MIPPresolve
    {
        AllowChangeBounds = 8,
        LogicPreprossessing = 2,
        ReducedCostFixing = 1
    }
}

