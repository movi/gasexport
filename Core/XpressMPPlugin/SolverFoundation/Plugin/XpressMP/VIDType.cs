﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    internal enum VIDType : byte
    {
        COLUMN = 2,
        ROW = 1,
        UNUSED = 0
    }
}

