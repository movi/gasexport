﻿namespace SolverFoundation.Plugin.XpressMP
{
    using Microsoft.SolverFoundation.Services;
    using System;

    public abstract class XpressMPSolveControlDirectives : MixedIntegerProgrammingDirective
    {
        public XpressMPParams xpparams = new XpressMPParams();

        internal XpressMPSolveControlDirectives()
        {
        }

        public bool AutoPerturb
        {
            get
            {
                return this.xpparams.AutoPerturb;
            }
            set
            {
                this.xpparams.AutoPerturb = value;
            }
        }

        public BacktrackAlg BackTrack
        {
            get
            {
                return this.xpparams.BackTrack;
            }
            set
            {
                this.xpparams.BackTrack = value;
            }
        }

        public BacktrackAlg BacktrackTie
        {
            get
            {
                return this.xpparams.BacktrackTie;
            }
            set
            {
                this.xpparams.BacktrackTie = value;
            }
        }

        public int BarCrash
        {
            get
            {
                return this.xpparams.BarCrash;
            }
            set
            {
                this.xpparams.BarCrash = value;
            }
        }

        public double BarDualStop
        {
            get
            {
                return this.xpparams.BarDualStop;
            }
            set
            {
                this.xpparams.BarDualStop = value;
            }
        }

        public double BarGapStop
        {
            get
            {
                return this.xpparams.BarGapStop;
            }
            set
            {
                this.xpparams.BarGapStop = value;
            }
        }

        public int BarIndefLimit
        {
            get
            {
                return this.xpparams.BarIndefLimit;
            }
            set
            {
                this.xpparams.BarIndefLimit = value;
            }
        }

        public int BarIterLimit
        {
            get
            {
                return this.xpparams.BarIterLimit;
            }
            set
            {
                this.xpparams.BarIterLimit = value;
            }
        }

        public BarrierOrdering BarOrder
        {
            get
            {
                return this.xpparams.BarOrder;
            }
            set
            {
                this.xpparams.BarOrder = value;
            }
        }

        public SolverFoundation.Plugin.XpressMP.BarPresolveOps BarPresolveOps
        {
            get
            {
                return this.xpparams.BarPresolveOps;
            }
            set
            {
                this.xpparams.BarPresolveOps = value;
            }
        }

        public double BarPrimalStop
        {
            get
            {
                return this.xpparams.BarPrimalStop;
            }
            set
            {
                this.xpparams.BarPrimalStop = value;
            }
        }

        public double BarStepStop
        {
            get
            {
                return this.xpparams.BarStepStop;
            }
            set
            {
                this.xpparams.BarStepStop = value;
            }
        }

        public int BarThreads
        {
            get
            {
                return this.xpparams.BarThreads;
            }
            set
            {
                this.xpparams.BarThreads = value;
            }
        }

        public double BigM
        {
            get
            {
                return this.xpparams.BigM;
            }
            set
            {
                this.xpparams.BigM = value;
            }
        }

        public SolverFoundation.Plugin.XpressMP.BranchChoice BranchChoice
        {
            get
            {
                return this.xpparams.BranchChoice;
            }
            set
            {
                this.xpparams.BranchChoice = value;
            }
        }

        public AlwaysNeverAutomatic BranchStructural
        {
            get
            {
                return this.xpparams.BranchStructural;
            }
            set
            {
                this.xpparams.BranchStructural = value;
            }
        }

        public int BreadthFirst
        {
            get
            {
                return this.xpparams.BreadthFirst;
            }
            set
            {
                this.xpparams.BreadthFirst = value;
            }
        }

        public int CacheSize
        {
            get
            {
                return this.xpparams.CacheSize;
            }
            set
            {
                this.xpparams.CacheSize = value;
            }
        }

        public CholeskyAlgorithm CholeskyAlg
        {
            get
            {
                return this.xpparams.CholeskyAlg;
            }
            set
            {
                this.xpparams.CholeskyAlg = value;
            }
        }

        public double CholeskyTol
        {
            get
            {
                return this.xpparams.CholeskyTol;
            }
            set
            {
                this.xpparams.CholeskyTol = value;
            }
        }

        public int CoverCuts
        {
            get
            {
                return this.xpparams.CoverCuts;
            }
            set
            {
                this.xpparams.CoverCuts = value;
            }
        }

        public int Crash
        {
            get
            {
                return this.xpparams.Crash;
            }
            set
            {
                this.xpparams.Crash = value;
            }
        }

        public AlwaysNeverAutomatic CrossOver
        {
            get
            {
                return this.xpparams.CrossOver;
            }
            set
            {
                this.xpparams.CrossOver = value;
            }
        }

        public CrossoverDynamicReduction CrossOverDRP
        {
            get
            {
                return this.xpparams.CrossOverDRP;
            }
            set
            {
                this.xpparams.CrossOverDRP = value;
            }
        }

        public int CutDepth
        {
            get
            {
                return this.xpparams.CutDepth;
            }
            set
            {
                this.xpparams.CutDepth = value;
            }
        }

        public double CutFactor
        {
            get
            {
                return this.xpparams.CutFactor;
            }
            set
            {
                this.xpparams.CutFactor = value;
            }
        }

        public int CutFreq
        {
            get
            {
                return this.xpparams.CutFreq;
            }
            set
            {
                this.xpparams.CutFreq = value;
            }
        }

        public SolverFoundation.Plugin.XpressMP.CutSelect CutSelect
        {
            get
            {
                return this.xpparams.CutSelect;
            }
            set
            {
                this.xpparams.CutSelect = value;
            }
        }

        public SolverFoundation.Plugin.XpressMP.CutStrategy CutStrategy
        {
            get
            {
                return this.xpparams.CutStrategy;
            }
            set
            {
                this.xpparams.CutStrategy = value;
            }
        }

        public SolverFoundation.Plugin.XpressMP.DefaultAlg DefaultAlg
        {
            get
            {
                return this.xpparams.DefaultAlg;
            }
            set
            {
                this.xpparams.DefaultAlg = value;
            }
        }

        public double DegradeFactor
        {
            get
            {
                return this.xpparams.DegradeFactor;
            }
            set
            {
                this.xpparams.DegradeFactor = value;
            }
        }

        public int DenseColLimit
        {
            get
            {
                return this.xpparams.DenseColLimit;
            }
            set
            {
                this.xpparams.DenseColLimit = value;
            }
        }

        public bool Deterministic
        {
            get
            {
                return this.xpparams.Deterministic;
            }
            set
            {
                this.xpparams.Deterministic = value;
            }
        }

        public SolverFoundation.Plugin.XpressMP.DualGradient DualGradient
        {
            get
            {
                return this.xpparams.DualGradient;
            }
            set
            {
                this.xpparams.DualGradient = value;
            }
        }

        public AlwaysNeverAutomatic Dualize
        {
            get
            {
                return this.xpparams.Dualize;
            }
            set
            {
                this.xpparams.Dualize = value;
            }
        }

        public SolverFoundation.Plugin.XpressMP.DualStrategy DualStrategy
        {
            get
            {
                return this.xpparams.DualStrategy;
            }
            set
            {
                this.xpparams.DualStrategy = value;
            }
        }

        public double EigenValueTol
        {
            get
            {
                return this.xpparams.EigenValueTol;
            }
            set
            {
                this.xpparams.EigenValueTol = value;
            }
        }

        public double ElimTol
        {
            get
            {
                return this.xpparams.ElimTol;
            }
            set
            {
                this.xpparams.ElimTol = value;
            }
        }

        public double EtaTol
        {
            get
            {
                return this.xpparams.EtaTol;
            }
            set
            {
                this.xpparams.EtaTol = value;
            }
        }

        public int ExtraCols
        {
            get
            {
                return this.xpparams.ExtraCols;
            }
            set
            {
                this.xpparams.ExtraCols = value;
            }
        }

        public long ExtraElems
        {
            get
            {
                return this.xpparams.ExtraElems;
            }
            set
            {
                this.xpparams.ExtraElems = value;
            }
        }

        public int ExtraMIPEnts
        {
            get
            {
                return this.xpparams.ExtraMIPEnts;
            }
            set
            {
                this.xpparams.ExtraMIPEnts = value;
            }
        }

        public long ExtraPresolve
        {
            get
            {
                return this.xpparams.ExtraPresolve;
            }
            set
            {
                this.xpparams.ExtraPresolve = value;
            }
        }

        public int ExtraRows
        {
            get
            {
                return this.xpparams.ExtraRows;
            }
            set
            {
                this.xpparams.ExtraRows = value;
            }
        }

        public long ExtraSetElems
        {
            get
            {
                return this.xpparams.ExtraSetElems;
            }
            set
            {
                this.xpparams.ExtraSetElems = value;
            }
        }

        public int ExtraSets
        {
            get
            {
                return this.xpparams.ExtraSets;
            }
            set
            {
                this.xpparams.ExtraSets = value;
            }
        }

        public SolverFoundation.Plugin.XpressMP.FeasibilityPump FeasibilityPump
        {
            get
            {
                return this.xpparams.FeasibilityPump;
            }
            set
            {
                this.xpparams.FeasibilityPump = value;
            }
        }

        public double FeasTol
        {
            get
            {
                return this.xpparams.FeasTol;
            }
            set
            {
                this.xpparams.FeasTol = value;
            }
        }

        public double GlobalFileBias
        {
            get
            {
                return this.xpparams.GlobalFileBias;
            }
            set
            {
                this.xpparams.GlobalFileBias = value;
            }
        }

        public int GomCuts
        {
            get
            {
                return this.xpparams.GomCuts;
            }
            set
            {
                this.xpparams.GomCuts = value;
            }
        }

        public int HeurDepth
        {
            get
            {
                return this.xpparams.HeurDepth;
            }
            set
            {
                this.xpparams.HeurDepth = value;
            }
        }

        public int HeurDiveSpeedUp
        {
            get
            {
                return this.xpparams.HeurDiveSpeedUp;
            }
            set
            {
                this.xpparams.HeurDiveSpeedUp = value;
            }
        }

        public int HeurDiveStrategy
        {
            get
            {
                return this.xpparams.HeurDiveStrategy;
            }
            set
            {
                this.xpparams.HeurDiveStrategy = value;
            }
        }

        public int HeurFreq
        {
            get
            {
                return this.xpparams.HeurFreq;
            }
            set
            {
                this.xpparams.HeurFreq = value;
            }
        }

        public int HeurMaxSol
        {
            get
            {
                return this.xpparams.HeurMaxSol;
            }
            set
            {
                this.xpparams.HeurMaxSol = value;
            }
        }

        public int HeurNodes
        {
            get
            {
                return this.xpparams.HeurNodes;
            }
            set
            {
                this.xpparams.HeurNodes = value;
            }
        }

        public double HeurSearchEffort
        {
            get
            {
                return this.xpparams.HeurSearchEffort;
            }
            set
            {
                this.xpparams.HeurSearchEffort = value;
            }
        }

        public int HeurSearchFreq
        {
            get
            {
                return this.xpparams.HeurSearchFreq;
            }
            set
            {
                this.xpparams.HeurSearchFreq = value;
            }
        }

        public HeuristicSearchSelect HeurSearchRootSelect
        {
            get
            {
                return this.xpparams.HeurSearchRootSelect;
            }
            set
            {
                this.xpparams.HeurSearchRootSelect = value;
            }
        }

        public HeuristicSearchSelect HeurSearchTreeSelect
        {
            get
            {
                return this.xpparams.HeurSearchTreeSelect;
            }
            set
            {
                this.xpparams.HeurSearchTreeSelect = value;
            }
        }

        public int HeurSelect
        {
            get
            {
                return this.xpparams.HeurSelect;
            }
            set
            {
                this.xpparams.HeurSelect = value;
            }
        }

        public HeuristicStrategy HeurStrategy
        {
            get
            {
                return this.xpparams.HeurStrategy;
            }
            set
            {
                this.xpparams.HeurStrategy = value;
            }
        }

        public bool IfCheckConvexity
        {
            get
            {
                return this.xpparams.IfCheckConvexity;
            }
            set
            {
                this.xpparams.IfCheckConvexity = value;
            }
        }

        public int InvertFreq
        {
            get
            {
                return this.xpparams.InvertFreq;
            }
            set
            {
                this.xpparams.InvertFreq = value;
            }
        }

        public int InvertMin
        {
            get
            {
                return this.xpparams.InvertMin;
            }
            set
            {
                this.xpparams.InvertMin = value;
            }
        }

        public int L1Cache
        {
            get
            {
                return this.xpparams.L1Cache;
            }
            set
            {
                this.xpparams.L1Cache = value;
            }
        }

        public int LNPBest
        {
            get
            {
                return this.xpparams.LNPBest;
            }
            set
            {
                this.xpparams.LNPBest = value;
            }
        }

        public int LNPIterLimit
        {
            get
            {
                return this.xpparams.LNPIterLimit;
            }
            set
            {
                this.xpparams.LNPIterLimit = value;
            }
        }

        public int LocalChoice
        {
            get
            {
                return this.xpparams.LocalChoice;
            }
            set
            {
                this.xpparams.LocalChoice = value;
            }
        }

        public int LPIterLimit
        {
            get
            {
                return this.xpparams.LPIterLimit;
            }
            set
            {
                this.xpparams.LPIterLimit = value;
            }
        }

        public int LPLog
        {
            get
            {
                return this.xpparams.LPLog;
            }
            set
            {
                this.xpparams.LPLog = value;
            }
        }

        public double MarkowitzTol
        {
            get
            {
                return this.xpparams.MarkowitzTol;
            }
            set
            {
                this.xpparams.MarkowitzTol = value;
            }
        }

        public double MatrixTol
        {
            get
            {
                return this.xpparams.MatrixTol;
            }
            set
            {
                this.xpparams.MatrixTol = value;
            }
        }

        public int MaxCutTime
        {
            get
            {
                return this.xpparams.MaxCutTime;
            }
            set
            {
                this.xpparams.MaxCutTime = value;
            }
        }

        public int MaxGlobalFileSize
        {
            get
            {
                return this.xpparams.MaxGlobalFileSize;
            }
            set
            {
                this.xpparams.MaxGlobalFileSize = value;
            }
        }

        public int MaxMIPSol
        {
            get
            {
                return this.xpparams.MaxMIPSol;
            }
            set
            {
                this.xpparams.MaxMIPSol = value;
            }
        }

        public int MaxNode
        {
            get
            {
                return this.xpparams.MaxNode;
            }
            set
            {
                this.xpparams.MaxNode = value;
            }
        }

        public int MaxPageLines
        {
            get
            {
                return this.xpparams.MaxPageLines;
            }
            set
            {
                this.xpparams.MaxPageLines = value;
            }
        }

        public int MaxScaleFactor
        {
            get
            {
                return this.xpparams.MaxScaleFactor;
            }
            set
            {
                this.xpparams.MaxScaleFactor = value;
            }
        }

        public int MaxTime
        {
            get
            {
                return this.xpparams.MaxTime;
            }
            set
            {
                this.xpparams.MaxTime = value;
            }
        }

        public double MIPAbsCutoff
        {
            get
            {
                return this.xpparams.MIPAbsCutoff;
            }
            set
            {
                this.xpparams.MIPAbsCutoff = value;
            }
        }

        public double MIPAbsStop
        {
            get
            {
                return this.xpparams.MIPAbsStop;
            }
            set
            {
                this.xpparams.MIPAbsStop = value;
            }
        }

        public double MIPAddCutoff
        {
            get
            {
                return this.xpparams.MIPAddCutoff;
            }
            set
            {
                this.xpparams.MIPAddCutoff = value;
            }
        }

        public int MIPLog
        {
            get
            {
                return this.xpparams.MIPLog;
            }
            set
            {
                this.xpparams.MIPLog = value;
            }
        }

        public SolverFoundation.Plugin.XpressMP.MIPPresolve MIPPresolve
        {
            get
            {
                return this.xpparams.MIPPresolve;
            }
            set
            {
                this.xpparams.MIPPresolve = value;
            }
        }

        public double MIPRelCutoff
        {
            get
            {
                return this.xpparams.MIPRelCutoff;
            }
            set
            {
                this.xpparams.MIPRelCutoff = value;
            }
        }

        public double MIPRelStop
        {
            get
            {
                return this.xpparams.MIPRelStop;
            }
            set
            {
                this.xpparams.MIPRelStop = value;
            }
        }

        public double MIPTarget
        {
            get
            {
                return this.xpparams.MIPTarget;
            }
            set
            {
                this.xpparams.MIPTarget = value;
            }
        }

        public int MIPThreads
        {
            get
            {
                return this.xpparams.MIPThreads;
            }
            set
            {
                this.xpparams.MIPThreads = value;
            }
        }

        public double MIPTol
        {
            get
            {
                return this.xpparams.MIPTol;
            }
            set
            {
                this.xpparams.MIPTol = value;
            }
        }

        public NodeSelectionCriteria NodeSelection
        {
            get
            {
                return this.xpparams.NodeSelection;
            }
            set
            {
                this.xpparams.NodeSelection = value;
            }
        }

        public double OptimalityTol
        {
            get
            {
                return this.xpparams.OptimalityTol;
            }
            set
            {
                this.xpparams.OptimalityTol = value;
            }
        }

        public OutputDetail OutputLogDetail
        {
            get
            {
                return this.xpparams.OutputLogDetail;
            }
            set
            {
                this.xpparams.OutputLogDetail = value;
            }
        }

        public double OutputTol
        {
            get
            {
                return this.xpparams.OutputTol;
            }
            set
            {
                this.xpparams.OutputTol = value;
            }
        }

        public double Penalty
        {
            get
            {
                return this.xpparams.Penalty;
            }
            set
            {
                this.xpparams.Penalty = value;
            }
        }

        public double Perturb
        {
            get
            {
                return this.xpparams.Perturb;
            }
            set
            {
                this.xpparams.Perturb = value;
            }
        }

        public double PivotTol
        {
            get
            {
                return this.xpparams.PivotTol;
            }
            set
            {
                this.xpparams.PivotTol = value;
            }
        }

        public double PPFactor
        {
            get
            {
                return this.xpparams.PPFactor;
            }
            set
            {
                this.xpparams.PPFactor = value;
            }
        }

        public PresolveCoefElim PreCoefElim
        {
            get
            {
                return this.xpparams.PreCoefElim;
            }
            set
            {
                this.xpparams.PreCoefElim = value;
            }
        }

        public PresolveDomColumn PreDomCol
        {
            get
            {
                return this.xpparams.PreDomCol;
            }
            set
            {
                this.xpparams.PreDomCol = value;
            }
        }

        public PresolveDomRow PreDomRow
        {
            get
            {
                return this.xpparams.PreDomRow;
            }
            set
            {
                this.xpparams.PreDomRow = value;
            }
        }

        public SolverFoundation.Plugin.XpressMP.PreProbing PreProbing
        {
            get
            {
                return this.xpparams.PreProbing;
            }
            set
            {
                this.xpparams.PreProbing = value;
            }
        }

        public PresolveOperations PresolveOps
        {
            get
            {
                return this.xpparams.PresolveOps;
            }
            set
            {
                this.xpparams.PresolveOps = value;
            }
        }

        public Presolve PresolveType
        {
            get
            {
                return this.xpparams.PresolveType;
            }
            set
            {
                this.xpparams.PresolveType = value;
            }
        }

        public Pricing PricingAlg
        {
            get
            {
                return this.xpparams.PricingAlg;
            }
            set
            {
                this.xpparams.PricingAlg = value;
            }
        }

        public SolverFoundation.Plugin.XpressMP.PrimalUnshift PrimalUnshift
        {
            get
            {
                return this.xpparams.PrimalUnshift;
            }
            set
            {
                this.xpparams.PrimalUnshift = value;
            }
        }

        public double PseudoCost
        {
            get
            {
                return this.xpparams.PseudoCost;
            }
            set
            {
                this.xpparams.PseudoCost = value;
            }
        }

        public AlwaysNeverAutomatic QuadraticUnshift
        {
            get
            {
                return this.xpparams.QuadraticUnshift;
            }
            set
            {
                this.xpparams.QuadraticUnshift = value;
            }
        }

        public double RelPivotTol
        {
            get
            {
                return this.xpparams.RelPivotTol;
            }
            set
            {
                this.xpparams.RelPivotTol = value;
            }
        }

        public RepairIndefinateQuadratic RepairIndefiniteQ
        {
            get
            {
                return this.xpparams.RepairIndefiniteQ;
            }
            set
            {
                this.xpparams.RepairIndefiniteQ = value;
            }
        }

        public AlwaysNeverAutomatic RootPresolve
        {
            get
            {
                return this.xpparams.RootPresolve;
            }
            set
            {
                this.xpparams.RootPresolve = value;
            }
        }

        public int SBBest
        {
            get
            {
                return this.xpparams.SBBest;
            }
            set
            {
                this.xpparams.SBBest = value;
            }
        }

        public double SbEffort
        {
            get
            {
                return this.xpparams.SbEffort;
            }
            set
            {
                this.xpparams.SbEffort = value;
            }
        }

        public int SBEstimate
        {
            get
            {
                return this.xpparams.SBEstimate;
            }
            set
            {
                this.xpparams.SBEstimate = value;
            }
        }

        public int SBIterLimit
        {
            get
            {
                return this.xpparams.SBIterLimit;
            }
            set
            {
                this.xpparams.SBIterLimit = value;
            }
        }

        public int SBSelect
        {
            get
            {
                return this.xpparams.SBSelect;
            }
            set
            {
                this.xpparams.SBSelect = value;
            }
        }

        public SolverFoundation.Plugin.XpressMP.Scaling Scaling
        {
            get
            {
                return this.xpparams.Scaling;
            }
            set
            {
                this.xpparams.Scaling = value;
            }
        }

        public double SOSRefTol
        {
            get
            {
                return this.xpparams.SOSRefTol;
            }
            set
            {
                this.xpparams.SOSRefTol = value;
            }
        }

        public AlwaysNeverAutomatic TempBounds
        {
            get
            {
                return this.xpparams.TempBounds;
            }
            set
            {
                this.xpparams.TempBounds = value;
            }
        }

        public int Threads
        {
            get
            {
                return this.xpparams.Threads;
            }
            set
            {
                this.xpparams.Threads = value;
            }
        }

        public int TreeCompression
        {
            get
            {
                return this.xpparams.TreeCompression;
            }
            set
            {
                this.xpparams.TreeCompression = value;
            }
        }

        public int TreeCoverCuts
        {
            get
            {
                return this.xpparams.TreeCoverCuts;
            }
            set
            {
                this.xpparams.TreeCoverCuts = value;
            }
        }

        public int TreeCutSelect
        {
            get
            {
                return this.xpparams.TreeCutSelect;
            }
            set
            {
                this.xpparams.TreeCutSelect = value;
            }
        }

        public SolverFoundation.Plugin.XpressMP.TreeDiagnostics TreeDiagnostics
        {
            get
            {
                return this.xpparams.TreeDiagnostics;
            }
            set
            {
                this.xpparams.TreeDiagnostics = value;
            }
        }

        public int TreeGomCuts
        {
            get
            {
                return this.xpparams.TreeGomCuts;
            }
            set
            {
                this.xpparams.TreeGomCuts = value;
            }
        }

        public int TreeMemoryLimit
        {
            get
            {
                return this.xpparams.TreeMemoryLimit;
            }
            set
            {
                this.xpparams.TreeMemoryLimit = value;
            }
        }

        public double TreeMemorySavingTarget
        {
            get
            {
                return this.xpparams.TreeMemorySavingTarget;
            }
            set
            {
                this.xpparams.TreeMemorySavingTarget = value;
            }
        }

        public VariableSelection VarSelection
        {
            get
            {
                return this.xpparams.VarSelection;
            }
            set
            {
                this.xpparams.VarSelection = value;
            }
        }

        internal XpressMPParams XpressParams
        {
            get
            {
                return this.xpparams;
            }
        }
    }
}

