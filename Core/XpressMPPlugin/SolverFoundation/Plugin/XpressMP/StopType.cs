﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public enum StopType
    {
        CtrlC = 2,
        IterLimit = 4,
        MIPGap = 5,
        NodeLimit = 3,
        SolLimit = 6,
        TimeLimit = 1,
        User = 9
    }
}

