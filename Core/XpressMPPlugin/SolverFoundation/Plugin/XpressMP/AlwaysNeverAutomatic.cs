﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public enum AlwaysNeverAutomatic
    {
        Always = 1,
        Automatic = -1,
        Never = 0
    }
}

