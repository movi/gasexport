﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public static class XpressMPSolverProperties
    {
        public static readonly string AbsoluteGap = "AbsoluteGap";
        public static readonly string ActiveNodes = "Xpress.ActiveNodes";
        public static readonly string BarAASize = "Xpress.BarAASize";
        public static readonly string BarDenseCol = "Xpress.BarDenseCol";
        public static readonly string BarDualInf = "Xpress.BarDualInf";
        public static readonly string BarDualObj = "Xpress.BarDualObj";
        public static readonly string BarIter = "Xpress.BarIter";
        public static readonly string BarLSize = "Xpress.BarLSize";
        public static readonly string BarPrimalInf = "Xpress.BarPrimalInf";
        public static readonly string BarPrimalObj = "Xpress.BarPrimalObj";
        public static readonly string BranchValue = "Xpress.BranchValue";
        public static readonly string BranchVar = "Xpress.BranchVar";
        public static readonly string CurrentObjectiveBound = "CurrentObjectiveBound";
        public static readonly string CurrentObjectiveValue = "CurrentObjectiveValue";
        public static readonly string CurrMipCutOff = "Xpress.CurrMipCutOff";
        public static readonly string Cuts = "Xpress.Cuts";
        public static readonly string DualInfeas = "Xpress.DualInfeas";
        public static readonly string DualInfeasibility = "DualInfeasibility";
        public static readonly string DualObjectiveValue = "DualObjectiveValue";
        public static readonly string Elems = "Xpress.Elems";
        public static readonly string ExploredNodeCount = "ExploredNodeCount";
        public static readonly string IterationCount = "IterationCount";
        public static readonly string MIPEnts = "Xpress.MIPEnts";
        public static readonly string MIPInfeas = "Xpress.MIPInfeas";
        public static readonly string MIPSolNode = "Xpress.MIPSolNode";
        public static readonly string MIPSols = "Xpress.MIPSols";
        public static readonly string NodeDepth = "Xpress.NodeDepth";
        public static readonly string Nodes = "Xpress.Nodes";
        public static readonly string ParentNode = "Xpress.ParentNode";
        public static readonly string PresolveState = "Xpress.PresolveState";
        public static readonly string PrimalInfeas = "Xpress.PrimalInfeas";
        public static readonly string PrimalInfeasibility = "PrimalInfeasibility";
        public static readonly string PrimalObjectiveValue = "PrimalObjectiveValue";
        public static readonly string RelativeGap = "RelativeGap";
        public static readonly string RowLowerBound = "RowLowerBound";
        public static readonly string RowUpperBound = "RowUpperBound";
        public static readonly string SimplexIter = "Xpress.SimplexIter";
        public static readonly string SolutionCount = "SolutionCount";
        public static readonly string SolveState = "SolveState";
        public static readonly string StopStatus = "Xpress.StopStatus";
        public static readonly string VariableLowerBound = "VariableLowerBound";
        public static readonly string VariableStartValue = "VariableStartValue";
        public static readonly string VariableUpperBound = "VariableUpperBound";
    }
}

