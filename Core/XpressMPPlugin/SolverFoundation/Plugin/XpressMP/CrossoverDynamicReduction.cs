﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    [Flags, SuppressMessage("Microsoft.Naming", "CA1714:FlagsEnumsShouldHavePluralNames")]
    public enum CrossoverDynamicReduction
    {
        AggressiveBeforeCrossover = 4,
        BeforeCrossover = 1,
        InsideCrossover = 2
    }
}

