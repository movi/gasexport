﻿namespace SolverFoundation.Plugin.XpressMP
{
    using Microsoft.SolverFoundation.Common;
    using Microsoft.SolverFoundation.Services;
    using Optimizer;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Threading;

    public class XpressMPSolver : XpressMPSolveAttributes, ILinearSolver, ISolver, ILinearModel, ISolverProperties
    {
        private XpressMPGoal currentGoal;
        private string currentSolveState;
        private XpressMPSolution finalSolution;
        private List<XpressMPGoal> goals;
        private Dictionary<int, SolverFoundation.Plugin.XpressMP.Bounds> ignoredBounds;
        [SuppressMessage("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        private XpressMPLicense license;
        private int nullKeyCounter;
        private TextWriter output;
        private static int probCounter;
        private int[] savedColBasis;
        private bool savedColBasisModified;
        private int[] savedRowBasis;
        private bool savedRowBasisModified;
        private Dictionary<int, SOSSet> setReferenceRows;
        private int state;
        private Dictionary<int, Rational> userSolution;
        private VIDManager vids;
        private XPRSprob xpprob;

        public XpressMPSolver()
        {
            try
            {
                this.license = XpressMPLicense.GetLicense();
                this.output = null;
                this.currentSolveState = XpressMPSolveStates.NotSolving;
                this.Reset();
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
        }

        public XpressMPSolver(ISolverEnvironment env) : this()
        {
        }

        public XpressMPSolver(ISolverEnvironment env, LinearModel mdl) : this()
        {
            this.LoadLinearModel(mdl);
        }

        public ILinearGoal AddGoal(int vid, int pri, bool fMinimize)
        {
            XpressMPGoal item = new XpressMPGoal(this, vid, fMinimize, pri);
            this.goals.Add(item);
            return item;
        }

        public bool AddRow(object key, out int vid)
        {
            if (key == null)
            {
                key = "__nullkey__row__" + this.nullKeyCounter++;
            }
            if (this.vids.TryGetIndexFromKey(key, out vid))
            {
                if (this.vids.IsColumn(vid))
                {
                    vid = -1;
                }
                return false;
            }
            try
            {
                this.xpprob.AddRows(1, 0, new char[] { 'N' }, new double[] { 1E+20 }, new double[] { 2E+20 }, null, null, null);
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
            int rownum = this.xpprob.Rows - 1;
            vid = this.vids.AddRowVID(key, rownum);
            return true;
        }

        public bool AddRow(object key, SpecialOrderedSetType sos, out int vidRow)
        {
            if (key == null)
            {
                key = "__nullkey__sosrow__" + this.nullKeyCounter++;
            }
            if (this.AddRow(key, out vidRow))
            {
                this.setReferenceRows[vidRow] = new SOSSet(this, vidRow, sos);
                return true;
            }
            return false;
        }

        public bool AddVariable(object key, out int vid)
        {
            if (key == null)
            {
                key = "__nullkey__var__" + this.nullKeyCounter++;
            }
            if (this.vids.TryGetIndexFromKey(key, out vid))
            {
                if (this.vids.IsRow(vid))
                {
                    vid = -1;
                }
                return false;
            }
            try
            {
                double[] numArray = new double[1];
                int[] numArray2 = new int[1];
                this.xpprob.AddCols(1, 0, numArray, numArray2, null, null, new double[] { -1E+20 }, new double[] { 1E+20 });
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
            int colnum = this.xpprob.Cols - 1;
            vid = this.vids.AddColumnVID(key, colnum);
            return true;
        }

        public void ClearGoals()
        {
            if (this.currentGoal != null)
            {
                this.currentGoal.MoveGoalFromXPRSObjective();
                this.currentGoal = null;
            }
            foreach (XpressMPGoal goal in this.goals)
            {
                this.RemoveGoal(goal.Index);
            }
        }

        public void FixIntegers()
        {
            try
            {
                this.FlushCachedCoefficients();
                this.xpprob.FixGlobal();
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
        }

        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        private void FlushCachedCoefficients()
        {
        }

        public bool GetBasic(int vid)
        {
            bool flag;
            try
            {
                if (this.vids.IsRow(vid))
                {
                    return (this.RowBasis[this.vids.GetRowIndex(vid)] == 1);
                }
                flag = this.ColBasis[this.vids.GetColumnIndex(vid)] == 1;
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
            return flag;
        }

        public void GetBounds(int vid, out Rational numLo, out Rational numHi)
        {
            double num;
            double num2;
            this.GetBounds(vid, out num, out num2);
            numLo = ToMsfRational(num);
            numHi = ToMsfRational(num2);
        }

        internal void GetBounds(int vid, out double numLo, out double numHi)
        {
            try
            {
                if (this.ignoredBounds.ContainsKey(vid))
                {
                    SolverFoundation.Plugin.XpressMP.Bounds bounds = this.ignoredBounds[vid];
                    numLo = bounds.lower;
                    numHi = bounds.upper;
                }
                else
                {
                    if (this.vids.IsRow(vid))
                    {
                        int rowIndex = this.vids.GetRowIndex(vid);
                        double[] numArray = new double[1];
                        double[] numArray2 = new double[1];
                        char[] chArray = new char[1];
                        this.xpprob.GetRowType(chArray, rowIndex, rowIndex);
                        this.xpprob.GetRHS(numArray, rowIndex, rowIndex);
                        this.xpprob.GetRHSRange(numArray2, rowIndex, rowIndex);
                        switch (chArray[0])
                        {
                            case 'E':
                                numLo = numArray[0];
                                numHi = numArray[0];
                                return;

                            case 'G':
                            case 'Q':
                                numLo = numArray[0];
                                numHi = 1E+20;
                                return;

                            case 'L':
                            case 'P':
                                numLo = -1E+20;
                                numHi = numArray[0];
                                return;

                            case 'N':
                                numLo = -1E+20;
                                numHi = 1E+20;
                                return;

                            case 'R':
                                numHi = numArray[0];
                                numLo = numArray[0] - numArray2[0];
                                return;
                        }
                        throw new MsfException("Unknown row type '" + chArray[0] + "'");
                    }
                    int columnIndex = this.vids.GetColumnIndex(vid);
                    double[] numArray3 = new double[1];
                    double[] numArray4 = new double[1];
                    this.xpprob.GetLB(numArray4, columnIndex, columnIndex);
                    this.xpprob.GetUB(numArray3, columnIndex, columnIndex);
                    numHi = numArray3[0];
                    numLo = numArray4[0];
                }
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
        }

        public Rational GetCoefficient(int vidRow, int vidVar)
        {
            Rational rational;
            try
            {
                if ((this.currentGoal != null) && (this.currentGoal.Index == vidRow))
                {
                    this.currentGoal.MoveGoalFromXPRSObjective();
                    this.currentGoal = null;
                }
                if (!this.vids.IsRow(vidRow))
                {
                    throw new NotSupportedException();
                }
                if (!this.vids.IsColumn(vidVar))
                {
                    if (vidRow == vidVar)
                    {
                        return 1;
                    }
                    return 0;
                }
                int rowIndex = this.vids.GetRowIndex(vidRow);
                int columnIndex = this.vids.GetColumnIndex(vidVar);
                rational = ToMsfRational(this.xpprob.GetCoef(rowIndex, columnIndex));
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
            return rational;
        }

        public Rational GetCoefficient(int goalRow, int vidVar1, int vidVar2)
        {
            Rational rational;
            try
            {
                ILinearGoal goal;
                double num;
                if (!this.vids.IsColumn(vidVar1) || !this.vids.IsColumn(vidVar2))
                {
                    throw new NotSupportedException();
                }
                if ((this.currentGoal != null) && (this.currentGoal.Index == goalRow))
                {
                    this.currentGoal.MoveGoalFromXPRSObjective();
                    this.currentGoal = null;
                }
                if (!this.IsGoal(goalRow, out goal))
                {
                    throw new ArgumentException(goalRow + " is not a goal row");
                }
                this.xpprob.GetQRowCoeff(this.vids.GetRowIndex(goal.Index), this.vids.GetColumnIndex(vidVar1), this.vids.GetColumnIndex(vidVar2), out num);
                rational = ToMsfRational(num);
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
            return rational;
        }

        public ILinearGoal GetGoalFromIndex(int vid)
        {
            ILinearGoal goal;
            if (!this.IsGoal(vid, out goal))
            {
                return null;
            }
            return goal;
        }

        public bool GetIgnoreBounds(int vid)
        {
            return this.ignoredBounds.ContainsKey(vid);
        }

        public int GetIndexFromKey(object key)
        {
            return this.vids.GetIndexFromKey(key);
        }

        public bool GetIntegrality(int vid)
        {
            bool flag;
            try
            {
                if (!this.vids.IsColumn(vid))
                {
                    throw new MsfException("Rows may not be intregal");
                }
                char[] chArray = new char[1];
                int columnIndex = this.vids.GetColumnIndex(vid);
                this.xpprob.GetColType(chArray, columnIndex, columnIndex);
                flag = chArray[0] != 'C';
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
            return flag;
        }

        public object GetKeyFromIndex(int vid)
        {
            return this.vids.GetKeyFromIndex(vid);
        }

        public object GetProperty(string propertyName, int vid)
        {
            if (propertyName.Equals(XpressMPSolverProperties.SolveState))
            {
                return this.currentSolveState;
            }
            if (propertyName.Equals(XpressMPSolverProperties.CurrentObjectiveBound))
            {
                return this.MipBestBound;
            }
            if (propertyName.Equals(XpressMPSolverProperties.CurrentObjectiveValue))
            {
                if ((this.xpprob.MIPStatus != MIPStatus.LPNotOptimal) && (this.xpprob.MIPStatus != MIPStatus.LPOptimal))
                {
                    return ToMsfRational(this.xpprob.MIPObjVal);
                }
                return ToMsfRational(this.xpprob.LPObjVal);
            }
            if (propertyName.Equals(XpressMPSolverProperties.SolutionCount))
            {
                return this.xpprob.MIPSols;
            }
            if (propertyName.Equals(XpressMPSolverProperties.IterationCount))
            {
                return (this.xpprob.SimplexIter + this.xpprob.BarIter);
            }
            if (propertyName.Equals(XpressMPSolverProperties.ExploredNodeCount))
            {
                return this.xpprob.Nodes;
            }
            if (propertyName.Equals(XpressMPSolverProperties.RelativeGap))
            {
                if ((this.xpprob.MIPStatus != MIPStatus.Optimal) && (this.xpprob.MIPStatus != MIPStatus.Solution))
                {
                    return Rational.Indeterminate;
                }
                if (this.xpprob.MIPObjVal == 0.0)
                {
                    return Rational.Indeterminate;
                }
                return ((this.xpprob.MIPObjVal - this.xpprob.BestBound) / this.xpprob.MIPObjVal);
            }
            if (propertyName.Equals(XpressMPSolverProperties.AbsoluteGap))
            {
                if ((this.xpprob.MIPStatus != MIPStatus.Optimal) && (this.xpprob.MIPStatus != MIPStatus.Solution))
                {
                    return Rational.PositiveInfinity;
                }
                return Math.Abs((double) (this.xpprob.MIPObjVal - this.xpprob.BestBound));
            }
            if (propertyName.Equals(XpressMPSolverProperties.PrimalInfeasibility))
            {
                return this.xpprob.PrimalInfeas;
            }
            if (propertyName.Equals(XpressMPSolverProperties.DualInfeasibility))
            {
                return this.xpprob.DualInfeas;
            }
            if (propertyName.Equals(XpressMPSolverProperties.VariableLowerBound))
            {
                Rational rational;
                Rational rational2;
                this.GetBounds(vid, out rational, out rational2);
                return rational;
            }
            if (propertyName.Equals(XpressMPSolverProperties.VariableUpperBound))
            {
                Rational rational3;
                Rational rational4;
                this.GetBounds(vid, out rational3, out rational4);
                return rational4;
            }
            if (propertyName.Equals(XpressMPSolverProperties.VariableStartValue))
            {
                return this.userSolution[vid];
            }
            if (propertyName.Equals(XpressMPSolverProperties.RowUpperBound))
            {
                Rational rational5;
                Rational rational6;
                this.GetBounds(vid, out rational5, out rational6);
                return rational6;
            }
            if (propertyName.Equals(XpressMPSolverProperties.RowLowerBound))
            {
                Rational rational7;
                Rational rational8;
                this.GetBounds(vid, out rational7, out rational8);
                return rational7;
            }
            if (!propertyName.StartsWith("Xpress."))
            {
                throw new MsfException(string.Format("Property '{0}' is not recognised or not supported by XpressMPSolver", propertyName));
            }
            return base.GetXpressProperty(propertyName, vid);
        }

        public ILinearSolverReport GetReport(LinearSolverReportType reportType)
        {
            if(reportType == LinearSolverReportType.Infeasibility)
                return new XpressMPInfeasibilityReport(this);

            return null;
        }

        public IEnumerable<LinearEntry> GetRowEntries(int vidRow)
        {
            IEnumerable<LinearEntry> enumerable;
            try
            {
                this.FlushCachedCoefficients();
                if ((this.currentGoal != null) && (this.currentGoal.Index == vidRow))
                {
                    this.currentGoal.MoveGoalFromXPRSObjective();
                    this.currentGoal = null;
                }
                int rowIndex = this.vids.GetRowIndex(vidRow);
                int maxcoeffs = this.xpprob.GetRows(null, null, null, 0, rowIndex, rowIndex);
                int[] numArray = new int[2];
                int[] numArray2 = new int[maxcoeffs];
                double[] numArray3 = new double[maxcoeffs];
                this.xpprob.GetRows(numArray, numArray2, numArray3, maxcoeffs, rowIndex, rowIndex);
                LinearEntry[] entryArray = new LinearEntry[maxcoeffs];
                for (int i = 0; i < maxcoeffs; i++)
                {
                    int index = this.vids.GetIndexFromColumn(numArray2[i]);
                    entryArray[i] = new LinearEntry { Index = index, Key = this.vids.GetKeyFromIndex(index), Value = numArray3[i] };
                }
                enumerable = entryArray;
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
            return enumerable;
        }

        public int GetRowEntryCount(int vidRow)
        {
            int num2;
            try
            {
                this.FlushCachedCoefficients();
                int rowIndex = this.vids.GetRowIndex(vidRow);
                num2 = this.xpprob.GetRows(null, null, null, 0, rowIndex, rowIndex);
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
            return num2;
        }

        public IEnumerable<QuadraticEntry> GetRowQuadraticEntries(int vidRow)
        {
            IEnumerable<QuadraticEntry> enumerable;
            try
            {
                if ((this.currentGoal != null) && (this.currentGoal.Index == vidRow))
                {
                    this.currentGoal.MoveGoalFromXPRSObjective();
                    this.currentGoal = null;
                }
                int rowIndex = this.vids.GetRowIndex(vidRow);
                int num2 = this.xpprob.GetQRowQMatrixTriplets(rowIndex, null, null, null);
                int[] numArray = new int[num2];
                int[] numArray2 = new int[num2];
                double[] dqe = new double[num2];
                this.xpprob.GetQRowQMatrixTriplets(rowIndex, numArray, numArray2, dqe);
                QuadraticEntry[] entryArray = new QuadraticEntry[num2];
                for (int i = 0; i < num2; i++)
                {
                   var index1 = this.vids.GetIndexFromColumn(numArray[i]);
                    var index2 =this.vids.GetIndexFromColumn(numArray2[i]);
                    entryArray[i] = new QuadraticEntry { Index1 = index1, Index2 = index2, Key1 = this.vids.GetKeyFromIndex(index1), Key2 = this.vids.GetKeyFromIndex(index2), Value = dqe[i] };
                }
                enumerable = entryArray;
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
            return enumerable;
        }

        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId="goalIndex")]
        public Rational GetSolutionValue(int goalIndex)
        {
            return ToMsfRational((this.IsMipModel && ((this.xpprob.MIPStatus == MIPStatus.Optimal) || (this.xpprob.MIPStatus == MIPStatus.Solution))) ? this.xpprob.MIPObjVal : this.xpprob.LPObjVal);
        }

        public IEnumerable<int> GetSpecialOrderedSetTypeRowIndexes(SpecialOrderedSetType sosType)
        {
            List<int> list = new List<int>();
            foreach (SOSSet set in this.setReferenceRows.Values)
            {
                if (set.Type == sosType)
                {
                    list.Add(set.VID);
                }
            }
            return list;
        }

        public Rational GetValue(int vid)
        {
            if ((this.currentGoal != null) && (this.currentGoal.Index == vid))
            {
                this.currentGoal.MoveGoalFromXPRSObjective();
                this.currentGoal = null;
            }
            return this.Solution.GetValue(vid);
        }

        public LinearValueState GetValueState(int vid)
        {
            return this.Solution.GetValueState(vid);
        }

        public IEnumerable<LinearEntry> GetVariableEntries(int vid)
        {
            IEnumerable<LinearEntry> enumerable;
            try
            {
                this.FlushCachedCoefficients();
                int columnIndex = this.vids.GetColumnIndex(vid);
                int maxcoeffs = this.xpprob.GetCols(null, null, null, 0, columnIndex, columnIndex);
                int[] numArray = new int[maxcoeffs];
                double[] numArray2 = new double[maxcoeffs];
                this.xpprob.GetCols(null, numArray, numArray2, maxcoeffs, columnIndex, columnIndex);
                LinearEntry[] entryArray = new LinearEntry[maxcoeffs];
                for (int i = 0; i < maxcoeffs; i++)
                {
                    var index = this.vids.GetIndexFromRow(numArray[i]);
                    entryArray[i] = new LinearEntry { Index = index, Key = this.vids.GetKeyFromIndex(index), Value = numArray2[i] };
                }
                enumerable = entryArray;
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
            return enumerable;
        }

        public int GetVariableEntryCount(int vid)
        {
            int num2;
            try
            {
                this.FlushCachedCoefficients();
                int columnIndex = this.vids.GetColumnIndex(vid);
                num2 = this.xpprob.GetCols(null, null, null, 0, columnIndex, columnIndex);
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
            return num2;
        }

        public bool IsGoal(int vid)
        {
            ILinearGoal goal;
            return this.IsGoal(vid, out goal);
        }

        public bool IsGoal(int vid, out ILinearGoal goal)
        {
            foreach (ILinearGoal goal2 in this.goals)
            {
                if (goal2.Index == vid)
                {
                    goal = goal2;
                    return true;
                }
            }
            goal = null;
            return false;
        }

        public bool IsQuadraticVariable(int vidVar)
        {
            bool flag;
            try
            {
                if (this.vids.IsRow(vidVar))
                {
                    if ((this.currentGoal != null) && (this.currentGoal.Index == vidVar))
                    {
                        return (this.xpprob.QElems > 0);
                    }
                    int rowIndex = this.vids.GetRowIndex(vidVar);
                    return (this.xpprob.GetQRowQMatrixTriplets(rowIndex, null, null, null) > 0);
                }
                if (this.currentGoal != null)
                {
                    this.currentGoal.MoveGoalFromXPRSObjective();
                    this.currentGoal = null;
                }
                int columnIndex = this.vids.GetColumnIndex(vidVar);
                int[] qcrows = new int[this.xpprob.GetQRows()];
                this.xpprob.GetQRows(qcrows);
                foreach (int num5 in qcrows)
                {
                    if (this.xpprob.GetQRowQMatrix(num5, null, null, null, 0, columnIndex, columnIndex) > 0)
                    {
                        return true;
                    }
                }
                flag = false;
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
            return flag;
        }

        public bool IsRow(int vid)
        {
            return this.vids.IsRow(vid);
        }

        public void LoadLinearModel(ILinearModel otherModel)
        {
            this.Reset();
            if (otherModel.IsSpecialOrderedSet)
            {
                throw new MsfException("Cannot copy a SOS ILinearModel");
            }
            Dictionary<int, int> dictionary = new Dictionary<int, int>();
            foreach (object obj2 in otherModel.Keys)
            {
                int num2;
                int indexFromKey = otherModel.GetIndexFromKey(obj2);
                if (otherModel.IsRow(indexFromKey))
                {
                    this.AddRow(obj2, out num2);
                }
                else
                {
                    this.AddVariable(obj2, out num2);
                }
                dictionary[indexFromKey] = num2;
            }
            foreach (ILinearGoal goal in otherModel.Goals)
            {
                this.AddGoal(dictionary[goal.Index], goal.Priority, goal.Minimize);
            }
            foreach (int num3 in otherModel.RowIndices)
            {
                int vidRow = dictionary[num3];
                foreach (LinearEntry entry in otherModel.GetRowEntries(num3))
                {
                    this.SetCoefficient(vidRow, dictionary[entry.Index], entry.Value);
                }
                foreach (QuadraticEntry entry2 in otherModel.GetRowQuadraticEntries(num3))
                {
                    this.SetCoefficient(vidRow, entry2.Value, dictionary[entry2.Index1], dictionary[entry2.Index2]);
                }
            }
            foreach (int num5 in otherModel.Indices)
            {
                Rational rational;
                Rational rational2;
                otherModel.GetBounds(num5, out rational, out rational2);
                this.SetBounds(dictionary[num5], rational, rational2);
            }
            foreach (int num6 in otherModel.VariableIndices)
            {
                this.SetIntegrality(dictionary[num6], otherModel.GetIntegrality(num6));
            }
            foreach (int num7 in otherModel.Indices)
            {
                this.SetBasic(dictionary[num7], otherModel.GetBasic(num7));
            }
            foreach (int num8 in otherModel.Indices)
            {
                this.SetValue(dictionary[num8], otherModel.GetValue(num8));
            }
        }

        private XpressMPSolution LoadNewSolution()
        {
            return new XpressMPSolution(this);
        }

        private void msg_cb(XPRSprob prob, object vContext, string msg, int len, int msgtype)
        {
            if (this.output != null)
            {
                if (msg != null)
                {
                    this.output.WriteLine(msg);
                }
                this.output.Flush();
            }
        }

        public bool RemoveGoal(int vid)
        {
            ILinearGoal goal;
            if (!this.IsGoal(vid, out goal))
            {
                return false;
            }
            this.goals.Remove((XpressMPGoal) goal);
            return true;
        }

        internal static void RemoveXPRSprobObjective(XPRSprob xpprob)
        {
            int[] numArray = new int[xpprob.Cols + 1];
            for (int i = xpprob.Cols - 1; i >= 0; i--)
            {
                numArray[i] = i;
            }
            numArray[xpprob.Cols] = -1;
            double[] numArray2 = new double[xpprob.Cols + 1];
            for (int j = 0; j < numArray2.Length; j++)
            {
                numArray2[j] = 0.0;
            }
            xpprob.ChgObj(xpprob.Cols + 1, numArray, numArray2);
            int maxcoeffs = xpprob.GetMQObj(null, null, null, 0, 0, xpprob.Cols - 1);
            if (maxcoeffs > 0)
            {
                int[] numArray3 = new int[xpprob.Cols + 1];
                int[] numArray4 = new int[maxcoeffs];
                double[] numArray5 = new double[maxcoeffs];
                xpprob.GetMQObj(numArray3, numArray4, numArray5, maxcoeffs, 0, xpprob.Cols - 1);
                int[] numArray6 = new int[maxcoeffs];
                int[] numArray7 = new int[maxcoeffs];
                for (int k = 0; k < xpprob.Cols; k++)
                {
                    for (int m = numArray3[k]; m < numArray3[k + 1]; m++)
                    {
                        numArray6[m] = k;
                        numArray7[m] = numArray4[m];
                        numArray5[m] = 0.0;
                    }
                }
                xpprob.ChgMQObj(maxcoeffs, numArray6, numArray7, numArray5);
            }
        }

        public void Reset()
        {
            try
            {
                if (this.xpprob == null)
                    this.xpprob = new XPRSprob();

                if (this.output != null)
                {
                    this.xpprob.MessageCallbacks += new MessageCallback(this.msg_cb);
                }
                this.xpprob.LoadLP("MSFprob" + ++probCounter, 0, 0, null, null, null, null, (int[]) null, null, null, null, null, null);
                this.State = SolverState.Start;
                this.vids = new VIDManager(null as IEqualityComparer<object>);
                this.goals = new List<XpressMPGoal>();
                this.setReferenceRows = new Dictionary<int, SOSSet>();
                this.ignoredBounds = new Dictionary<int, SolverFoundation.Plugin.XpressMP.Bounds>();
                this.finalSolution = null;
                this.userSolution = new Dictionary<int, Rational>();
                this.savedColBasisModified = false;
                this.savedRowBasisModified = false;
                this.savedColBasis = null;
                this.savedRowBasis = null;
                this.currentGoal = null;
                this.currentSolveState = "doing nothing";
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
        }

        public void SetBasic(int vid, bool fBasic)
        {
            try
            {
                if (this.vids.IsRow(vid))
                {
                    this.RowBasis[this.vids.GetRowIndex(vid)] = fBasic ? 1 : 0;
                    this.savedRowBasisModified = true;
                }
                else
                {
                    this.ColBasis[this.vids.GetColumnIndex(vid)] = fBasic ? 1 : 0;
                    this.savedColBasisModified = true;
                }
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
        }

        public void SetBounds(int vid, Rational numLo, Rational numHi)
        {
            if ((this.currentGoal != null) && (this.currentGoal.Index == vid))
            {
                this.currentGoal.MoveGoalFromXPRSObjective();
                this.currentGoal = null;
            }
            this.SetBounds(vid, XpressMPDouble(numLo), XpressMPDouble(numHi));
        }

        internal void SetBounds(int vid, double loBound, double hiBound)
        {
            if (loBound > hiBound)
            {
                throw new ArgumentException("Lower bound may not be greater than upper bound");
            }
            try
            {
                if (this.ignoredBounds.ContainsKey(vid))
                {
                    this.ignoredBounds[vid] = new SolverFoundation.Plugin.XpressMP.Bounds(loBound, hiBound);
                }
                else if (this.vids.IsRow(vid))
                {
                    int rowIndex = this.vids.GetRowIndex(vid);
                    if (loBound == hiBound)
                    {
                        this.xpprob.ChgRowType(1, new int[] { rowIndex }, new char[] { 'E' });
                        this.xpprob.ChgRHS(1, new int[] { rowIndex }, new double[] { hiBound });
                    }
                    else if (((loBound == -1E+20) && (hiBound == 1E+20)) && !this.IsQuadraticVariable(vid))
                    {
                        this.xpprob.ChgRowType(1, new int[] { rowIndex }, new char[] { 'N' });
                    }
                    else if (loBound == -1E+20)
                    {
                        this.xpprob.ChgRowType(1, new int[] { rowIndex }, new char[] { 'L' });
                        this.xpprob.ChgRHS(1, new int[] { rowIndex }, new double[] { hiBound });
                    }
                    else if (hiBound == 1E+20)
                    {
                        this.xpprob.ChgRowType(1, new int[] { rowIndex }, new char[] { 'G' });
                        this.xpprob.ChgRHS(1, new int[] { rowIndex }, new double[] { loBound });
                    }
                    else
                    {
                        this.xpprob.ChgRowType(1, new int[] { rowIndex }, new char[] { 'R' });
                        this.xpprob.ChgRHS(1, new int[] { rowIndex }, new double[] { hiBound });
                        this.xpprob.ChgRHSRange(1, new int[] { rowIndex }, new double[] { hiBound - loBound });
                    }
                }
                else
                {
                    int columnIndex = this.vids.GetColumnIndex(vid);
                    this.xpprob.ChgBounds(1, new int[] { columnIndex }, new char[] { 'L' }, new double[] { loBound });
                    this.xpprob.ChgBounds(1, new int[] { columnIndex }, new char[] { 'U' }, new double[] { hiBound });
                }
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
        }

        public void SetCoefficient(int vidRow, int vidVar, Rational num)
        {
            try
            {
                SOSSet set;
                if (!this.vids.IsRow(vidRow))
                {
                    throw new NotSupportedException();
                }
                if (!this.vids.IsColumn(vidVar))
                {
                    throw new NotSupportedException();
                }
                if ((this.currentGoal != null) && (this.currentGoal.Index == vidRow))
                {
                    this.currentGoal.MoveGoalFromXPRSObjective();
                    this.currentGoal = null;
                }
                if (this.setReferenceRows.TryGetValue(vidRow, out set) && set.IsCreated)
                {
                    this.UncreateAllSets();
                    this.xpprob.ChgCoef(this.vids.GetRowIndex(vidRow), this.vids.GetColumnIndex(vidVar), XpressMPDouble(num));
                }
                else
                {
                    this.xpprob.ChgCoef(this.vids.GetRowIndex(vidRow), this.vids.GetColumnIndex(vidVar), XpressMPDouble(num));
                }
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
        }

        public void SetCoefficient(int vidRow, Rational num, int vidVar1, int vidVar2)
        {
            try
            {
                ILinearGoal goal;
                if (!this.vids.IsColumn(vidVar1) || !this.vids.IsColumn(vidVar2))
                {
                    throw new NotSupportedException();
                }
                if ((this.currentGoal != null) && (this.currentGoal.Index == vidRow))
                {
                    this.currentGoal.MoveGoalFromXPRSObjective();
                    this.currentGoal = null;
                }
                if (!this.IsGoal(vidRow, out goal))
                {
                    throw new ArgumentException(vidRow + " is not a goal row");
                }
                double dval = XpressMPDouble(num) * 2.0;
                if (vidVar1 != vidVar2)
                {
                    dval /= 2.0;
                }
                this.xpprob.ChgQRowCoeff(this.vids.GetRowIndex(goal.Index), this.vids.GetColumnIndex(vidVar1), this.vids.GetColumnIndex(vidVar2), dval);
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
        }

        public void SetIgnoreBounds(int vid, bool fIgnore)
        {
            try
            {
                if (this.IsGoal(vid))
                {
                    throw new NotSupportedException("Cannot call SetIgnoreBounds on a goal row");
                }
                if (fIgnore)
                {
                    if (!this.ignoredBounds.ContainsKey(vid))
                    {
                        this.ignoredBounds[vid] = new SolverFoundation.Plugin.XpressMP.Bounds(this, vid);
                        if (this.vids.IsColumn(vid))
                        {
                            int columnIndex = this.vids.GetColumnIndex(vid);
                            this.xpprob.ChgBounds(1, new int[] { columnIndex }, new char[] { 'U' }, new double[] { 1E+20 });
                            this.xpprob.ChgBounds(1, new int[] { columnIndex }, new char[] { 'L' }, new double[] { -1E+20 });
                        }
                        else
                        {
                            int num2 = this.vids.GetColumnIndex(vid);
                            this.xpprob.ChgRowType(1, new int[] { num2 }, new char[] { 'N' });
                        }
                    }
                }
                else if (this.ignoredBounds.ContainsKey(vid))
                {
                    SolverFoundation.Plugin.XpressMP.Bounds bounds = this.ignoredBounds[vid];
                    this.ignoredBounds.Remove(vid);
                    this.SetBounds(vid, bounds.lower, bounds.upper);
                }
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
        }

        public void SetIntegrality(int vid, bool fInteger)
        {
            try
            {
                if (!this.vids.IsColumn(vid))
                {
                    throw new MsfException("Rows may not be intregal");
                }
                this.xpprob.ChgColType(1, new int[] { this.vids.GetColumnIndex(vid) }, new char[] { fInteger ? 'I' : 'C' });
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
        }

        public void SetLowerBound(int vid, Rational numLo)
        {
            try
            {
                if (this.ignoredBounds.ContainsKey(vid))
                {
                    this.ignoredBounds[vid] = new SolverFoundation.Plugin.XpressMP.Bounds((double) numLo, this.ignoredBounds[vid].upper);
                }
                else if (this.vids.IsRow(vid))
                {
                    Rational rational;
                    Rational rational2;
                    this.GetBounds(vid, out rational2, out rational);
                    if (numLo > rational)
                    {
                        rational = numLo;
                    }
                    this.SetBounds(vid, numLo, rational);
                }
                else
                {
                    this.xpprob.ChgBounds(1, new int[] { this.vids.GetColumnIndex(vid) }, new char[] { 'L' }, new double[] { (double) numLo });
                }
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
        }

        public void SetProperty(string propertyName, int vid, object value)
        {
            if (propertyName.Equals(SolverProperties.VariableLowerBound))
            {
                this.SetLowerBound(vid, (Rational) value);
            }
            else if (propertyName.Equals(SolverProperties.VariableUpperBound))
            {
                this.SetUpperBound(vid, (Rational) value);
            }
            else
            {
                if (propertyName.Equals(SolverProperties.VariableStartValue))
                {
                    this.SetValue(vid, (Rational) value);
                }
                //if (propertyName.Equals(SolverProperties.RowUpperBound))
                //{
                //    this.SetUpperBound(vid, (Rational) value);
                //}
                //else
                //{
                //    if (!propertyName.Equals(SolverProperties.RowLowerBound))
                //    {
                //        throw new MsfException(string.Format("Setting of XpressMPSolver property {0} is not permitted.", propertyName));
                //    }
                //    this.SetLowerBound(vid, (Rational) value);
                //}
            }
        }

        internal void SetSolveState(string newstate)
        {
            this.currentSolveState = newstate;
        }

        public void SetUpperBound(int vid, Rational numHi)
        {
            try
            {
                if (this.ignoredBounds.ContainsKey(vid))
                {
                    this.ignoredBounds[vid] = new SolverFoundation.Plugin.XpressMP.Bounds(this.ignoredBounds[vid].lower, (double) numHi);
                }
                if (this.vids.IsRow(vid))
                {
                    Rational rational;
                    Rational rational2;
                    this.GetBounds(vid, out rational2, out rational);
                    if (numHi < rational2)
                    {
                        rational2 = numHi;
                    }
                    this.SetBounds(vid, rational2, numHi);
                }
                else
                {
                    this.xpprob.ChgBounds(1, new int[] { this.vids.GetColumnIndex(vid) }, new char[] { 'U' }, new double[] { (double) numHi });
                }
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
        }

        public void SetValue(int vid, Rational num)
        {
            if ((this.currentGoal != null) && (this.currentGoal.Index == vid))
            {
                this.currentGoal.MoveGoalFromXPRSObjective();
                this.currentGoal = null;
            }
            this.userSolution[vid] = num;
        }

        public void Shutdown()
        {
            if (this.State != SolverState.Disposed)
            {
                while (Interlocked.CompareExchange(ref this.state, 5, 0) == 0)
                {
                }
                if (this.State != SolverState.Disposing)
                {
                    while ((this.State != SolverState.Solved) && (this.State != SolverState.Aborted))
                    {
                        Thread.Sleep(0);
                    }
                }
                this.State = SolverState.Disposing;
                this.xpprob.Dispose();
                this.license = null;
                this.State = SolverState.Disposed;
            }
        }

        public ILinearSolution Solve(ISolverParameters parameters)
        {
            return this.Solve(parameters, false);
        }

        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        internal ILinearSolution Solve(ISolverParameters parameters, bool solveRelaxation)
        {
            ILinearSolution solution;
            bool flag = false;
            XpressMPParams prms = null;
            try
            {
                this.currentSolveState = XpressMPSolveStates.NotSolving;
                if ((this.State == SolverState.Disposing) || (this.State == SolverState.Disposed))
                {
                    return this.Solution;
                }
                this.State = SolverState.Solving;
                if (this.State == SolverState.Solving)
                {
                    this.finalSolution = null;
                    prms = parameters as XpressMPParams;
                    if (prms == null)
                    {
                        prms = new XpressMPParams {
                            QueryAbort = parameters.QueryAbort
                        };
                    }
                    prms.ApplyParameters(this);
                    flag = true;
                    this.FlushCachedCoefficients();
                    foreach (SOSSet set in this.setReferenceRows.Values)
                    {
                        if (!set.IsCreated)
                        {
                            set.CreateSet();
                        }
                    }
                    int num = 0;
                    foreach (int num2 in this.vids.VariableIndices)
                    {
                        Rational rational;
                        if ((this.userSolution.TryGetValue(num2, out rational) && (rational != Rational.Indeterminate)) && !this.IsGoal(num2))
                        {
                            num++;
                        }
                    }
                    if (num > 1)
                    {
                        double[] dsol = new double[this.xpprob.OriginalCols];
                        foreach (int num3 in this.vids.VariableIndices)
                        {
                            Rational rational2;
                            if (this.userSolution.TryGetValue(num3, out rational2) && (rational2 != Rational.Indeterminate))
                            {
                                dsol[this.vids.GetColumnIndex(num3)] = XpressMPDouble(rational2);
                            }
                            else
                            {
                                dsol[this.vids.GetColumnIndex(num3)] = 0.0;
                            }
                        }
                        this.xpprob.LoadMipSol(dsol);
                    }
                    if (this.savedRowBasisModified || this.savedColBasisModified)
                    {
                        this.xpprob.LoadBasis(this.RowBasis, this.ColBasis);
                    }
                    this.savedRowBasis = null;
                    this.savedRowBasisModified = false;
                    this.savedColBasis = null;
                    this.savedColBasisModified = false;
                    if (this.GoalCount == 0)
                    {
                        if (this.currentGoal != null)
                        {
                            this.currentGoal.MoveGoalFromXPRSObjective();
                        }
                        this.xpprob.Minim();
                        if (this.xpprob.LPStatus == LPStatus.Unbounded)
                        {
                            this.xpprob.Maxim();
                        }
                        this.finalSolution = new XpressMPSolution(this);
                    }
                    else
                    {
                        if (this.GoalCount > 1)
                        {
                            throw new NotSupportedException("Xpress solver plugin cannot solve for multiple goals at this time");
                        }
                        XpressMPGoal goal = this.goals[0];
                        if (this.currentGoal != goal)
                        {
                            if (this.currentGoal != null)
                            {
                                this.currentGoal.MoveGoalFromXPRSObjective();
                            }
                            goal.MoveGoalToXPRSObjective();
                            this.currentGoal = goal;
                        }
                        string str = "";
                        if (prms.DefaultAlg == SolverFoundation.Plugin.XpressMP.DefaultAlg.Dual)
                        {
                            str = str + "d";
                        }
                        else if (prms.DefaultAlg == SolverFoundation.Plugin.XpressMP.DefaultAlg.Primal)
                        {
                            str = str + "p";
                        }
                        else if (prms.DefaultAlg == SolverFoundation.Plugin.XpressMP.DefaultAlg.Barrier)
                        {
                            str = str + "b";
                        }
                        if (this.IsMipModel && !solveRelaxation)
                        {
                            str = str + "g";
                        }
                        else
                        {
                            str = str + "l";
                        }
                        if (prms.Debug)
                        {
                            this.xpprob.Save();
                            this.xpprob.WriteProb(null, "p");
                        }
                        this.currentSolveState = XpressMPSolveStates.InPresolve;
                        if (goal.Minimize)
                        {
                            this.xpprob.Minim(str);
                        }
                        else
                        {
                            this.xpprob.Maxim(str);
                        }
                        if ((this.xpprob.PresolveState & 6) != 0)
                        {
                            this.xpprob.PostSolve();
                        }
                        this.finalSolution = new XpressMPSolution(this);
                    }
                    if (this.State == SolverState.Aborting)
                    {
                        this.State = SolverState.Aborted;
                    }
                    else if (this.State == SolverState.Solving)
                    {
                        this.State = SolverState.Solved;
                    }
                }
                solution = this.Solution;
            }
            catch (XPRSException exception)
            {
                this.State = SolverState.Aborted;
                throw new MsfException(exception.Message, exception);
            }
            catch (Exception)
            {
                this.State = SolverState.Aborted;
                throw;
            }
            finally
            {
                if (flag)
                {
                    prms.RemoveParameters(this);
                }
                this.savedRowBasis = null;
                this.savedColBasis = null;
                this.savedRowBasisModified = false;
                this.savedColBasisModified = false;
                if ((this.xpprob.PresolveState & 6) != 0)
                {
                    this.xpprob.PostSolve();
                }
                this.currentSolveState = XpressMPSolveStates.NotSolving;
            }
            return solution;
        }

        public ILinearSolution SolveRelaxation(ISolverParameters parameters)
        {
            return this.Solve(parameters, true);
        }

        internal static Rational ToMsfRational(double val)
        {
            if (val <= -1E+20)
            {
                return Rational.NegativeInfinity;
            }
            if (val >= 1E+20)
            {
                return Rational.PositiveInfinity;
            }
            return val;
        }

        public bool TryGetIndexFromKey(object key, out int vid)
        {
            return this.vids.TryGetIndexFromKey(key, out vid);
        }

        internal void UncreateAllSets()
        {
            try
            {
                while (this.xpprob.Sets > 0)
                {
                    this.xpprob.DelSets(1, new int[1]);
                }
                foreach (SOSSet set in this.setReferenceRows.Values)
                {
                    set.SetUncreated();
                }
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
        }

        internal static double XpressMPDouble(Rational value)
        {
            if (value == Rational.NegativeInfinity)
            {
                return -1E+20;
            }
            if (value == Rational.PositiveInfinity)
            {
                return 1E+20;
            }
            if (value == Rational.Indeterminate)
            {
                throw new MsfException("Not a number");
            }
            return XpressMPDouble((double) value);
        }

        internal static double XpressMPDouble(double value)
        {
            if (value <= -1E+20)
            {
                return -1E+20;
            }
            if (value >= 1E+20)
            {
                return 1E+20;
            }
            return value;
        }

        internal static LinearResult XpressStatusToLinearResult(LPStatus status)
        {
            switch (status)
            {
                case LPStatus.Unstarted:
                    return LinearResult.Invalid;

                case LPStatus.Optimal:
                    return LinearResult.Optimal;

                case LPStatus.Infeasible:
                    return LinearResult.InfeasiblePrimal;

                case LPStatus.CutOff:
                case LPStatus.CutOffInDual:
                    return LinearResult.InfeasibleOrUnbounded;

                case LPStatus.Unfinished:
                    return LinearResult.Interrupted;

                case LPStatus.Unbounded:
                    return LinearResult.UnboundedPrimal;

                case LPStatus.Unsolved:
                    return LinearResult.Invalid;
            }
            throw new MsfException("Unexpected problem status - LPStatus=" + status.ToString());
        }

        internal static LinearResult XpressStatusToLinearResult(MIPStatus status)
        {
            switch (status)
            {
                case MIPStatus.LPOptimal:
                    return LinearResult.Interrupted;

                case MIPStatus.NoSolutionFound:
                    return LinearResult.InfeasiblePrimal;

                case MIPStatus.Solution:
                    return LinearResult.Feasible;

                case MIPStatus.Infeasible:
                    return LinearResult.InfeasiblePrimal;

                case MIPStatus.Optimal:
                    return LinearResult.Optimal;
            }
            return LinearResult.Invalid;
        }

        public int CoefficientCount
        {
            get
            {
                this.FlushCachedCoefficients();
                return (int)this.xpprob.Elems;
            }
        }

        private int[] ColBasis
        {
            get
            {
                if ((this.savedColBasis == null) || (this.savedColBasis.Length < this.xpprob.OriginalCols))
                {
                    int[] numArray = new int[this.xpprob.OriginalCols];
                    this.xpprob.GetBasis(null, numArray);
                    if ((this.savedColBasis != null) && this.savedColBasisModified)
                    {
                        for (int i = 0; i < this.savedColBasis.Length; i++)
                        {
                            numArray[i] = this.savedColBasis[i];
                        }
                    }
                    this.savedColBasis = numArray;
                    this.savedColBasisModified = false;
                }
                return this.savedColBasis;
            }
        }

        public int GoalCount
        {
            get
            {
                return this.goals.Count;
            }
        }

        public IEnumerable<ILinearGoal> Goals
        {
            get
            {
                List<ILinearGoal> list = new List<ILinearGoal>();
                foreach (XpressMPGoal goal in this.goals)
                {
                    list.Add(goal);
                }
                return list;
            }
        }

        public IEnumerable<int> Indices
        {
            get
            {
                return this.vids.Indices;
            }
        }

        public int IntegerIndexCount
        {
            get
            {
                return this.xpprob.MIPEnts;
            }
        }

        public bool IsMipModel
        {
            get
            {
                if (this.xpprob.MIPEnts <= 0)
                {
                    return (this.xpprob.Sets > 0);
                }
                return true;
            }
        }

        public bool IsQuadraticModel
        {
            get
            {
                foreach (XpressMPGoal goal in this.goals)
                {
                    if (goal == this.currentGoal)
                    {
                        if (this.xpprob.QElems > 0)
                        {
                            return true;
                        }
                    }
                    else if (this.xpprob.GetQRowQMatrix(this.vids.GetRowIndex(goal.Index), null, null, null, 0, 0, this.xpprob.OriginalCols - 1) > 0)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public bool IsSpecialOrderedSet
        {
            get
            {
                return (this.setReferenceRows.Count > 0);
            }
        }

        public IEqualityComparer<object> KeyComparer
        {
            get
            {
                return this.vids.KeyComparer;
            }
        }

        public int KeyCount
        {
            get
            {
                return this.vids.KeyCount;
            }
        }

        public IEnumerable<object> Keys
        {
            get
            {
                return this.vids.Keys;
            }
        }

        public LinearResult LpResult
        {
            get
            {
                return XpressStatusToLinearResult(this.xpprob.LPStatus);
            }
        }

        public Rational LpSolutionValue
        {
            get
            {
                return ToMsfRational(this.xpprob.LPObjVal);
            }
        }

        public Rational MipBestBound
        {
            get
            {
                return ToMsfRational(this.xpprob.BestBound);
            }
        }

        public LinearResult MipResult
        {
            get
            {
                return XpressStatusToLinearResult(this.xpprob.MIPStatus);
            }
        }

        public Rational MipSolutionValue
        {
            get
            {
                return ToMsfRational(this.xpprob.MIPObjVal);
            }
        }

        public TextWriter OutputLog
        {
            get
            {
                return this.output;
            }
            set
            {
                if (value == null)
                {
                    if (this.output != null)
                    {
                        this.xpprob.RemoveMessageCallbacks();
                    }
                    this.output = value;
                }
                else if (this.output == null)
                {
                    this.output = value;
                    this.xpprob.MessageCallbacks += new MessageCallback(this.msg_cb);
                }
                else
                {
                    this.output = value;
                }
            }
        }

        public LinearResult Result
        {
            get
            {
                if ((this.xpprob.MIPEnts != 0) && (this.xpprob.MIPStatus != MIPStatus.LPNotOptimal))
                {
                    return XpressStatusToLinearResult(this.xpprob.MIPStatus);
                }
                return XpressStatusToLinearResult(this.xpprob.LPStatus);
            }
        }

        private int[] RowBasis
        {
            get
            {
                int[] savedRowBasis;
                try
                {
                    if ((this.savedRowBasis == null) || (this.savedRowBasis.Length < this.xpprob.OriginalRows))
                    {
                        int[] numArray = new int[this.xpprob.OriginalRows];
                        this.xpprob.GetBasis(numArray, null);
                        if ((this.savedRowBasis != null) && this.savedRowBasisModified)
                        {
                            for (int i = 0; i < this.savedRowBasis.Length; i++)
                            {
                                numArray[i] = this.savedRowBasis[i];
                            }
                        }
                        this.savedRowBasis = numArray;
                        this.savedRowBasisModified = false;
                    }
                    savedRowBasis = this.savedRowBasis;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return savedRowBasis;
            }
        }

        public int RowCount
        {
            get
            {
                return this.vids.RowCount;
            }
        }

        public IEnumerable<int> RowIndices
        {
            get
            {
                return this.vids.RowIndices;
            }
        }

        public IEnumerable<object> RowKeys
        {
            get
            {
                return this.vids.RowKeys;
            }
        }

        public ILinearSolution Solution
        {
            get
            {
                if (this.finalSolution == null)
                {
                    return this.LoadNewSolution();
                }
                return this.finalSolution;
            }
        }

        internal SolverState State
        {
            get
            {
                return (SolverState) this.state;
            }
            set
            {
                this.state = (int) value;
            }
        }

        internal Dictionary<int, Rational> UserSolution
        {
            get
            {
                return this.userSolution;
            }
        }

        public int VariableCount
        {
            get
            {
                return this.vids.VariableCount;
            }
        }

        public IEnumerable<int> VariableIndices
        {
            get
            {
                return this.vids.VariableIndices;
            }
        }

        public IEnumerable<object> VariableKeys
        {
            get
            {
                return this.vids.VariableKeys;
            }
        }

        internal VIDManager VIDs
        {
            get
            {
                return this.vids;
            }
        }

        internal XPRSprob XpressProb
        {
            get
            {
                return this.xpprob;
            }
        }

        internal override XPRSprob xprsprob
        {
            get
            {
                return this.xpprob;
            }
        }
    }
}

