﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public enum HeuristicSearchSelect
    {
        LocalSearchLargeNeighbourhood,
        LocalSearchNodeNeighbourhood,
        LocalSearchSolutionNeighbourhood
    }
}

