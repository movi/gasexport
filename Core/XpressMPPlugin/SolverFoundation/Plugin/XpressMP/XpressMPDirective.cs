﻿namespace SolverFoundation.Plugin.XpressMP
{
    using Microsoft.SolverFoundation.Services;
    using System;
    using System.IO;

    public class XpressMPDirective : XpressMPSolveControlDirectives
    {
        private bool debugFlag;
        private TextWriter outputLog;

        public XpressMPDirective()
        {
            base.Arithmetic = Arithmetic.Default;
            base.MaximumGoalCount = 1;
            this.outputLog = null;
            this.debugFlag = false;
        }

        public XpressMPDirective(TextWriter outputLog) : this()
        {
            this.outputLog = outputLog;
        }

        public bool Debug
        {
            get
            {
                return this.debugFlag;
            }
            set
            {
                this.debugFlag = value;
            }
        }

        public TextWriter OutputLog
        {
            get
            {
                return this.outputLog;
            }
            set
            {
                this.outputLog = value;
            }
        }
    }
}

