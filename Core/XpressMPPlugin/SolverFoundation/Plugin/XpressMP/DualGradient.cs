﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public enum DualGradient
    {
        Automatic = -1,
        Devex = 0,
        SteepestEdge = 1
    }
}

