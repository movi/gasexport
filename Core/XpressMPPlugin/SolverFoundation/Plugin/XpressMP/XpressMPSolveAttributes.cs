﻿namespace SolverFoundation.Plugin.XpressMP
{
    using Microsoft.SolverFoundation.Common;
    using Optimizer;
    using System;

    public abstract class XpressMPSolveAttributes
    {
        protected XpressMPSolveAttributes()
        {
        }

        internal object GetXpressProperty(string propertyName, int vid)
        {
            if (propertyName.StartsWith("Xpress."))
            {
                if (propertyName.Equals(XpressMPSolverProperties.Elems))
                {
                    return this.Elems;
                }
                if (propertyName.Equals(XpressMPSolverProperties.PrimalInfeas))
                {
                    return this.PrimalInfeas;
                }
                if (propertyName.Equals(XpressMPSolverProperties.DualInfeas))
                {
                    return this.DualInfeas;
                }
                if (propertyName.Equals(XpressMPSolverProperties.SimplexIter))
                {
                    return this.SimplexIter;
                }
                if (propertyName.Equals(XpressMPSolverProperties.Cuts))
                {
                    return this.Cuts;
                }
                if (propertyName.Equals(XpressMPSolverProperties.Nodes))
                {
                    return this.Nodes;
                }
                if (propertyName.Equals(XpressMPSolverProperties.NodeDepth))
                {
                    return this.NodeDepth;
                }
                if (propertyName.Equals(XpressMPSolverProperties.ActiveNodes))
                {
                    return this.ActiveNodes;
                }
                if (propertyName.Equals(XpressMPSolverProperties.MIPSolNode))
                {
                    return this.MIPSolNode;
                }
                if (propertyName.Equals(XpressMPSolverProperties.MIPSols))
                {
                    return this.MIPSols;
                }
                if (propertyName.Equals(XpressMPSolverProperties.MIPInfeas))
                {
                    return this.MIPInfeas;
                }
                if (propertyName.Equals(XpressMPSolverProperties.PresolveState))
                {
                    return this.PresolveState;
                }
                if (propertyName.Equals(XpressMPSolverProperties.ParentNode))
                {
                    return this.ParentNode;
                }
                if (propertyName.Equals(XpressMPSolverProperties.MIPEnts))
                {
                    return this.MIPEnts;
                }
                if (propertyName.Equals(XpressMPSolverProperties.BranchVar))
                {
                    return this.BranchVar;
                }
                if (propertyName.Equals(XpressMPSolverProperties.StopStatus))
                {
                    return this.StopStatus;
                }
                if (propertyName.Equals(XpressMPSolverProperties.BranchValue))
                {
                    return XpressMPSolver.ToMsfRational(this.BranchValue);
                }
                if (propertyName.Equals(XpressMPSolverProperties.CurrMipCutOff))
                {
                    return XpressMPSolver.ToMsfRational(this.CurrMipCutOff);
                }
                if (propertyName.Equals(XpressMPSolverProperties.BarPrimalObj))
                {
                    return XpressMPSolver.ToMsfRational(this.BarPrimalObj);
                }
                if (propertyName.Equals(XpressMPSolverProperties.BarDualObj))
                {
                    return XpressMPSolver.ToMsfRational(this.BarDualObj);
                }
                if (propertyName.Equals(XpressMPSolverProperties.BarPrimalInf))
                {
                    return XpressMPSolver.ToMsfRational(this.BarPrimalInf);
                }
                if (propertyName.Equals(XpressMPSolverProperties.BarDualInf))
                {
                    return XpressMPSolver.ToMsfRational(this.BarDualInf);
                }
                if (propertyName.Equals(XpressMPSolverProperties.BarIter))
                {
                    return this.BarIter;
                }
                if (propertyName.Equals(XpressMPSolverProperties.BarAASize))
                {
                    return this.BarAASize;
                }
                if (propertyName.Equals(XpressMPSolverProperties.BarLSize))
                {
                    return this.BarLSize;
                }
                if (propertyName.Equals(XpressMPSolverProperties.BarDenseCol))
                {
                    return this.BarDenseCol;
                }
            }
            throw new MsfException(string.Format("Property '{0}' is not recognised or not supported by this version of the Xpress solver plugin", propertyName));
        }

        public int ActiveNodes
        {
            get
            {
                int activeNodes;
                try
                {
                    activeNodes = this.xprsprob.ActiveNodes;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return activeNodes;
            }
        }

        public int BarAASize
        {
            get
            {
                int barAASize;
                try
                {
                    barAASize = this.xprsprob.BarAASize;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return barAASize;
            }
        }

        public int BarDenseCol
        {
            get
            {
                int barDenseCol;
                try
                {
                    barDenseCol = this.xprsprob.BarDenseCol;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return barDenseCol;
            }
        }

        public double BarDualInf
        {
            get
            {
                double barDualInf;
                try
                {
                    barDualInf = this.xprsprob.BarDualInf;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return barDualInf;
            }
        }

        public double BarDualObj
        {
            get
            {
                double barDualObj;
                try
                {
                    barDualObj = this.xprsprob.BarDualObj;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return barDualObj;
            }
        }

        public int BarIter
        {
            get
            {
                int barIter;
                try
                {
                    barIter = this.xprsprob.BarIter;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return barIter;
            }
        }

        public int BarLSize
        {
            get
            {
                int barLSize;
                try
                {
                    barLSize = this.xprsprob.BarLSize;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return barLSize;
            }
        }

        public double BarPrimalInf
        {
            get
            {
                double barPrimalInf;
                try
                {
                    barPrimalInf = this.xprsprob.BarPrimalInf;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return barPrimalInf;
            }
        }

        public double BarPrimalObj
        {
            get
            {
                double barPrimalObj;
                try
                {
                    barPrimalObj = this.xprsprob.BarPrimalObj;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return barPrimalObj;
            }
        }

        public double BranchValue
        {
            get
            {
                double branchValue;
                try
                {
                    branchValue = this.xprsprob.BranchValue;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return branchValue;
            }
        }

        public int BranchVar
        {
            get
            {
                int branchVar;
                try
                {
                    branchVar = this.xprsprob.BranchVar;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return branchVar;
            }
        }

        public double CurrMipCutOff
        {
            get
            {
                double currMipCutOff;
                try
                {
                    currMipCutOff = this.xprsprob.CurrMipCutOff;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return currMipCutOff;
            }
        }

        public int Cuts
        {
            get
            {
                int cuts;
                try
                {
                    cuts = this.xprsprob.Cuts;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return cuts;
            }
        }

        public int DualInfeas
        {
            get
            {
                int dualInfeas;
                try
                {
                    dualInfeas = this.xprsprob.DualInfeas;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return dualInfeas;
            }
        }

        public long Elems
        {
            get
            {
                long num;
                try
                {
                    num = this.xprsprob.Elems;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return num;
            }
        }

        public int MIPEnts
        {
            get
            {
                int mIPEnts;
                try
                {
                    mIPEnts = this.xprsprob.MIPEnts;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return mIPEnts;
            }
        }

        public int MIPInfeas
        {
            get
            {
                int mIPInfeas;
                try
                {
                    mIPInfeas = this.xprsprob.MIPInfeas;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return mIPInfeas;
            }
        }

        public int MIPSolNode
        {
            get
            {
                int mIPSolNode;
                try
                {
                    mIPSolNode = this.xprsprob.MIPSolNode;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return mIPSolNode;
            }
        }

        public int MIPSols
        {
            get
            {
                int mIPSols;
                try
                {
                    mIPSols = this.xprsprob.MIPSols;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return mIPSols;
            }
        }

        public int NodeDepth
        {
            get
            {
                int nodeDepth;
                try
                {
                    nodeDepth = this.xprsprob.NodeDepth;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return nodeDepth;
            }
        }

        public int Nodes
        {
            get
            {
                int nodes;
                try
                {
                    nodes = this.xprsprob.Nodes;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return nodes;
            }
        }

        public int ParentNode
        {
            get
            {
                int parentNode;
                try
                {
                    parentNode = this.xprsprob.ParentNode;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return parentNode;
            }
        }

        public SolverFoundation.Plugin.XpressMP.PresolveState PresolveState
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.PresolveState presolveState;
                try
                {
                    presolveState = (SolverFoundation.Plugin.XpressMP.PresolveState) this.xprsprob.PresolveState;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return presolveState;
            }
        }

        public int PrimalInfeas
        {
            get
            {
                int primalInfeas;
                try
                {
                    primalInfeas = this.xprsprob.PrimalInfeas;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return primalInfeas;
            }
        }

        public int SimplexIter
        {
            get
            {
                int simplexIter;
                try
                {
                    simplexIter = this.xprsprob.SimplexIter;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return simplexIter;
            }
        }

        public int StopStatus
        {
            get
            {
                int stopStatus;
                try
                {
                    stopStatus = this.xprsprob.StopStatus;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return stopStatus;
            }
        }

        internal abstract XPRSprob xprsprob { get; }
    }
}

