﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public enum BacktrackAlg
    {
        BestBound = 3,
        BestEstimate = 2,
        BestEstimateWithMinimumInfeas = 10,
        DeepestBestEstimate = 11,
        DeepestNode = 4,
        EarliestNode = 6,
        HighestNode = 5,
        LatestNode = 7,
        MinimumInfeas = 9,
        Random = 8
    }
}

