﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public enum PrimalUnshift
    {
        AllowDualUnshift,
        NoDualUnshift
    }
}

