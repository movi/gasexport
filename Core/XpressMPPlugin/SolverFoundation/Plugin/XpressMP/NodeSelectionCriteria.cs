﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public enum NodeSelectionCriteria
    {
        BestFirst = 2,
        BestFirstThenLocalFirst = 4,
        DepthFirst = 5,
        LocalDepthFirst = 3,
        LocalFirst = 1
    }
}

