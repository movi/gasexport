﻿namespace SolverFoundation.Plugin.XpressMP
{
    using Microsoft.SolverFoundation.Common;
    using Microsoft.SolverFoundation.Services;
    using Optimizer;
    using System;
    using System.Collections.Generic;

    internal class XpressMPInfeasibilityReport : ILinearSolverInfeasibilityReport, ILinearSolverReport
    {
        private List<int> iisVids;

        public XpressMPInfeasibilityReport(XpressMPSolver solver)
        {
            try
            {
                this.iisVids = new List<int>();
                XPRSprob xpressProb = solver.XpressProb;
                xpressProb.FirstIIS(1);
                if (xpressProb.NumIIS != 0)
                {
                    int num;
                    int num2;
                    xpressProb.GetIISData(1, out num2, out num, null, null, null, null, null, null, null, null);
                    int[] miiscol = new int[num];
                    int[] miisrow = new int[num2];
                    xpressProb.GetIISData(1, out num2, out num, miisrow, miiscol, null, null, null, null, null, null);
                    foreach (int num3 in miisrow)
                    {
                        this.iisVids.Add(solver.VIDs.GetIndexFromRow(num3));
                    }
                    foreach (int num4 in miiscol)
                    {
                        this.iisVids.Add(solver.VIDs.GetIndexFromColumn(num4));
                    }
                }
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
        }

        public IEnumerable<int> IrreducibleInfeasibleSet
        {
            get
            {
                return this.iisVids;
            }
        }
    }
}

