﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public enum Presolve
    {
        Default = 1,
        KeepRedundantBounds = 2,
        None = 0,
        NoPrimalInfeasibility = -1
    }
}

