﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public enum BarPresolveOps
    {
        StandardPresolve,
        ExtraBarrierPresolve
    }
}

