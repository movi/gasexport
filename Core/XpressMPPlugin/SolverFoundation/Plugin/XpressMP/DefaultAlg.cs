﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public enum DefaultAlg
    {
        Automatic = 1,
        Barrier = 4,
        Default = 1,
        Dual = 2,
        DualSimplex = 2,
        NewtonBarrier = 4,
        Primal = 3,
        PrimalSimplex = 3
    }
}

