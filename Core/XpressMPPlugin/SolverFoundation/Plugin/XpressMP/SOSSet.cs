﻿namespace SolverFoundation.Plugin.XpressMP
{
    using Microsoft.SolverFoundation.Common;
    using Microsoft.SolverFoundation.Services;
    using Optimizer;
    using System;

    internal class SOSSet
    {
        private SolverFoundation.Plugin.XpressMP.Bounds originalSetRowBounds;
        private bool setCreated;
        private int setReferenceRowVID;
        private XpressMPSolver solver;
        private SpecialOrderedSetType type;

        public SOSSet(XpressMPSolver solver, int vid, SpecialOrderedSetType type)
        {
            this.solver = solver;
            this.setReferenceRowVID = vid;
            this.type = type;
            this.setCreated = false;
        }

        public void CreateSet()
        {
            try
            {
                XPRSprob xpressProb = this.solver.XpressProb;
                if (this.setCreated)
                {
                    throw new MsfException("SOS Set already created in XPRSprob!");
                }
                int rowIndex = this.solver.VIDs.GetRowIndex(this.setReferenceRowVID);
                this.originalSetRowBounds = new SolverFoundation.Plugin.XpressMP.Bounds(this.solver, this.setReferenceRowVID);
                int maxcoeffs = xpressProb.GetRows(null, null, null, 0, rowIndex, rowIndex);
                int[] numArray = new int[2];
                int[] numArray2 = new int[maxcoeffs];
                double[] numArray3 = new double[maxcoeffs];
                xpressProb.GetRows(numArray, numArray2, numArray3, maxcoeffs, rowIndex, rowIndex);
                if ((xpressProb.Version <= 0x7da) && (maxcoeffs > 1))
                {
                    SortSOSVariables(numArray2, numArray3, 0, maxcoeffs - 1);
                    for (int i = 1; i < maxcoeffs; i++)
                    {
                        if (numArray3[i - 1] >= numArray3[i])
                        {
                            throw new MsfException("Quicksort failed!");
                        }
                    }
                }
                xpressProb.AddSets(1, maxcoeffs, new char[] { (this.type == SpecialOrderedSetType.SOS1) ? '1' : '2' }, numArray, numArray2, numArray3);
                xpressProb.ChgRowType(1, new int[] { rowIndex }, new char[] { 'N' });
                this.setCreated = true;
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
        }

        public void SetUncreated()
        {
            try
            {
                if (!this.setCreated)
                {
                    throw new MsfException("SOS Set not created in XPRSprob!");
                }
                this.solver.VIDs.GetRowIndex(this.setReferenceRowVID);
                this.solver.SetBounds(this.setReferenceRowVID, this.originalSetRowBounds.lower, this.originalSetRowBounds.upper);
                this.setCreated = false;
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
        }

        private static void SortSOSVariables(int[] mclind, double[] dmatval, int start, int end)
        {
            if (start != end)
            {
                int num = start + ((end - start) / 2);
                SwapSOSVariables(mclind, dmatval, end, num);
                num = end;
                int num2 = start;
                for (int i = start; i < end; i++)
                {
                    if (dmatval[i] <= dmatval[num])
                    {
                        SwapSOSVariables(mclind, dmatval, i, num2);
                        num2++;
                    }
                }
                SwapSOSVariables(mclind, dmatval, num2, end);
                if (num2 > start)
                {
                    SortSOSVariables(mclind, dmatval, start, num2 - 1);
                }
                if (num2 < end)
                {
                    SortSOSVariables(mclind, dmatval, num2 + 1, end);
                }
            }
        }

        private static void SwapSOSVariables(int[] mclind, double[] dmatval, int i1, int i2)
        {
            double num = dmatval[i1];
            dmatval[i1] = dmatval[i2];
            dmatval[i2] = num;
            int num2 = mclind[i1];
            mclind[i1] = mclind[i2];
            mclind[i2] = num2;
        }

        public bool IsCreated
        {
            get
            {
                return this.setCreated;
            }
        }

        public SpecialOrderedSetType Type
        {
            get
            {
                return this.type;
            }
        }

        public int VID
        {
            get
            {
                return this.setReferenceRowVID;
            }
        }
    }
}

