﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public static class XpressMPSolveStates
    {
        public static readonly string ExploringMipNode = "Exploring a MIP node";
        public static readonly string FoundNewIncumbent = "Found New Incumbent";
        public static readonly string InMipSearch = "In MIP";
        public static readonly string InPresolve = "In Presolve";
        public static readonly string InSimplex = "In Simplex";
        public static readonly string NotSolving = "Not Solving";
        public static readonly string PrintingLogLine = "Printing a log message";
    }
}

