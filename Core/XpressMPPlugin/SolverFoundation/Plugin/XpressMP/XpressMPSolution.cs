﻿namespace SolverFoundation.Plugin.XpressMP
{
    using Microsoft.SolverFoundation.Common;
    using Microsoft.SolverFoundation.Services;
    using Optimizer;
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Runtime.InteropServices;

    internal class XpressMPSolution : ILinearSolution, ISolverSolution
    {
        private ILinearGoal goal;
        private bool isMipSol;
        private int lpIterCount;
        private double lpObjective;
        private LPStatus lpStatus;
        private double mipBestBound;
        private int mipNodeCount;
        private double mipObjective;
        private int mipSolNumber;
        private MIPStatus mipStatus;
        private XpressMPVariableSolution[] sol;
        private VIDManager vids;

        internal XpressMPSolution(XpressMPSolver solver) : this(solver, solver.XpressProb)
        {
        }

        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        internal XpressMPSolution(XpressMPSolver solver, XPRSprob prob)
        {
            if (solver.GoalCount == 0)
            {
                this.goal = null;
            }
            else
            {
                this.goal = solver.Goals.First<ILinearGoal>();
            }
            this.vids = new VIDManager(solver.VIDs);
            try
            {
                this.isMipSol = solver.IsMipModel && ((prob.MIPStatus == MIPStatus.Solution) || (prob.MIPStatus == MIPStatus.Optimal));
                this.mipStatus = prob.MIPStatus;
                this.lpStatus = prob.LPStatus;
                this.lpObjective = prob.LPObjVal;
                this.mipObjective = prob.MIPObjVal;
                this.mipBestBound = prob.BestBound;
                this.mipSolNumber = prob.MIPSols;
                this.mipNodeCount = prob.Nodes;
                this.lpIterCount = prob.SimplexIter + prob.BarIter;
                int originalCols = prob.OriginalCols;
                int originalRows = prob.OriginalRows;
                this.sol = new XpressMPVariableSolution[this.vids.LargestIndex + 1];
                int[] numArray = new int[originalRows];
                int[] numArray2 = new int[originalCols];
                bool flag = true;
                if (((prob.Version < 0x186a0) && (prob.QElems > 0)) && (prob.DefaultAlg != Optimizer.DefaultAlg.Primal))
                {
                    flag = false;
                }
                if (this.isMipSol)
                {
                    flag = false;
                }
                if (flag)
                {
                    prob.GetBasis(numArray, numArray2);
                }
                else
                {
                    for (int i = 0; i < originalRows; i++)
                    {
                        numArray[i] = 0;
                    }
                    for (int j = 0; j < originalCols; j++)
                    {
                        numArray2[j] = 0;
                    }
                }
                if (this.isMipSol && ((prob.MIPStatus == MIPStatus.NoSolutionFound) || (prob.MIPStatus == MIPStatus.Infeasible)))
                {
                    for (int k = 0; k < originalCols; k++)
                    {
                        int indexFromColumn = this.vids.GetIndexFromColumn(k);
                        this.sol[indexFromColumn].isSet = false;
                    }
                    for (int m = 0; m < originalRows; m++)
                    {
                        int indexFromRow = this.vids.GetIndexFromRow(m);
                        this.sol[indexFromRow].isSet = false;
                    }
                }
                else if (this.isMipSol)
                {
                    double[] numArray3 = new double[prob.OriginalCols];
                    prob.GetMipSol(numArray3);
                    for (int n = 0; n < originalCols; n++)
                    {
                        int index = this.vids.GetIndexFromColumn(n);
                        this.sol[index].isSet = true;
                        this.sol[index].value = numArray3[n];
                        this.sol[index].state = LinearValueState.Invalid;
                        this.sol[index].isBasic = numArray2[n] == 1;
                    }
                    for (int num11 = 0; num11 < originalRows; num11++)
                    {
                        int num12 = this.vids.GetIndexFromRow(num11);
                        this.sol[num12].isSet = false;
                    }
                    if (this.goal != null)
                    {
                        this.sol[this.goal.Index] = new XpressMPVariableSolution(this.mipObjective, LinearValueState.Between, false);
                    }
                }
                else if (prob.LPStatus == LPStatus.Optimal)
                {
                    double[] numArray4 = new double[prob.OriginalCols];
                    double[] numArray5 = new double[prob.OriginalRows];
                    prob.GetLpSol(numArray4, null, numArray5, null);
                    for (int num13 = 0; num13 < originalCols; num13++)
                    {
                        double num15;
                        double num16;
                        int vid = this.vids.GetIndexFromColumn(num13);
                        solver.GetBounds(vid, out num15, out num16);
                        this.sol[vid].isSet = true;
                        this.sol[vid].value = numArray4[num13];
                        this.sol[vid].state = CalculateLinearValueState(numArray4[num13], num15, num16);
                        this.sol[vid].isBasic = numArray2[num13] == 1;
                    }
                    for (int num17 = 0; num17 < originalRows; num17++)
                    {
                        double num19;
                        double num20;
                        int num18 = this.vids.GetIndexFromRow(num17);
                        solver.GetBounds(num18, out num19, out num20);
                        this.sol[num18].isSet = true;
                        this.sol[num18].value = numArray5[num17];
                        this.sol[num18].state = CalculateLinearValueState(numArray5[num17], num19, num20);
                        this.sol[num18].isBasic = numArray[num17] == 1;
                    }
                    if (this.goal != null)
                    {
                        this.sol[this.goal.Index] = new XpressMPVariableSolution(this.lpObjective, LinearValueState.Between, false);
                    }
                }
                else
                {
                    for (int num21 = 0; num21 < originalCols; num21++)
                    {
                        int num22 = this.vids.GetIndexFromColumn(num21);
                        this.sol[num22].isSet = false;
                    }
                    for (int num23 = 0; num23 < originalRows; num23++)
                    {
                        int num24 = this.vids.GetIndexFromRow(num23);
                        this.sol[num24].isSet = false;
                    }
                }
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
        }

        private static LinearValueState CalculateLinearValueState(double value, double lb, double ub)
        {
            if (value > ub)
            {
                return LinearValueState.Above;
            }
            if (value == ub)
            {
                return LinearValueState.AtUpper;
            }
            if (value < lb)
            {
                return LinearValueState.Below;
            }
            if (value == lb)
            {
                return LinearValueState.AtLower;
            }
            return LinearValueState.Between;
        }

        public bool GetBasic(int vid)
        {
            return (((vid < this.sol.Length) && this.sol[vid].isSet) && this.sol[vid].isBasic);
        }

        public Rational GetSolutionValue(int goalIndex)
        {
            return XpressMPSolver.ToMsfRational(this.isMipSol ? this.mipObjective : this.lpObjective);
        }

        public void GetSolvedGoal(int igoal, out object key, out int vid, out bool fMinimize, out bool fOptimal)
        {
            if (this.goal == null)
            {
                key = null;
                vid = -1;
                fMinimize = false;
                fOptimal = this.Result == LinearResult.Optimal;
            }
            else
            {
                vid = this.goal.Index;
                key = this.goal.Key;
                fMinimize = this.goal.Minimize;
                fOptimal = this.Result == LinearResult.Optimal;
            }
        }

        public Rational GetValue(int vid)
        {
            if ((vid < this.sol.Length) && this.sol[vid].isSet)
            {
                return XpressMPSolver.ToMsfRational(this.sol[vid].value);
            }
            return Rational.Indeterminate;
        }

        public LinearValueState GetValueState(int vid)
        {
            if ((vid < this.sol.Length) && this.sol[vid].isSet)
            {
                return this.sol[vid].state;
            }
            return LinearValueState.Invalid;
        }

        internal bool HasValue(int vid)
        {
            return ((vid < this.sol.Length) && this.sol[vid].isSet);
        }

        internal bool IsCurrentSolution(XpressMPSolver solver)
        {
            bool flag;
            try
            {
                XPRSprob xpressProb = solver.XpressProb;
                Console.WriteLine("old Nodes={0}", this.mipNodeCount);
                Console.WriteLine("old MIPSols={0}", this.mipSolNumber);
                Console.WriteLine("new Nodes={0}", xpressProb.Nodes);
                Console.WriteLine("new MIPSols={0}", xpressProb.MIPSols);
                if ((this.lpStatus != xpressProb.LPStatus) || (this.mipStatus != xpressProb.MIPStatus))
                {
                    return false;
                }
                if (this.mipStatus == MIPStatus.Optimal)
                {
                    return true;
                }
                if ((!this.isMipSol && !solver.IsMipModel) && (this.lpStatus == LPStatus.Optimal))
                {
                    return true;
                }
                if (this.isMipSol)
                {
                    return ((this.mipSolNumber == xpressProb.MIPSols) && (this.mipNodeCount == xpressProb.Nodes));
                }
                flag = this.lpIterCount == (xpressProb.SimplexIter + xpressProb.BarIter);
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
            return flag;
        }

        public LinearResult LpResult
        {
            get
            {
                return XpressMPSolver.XpressStatusToLinearResult(this.lpStatus);
            }
        }

        public Rational MipBestBound
        {
            get
            {
                return XpressMPSolver.ToMsfRational(this.mipBestBound);
            }
        }

        public LinearResult MipResult
        {
            get
            {
                return XpressMPSolver.XpressStatusToLinearResult(this.mipStatus);
            }
        }

        public LinearResult Result
        {
            get
            {
                if (this.isMipSol)
                {
                    return this.MipResult;
                }
                return this.LpResult;
            }
        }

        public LinearSolutionQuality SolutionQuality
        {
            get
            {
                return LinearSolutionQuality.Approximate;
            }
        }

        public int SolvedGoalCount
        {
            get
            {
                if ((this.goal != null) && (this.Result == LinearResult.Optimal))
                {
                    return 1;
                }
                return 0;
            }
        }
    }
}

