﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public enum PresolveDomColumn
    {
        Aggressive = 2,
        Automatic = -1,
        Cautious = 1,
        Disabled = 0
    }
}

