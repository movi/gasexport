﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public enum RepairIndefinateQuadratic: int
    {
        RepairIfPossible = 0,
        NoRepair = 1 
    }
}

