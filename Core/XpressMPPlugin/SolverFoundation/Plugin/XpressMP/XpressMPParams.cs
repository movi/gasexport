﻿namespace SolverFoundation.Plugin.XpressMP
{
    using Microsoft.SolverFoundation.Services;
    using Optimizer;
    using System;
    using System.IO;

    public class XpressMPParams : XpressMPSolveControlParameters, ISolverParameters, ISolverEvents
    {
        private bool debugFlag;
        private System.Action<XpressMPSolver, int> fnChangeNode;
        private System.Action<XpressMPSolver, ILinearSolution> fnIntSol;
        private System.Func<bool> fnQueryAbort;
        private System.Action fnSolveEvent;
        private bool generateSensitivityReport;
        private MessageCallback msgCbDelegate;
        private TextWriter outputLog;

        public XpressMPParams()
        {
            this.debugFlag = false;
            this.generateSensitivityReport = false;
            this.fnQueryAbort = null;
            this.outputLog = null;
            this.msgCbDelegate = new MessageCallback(this.msg_cb);
            this.fnSolveEvent = null;
        }

        public XpressMPParams(System.Func<bool> fnQueryAbort) : this()
        {
            this.fnQueryAbort = fnQueryAbort;
        }

        public XpressMPParams(Directive directive) : this()
        {
            XpressMPDirective directive2 = directive as XpressMPDirective;
            if (directive2 != null)
            {
                base.XpressProb.CopyControls(directive2.XpressParams.XpressProb);
                this.outputLog = directive2.OutputLog;
                this.debugFlag = directive2.Debug;
            }
            if (directive.Arithmetic == Arithmetic.Exact)
            {
                throw new NotSupportedException("Xpress solver only supports approximate arithmetic");
            }
            if (directive.TimeLimit >= 0)
            {
                base.MaxTime = -((directive.TimeLimit + 0x3e7) / 0x3e8);
            }
            SimplexDirective directive4 = directive as SimplexDirective;
            if (directive4 != null)
            {
                if (directive4.Algorithm == SimplexAlgorithm.Primal)
                {
                    base.DefaultAlg = SolverFoundation.Plugin.XpressMP.DefaultAlg.Primal;
                }
                else if (directive4.Algorithm == SimplexAlgorithm.Dual)
                {
                    base.DefaultAlg = SolverFoundation.Plugin.XpressMP.DefaultAlg.Dual;
                }
                switch (directive4.Basis)
                {
                    case SimplexBasis.Crash:
                        base.Crash = 2;
                        break;

                    case SimplexBasis.Slack:
                        base.Crash = 0;
                        break;

                    case SimplexBasis.Freedom:
                        base.Crash = 2;
                        break;
                }
                if (directive4.IterationLimit >= 0)
                {
                    base.LPIterLimit = directive4.IterationLimit;
                    base.BarIterLimit = directive4.IterationLimit;
                }
                switch (directive4.Pricing)
                {
                    case SimplexPricing.SteepestEdge:
                        base.PricingAlg = (SolverFoundation.Plugin.XpressMP.Pricing) 2;
                        break;

                    case SimplexPricing.ReducedCost:
                        base.PricingAlg = SolverFoundation.Plugin.XpressMP.Pricing.Devex;
                        break;

                    case SimplexPricing.Partial:
                        base.PricingAlg = SolverFoundation.Plugin.XpressMP.Pricing.Partial;
                        break;
                }
                if (directive4.PricingTolerance > 0.0)
                {
                    base.OptimalityTol = directive4.PricingTolerance;
                }
                if (directive4.VariableTolerance > 0.0)
                {
                    base.MatrixTol = directive4.VariableTolerance;
                }
                this.GetSensitivity = directive4.GetSensitivity;
            }
            MixedIntegerProgrammingDirective directive3 = directive as MixedIntegerProgrammingDirective;
            if (directive3 != null)
            {
                if (!directive3.CuttingPlaneGeneration)
                {
                    base.CutStrategy = SolverFoundation.Plugin.XpressMP.CutStrategy.None;
                }
                base.MIPAbsStop = directive3.GapTolerance;
                if (!directive3.LocalSearch)
                {
                    base.HeurSearchRootSelect = SolverFoundation.Plugin.XpressMP.HeuristicSearchSelect.LocalSearchLargeNeighbourhood;
                    base.HeurSearchTreeSelect = SolverFoundation.Plugin.XpressMP.HeuristicSearchSelect.LocalSearchLargeNeighbourhood;
                }
            }
        }

        public XpressMPParams(XpressMPParams prms) : this()
        {
            base.XpressProb.CopyControls(prms.XpressProb);
            this.fnQueryAbort = prms.QueryAbort;
            this.outputLog = prms.OutputLog;
        }

        private int AbortCallback(XPRSprob prob, object context)
        {
            XpressMPSolver solver = (XpressMPSolver) context;
            if (((solver.State != SolverState.Aborted) && (solver.State != SolverState.Aborting)) && ((this.fnQueryAbort == null) || !this.fnQueryAbort()))
            {
                return 0;
            }
            if (solver.State == SolverState.Solving)
            {
                solver.State = SolverState.Aborting;
            }
            prob.Interrupt(Optimizer.StopType.User);
            return 1;
        }

        internal void ApplyParameters(XpressMPSolver solver)
        {
            XPRSprob xpressProb = solver.XpressProb;
            xpressProb.RemoveBarlogCallbacks();
            xpressProb.RemoveChgbranchCallbacks();
            xpressProb.RemoveChgnodeCallbacks();
            xpressProb.RemoveCutlogCallbacks();
            xpressProb.RemoveCutmgrCallbacks();
            xpressProb.RemoveEstimateCallbacks();
            xpressProb.RemoveGloballogCallbacks();
            xpressProb.RemoveInfnodeCallbacks();
            xpressProb.RemoveIntsolCallbacks();
            xpressProb.RemoveLplogCallbacks();
            xpressProb.RemoveMipThreadCallbacks();
            xpressProb.RemoveMipThreadDestroyCallbacks();
            xpressProb.RemoveMsgHandlerCallbacks();
            xpressProb.RemoveNodecutoffCallbacks();
            xpressProb.RemoveOptnodeCallbacks();
            xpressProb.RemovePreIntsolCallbacks();
            xpressProb.RemovePrenodeCallbacks();
            xpressProb.RemoveSepnodeCallbacks();
            xpressProb.CopyControls(base.XpressProb);
            xpressProb.AddGloballogCallback(new GloballogCallback(this.AbortCallback), solver);
            xpressProb.AddLplogCallback(new LplogCallback(this.AbortCallback), solver);
            xpressProb.AddCutlogCallback(new CutlogCallback(this.AbortCallback), solver);
            xpressProb.AddBarlogCallback(new BarlogCallback(this.AbortCallback), solver);
            if (this.fnChangeNode != null)
            {
                xpressProb.AddChgnodeCallback(new ChgnodeCallback(this.ChangeNodeCallback), solver);
            }
            if (this.fnIntSol != null)
            {
                xpressProb.AddIntsolCallback(new IntsolCallback(this.IntSolCallback), solver);
            }
            if (this.outputLog != null)
            {
                xpressProb.MessageCallbacks += this.msgCbDelegate;
            }
            if (this.fnSolveEvent != null)
            {
                xpressProb.AddMessageCallback(new MessageCallback(this.SolveEvent_Message), solver);
                xpressProb.AddLplogCallback(new LplogCallback(this.SolveEvent_LpLog), solver);
                xpressProb.AddCutlogCallback(new CutlogCallback(this.SolveEvent_CutLog), solver);
                xpressProb.AddGloballogCallback(new GloballogCallback(this.SolveEvent_GlobalLog), solver);
                xpressProb.AddIntsolCallback(new IntsolCallback(this.SolveEvent_IntSol), solver);
                xpressProb.AddChgnodeCallback(new ChgnodeCallback(this.SolveEvent_ChgNode), solver);
            }
        }

        private void ChangeNodeCallback(XPRSprob prob, object vContext, ref int nodenum)
        {
            XpressMPSolver solver = (XpressMPSolver) vContext;
            if (this.fnChangeNode != null)
            {
                this.fnChangeNode(solver, nodenum);
            }
        }

        private void IntSolCallback(XPRSprob prob, object vContext)
        {
            XpressMPSolver solver = (XpressMPSolver) vContext;
            if (this.fnIntSol != null)
            {
                this.fnIntSol(solver, new XpressMPSolution(solver, prob));
            }
        }

        private void msg_cb(XPRSprob prob, object vContext, string msg, int len, int msgtype)
        {
            if (this.outputLog != null)
            {
                if (msg != null)
                {
                    this.outputLog.WriteLine(msg);
                }
                this.outputLog.Flush();
            }
        }

        internal void RemoveParameters(XpressMPSolver solver)
        {
            XPRSprob xpressProb = solver.XpressProb;
            xpressProb.RemoveMessageCallback(this.msgCbDelegate);
            xpressProb.RemoveBarlogCallbacks();
            xpressProb.RemoveChgbranchCallbacks();
            xpressProb.RemoveChgnodeCallbacks();
            xpressProb.RemoveCutlogCallbacks();
            xpressProb.RemoveCutmgrCallbacks();
            xpressProb.RemoveEstimateCallbacks();
            xpressProb.RemoveGloballogCallbacks();
            xpressProb.RemoveInfnodeCallbacks();
            xpressProb.RemoveIntsolCallbacks();
            xpressProb.RemoveLplogCallbacks();
            xpressProb.RemoveMipThreadCallbacks();
            xpressProb.RemoveMipThreadDestroyCallbacks();
            xpressProb.RemoveMsgHandlerCallbacks();
            xpressProb.RemoveNodecutoffCallbacks();
            xpressProb.RemoveOptnodeCallbacks();
            xpressProb.RemovePreIntsolCallbacks();
            xpressProb.RemovePrenodeCallbacks();
            xpressProb.RemoveSepnodeCallbacks();
        }

        private void SolveEvent_ChgNode(XPRSprob prob, object ctx, ref int nodnum)
        {
            ((XpressMPSolver) ctx).SetSolveState(XpressMPSolveStates.ExploringMipNode);
            this.fnSolveEvent();
        }

        private int SolveEvent_CutLog(XPRSprob prob, object ctx)
        {
            ((XpressMPSolver) ctx).SetSolveState(XpressMPSolveStates.InMipSearch);
            this.fnSolveEvent();
            return 0;
        }

        private int SolveEvent_GlobalLog(XPRSprob prob, object ctx)
        {
            ((XpressMPSolver) ctx).SetSolveState(XpressMPSolveStates.InMipSearch);
            this.fnSolveEvent();
            return 0;
        }

        private void SolveEvent_IntSol(XPRSprob prob, object ctx)
        {
            ((XpressMPSolver) ctx).SetSolveState(XpressMPSolveStates.FoundNewIncumbent);
            this.fnSolveEvent();
        }

        private int SolveEvent_LpLog(XPRSprob prob, object ctx)
        {
            ((XpressMPSolver) ctx).SetSolveState(XpressMPSolveStates.InSimplex);
            this.fnSolveEvent();
            return 0;
        }

        private void SolveEvent_Message(XPRSprob prob, object ctx, string msg, int len, int type)
        {
            ((XpressMPSolver) ctx).SetSolveState(XpressMPSolveStates.PrintingLogLine);
            this.fnSolveEvent();
        }

        public bool Debug
        {
            get
            {
                return this.debugFlag;
            }
            set
            {
                this.debugFlag = value;
            }
        }

        public bool GetSensitivity
        {
            get
            {
                return this.generateSensitivityReport;
            }
            set
            {
                this.generateSensitivityReport = value;
            }
        }

        public System.Action<XpressMPSolver, int> NotifyChangeNode
        {
            get
            {
                return this.fnChangeNode;
            }
            set
            {
                this.fnChangeNode = value;
            }
        }

        public System.Action<XpressMPSolver, ILinearSolution> NotifyNewIntegerSolution
        {
            get
            {
                return this.fnIntSol;
            }
            set
            {
                this.fnIntSol = value;
            }
        }

        public TextWriter OutputLog
        {
            get
            {
                return this.outputLog;
            }
            set
            {
                this.outputLog = value;
            }
        }

        public System.Func<bool> QueryAbort
        {
            get
            {
                return this.fnQueryAbort;
            }
            set
            {
                this.fnQueryAbort = value;
            }
        }

        public System.Action Solving
        {
            get
            {
                return this.fnSolveEvent;
            }
            set
            {
                this.fnSolveEvent = value;
            }
        }
    }
}

