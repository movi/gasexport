﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public enum DualStrategy
    {
        RemoveInfeasibilitiesWithPrimal,
        RemoveInfeasibilitiesWithDual
    }
}

