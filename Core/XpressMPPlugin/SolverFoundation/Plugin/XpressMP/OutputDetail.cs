﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public enum OutputDetail
    {
        NoOutput,
        FullOutput,
        ErrorsAndWarnings,
        ErrorsOnly
    }
}

