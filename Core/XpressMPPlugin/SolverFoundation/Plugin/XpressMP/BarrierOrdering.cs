﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public enum BarrierOrdering
    {
        Default,
        MinDegree,
        MinLocalFill,
        NestedDissection
    }
}

