﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    [Flags]
    public enum TreeDiagnostics
    {
        MemorySavedReports = 2,
        MemoryUsageSummaries = 1
    }
}

