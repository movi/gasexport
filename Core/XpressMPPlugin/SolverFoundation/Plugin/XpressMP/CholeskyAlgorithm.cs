﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public enum CholeskyAlgorithm
    {
        PullCholesky,
        PushCholesky
    }
}

