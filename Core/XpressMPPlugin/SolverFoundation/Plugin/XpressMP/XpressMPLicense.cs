﻿namespace SolverFoundation.Plugin.XpressMP
{
    using Microsoft.SolverFoundation.Common;
    using Optimizer;
    using System;

    public sealed class XpressMPLicense : IDisposable
    {
        private static WeakReference licRef = null;
        private static object licSync = new object();
        private bool revoked = false;

        private XpressMPLicense()
        {
            try
            {
                this.revoked = false;
                int dcode = 0;
                string licmsg = "";
                XPRS.License(ref dcode, ref licmsg);
                dcode = 0x5390531 - ((dcode * dcode) / 0x13);
                XPRS.License(ref dcode, ref licmsg);
                XPRS.Init("");
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
        }

        public void Dispose()
        {
            lock (licSync)
            {
                if (!this.revoked)
                {
                    this.revoked = true;
                    licRef = null;
                    XPRS.Free();
                    GC.SuppressFinalize(this);
                }
            }
        }

        ~XpressMPLicense()
        {
            try
            {
                this.Dispose();
            }
            catch (Exception)
            {
            }
        }

        internal static XpressMPLicense GetLicense()
        {
            lock (licSync)
            {
                XpressMPLicense target = null;
                if (licRef != null)
                {
                    target = (XpressMPLicense) licRef.Target;
                }
                if (target == null)
                {
                    target = new XpressMPLicense();
                    licRef = new WeakReference(target);
                }
                return target;
            }
        }

        public bool IsRevoked
        {
            get
            {
                return this.revoked;
            }
        }

        public static void ReleaseXpress()
        {
            if (licRef != null && licRef.Target is XpressMPLicense)
                ((XpressMPLicense) licRef.Target).Dispose();
        }
    }
}

