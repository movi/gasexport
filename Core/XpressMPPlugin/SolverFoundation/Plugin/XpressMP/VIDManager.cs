﻿namespace SolverFoundation.Plugin.XpressMP
{
    using Microsoft.SolverFoundation.Common;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    internal class VIDManager
    {
        private VIDManager cloneOf;
        private List<VIDManager> clones;
        private Dictionary<int, int> columnIndexToVID;
        private IEqualityComparer<object> keyComparer;
        private Dictionary<object, int> keyToVID;
        private int largestVID;
        private Dictionary<int, int> rowIndexToVID;
        private VID[] vids;

        internal VIDManager(IEqualityComparer<object> cmp)
        {
            this.vids = new VID[0x80];
            for (int i = 0; i < this.vids.Length; i++)
            {
                this.vids[i].type = VIDType.UNUSED;
            }
            this.largestVID = -1;
            this.keyToVID = null;
            this.rowIndexToVID = null;
            this.columnIndexToVID = null;
            this.cloneOf = null;
            this.clones = new List<VIDManager>();
            this.keyComparer = cmp;
        }

        internal VIDManager(VIDManager other)
        {
            this.vids = null;
            this.cloneOf = other.Concrete;
            this.cloneOf.clones.Add(this);
            this.keyToVID = null;
            this.rowIndexToVID = null;
            this.columnIndexToVID = null;
            this.clones = null;
            this.keyComparer = null;
        }

        internal int AddColumnVID(object key, int colnum)
        {
            return this.AddVID(key, VIDType.COLUMN, colnum);
        }

        internal int AddRowVID(object key, int rownum)
        {
            return this.AddVID(key, VIDType.ROW, rownum);
        }

        internal int AddVID(object key, VIDType type, int modelIndex)
        {
            int length;
            if (this.IsClone || (this.clones.Count > 0))
            {
                if (this.IsClone)
                {
                    this.DeClone();
                }
                foreach (VIDManager manager in this.clones)
                {
                    manager.DeClone();
                }
                if (this.clones.Count > 0)
                {
                    throw new MsfException("INTERNAL ERROR: clones.Count should be 0 after de-cloning all clones!");
                }
            }
            if (this.largestVID == (this.vids.Length - 1))
            {
                length = this.vids.Length;
                int num2 = 0x80;
                if (this.vids.Length > 0x186a0)
                {
                    num2 = 0x61a8;
                }
                else if (this.vids.Length > 0x61a8)
                {
                    num2 = 0x1388;
                }
                else if (this.vids.Length > 0x3e8)
                {
                    num2 = 0x400;
                }
                VID[] array = new VID[this.vids.Length + num2];
                this.vids.CopyTo(array, 0);
                for (int i = this.vids.Length + 1; i < array.Length; i++)
                {
                    array[i].type = VIDType.UNUSED;
                }
                this.vids = array;
            }
            else
            {
                length = this.largestVID + 1;
                while ((length > 0) && (this.vids[length - 1].type == VIDType.UNUSED))
                {
                    length--;
                }
            }
            this.vids[length].type = type;
            this.vids[length].key = key;
            this.vids[length].modelIndex = modelIndex;
            this.largestVID = length;
            if ((this.columnIndexToVID != null) && (type == VIDType.COLUMN))
            {
                this.columnIndexToVID[modelIndex] = length;
            }
            if ((this.rowIndexToVID != null) && (type == VIDType.ROW))
            {
                this.rowIndexToVID[modelIndex] = length;
            }
            if (this.keyToVID != null)
            {
                this.keyToVID[key] = length;
            }
            return length;
        }

        private void DeClone()
        {
            this.vids = new VID[this.cloneOf.vids.Length];
            for (int i = 0; i < this.vids.Length; i++)
            {
                this.vids[i] = this.cloneOf.vids[i];
            }
            this.keyToVID = null;
            this.rowIndexToVID = null;
            this.columnIndexToVID = null;
            this.clones = new List<VIDManager>();
            this.keyComparer = this.cloneOf.KeyComparer;
            this.cloneOf.clones.Remove(this);
            this.cloneOf = null;
        }

        public int GetColumnIndex(int vid)
        {
            return this.VIDs[vid].modelIndex;
        }

        public int GetIndexFromColumn(int colnum)
        {
            return this.ColumnIndexToVID[colnum];
        }

        public int GetIndexFromKey(object key)
        {
            return this.KeyToVID[key];
        }

        public int GetIndexFromRow(int rownum)
        {
            return this.RowIndexToVID[rownum];
        }

        public object GetKeyFromIndex(int vid)
        {
            return this.VIDs[vid].key;
        }

        public int GetRowIndex(int vid)
        {
            return this.VIDs[vid].modelIndex;
        }

        public bool IsColumn(int vid)
        {
            return (this.VIDs[vid].type == VIDType.COLUMN);
        }

        public bool IsRow(int vid)
        {
            return (this.VIDs[vid].type == VIDType.ROW);
        }

        public bool TryGetIndexFromKey(object key, out int vid)
        {
            return this.KeyToVID.TryGetValue(key, out vid);
        }

        private Dictionary<int, int> ColumnIndexToVID
        {
            get
            {
                if (this.IsClone)
                {
                    return this.cloneOf.ColumnIndexToVID;
                }
                if (this.columnIndexToVID == null)
                {
                    Dictionary<int, int> dictionary = new Dictionary<int, int>();
                    for (int i = 0; i < this.vids.Length; i++)
                    {
                        if (this.vids[i].type == VIDType.COLUMN)
                        {
                            dictionary[this.vids[i].modelIndex] = i;
                        }
                    }
                    this.columnIndexToVID = dictionary;
                }
                return this.columnIndexToVID;
            }
        }

        private VIDManager Concrete
        {
            get
            {
                if (this.IsClone)
                {
                    return this.cloneOf.Concrete;
                }
                return this;
            }
        }

        public IEnumerable<int> Indices
        {
            get
            {
                List<int> list = new List<int>(this.VIDs.Length);
                for (int i = 0; i < this.VIDs.Length; i++)
                {
                    if (this.VIDs[i].type != VIDType.UNUSED)
                    {
                        list.Add(i);
                    }
                }
                return list;
            }
        }

        private bool IsClone
        {
            get
            {
                return (this.cloneOf != null);
            }
        }

        public IEqualityComparer<object> KeyComparer
        {
            get
            {
                return this.keyComparer;
            }
        }

        public int KeyCount
        {
            get
            {
                return this.KeyToVID.Count;
            }
        }

        public IEnumerable<object> Keys
        {
            get
            {
                List<object> list = new List<object>(this.KeyCount);
                for (int i = 0; i < this.VIDs.Length; i++)
                {
                    if (this.VIDs[i].type != VIDType.UNUSED)
                    {
                        list.Add(this.VIDs[i].key);
                    }
                }
                return list;
            }
        }

        private Dictionary<object, int> KeyToVID
        {
            get
            {
                if (this.IsClone)
                {
                    return this.cloneOf.KeyToVID;
                }
                if (this.keyToVID == null)
                {
                    Dictionary<object, int> dictionary = new Dictionary<object, int>(this.KeyComparer);
                    for (int i = 0; i < this.vids.Length; i++)
                    {
                        if (this.vids[i].type != VIDType.UNUSED)
                        {
                            dictionary[this.vids[i].key] = i;
                        }
                    }
                    this.keyToVID = dictionary;
                }
                return this.keyToVID;
            }
        }

        public int LargestIndex
        {
            get
            {
                for (int i = this.VIDs.Length - 1; i >= 0; i--)
                {
                    if (this.VIDs[i].type != VIDType.UNUSED)
                    {
                        return i;
                    }
                }
                return -1;
            }
        }

        public int RowCount
        {
            get
            {
                int num = 0;
                for (int i = 0; i < this.VIDs.Length; i++)
                {
                    if (this.VIDs[i].type == VIDType.ROW)
                    {
                        num++;
                    }
                }
                return num;
            }
        }

        private Dictionary<int, int> RowIndexToVID
        {
            get
            {
                if (this.IsClone)
                {
                    return this.cloneOf.RowIndexToVID;
                }
                if (this.rowIndexToVID == null)
                {
                    Dictionary<int, int> dictionary = new Dictionary<int, int>();
                    for (int i = 0; i < this.vids.Length; i++)
                    {
                        if (this.vids[i].type == VIDType.ROW)
                        {
                            dictionary[this.vids[i].modelIndex] = i;
                        }
                    }
                    this.rowIndexToVID = dictionary;
                }
                return this.rowIndexToVID;
            }
        }

        public IEnumerable<int> RowIndices
        {
            get
            {
                List<int> list = new List<int>(this.RowCount);
                for (int i = 0; i < this.VIDs.Length; i++)
                {
                    if (this.VIDs[i].type == VIDType.ROW)
                    {
                        list.Add(i);
                    }
                }
                return list;
            }
        }

        public IEnumerable<object> RowKeys
        {
            get
            {
                List<object> list = new List<object>(this.RowCount);
                for (int i = 0; i < this.VIDs.Length; i++)
                {
                    if (this.VIDs[i].type == VIDType.ROW)
                    {
                        list.Add(this.VIDs[i].key);
                    }
                }
                return list;
            }
        }

        public int VariableCount
        {
            get
            {
                int num = 0;
                for (int i = 0; i < this.VIDs.Length; i++)
                {
                    if (this.VIDs[i].type == VIDType.COLUMN)
                    {
                        num++;
                    }
                }
                return num;
            }
        }

        public IEnumerable<int> VariableIndices
        {
            get
            {
                List<int> list = new List<int>(this.VariableCount);
                for (int i = 0; i < this.VIDs.Length; i++)
                {
                    if (this.VIDs[i].type == VIDType.COLUMN)
                    {
                        list.Add(i);
                    }
                }
                return list;
            }
        }

        public IEnumerable<object> VariableKeys
        {
            get
            {
                List<object> list = new List<object>(this.VariableCount);
                for (int i = 0; i < this.VIDs.Length; i++)
                {
                    if (this.VIDs[i].type == VIDType.COLUMN)
                    {
                        list.Add(this.VIDs[i].key);
                    }
                }
                return list;
            }
        }

        private VID[] VIDs
        {
            get
            {
                if (this.IsClone)
                {
                    return this.cloneOf.VIDs;
                }
                return this.vids;
            }
        }
    }
}

