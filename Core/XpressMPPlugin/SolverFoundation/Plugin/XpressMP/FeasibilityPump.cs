﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public enum FeasibilityPump
    {
        Never,
        Always,
        LastResort
    }
}

