﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public enum PresolveDomRow
    {
        Aggressive = 3,
        Automatic = -1,
        Cautious = 1,
        Disabled = 0,
        Medium = 2
    }
}

