﻿namespace SolverFoundation.Plugin.XpressMP
{
    using Microsoft.SolverFoundation.Common;
    using Optimizer;
    using System;

    public abstract class XpressMPSolveControlParameters
    {
        private int __Reflexil_BarStart;
        private int __Reflexil_Trace;
        private XpressMPLicense license;
        private XPRSprob xpprob;

        /// <summary>
        /// Недостающая во враппере Dash 2009 константа
        /// </summary>
        private const int XPRS_BARSTART = 8180;

        internal XpressMPSolveControlParameters()
        {
            try
            {
                this.license = XpressMPLicense.GetLicense();
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
            this.xpprob = new XPRSprob();
        }

        public bool AutoPerturb
        {
            get
            {
                bool flag;
                try
                {
                    flag = this.xpprob.AutoPerturb != 0;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return flag;
            }
            set
            {
                try
                {
                    this.xpprob.AutoPerturb = value ? 1 : 0;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.BacktrackAlg BackTrack
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.BacktrackAlg backTrack;
                try
                {
                    backTrack = (SolverFoundation.Plugin.XpressMP.BacktrackAlg) this.xpprob.BackTrack;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return backTrack;
            }
            set
            {
                try
                {
                    this.xpprob.BackTrack = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.BacktrackAlg BacktrackTie
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.BacktrackAlg backtrackTie;
                try
                {
                    backtrackTie = (SolverFoundation.Plugin.XpressMP.BacktrackAlg) this.xpprob.BacktrackTie;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return backtrackTie;
            }
            set
            {
                try
                {
                    this.xpprob.BacktrackTie = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int BarCrash
        {
            get
            {
                int barCrash;
                try
                {
                    barCrash = this.xpprob.BarCrash;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return barCrash;
            }
            set
            {
                try
                {
                    this.xpprob.BarCrash = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double BarDualStop
        {
            get
            {
                double barDualStop;
                try
                {
                    barDualStop = this.xpprob.BarDualStop;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return barDualStop;
            }
            set
            {
                try
                {
                    this.xpprob.BarDualStop = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double BarGapStop
        {
            get
            {
                double barGapStop;
                try
                {
                    barGapStop = this.xpprob.BarGapStop;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return barGapStop;
            }
            set
            {
                try
                {
                    this.xpprob.BarGapStop = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int BarIndefLimit
        {
            get
            {
                int barIndefLimit;
                try
                {
                    barIndefLimit = this.xpprob.BarIndefLimit;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return barIndefLimit;
            }
            set
            {
                try
                {
                    this.xpprob.BarIndefLimit = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int BarIterLimit
        {
            get
            {
                int barIterLimit;
                try
                {
                    barIterLimit = this.xpprob.BarIterLimit;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return barIterLimit;
            }
            set
            {
                try
                {
                    this.xpprob.BarIterLimit = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public BarrierOrdering BarOrder
        {
            get
            {
                BarrierOrdering barOrder;
                try
                {
                    barOrder = (BarrierOrdering) this.xpprob.BarOrder;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return barOrder;
            }
            set
            {
                try
                {
                    this.xpprob.BarOrder = (Optimizer.BarOrder) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.BarPresolveOps BarPresolveOps
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.BarPresolveOps barPresolveOps;
                try
                {
                    barPresolveOps = (SolverFoundation.Plugin.XpressMP.BarPresolveOps) this.xpprob.BarPresolveOps;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return barPresolveOps;
            }
            set
            {
                try
                {
                    this.xpprob.BarPresolveOps = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double BarPrimalStop
        {
            get
            {
                double barPrimalStop;
                try
                {
                    barPrimalStop = this.xpprob.BarPrimalStop;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return barPrimalStop;
            }
            set
            {
                try
                {
                    this.xpprob.BarPrimalStop = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int BarStart
        {
            get
            {
                int barStart;
                try
                {
                    barStart = this.xpprob.GetIntControl(XPRS_BARSTART);
                    //barStart = this.xpprob.BarStart;
                }
                catch (Exception exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return barStart;
            }
            set
            {
                try
                {
                    this.xpprob.SetIntControl(XPRS_BARSTART, value);                        
                    //BarStart = value;
                }
                catch (Exception exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double BarStepStop
        {
            get
            {
                double barStepStop;
                try
                {
                    barStepStop = this.xpprob.BarStepStop;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return barStepStop;
            }
            set
            {
                try
                {
                    this.xpprob.BarStepStop = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int BarThreads
        {
            get
            {
                int barThreads;
                try
                {
                    barThreads = this.xpprob.BarThreads;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return barThreads;
            }
            set
            {
                try
                {
                    this.xpprob.BarThreads = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double BigM
        {
            get
            {
                double bigM;
                try
                {
                    bigM = this.xpprob.BigM;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return bigM;
            }
            set
            {
                try
                {
                    this.xpprob.BigM = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.BranchChoice BranchChoice
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.BranchChoice branchChoice;
                try
                {
                    branchChoice = (SolverFoundation.Plugin.XpressMP.BranchChoice) this.xpprob.BranchChoice;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return branchChoice;
            }
            set
            {
                try
                {
                    this.xpprob.BranchChoice = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.AlwaysNeverAutomatic BranchStructural
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.AlwaysNeverAutomatic branchStructural;
                try
                {
                    branchStructural = (SolverFoundation.Plugin.XpressMP.AlwaysNeverAutomatic) this.xpprob.BranchStructural;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return branchStructural;
            }
            set
            {
                try
                {
                    this.xpprob.BranchStructural = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int BreadthFirst
        {
            get
            {
                int breadthFirst;
                try
                {
                    breadthFirst = this.xpprob.BreadthFirst;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return breadthFirst;
            }
            set
            {
                try
                {
                    this.xpprob.BreadthFirst = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int CacheSize
        {
            get
            {
                int cacheSize;
                try
                {
                    cacheSize = this.xpprob.CacheSize;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return cacheSize;
            }
            set
            {
                try
                {
                    this.xpprob.CacheSize = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.CholeskyAlgorithm CholeskyAlg
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.CholeskyAlgorithm choleskyAlg;
                try
                {
                    choleskyAlg = (SolverFoundation.Plugin.XpressMP.CholeskyAlgorithm) this.xpprob.CholeskyAlg;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return choleskyAlg;
            }
            set
            {
                try
                {
                    this.xpprob.CholeskyAlg = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double CholeskyTol
        {
            get
            {
                double choleskyTol;
                try
                {
                    choleskyTol = this.xpprob.CholeskyTol;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return choleskyTol;
            }
            set
            {
                try
                {
                    this.xpprob.CholeskyTol = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int CoverCuts
        {
            get
            {
                int coverCuts;
                try
                {
                    coverCuts = this.xpprob.CoverCuts;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return coverCuts;
            }
            set
            {
                try
                {
                    this.xpprob.CoverCuts = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int Crash
        {
            get
            {
                int crash;
                try
                {
                    crash = this.xpprob.Crash;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return crash;
            }
            set
            {
                try
                {
                    this.xpprob.Crash = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.AlwaysNeverAutomatic CrossOver
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.AlwaysNeverAutomatic crossOver;
                try
                {
                    crossOver = (SolverFoundation.Plugin.XpressMP.AlwaysNeverAutomatic) this.xpprob.CrossOver;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return crossOver;
            }
            set
            {
                try
                {
                    this.xpprob.CrossOver = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.CrossoverDynamicReduction CrossOverDRP
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.CrossoverDynamicReduction crossOverDRP;
                try
                {
                    crossOverDRP = (SolverFoundation.Plugin.XpressMP.CrossoverDynamicReduction) this.xpprob.CrossOverDRP;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return crossOverDRP;
            }
            set
            {
                try
                {
                    this.xpprob.CrossOverDRP = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int CutDepth
        {
            get
            {
                int cutDepth;
                try
                {
                    cutDepth = this.xpprob.CutDepth;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return cutDepth;
            }
            set
            {
                try
                {
                    this.xpprob.CutDepth = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double CutFactor
        {
            get
            {
                double cutFactor;
                try
                {
                    cutFactor = this.xpprob.CutFactor;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return cutFactor;
            }
            set
            {
                try
                {
                    this.xpprob.CutFactor = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int CutFreq
        {
            get
            {
                int cutFreq;
                try
                {
                    cutFreq = this.xpprob.CutFreq;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return cutFreq;
            }
            set
            {
                try
                {
                    this.xpprob.CutFreq = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.CutSelect CutSelect
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.CutSelect cutSelect;
                try
                {
                    cutSelect = (SolverFoundation.Plugin.XpressMP.CutSelect) this.xpprob.CutSelect;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return cutSelect;
            }
            set
            {
                try
                {
                    this.xpprob.CutSelect = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.CutStrategy CutStrategy
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.CutStrategy cutStrategy;
                try
                {
                    cutStrategy = (SolverFoundation.Plugin.XpressMP.CutStrategy) this.xpprob.CutStrategy;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return cutStrategy;
            }
            set
            {
                try
                {
                    this.xpprob.CutStrategy = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.DefaultAlg DefaultAlg
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.DefaultAlg defaultAlg;
                try
                {
                    defaultAlg = (SolverFoundation.Plugin.XpressMP.DefaultAlg) this.xpprob.DefaultAlg;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return defaultAlg;
            }
            set
            {
                try
                {
                    this.xpprob.DefaultAlg = (Optimizer.DefaultAlg) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double DegradeFactor
        {
            get
            {
                double num;
                try
                {
                    num = 1;//num = this.xpprob.get_DegradeFactor();
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return num;
            }
            set
            {
                try
                {
                    //this.xpprob.set_DegradeFactor(value);
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int DenseColLimit
        {
            get
            {
                int denseColLimit;
                try
                {
                    denseColLimit = this.xpprob.DenseColLimit;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return denseColLimit;
            }
            set
            {
                try
                {
                    this.xpprob.DenseColLimit = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public bool Deterministic
        {
            get
            {
                bool flag;
                try
                {
                    flag = this.xpprob.Deterministic != 0;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return flag;
            }
            set
            {
                try
                {
                    this.xpprob.Deterministic = value ? 1 : 0;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.DualGradient DualGradient
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.DualGradient dualGradient;
                try
                {
                    dualGradient = (SolverFoundation.Plugin.XpressMP.DualGradient) this.xpprob.DualGradient;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return dualGradient;
            }
            set
            {
                try
                {
                    this.xpprob.DualGradient = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.AlwaysNeverAutomatic Dualize
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.AlwaysNeverAutomatic dualize;
                try
                {
                    dualize = (SolverFoundation.Plugin.XpressMP.AlwaysNeverAutomatic) this.xpprob.Dualize;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return dualize;
            }
            set
            {
                try
                {
                    this.xpprob.Dualize = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.DualStrategy DualStrategy
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.DualStrategy dualStrategy;
                try
                {
                    dualStrategy = (SolverFoundation.Plugin.XpressMP.DualStrategy) this.xpprob.DualStrategy;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return dualStrategy;
            }
            set
            {
                try
                {
                    this.xpprob.DualStrategy = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double EigenValueTol
        {
            get
            {
                double eigenValueTol;
                try
                {
                    eigenValueTol = this.xpprob.EigenValueTol;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return eigenValueTol;
            }
            set
            {
                try
                {
                    this.xpprob.EigenValueTol = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double ElimTol
        {
            get
            {
                double elimTol;
                try
                {
                    elimTol = this.xpprob.ElimTol;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return elimTol;
            }
            set
            {
                try
                {
                    this.xpprob.ElimTol = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double EtaTol
        {
            get
            {
                double etaTol;
                try
                {
                    etaTol = this.xpprob.EtaTol;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return etaTol;
            }
            set
            {
                try
                {
                    this.xpprob.EtaTol = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int ExtraCols
        {
            get
            {
                int extraCols;
                try
                {
                    extraCols = this.xpprob.ExtraCols;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return extraCols;
            }
            set
            {
                try
                {
                    this.xpprob.ExtraCols = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public long ExtraElems
        {
            get
            {
                long num;
                try
                {
                    num = this.xpprob.ExtraElems;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return num;
            }
            set
            {
                try
                {
                    this.xpprob.ExtraElems = (int)value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int ExtraMIPEnts
        {
            get
            {
                int extraMIPEnts;
                try
                {
                    extraMIPEnts = this.xpprob.ExtraMIPEnts;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return extraMIPEnts;
            }
            set
            {
                try
                {
                    this.xpprob.ExtraMIPEnts = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public long ExtraPresolve
        {
            get
            {
                long num;
                try
                {
                    num = this.xpprob.ExtraPresolve;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return num;
            }
            set
            {
                try
                {
                    this.xpprob.ExtraPresolve = (int)value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int ExtraRows
        {
            get
            {
                int extraRows;
                try
                {
                    extraRows = this.xpprob.ExtraRows;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return extraRows;
            }
            set
            {
                try
                {
                    this.xpprob.ExtraRows = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public long ExtraSetElems
        {
            get
            {
                long num;
                try
                {
                    num = this.xpprob.ExtraSetElems;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return num;
            }
            set
            {
                try
                {
                    this.xpprob.ExtraSetElems = (int)value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int ExtraSets
        {
            get
            {
                int extraSets;
                try
                {
                    extraSets = this.xpprob.ExtraSets;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return extraSets;
            }
            set
            {
                try
                {
                    this.xpprob.ExtraSets = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.FeasibilityPump FeasibilityPump
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.FeasibilityPump feasibilityPump;
                try
                {
                    feasibilityPump = (SolverFoundation.Plugin.XpressMP.FeasibilityPump) this.xpprob.FeasibilityPump;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return feasibilityPump;
            }
            set
            {
                try
                {
                    this.xpprob.FeasibilityPump = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double FeasTol
        {
            get
            {
                double feasTol;
                try
                {
                    feasTol = this.xpprob.FeasTol;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return feasTol;
            }
            set
            {
                try
                {
                    this.xpprob.FeasTol = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double GlobalFileBias
        {
            get
            {
                double globalFileBias;
                try
                {
                    globalFileBias = this.xpprob.GlobalFileBias;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return globalFileBias;
            }
            set
            {
                try
                {
                    this.xpprob.GlobalFileBias = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int GomCuts
        {
            get
            {
                int gomCuts;
                try
                {
                    gomCuts = this.xpprob.GomCuts;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return gomCuts;
            }
            set
            {
                try
                {
                    this.xpprob.GomCuts = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int HeurDepth
        {
            get
            {
                int heurDepth;
                try
                {
                    heurDepth = this.xpprob.HeurDepth;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return heurDepth;
            }
            set
            {
                try
                {
                    this.xpprob.HeurDepth = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int HeurDiveSpeedUp
        {
            get
            {
                int heurDiveSpeedUp;
                try
                {
                    heurDiveSpeedUp = this.xpprob.HeurDiveSpeedUp;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return heurDiveSpeedUp;
            }
            set
            {
                try
                {
                    this.xpprob.HeurDiveSpeedUp = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int HeurDiveStrategy
        {
            get
            {
                int heurDiveStrategy;
                try
                {
                    heurDiveStrategy = this.xpprob.HeurDiveStrategy;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return heurDiveStrategy;
            }
            set
            {
                try
                {
                    this.xpprob.HeurDiveStrategy = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int HeurFreq
        {
            get
            {
                int heurFreq;
                try
                {
                    heurFreq = this.xpprob.HeurFreq;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return heurFreq;
            }
            set
            {
                try
                {
                    this.xpprob.HeurFreq = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int HeurMaxSol
        {
            get
            {
                int heurMaxSol;
                try
                {
                    heurMaxSol = this.xpprob.HeurMaxSol;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return heurMaxSol;
            }
            set
            {
                try
                {
                    this.xpprob.HeurMaxSol = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int HeurNodes
        {
            get
            {
                int heurNodes;
                try
                {
                    heurNodes = this.xpprob.HeurNodes;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return heurNodes;
            }
            set
            {
                try
                {
                    this.xpprob.HeurNodes = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double HeurSearchEffort
        {
            get
            {
                double heurSearchEffort;
                try
                {
                    heurSearchEffort = this.xpprob.HeurSearchEffort;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return heurSearchEffort;
            }
            set
            {
                try
                {
                    this.xpprob.HeurSearchEffort = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int HeurSearchFreq
        {
            get
            {
                int heurSearchFreq;
                try
                {
                    heurSearchFreq = this.xpprob.HeurSearchFreq;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return heurSearchFreq;
            }
            set
            {
                try
                {
                    this.xpprob.HeurSearchFreq = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.HeuristicSearchSelect HeurSearchRootSelect
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.HeuristicSearchSelect heurSearchRootSelect;
                try
                {
                    heurSearchRootSelect = (SolverFoundation.Plugin.XpressMP.HeuristicSearchSelect) this.xpprob.HeurSearchRootSelect;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return heurSearchRootSelect;
            }
            set
            {
                try
                {
                    this.xpprob.HeurSearchRootSelect = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.HeuristicSearchSelect HeurSearchTreeSelect
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.HeuristicSearchSelect heurSearchTreeSelect;
                try
                {
                    heurSearchTreeSelect = (SolverFoundation.Plugin.XpressMP.HeuristicSearchSelect) this.xpprob.HeurSearchTreeSelect;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return heurSearchTreeSelect;
            }
            set
            {
                try
                {
                    this.xpprob.HeurSearchTreeSelect = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int HeurSelect
        {
            get
            {
                int heurSelect;
                try
                {
                    heurSelect = this.xpprob.HeurSelect;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return heurSelect;
            }
            set
            {
                try
                {
                    this.xpprob.HeurSelect = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.HeuristicStrategy HeurStrategy
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.HeuristicStrategy heurStrategy;
                try
                {
                    heurStrategy = (SolverFoundation.Plugin.XpressMP.HeuristicStrategy) this.xpprob.HeurStrategy;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return heurStrategy;
            }
            set
            {
                try
                {
                    this.xpprob.HeurStrategy = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public bool IfCheckConvexity
        {
            get
            {
                bool flag;
                try
                {
                    flag = this.xpprob.IfCheckConvexity != 0;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return flag;
            }
            set
            {
                try
                {
                    this.xpprob.IfCheckConvexity = value ? 1 : 0;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int InvertFreq
        {
            get
            {
                int invertFreq;
                try
                {
                    invertFreq = this.xpprob.InvertFreq;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return invertFreq;
            }
            set
            {
                try
                {
                    this.xpprob.InvertFreq = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int InvertMin
        {
            get
            {
                int invertMin;
                try
                {
                    invertMin = this.xpprob.InvertMin;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return invertMin;
            }
            set
            {
                try
                {
                    this.xpprob.InvertMin = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int L1Cache
        {
            get
            {
                int num;
                try
                {
                    num = this.xpprob.L1Cache;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return num;
            }
            set
            {
                try
                {
                    this.xpprob.L1Cache = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public XpressMPLicense License
        {
            get
            {
                return this.license;
            }
        }

        public int LNPBest
        {
            get
            {
                int lNPBest;
                try
                {
                    lNPBest = this.xpprob.LNPBest;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return lNPBest;
            }
            set
            {
                try
                {
                    this.xpprob.LNPBest = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int LNPIterLimit
        {
            get
            {
                int lNPIterLimit;
                try
                {
                    lNPIterLimit = this.xpprob.LNPIterLimit;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return lNPIterLimit;
            }
            set
            {
                try
                {
                    this.xpprob.LNPIterLimit = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int LocalChoice
        {
            get
            {
                int localChoice;
                try
                {
                    localChoice = this.xpprob.LocalChoice;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return localChoice;
            }
            set
            {
                try
                {
                    this.xpprob.LocalChoice = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int LPIterLimit
        {
            get
            {
                int lPIterLimit;
                try
                {
                    lPIterLimit = this.xpprob.LPIterLimit;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return lPIterLimit;
            }
            set
            {
                try
                {
                    this.xpprob.LPIterLimit = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int LPLog
        {
            get
            {
                int lPLog;
                try
                {
                    lPLog = this.xpprob.LPLog;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return lPLog;
            }
            set
            {
                try
                {
                    this.xpprob.LPLog = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double MarkowitzTol
        {
            get
            {
                double markowitzTol;
                try
                {
                    markowitzTol = this.xpprob.MarkowitzTol;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return markowitzTol;
            }
            set
            {
                try
                {
                    this.xpprob.MarkowitzTol = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double MatrixTol
        {
            get
            {
                double matrixTol;
                try
                {
                    matrixTol = this.xpprob.MatrixTol;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return matrixTol;
            }
            set
            {
                try
                {
                    this.xpprob.MatrixTol = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int MaxCutTime
        {
            get
            {
                int maxCutTime;
                try
                {
                    maxCutTime = this.xpprob.MaxCutTime;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return maxCutTime;
            }
            set
            {
                try
                {
                    this.xpprob.MaxCutTime = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int MaxGlobalFileSize
        {
            get
            {
                int maxGlobalFileSize;
                try
                {
                    maxGlobalFileSize = this.xpprob.MaxGlobalFileSize;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return maxGlobalFileSize;
            }
            set
            {
                try
                {
                    this.xpprob.MaxGlobalFileSize = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int MaxMIPSol
        {
            get
            {
                int maxMIPSol;
                try
                {
                    maxMIPSol = this.xpprob.MaxMIPSol;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return maxMIPSol;
            }
            set
            {
                try
                {
                    this.xpprob.MaxMIPSol = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int MaxNode
        {
            get
            {
                int maxNode;
                try
                {
                    maxNode = this.xpprob.MaxNode;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return maxNode;
            }
            set
            {
                try
                {
                    this.xpprob.MaxNode = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int MaxPageLines
        {
            get
            {
                int maxPageLines;
                try
                {
                    maxPageLines = this.xpprob.MaxPageLines;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return maxPageLines;
            }
            set
            {
                try
                {
                    this.xpprob.MaxPageLines = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int MaxScaleFactor
        {
            get
            {
                int maxScaleFactor;
                try
                {
                    maxScaleFactor = this.xpprob.MaxScaleFactor;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return maxScaleFactor;
            }
            set
            {
                try
                {
                    this.xpprob.MaxScaleFactor = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int MaxTime
        {
            get
            {
                int maxTime;
                try
                {
                    maxTime = this.xpprob.MaxTime;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return maxTime;
            }
            set
            {
                try
                {
                    this.xpprob.MaxTime = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double MIPAbsCutoff
        {
            get
            {
                double mIPAbsCutoff;
                try
                {
                    mIPAbsCutoff = this.xpprob.MIPAbsCutoff;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return mIPAbsCutoff;
            }
            set
            {
                try
                {
                    this.xpprob.MIPAbsCutoff = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double MIPAbsStop
        {
            get
            {
                double mIPAbsStop;
                try
                {
                    mIPAbsStop = this.xpprob.MIPAbsStop;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return mIPAbsStop;
            }
            set
            {
                try
                {
                    this.xpprob.MIPAbsStop = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double MIPAddCutoff
        {
            get
            {
                double mIPAddCutoff;
                try
                {
                    mIPAddCutoff = this.xpprob.MIPAddCutoff;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return mIPAddCutoff;
            }
            set
            {
                try
                {
                    this.xpprob.MIPAddCutoff = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int MIPLog
        {
            get
            {
                int mIPLog;
                try
                {
                    mIPLog = this.xpprob.MIPLog;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return mIPLog;
            }
            set
            {
                try
                {
                    this.xpprob.MIPLog = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.MIPPresolve MIPPresolve
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.MIPPresolve mIPPresolve;
                try
                {
                    mIPPresolve = (SolverFoundation.Plugin.XpressMP.MIPPresolve) this.xpprob.MIPPresolve;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return mIPPresolve;
            }
            set
            {
                try
                {
                    this.xpprob.MIPPresolve = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double MIPRelCutoff
        {
            get
            {
                double mIPRelCutoff;
                try
                {
                    mIPRelCutoff = this.xpprob.MIPRelCutoff;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return mIPRelCutoff;
            }
            set
            {
                try
                {
                    this.xpprob.MIPRelCutoff = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double MIPRelStop
        {
            get
            {
                double mIPRelStop;
                try
                {
                    mIPRelStop = this.xpprob.MIPRelStop;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return mIPRelStop;
            }
            set
            {
                try
                {
                    this.xpprob.MIPRelStop = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double MIPTarget
        {
            get
            {
                double num = 1;
                //try
                //{
                //    num = this.xpprob.get_MIPTarget();
                //}
                //catch (XPRSException exception)
                //{
                //    throw new MsfException(exception.Message, exception);
                //}
                return num;
            }
            set
            {
                //try
                //{
                //    this.xpprob.set_MIPTarget(value);
                //}
                //catch (XPRSException exception)
                //{
                //    throw new MsfException(exception.Message, exception);
                //}
            }
        }

        public int MIPThreads
        {
            get
            {
                int mIPThreads;
                try
                {
                    mIPThreads = this.xpprob.MIPThreads;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return mIPThreads;
            }
            set
            {
                try
                {
                    this.xpprob.MIPThreads = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double MIPTol
        {
            get
            {
                double mIPTol;
                try
                {
                    mIPTol = this.xpprob.MIPTol;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return mIPTol;
            }
            set
            {
                try
                {
                    this.xpprob.MIPTol = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.NodeSelectionCriteria NodeSelection
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.NodeSelectionCriteria nodeSelection;
                try
                {
                    nodeSelection = (SolverFoundation.Plugin.XpressMP.NodeSelectionCriteria) this.xpprob.NodeSelection;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return nodeSelection;
            }
            set
            {
                try
                {
                    this.xpprob.NodeSelection = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double OptimalityTol
        {
            get
            {
                double optimalityTol;
                try
                {
                    optimalityTol = this.xpprob.OptimalityTol;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return optimalityTol;
            }
            set
            {
                try
                {
                    this.xpprob.OptimalityTol = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.OutputDetail OutputLogDetail
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.OutputDetail outputLog;
                try
                {
                    outputLog = (SolverFoundation.Plugin.XpressMP.OutputDetail) this.xpprob.OutputLog;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return outputLog;
            }
            set
            {
                try
                {
                    this.xpprob.OutputLog = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double OutputTol
        {
            get
            {
                double outputTol;
                try
                {
                    outputTol = this.xpprob.OutputTol;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return outputTol;
            }
            set
            {
                try
                {
                    this.xpprob.OutputTol = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double Penalty
        {
            get
            {
                double penalty;
                try
                {
                    penalty = this.xpprob.Penalty;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return penalty;
            }
            set
            {
                try
                {
                    this.xpprob.Penalty = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double Perturb
        {
            get
            {
                double perturb;
                try
                {
                    perturb = this.xpprob.Perturb;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return perturb;
            }
            set
            {
                try
                {
                    this.xpprob.Perturb = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double PivotTol
        {
            get
            {
                double pivotTol;
                try
                {
                    pivotTol = this.xpprob.PivotTol;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return pivotTol;
            }
            set
            {
                try
                {
                    this.xpprob.PivotTol = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double PPFactor
        {
            get
            {
                double pPFactor;
                try
                {
                    pPFactor = this.xpprob.PPFactor;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return pPFactor;
            }
            set
            {
                try
                {
                    this.xpprob.PPFactor = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.PresolveCoefElim PreCoefElim
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.PresolveCoefElim preCoefElim;
                try
                {
                    preCoefElim = (SolverFoundation.Plugin.XpressMP.PresolveCoefElim) this.xpprob.PreCoefElim;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return preCoefElim;
            }
            set
            {
                try
                {
                    this.xpprob.PreCoefElim = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.PresolveDomColumn PreDomCol
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.PresolveDomColumn preDomCol;
                try
                {
                    preDomCol = (SolverFoundation.Plugin.XpressMP.PresolveDomColumn) this.xpprob.PreDomCol;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return preDomCol;
            }
            set
            {
                try
                {
                    this.xpprob.PreDomCol = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.PresolveDomRow PreDomRow
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.PresolveDomRow preDomRow;
                try
                {
                    preDomRow = (SolverFoundation.Plugin.XpressMP.PresolveDomRow) this.xpprob.PreDomRow;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return preDomRow;
            }
            set
            {
                try
                {
                    this.xpprob.PreDomRow = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.PreProbing PreProbing
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.PreProbing preProbing;
                try
                {
                    preProbing = (SolverFoundation.Plugin.XpressMP.PreProbing) this.xpprob.PreProbing;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return preProbing;
            }
            set
            {
                try
                {
                    this.xpprob.PreProbing = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.PresolveOperations PresolveOps
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.PresolveOperations presolveOps;
                try
                {
                    presolveOps = (SolverFoundation.Plugin.XpressMP.PresolveOperations) this.xpprob.PresolveOps;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return presolveOps;
            }
            set
            {
                try
                {
                    this.xpprob.PresolveOps = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.Presolve PresolveType
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.Presolve presolve;
                try
                {
                    presolve = (SolverFoundation.Plugin.XpressMP.Presolve) this.xpprob.Presolve;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return presolve;
            }
            set
            {
                try
                {
                    this.xpprob.Presolve = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.Pricing PricingAlg
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.Pricing pricingAlg;
                try
                {
                    pricingAlg = (SolverFoundation.Plugin.XpressMP.Pricing) this.xpprob.PricingAlg;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return pricingAlg;
            }
            set
            {
                try
                {
                    this.xpprob.PricingAlg = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.PrimalUnshift PrimalUnshift
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.PrimalUnshift primalUnshift;
                try
                {
                    primalUnshift = (SolverFoundation.Plugin.XpressMP.PrimalUnshift) this.xpprob.PrimalUnshift;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return primalUnshift;
            }
            set
            {
                try
                {
                    this.xpprob.PrimalUnshift = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double PseudoCost
        {
            get
            {
                double pseudoCost;
                try
                {
                    pseudoCost = this.xpprob.PseudoCost;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return pseudoCost;
            }
            set
            {
                try
                {
                    this.xpprob.PseudoCost = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.AlwaysNeverAutomatic QuadraticUnshift
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.AlwaysNeverAutomatic quadraticUnshift;
                try
                {
                    quadraticUnshift = (SolverFoundation.Plugin.XpressMP.AlwaysNeverAutomatic) this.xpprob.QuadraticUnshift;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return quadraticUnshift;
            }
            set
            {
                try
                {
                    this.xpprob.QuadraticUnshift = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double RelPivotTol
        {
            get
            {
                double relPivotTol;
                try
                {
                    relPivotTol = this.xpprob.RelPivotTol;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return relPivotTol;
            }
            set
            {
                try
                {
                    this.xpprob.RelPivotTol = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public RepairIndefinateQuadratic RepairIndefiniteQ
        {
            get
            {
                RepairIndefinateQuadratic quadratic;
                try
                {
                    quadratic = (RepairIndefinateQuadratic) this.xpprob.RepairIndefiniteQ;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return quadratic;
            }
            set
            {
                try
                {
                    this.xpprob.RepairIndefiniteQ = (RepairIndefiniteQuadratic)value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.AlwaysNeverAutomatic RootPresolve
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.AlwaysNeverAutomatic rootPresolve;
                try
                {
                    rootPresolve = (SolverFoundation.Plugin.XpressMP.AlwaysNeverAutomatic) this.xpprob.RootPresolve;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return rootPresolve;
            }
            set
            {
                try
                {
                    this.xpprob.RootPresolve = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int SBBest
        {
            get
            {
                int sBBest;
                try
                {
                    sBBest = this.xpprob.SBBest;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return sBBest;
            }
            set
            {
                try
                {
                    this.xpprob.SBBest = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double SbEffort
        {
            get
            {
                double sbEffort;
                try
                {
                    sbEffort = this.xpprob.SbEffort;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return sbEffort;
            }
            set
            {
                try
                {
                    this.xpprob.SbEffort = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int SBEstimate
        {
            get
            {
                int sBEstimate;
                try
                {
                    sBEstimate = this.xpprob.SBEstimate;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return sBEstimate;
            }
            set
            {
                try
                {
                    this.xpprob.SBEstimate = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int SBIterLimit
        {
            get
            {
                int sBIterLimit;
                try
                {
                    sBIterLimit = this.xpprob.SBIterLimit;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return sBIterLimit;
            }
            set
            {
                try
                {
                    this.xpprob.SBIterLimit = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int SBSelect
        {
            get
            {
                int sBSelect;
                try
                {
                    sBSelect = this.xpprob.SBSelect;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return sBSelect;
            }
            set
            {
                try
                {
                    this.xpprob.SBSelect = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.Scaling Scaling
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.Scaling scaling;
                try
                {
                    scaling = (SolverFoundation.Plugin.XpressMP.Scaling) this.xpprob.Scaling;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return scaling;
            }
            set
            {
                try
                {
                    this.xpprob.Scaling = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double SOSRefTol
        {
            get
            {
                double sOSRefTol;
                try
                {
                    sOSRefTol = this.xpprob.SOSRefTol;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return sOSRefTol;
            }
            set
            {
                try
                {
                    this.xpprob.SOSRefTol = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.AlwaysNeverAutomatic TempBounds
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.AlwaysNeverAutomatic tempBounds;
                try
                {
                    tempBounds = (SolverFoundation.Plugin.XpressMP.AlwaysNeverAutomatic) this.xpprob.TempBounds;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return tempBounds;
            }
            set
            {
                try
                {
                    this.xpprob.TempBounds = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int Threads
        {
            get
            {
                int threads;
                try
                {
                    threads = this.xpprob.Threads;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return threads;
            }
            set
            {
                try
                {
                    this.xpprob.Threads = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int Trace
        {
            get
            {
                int trace;
                try
                {
                    trace = this.xpprob.Trace;
                }
                catch (Exception exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return trace;
            }
            set
            {
                try
                {
                    this.xpprob.Trace = value;
                }
                catch (Exception exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int TreeCompression
        {
            get
            {
                int treeCompression;
                try
                {
                    treeCompression = this.xpprob.TreeCompression;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return treeCompression;
            }
            set
            {
                try
                {
                    this.xpprob.TreeCompression = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int TreeCoverCuts
        {
            get
            {
                int treeCoverCuts;
                try
                {
                    treeCoverCuts = this.xpprob.TreeCoverCuts;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return treeCoverCuts;
            }
            set
            {
                try
                {
                    this.xpprob.TreeCoverCuts = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int TreeCutSelect
        {
            get
            {
                int treeCutSelect;
                try
                {
                    treeCutSelect = this.xpprob.TreeCutSelect;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return treeCutSelect;
            }
            set
            {
                try
                {
                    this.xpprob.TreeCutSelect = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.TreeDiagnostics TreeDiagnostics
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.TreeDiagnostics treeDiagnostics;
                try
                {
                    treeDiagnostics = (SolverFoundation.Plugin.XpressMP.TreeDiagnostics) this.xpprob.TreeDiagnostics;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return treeDiagnostics;
            }
            set
            {
                try
                {
                    this.xpprob.TreeDiagnostics = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int TreeGomCuts
        {
            get
            {
                int treeGomCuts;
                try
                {
                    treeGomCuts = this.xpprob.TreeGomCuts;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return treeGomCuts;
            }
            set
            {
                try
                {
                    this.xpprob.TreeGomCuts = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public int TreeMemoryLimit
        {
            get
            {
                int treeMemoryLimit;
                try
                {
                    treeMemoryLimit = this.xpprob.TreeMemoryLimit;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return treeMemoryLimit;
            }
            set
            {
                try
                {
                    this.xpprob.TreeMemoryLimit = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public double TreeMemorySavingTarget
        {
            get
            {
                double treeMemorySavingTarget;
                try
                {
                    treeMemorySavingTarget = this.xpprob.TreeMemorySavingTarget;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return treeMemorySavingTarget;
            }
            set
            {
                try
                {
                    this.xpprob.TreeMemorySavingTarget = value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        public SolverFoundation.Plugin.XpressMP.VariableSelection VarSelection
        {
            get
            {
                SolverFoundation.Plugin.XpressMP.VariableSelection varSelection;
                try
                {
                    varSelection = (SolverFoundation.Plugin.XpressMP.VariableSelection) this.xpprob.VarSelection;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
                return varSelection;
            }
            set
            {
                try
                {
                    this.xpprob.VarSelection = (int) value;
                }
                catch (XPRSException exception)
                {
                    throw new MsfException(exception.Message, exception);
                }
            }
        }

        internal XPRSprob XpressProb
        {
            get
            {
                return this.xpprob;
            }
        }
    }
}

