﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("Microsoft.Naming", "CA1714:FlagsEnumsShouldHavePluralNames"), Flags]
    public enum PresolveState
    {
        ProblemLoaded = 1,
        ProblemLPPresolved = 2,
        ProblemMIPPresolved = 4,
        SolutionValid = 0x80
    }
}

