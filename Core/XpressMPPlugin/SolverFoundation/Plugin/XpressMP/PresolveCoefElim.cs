﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public enum PresolveCoefElim
    {
        Disabled,
        Aggressive,
        Cautious
    }
}

