﻿namespace SolverFoundation.Plugin.XpressMP
{
    using Microsoft.SolverFoundation.Common;
    using Microsoft.SolverFoundation.Services;
    using Optimizer;
    using System;
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses")]
    internal class XpressMPSensitivityReport : ILinearSolverSensitivityReport, ILinearSolverReport
    {
        private LinearSolverSensitivityRange[] objCoeffRange;
        private XpressMPSolution sol;
        private LinearSolverSensitivityRange[] varRange;

        public XpressMPSensitivityReport(XpressMPSolver solver)
        {
            try
            {
                XPRSprob xpressProb = solver.XpressProb;
                if (xpressProb.MIPStatus == MIPStatus.Optimal)
                {
                    solver.FixIntegers();
                    solver.SolveRelaxation(new XpressMPParams());
                }
                if (xpressProb.LPStatus != LPStatus.Optimal)
                {
                    throw new MsfException("Sensitivity report can only be generated for an optimal LP");
                }
                VIDManager manager = new VIDManager(solver.VIDs);
                this.sol = (XpressMPSolution) solver.Solution;
                this.objCoeffRange = new LinearSolverSensitivityRange[manager.LargestIndex + 1];
                foreach (int num in manager.Indices)
                {
                    this.objCoeffRange[num].Lower = Rational.Indeterminate;
                    this.objCoeffRange[num].Upper = Rational.Indeterminate;
                    this.objCoeffRange[num].Current = this.sol.GetValue(num);
                }
                int[] mindex = new int[xpressProb.OriginalCols];
                for (int i = 0; i < mindex.Length; i++)
                {
                    mindex[i] = i;
                }
                double[] lower = new double[xpressProb.OriginalCols];
                double[] upper = new double[xpressProb.OriginalCols];
                xpressProb.Objsa(mindex.Length, mindex, lower, upper);
                foreach (int num3 in manager.VariableIndices)
                {
                    int columnIndex = manager.GetColumnIndex(num3);
                    this.objCoeffRange[num3].Lower = XpressMPSolver.ToMsfRational(lower[columnIndex]);
                    this.objCoeffRange[num3].Upper = XpressMPSolver.ToMsfRational(upper[columnIndex]);
                }
                this.varRange = new LinearSolverSensitivityRange[manager.LargestIndex + 1];
                foreach (int num5 in manager.Indices)
                {
                    this.varRange[num5].Lower = Rational.Indeterminate;
                    this.varRange[num5].Upper = Rational.Indeterminate;
                    this.varRange[num5].Current = this.sol.GetValue(num5);
                }
                xpressProb.Range();
                double[] numArray4 = new double[xpressProb.OriginalRows];
                double[] numArray5 = new double[xpressProb.OriginalRows];
                double[] numArray6 = new double[xpressProb.OriginalRows];
                double[] numArray7 = new double[xpressProb.OriginalRows];
                xpressProb.GetRowRange(numArray4, numArray5, numArray6, numArray7);
                foreach (int num6 in manager.RowIndices)
                {
                    int rowIndex = manager.GetRowIndex(num6);
                    this.varRange[num6].Lower = numArray5[rowIndex];
                    this.varRange[num6].Upper = numArray4[rowIndex];
                }
                double[] numArray8 = new double[xpressProb.OriginalCols];
                double[] numArray9 = new double[xpressProb.OriginalCols];
                double[] numArray10 = new double[xpressProb.OriginalCols];
                double[] numArray11 = new double[xpressProb.OriginalCols];
                double[] numArray12 = new double[xpressProb.OriginalCols];
                double[] numArray13 = new double[xpressProb.OriginalCols];
                xpressProb.GetColRange(numArray8, numArray9, numArray10, numArray11, numArray12, numArray13);
                foreach (int num8 in manager.VariableIndices)
                {
                    int index = manager.GetColumnIndex(num8);
                    this.varRange[num8].Lower = numArray9[index];
                    this.varRange[num8].Upper = numArray8[index];
                }
            }
            catch (XPRSException exception)
            {
                throw new MsfException(exception.Message, exception);
            }
        }

        public Rational GetDualValue(int rid)
        {
            return this.sol.GetValue(rid);
        }

        public LinearSolverSensitivityRange GetObjectiveCoefficientRange(int vid)
        {
            return this.objCoeffRange[vid];
        }

        public LinearSolverSensitivityRange GetObjectiveCoefficientRange(int vid, int pri)
        {
            return this.GetObjectiveCoefficientRange(vid);
        }

        public LinearSolverSensitivityRange GetVariableRange(int vid)
        {
            return this.varRange[vid];
        }
    }
}

