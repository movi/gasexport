﻿namespace SolverFoundation.Plugin.XpressMP
{
    using Microsoft.SolverFoundation.Services;
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    internal struct XpressMPVariableSolution
    {
        public bool isSet;
        public double value;
        public LinearValueState state;
        public bool isBasic;
        public XpressMPVariableSolution(double val, LinearValueState state, bool isBasic)
        {
            this.isSet = true;
            this.value = val;
            this.state = state;
            this.isBasic = isBasic;
        }
    }
}

