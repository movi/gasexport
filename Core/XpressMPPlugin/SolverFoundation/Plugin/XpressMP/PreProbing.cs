﻿namespace SolverFoundation.Plugin.XpressMP
{
    using System;

    public enum PreProbing
    {
        Automatic = -1,
        Disabled = 0,
        FullProbing = 2,
        FullProbingAndRepeat = 3,
        LightProbing = 1
    }
}

