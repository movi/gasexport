﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Mask;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.ViewInfo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using GEx.ErrorHandler;
using GEx.Iface;

namespace CommonUIControls.MatrixControl
{
    public partial class CtrlRelationMatrix : XtraUserControl
    {

        public class ZonesView
        {
            internal int ZoneId { get; set; }
            
            [DisplayName("Оператор")]
            public string Name { get; set; }

            [DisplayName("Узлы")]
            public BindingList<DTO_Node> Nodes { get; set; }
        }

        public CtrlRelationMatrix()
        {            
            InitializeComponent();
            ExtraInit();
        }

        private void ExtraInit()
        {
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            GridView view = sender as GridView;

            var row = view.GetRow(e.RowHandle);
            var col = e.Column;

            //HACK: a very-very dirty hack
            //знаю, в каком порядке идут строки (rowhandle), FieldName = "1" .. (количество узлов + 1).ToString(), как в описании дескрипторов MatrixBindingList (ITypedList), 
            //0 - для самого первого столбца с названиями узлов
            if (Equals(col.FieldName, (e.RowHandle + 1).ToString()))
            {
               // ((GridCellInfo)e.Cell).Editor = _roRepositoryItemCheckEdit;
                CheckEditViewInfo viewInfo = ((GridCellInfo)e.Cell).ViewInfo as CheckEditViewInfo;
                if (viewInfo != null)
                {
                    viewInfo.CheckInfo.State = DevExpress.Utils.Drawing.ObjectState.Disabled;
                    viewInfo.CheckInfo.IsDrawOnGlass = true;
                    viewInfo.IsReady = true;
                }
            }
            //e.Appearance.BackColor = e.Appearance.BackColor2 = e.RowHandle % 2 == 0 ? Color.White : Color.LightCyan;

            e.Appearance.Options.UseBackColor = true;
            e.Appearance.BackColor = e.RowHandle + 1 == e.Column.ColumnHandle
                ? Color.Silver
                : Color.White;

            e.DisplayText = e.RowHandle + 1 == e.Column.ColumnHandle
                ? string.Empty
                : e.DisplayText;
        }

        private MatrixBindingList _mlist;

        private List<ZonesView> _ZoneViews;
        private GEx.ErrorHandler.IErrorOutput _errorHandler;

        public IEnumerable<DTO_NodeRelation> NodesRelations
        {
            get
            {
               List<DTO_NodeRelation> relations =new List<DTO_NodeRelation> ();
               foreach (var view in _ZoneViews)
               {
                   var matrix = view.Nodes as MatrixBindingList;
                   if (matrix != null)
                    relations.AddRange(matrix.NodeRelations);
               }
               return relations.ToArray();
            }
        }

        public IEnumerable<DTO_BalanceZone> BalansZones
        {
            get
            {
                return _ZoneViews.Select(zone => new DTO_BalanceZone() { ID = zone.ZoneId, Name = zone.Name, }).ToList();                
            }
        }

        private int getNewZoneId()
        {
            var t = _ZoneViews.Min(z=> z.ZoneId);
            if (t > 0) t = 0;
            return t-1;
        }

        public void Init(IErrorOutput eh, IEnumerable<DTO_Node> nodes,IEnumerable<DTO_NodeRelation> nodesRelations, IEnumerable<DTO_BalanceZone> balanceZones)
        {
            _errorHandler = eh;
           
            var ds = PrepareDataSource(nodes, nodesRelations, balanceZones);
            gcMatrix.DataSource = ds;
            gvMatrix.Columns["Name"].OptionsColumn.AllowEdit = gvMatrix.Columns["Name"].OptionsColumn.AllowFocus = false;

            gvMatrix.MasterRowExpanded += gridView1_MasterRowExpanded;
        }

        private List<ZonesView> PrepareDataSource(IEnumerable<DTO_Node> nodes, IEnumerable<DTO_NodeRelation> nodesRelations, IEnumerable<DTO_BalanceZone> balanceZones)
        {
            //из списка узлов выделить пары {узел - список всех зон(операторов) с интерконнекторов и поля "Зона"}
            
            // List{ Node, List{Zone>}}
            var nodesWithUnitedZones = nodes.Select(n => new
            {
                n,
                //все-все-все зоны узла - со всех интерконнекторов и полей ZoneID, ZoneName
                Zones1 =
                    (n.InterConnections.Select(x => x.ZoneFrom).Union(
                        n.InterConnections.Select(x => x.ZoneTo)).ToList().Union(
                            new[] {balanceZones.FirstOrDefault(z => z.ID == n.ZoneId)})).Where(elem => (elem != null))
                        .ToList()
            });
            
            // плющим список в пары
            // List {Node,Zone}
            var nodeZonePairs =
                nodesWithUnitedZones.SelectMany(x => x.Zones1.Select(y => new {Node = x.n, Zone = y})).ToList();

            nodeZonePairs.Sort((x, y) => String.Compare(x.Node.Name, y.Node.Name, StringComparison.Ordinal));

            //объединяем по зонам
            //Dictionary {Zone, List{Node}}
            var zoneNodesDictionary = nodeZonePairs.GroupBy(x => x.Zone)
                .ToDictionary(x => x.Key, y => y.Select(zz=>zz.Node).ToList());
            
            //конвертим в представление
            var source =
               zoneNodesDictionary.Select(z =>
                    new ZonesView
                    {
                        ZoneId = z.Key.ID,
                        Name = z.Key.Name,
                        Nodes =
                            new MatrixBindingList(z.Key.ID, z.Value.OrderBy(n=> n.Name).ToList(), nodesRelations.Where(r => r.ZoneID == z.Key.ID).ToArray())
                    }).ToList();
            
            ;
            
            var ds = source;            
            
            // т.к. редактирование зон перемещено в другой контрол, нет необходимости выбирать отдельно пустые зоны
            /*
            foreach (var bz in balanceZones)
            {
                if (!ds.Any(b => b.ZoneId == bz.ID && b.Name == bz.Name))
                {
                    ds.Add(new ZonesView()
                    {
                        ZoneId = bz.ID,
                        Name = bz.Name,
                        Nodes = new MatrixBindingList(bz.ID, new List<DTO_Node>(), new DTO_NodeRelation[0])
                    });
                }
            }
            */
            ds.Sort((x, y) =>
            {
                if (y.Nodes.Count > 0 && x.Nodes.Count > 0)
                    return String.Compare(x.Name, y.Name, StringComparison.Ordinal);
                if (y.Nodes.Count > 0 && x.Nodes.Count == 0)
                    return 1;
                if (y.Nodes.Count == 0 && x.Nodes.Count > 0)
                    return -1;
                if (y.Nodes.Count == 0 && x.Nodes.Count == 0)
                    return String.Compare(x.Name, y.Name, StringComparison.Ordinal);
                return 0;
            });
            
            _ZoneViews = ds;
            return ds;
        }

        /// <summary>
        /// Событие развертывания строки из Main View
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridView1_MasterRowExpanded(object sender, CustomMasterRowEventArgs e)
        {
            GridView childView = this.gvMatrix.GetDetailView(e.RowHandle, e.RelationIndex) as GridView;
            if (childView == null) return;

//            var columns = childView.Columns;
            childView.CustomDrawCell += gridView1_CustomDrawCell;
            childView.CustomRowCellEdit += ChildViewOnCustomRowCellEdit;
            childView.OptionsCustomization.AllowSort = false;
//            SetEditorForColumns(columns);
        }
        
        /// <summary>
        /// Назначим едиторы для грида
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChildViewOnCustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            e.RepositoryItem = e.RowHandle + 1 == e.Column.ColumnHandle 
                ? RepositoryItemTextEditReadOnly 
                : RepositoryItemTextEdit;
        }

        private static RepositoryItemTextEdit _repositoryItemTextEditReadOnly;
        private static RepositoryItemTextEdit RepositoryItemTextEditReadOnly
        {
            get
            {
                if (_repositoryItemTextEditReadOnly != null) return _repositoryItemTextEditReadOnly;
                return _repositoryItemTextEditReadOnly = new RepositoryItemTextEdit
                {
                    ReadOnly = true
                };
            }
        }

        private static RepositoryItemTextEdit _repositoryItemTextEdit;
        private static RepositoryItemTextEdit RepositoryItemTextEdit
        {
            get
            {
                if (_repositoryItemTextEdit != null) return _repositoryItemTextEdit;
                _repositoryItemTextEdit = new RepositoryItemTextEdit
                {
                    ReadOnly = false,
//                Mask = { MaskType = MaskType.RegEx, EditMask = @"100|\d{0,2}\,\d{0,2}" }
                    Mask = { MaskType = MaskType.RegEx, EditMask = @"100|\d{0,2}" }
                };
                _repositoryItemTextEdit.EditValueChanged += ceItem_EditValueChanged;
                return _repositoryItemTextEdit;
            }
        }

        private void SetEditorForColumns(GridColumnCollection columns)
        {
            var i = 0;
            foreach (GridColumn column in columns)
            { 
                if (i++ == 0)
                {
                    column.OptionsColumn.AllowFocus = false;
                }
                else
                    column.ColumnEdit = RepositoryItemTextEdit;
            }
        }

        private static void ceItem_EditValueChanged(object sender, EventArgs e)
        {
            var editor = (sender as CheckEdit);
            if (editor == null) return;
            var grid = editor.Parent as GridControl;
            
            if (grid == null) return;
            var view = grid.FocusedView as GridView;

            if (view != null)
            {
                view.PostEditor();
                view.CloseEditor();
                view.UpdateCurrentRow();
            }
        }          
    }
}
