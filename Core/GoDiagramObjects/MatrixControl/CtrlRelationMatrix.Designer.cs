﻿namespace CommonUIControls.MatrixControl
{
    partial class CtrlRelationMatrix
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcMatrix = new DevExpress.XtraGrid.GridControl();
            this.gvMatrix = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.gcMatrix)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMatrix)).BeginInit();
            this.SuspendLayout();
            // 
            // gcMatrix
            // 
            this.gcMatrix.Cursor = System.Windows.Forms.Cursors.Default;
            this.gcMatrix.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcMatrix.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gcMatrix.Location = new System.Drawing.Point(0, 0);
            this.gcMatrix.MainView = this.gvMatrix;
            this.gcMatrix.Name = "gcMatrix";
            this.gcMatrix.Size = new System.Drawing.Size(544, 388);
            this.gcMatrix.TabIndex = 0;
            this.gcMatrix.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvMatrix});
            // 
            // gvMatrix
            // 
            this.gvMatrix.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gvMatrix.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvMatrix.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gvMatrix.Appearance.Row.Options.UseFont = true;
            this.gvMatrix.GridControl = this.gcMatrix;
            this.gvMatrix.Name = "gvMatrix";
            this.gvMatrix.OptionsCustomization.AllowGroup = false;
            this.gvMatrix.OptionsCustomization.AllowQuickHideColumns = false;
            this.gvMatrix.OptionsFind.AllowFindPanel = false;
            this.gvMatrix.OptionsNavigation.AutoFocusNewRow = true;
            this.gvMatrix.OptionsView.ShowGroupPanel = false;
            // 
            // CtrlRelationMatrix
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.gcMatrix);
            this.Name = "CtrlRelationMatrix";
            this.Size = new System.Drawing.Size(544, 388);
            ((System.ComponentModel.ISupportInitialize)(this.gcMatrix)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMatrix)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraGrid.GridControl gcMatrix;
        public DevExpress.XtraGrid.Views.Grid.GridView gvMatrix;

    }
}