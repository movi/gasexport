﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using GEx.Iface;
//using GEx.Domain;

namespace CommonUIControls.MatrixControl
{
    public delegate object DynamicGetValue(object component);

    public delegate void DynamicSetValue(object component, object newValue);

    public class DynamicPropertyDescriptor : PropertyDescriptor
    {
        protected Type _componentType;
        protected Type _propertyType;
        protected DynamicGetValue _getDelegate;
        protected DynamicSetValue _setDelegate;
        private string _displayName;

        public DynamicPropertyDescriptor(Type componentType, string name, string displayname, Type propertyType, DynamicGetValue getDelegate,
            DynamicSetValue setDelegate)
            :base(name, null)
        {
            _componentType = componentType;
            _propertyType = propertyType;
            _getDelegate = getDelegate;
            _setDelegate = setDelegate;
            _displayName = displayname;
        }

        public override string DisplayName
        {
            get { return _displayName; }
        }

        public override bool CanResetValue(object component)
        {
            return false;
        }

        public override Type ComponentType
        {
            get { return _componentType; }
        }

        public override object GetValue(object component)
        {
            return _getDelegate(component);
        }

        public override bool IsReadOnly
        {
            get { return _setDelegate == null; }
        }

        public override Type PropertyType
        {
            get { return _propertyType; }
        }

        public override void ResetValue(object component)
        {
        }

        public override void SetValue(object component, object value)
        {
            _setDelegate(component, value);
            EventArgs args = new EventArgs();
            OnValueChanged(component, args);
        }

        public override bool ShouldSerializeValue(object component)
        {
            return true;
        }
    }
    
    /// <summary>
    /// Обертка для списка узлов для представления матрицы связей в этом списке,
    /// сразу идет как datasource в любой грид
    /// </summary>
    internal class MatrixBindingList : BindingList<DTO_Node>, ITypedList
    {
        private int _ZoneId;

        private List<DTO_Node> _Nodes;

        private List<DTO_NodeRelation> _NodesRelations;

        public IEnumerable<DTO_NodeRelation> NodeRelations
        {
            get
            {
                return _NodesRelations;
            }
        }

        public MatrixBindingList(int zoneId,List<DTO_Node> nodes,IEnumerable<DTO_NodeRelation> nodesRelations)
            : base(nodes)
        {
            _ZoneId = zoneId;
            _NodesRelations = new List<DTO_NodeRelation>();
            if (nodesRelations != null)
            {                
                _NodesRelations.AddRange(nodesRelations);                
            }          
        }

        public string GetListName(PropertyDescriptor[] listAccessors)
        {
            return null;
        }

        public PropertyDescriptorCollection GetItemProperties(PropertyDescriptor[] listAccessors)
        {
            var pdc = GenerateDescriptorCollection(this);
            return pdc;
        }
        
        /// <summary>
        /// Формирует набор отображаемых свойств в гриде на основе списка узлов
        /// (по сути - колонки)
        /// </summary>
        /// <param name="nodes"></param>
        /// <returns></returns>
        public PropertyDescriptorCollection GenerateDescriptorCollection(IList<DTO_Node> nodes)
        {            
            List<PropertyDescriptor> props = new List<PropertyDescriptor>();

            int propNum = 0;

            props.Add(new DynamicPropertyDescriptor(
                typeof(DTO_Node),
                propNum.ToString(),
                "Узел",
                typeof (string),
                rowNode => ((DTO_Node)rowNode).Name,
                null
                ));

            propNum++;
            
            //перебор формирует колонки
            foreach (var node in nodes.OrderBy(r=>r.Name))
            {
                var columnNode = node;
                var prop = new DynamicPropertyDescriptor(
                    typeof (DTO_Node),
                    propNum.ToString(),
                    String.IsNullOrEmpty(node.Name) ? " " : node.Name,
                    typeof (double),

                    //getter
                    rowNode =>
                    {
                        var dtoRowNode = rowNode as DTO_Node;
                        var dtoNodeRelation = _NodesRelations.FirstOrDefault(nr => dtoRowNode != null &&
                                                                                   (nr.NodeIdSecond == columnNode.ID &&
                                                                                    nr.NodeIdFirst == dtoRowNode.ID));
                        return dtoNodeRelation == null ? 0 : dtoNodeRelation.Value;
                    },

                    //setter
                    // SetNewValue устанавливает/снимает связь rowNode к columnNode
                    (rowNode, newPropVal) => SetNewValue(rowNode as DTO_Node, columnNode, (double?) newPropVal ?? 0)
                );
                props.Add(prop);
                propNum++;
            }

            PropertyDescriptor[] propArray = new PropertyDescriptor[props.Count];
            props.CopyTo(propArray);
            return new PropertyDescriptorCollection(propArray);
        }

        private void AddLink(DTO_Node curNode, DTO_Node node, double value)
        {
            _NodesRelations.Add(new DTO_NodeRelation() { NodeIdFirst = curNode.ID, NodeIdSecond = node.ID ,ZoneID=_ZoneId, Value = value});
        }

        private void RemoveLink(DTO_Node curNode, DTO_Node node)
        {
            var indexesOfRelationsForDelete = 
                _NodesRelations.Where(nr => nr.NodeIdFirst == curNode.ID && nr.NodeIdSecond == node.ID)
                //.Union(_NodesRelations.Where(nr => nr.NodeIdFirst == node.ID && nr.NodeIdSecond == curNode.ID))
           .Select(rel=>_NodesRelations.IndexOf(rel));

            var forDelete = indexesOfRelationsForDelete as IList<int> ?? indexesOfRelationsForDelete.ToList();
            for(int i=0;i<forDelete.Count();i++)
            {
                var rel = _NodesRelations.ElementAt(forDelete.ElementAt(i));
                _NodesRelations.Remove(rel);
            }
        }

        private void SetNewValue(DTO_Node curNode, DTO_Node node, double val)
        {
            if (curNode == node) val = 0;

            double TOLERANCE = 0;
            if (Math.Abs(val) > TOLERANCE)
            {
                RemoveLink(curNode, node);
                AddLink(curNode, node, val);
            }
            else
            {
                RemoveLink(curNode, node);
            }

            OnListChanged(new ListChangedEventArgs(ListChangedType.ItemChanged, Items.IndexOf(curNode)));
            OnListChanged(new ListChangedEventArgs(ListChangedType.ItemChanged, Items.IndexOf(node)));
        }

    }
}
