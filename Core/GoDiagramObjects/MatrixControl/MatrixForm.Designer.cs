﻿namespace CommonUIControls.MatrixControl
{
    partial class MatrixForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sbtnOK = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.matrixCtrl = new CommonUIControls.MatrixControl.CtrlRelationMatrix();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // sbtnOK
            // 
            this.sbtnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnOK.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.sbtnOK.Appearance.Options.UseFont = true;
            this.sbtnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.sbtnOK.Location = new System.Drawing.Point(475, 7);
            this.sbtnOK.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.sbtnOK.Name = "sbtnOK";
            this.sbtnOK.Size = new System.Drawing.Size(86, 32);
            this.sbtnOK.TabIndex = 1;
            this.sbtnOK.Text = "ОК";
            this.sbtnOK.Click += new System.EventHandler(this.sbtnOK_Click);
            // 
            // sbtnCancel
            // 
            this.sbtnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.sbtnCancel.Appearance.Options.UseFont = true;
            this.sbtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.sbtnCancel.Location = new System.Drawing.Point(566, 7);
            this.sbtnCancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.sbtnCancel.Name = "sbtnCancel";
            this.sbtnCancel.Size = new System.Drawing.Size(86, 32);
            this.sbtnCancel.TabIndex = 2;
            this.sbtnCancel.Text = "Отмена";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.sbtnOK);
            this.panel1.Controls.Add(this.sbtnCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 471);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(660, 48);
            this.panel1.TabIndex = 3;
            // 
            // matrixCtrl
            // 
            this.matrixCtrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.matrixCtrl.Location = new System.Drawing.Point(0, 0);
            this.matrixCtrl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.matrixCtrl.Name = "matrixCtrl";
            this.matrixCtrl.Size = new System.Drawing.Size(660, 471);
            this.matrixCtrl.TabIndex = 0;
            // 
            // MatrixForm
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(660, 519);
            this.Controls.Add(this.matrixCtrl);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 8F);
            this.MinimumSize = new System.Drawing.Size(600, 500);
            this.Name = "MatrixForm";
            this.Text = "Матрица связей";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MatrixForm_FormClosed);
            this.Load += new System.EventHandler(this.MatrixForm_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MatrixControl.CtrlRelationMatrix matrixCtrl;
        private DevExpress.XtraEditors.SimpleButton sbtnOK;
        private DevExpress.XtraEditors.SimpleButton sbtnCancel;
        private System.Windows.Forms.Panel panel1;


    }
}