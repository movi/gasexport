﻿using System.Collections.Generic;
using System.Windows.Forms;
using GasExportApp.Dialogs;
using GEx.ErrorHandler;
using GEx.Iface;
///using GEx.Domain;

namespace CommonUIControls.MatrixControl
{
    public partial class MatrixForm : DevExpress.XtraEditors.XtraForm, IDialogForm
    {
        private IMessageFilter _filter;

        public MatrixForm()
        {
            InitializeComponent();           
        }        

        public IEnumerable<DTO_NodeRelation> NodesRelations
        {
            get { return matrixCtrl.NodesRelations; }
        }

        public IEnumerable<DTO_BalanceZone> BalansZones
        {
            get { return matrixCtrl.BalansZones; }
        }

        public void Init(IErrorOutput errorHandler, IEnumerable<DTO_Node> getSchemaNodes, IEnumerable<DTO_NodeRelation> getNodesRelations, IEnumerable<DTO_BalanceZone> getBalanceZones)
        {
            matrixCtrl.Init(errorHandler, getSchemaNodes, getNodesRelations, getBalanceZones);
        }

        public void ReactionForEnter()
        {
            if (matrixCtrl.gvMatrix.ActiveEditor != null)
            {
                matrixCtrl.gvMatrix.CloseEditor();
                matrixCtrl.gvMatrix.UpdateCurrentRow();
            }
            else
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
        }

        public void ReactionForEsc()
        {
            if (matrixCtrl.gvMatrix.ActiveEditor != null)
            {
                matrixCtrl.gvMatrix.HideEditor();
                matrixCtrl.gvMatrix.UpdateCurrentRow();
            }
            else
            {
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                this.Close();
            }
        }


        public System.Windows.Forms.Control ReturnMainControl()
        {
            return this;
        }

        private void MatrixForm_Load(object sender, System.EventArgs e)
        {
            _filter = new MessageFilter(this);
            Application.AddMessageFilter(_filter);
        }

        private void MatrixForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.RemoveMessageFilter(_filter);
        }

        private void sbtnOK_Click(object sender, System.EventArgs e)
        {

        }
    }
}
