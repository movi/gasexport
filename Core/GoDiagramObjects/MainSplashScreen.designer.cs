﻿namespace GasExportApp
{
    partial class MainSplashScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainSplashScreen));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.lcCompanyName = new DevExpress.XtraEditors.LabelControl();
            this.lcVersion = new DevExpress.XtraEditors.LabelControl();
            this.lcProcess = new DevExpress.XtraEditors.LabelControl();
            this.marqueeProgressBarControl1 = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciImage = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciProgress = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciStartingText = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciVersion = new DevExpress.XtraLayout.LayoutControlItem();
            this.esiEmpty = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lciCompany = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciProgress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciStartingText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.esiEmpty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCompany)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.lcCompanyName);
            this.layoutControl1.Controls.Add(this.lcVersion);
            this.layoutControl1.Controls.Add(this.lcProcess);
            this.layoutControl1.Controls.Add(this.marqueeProgressBarControl1);
            this.layoutControl1.Controls.Add(this.pictureEdit2);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(497, 76, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(369, 286);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // lcCompanyName
            // 
            this.lcCompanyName.Location = new System.Drawing.Point(232, 258);
            this.lcCompanyName.Name = "lcCompanyName";
            this.lcCompanyName.Size = new System.Drawing.Size(125, 13);
            this.lcCompanyName.StyleController = this.layoutControl1;
            this.lcCompanyName.TabIndex = 16;
            this.lcCompanyName.Text = "ООО \"НИИгазэкономика\"";
            // 
            // lcVersion
            // 
            this.lcVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lcVersion.Location = new System.Drawing.Point(12, 258);
            this.lcVersion.Margin = new System.Windows.Forms.Padding(2);
            this.lcVersion.Name = "lcVersion";
            this.lcVersion.Size = new System.Drawing.Size(216, 16);
            this.lcVersion.StyleController = this.layoutControl1;
            this.lcVersion.TabIndex = 17;
            this.lcVersion.Text = "Версия 1.0.0.0";
            // 
            // lcProcess
            // 
            this.lcProcess.Location = new System.Drawing.Point(12, 213);
            this.lcProcess.Name = "lcProcess";
            this.lcProcess.Size = new System.Drawing.Size(112, 13);
            this.lcProcess.StyleController = this.layoutControl1;
            this.lcProcess.TabIndex = 15;
            this.lcProcess.Text = "Запуск приложения...";
            // 
            // marqueeProgressBarControl1
            // 
            this.marqueeProgressBarControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.marqueeProgressBarControl1.EditValue = 0;
            this.marqueeProgressBarControl1.Location = new System.Drawing.Point(12, 230);
            this.marqueeProgressBarControl1.Name = "marqueeProgressBarControl1";
            this.marqueeProgressBarControl1.Size = new System.Drawing.Size(345, 14);
            this.marqueeProgressBarControl1.StyleController = this.layoutControl1;
            this.marqueeProgressBarControl1.TabIndex = 13;
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit2.EditValue = ((object)(resources.GetObject("pictureEdit2.EditValue")));
            this.pictureEdit2.Location = new System.Drawing.Point(12, 12);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.AllowFocused = false;
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Properties.ShowMenu = false;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.pictureEdit2.Size = new System.Drawing.Size(345, 197);
            this.pictureEdit2.StyleController = this.layoutControl1;
            this.pictureEdit2.TabIndex = 11;
            this.pictureEdit2.Visible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciImage,
            this.lciProgress,
            this.lciStartingText,
            this.lciVersion,
            this.esiEmpty,
            this.lciCompany});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(369, 286);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // lciImage
            // 
            this.lciImage.Control = this.pictureEdit2;
            this.lciImage.CustomizationFormText = "layoutControlItem1";
            this.lciImage.Location = new System.Drawing.Point(0, 0);
            this.lciImage.Name = "lciImage";
            this.lciImage.Size = new System.Drawing.Size(349, 201);
            this.lciImage.Text = "lciImage";
            this.lciImage.TextLocation = DevExpress.Utils.Locations.Top;
            this.lciImage.TextSize = new System.Drawing.Size(0, 0);
            this.lciImage.TextVisible = false;
            // 
            // lciProgress
            // 
            this.lciProgress.Control = this.marqueeProgressBarControl1;
            this.lciProgress.ControlAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.lciProgress.CustomizationFormText = "layoutControlItem2";
            this.lciProgress.Location = new System.Drawing.Point(0, 218);
            this.lciProgress.Name = "lciProgress";
            this.lciProgress.Size = new System.Drawing.Size(349, 18);
            this.lciProgress.Text = "lciProgress";
            this.lciProgress.TextSize = new System.Drawing.Size(0, 0);
            this.lciProgress.TextVisible = false;
            // 
            // lciStartingText
            // 
            this.lciStartingText.Control = this.lcProcess;
            this.lciStartingText.ControlAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.lciStartingText.CustomizationFormText = "layoutControlItem3";
            this.lciStartingText.Location = new System.Drawing.Point(0, 201);
            this.lciStartingText.Name = "lciStartingText";
            this.lciStartingText.Size = new System.Drawing.Size(349, 17);
            this.lciStartingText.Text = "lciStartingText";
            this.lciStartingText.TextSize = new System.Drawing.Size(0, 0);
            this.lciStartingText.TextVisible = false;
            // 
            // lciVersion
            // 
            this.lciVersion.Control = this.lcVersion;
            this.lciVersion.ControlAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.lciVersion.CustomizationFormText = "lciVersion";
            this.lciVersion.Location = new System.Drawing.Point(0, 246);
            this.lciVersion.MinSize = new System.Drawing.Size(89, 20);
            this.lciVersion.Name = "lciVersion";
            this.lciVersion.Size = new System.Drawing.Size(220, 20);
            this.lciVersion.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciVersion.Text = "lciVersion";
            this.lciVersion.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lciVersion.TextSize = new System.Drawing.Size(0, 0);
            this.lciVersion.TextToControlDistance = 0;
            this.lciVersion.TextVisible = false;
            // 
            // esiEmpty
            // 
            this.esiEmpty.AllowHotTrack = false;
            this.esiEmpty.CustomizationFormText = "emptySpaceItem2";
            this.esiEmpty.Location = new System.Drawing.Point(0, 236);
            this.esiEmpty.Name = "esiEmpty";
            this.esiEmpty.Size = new System.Drawing.Size(349, 10);
            this.esiEmpty.Text = "esiEmpty";
            this.esiEmpty.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lciCompany
            // 
            this.lciCompany.Control = this.lcCompanyName;
            this.lciCompany.ControlAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.lciCompany.CustomizationFormText = "lciCompany";
            this.lciCompany.Location = new System.Drawing.Point(220, 246);
            this.lciCompany.Name = "lciCompany";
            this.lciCompany.Size = new System.Drawing.Size(129, 20);
            this.lciCompany.Text = "lciCompany";
            this.lciCompany.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.lciCompany.TextSize = new System.Drawing.Size(0, 0);
            this.lciCompany.TextToControlDistance = 0;
            this.lciCompany.TextVisible = false;
            // 
            // MainSplashScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 286);
            this.Controls.Add(this.layoutControl1);
            this.Name = "MainSplashScreen";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciProgress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciStartingText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.esiEmpty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCompany)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.LabelControl lcProcess;
        private DevExpress.XtraEditors.MarqueeProgressBarControl marqueeProgressBarControl1;
        private DevExpress.XtraLayout.LayoutControlItem lciProgress;
        private DevExpress.XtraLayout.LayoutControlItem lciStartingText;
        private DevExpress.XtraEditors.LabelControl lcVersion;
        private DevExpress.XtraLayout.LayoutControlItem lciVersion;
        private DevExpress.XtraLayout.EmptySpaceItem esiEmpty;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraLayout.LayoutControlItem lciImage;
        private DevExpress.XtraEditors.LabelControl lcCompanyName;
        private DevExpress.XtraLayout.LayoutControlItem lciCompany;

    }
}
