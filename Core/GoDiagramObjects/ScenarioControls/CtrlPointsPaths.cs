﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GEx.Iface;

namespace CommonUIControls
{
    public partial class CtrlPointsPaths : UserControl
    {
        public CtrlPointsPaths()
        {
            InitializeComponent();
        }

        public void Init()
        {
            ctrlSelectionPointPath1.ChosePoint += ChoseFirstPoint;
            ctrlSelectionPointPath1.RemovePoint += RemoveChoseFirstPoint;
            ctrlSelectionPointPath2.ChosePoint += ChoseLastPoint;
            ctrlSelectionPointPath2.RemovePoint += RemoveChoseLastPoint;
        }

        public Control FirstSelectControl
        {
            get { return ctrlSelectionPointPath1.SelectPointControl; }
        }

        public Control FirstAllControl
        {
            get { return ctrlSelectionPointPath1.SelectAllControl; }
        }

        public Control LastSelectControl
        {
            get { return ctrlSelectionPointPath2.SelectPointControl; }
        }

        public Control LastAllControl
        {
            get { return ctrlSelectionPointPath2.SelectAllControl; }
        }

        public Action<DTO_Node> ChoseFirstPoint;
        public Action<DTO_Node> ChoseLastPoint;
        public Action<DTO_Node> RemoveChoseFirstPoint;
        public Action<DTO_Node> RemoveChoseLastPoint;
    }
}
