﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.XtraGrid.Views.Grid;

namespace CommonUIControls.ScenarioControls
{
    public delegate void CallBackChoiceScenario(int scenID);

    public partial class CtrlScenariosList : DevExpress.XtraEditors.XtraUserControl, IScenarioControl
    {
        private const string ColumnId = "ScenID";
        private const string ColumnName = "Name";
        private const string ColumnComment = "Comment";
        private const string ColumnAutor = "Author";
        private const string ColumnDate = "CreationDate";
        protected CallBackChoiceScenario _getDelegateChoiceScenario;
        /// <summary>
        /// Идентификатор выбранного сценария
        /// </summary>
        private int? _ScenId;
        private ScenariosListPresenter _presenter; 

        public int? SelectedScenario
        {
            get { return _ScenId; }
        }

        public CtrlScenariosList()
        {
            InitializeComponent();
            //_presenter = presenter;          
        }

        public void Init(ScenariosListPresenter presenter, CallBackChoiceScenario Delegate)
        {
            _presenter = presenter;
            _getDelegateChoiceScenario = Delegate;
        }

        /// <summary>
        /// Инициализировать список доступных сценариев
        /// </summary>       
        public void InitControlData()
        {
            int? currScenarioID = _ScenId;             
            try
            {
                this.gvScenarios.BeginUpdate();                
                this.gcScenarios.DataSource = _presenter.ShowDataForCtrl();
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit cEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                cEdit.DisplayFormat.FormatString = "dd.MM.yy H:mm:ss";
                this.gvScenarios.Columns[ColumnDate].ColumnEdit = cEdit; 
                GridAfterInit();
                UpdateCtrl();
                if (currScenarioID != null)
                {
                    gvScenarios.MoveLast();
                    var i = gvScenarios.RowCount;
                    while ((int)gvScenarios.GetFocusedRowCellValue(ColumnId) != currScenarioID && i > 0)
                    {
                        gvScenarios.MovePrev();
                        i--;
                    }
                    _ScenId = currScenarioID;
                }
                else gvScenarios.MoveFirst();
            }
            finally
            {
                gvScenarios.EndUpdate();
            }        
        }

        private void GridAfterInit()
        {
            this.gvScenarios.Columns[ColumnId].Visible = false;
            this.gvScenarios.Columns[ColumnAutor].OptionsColumn.AllowEdit = false;
            this.gvScenarios.Columns[ColumnDate].OptionsColumn.AllowEdit = false;
            this.gvScenarios.Columns[ColumnName].OptionsColumn.AllowEdit = true;
            this.gvScenarios.BestFitColumns();
        }

        private void gvScenarios_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _ScenId = (int)gvScenarios.GetFocusedRowCellValue(gvScenarios.Columns["ScenID"]);            
        }

        private void gvScenarios_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            var curRow = gvScenarios.GetFocusedRow();
            if (curRow != null) _ScenId = (int)gvScenarios.GetFocusedRowCellValue(gvScenarios.Columns["ScenID"]);
            gvScenarios.Columns[ColumnName].OptionsColumn.AllowEdit = false;
            gvScenarios.OptionsBehavior.Editable = false;
            gvScenarios.OptionsBehavior.ReadOnly = true;
            gvScenarios.OptionsSelection.EnableAppearanceFocusedCell = false;
            gvScenarios.Columns[ColumnName].OptionsColumn.AllowEdit = false;            
        }

        private void gvScenarios_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {             
        }

        private void gvScenarios_RowClick(object sender, RowClickEventArgs e)
        {            
            if (e.Clicks == 2 && _ScenId != null)
            {
                if (_getDelegateChoiceScenario != null) _getDelegateChoiceScenario((int)_ScenId);
            }             
        }

        private void gvScenarios_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
           // e.
        }

        private void gvScenarios_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
           
        }

        private void tsmiLoad_Click(object sender, EventArgs e)
        {
            if (_ScenId != null)
                if (_getDelegateChoiceScenario != null) _getDelegateChoiceScenario((int)_ScenId);            
        }

        private void tsmiRemove_Click(object sender, EventArgs e)
        {
            const string errorSource = "CtrlScenariosList::iRemove_ItemClick()";
            try
            {
                if (SelectedScenario.HasValue &&
                MessageBox.Show("Вы действительно хотите удалить выбранный сценарий?", "Удаление сценария", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    _presenter.RemoveScenario((int)SelectedScenario);
                    InitControlData();
                    var curRow = gvScenarios.GetFocusedRow();
                    if (curRow != null) _ScenId = (int)gvScenarios.GetFocusedRowCellValue(gvScenarios.Columns["ScenID"]);
                    else _ScenId = null;
                }
            }
            catch (Exception ex)
            {
                _presenter.WriteLogError(ex.Message, errorSource);
            } 
        }
        
        private void cEdit_Validating(object sender, CancelEventArgs e)
        {
            if (_ScenId != null)
            {
                _presenter.SaveCurrentHeaderName((int)_ScenId, (sender as DevExpress.XtraEditors.TextEdit).Text);
                e.Cancel = false;
            }
            //gvScenarios.OptionsSelection.EnableAppearanceFocusedCell = false;
        }

        private void iRemove_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            const string errorSource = "CtrlScenariosList::iRemove_ItemClick()";
            try
            {
                if (SelectedScenario.HasValue &&
                MessageBox.Show("Вы действительно хотите удалить выбранный сценарий?", "Удаление сценария", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    _presenter.RemoveScenario((int)SelectedScenario);
                   InitControlData();                  
                }
            }
            catch (Exception ex)
            {
                _presenter.WriteLogError(ex.Message, errorSource);
            }              
        }

        private void iRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            const string errorSource = "CtrlScenariosList::iRefresh_ItemClick()";
            try
            {
                InitControlData();
                UpdateCtrl();
            }
            catch (Exception ex)
            {
                _presenter.WriteLogError(ex.Message, errorSource);
            }
        }

        private void iCreate_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            /*
            const string errorSource = "CtrlScenariosList::iCreate_ItemClick()";
            try
            {
                _presenter.DefaultScenario();
                UpdateCtrl();
            }
            catch (Exception ex)
            {
                _presenter.WriteLogError(ex.Message, errorSource);
            }  
            */ 
        }

        private void iOpen_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            /*
            const string errorSource = "CtrlScenariosList::iOpen_ItemClick()";
            try
            {
                if (SelectedScenario.HasValue)
                {
                    _presenter.LoadScenario((int)SelectedScenario);
                    UpdateCtrl();
                }                
            }
            catch (Exception ex)
            {
                _presenter.WriteLogError(ex.Message, errorSource);
            } 
            */ 
        }

        private void iSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            /*
            const string errorSource = "CtrlScenariosList::iSave_ItemClick()";
            try
            {
                if (!_presenter.AllowSaveScenario())
                {
                    MessageBox.Show("В схеме есть новые элементы, сначала сохраните схему.", "Ошибка сохранения");
                    return;
                }

                if (!_presenter.isNewScenario())
                    _presenter.SaveScenario();
                else
                {
                    iSaveAs_ItemClick(sender, e);
                    return;
                }
                _presenter.SaveScenario();                
                InitControlData();
                //siStatus.Caption = "Cохранен сценарий '" + teCurrentScenarioName.Text + "'";
            }
            catch (Exception ex)
            {
                _presenter.WriteLogError(ex.Message, errorSource);
            }
             */ 
        }

        private void iSaveAs_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            /*
            InputString frm = new InputString("Сохранить сценарий как новый", "Введите название сценария", _presenter.GetScenarioName());
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                const string errorSource = "CtrlScenariosList::iSaveAs_ItemClick()";
                try
                {
                    if (!_presenter.AllowSaveScenario())
                    {
                        MessageBox.Show("В схеме есть новые элементы, сначала сохраните схему.", "Ошибка сохранения");
                        return;
                    }
                    _presenter.UpdateScenarioName(frm.Value);
                    _presenter.SaveScenarioAs();
                    InitControlData();                    
                    //teCurrentScenarioName.Text = frm.Value + " (" + DateTime.Today.ToLongDateString() + ")";
                    
                }
                catch (Exception ex)
                {
                    _presenter.WriteLogError(ex.Message, errorSource);
                }
            }
             */ 
        }
         
        public void UpdateCtrl()
        {
 	       
        }

        private void gvScenarios_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            e.Appearance.BackColor = e.Appearance.BackColor2 = e.RowHandle % 2 == 0 ? Color.White : Color.LightCyan;
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {            
            bool isEnabled = false;
            if (gvScenarios.GetFocusedRow() != null)
            {
                var focusedRowCellValue = gvScenarios.GetFocusedRowCellValue(gvScenarios.Columns[ColumnAutor]).ToString().ToUpperInvariant();
                isEnabled = focusedRowCellValue.Equals((Environment.UserDomainName + "\\" + Environment.UserName).ToUpperInvariant());
            }
            tsmiOptions.Enabled = tsmiRemove.Enabled = isEnabled;
        }

        private void gvScenarios_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            
        }

        private void tsmiRename_Click(object sender, EventArgs e)
        {
            if (gvScenarios.GetFocusedRow() != null)
            {
                gvScenarios.Columns[ColumnName].OptionsColumn.AllowEdit = true;
                gvScenarios.OptionsBehavior.Editable = true;
                gvScenarios.OptionsBehavior.ReadOnly = false;
                gvScenarios.OptionsSelection.EnableAppearanceFocusedCell = true;
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit cEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                gvScenarios.Columns[ColumnName].ColumnEdit = cEdit;
                gvScenarios.Columns[ColumnName].OptionsColumn.AllowEdit = true;
                cEdit.Validating += cEdit_Validating;
                //cEdit.Modified += cEdit_EditValueChanged;
                cEdit.AllowFocused = true;
                gvScenarios.FocusedColumn = gvScenarios.Columns[ColumnName];

                gvScenarios.SelectCell(gvScenarios.FocusedRowHandle, gvScenarios.Columns[ColumnName]);
                gvScenarios.ShowEditor();
                if (gvScenarios.ActiveEditor != null)
                    gvScenarios.ActiveEditor.Focus();
            }            
        }

        private void tsmiComment_Click(object sender, EventArgs e)
        {
            if (gvScenarios.GetFocusedRow() != null)
            {
                gvScenarios.Columns[ColumnComment].OptionsColumn.AllowEdit = true;
                gvScenarios.OptionsBehavior.Editable = true;
                gvScenarios.OptionsBehavior.ReadOnly = false;
                gvScenarios.OptionsSelection.EnableAppearanceFocusedCell = true;
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit cEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                gvScenarios.Columns[ColumnComment].ColumnEdit = cEdit;
                gvScenarios.Columns[ColumnComment].OptionsColumn.AllowEdit = true;
                cEdit.Validating += comEdit_Validating;
                //cEdit.Modified += cEdit_EditValueChanged;
                cEdit.AllowFocused = true;
                gvScenarios.FocusedColumn = gvScenarios.Columns[ColumnComment];

                gvScenarios.SelectCell(gvScenarios.FocusedRowHandle, gvScenarios.Columns[ColumnComment]);
                gvScenarios.ShowEditor();
                if (gvScenarios.ActiveEditor != null)
                    gvScenarios.ActiveEditor.Focus();
            }            
        }

        private void comEdit_Validating(object sender, CancelEventArgs e)
        {
            if (_ScenId != null)
            {
                _presenter.SaveCurrentHeaderComment((int)_ScenId, (sender as DevExpress.XtraEditors.TextEdit).Text);
                e.Cancel = false;
            }
        }
}
    
}