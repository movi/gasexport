﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using GEx.ErrorHandler;
using GEx.Iface;

namespace CommonUIControls.ScenarioControls
{

    /// <summary>
    /// Класс описания рег. данных о сети
    /// </summary>
    public class ScenarioInfo
    {
        [DisplayName("Id")]
        public Int32 ScenID { get; set; }

        [DisplayName("Наименование")]
        public String Name { get; set; }

        [DisplayName("Комментарий")]
        public String Comment { get; set; }

        [DisplayName("Дата последнего редактирования")]
        public DateTime CreationDate { get; set; }

        [DisplayName("Автор")]
        public string Author { get; set; }        

        public int SortByDateAscending(DateTime DateTime1, DateTime DateTime2)
        {
            return DateTime1.CompareTo(DateTime2);
        }

    }

    public class ScenariosListPresenter 
    {  
        #region Model
               
        private GEx.Iface.IGExDomain _coreHolder;
        private GEx.ErrorHandler.IErrorOutput _errorHandler;
        private List<ScenarioInfo> _dataSource;
        private IScenarioControl _ctrl;
        #endregion

        public ScenariosListPresenter(GEx.Iface.IGExDomain coreHolder, GEx.ErrorHandler.IErrorOutput eh, IScenarioControl ctrl)
        {            
            _coreHolder = coreHolder;
            _errorHandler = eh;
            _ctrl = ctrl;
        }

        private void PrepareDataForCtrl()
        {
            //IEnumerable<CalculationScenario> scenars
            var result = new List<ScenarioInfo>();
            var Scenarios = _coreHolder.LoadCalculationScenarios();

            foreach (var scen in Scenarios)
            {
                ScenarioInfo geScen = new ScenarioInfo
                {
                    Name = scen.Name,
                    ScenID = Convert.ToInt32(scen.ID),
                    CreationDate = Convert.ToDateTime(scen.CreationDate),
                    Author = scen.Author,
                    Comment = scen.Comment
                };
                result.Add(geScen);
            }
            result.Sort((x, y) => y.CreationDate.CompareTo(x.CreationDate));
            _dataSource = result;
        }

        public object ShowDataForCtrl()
        {
            PrepareDataForCtrl();
            return (object)_dataSource;
        }
        
        public void RemoveScenario(int IDscenario)
        {
            _coreHolder.RemoveScenario(IDscenario);
        }

        public void SaveCurrentHeaderName(int Id, string Name)
        {
            _coreHolder.UpdateScenarioName(Id, Name);
        }

        public void SaveCurrentHeaderComment(int Id, string comm)
        {
            _coreHolder.UpdateScenarioComment(Id, comm);
        }
       
        public void WriteLogError(string sMsg, string sSource)
        {
            _errorHandler.ReportError(eErrClass.EC_ERROR, sMsg, sSource);
        }                
        
        public string GetScenarioDescription()
        {
            var t = _coreHolder.GetScenario();
            return t == null ? string.Empty : t.Name + " (" + t.CreationDate.ToLongDateString() + ")";
        }        
    }
}
