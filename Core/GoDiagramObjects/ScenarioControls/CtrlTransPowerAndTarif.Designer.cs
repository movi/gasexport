﻿
namespace CommonUIControls.ScenarioControls
{
    partial class CtrlTransPowerAndTarif
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcPowerAndTarifInfo = new DevExpress.XtraGrid.GridControl();
            this.gvPowerAndTarifInfo = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            ((System.ComponentModel.ISupportInitialize)(this.gcPowerAndTarifInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPowerAndTarifInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // gcPowerAndTarifInfo
            // 
            this.gcPowerAndTarifInfo.Cursor = System.Windows.Forms.Cursors.Default;
            this.gcPowerAndTarifInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcPowerAndTarifInfo.Location = new System.Drawing.Point(0, 0);
            this.gcPowerAndTarifInfo.MainView = this.gvPowerAndTarifInfo;
            this.gcPowerAndTarifInfo.Name = "gcPowerAndTarifInfo";
            this.gcPowerAndTarifInfo.Size = new System.Drawing.Size(377, 308);
            this.gcPowerAndTarifInfo.TabIndex = 2;
            this.gcPowerAndTarifInfo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvPowerAndTarifInfo});
            // 
            // gvPowerAndTarifInfo
            // 
            this.gvPowerAndTarifInfo.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gvPowerAndTarifInfo.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvPowerAndTarifInfo.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gvPowerAndTarifInfo.Appearance.Row.Options.UseFont = true;
            this.gvPowerAndTarifInfo.GridControl = this.gcPowerAndTarifInfo;
            this.gvPowerAndTarifInfo.Name = "gvPowerAndTarifInfo";
            this.gvPowerAndTarifInfo.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
            this.gvPowerAndTarifInfo.OptionsCustomization.AllowGroup = false;
            this.gvPowerAndTarifInfo.OptionsNavigation.AutoMoveRowFocus = false;
            this.gvPowerAndTarifInfo.OptionsView.AllowCellMerge = true;
            this.gvPowerAndTarifInfo.OptionsView.ColumnAutoWidth = false;
            this.gvPowerAndTarifInfo.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.gvPowerAndTarifInfo.OptionsView.ShowGroupPanel = false;
            this.gvPowerAndTarifInfo.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gvPowerAndTarifInfo_RowCellClick);
            this.gvPowerAndTarifInfo.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gvPowerAndTarifInfo_CustomDrawCell);
            this.gvPowerAndTarifInfo.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvPowerAndTarifInfo_FocusedRowChanged);
            this.gvPowerAndTarifInfo.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvPowerAndTarifInfo_CellValueChanged);
            this.gvPowerAndTarifInfo.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gvPowerAndTarifInfo_RowUpdated);
            this.gvPowerAndTarifInfo.GotFocus += new System.EventHandler(this.gvPowerAndTarifInfo_GotFocus);
            // 
            // CtrlTransPowerAndTarif
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcPowerAndTarifInfo);
            this.Name = "CtrlTransPowerAndTarif";
            this.Size = new System.Drawing.Size(377, 308);
            ((System.ComponentModel.ISupportInitialize)(this.gcPowerAndTarifInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPowerAndTarifInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcPowerAndTarifInfo;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gvPowerAndTarifInfo;
    }
}
