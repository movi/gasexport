﻿
namespace CommonUIControls.ScenarioControls
{
    partial class CtrlStorageExtraction
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcExtractionInfo = new DevExpress.XtraGrid.GridControl();
            this.gvExtractionInfo = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.gcExtractionInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvExtractionInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // gcExtractionInfo
            // 
            this.gcExtractionInfo.Cursor = System.Windows.Forms.Cursors.Default;
            this.gcExtractionInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcExtractionInfo.Location = new System.Drawing.Point(0, 0);
            this.gcExtractionInfo.MainView = this.gvExtractionInfo;
            this.gcExtractionInfo.Name = "gcExtractionInfo";
            this.gcExtractionInfo.Size = new System.Drawing.Size(377, 308);
            this.gcExtractionInfo.TabIndex = 2;
            this.gcExtractionInfo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvExtractionInfo});
            // 
            // gvExtractionInfo
            // 
            this.gvExtractionInfo.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gvExtractionInfo.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvExtractionInfo.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gvExtractionInfo.Appearance.Row.Options.UseFont = true;
            this.gvExtractionInfo.GridControl = this.gcExtractionInfo;
            this.gvExtractionInfo.Name = "gvExtractionInfo";
            this.gvExtractionInfo.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
            this.gvExtractionInfo.OptionsCustomization.AllowGroup = false;
            this.gvExtractionInfo.OptionsNavigation.AutoMoveRowFocus = false;
            this.gvExtractionInfo.OptionsView.ColumnAutoWidth = false;
            this.gvExtractionInfo.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.gvExtractionInfo.OptionsView.ShowGroupPanel = false;
            this.gvExtractionInfo.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gvExtractionInfo_CustomDrawCell);
            this.gvExtractionInfo.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvExtractionInfo_FocusedRowChanged);
            this.gvExtractionInfo.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvExtractionInfo_CellValueChanged);
            this.gvExtractionInfo.GotFocus += new System.EventHandler(this.gvExtractionInfo_GotFocus);
            // 
            // CtrlStorageExtraction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcExtractionInfo);
            this.Name = "CtrlStorageExtraction";
            this.Size = new System.Drawing.Size(377, 308);
            ((System.ComponentModel.ISupportInitialize)(this.gcExtractionInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvExtractionInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcExtractionInfo;
        private DevExpress.XtraGrid.Views.Grid.GridView gvExtractionInfo;


    }
}
