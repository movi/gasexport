﻿
namespace CommonUIControls.ScenarioControls
{
    partial class CtrlScenariosList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CtrlScenariosList));
            this.gcScenarios = new DevExpress.XtraGrid.GridControl();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiOptions = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRename = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiComment = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.gvScenarios = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.ribbonImageCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.ribbonImageCollectionLarge = new DevExpress.Utils.ImageCollection(this.components);
            this.applicationMenu1 = new DevExpress.XtraBars.Ribbon.ApplicationMenu(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gcScenarios)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvScenarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollectionLarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).BeginInit();
            this.SuspendLayout();
            // 
            // gcScenarios
            // 
            this.gcScenarios.ContextMenuStrip = this.contextMenuStrip1;
            this.gcScenarios.Cursor = System.Windows.Forms.Cursors.Default;
            this.gcScenarios.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcScenarios.Location = new System.Drawing.Point(0, 0);
            this.gcScenarios.MainView = this.gvScenarios;
            this.gcScenarios.Name = "gcScenarios";
            this.gcScenarios.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.gcScenarios.Size = new System.Drawing.Size(439, 484);
            this.gcScenarios.TabIndex = 2;
            this.gcScenarios.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvScenarios});
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiOptions,
            this.toolStripSeparator1,
            this.tsmiLoad,
            this.tsmiRemove});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.ShowImageMargin = false;
            this.contextMenuStrip1.Size = new System.Drawing.Size(152, 76);
            this.contextMenuStrip1.Text = "Удалить расчет";
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // tsmiOptions
            // 
            this.tsmiOptions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiRename,
            this.tsmiComment});
            this.tsmiOptions.Name = "tsmiOptions";
            this.tsmiOptions.Size = new System.Drawing.Size(151, 22);
            this.tsmiOptions.Text = "Редактировать";
            // 
            // tsmiRename
            // 
            this.tsmiRename.Name = "tsmiRename";
            this.tsmiRename.Size = new System.Drawing.Size(152, 22);
            this.tsmiRename.Text = "Наименование";
            this.tsmiRename.Click += new System.EventHandler(this.tsmiRename_Click);
            // 
            // tsmiComment
            // 
            this.tsmiComment.Name = "tsmiComment";
            this.tsmiComment.Size = new System.Drawing.Size(152, 22);
            this.tsmiComment.Text = "Комментарий";
            this.tsmiComment.Click += new System.EventHandler(this.tsmiComment_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(148, 6);
            // 
            // tsmiLoad
            // 
            this.tsmiLoad.Name = "tsmiLoad";
            this.tsmiLoad.Size = new System.Drawing.Size(151, 22);
            this.tsmiLoad.Text = "Загрузить сценарий";
            this.tsmiLoad.ToolTipText = "Удалить все расчеты";
            this.tsmiLoad.Click += new System.EventHandler(this.tsmiLoad_Click);
            // 
            // tsmiRemove
            // 
            this.tsmiRemove.Name = "tsmiRemove";
            this.tsmiRemove.Size = new System.Drawing.Size(151, 22);
            this.tsmiRemove.Text = "Удалить сценарий";
            this.tsmiRemove.ToolTipText = "Удалить активный расчет";
            this.tsmiRemove.Click += new System.EventHandler(this.tsmiRemove_Click);
            // 
            // gvScenarios
            // 
            this.gvScenarios.Appearance.FocusedCell.BorderColor = System.Drawing.Color.Transparent;
            this.gvScenarios.Appearance.FocusedCell.Options.UseBorderColor = true;
            this.gvScenarios.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.gvScenarios.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvScenarios.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gvScenarios.Appearance.Row.Options.UseFont = true;
            this.gvScenarios.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.gvScenarios.GridControl = this.gcScenarios;
            this.gvScenarios.Name = "gvScenarios";
            this.gvScenarios.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvScenarios.OptionsBehavior.Editable = false;
            this.gvScenarios.OptionsBehavior.ReadOnly = true;
            this.gvScenarios.OptionsCustomization.AllowFilter = false;
            this.gvScenarios.OptionsCustomization.AllowGroup = false;
            this.gvScenarios.OptionsCustomization.AllowQuickHideColumns = false;
            this.gvScenarios.OptionsNavigation.AutoMoveRowFocus = false;
            this.gvScenarios.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvScenarios.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.gvScenarios.OptionsView.RowAutoHeight = true;
            this.gvScenarios.OptionsView.ShowGroupPanel = false;
            this.gvScenarios.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gvScenarios_RowClick);
            this.gvScenarios.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gvScenarios_CustomDrawCell);
            this.gvScenarios.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gvScenarios_PopupMenuShowing);
            this.gvScenarios.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gvScenarios_SelectionChanged);
            this.gvScenarios.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvScenarios_FocusedRowChanged);
            this.gvScenarios.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvScenarios_ValidateRow);
            this.gvScenarios.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gvScenarios_RowUpdated);
            this.gvScenarios.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gvScenarios_ValidatingEditor);
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.DisplayFormat.FormatString = "dd.MM.yy hh:mm:ss";
            this.repositoryItemTextEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // ribbonImageCollection
            // 
            this.ribbonImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ribbonImageCollection.ImageStream")));
            this.ribbonImageCollection.Images.SetKeyName(0, "Ribbon_New_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(1, "Ribbon_Open_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(2, "Ribbon_Close_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(3, "Ribbon_Find_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(4, "Ribbon_Save_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(5, "Ribbon_SaveAs_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(6, "Ribbon_Exit_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(7, "Ribbon_Content_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(8, "Ribbon_Info_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(9, "Ribbon_Bold_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(10, "Ribbon_Italic_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(11, "Ribbon_Underline_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(12, "Ribbon_AlignLeft_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(13, "Ribbon_AlignCenter_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(14, "Ribbon_AlignRight_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(15, "Cursor_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(16, "green.png");
            this.ribbonImageCollection.Images.SetKeyName(17, "orange.png");
            this.ribbonImageCollection.Images.SetKeyName(18, "red.png");
            this.ribbonImageCollection.Images.SetKeyName(19, "violet.png");
            this.ribbonImageCollection.Images.SetKeyName(20, "Play_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(21, "Refresh_16x16.png");
            // 
            // ribbonImageCollectionLarge
            // 
            this.ribbonImageCollectionLarge.ImageSize = new System.Drawing.Size(32, 32);
            this.ribbonImageCollectionLarge.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ribbonImageCollectionLarge.ImageStream")));
            this.ribbonImageCollectionLarge.Images.SetKeyName(0, "Ribbon_New_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(1, "Ribbon_Open_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(2, "Ribbon_Close_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(3, "Ribbon_Find_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(4, "Ribbon_Save_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(5, "Ribbon_SaveAs_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(6, "Ribbon_Exit_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(7, "Ribbon_Content_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(8, "Ribbon_Info_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(9, "Cursor_16x16.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(10, "gnome-mime-application-msword.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(11, "Play_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(12, "Refresh_32x32.png");
            // 
            // applicationMenu1
            // 
            this.applicationMenu1.Name = "applicationMenu1";
            // 
            // CtrlScenariosList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcScenarios);
            this.Name = "CtrlScenariosList";
            this.Size = new System.Drawing.Size(439, 484);
            ((System.ComponentModel.ISupportInitialize)(this.gcScenarios)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvScenarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollectionLarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraGrid.GridControl gcScenarios;
        public DevExpress.XtraGrid.Views.Grid.GridView gvScenarios;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmiOptions;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem tsmiLoad;
        private System.Windows.Forms.ToolStripMenuItem tsmiRemove;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.Utils.ImageCollection ribbonImageCollection;
        private DevExpress.Utils.ImageCollection ribbonImageCollectionLarge;
        private DevExpress.XtraBars.Ribbon.ApplicationMenu applicationMenu1;
        private System.Windows.Forms.ToolStripMenuItem tsmiRename;
        private System.Windows.Forms.ToolStripMenuItem tsmiComment;


    }
}
