﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using CommonUIControls.Helper;
using DevExpress.XtraGrid.Columns;

namespace CommonUIControls.ScenarioControls
{
    public partial class CtrlContractsVolume : DevExpress.XtraEditors.XtraUserControl, IScenarioControl
    {
        private const string ColumnObjName = "ItemName";
        private const string ColumnVolume = "ItemValue";
        private const string ColumnNode = "IdNode";
        private const string ColumnActive = "isActive";

        private IValuesPresenterResultsForControl _presenter;        
        //private ToolStripMenuItem _excelToolStripMenuItem;

        public CtrlContractsVolume()
        {
            InitializeComponent();

            PopupHelper.Controls = new Control[] { gcVolumeInfo };
        }

        public void SetPresenter(IValuesPresenterResultsForControl presenter)
        {
            _presenter = presenter;
        }

        public void InitControlData()
        {
            try
            {
                gvVolumeInfo.BeginUpdate();
                gcVolumeInfo.DataSource = _presenter.ShowDataForCtrl();
                GridAfterInit();
            }
            finally
            {
                gvVolumeInfo.EndUpdate();
            }
        }

        private void GridAfterInit()
        {
            gvVolumeInfo.BeginUpdate();
            Graphics gr = gcVolumeInfo.CreateGraphics();
            gvVolumeInfo.Columns[ColumnActive].Visible = gvVolumeInfo.Columns[ColumnNode].Visible = false;
            gvVolumeInfo.Columns[ColumnObjName].Fixed = FixedStyle.Left;
            gvVolumeInfo.Columns[ColumnObjName].OptionsColumn.ReadOnly /*= gvVolumeInfo.Columns[ColumnVolume].OptionsColumn.ReadOnly*/ = true;
            gvVolumeInfo.Columns[ColumnObjName].OptionsColumn.AllowEdit /* = gvVolumeInfo.Columns[ColumnVolume].OptionsColumn.AllowEdit*/ =
            gvVolumeInfo.Columns[ColumnObjName].OptionsColumn.AllowFocus /*= gvVolumeInfo.Columns[ColumnVolume].OptionsColumn.AllowFocus*/ = false;

            List<ContractsVolumeViewItem> listItems = (List<ContractsVolumeViewItem>)gcVolumeInfo.DataSource;

            int wdth = (int)gr.MeasureString("Узел", gvVolumeInfo.Appearance.Row.Font).Width;
            foreach (var pth in listItems)
            {
                var tmp = gr.MeasureString(pth.ItemName, gvVolumeInfo.Appearance.Row.Font);
                if (wdth < (int)tmp.Width)
                {
                    wdth = (int)tmp.Width;
                }
            }
            gvVolumeInfo.Columns[ColumnVolume].Width = 105;
            gvVolumeInfo.Columns[ColumnObjName].Width = wdth + 20;
            var edit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
              
            edit.KeyDown += edit_KeyDown;
            edit.EditValueChanging += edit_EditValueChanging;
            edit.EditValueChanged += EditOnEditValueChanged;
            gvVolumeInfo.Columns[ColumnVolume].ColumnEdit = edit;
            //gvVolumeInfo.BestFitColumns();
            gvVolumeInfo.EndUpdate();
        }

        private void EditOnEditValueChanged(object sender, EventArgs eventArgs)
        {
            gvVolumeInfo.PostEditor();
//            gvVolumeInfo.CloseEditor();
//            gvVolumeInfo.UpdateCurrentRow();
        }

        private void edit_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            double tmp;
            if (e.NewValue != null && e.NewValue.ToString().Length > 0 && !double.TryParse(e.NewValue.ToString(), out tmp))
            {
                e.Cancel = true;   
                //e.NewValue = e.OldValue;
                //(sender as DevExpress.XtraEditors.TextEdit).SelectionStart = (sender as DevExpress.XtraEditors.TextEdit).Text.Length - 1;
            }
        }

        private void edit_KeyDown(object sender, KeyEventArgs e)
        {
            var tmp = (sender as DevExpress.XtraEditors.TextEdit);
            if (tmp.SelectionLength == tmp.Text.Length)
            {
                if (e.KeyCode == Keys.Left)
                {
                    tmp.SelectionStart = 0;
                    tmp.SelectionLength = 0;
                }
                else if (e.KeyCode == Keys.Right)
                {
                    tmp.SelectionStart = tmp.Text.Length - 1;
                    tmp.SelectionLength = 0;
                }
            }
        }

        public void UpdateCtrl()
        {
            InitControlData();
        }

        private void gvVolumeInfo_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column == gvVolumeInfo.Columns[ColumnVolume])
            {
                _presenter.SetValue(gvVolumeInfo.GetFocusedRow());
            }
        }

        private void gvVolumeInfo_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            bool isActive;
            try
            {
                isActive = (bool)gvVolumeInfo.GetRowCellValue(e.RowHandle, ColumnActive);                
            }
            catch 
            {
                isActive = false;
            }
            Color curColor = e.RowHandle % 2 == 0 ? Color.White : Color.LightCyan;
            e.Appearance.BackColor = e.Appearance.BackColor2 = isActive ? curColor : Color.LightPink;
        }

        private void gvVolumeInfo_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0 && (gcVolumeInfo.DataSource as List<ContractsVolumeViewItem>).Count > e.FocusedRowHandle)
            {
                bool isActive;
                try
                {

                    isActive = (bool)gvVolumeInfo.GetRowCellValue(e.FocusedRowHandle, ColumnActive);

                }
                catch
                {
                    isActive = false;
                }
                gvVolumeInfo.Columns[ColumnVolume].OptionsColumn.ReadOnly = !isActive;
                gvVolumeInfo.Columns[ColumnVolume].OptionsColumn.AllowEdit = gvVolumeInfo.Columns[ColumnVolume].OptionsColumn.AllowFocus = isActive;
            }
        }

        private void gvVolumeInfo_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {

        }

        private void gvVolumeInfo_GotFocus(object sender, EventArgs e)
        {
            bool isActive;
            if (gvVolumeInfo.RowCount > 0 && gvVolumeInfo.FocusedRowHandle >= 0)
            {
                isActive = (bool)gvVolumeInfo.GetFocusedRowCellValue(ColumnActive);
            }
            else
            {
                isActive = false;
            }
            gvVolumeInfo.Columns[ColumnVolume].OptionsColumn.ReadOnly = !isActive;
            gvVolumeInfo.Columns[ColumnVolume].OptionsColumn.AllowEdit = gvVolumeInfo.Columns[ColumnVolume].OptionsColumn.AllowFocus = isActive;
        }

    }
}