﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using GEx.Iface;

namespace CommonUIControls.ScenarioControls
{  

    /// <summary>
    /// Тип данных для отображения в гриде
    /// </summary>
    public class StorageExtractionViewItem
    {
        //Узел
        [DisplayName("Узел")]
        public string ItemName { get; internal set; }

        //Макс. объём отбора, млн м3 - это входное поле, редактируется вручную 
        [DisplayName("Макс. объём, млн м³")]
        public double? ItemValue { get; set; }

        //сохранить, но в гриде не отображать
        public int IdNode { get; set; }

        //Возможно ли редактирование узла сценария
        public bool isActive { get; set; }
    }

    public class StorageExtractionPresenterContainer
    {
        //internal INodeEdgeValuesContainer<EdgeSolutionBase, NodeSolutionBase> DocResults;
        //internal ICapacityProcessor CapProcessor;
        //internal MinMaxFlowsDynamicsContainer MinMaxFlowsDynamics;
        //internal IPipeCapacities PipeCaps;
    }

    public class StorageExtractionPresenter : IValuesPresenterDataAccess, IValuesPresenterResultsForControl, IGExDomainCallback
    {
        #region Model

        private StorageExtractionPresenterContainer _container;
        private GEx.Iface.IGExDomain _coreHolder;
        private GEx.ErrorHandler.IErrorOutput _errorHandler; 
        private List<StorageExtractionViewItem> _dataSource;
        private IScenarioControl _ctrl;
        #endregion

        public StorageExtractionPresenter(GEx.Iface.IGExDomain coreHolder, GEx.ErrorHandler.IErrorOutput eh, IScenarioControl ctrl)
        {
            _container = new StorageExtractionPresenterContainer();
            _coreHolder = coreHolder;
            _errorHandler = eh;
            _ctrl = ctrl;
        }
        
        private void PrepareDataForCtrl()
        {
            var result = new List<StorageExtractionViewItem>();
            var list1 = _coreHolder.LoadUGSScenarioData() ?? new List<DTO_UGSScenarioData>(); 
            List<DTO_UGSScenarioData> list = list1.ToList();

            foreach (var cScen in list)
            {
                var data = new StorageExtractionViewItem
                {
                    ItemName = cScen.UGSName,  //"Тестовый узел",   //Наименование
                    ItemValue = cScen.MaxUGSWithdrawal,      //100D,             //значение объема
                    IdNode = cScen.Id,          //ID объекта схемы
                    isActive = cScen.IsActive
                };

                result.Add(data);
            }
            result.Sort((x, y) =>
            {
                if (y.isActive.CompareTo(x.isActive) == 0)
                    return x.ItemName.CompareTo(y.ItemName);
                else
                    return y.isActive.CompareTo(x.isActive);
            });
            _dataSource = result;
        }

        public IList ShowDataForCtrl()
        {
            PrepareDataForCtrl();
            return _dataSource;
        }

        public void CallGui(string msg)
        {
            throw new NotImplementedException();
        }

        void IGExDomainCallback.BeforeNewSchemaSave(DTO_Schema sch)
        {
        }

        void IGExDomainCallback.OnScenarioLoaded()
        {
            _ctrl.UpdateCtrl();
        }

        void IGExDomainCallback.OnScenarioSaved()
        {
        }



        public void SetValue(object val)
        {
            StorageExtractionViewItem item = val as StorageExtractionViewItem;
            if (item != null)
            {
                _coreHolder.UpdateUGSScenarioData(item.IdNode, item.ItemValue);
            }
        }

        public void OnNodeAdded()
        {
            _ctrl.UpdateCtrl();
        }


        public void OnNodeUpdated(DTO_Node node)
        {
            _ctrl.UpdateCtrl();
        }


        public void OnRemoveNode(int nodeId)
        {
            _ctrl.UpdateCtrl();
        }


        public void OnSchemaSaved(SavedEntityInfo schemaInfo, IEnumerable<SavedEntityInfo> nodesInfo)
        {
            _ctrl.UpdateCtrl();
           // throw new NotImplementedException();
        }


        public void OnCalculated()
        {
            //throw new NotImplementedException();
        }

        public void OnRemoveNodeAsSelectedForPath(DTO_Node node)
        {
            throw new NotImplementedException();
        }


        public void OnSchemaInit()
        {
            _ctrl.UpdateCtrl();
        }


        public bool IsCalculated()
        {
            throw new NotImplementedException();
        }
    }
}
