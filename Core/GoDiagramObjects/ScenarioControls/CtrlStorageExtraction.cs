﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using CommonUIControls.Helper;
using DevExpress.XtraGrid.Columns;

namespace CommonUIControls.ScenarioControls
{
    public partial class CtrlStorageExtraction : DevExpress.XtraEditors.XtraUserControl, IScenarioControl
    {
        private const string ColumnObjName = "ItemName";
        private const string ColumnVolume = "ItemValue";
        private const string ColumnNode = "IdNode";
        private const string ColumnActive = "isActive";

        private IValuesPresenterResultsForControl _presenter;        
        //private ToolStripMenuItem _excelToolStripMenuItem;

        public CtrlStorageExtraction()
        {
            InitializeComponent();

            PopupHelper.Controls = new Control[] { gcExtractionInfo };
        }

        public void SetPresenter(IValuesPresenterResultsForControl presenter)
        {
            _presenter = presenter;
        }

        public void InitControlData()
        {
            try
            {
                gvExtractionInfo.BeginUpdate();
                gcExtractionInfo.DataSource = _presenter.ShowDataForCtrl();
                GridAfterInit();
            }
            finally
            {
                gvExtractionInfo.EndUpdate();
            }
        }

        private void GridAfterInit()
        {
            gvExtractionInfo.BeginUpdate();
            Graphics gr = gcExtractionInfo.CreateGraphics();
            gvExtractionInfo.Columns[ColumnActive].Visible = gvExtractionInfo.Columns[ColumnNode].Visible = false;
            gvExtractionInfo.Columns[ColumnObjName].Fixed = FixedStyle.Left;
            gvExtractionInfo.Columns[ColumnObjName].OptionsColumn.ReadOnly /*= gvVolumeInfo.Columns[ColumnVolume].OptionsColumn.ReadOnly*/ = true;
            gvExtractionInfo.Columns[ColumnObjName].OptionsColumn.AllowEdit /* = gvVolumeInfo.Columns[ColumnVolume].OptionsColumn.AllowEdit*/ =
            gvExtractionInfo.Columns[ColumnObjName].OptionsColumn.AllowFocus /*= gvVolumeInfo.Columns[ColumnVolume].OptionsColumn.AllowFocus*/ = false;

            List<StorageExtractionViewItem> listItems = (List<StorageExtractionViewItem>)gvExtractionInfo.DataSource;

            int wdth = (int)gr.MeasureString("Узел", gvExtractionInfo.Appearance.Row.Font).Width;
            foreach (var pth in listItems)
            {
                var tmp = gr.MeasureString(pth.ItemName, gvExtractionInfo.Appearance.Row.Font);
                if (wdth < (int)tmp.Width)
                {
                    wdth = (int)tmp.Width;
                }
            }
            gvExtractionInfo.Columns[ColumnVolume].Width = 95;
            gvExtractionInfo.Columns[ColumnObjName].Width = wdth + 20;
                       
            var edit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            edit.KeyDown += edit_KeyDown;
            edit.EditValueChanging += edit_EditValueChanging;
            edit.EditValueChanged += EditOnEditValueChanged;
            gvExtractionInfo.Columns[ColumnVolume].ColumnEdit = edit;
            //gvExtractionInfo.BestFitColumns();
            gvExtractionInfo.EndUpdate();
        }

        private void EditOnEditValueChanged(object sender, EventArgs eventArgs)
        {
            gvExtractionInfo.PostEditor();
//            gvExtractionInfo.CloseEditor();
//            gvExtractionInfo.UpdateCurrentRow();
        }

        private void edit_KeyDown(object sender, KeyEventArgs e)
        {
            var tmp = (sender as DevExpress.XtraEditors.TextEdit);
            if (tmp.SelectionLength == tmp.Text.Length)
            {
                if (e.KeyCode == Keys.Left)
                {
                    tmp.SelectionStart = 0;
                    tmp.SelectionLength = 0;
                }
                else if (e.KeyCode == Keys.Right)
                {
                    tmp.SelectionStart = tmp.Text.Length - 1;
                    tmp.SelectionLength = 0;
                }
            }
        }

        private void edit_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            double tmp;
            if (e.NewValue != null && e.NewValue.ToString().Length > 0 && !double.TryParse(e.NewValue.ToString(), out tmp))
                e.Cancel = true;
        }

        public void UpdateCtrl()
        {
            InitControlData();           
        }

        private void gvExtractionInfo_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column == gvExtractionInfo.Columns[ColumnVolume])
            {
                _presenter.SetValue(gvExtractionInfo.GetFocusedRow());
            }
        }

        private void gvExtractionInfo_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            bool isActive;
            try
            {
                isActive = (bool)gvExtractionInfo.GetRowCellValue(e.RowHandle, ColumnActive);
            }
            catch
            {
                isActive = false;
            }
            Color curColor = e.RowHandle % 2 == 0 ? Color.White : Color.LightCyan;
            e.Appearance.BackColor = e.Appearance.BackColor2 = isActive ? curColor : Color.LightPink;
        }

        private void gvExtractionInfo_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0 && (gcExtractionInfo.DataSource as List<StorageExtractionViewItem>).Count > e.FocusedRowHandle)
            {
                bool isActive;
                try
                {

                    isActive = (bool)gvExtractionInfo.GetRowCellValue(e.FocusedRowHandle, ColumnActive);
                }
                catch
                {
                    isActive = false;
                }
                gvExtractionInfo.Columns[ColumnVolume].OptionsColumn.ReadOnly = !isActive;
                gvExtractionInfo.Columns[ColumnVolume].OptionsColumn.AllowEdit = gvExtractionInfo.Columns[ColumnVolume].OptionsColumn.AllowFocus = isActive;
            }
        }

        private void gvExtractionInfo_GotFocus(object sender, EventArgs e)
        {
            bool isActive;
            if (gvExtractionInfo.RowCount > 0 && gvExtractionInfo.FocusedRowHandle >= 0)
            {
                isActive = (bool)gvExtractionInfo.GetFocusedRowCellValue(ColumnActive);
            }
            else
            {
                isActive = false;
            }
            gvExtractionInfo.Columns[ColumnVolume].OptionsColumn.ReadOnly = !isActive;
            gvExtractionInfo.Columns[ColumnVolume].OptionsColumn.AllowEdit = gvExtractionInfo.Columns[ColumnVolume].OptionsColumn.AllowFocus = isActive;
        }
    }
}