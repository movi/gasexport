﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using CommonUIControls.Helper;
using DevExpress.Utils;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraPrinting.Native;

namespace CommonUIControls.ScenarioControls
{
    public partial class CtrlTransPowerAndTarif : DevExpress.XtraEditors.XtraUserControl, IScenarioControl 
    {
        private const string ColumnObjName = "ItemName";
        private const string ColumnOperatorName = "OperatorName";
        private const string ColumnObjId = "IdNode";
        private const string ColumnOperatorId = "IdBalanceZone";

        private const string ColumnDirection = "Direction";

        private const string ColumnReservedVolumeValue = "ReservedVolumeValue";
        private const string ColumnAbleVolumeValue = "AbleVolumeValue";
        private const string ColumnTarifValue = "TarifValue";
        private const string ColumnGsnValue = "GsnValue";
        private const string ColumnGsnPercentValue = "GsnPercentValue";
        private const string ColumnDescription = "Description";
     
        private const string ColumnActive = "isActive";

        private IValuesPresenterResultsForControl _presenter;        
        //private ToolStripMenuItem _excelToolStripMenuItem;

        public CtrlTransPowerAndTarif()
        {
            InitializeComponent();

            PopupHelper.Controls = new Control[] { gcPowerAndTarifInfo };
        }

        public void SetPresenter(IValuesPresenterResultsForControl presenter)
        {
            _presenter = presenter;
        }

        public void InitControlData()
        {
            try
            {
                gvPowerAndTarifInfo.BeginUpdate();
                gcPowerAndTarifInfo.DataSource = _presenter.ShowDataForCtrl();
                GridAfterInit();
            }
            finally
            {
                gvPowerAndTarifInfo.EndUpdate();
            }
        }

        private void GridAfterInit()
        {
            gvPowerAndTarifInfo.BeginUpdate();
            Graphics gr = gcPowerAndTarifInfo.CreateGraphics();
            
            gvPowerAndTarifInfo.Columns[ColumnObjId].Visible = gvPowerAndTarifInfo.Columns[ColumnOperatorId].Visible = 
            gvPowerAndTarifInfo.Columns[ColumnActive].Visible = false;

            gvPowerAndTarifInfo.Columns[ColumnDirection].Fixed = gvPowerAndTarifInfo.Columns[ColumnOperatorName].Fixed =
            gvPowerAndTarifInfo.Columns[ColumnObjName].Fixed = FixedStyle.Left;
            
            gvPowerAndTarifInfo.Columns[ColumnOperatorName].OptionsColumn.ReadOnly = gvPowerAndTarifInfo.Columns[ColumnObjName].OptionsColumn.ReadOnly =
            gvPowerAndTarifInfo.Columns[ColumnDirection].OptionsColumn.ReadOnly = true;
            
            gvPowerAndTarifInfo.Columns[ColumnObjName].OptionsColumn.AllowEdit = gvPowerAndTarifInfo.Columns[ColumnObjName].OptionsColumn.AllowFocus = 
            gvPowerAndTarifInfo.Columns[ColumnOperatorName].OptionsColumn.AllowEdit = gvPowerAndTarifInfo.Columns[ColumnOperatorName].OptionsColumn.AllowFocus =
            gvPowerAndTarifInfo.Columns[ColumnDirection].OptionsColumn.AllowEdit = gvPowerAndTarifInfo.Columns[ColumnDirection].OptionsColumn.AllowFocus = false;

            List<TransPowerAndTarifViewItem> listItems = (List<TransPowerAndTarifViewItem>)gvPowerAndTarifInfo.DataSource;

            int wdthObjName = (int)gr.MeasureString("Наименование", gvPowerAndTarifInfo.Appearance.Row.Font).Width;
            int wdthZoneName = (int)gr.MeasureString("Оператор", gvPowerAndTarifInfo.Appearance.Row.Font).Width;
            int wdthDirect = (int)gr.MeasureString("Вх/Вых", gvPowerAndTarifInfo.Appearance.Row.Font).Width;
            foreach (var pth in listItems)
            {
                var tmpObj = gr.MeasureString(pth.ItemName, gvPowerAndTarifInfo.Appearance.Row.Font);
                if (wdthObjName < tmpObj.Width)
                {
                    wdthObjName = (int)tmpObj.Width;
                }
                var tmpZone = gr.MeasureString(pth.OperatorName, gvPowerAndTarifInfo.Appearance.Row.Font);
                if (wdthZoneName < tmpZone.Width)
                {
                    wdthZoneName = (int)tmpZone.Width;
                }
            }

            gvPowerAndTarifInfo.Columns[ColumnOperatorName].Width = wdthZoneName + 20;
            gvPowerAndTarifInfo.Columns[ColumnObjName].Width = wdthObjName + 20;
            gvPowerAndTarifInfo.Columns[ColumnDirection].Width = 60;

            gvPowerAndTarifInfo.Columns[ColumnReservedVolumeValue].Width = 105;
            gvPowerAndTarifInfo.Columns[ColumnAbleVolumeValue].Width = 105;
            gvPowerAndTarifInfo.Columns[ColumnTarifValue].Width = 70;
            gvPowerAndTarifInfo.Columns[ColumnGsnValue].Width = 70;
            gvPowerAndTarifInfo.Columns[ColumnGsnPercentValue].Width = 70;
            gvPowerAndTarifInfo.Columns[ColumnDescription].Width = 105;

            var edit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();            
            edit.KeyDown += edit_KeyDown;
            edit.EditValueChanging += edit_EditValueChanging;
            edit.EditValueChanged += EditOnEditValueChanged;
            gvPowerAndTarifInfo.Columns[ColumnAbleVolumeValue].ColumnEdit = edit;
            gvPowerAndTarifInfo.Columns[ColumnReservedVolumeValue].ColumnEdit = edit;
            gvPowerAndTarifInfo.Columns[ColumnTarifValue].ColumnEdit = edit;
            gvPowerAndTarifInfo.Columns[ColumnGsnValue].ColumnEdit = edit;
            gvPowerAndTarifInfo.Columns[ColumnGsnPercentValue].ColumnEdit = edit;
            gvPowerAndTarifInfo.Columns[ColumnDescription].ColumnEdit = edit;

            gvPowerAndTarifInfo.Columns.OfType<BandedGridColumn>().ForEach(c=>
            {
                c.OptionsColumn.AllowMerge = c.FieldName == ColumnObjName 
                    ? DefaultBoolean.True 
                    : DefaultBoolean.False;
            });

            gvPowerAndTarifInfo.Columns.OfType<BandedGridColumn>().ForEach(column =>
            {
                column.Visible = column.FieldName != "IdNode" && column.FieldName != "IdBalanceZone" && column.FieldName != "isActive";
            });
            
            //gvPowerAndTarifInfo.BestFitColumns();
            gvPowerAndTarifInfo.EndUpdate();

            InsertColumnToBanded();
        }

        private void EditOnEditValueChanged(object sender, EventArgs eventArgs)
        {
            gvPowerAndTarifInfo.PostEditor();
//            gvPowerAndTarifInfo.CloseEditor();
            gvPowerAndTarifInfo.UpdateCurrentRow();
        }

        /// <summary>
        /// Вставим колонки в банды, настроим BandedeGridView
        /// </summary>
        private void InsertColumnToBanded()
        {
            gvPowerAndTarifInfo.BeginUpdate();
            
            gvPowerAndTarifInfo.Bands.Clear();

            var bandedGridColumns = gvPowerAndTarifInfo.Columns.OfType<BandedGridColumn>().ToList();

            bandedGridColumns.ForEach(c =>
            {
                var gridBand = new GridBand
                {
                    OptionsBand =
                    {
                        AllowHotTrack = false,
                        AllowMove = false,
                        AllowPress = false,
                    },
                    AppearanceHeader =
                    {
                        Options =
                        {
                            UseTextOptions = true
                        }, 
                        TextOptions =
                        {
                            WordWrap = WordWrap.Wrap
                        }
                    },
                    Width = c.Width,
                    RowCount = 3,
                    Caption = c.GetCaption(),
                    Visible = c.Visible
                };
                gridBand.Columns.Add(c);
                gvPowerAndTarifInfo.Bands.Add(gridBand);
            });

            #region Переменная часть тарифа

            var parentBand = gvPowerAndTarifInfo.Bands.First(b => b.Caption == @"Стоимость ГСН, €/млн м³");
            parentBand.Caption = @"Переменная часть тарифа";
            parentBand.RowCount = 1;
            parentBand.Columns.Clear();
            var secondBand = gvPowerAndTarifInfo.Bands.First(b => b.Caption == @"% от стоимости  ГСН");
            parentBand.Width += secondBand.Width;
            secondBand.RowCount = 2;

            var firstBand = new GridBand
            {
                OptionsBand =
                {
                    AllowHotTrack = false,
                    AllowMove = false,
                    AllowPress = false,
                },
                AppearanceHeader =
                {
                    Options =
                    {
                        UseTextOptions = true
                    },
                    TextOptions =
                    {
                        WordWrap = WordWrap.Wrap
                    }
                },
                Width = parentBand.Width - secondBand.Width,
                RowCount = 2,
                Caption = @"Стоимость ГСН, €/млн м³",
                Visible = true,
            };

            firstBand.Columns.Clear();
            firstBand.Columns.Add(bandedGridColumns.First(c => c.GetCaption() == "Стоимость ГСН, €/млн м³"));

            parentBand.Children.Add(firstBand);
            parentBand.Children.Add(secondBand);

            #endregion //Переменная часть тарифа

            gvPowerAndTarifInfo.OptionsView.ShowColumnHeaders = false;

            gvPowerAndTarifInfo.EndUpdate();
        }

        private void edit_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            double tmp;
            if (e.NewValue != null && e.NewValue.ToString().Length > 0 && !double.TryParse(e.NewValue.ToString(), out tmp))
            {
                e.Cancel = true;
                //e.NewValue = e.OldValue;
                //(sender as DevExpress.XtraEditors.TextEdit).SelectionStart = (sender as DevExpress.XtraEditors.TextEdit).Text.Length - 1;
            }
        }

        private void edit_KeyDown(object sender, KeyEventArgs e)
        {
            var tmp = (sender as DevExpress.XtraEditors.TextEdit);
            if (tmp.SelectionLength == tmp.Text.Length)
            {
                if (e.KeyCode == Keys.Left)
                {
                    tmp.SelectionStart = 0;
                    tmp.SelectionLength = 0;
                }
                else if (e.KeyCode == Keys.Right)
                {
                    tmp.SelectionStart = tmp.Text.Length > 0 ? tmp.Text.Length - 1 : 0;
                    tmp.SelectionLength = 0;
                }
            }
        }

        public void UpdateCtrl()
        {
            InitControlData();            
        }

        private void gvPowerAndTarifInfo_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {           
           _presenter.SetValue(e.Row);           
        }

        private void gvPowerAndTarifInfo_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            bool isActive;
            try
            {
                isActive = (bool)gvPowerAndTarifInfo.GetRowCellValue(e.RowHandle, ColumnActive);
            }
            catch
            {
                isActive = false;
            }
            Color curColor = e.RowHandle % 2 == 0 || e.Column.FieldName == ColumnObjName ? Color.White : Color.LightCyan;
            e.Appearance.BackColor = e.Appearance.BackColor2 = isActive ? curColor : Color.LightPink;
        }

        private void gvPowerAndTarifInfo_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0 && (gcPowerAndTarifInfo.DataSource as List<TransPowerAndTarifViewItem>).Count > e.FocusedRowHandle)
            {
                bool isActive;
                try
                {                  
                    isActive = (bool)gvPowerAndTarifInfo.GetRowCellValue(e.FocusedRowHandle, ColumnActive);
                }
                catch
                {
                    isActive = false;
                }
                gvPowerAndTarifInfo.Columns[ColumnAbleVolumeValue].OptionsColumn.ReadOnly =
                gvPowerAndTarifInfo.Columns[ColumnReservedVolumeValue].OptionsColumn.ReadOnly =
                gvPowerAndTarifInfo.Columns[ColumnTarifValue].OptionsColumn.ReadOnly = 
                gvPowerAndTarifInfo.Columns[ColumnGsnValue].OptionsColumn.ReadOnly = 
                gvPowerAndTarifInfo.Columns[ColumnGsnPercentValue].OptionsColumn.ReadOnly = 
                gvPowerAndTarifInfo.Columns[ColumnDescription].OptionsColumn.ReadOnly = !isActive;

                gvPowerAndTarifInfo.Columns[ColumnAbleVolumeValue].OptionsColumn.AllowEdit = 
                gvPowerAndTarifInfo.Columns[ColumnAbleVolumeValue].OptionsColumn.AllowFocus =
                gvPowerAndTarifInfo.Columns[ColumnReservedVolumeValue].OptionsColumn.AllowEdit =
                gvPowerAndTarifInfo.Columns[ColumnReservedVolumeValue].OptionsColumn.AllowFocus =
                gvPowerAndTarifInfo.Columns[ColumnTarifValue].OptionsColumn.AllowEdit =
                gvPowerAndTarifInfo.Columns[ColumnTarifValue].OptionsColumn.AllowFocus =
                gvPowerAndTarifInfo.Columns[ColumnGsnValue].OptionsColumn.AllowEdit =
                gvPowerAndTarifInfo.Columns[ColumnGsnValue].OptionsColumn.AllowFocus =
                gvPowerAndTarifInfo.Columns[ColumnGsnPercentValue].OptionsColumn.AllowEdit =
                gvPowerAndTarifInfo.Columns[ColumnGsnPercentValue].OptionsColumn.AllowFocus =
                gvPowerAndTarifInfo.Columns[ColumnDescription].OptionsColumn.AllowEdit =
                gvPowerAndTarifInfo.Columns[ColumnDescription].OptionsColumn.AllowFocus = isActive;
            }
        }

        private void gvPowerAndTarifInfo_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {          
        }

        private void gvPowerAndTarifInfo_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {           
        }

        private void gvPowerAndTarifInfo_GotFocus(object sender, EventArgs e)
        {
            bool isActive;
            if (gvPowerAndTarifInfo.RowCount > 0 && gvPowerAndTarifInfo.FocusedRowHandle >= 0)
            {
                isActive = (bool)gvPowerAndTarifInfo.GetFocusedRowCellValue(ColumnActive);
            }
            else
            {
                isActive = false;
            }
            gvPowerAndTarifInfo.Columns[ColumnReservedVolumeValue].OptionsColumn.ReadOnly =
            gvPowerAndTarifInfo.Columns[ColumnAbleVolumeValue].OptionsColumn.ReadOnly =
            gvPowerAndTarifInfo.Columns[ColumnTarifValue].OptionsColumn.ReadOnly = 
            gvPowerAndTarifInfo.Columns[ColumnGsnValue].OptionsColumn.ReadOnly =
            gvPowerAndTarifInfo.Columns[ColumnGsnPercentValue].OptionsColumn.ReadOnly = 
            gvPowerAndTarifInfo.Columns[ColumnDescription].OptionsColumn.ReadOnly = !isActive;

            gvPowerAndTarifInfo.Columns[ColumnReservedVolumeValue].OptionsColumn.AllowEdit =
            gvPowerAndTarifInfo.Columns[ColumnReservedVolumeValue].OptionsColumn.AllowFocus =
            gvPowerAndTarifInfo.Columns[ColumnAbleVolumeValue].OptionsColumn.AllowEdit =
            gvPowerAndTarifInfo.Columns[ColumnAbleVolumeValue].OptionsColumn.AllowFocus =
            gvPowerAndTarifInfo.Columns[ColumnTarifValue].OptionsColumn.AllowEdit =
            gvPowerAndTarifInfo.Columns[ColumnTarifValue].OptionsColumn.AllowFocus =
            gvPowerAndTarifInfo.Columns[ColumnGsnValue].OptionsColumn.AllowEdit =
            gvPowerAndTarifInfo.Columns[ColumnGsnValue].OptionsColumn.AllowFocus = 
            gvPowerAndTarifInfo.Columns[ColumnGsnPercentValue].OptionsColumn.AllowEdit =
            gvPowerAndTarifInfo.Columns[ColumnGsnPercentValue].OptionsColumn.AllowFocus = 
            gvPowerAndTarifInfo.Columns[ColumnDescription].OptionsColumn.AllowEdit =
            gvPowerAndTarifInfo.Columns[ColumnDescription].OptionsColumn.AllowFocus = isActive;
        }
    }
}