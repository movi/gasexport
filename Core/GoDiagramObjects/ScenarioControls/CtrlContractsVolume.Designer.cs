﻿
namespace CommonUIControls.ScenarioControls
{
    partial class CtrlContractsVolume
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcVolumeInfo = new DevExpress.XtraGrid.GridControl();
            this.gvVolumeInfo = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gcVolumeInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVolumeInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // gcVolumeInfo
            // 
            this.gcVolumeInfo.Cursor = System.Windows.Forms.Cursors.Default;
            this.gcVolumeInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcVolumeInfo.Location = new System.Drawing.Point(0, 0);
            this.gcVolumeInfo.MainView = this.gvVolumeInfo;
            this.gcVolumeInfo.Name = "gcVolumeInfo";
            this.gcVolumeInfo.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.gcVolumeInfo.Size = new System.Drawing.Size(377, 308);
            this.gcVolumeInfo.TabIndex = 2;
            this.gcVolumeInfo.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvVolumeInfo});
            // 
            // gvVolumeInfo
            // 
            this.gvVolumeInfo.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gvVolumeInfo.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvVolumeInfo.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gvVolumeInfo.Appearance.Row.Options.UseFont = true;
            this.gvVolumeInfo.GridControl = this.gcVolumeInfo;
            this.gvVolumeInfo.Name = "gvVolumeInfo";
            this.gvVolumeInfo.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvVolumeInfo.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvVolumeInfo.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
            this.gvVolumeInfo.OptionsCustomization.AllowGroup = false;
            this.gvVolumeInfo.OptionsNavigation.AutoMoveRowFocus = false;
            this.gvVolumeInfo.OptionsView.ColumnAutoWidth = false;
            this.gvVolumeInfo.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.gvVolumeInfo.OptionsView.ShowGroupPanel = false;
            this.gvVolumeInfo.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gvVolumeInfo_RowCellClick);
            this.gvVolumeInfo.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gvVolumeInfo_CustomDrawCell);
            this.gvVolumeInfo.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvVolumeInfo_FocusedRowChanged);
            this.gvVolumeInfo.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvVolumeInfo_CellValueChanged);
            this.gvVolumeInfo.GotFocus += new System.EventHandler(this.gvVolumeInfo_GotFocus);
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // CtrlContractsVolume
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcVolumeInfo);
            this.Name = "CtrlContractsVolume";
            this.Size = new System.Drawing.Size(377, 308);
            ((System.ComponentModel.ISupportInitialize)(this.gcVolumeInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVolumeInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcVolumeInfo;
        private DevExpress.XtraGrid.Views.Grid.GridView gvVolumeInfo;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;


    }
}
