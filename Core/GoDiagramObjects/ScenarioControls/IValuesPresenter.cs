﻿using System.Collections;
using System.ComponentModel;
using GEx.ErrorHandler;
namespace CommonUIControls.ScenarioControls
{
    //Контракты для реализации в презентере

    /// <summary>
    /// Контракт для подсовывания данных на обработку
    /// и получения min/max значений на расчет
    /// </summary>
    public interface IValuesPresenterDataAccess
    {
        //void SetDocResult(INodeEdgeValuesContainer<EdgeSolutionBase, NodeSolutionBase> docResult);
        //Dictionary<HEdgeTrunk, Dictionary<TimePeriod, double?>> GetMinValueForPeriodTrunk();
        //Dictionary<HEdgeTrunk, Dictionary<TimePeriod, double?>> GetMaxValueForPeriodTrunk();
    }

    /// <summary>
    /// Для общения с контролом
    /// 
    /// (получение полу-готового набора данных для отображения (хотя можно и сразу биндить),
    /// проброс  min/max значений в контейнер)
    /// </summary>
    public interface IValuesPresenterResultsForControl
    {
        IList ShowDataForCtrl();        
        void SetValue(object val);
        //void SetValueInMaxContainer(HEdgeTrunk curTrunk, TimePeriod period, double? val);
        //void Export(HEdgeTrunk trunk, string name);
        //bool CanBeExported { get; }

        bool IsCalculated();
    }

    public interface IScenarioPresenter : IValuesPresenterResultsForControl
    {
        void LoadScenario(int IDscenario);
        void SaveScenario();
        void SaveScenarioAs();
        void RemoveScenario(int IDscenario);
        void SaveCurrentHeader(int Id, string Name);
        void WriteLogError(string sMsg, string sSource);
        void DefaultScenario();
        bool isNewScenario();
        bool AllowSaveScenario();
        string GetScenarioName();
        void UpdateScenarioName(string newName);
        string GetScenarioDescription();
    }
}
