﻿namespace CommonUIControls.ScenarioControls
{

    /// <summary>
    /// Для обратной связи с презентером
    /// </summary>
    public interface IScenarioControl
    {
        void UpdateCtrl();        
    }

   
}
