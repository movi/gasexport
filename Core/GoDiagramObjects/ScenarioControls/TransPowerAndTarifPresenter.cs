﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using GEx.Iface;
//using GEx.Domain;

namespace CommonUIControls.ScenarioControls
{  

    /// <summary>
    /// Тип данных для отображения в гриде
    /// </summary>
    public class TransPowerAndTarifViewItem
    {
        //Узел
        [DisplayName("Узел")]
        public string ItemName { get; internal set; }

        //Оператор
        [DisplayName("Оператор")]
        public string OperatorName { get; internal set; }

        //Направление
        internal DTO_Direction DTODirection { get; set; }
        
        //Направление
        [DisplayName("Вх/Вых")]
        public string Direction 
        {
            get
            {
                return DTODirection.Description();
            }
        }

        //Уже забронированный объём, млн м3 - это входное поле, редактируется вручную 
        [DisplayName("Уже забронир. объём, млн м³")]
        public double? ReservedVolumeValue { get; set; }

        //Доступный объём, млн м3 - это входное поле, редактируется вручную 
        [DisplayName("Доступный объём, млн м³")]
        public double? AbleVolumeValue { get; set; }

        //Фиксированная часть тарифа, € / млн м3 - это входное поле, редактируется вручную 
        [DisplayName("Фиксированная часть тарифа, €/млн м³")]
        public double? TarifValue { get; set; }

        [DisplayName("Стоимость ГСН, €/млн м³")]
        public double? GsnValue { get; set; }

        [DisplayName("% от стоимости  ГСН")]
        public double? GsnPercentValue { get; set; }

        [DisplayName("Комментарий")]
        public string Description { get; set; }

        //сохранить, но в гриде не отображать
        public /*PathPoint*/int IdNode { get; set; }
        public int IdBalanceZone { get; set; }
        
        //Возможно ли редактирование узла сценария
        public bool isActive { get; set; }
    }

    public class TransPowerAndTarifPresenterContainer
    {
        //internal INodeEdgeValuesContainer<EdgeSolutionBase, NodeSolutionBase> DocResults;
        //internal ICapacityProcessor CapProcessor;
        //internal MinMaxFlowsDynamicsContainer MinMaxFlowsDynamics;
        //internal IPipeCapacities PipeCaps;
    }

    public class TransPowerAndTarifPresenter : IValuesPresenterDataAccess, IValuesPresenterResultsForControl, IGExDomainCallback
    {  
        #region Model

        private TransPowerAndTarifPresenterContainer _container;
        private GEx.Iface.IGExDomain _coreHolder;
        private GEx.ErrorHandler.IErrorOutput _errorHandler;
        private List<TransPowerAndTarifViewItem> _dataSource;
        private IScenarioControl _ctrl;
        #endregion

        public TransPowerAndTarifPresenter(GEx.Iface.IGExDomain coreHolder, GEx.ErrorHandler.IErrorOutput eh, IScenarioControl ctrl)
        {
            _container = new TransPowerAndTarifPresenterContainer();
            _coreHolder = coreHolder;
            _errorHandler = eh;
            _ctrl = ctrl;
        }

        private void PrepareDataForCtrl()
        {
            ////
            var result = new List<TransPowerAndTarifViewItem>();
            var list1 = _coreHolder.LoadBorderScenarioData() ?? new List<DTO_BorderScenarioData>();
            List<DTO_BorderScenarioData> list = list1.ToList();

            foreach (var cScen in list)
            {
                var data = new TransPowerAndTarifViewItem
                {
                    ItemName = cScen.BorderName,  
                    IdNode = cScen.Id_Border,      
                    IdBalanceZone = cScen.Id_BalanceZone,
                    OperatorName = cScen.BalanceZoneName,
                    DTODirection = cScen.Direction,
                    AbleVolumeValue = cScen.AbleVolumeValue,
                    ReservedVolumeValue = cScen.ReservedVolumeValue,
                    TarifValue = cScen.TarifValue,
                    GsnValue = cScen.GsnValue,
                    GsnPercentValue = cScen.GsnPercentValue,
                    Description = cScen.Description,
                    isActive = cScen.IsActive
                };

                result.Add(data);
            }

            result.Sort((x, y) =>
            {
                if (y.isActive.CompareTo(x.isActive) == 0)
                {
                    if (x.ItemName.CompareTo(y.ItemName) == 0)
                    {
                        if (y.Direction.CompareTo(x.Direction) == 0)
                            return x.OperatorName.CompareTo(y.OperatorName);
                        else
                            return y.Direction.CompareTo(x.Direction);
                    }
                    else
                        return x.ItemName.CompareTo(y.ItemName);
                }
                else
                    return y.isActive.CompareTo(x.isActive);

            });
            _dataSource = result;           
            
        }

        public IList ShowDataForCtrl()
        {
            PrepareDataForCtrl();
            return _dataSource;
        }

        public void CallGui(string msg)
        {
            throw new NotImplementedException();
        }

        void IGExDomainCallback.BeforeNewSchemaSave(DTO_Schema sch)
        {
        }

        void IGExDomainCallback.OnScenarioLoaded()
        {
            _ctrl.UpdateCtrl();
        }

        void IGExDomainCallback.OnScenarioSaved()
        {
        }
        
        public void SetValue(object val)
        {
            TransPowerAndTarifViewItem item = val as TransPowerAndTarifViewItem;            
            if (item != null)
            {
                _coreHolder.UpdateBorderScenario(item.IdNode, item.IdBalanceZone, item.DTODirection, item.AbleVolumeValue, item.ReservedVolumeValue, item.TarifValue, item.GsnValue, item.GsnPercentValue, item.Description);
            }
            
            if (item != null)
            {
                //_coreHolder.SetScenarioParameterValue(GasExportObjectType.UGS, new Tuple<PathPoint, double?, double?, double?>(item.Point, item.ReservedVolumeValue, 
                //                                        item.AbleVolumeValue, item.TarifValue));
            }
        }

        public void OnNodeAdded()
        {
            _ctrl.UpdateCtrl();
        }


        public void OnNodeUpdated(DTO_Node node)
        {
            _ctrl.UpdateCtrl();
        }


        public void OnRemoveNode(int nodeId)
        {
            _ctrl.UpdateCtrl();
        }


        public void OnSchemaSaved(SavedEntityInfo schemaInfo, IEnumerable<SavedEntityInfo> nodesInfo)
        {
            _ctrl.UpdateCtrl();
           // throw new NotImplementedException();
        }


        public void OnCalculated()
        {
            //throw new NotImplementedException();
        }

        public void OnRemoveNodeAsSelectedForPath(DTO_Node node)
        {
            throw new NotImplementedException();
        }


        public void OnSchemaInit()
        {
            _ctrl.UpdateCtrl();
        }


        public bool IsCalculated()
        {
            throw new NotImplementedException();
        }
    }
}
