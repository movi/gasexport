﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GEx.Iface;

namespace CommonUIControls
{
    public partial class CtrlSelectionPointPath : UserControl
    {
        public CtrlSelectionPointPath()
        {
            InitializeComponent();

            bChose.Click += (sender, args) => { ChosePoint(lbAllPoints.SelectedItem as DTO_Node); };
            bRemove.Click += (sender, args) => { RemovePoint(lbChosenPoints.SelectedItem as DTO_Node); };
        }

        public Control SelectPointControl
        {
            get { return lbChosenPoints; }
        }

        public Control SelectAllControl
        {
            get { return lbAllPoints; }
        }

        public Action<DTO_Node> ChosePoint;
        public Action<DTO_Node> RemovePoint;
    }
}
