﻿using System.Collections;
using System.ComponentModel;
using GEx.ErrorHandler;
using GEx.Iface;
namespace CommonUIControls
{  
    public interface IBalZoneToNode
    {
        int GetNodeID();
        string GetNodeName();
        DTO_Node GetNode();
        //Dictionary<HEdgeTrunk, Dictionary<TimePeriod, double?>> GetMinValueForPeriodTrunk();
        //Dictionary<HEdgeTrunk, Dictionary<TimePeriod, double?>> GetMaxValueForPeriodTrunk();
    }

}
