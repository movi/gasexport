using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using GEx.Iface;

namespace CommonUIControls
{
    public class BalanceZonesPresenter
    {
        /// <summary>
        /// ������������� ��� �������� (master) ������
        /// (����� ����� ��� ��������)
        /// 
        /// � ������ Views ���������� ��� �������������
        /// �� ����������� � ���������� (��� �������� �����-������ � ���� ��������� �����)
        /// </summary>
        public class BalanceZonesMasterView
        {
            [DisplayName("������")]
            public string Name { get; set; }

            [DisplayName("����")]
            public IBindingList Views { get; set; }
        }

        /// <summary>
        /// ������������� ��� ������ GasExportObject
        /// ��������� ��� ������� �������� (���� � ���������� �����(����������) �� ����������)
        /// (������ �������)
        /// </summary>
        public class NodeToBalanceZonesConnectionView : IBalZoneToNode
        {
            private DTO_Node _innerNode;

            private IEnumerable<DTO_BalanceZone> _linkToZones;

            public NodeToBalanceZonesConnectionView(DTO_Node node, IEnumerable<DTO_BalanceZone> linkToZones)
            {
                _innerNode = node;
                _linkToZones = linkToZones;
            }

            public int GetNodeID()
            {
                return _innerNode.ID;
            }

            [DisplayName("������������")]
            public string NodeName
            {
                get { return _innerNode.Name; }
                //set { _innerNode.Name = value; }
            }

            [DisplayName("��������")]
            public DTO_BalanceZone Zone
            {
                get
                {
                    if (_innerNode.ZoneId.HasValue)
                        return _linkToZones.FirstOrDefault(z => z.ID == _innerNode.ZoneId);
                    return null;
                }

                set
                {
                    _innerNode.ZoneId = value!=null? value.ID:-1;
                    _innerNode.ZoneName = value!=null?value.Name:String.Empty;
                }
            }
            
            public string GetNodeName()
            {
                return _innerNode.Name;
            }

            public DTO_Node GetNode()
            {
                return _innerNode;
            }
        }

        /// <summary>
        /// ������������� ��� ������� ������� � ������������� �������� �� ������� ������� (������������)
        /// (������ �������)
        /// </summary>
        public class BorderView : IBalZoneToNode
        {
            private string _nodeName;
            private readonly int _nodeID;
            private int _interConnectionID;
            private DTO_Node _innerNode;

            public BorderView(int nodeID, string nodeName, int interConnectionID, DTO_Node node)
            {
                _innerNode = node;
                _nodeID = nodeID;
                _nodeName = nodeName;
                _interConnectionID = interConnectionID;
            }
            
            //this is hidden

            //public int NodeID
            //{
            //    get { return _nodeID; }
            //}

            public DTO_Node_type GetNodeType()
            {
                return _innerNode.Type;
            }

            public int GetNodeID()
            {
                return _innerNode.ID;
            }
                        
            [DisplayName("������������")]
            public string NodeName
            {
                get { return _nodeName; }
                //set { _nodeName = value; }
            }

            [DisplayName("������ ��������")]
            public DTO_BalanceZone ZoneFirst { get; set; }

            [DisplayName("������ ��������")]
            public DTO_BalanceZone ZoneSecond { get; set; }

            internal int InterConnectionID
            {
                get { return _interConnectionID; }
            }

            public string GetNodeName()
            {
                return _nodeName;
            }


            public DTO_Node GetNode()
            {
                return _innerNode;
            }
        }

        private List<NodeToBalanceZonesConnectionView> _onezoneData;
        private BindingList<BorderView> _bordersInterconnetionsData;
        private BindingList<BorderView> _vtpInterconnetionsData;

        
        /// <summary>
        /// ������ ����� � ���, ������� ����� ����� ��������� � ���������� ����� �� ������� ���� �� ���� ����� � ���� ����
        /// </summary>
        private List<KeyValuePair<int, DTO_BalanceZone>> _zonesForUpdate;
        public List<KeyValuePair<int, DTO_BalanceZone>> ZonesForUpdate
        {
            get
            {
                return _zonesForUpdate;
            }
        }
        
        private readonly IEnumerable<DTO_Node> _nodes;
        private readonly IEnumerable<DTO_BalanceZone> _balanceZones;
        private IGExDomain _holder;

        public IGExDomain Holder { get {return _holder;} }

        public BalanceZonesPresenter(IEnumerable<DTO_Node> nodes, IEnumerable<DTO_BalanceZone> balanceZones, IGExDomain holder)
        {
            _nodes = nodes;
            _balanceZones = balanceZones;
            _holder = holder;
            _zonesForUpdate = new List<KeyValuePair<int, DTO_BalanceZone>>();
        }

        /// <summary>
        /// ������������ ��������� ������ ��� ����� � ��������
        /// </summary>
        /// <returns></returns>
        public BalanceZonesMasterView[] PrepareMasterSource()
        {
            var _onezoneData = _nodes.Where(n => n.Type != DTO_Node_type.Border && n.Type != DTO_Node_type.VirtualTradingPoint).Select(x => new NodeToBalanceZonesConnectionView(x, _balanceZones)).ToList();
            _onezoneData.Sort((x, y) =>
            {
                if (String.Compare(x.NodeName, y.NodeName, StringComparison.Ordinal) == 0)
                    return x.GetNode().ID.CompareTo(y.GetNode().ID);
                return x.NodeName.CompareTo(y.NodeName);
            });


            _bordersInterconnetionsData = GetInterconnections(DTO_Node_type.Border);

            _vtpInterconnetionsData = GetInterconnections(DTO_Node_type.VirtualTradingPoint);

            var masterSource =
                new[]
                {
                    new BalanceZonesMasterView
                    {
                        Name = "���������� ������� � ���",
                        Views = new BindingList<NodeToBalanceZonesConnectionView>(_onezoneData)
                    },
                    new BalanceZonesMasterView
                    {
                        Name = "���������������",
                        Views = _bordersInterconnetionsData
                    },
                    new BalanceZonesMasterView
                    {
                        Name = "����������� �������� ��������",
                        Views = _vtpInterconnetionsData
                    }
                };
            return masterSource;
        }

        private BindingList<BorderView> GetInterconnections(DTO_Node_type type)
        {
            List<BorderView> borderViews = new List<BorderView>();
            var borders = _nodes.Where(n => n.Type == type).ToArray();
            foreach (var border in borders)
            {
                if (border.InterConnections.Count > 0)
                    foreach (var conn in border.InterConnections)
                    {
                        var borderView = new BorderView(border.ID, border.Name, conn.Id, border)
                        {
                            //NodeID = ,
                            //NodeName = ,
                            ZoneFirst = conn.ZoneFrom,
                            ZoneSecond = conn.ZoneTo
                        };
                        borderViews.Add(borderView);
                    }
                else
                {
                    //����� ��� InterConnector-�
                    var borderView = new BorderView(border.ID, border.Name, -1, border)
                    {
                        //NodeID = ,
                        //NodeName = border.Name,
                        ZoneFirst = null,
                        ZoneSecond = null
                    };
                    borderViews.Add(borderView);
                }
            }

            //var borderViews =
            //    Nodes.Where(n => n.Type == DTO_Node_type.Border);.Select(x => new BorderView() { NodeName=x.Name,NodeID=x.ID}).ToList();
            borderViews.Sort((x, y) =>
            {
                if (x.NodeName.CompareTo(y.NodeName) == 0)
                    return x.GetNode().ID.CompareTo(y.GetNode().ID);
                return x.NodeName.CompareTo(y.NodeName);
            });

            var bordersInterconnetionsData = new BindingList<BorderView>(borderViews);
            return bordersInterconnetionsData;
        }

        public void AddElementToBorderViews(BorderView bw)
        {
            var currListInterconnetionsData = bw.GetNodeType() == DTO_Node_type.Border ? _bordersInterconnetionsData : _vtpInterconnetionsData;

            var lastIn = currListInterconnetionsData.FirstOrDefault(b => b.GetNode() == bw.GetNode() && 
                                                            b.NodeName == bw.NodeName && Equals(b.ZoneFirst, bw.ZoneFirst) && Equals(b.ZoneSecond, bw.ZoneSecond));
            if (lastIn != null)
            {
                currListInterconnetionsData.Insert(currListInterconnetionsData.IndexOf(lastIn), bw);
            }
            else
            {
                currListInterconnetionsData.Add(bw);
            }
            /*
            var t = _bordersInterconnetionsData.ToList();
            t.Sort((x, y) =>
            {
                if (x.NodeName.CompareTo(y.NodeName) == 0)
                    return x.GetNode().ID.CompareTo(y.GetNode().ID);
                return x.NodeName.CompareTo(y.NodeName);
            });
            _bordersInterconnetionsData = new BindingList<BorderView>(t);
            */
            //_bordersInterconnetionsData =
            //    new BindingList<BorderView>(_bordersInterconnetionsData.OrderBy(b => b.NodeName).ToList());
        }

        public void RemoveElementFromBorderViews(BorderView bw)
        {
            if (bw.GetNodeType() == DTO_Node_type.Border)  
                _bordersInterconnetionsData.Remove(bw);
            else if (bw.GetNodeType() == DTO_Node_type.VirtualTradingPoint) 
                _vtpInterconnetionsData.Remove(bw);
            
        }

        public void SaveInterconnectorsObject(DTO_Node_type type)
        {
            var borders = _nodes.Where(n => n.Type == type);
            var views = type == DTO_Node_type.VirtualTradingPoint ? _vtpInterconnetionsData : _bordersInterconnetionsData;
            foreach (var brdr in borders)
            {
                var border = brdr;
                var borderViews = views.Where(b => b.GetNodeID() == border.ID);
                int id = 0;
                var connectors = borderViews.Where(bw=>bw.ZoneFirst!=null && bw.ZoneSecond!=null).Select(bv => new DTO_BorderInterconnection()
                {
                    Id = bv.InterConnectionID,
                    ZoneFrom = bv.ZoneFirst,
                    ZoneTo = bv.ZoneSecond,
                });
                brdr.InterConnections = connectors.ToList();
            }
        }
        
        /// <summary>
        /// ���������� ��� ����� ��� ������ ���������� ����
        /// </summary>
        /// <param name="nodeid"></param>
        /// <param name="zone"></param>
        internal void SetConnections(int nodeid, DTO_BalanceZone zone)
        {
            //���� ��� �� ��������� - ��������� ������ ����� ������ ����� ���������� ���������� �� ����� � ����� - ����� ������� ������ ������� - 
            //�� ��������� ���� � �����, � ����� ������������� - �����!
          //  _holder.SetConnections(nodeid, zone);

            _zonesForUpdate.Add(new KeyValuePair<int, DTO_BalanceZone>(nodeid, zone));
        }
    }
}