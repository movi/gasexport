﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommonUIControls.Helper;
using CommonUIControls.ScenarioControls;
using DevExpress.Utils;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraPrinting.Native;

namespace CommonUIControls
{
    public partial class CtrlDeliveries : DevExpress.XtraEditors.XtraUserControl, IScenarioControl
    {
        private DeliveriesPresenter _presenter;

        public CtrlDeliveries()
        {
            InitializeComponent();

            gvDel.OptionsView.AllowCellMerge = true;

            gvDel.OptionsView.ShowIndicator = false;
            gvDel.OptionsView.ShowGroupPanel = false;

            gvDel.Appearance.Row.Font = new Font("Tahoma", 10F);
            gvDel.Appearance.Row.Options.UseFont = true;

            gvDel.CustomDrawCell += (sender, e) =>
            {
                e.Appearance.BackColor = e.RowHandle % 2 == 0
                    ? Color.White
                    : Color.LightCyan;
            };

            PopupHelper.Controls = new Control[] { gcDel };
        }

        public void UpdateCtrl()
        {
            InitControlData();
        }

        public void SetPresenter(DeliveriesPresenter nPresenter)
        {
            _presenter = nPresenter;
        }

        private void InitControlData()
        {
            try
            {
                gvDel.BeginUpdate();
                var dataSource = _presenter.ShowDataForCtrl();
                gcDel.DataSource = dataSource;
                gvDel.CustomColumnDisplayText += (o, e) => e.DisplayText = String.Format(CultureInfo.CurrentCulture, "{0:N2}", e.Value);
                gvDel.Columns.OfType<GridColumn>().ForEach(c =>
                {
                    c.OptionsColumn.AllowEdit = false;

                    c.OptionsColumn.AllowMove = false;
                    c.OptionsColumn.AllowSort = DefaultBoolean.False;
                    c.OptionsColumn.AllowGroup = DefaultBoolean.False;
                    c.OptionsFilter.AllowFilter = false;
                });

            }
            finally
            {
                gvDel.EndUpdate();
            }
        }
    }
}
