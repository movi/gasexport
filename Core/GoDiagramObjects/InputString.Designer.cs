﻿namespace CommonUIControls
{
    partial class InputString
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lcCaption = new DevExpress.XtraEditors.LabelControl();
            this.teStringValue = new DevExpress.XtraEditors.TextEdit();
            this.sbCancel = new DevExpress.XtraEditors.SimpleButton();
            this.sbOK = new DevExpress.XtraEditors.SimpleButton();
            this.lcComment = new DevExpress.XtraEditors.LabelControl();
            this.meComment = new DevExpress.XtraEditors.MemoEdit();
            ((System.ComponentModel.ISupportInitialize)(this.teStringValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lcCaption
            // 
            this.lcCaption.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lcCaption.Location = new System.Drawing.Point(12, 8);
            this.lcCaption.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lcCaption.Name = "lcCaption";
            this.lcCaption.Size = new System.Drawing.Size(43, 16);
            this.lcCaption.TabIndex = 3;
            this.lcCaption.Text = "Caption";
            // 
            // teStringValue
            // 
            this.teStringValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.teStringValue.Location = new System.Drawing.Point(12, 28);
            this.teStringValue.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.teStringValue.Name = "teStringValue";
            this.teStringValue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.teStringValue.Properties.Appearance.Options.UseFont = true;
            this.teStringValue.Size = new System.Drawing.Size(370, 22);
            this.teStringValue.TabIndex = 2;
            this.teStringValue.EditValueChanged += new System.EventHandler(this.teStringValue_EditValueChanged);
            this.teStringValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.teStringValue_KeyPress);
            // 
            // sbCancel
            // 
            this.sbCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sbCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.sbCancel.Appearance.Options.UseFont = true;
            this.sbCancel.Location = new System.Drawing.Point(296, 132);
            this.sbCancel.Name = "sbCancel";
            this.sbCancel.Size = new System.Drawing.Size(86, 32);
            this.sbCancel.TabIndex = 4;
            this.sbCancel.Text = "Отмена";
            this.sbCancel.Click += new System.EventHandler(this.sbCancel_Click);
            // 
            // sbOK
            // 
            this.sbOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sbOK.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.sbOK.Appearance.Options.UseFont = true;
            this.sbOK.Location = new System.Drawing.Point(204, 132);
            this.sbOK.Name = "sbOK";
            this.sbOK.Size = new System.Drawing.Size(86, 32);
            this.sbOK.TabIndex = 5;
            this.sbOK.Text = "Принять";
            this.sbOK.Click += new System.EventHandler(this.sbOK_Click);
            // 
            // lcComment
            // 
            this.lcComment.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lcComment.Location = new System.Drawing.Point(12, 54);
            this.lcComment.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lcComment.Name = "lcComment";
            this.lcComment.Size = new System.Drawing.Size(43, 16);
            this.lcComment.TabIndex = 3;
            this.lcComment.Text = "Caption";
            // 
            // meComment
            // 
            this.meComment.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.meComment.Location = new System.Drawing.Point(12, 75);
            this.meComment.Name = "meComment";
            this.meComment.Size = new System.Drawing.Size(370, 51);
            this.meComment.TabIndex = 6;
            this.meComment.TextChanged += new System.EventHandler(this.meComment_TextChanged);
            // 
            // InputString
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 165);
            this.Controls.Add(this.meComment);
            this.Controls.Add(this.sbOK);
            this.Controls.Add(this.sbCancel);
            this.Controls.Add(this.lcComment);
            this.Controls.Add(this.lcCaption);
            this.Controls.Add(this.teStringValue);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimumSize = new System.Drawing.Size(400, 105);
            this.Name = "InputString";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.teStringValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lcCaption;
        private DevExpress.XtraEditors.TextEdit teStringValue;
        private DevExpress.XtraEditors.SimpleButton sbCancel;
        private DevExpress.XtraEditors.SimpleButton sbOK;
        private DevExpress.XtraEditors.LabelControl lcComment;
        private DevExpress.XtraEditors.MemoEdit meComment;



    }
}