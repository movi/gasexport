﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraSplashScreen;
using System.Configuration;
using System.Diagnostics;
using DevExpress.Utils.Text;
using DevExpress.Utils.Drawing;

namespace GasExportApp
{
    public partial class MainSplashScreen : SplashScreen
    {
        #region Nested types

        public enum SplashScreenCommand
        {
        }

        class SplashImagePainter : ICustomImagePainter
        {
            static SplashImagePainter()
            {
                Painter = new SplashImagePainter();
            }
            protected SplashImagePainter() { }
            public static SplashImagePainter Painter { get; private set; }

            ViewInfo info = null;
            public ViewInfo ViewInfo
            {
                get
                {
                    if (this.info == null) this.info = new ViewInfo();
                    return this.info;
                }
            }

            #region Drawing
            public void Draw(GraphicsCache cache, Rectangle bounds)
            {
                PointF pt = ViewInfo.CalcProgressLabelPoint(cache, bounds);
                cache.Graphics.DrawString(ViewInfo.Text, ViewInfo.ProgressLabelFont, ViewInfo.Brush, pt);
            }
            #endregion
        }

        class ViewInfo
        {
            public ViewInfo()
            {
                Counter = 1;
                Stage = string.Empty;
            }

            public int Counter { get; set; }
            public string Stage { get; set; }

            public PointF CalcProgressLabelPoint(GraphicsCache cache, Rectangle bounds)
            {
                const int yOffset = 40;
                Size size = TextUtils.GetStringSize(cache.Graphics, Text, ProgressLabelFont);
                return new Point(bounds.Width / 2 - size.Width / 2, yOffset);
            }
            Font progressFont = null;
            public Font ProgressLabelFont
            {
                get
                {
                    if (this.progressFont == null) this.progressFont = new Font("Consolas", 10);
                    return this.progressFont;
                }
            }
            Brush brush = null;
            public Brush Brush
            {
                get
                {
                    if (this.brush == null) this.brush = new SolidBrush(Color.Black);
                    return this.brush;
                }
            }
            public string Text { get { return string.Format("{0} - ({1}%)", Stage, Counter.ToString("D2")); } }
        }


        #endregion Nested types

        #region Fields

        private Color _textColor;

        private Font _font;

        #endregion Fields

        #region .ctors

        public MainSplashScreen()
        {
            InitializeComponent();                                  
            BindEventsHandlers();
        }

        #endregion .ctors

        #region Methods

        private void BindEventsHandlers()
        {
            this.Load += MainSplashScreen_Load;
            lcCompanyName.MouseEnter += lcCompanyName_MouseEnter;
            lcCompanyName.MouseLeave += lcCompanyName_MouseLeave;
            lcCompanyName.Click += lcCompanyName_Click;
            pictureEdit2.Paint += pictureEdit2_Paint;
        }

        public override void ProcessCommand(Enum cmd, object arg)
        {
            base.ProcessCommand(cmd, arg);
        }

        #region Event handlers

        private void MainSplashScreen_Load(object sender, EventArgs e)
        {
            var version = ConfigurationManager.AppSettings["Version"];
            if (String.IsNullOrWhiteSpace(version)) version = "1.0.0.0";
            lcVersion.Text = "Версия: " + version;

            _font = new Font(Control.DefaultFont.FontFamily, 16, Control.DefaultFont.Style);
        }

        private void pictureEdit2_Paint(object sender, PaintEventArgs e)
        {            
            const string text = " Калькулятор маршрутов ";
            var textSize = TextRenderer.MeasureText(text, _font);
            var rect = new RectangleF(new PointF(pictureEdit2.Width / 2 - textSize.Width / 2, pictureEdit2.Height - textSize.Height), textSize);
            e.Graphics.DrawString(text, _font, Brushes.Black, rect);
        }

        private void lcCompanyName_MouseEnter(object sender, EventArgs e)
        {
            _textColor = lcCompanyName.ForeColor;
            lcCompanyName.ForeColor = Color.Blue;
            lcCompanyName.Font = new Font(lcCompanyName.Font, lcCompanyName.Font.Style | FontStyle.Underline);
            lcCompanyName.Cursor = Cursors.Hand;
        }

        private void lcCompanyName_MouseLeave(object sender, EventArgs e)
        {
            lcCompanyName.Cursor = Cursors.Default;
            lcCompanyName.ForeColor = _textColor;
            lcCompanyName.Font = new Font(lcCompanyName.Font, lcCompanyName.Font.Style ^ FontStyle.Underline);
        }

        private void lcCompanyName_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start("http://niigazekonomika.gazprom.ru");
            }
            catch
            { 
                // Do nothing, because it is not important to launch web-site
            }
        }

        #endregion Event handlers

        #endregion Methods
    }
}