﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GEx.Iface;

namespace CommonUIControls
{
    
    public interface IMainForm
    {
        void OnPathPointSet();
        void OnWaitCursor();
        void OnDefaultCursor();
        void OnschemaInit();
        void RefreshSomeCaptions();
        void AddFirstPoint(DTO_Node point);
        void AddLastPoint(DTO_Node point);
        void RemovePoint(DTO_Node point);
        void AddAllPoints(IEnumerable<DTO_Node> points);
        void FirstPointClear();
        void LastPointClear();
    }
}
