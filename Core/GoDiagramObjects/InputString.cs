﻿using System;
using System.Windows.Forms;

namespace CommonUIControls
{
    public partial class InputString : DevExpress.XtraEditors.XtraForm
    {

        public String Caption
        {
            get { return lcCaption.Text; }
            set { lcCaption.Text = value; }
        }

        public String CaptionComment
        {
            get { return lcComment.Text; }
            set { lcComment.Text = value; }
        }

        private string _value;

        private string _comment;

        public String Value
        {
            get { return this.teStringValue.Text; }
            set { this.teStringValue.Text = value; }
        }

        public String Comment
        {
            get { return this.meComment.Text; }
            set { this.meComment.Text = value; }
        }

        public InputString(string caption, string value)
        {            
            InitializeComponent();
            Caption = caption;
            _value = this.teStringValue.Text = value;
            lcComment.Visible = meComment.Visible = false;
            this.Height = this.Height - lcComment.Height - meComment.Height; 
        }

        public InputString(string title, string caption, string value)
        {            
            InitializeComponent();
            this.Text = title;
            Caption = caption;
            Value = value;
            lcComment.Visible = meComment.Visible = false;
            this.Height = this.Height - lcComment.Height - meComment.Height; 
        }

        public InputString(string title, string caption, string value, string captionComment, string valueComment)
        {
            InitializeComponent();
            this.Text = title;
            Caption = caption;
            Value = value;
            CaptionComment = captionComment;
            Comment = valueComment;
        }

        private void teStringValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                _value = Value;
                _comment = Comment;
                this.Close();
            }
        }

        private void sbCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void sbOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            _value = Value;
            _comment = Comment;
            this.Close();
        }

        private void teStringValue_EditValueChanged(object sender, EventArgs e)
        {
            //_value = teStringValue.Text;
        }

        private void meComment_TextChanged(object sender, EventArgs e)
        {
            //_comment = meComment.Text;
        }
    }
}
