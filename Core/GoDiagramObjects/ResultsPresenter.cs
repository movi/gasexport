﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using CommonUIControls.ScenarioControls;
using GEx.ErrorHandler;
using GEx.Iface;

namespace CommonUIControls
{  

    /// <summary>
    /// Тип данных для отображения в master гриде
    /// </summary>
    public class MasterPathsView
    {
        /// <summary>
        /// Используется для группировки в режиме мульти
        /// </summary>
        [DisplayName("Конечная точка")]
        public string OutputName { get; set; }

        [DisplayName("Маршрут")]
        public string PathName { get; set; }

        /// <summary>
        /// Стоимость транспортировки млн м3 по маршруту
        /// </summary>
        [DisplayName("Удельная фиксированная cтоимость транспортировки €/млн м³")]
        public double? RelativeFixedCost { get; set; }

        [DisplayName("Удельная переменная cтоимость транспортировки €/млн м³")]
        public double? RelativeVariableCost { get; set; }

        [DisplayName("Фиксированная часть стоимости, €")]
        public double AbsoluteFixedCost { get; set; }

        [DisplayName("Переменная часть стоимости, €")]
        public double AbsoluteVariableCost { get; set; }

        /// <summary>
        /// Общая пропускная способность маршрута
        /// </summary>
        [DisplayName("Пропускная способность, млн м³")]
        public double TotalCapacity { get; set; }

        /// <summary>
        /// Список узлов маршрута
        /// </summary>
        [DisplayName("Узлы")]
        public BindingList<SlavePathView> Views { get; set; }
    }

    /// <summary>
    /// Тип данных для отображения в slave гриде
    /// </summary>
    public class SlavePathView
    {
        internal DTO_Node _Node { get; set; }
                
        internal DTO_BalanceZone _Zone { get; set; }

        internal DTO_Direction _Direction { get; set; }        

        [DisplayName("Наименование")]
        public string NodeName
        {
            get { return _Node.Name; }
        }

        [DisplayName("Оператор")]
        public string Zone
        {
            get
            {
                return _Zone.Name;
            }
        }

        [DisplayName("Направление")]
        public string Direction
        {
            get
            {
                return _Direction.Description();
            }
        }

        /// <summary>
        /// Стоимость транспортировки млн м3
        /// </summary>
        [DisplayName("Удельная фиксированная cтоимость транспортировки, € / млн м³")]
        public double? RelativeFixedCost { get; set; }

        [DisplayName("Удельная переменная cтоимость транспортировки, € / млн м³")]
        public double? RelativeVariableCost { get; set; }

        [DisplayName("Фиксированная часть стоимости, €")]
        public double AbsoluteFixedCost { get; set; }

        [DisplayName("Переменная часть стоимости, €")]
        public double AbsoluteVariableCost { get; set; }

        /// <summary>
        /// Общая пропускная способность маршрута
        /// </summary>
        [DisplayName("Пропускная способность, млн м³")]
        public double Capacity { get; set; }
               

        public SlavePathView(DTO_Vertex vertex)
        {            
            _Direction = vertex.Direction;
            _Node = vertex.Node;
            _Zone = vertex.Zone;
            RelativeFixedCost = vertex.RelativeFixedCost;
            RelativeVariableCost = vertex.RelativeVariableCost;
            AbsoluteFixedCost = vertex.AbsoluteFixedCost;
            AbsoluteVariableCost = vertex.AbsoluteVariableCost;
            Capacity = vertex.Capacity;
        }
    }
    
    public class ResultsPresenter : IValuesPresenterDataAccess, IValuesPresenterResultsForControl, IGExDomainCallback
    {  
        #region Model

        private List<MasterPathsView> _dataSource;
        private IGExDomain _coreHolder;
        private IErrorOutput _errorHandler;
        private IScenarioControl _ctrl;

        #endregion

        public ResultsPresenter(IGExDomain coreHolder, IErrorOutput eh, IScenarioControl ctrl)
        {
            _coreHolder = coreHolder;
            _errorHandler = eh;
            _ctrl = ctrl;
        }

        private void PrepareDataForCtrl()
        {
            var result = new List<MasterPathsView>();
            List<DTO_Path> list = _coreHolder.GetCalculatedPaths().ToList();
            list.Sort((x, y) => { return x.RelativeFixedCost.CompareTo(y.RelativeFixedCost); });
            int indName = 1;
            result = list.Select(path =>
                new MasterPathsView
                {
                    OutputName = path.OutputZone != null ? path.OutputZone.Name : null,
                    PathName = "Маршрут №" + indName++.ToString(),
                    TotalCapacity = path.TotalCapacity,
                    RelativeFixedCost = path.RelativeFixedCost,
                    RelativeVariableCost = path.RelativeVariableCost,
                    AbsoluteFixedCost = path.AbsoluteFixedCost,
                    AbsoluteVariableCost = path.AbsoluteVariableCost,
                    Views = new BindingList<SlavePathView>(path.Vertices.Select(vert => new SlavePathView(vert)).ToList())

                }).ToList();

            if (result.Count > 0)
                result.AddRange(
                 result.GroupBy(m => m.OutputName)
                .Select(grp => new MasterPathsView
                {
                    OutputName = grp.Key,
                    PathName = "Итого",
                    TotalCapacity = grp.Sum(x => x.TotalCapacity),
                    RelativeFixedCost = grp.Sum(x => x.RelativeFixedCost),
                    RelativeVariableCost = grp.Sum(x => x.RelativeVariableCost),
                    AbsoluteFixedCost = grp.Sum(x => x.AbsoluteFixedCost),
                    AbsoluteVariableCost = grp.Sum(x => x.AbsoluteVariableCost),
                })
                );
            _dataSource = result;
        }

        public IList ShowDataForCtrl()
        {
            PrepareDataForCtrl();
            return _dataSource;
        }

        public void CallGui(string msg)
        {
            throw new NotImplementedException();
        }

        void IGExDomainCallback.BeforeNewSchemaSave(DTO_Schema sch)
        {
        }

        void IGExDomainCallback.OnScenarioLoaded()
        {
            _ctrl.UpdateCtrl();
        }

        void IGExDomainCallback.OnScenarioSaved()
        {

        }
        
        public void SetValue(object val)
        {
           
        }

        public void OnNodeAdded()
        {
            //_ctrl.UpdateCtrl();
        }


        public void OnNodeUpdated(DTO_Node node)
        {
            //_ctrl.UpdateCtrl();
        }


        public void OnRemoveNode(int nodeId)
        {
            _ctrl.UpdateCtrl();
        }


        public void OnSchemaSaved(SavedEntityInfo schemaInfo, IEnumerable<SavedEntityInfo> nodesInfo)
        {
            //_ctrl.UpdateCtrl();          
        }


        public void OnCalculated()
        {
            _ctrl.UpdateCtrl();
            //throw new NotImplementedException();
        }

        public void OnRemoveNodeAsSelectedForPath(DTO_Node node)
        {
            throw new NotImplementedException();
        }


        public void OnSchemaInit()
        {
            _ctrl.UpdateCtrl();
        }


        public bool IsCalculated()
        {
            return _coreHolder.IsCalculated();
        }
    }
}
