﻿namespace CommonUIControls
{
    partial class FrmBalanceZones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sbtnOK = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.balanceZonesCtrl = new CommonUIControls.BalanceZonesCtrl();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // sbtnOK
            // 
            this.sbtnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnOK.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.sbtnOK.Appearance.Options.UseFont = true;
            this.sbtnOK.Location = new System.Drawing.Point(599, 7);
            this.sbtnOK.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.sbtnOK.Name = "sbtnOK";
            this.sbtnOK.Size = new System.Drawing.Size(86, 32);
            this.sbtnOK.TabIndex = 1;
            this.sbtnOK.Text = "Сохранить";
            this.sbtnOK.Click += new System.EventHandler(this.sbtnOK_Click);
            // 
            // sbtnCancel
            // 
            this.sbtnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.sbtnCancel.Appearance.Options.UseFont = true;
            this.sbtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.sbtnCancel.Location = new System.Drawing.Point(691, 7);
            this.sbtnCancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.sbtnCancel.Name = "sbtnCancel";
            this.sbtnCancel.Size = new System.Drawing.Size(86, 32);
            this.sbtnCancel.TabIndex = 2;
            this.sbtnCancel.Text = "Отмена";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.sbtnCancel);
            this.panel1.Controls.Add(this.sbtnOK);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 520);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(785, 47);
            this.panel1.TabIndex = 3;
            // 
            // balanceZonesCtrl
            // 
            this.balanceZonesCtrl.BalanceZones = null;
            this.balanceZonesCtrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.balanceZonesCtrl.Location = new System.Drawing.Point(0, 0);
            this.balanceZonesCtrl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.balanceZonesCtrl.Name = "balanceZonesCtrl";
            this.balanceZonesCtrl.Nodes = null;
            this.balanceZonesCtrl.Presenter = null;
            this.balanceZonesCtrl.Size = new System.Drawing.Size(785, 520);
            this.balanceZonesCtrl.TabIndex = 0;
            // 
            // FrmBalanceZones
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(785, 567);
            this.Controls.Add(this.balanceZonesCtrl);
            this.Controls.Add(this.panel1);
            this.MinimumSize = new System.Drawing.Size(600, 500);
            this.Name = "FrmBalanceZones";
            this.Text = "Формирование и просмотр привязок к операторам";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmBalanceZones_FormClosed);
            this.Load += new System.EventHandler(this.FrmBalanceZones_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private BalanceZonesCtrl balanceZonesCtrl;
        private DevExpress.XtraEditors.SimpleButton sbtnOK;
        private DevExpress.XtraEditors.SimpleButton sbtnCancel;
        private System.Windows.Forms.Panel panel1;


    }
}