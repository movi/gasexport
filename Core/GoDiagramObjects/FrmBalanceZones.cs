﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using GasExportApp.Dialogs;
using GEx.Iface;
///using GEx.Domain;

namespace CommonUIControls
{
    public partial class FrmBalanceZones : DevExpress.XtraEditors.XtraForm, IDialogForm
    {
        private IMessageFilter _filter;
              

        public FrmBalanceZones()
        {
            InitializeComponent();    
            
         //   balanceZonesCtrl.Scheme = scheme;
         //   balanceZonesCtrl.BalanceZones = balanceZones;
         //    balanceZonesCtrl.Init();
        }

        public void Init(IEnumerable<DTO_Node> nodes, IEnumerable<DTO_BalanceZone> balanceZones, IGExDomain holder)
        {
            balanceZonesCtrl.Nodes = nodes;
   
            balanceZonesCtrl.BalanceZones = new List<DTO_BalanceZone>(balanceZones);
            balanceZonesCtrl.Presenter = new BalanceZonesPresenter(balanceZonesCtrl.Nodes, balanceZonesCtrl.BalanceZones, holder);
            balanceZonesCtrl.Init();
        }

        public IEnumerable<DTO_Node> Nodes
        {
            get
            {
                return balanceZonesCtrl.Nodes;
            }
        }

        public IEnumerable<DTO_BalanceZone> Zones
        {
            get
            {
                return balanceZonesCtrl.BalanceZones;
            }
        }

        private void sbtnOK_Click(object sender, EventArgs e)
        {
            //дополнительно сохранить границы по кнопке OK
            var msg = String.Format("Внесенные изменения вступят в силу {0} для всех существующих схем. Продолжить?", Environment.NewLine);
            var caption = "Сохранение изменений";
            
            if (MessageBox.Show(msg, caption, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                balanceZonesCtrl.SaveBorders();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void FrmBalanceZones_Load(object sender, EventArgs e)
        {
            _filter = new MessageFilter(this);
            Application.AddMessageFilter(_filter);
        }

        private void FrmBalanceZones_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.RemoveMessageFilter(_filter);
        }

        public void ReactionForEnter()
        {
            if (balanceZonesCtrl.gvBalZones.ActiveEditor != null || balanceZonesCtrl.gvOperators.ActiveEditor != null)
            {
                GridView detailView = balanceZonesCtrl.gvBalZones.GetDetailView(balanceZonesCtrl.gvBalZones.FocusedRowHandle, 0) as GridView;
                if (detailView != null)
                {
                    detailView.CloseEditor();
                    detailView.UpdateCurrentRow();
                }
                balanceZonesCtrl.gvBalZones.CloseEditor();
                balanceZonesCtrl.gvBalZones.UpdateCurrentRow();
                balanceZonesCtrl.gvOperators.CloseEditor();
                balanceZonesCtrl.gvOperators.UpdateCurrentRow();
            }
            else
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
        }

        public void ReactionForEsc()
        {
            if (balanceZonesCtrl.gvBalZones.ActiveEditor != null || balanceZonesCtrl.gvOperators.ActiveEditor != null)
            {
                GridView detailView = balanceZonesCtrl.gvBalZones.GetDetailView(balanceZonesCtrl.gvBalZones.FocusedRowHandle, 0) as GridView;
                if (detailView != null)
                {
                    detailView.HideEditor();
                    detailView.UpdateCurrentRow();
                }
                balanceZonesCtrl.gvBalZones.HideEditor();
                balanceZonesCtrl.gvBalZones.UpdateCurrentRow();
                balanceZonesCtrl.gvOperators.HideEditor();
                balanceZonesCtrl.gvOperators.UpdateCurrentRow();
            }
            else
            {
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                this.Close();
            }
        }


        public System.Windows.Forms.Control ReturnMainControl()
        {
            return this;
        }

        public List<KeyValuePair<int, DTO_BalanceZone>> NodeZones
        {
            get
            {
                return balanceZonesCtrl.NodeZones;
            }
        }
    }
}
