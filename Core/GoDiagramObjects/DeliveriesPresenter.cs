﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonUIControls.ScenarioControls;
using GEx.ErrorHandler;
using GEx.Iface;

namespace CommonUIControls
{
    public class DeliveriesViewItem
    {
        //Узел
        [DisplayName("Узел")]
        public string ItemName { get; internal set; }

        //Оператор
        [DisplayName("Недопоставка")]
        public double DeliveriesValue { get; internal set; }
    }

    public class DeliveriesPresenter : IValuesPresenterDataAccess, IValuesPresenterResultsForControl, IGExDomainCallback
    {
        private IGExDomain _coreHolder;
        private IErrorOutput _errorHandler;
        private IScenarioControl _ctrl;
        private List<DeliveriesViewItem> _dataSource;

        public DeliveriesPresenter(IGExDomain coreHolder, IErrorOutput eh, IScenarioControl ctrl)
        {
            _coreHolder = coreHolder;
            _errorHandler = eh;
            _ctrl = ctrl;
        }
        public IList ShowDataForCtrl()
        {
            PrepareDataForCtrl();
            return _dataSource;
        }

        private void PrepareDataForCtrl()
        {
            var result = new List<DeliveriesViewItem>();
            var list1 = _coreHolder.LoadDeliveries() ?? new List<DTO_Deliveries>();
            List<DTO_Deliveries> list = list1.ToList();

            foreach (var cScen in list)
            {
                var data = new DeliveriesViewItem
                {
                    ItemName = cScen.ItemName,
                    DeliveriesValue = cScen.DeliveriesValue,
                };

                result.Add(data);
            }

            _dataSource = result;
        }

        public void CallGui(string msg)
        {
            throw new NotImplementedException();
        }

        void IGExDomainCallback.BeforeNewSchemaSave(DTO_Schema sch)
        {
        }

        void IGExDomainCallback.OnScenarioLoaded()
        {
            _ctrl.UpdateCtrl();
        }

        void IGExDomainCallback.OnScenarioSaved()
        {
        }

        public void SetValue(object val)
        {
        }

        public void OnNodeAdded()
        {
            _ctrl.UpdateCtrl();
        }


        public void OnNodeUpdated(DTO_Node node)
        {
            _ctrl.UpdateCtrl();
        }


        public void OnRemoveNode(int nodeId)
        {
            _ctrl.UpdateCtrl();
        }


        public void OnSchemaSaved(SavedEntityInfo schemaInfo, IEnumerable<SavedEntityInfo> nodesInfo)
        {
            _ctrl.UpdateCtrl();
        }


        public void OnCalculated()
        {
            _ctrl.UpdateCtrl();
        }

        public void OnRemoveNodeAsSelectedForPath(DTO_Node node)
        {
            throw new NotImplementedException();
        }


        public void OnSchemaInit()
        {
            _ctrl.UpdateCtrl();
        }


        public bool IsCalculated()
        {
            return _coreHolder.IsCalculated();
        }
    }
}
