﻿
namespace CommonUIControls
{
    partial class CtrlResults
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcResults = new DevExpress.XtraGrid.GridControl();
            this.gvResults = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.lbResultCount = new DevExpress.XtraEditors.LabelControl();
            this.pHeader = new System.Windows.Forms.Panel();
            this.sfdExcelExport = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.gcResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvResults)).BeginInit();
            this.pHeader.SuspendLayout();
            this.SuspendLayout();
            // 
            // gcResults
            // 
            this.gcResults.Cursor = System.Windows.Forms.Cursors.Default;
            this.gcResults.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcResults.Location = new System.Drawing.Point(0, 33);
            this.gcResults.MainView = this.gvResults;
            this.gcResults.Name = "gcResults";
            this.gcResults.Size = new System.Drawing.Size(394, 443);
            this.gcResults.TabIndex = 4;
            this.gcResults.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvResults});
            // 
            // gvResults
            // 
            this.gvResults.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gvResults.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvResults.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gvResults.Appearance.Row.Options.UseFont = true;
            this.gvResults.GridControl = this.gcResults;
            this.gvResults.Name = "gvResults";
            this.gvResults.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvResults.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvResults.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.gvResults.OptionsBehavior.Editable = false;
            this.gvResults.OptionsCustomization.AllowFilter = false;
            this.gvResults.OptionsCustomization.AllowGroup = false;
            this.gvResults.OptionsCustomization.AllowQuickHideColumns = false;
            this.gvResults.OptionsPrint.ExpandAllDetails = true;
            this.gvResults.OptionsPrint.PrintDetails = true;
            this.gvResults.OptionsView.ColumnAutoWidth = false;
            this.gvResults.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.gvResults.OptionsView.ShowGroupPanel = false;
            this.gvResults.MasterRowExpanded += new DevExpress.XtraGrid.Views.Grid.CustomMasterRowEventHandler(this.gvResults_MasterRowExpanded);
            // 
            // lbResultCount
            // 
            this.lbResultCount.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbResultCount.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lbResultCount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbResultCount.Location = new System.Drawing.Point(3, 3);
            this.lbResultCount.Name = "lbResultCount";
            this.lbResultCount.Size = new System.Drawing.Size(391, 27);
            this.lbResultCount.TabIndex = 5;
            this.lbResultCount.Text = " Построено маршрутов:  0";
            // 
            // pHeader
            // 
            this.pHeader.Controls.Add(this.lbResultCount);
            this.pHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pHeader.Location = new System.Drawing.Point(0, 0);
            this.pHeader.Name = "pHeader";
            this.pHeader.Size = new System.Drawing.Size(394, 33);
            this.pHeader.TabIndex = 6;
            // 
            // sfdExcelExport
            // 
            this.sfdExcelExport.DefaultExt = "xlsx";
            this.sfdExcelExport.Filter = "Excel files|*.xls*";
            // 
            // CtrlResults
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcResults);
            this.Controls.Add(this.pHeader);
            this.Name = "CtrlResults";
            this.Size = new System.Drawing.Size(394, 476);
            ((System.ComponentModel.ISupportInitialize)(this.gcResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvResults)).EndInit();
            this.pHeader.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcResults;
        private DevExpress.XtraGrid.Views.Grid.GridView gvResults;
        private DevExpress.XtraEditors.LabelControl lbResultCount;
        private System.Windows.Forms.Panel pHeader;
        private System.Windows.Forms.SaveFileDialog sfdExcelExport;




    }
}
