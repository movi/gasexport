﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraGrid;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Native;

namespace CommonUIControls.Helper
{
        /// <summary>
        /// Класс предоставляющий возможности для Экспорта в Excel как есть для
        /// - TreeList
        /// - GridControl
        /// - ChartControl
        /// </summary>
        public class PopupHelper
        {
            private static PopupMenu _popupMenu;

            private static readonly SaveFileDialog SfdExcelExport = new SaveFileDialog
            {
                DefaultExt = "xlsx",
                Filter = @"Excel files|*.xls*"
            };

            private static List<Control> _controls = new List<Control>();
            private static Control _control;

            public static Control[] Controls
            {
//                private get { return _controls.ToArray(); }
                set
                {
                    value.ForEach(v =>
                    {
                        v.MouseDown += ControlOnMouseDown;
                        _controls.Add(v);
                    });
                    CreatePopupMenu();
                }
            }

            private static void CreatePopupMenu()
            {
                var barManager = new BarManager();
                _popupMenu = new PopupMenu { Manager = barManager };
                var bbiExportExcel = new BarButtonItem { Caption = @"Выгрузить в Excel", };
                bbiExportExcel.ItemClick += ExportExcel;
                _popupMenu.AddItem(bbiExportExcel);
            }

            private static void ExportExcel(object sender, ItemClickEventArgs e)
            {
                if (_dictionary.ContainsKey(_control))
                {
                    var action = _dictionary[_control];
                    action(_control);
                    return;
                }

//                if (_control is TreeList)
//                {
//                    var treeList = (TreeList)_control;
//                    ExportToExcel(treeList);
//                    return;
//                }
                if (_control is GridControl)
                {
                    var grid = (GridControl)_control;
                    ExportToGrid(grid);
                    return;
                }
//                if (_control is ChartControl)
//                {
//                    var chart = (ChartControl)_control;
//                    ExportToChart(chart);
//                    return;
//                }
            }

            private static void ControlOnMouseDown(object sender, MouseEventArgs e)
            {
                if (e.Button == MouseButtons.Right)
                {
                    _control = (Control)sender;
                    _popupMenu.ShowPopup(_control.PointToScreen(new Point(e.X, e.Y)));
                }
                else
                {
                    _popupMenu.HidePopup();
                }
            }

            #region ExportToExcel

//            public static void ExportToExcel(TreeList treeList)
//            {
//                if (SfdExcelExport.ShowDialog(treeList) != DialogResult.OK) return;
//                if (SfdExcelExport.FileName.Length <= 0) return;
//
//                var oldCursor = treeList.Cursor;
//                try
//                {
//                    treeList.Cursor = Cursors.WaitCursor;
//
//                    var currNode = treeList.FocusedNode;
//                    Dictionary<TreeListNode, bool> nodesTree = treeList.GetNodeList().ToDictionary(tlNode => tlNode, tlNode => tlNode.Expanded);
//
//                    treeList.BeginUpdate();
//
//                    treeList.ExpandAll();
//
//                    treeList.OptionsPrint.PrintAllNodes = true;
//                    treeList.OptionsPrint.PrintTree = false;
//
//
//                    if (SfdExcelExport.FileName.Substring(SfdExcelExport.FileName.LastIndexOf(".", StringComparison.Ordinal) + 1) != "xlsx")
//                    {
//                        var optns = new XlsExportOptions
//                        {
//                            ExportMode = XlsExportMode.SingleFile,
//                            TextExportMode = TextExportMode.Value
//                        };
//                        treeList.ExportToXls(SfdExcelExport.FileName, optns);
//                    }
//                    else
//                    {
//                        var optnsX = new XlsxExportOptions
//                        {
//                            ExportMode = XlsxExportMode.SingleFile,
//                            TextExportMode = TextExportMode.Value
//                        };
//                        treeList.ExportToXlsx(SfdExcelExport.FileName, optnsX);
//                    }
//
//                    foreach (var tlNode in treeList.GetNodeList())
//                    {
//                        if (nodesTree.ContainsKey(tlNode))
//                            tlNode.Expanded = nodesTree[tlNode];
//                    }
//                    treeList.FocusedNode = currNode;
//
//                }
//                finally
//                {
//                    treeList.EndUpdate();
//                    treeList.Cursor = oldCursor;
//                }
//            }

            #endregion //ExportToExcel

            #region ExportToGrid

            public static void ExportToGrid(GridControl grid)
            {
                if (SfdExcelExport.ShowDialog(grid) != DialogResult.OK) return;
                if (SfdExcelExport.FileName.Length <= 0) return;

                var oldCursor = grid.Cursor;
                try
                {
                    grid.Cursor = Cursors.WaitCursor;

                    if (SfdExcelExport.FileName.Substring(SfdExcelExport.FileName.LastIndexOf(".", StringComparison.Ordinal) + 1) != "xlsx")
                    {
                        var optns = new XlsExportOptions
                        {
                            ExportMode = XlsExportMode.SingleFile,
                            TextExportMode = TextExportMode.Value
                        };
                        grid.ExportToXls(SfdExcelExport.FileName, optns);
                    }
                    else
                    {
                        var optnsX = new XlsxExportOptions
                        {
                            ExportMode = XlsxExportMode.SingleFile,
                            TextExportMode = TextExportMode.Value
                        };
                        grid.ExportToXlsx(SfdExcelExport.FileName, optnsX);
                    }
                }
                finally
                {
                    grid.Cursor = oldCursor;
                }
            }

            #endregion //ExportToGrid

            #region ExportToChart

//            public static void ExportToChart(ChartControl chart)
//            {
//                if (SfdExcelExport.ShowDialog(chart) != DialogResult.OK) return;
//                if (SfdExcelExport.FileName.Length <= 0) return;
//
//                var oldCursor = chart.Cursor;
//                try
//                {
//                    chart.Cursor = Cursors.WaitCursor;
//
//                    if (SfdExcelExport.FileName.Substring(SfdExcelExport.FileName.LastIndexOf(".", StringComparison.Ordinal) + 1) != "xlsx")
//                    {
//                        var optns = new XlsExportOptions
//                        {
//                            ExportMode = XlsExportMode.SingleFile,
//                            TextExportMode = TextExportMode.Value
//                        };
//                        chart.ExportToXls(SfdExcelExport.FileName, optns);
//                    }
//                    else
//                    {
//                        var optnsX = new XlsxExportOptions
//                        {
//                            ExportMode = XlsxExportMode.SingleFile,
//                            TextExportMode = TextExportMode.Value
//                        };
//                        chart.ExportToXlsx(SfdExcelExport.FileName, optnsX);
//                    }
//                }
//                finally
//                {
//                    chart.Cursor = oldCursor;
//                }
//            }

            #endregion //ExportToChart


            private static Dictionary<Control, Action<Control>> _dictionary = new Dictionary<Control, Action<Control>>();

            /// <summary>
            /// Метод принимающий пару: контрол, делегат для обработки нештатной ситуации
            /// </summary>
            /// <param name="gcResults"></param>
            /// <param name="export"></param>
            public static void ControlWithAction(GridControl gcResults, Action<Control> export)
            {
                Controls = new Control[] { gcResults };
                _dictionary.Add(gcResults, export);
            }
        }
    }
