﻿using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using CommonUIControls.Helper;
using CommonUIControls.ScenarioControls;
using DevExpress.Utils;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting.Native;

namespace CommonUIControls
{
    public partial class CtrlNominations : DevExpress.XtraEditors.XtraUserControl, IScenarioControl
    {
        private NominationsPresenter _presenter;
        private const string ColumnObjName = "ItemName";

        public CtrlNominations()
        {
            InitializeComponent();       
            
            gvNomTop.OptionsView.AllowCellMerge = gvNomBottom.OptionsView.AllowCellMerge = true;

            gvNomTop.OptionsView.ShowIndicator = gvNomBottom.OptionsView.ShowIndicator =
            gvNomTop.OptionsView.ShowGroupPanel = gvNomBottom.OptionsView.ShowGroupPanel = false;

            gvNomTop.Appearance.Row.Font = gvNomBottom.Appearance.Row.Font = new Font("Tahoma", 10F);
            gvNomTop.Appearance.Row.Options.UseFont = gvNomBottom.Appearance.Row.Options.UseFont = true;

            gvNomTop.CustomDrawCell += (sender, e) =>
            {
                e.Appearance.BackColor = e.RowHandle % 2 == 0 || e.Column.FieldName == ColumnObjName
                    ? Color.White
                    : Color.LightCyan;
            };

            gvNomBottom.CustomDrawCell += (sender, e) =>
            {
                e.Appearance.BackColor = e.RowHandle % 2 == 0 || e.Column.FieldName == ColumnObjName
                    ? Color.White
                    : Color.LightCyan;
            };

            PopupHelper.Controls = new Control[] { gcNomTop, gcNomBottom };
        }

        public void UpdateCtrl()
        {
            var source = _presenter.ShowDataForCtrl().OfType<NominationsViewItem>();
            var items = source as IList<NominationsViewItem> ?? source.ToList();
            InitControlData(gvNomTop, gcNomTop, items.Where(n=>n.IsSource).ToList());
            InitControlData(gvNomBottom, gcNomBottom, items.Where(n=>!n.IsSource).ToList());
        }

        public void SetPresenter(NominationsPresenter nPresenter)
        {
            _presenter = nPresenter;
        }

        private void InitControlData(GridView gridView, GridControl gridControl, List<NominationsViewItem> dataSource)
        {
            try
            {
                gridView.BeginUpdate();
                gridControl.DataSource = dataSource;

                gridView.Columns.ForEach(c =>
                {
                    c.OptionsColumn.AllowMerge = c.FieldName == ColumnObjName
                        ? DefaultBoolean.True
                        : DefaultBoolean.False;

                    c.OptionsColumn.AllowEdit = false;

                    c.OptionsColumn.AllowMove = false;
                    c.OptionsColumn.AllowSort = DefaultBoolean.False;
                    c.OptionsColumn.AllowGroup = DefaultBoolean.False;
                    c.OptionsFilter.AllowFilter = false;
                });
            }
            finally
            {
                gridView.EndUpdate();
            }
        }
    }
}
