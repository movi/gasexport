﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Aspose.Cells;
using CommonUIControls.Helper;
using CommonUIControls.ScenarioControls;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using GEx.Iface;

namespace CommonUIControls
{
    public partial class CtrlResults : DevExpress.XtraEditors.XtraUserControl, IScenarioControl
    {
        private const string ColumnOutputName = "OutputName";

        private const string ColumnPathName = "PathName";
        private const string ColumnTotalRelativeFixedCost = "RelativeFixedCost";
        private const string ColumnTotalRelativeVariableCost = "RelativeVariableCost";
        private const string ColumnTotalCapacity = "TotalCapacity";

        private const string ColumnObjName = "NodeName";
        private const string ColumnZoneName = "Zone";
        private const string ColumnDirection = "Direction";
        private const string ColumnRelativeFixedCost = "RelativeFixedCost";
        private const string ColumnRelativeVariableCost = "RelativeVariableCost";
        private const string ColumnCapacity = "Capacity";

        private const string ColumnAbsoluteFixedCost = "AbsoluteFixedCost";
        private const string ColumnAbsoluteVariableCost = "AbsoluteVariableCost";

        private IValuesPresenterResultsForControl _presenter;
        public Action<MasterPathsView> OnPathSelect;
        public Action<SlavePathView, SlavePathView> OnEdgeSelect;
        private List<MasterPathsView> _pathViews;

        private bool FirstTime = true;

        private IGExDomain _coreHolder;

        //private ToolStripMenuItem _excelToolStripMenuItem;

        public CtrlResults()
        {
            InitializeComponent();

            gvResults.CustomColumnSort += GridViewOnCustomColumnSort;
            gvResults.CustomDrawGroupRow += GvResultsOnCustomDrawGroupRow;
            gvResults.CustomDrawCell += GvResultsOnCustomDrawCell;

            PopupHelper.ControlWithAction(gcResults, ExportToExcel);
        }

        /// <summary>
        /// Событие на кастомизацию строки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GvResultsOnCustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            try
            {
                var rowHandle = e.RowHandle;
                if (rowHandle < 0) return;

                var masterPathsView = gvResults.GetRow(rowHandle) as MasterPathsView;
                if (masterPathsView == null) return;

                if (masterPathsView.OutputName != "Сумма по всем маршрутам") return;
                e.Appearance.BackColor = Color.LightBlue;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Событие на кастомизацию группирующей строки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GvResultsOnCustomDrawGroupRow(object sender, RowObjectCustomDrawEventArgs e)
        {
           // e.Appearance.BackColor = Color.Aqua;
        }

        #region Export to Excel

        private void ExportToExcel(Control control)
        {
            var gridControl = control as GridControl;
            if (gridControl == null) return;

            var ds = gridControl.DataSource as List<MasterPathsView>;
            if (ds == null) return;

            var workbook = new Workbook();
            var worksheet = workbook.Worksheets[0];

            worksheet.Cells[0, 0].Value = "Маршрут";
            worksheet.Cells[0, 1].Value = "Наименование";
            worksheet.Cells[0, 2].Value = "Оператор";
            worksheet.Cells[0, 3].Value = "Направление";
            worksheet.Cells[0, 4].Value = _coreHolder.Mode == ModeWork.Multi ? "Фиксированная часть стоимости, €" : "Удельная фиксированная cтоимость транспортировки €/млн м³";
            worksheet.Cells[0, 5].Value = _coreHolder.Mode == ModeWork.Multi ? "Переменная часть стоимости, € " : "Удельная переменная cтоимость транспортировки €/млн м³";
            worksheet.Cells[0, 6].Value = _coreHolder.Mode == ModeWork.Multi ? "Объём поставок, млн м³" : "Пропускная способность, млн м³";
            
            var row = 1;
            foreach (var v in ds.Where(d=>d.Views != null).OrderBy(d=>d.OutputName))
            {
                var master = v;
                var slave = v.Views;

                worksheet.Cells[row, 0].Value = "Конечная точка: " + master.OutputName;
                row++;
                worksheet.Cells[row, 0].Value = master.PathName;
                worksheet.Cells[row, 4].Value = _coreHolder.Mode == ModeWork.Multi ? master.AbsoluteFixedCost : master.RelativeFixedCost;
                worksheet.Cells[row, 5].Value = _coreHolder.Mode == ModeWork.Multi ? master.AbsoluteVariableCost : master.RelativeVariableCost;
                worksheet.Cells[row, 6].Value = master.TotalCapacity;
                row++;

                foreach (var s in slave)
                {
                    worksheet.Cells[row, 1].Value = s.NodeName;
                    worksheet.Cells[row, 2].Value = s.Zone;
                    worksheet.Cells[row, 3].Value = s.Direction;
                    worksheet.Cells[row, 4].Value = _coreHolder.Mode == ModeWork.Multi ? s.AbsoluteFixedCost : s.RelativeFixedCost;
                    worksheet.Cells[row, 5].Value = _coreHolder.Mode == ModeWork.Multi ? s.AbsoluteVariableCost : s.RelativeVariableCost;
                    worksheet.Cells[row, 6].Value = s.Capacity;
                    row++;
                }
            }

            worksheet.AutoFitColumns();
            worksheet.AutoFitRows();

            Export(workbook);
        }

        public void Export(Workbook workbook)
        {
            var saveFileDialog = new SaveFileDialog
            {
                DefaultExt = "xlsx",
                Filter = @"Excel files|*.xls*"
            };

            if (saveFileDialog.ShowDialog(gcResults) != DialogResult.OK) return;
            if (saveFileDialog.FileName.Length <= 0) return;

            var oldCursor = gcResults.Cursor;
            try
            {
                gcResults.Cursor = Cursors.WaitCursor;

                workbook.Save(saveFileDialog.FileName);
            }
            finally
            {
                gcResults.Cursor = oldCursor;
            }
        }

        #endregion //Export to Excel

        public void SetPresenter(IValuesPresenterResultsForControl presenter)
        {
            _presenter = presenter;
        }

        public void InitControlData()
        {
            try
            {
                gvResults.BeginUpdate();
                gvResults.Columns.Clear();
                var dataForCtrl = (List<MasterPathsView>)_presenter.ShowDataForCtrl();
                dataForCtrl.Add(new MasterPathsView
                { 
                    OutputName = "Сумма по всем маршрутам",
                    PathName = "ИТОГО",
                    AbsoluteFixedCost = dataForCtrl.Where(d=>d.Views != null).Sum(d=>d.AbsoluteFixedCost),
                    AbsoluteVariableCost = dataForCtrl.Where(d => d.Views != null).Sum(d => d.AbsoluteVariableCost),
                    RelativeFixedCost = dataForCtrl.Where(d => d.Views != null).Sum(d => d.RelativeFixedCost),
                    RelativeVariableCost = dataForCtrl.Where(d => d.Views != null).Sum(d => d.RelativeVariableCost),
                    TotalCapacity = dataForCtrl.Where(d => d.Views != null).Sum(d => d.TotalCapacity),
                });
                _pathViews = dataForCtrl;
                gcResults.DataSource = _pathViews;
                gvResults.PopulateColumns();
                gvResults.Columns[0].SortMode = ColumnSortMode.Custom;
                lbResultCount.Text = @" Построено маршрутов: " + dataForCtrl.Count(d => d.Views != null);
                lbResultCount.ForeColor = _pathViews.Count > 0 ? Color.Black : Color.Red;
                var gridView = gcResults.DefaultView as GridView;
                if (gridView != null)
                {
                    gridView.FocusedRowChanged += CtrlResults_FocusedRowChanged;
                    var rowHandle = gridView.FocusedRowHandle;

                    ShowPathForRow(rowHandle);
                }

                GridAfterInit();
            }
            finally
            {
                gvResults.EndUpdate();
            }
        }

        /// <summary>
        /// Настройка сортировки кастомной
        /// </summary>
        /// <param name="sender1"></param>
        /// <param name="e"></param>
        private void GridViewOnCustomColumnSort(object sender1, CustomColumnSortEventArgs e)
        {
            try
            {
                if (e.Column.FieldName != ColumnOutputName) return;

                var val1 = (MasterPathsView)e.RowObject1;
                var val2 = (MasterPathsView)e.RowObject2;

                var name1 = val1.OutputName;
                var name2 = val2.OutputName;

                e.Handled = true;
                e.Result = name1 == "Сумма по всем маршрутам" ? 1 : System.Collections.Comparer.Default.Compare(name1, name2);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void CtrlResults_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ShowPathForRow(e.FocusedRowHandle);
        }

        private void ShowPathForRow(int rowHandle)
        {
            var row = gcResults.DefaultView.GetRow(rowHandle) as MasterPathsView;
            ShowPath(row);            
        }

        private void ShowPath(MasterPathsView path)
        {
            if (path != null && OnPathSelect != null)
                OnPathSelect(path);
        }

        void SelectChildRow(IList<SlavePathView> path, SlavePathView nodeView)
        {
            if (path.Count<2) return;
            
            int index = 0;
            SlavePathView fromNode = null;
            SlavePathView toNode = null;
            foreach (var node in path)
            {
                if (node._Node.ID == nodeView._Node.ID)
                {
                    toNode = node;
                    break;
                }
                fromNode = node;
                index++;
            }
            if (index == 0)
            {
                fromNode = path[0];
                toNode = path[1];
            }
            if (toNode == null)
            {

            }

            if (OnEdgeSelect != null)
                OnEdgeSelect(fromNode, toNode);
        }

        private void childView_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            var childView = sender as GridView; 
            var gridView = gcResults.DefaultView as GridView;
            if (childView == null) return;

            int rowHandle = e.FocusedRowHandle;


            var masterFocucedRowHandle = gridView.FocusedRowHandle;
            
            var masterRowHandle = childView.SourceRowHandle;
            if (masterRowHandle != masterFocucedRowHandle)
            {
                //если мы выделили внутреннюю строку, а верхняя - другая, то нужно
                //сменить маршрут на тот, что внутри
                gridView.FocusedRowHandle = masterRowHandle;
            }

            var path = gcResults.DefaultView.GetRow(masterRowHandle) as MasterPathsView;
            var node = childView.GetRow(rowHandle) as SlavePathView;

            SelectChildRow(path.Views, node);
        }

        private void GridAfterInit()
        {
            Graphics gr = gcResults.CreateGraphics();
            gvResults.BeginUpdate();
            gvResults.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            gvResults.Columns[ColumnPathName].AppearanceHeader.Options.UseTextOptions =
                gvResults.Columns[ColumnTotalRelativeFixedCost].AppearanceHeader.Options.UseTextOptions =
                    gvResults.Columns[ColumnTotalRelativeVariableCost].AppearanceHeader.Options.UseTextOptions =
                        gvResults.Columns[ColumnAbsoluteFixedCost].AppearanceHeader.Options.UseTextOptions =
                            gvResults.Columns[ColumnAbsoluteVariableCost].AppearanceHeader.Options.UseTextOptions =
                                gvResults.Columns[ColumnTotalCapacity].AppearanceHeader.Options.UseTextOptions = true;

            gvResults.Columns[ColumnPathName].AppearanceHeader.TextOptions.WordWrap =
                gvResults.Columns[ColumnTotalRelativeFixedCost].AppearanceHeader.TextOptions.WordWrap =
                    gvResults.Columns[ColumnTotalRelativeVariableCost].AppearanceHeader.TextOptions.WordWrap =
                        gvResults.Columns[ColumnAbsoluteFixedCost].AppearanceHeader.TextOptions.WordWrap =
                            gvResults.Columns[ColumnAbsoluteVariableCost].AppearanceHeader.TextOptions.WordWrap =
                                gvResults.Columns[ColumnTotalCapacity].AppearanceHeader.TextOptions.WordWrap =
                                    DevExpress.Utils.WordWrap.Wrap;
            gvResults.Columns[ColumnPathName].Fixed = FixedStyle.Left;
            //gvResults.Columns[ColumnTotalRelativeFixedCost].SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;

            int wdth = (int) gr.MeasureString("Маршруты", gvResults.Appearance.Row.Font).Width;
            foreach (var pth in _pathViews)
            {
                var tmp = gr.MeasureString(pth.PathName, gvResults.Appearance.Row.Font);
                if (wdth < (int) tmp.Width)
                {
                    wdth = (int) tmp.Width;
                }
            }
            //if (FirstTime)
            {
                gvResults.Columns[ColumnTotalRelativeFixedCost].OptionsColumn.FixedWidth =
                    gvResults.Columns[ColumnTotalRelativeVariableCost].OptionsColumn.FixedWidth =
                        gvResults.Columns[ColumnAbsoluteFixedCost].OptionsColumn.FixedWidth =
                            gvResults.Columns[ColumnAbsoluteVariableCost].OptionsColumn.FixedWidth =
                                gvResults.Columns[ColumnTotalCapacity].OptionsColumn.FixedWidth = true;
                gvResults.Columns[ColumnTotalRelativeFixedCost].Width = 130;
                gvResults.Columns[ColumnTotalRelativeVariableCost].Width = 130;
                gvResults.Columns[ColumnAbsoluteFixedCost].Width = 130;
                gvResults.Columns[ColumnAbsoluteVariableCost].Width = 130;
                gvResults.Columns[ColumnTotalCapacity].Width = 85;
                //FirstTime = false;

                gvResults.Columns[ColumnTotalRelativeFixedCost].Width = 130;
                gvResults.Columns[ColumnTotalRelativeVariableCost].Width = 130;
                gvResults.Columns[ColumnAbsoluteFixedCost].Width = 130;
                gvResults.Columns[ColumnAbsoluteVariableCost].Width = 130;
                gvResults.Columns[ColumnTotalCapacity].Width = 85;

            }
            gvResults.Columns[ColumnPathName].Width = wdth + 35;

            if (_coreHolder.Mode == ModeWork.Single)
            {
                gvResults.Columns[ColumnOutputName].Visible = false;

                gvResults.Columns[ColumnAbsoluteFixedCost].Visible = false;
                gvResults.Columns[ColumnAbsoluteVariableCost].Visible = false;
            }
            else
            {
                gvResults.Columns[ColumnOutputName].Group();
                gvResults.ExpandAllGroups();

                gvResults.Columns[ColumnTotalCapacity].Caption = @"Объём поставок, млн м³";

                gvResults.Columns[ColumnTotalRelativeFixedCost].Visible = false;
                gvResults.Columns[ColumnTotalRelativeVariableCost].Visible = false;
            }
           
            //---
            gvResults.CustomColumnDisplayText += (o, e) => e.DisplayText = String.Format(CultureInfo.CurrentCulture, "{0:N2}", e.Value);
            //----
            gvResults.EndUpdate();
        }

        public void UpdateCtrl()
        {
            InitControlData();
        }

        private void gvResults_MasterRowExpanded(object sender, CustomMasterRowEventArgs e)
        {
            var view = sender as GridView;
            GridView childView = view.GetDetailView(e.RowHandle, e.RelationIndex) as GridView;
            if (childView == null) return;


            childView.FocusedRowChanged += childView_FocusedRowChanged;
         
            childView.OptionsBehavior.ReadOnly = true;
            childView.OptionsCustomization.AllowFilter =
                childView.OptionsCustomization.AllowGroup =
                    childView.OptionsCustomization.AllowSort = false;
            childView.CustomDrawCell += gvResults_CustomDrawCell;
            BindingList<SlavePathView> ds = (gvResults.GetRow(e.RowHandle) as MasterPathsView).Views;
            InitChildGridProperties(childView, ds);
        }

        private void InitChildGridProperties(GridView childView, BindingList<SlavePathView> ds)
        {
            Graphics gr = gcResults.CreateGraphics();
            childView.BeginUpdate();
            childView.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;

            childView.Columns[ColumnDirection].Fixed = childView.Columns[ColumnZoneName].Fixed =
                childView.Columns[ColumnObjName].Fixed = FixedStyle.Left;

            int wdthObjName = (int) gr.MeasureString("Наименование", gvResults.Appearance.Row.Font).Width;
            int wdthZoneName = (int) gr.MeasureString("Балансовая", gvResults.Appearance.Row.Font).Width;
            int wdthDirect = (int) gr.MeasureString("Направление", gvResults.Appearance.Row.Font).Width;
            foreach (var pth in ds)
            {
                var tmpObj = gr.MeasureString(pth.NodeName, gvResults.Appearance.Row.Font);
                if (wdthObjName < tmpObj.Width)
                {
                    wdthObjName = (int) tmpObj.Width;
                }
                var tmpZone = gr.MeasureString(pth.Zone, gvResults.Appearance.Row.Font);
                if (wdthZoneName < tmpZone.Width)
                {
                    wdthZoneName = (int) tmpZone.Width;
                }
            }

            childView.OptionsCustomization.AllowSort = false;
            childView.Columns[ColumnZoneName].Width = wdthZoneName + 20;
            childView.Columns[ColumnObjName].Width = wdthObjName + 20;
            childView.Columns[ColumnDirection].Width = wdthDirect;

            childView.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            childView.Columns[ColumnZoneName].AppearanceHeader.Options.UseTextOptions =
                childView.Columns[ColumnObjName].AppearanceHeader.Options.UseTextOptions =
                    childView.Columns[ColumnRelativeFixedCost].AppearanceHeader.Options.UseTextOptions =
                        childView.Columns[ColumnRelativeVariableCost].AppearanceHeader.Options.UseTextOptions =
                            childView.Columns[ColumnAbsoluteFixedCost].AppearanceHeader.Options.UseTextOptions =
                                childView.Columns[ColumnAbsoluteVariableCost].AppearanceHeader.Options.UseTextOptions =
                                    childView.Columns[ColumnCapacity].AppearanceHeader.Options.UseTextOptions = true;

            childView.Columns[ColumnZoneName].AppearanceHeader.TextOptions.WordWrap =
                childView.Columns[ColumnObjName].AppearanceHeader.TextOptions.WordWrap =
                    childView.Columns[ColumnRelativeFixedCost].AppearanceHeader.TextOptions.WordWrap =
                        childView.Columns[ColumnRelativeVariableCost].AppearanceHeader.TextOptions.WordWrap =
                            childView.Columns[ColumnAbsoluteFixedCost].AppearanceHeader.TextOptions.WordWrap =
                                childView.Columns[ColumnAbsoluteVariableCost].AppearanceHeader.TextOptions.WordWrap =
                                    childView.Columns[ColumnCapacity].AppearanceHeader.TextOptions.WordWrap =
                                        DevExpress.Utils.WordWrap.Wrap;
            //if (FirstTime)
            {
                childView.Columns[ColumnRelativeFixedCost].OptionsColumn.FixedWidth =
                    childView.Columns[ColumnRelativeVariableCost].OptionsColumn.FixedWidth =
                        childView.Columns[ColumnAbsoluteFixedCost].OptionsColumn.FixedWidth =
                            childView.Columns[ColumnAbsoluteVariableCost].OptionsColumn.FixedWidth =
                                childView.Columns[ColumnCapacity].OptionsColumn.FixedWidth = true;
                childView.Columns[ColumnRelativeFixedCost].Width = 130;
                childView.Columns[ColumnRelativeVariableCost].Width = 130;
                childView.Columns[ColumnAbsoluteFixedCost].Width = 130;
                childView.Columns[ColumnAbsoluteVariableCost].Width = 130;
                childView.Columns[ColumnCapacity].Width = 85;
                //FirstTime = false;
            }

            if (_coreHolder.Mode == ModeWork.Single)
            {
                childView.Columns[ColumnAbsoluteFixedCost].Visible = false;
                childView.Columns[ColumnAbsoluteVariableCost].Visible = false;
            }
            else
            {
                childView.Columns[ColumnCapacity].Caption = @"Объём поставок, млн м³";

                childView.Columns[ColumnRelativeFixedCost].Visible = false;
                childView.Columns[ColumnRelativeVariableCost].Visible = false;
            }
            childView.CustomColumnDisplayText += (o, e) => e.DisplayText = String.Format(CultureInfo.CurrentCulture, "{0:N2}", e.Value);
            childView.EndUpdate();
        }

        private void gvResults_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            e.Appearance.BackColor = e.Appearance.BackColor2 = e.RowHandle % 2 == 0 ? Color.White : Color.LightCyan;
        }
        
        public void SetCoreHolder(IGExDomain coreHolder)
        {
            _coreHolder = coreHolder;
        }
    }
}