﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using CommonUIControls;
using GasExportApp.Dialogs;
using GEx.CoreHolder;
using GEx.ErrorHandler;
using GEx.Iface;

namespace GasExportApp
{
    public class MainFormPresenter : IGExDomainCallback
    {
        private readonly List<IGExDomainCallback> _childCallbackList = new List<IGExDomainCallback>();
        //private Dictionary<INodeView, int> _goNodeToNodeIdDictionary = new Dictionary<INodeView,int> ();

        private readonly IGExDomain _coreHolder = new CoreHolder();
        private readonly IErrorOutput _errorHandler;

        /// <summary>
        ///     Пока мы знаем ВСЁ (!!!) о GoView и о методах
        ///     работы с ним.
        ///     Для отвязки от конкретной реализации нужно понимать, что
        ///     от такого _goView (представление сети) нам нужно (AddToNetWork, EnumerateNetworkObjects)
        ///     и здесь пользоваться ТОЛЬКО интерфейсом
        ///     (это нужно для возможной замены GoView)
        /// </summary>
        private readonly INetworkView _goView;

        //_goNodeToNodeIdDictionary можно удалить
        private readonly Dictionary<DTO_Node, INodeView> _nodeToGoNodeDictionary = new Dictionary<DTO_Node, INodeView>();
        private IDiagramFactory _diagramFactory;

        private readonly IMainForm _view;
        private int? _schemeID;
        private int? _scenarioID;
        private List<DTO_Node> _firstPoints = new List<DTO_Node>();
        private List<DTO_Node> _lastPoints = new List<DTO_Node>();
        private ModeWork _mode;

        public MainFormPresenter(IErrorOutput eh, INetworkView goView, IDiagramFactory diagramFactory, IMainForm view)
        {
            _errorHandler = eh;
            _goView = goView;
            _diagramFactory = diagramFactory;
            _view = view;
        }

        public void Init()
        {
            _coreHolder.Init(_errorHandler, this);
            Mode = _coreHolder.Mode;
        }

        public IGExDomain CoreHolder
        {
            get { return _coreHolder; }
        }

        public void OnNodeUpdated(DTO_Node node)
        {
            foreach (var cbk in _childCallbackList)
                cbk.OnNodeUpdated(node);
        }

        public void OnRemoveNode(int nodeId)
        {
            ClearLinksGoView();

            var firstNode = _firstPoints.FirstOrDefault(f => f.ID == nodeId);
            if (firstNode != null) _firstPoints.Remove(firstNode);

            var lastNode = _lastPoints.FirstOrDefault(l => l.ID == nodeId);
            if (lastNode != null) _lastPoints.Remove(lastNode);

            _view.RefreshSomeCaptions();
            foreach (var cbk in _childCallbackList)
                cbk.OnRemoveNode(nodeId);
        }

        public void RemovePointPath(int nodeId)
        {
            var firstNode = _firstPoints.FirstOrDefault(f => f.ID == nodeId);
            if (firstNode != null) _firstPoints.Remove(firstNode);

            var lastNode = _lastPoints.FirstOrDefault(l => l.ID == nodeId);
            if (lastNode != null) _lastPoints.Remove(lastNode);
        }

        public void OnCalculated()
        {
            foreach (var cbk in _childCallbackList)
                cbk.OnCalculated();
        }

        public void OnRemoveNodeAsSelectedForPath(DTO_Node node)
        {
            _goView.RemoveNodeAsSelectedForPath(node);
        }

        public void AddChildPresenter(IGExDomainCallback childPresenter)
        {
            _childCallbackList.Add(childPresenter);
        }

        public void RemoveChildPresenter(IGExDomainCallback childPresenter)
        {
            if (_childCallbackList.Any(e => e == childPresenter))
                _childCallbackList.Remove(childPresenter);
        }

        /// <summary>
        ///     Создание объектов в goView
        ///     из уже загруженной схемы
        /// </summary>
        /// <param name="schema"></param>
        /// <param name="goView"></param>
        private void LoadScheme( /*Schema schema*/)
        {
            // Для загрузки схемы использовать эти 2 метода
            //        IEnumerable<DTO_Node> IGExDomain.GetSchemaNodes()
            //        IEnumerable<Tuple<DTO_Node,DTO_Node>> LoadNodeRelations();
            _nodeToGoNodeDictionary.Clear();
            var nodes = _coreHolder.GetSchemaNodes();
            foreach (var node in nodes)
            {
                INodeView obj = _diagramFactory.CreateNode();
                obj.Name = node.Name;

                obj.X = (float)node.X;
                obj.Y = (float)node.Y;

                obj.LabelX = (float?)node.LabelX ?? obj.LabelX;
                obj.LabelY = (float?)node.LabelY ?? obj.LabelY;

                obj.NodeDocument = node;


                //TODO: как вариант - нужно переделать контекстное меню - одно для всех объектов

                obj.OnChoosingFirstPoint += _coreHolder.SetFirstPointsForPath;
                obj.OnChosenFirstPoint += point =>
                {
                    if (Mode == ModeWork.Single) _firstPoints.Clear();
                    _firstPoints.Add(point);

                    if (Mode == ModeWork.Single) _view.FirstPointClear();
                    _view.AddFirstPoint(point);
                };

                obj.ChooseSecondPoint += _coreHolder.SetLastPointsForPath;
                obj.ChooseSecondPoint += point =>
                {
                    if (Mode == ModeWork.Single) _lastPoints.Clear();
                    _lastPoints.Add(point);

                    if (Mode == ModeWork.Single) _view.LastPointClear();
                    _view.AddLastPoint(point);
                };

                obj.RemovePoint += dtoNode =>
                {
                    _coreHolder.RemovePoint(dtoNode);
                    RemovePointPath(dtoNode.ID);
                    _view.RemovePoint(dtoNode);
                };

                obj.NodeType = node.Type;
                //obj.Label.Editable = true;
                obj.SetLabelFontSize(float.Parse(ConfigurationManager.AppSettings["NodeFontSize"]));
                //obj.Label.Alignment = GoText.TopCenter;
                _nodeToGoNodeDictionary[node] = obj;
                //   _goNodeToNodeIdDictionary[obj] = node.ID;
                _goView.AddToNetwork(obj);
            }

            _view.AddAllPoints(nodes);
        }

        public void AddCreatedVisualNodeToScheme(INodeView gn)
        {
            var node = new DTO_Node();
            node.Type = gn.NodeType;
            gn.NodeDocument = node;

            // node.ID = -1;
            node.Name = gn.Name;
            node.X = gn.X;
            node.Y = gn.Y;

            node.LabelX = gn.LabelX;
            node.LabelY = gn.LabelY;

            node.ZoneName = "";

            _nodeToGoNodeDictionary[node] = gn;

            var nodeId = _coreHolder.AddNode(node, true);
            node.ID = nodeId;

            gn.OnChoosingFirstPoint += _coreHolder.SetFirstPointsForPath;
            gn.ChooseSecondPoint += _coreHolder.SetLastPointsForPath;
            gn.RemovePoint += dtoNode =>
            {
                _coreHolder.RemovePoint(dtoNode);
                RemovePointPath(dtoNode.ID);
                _view.RemovePoint(dtoNode);
            };
        }

        /// <summary>
        ///     Выбор и загрузка схемы
        /// </summary>
        /// <param name="gv">
        ///     GoView, куда будут загружены объекты
        /// </param>
        public void OnLoad()
        {
            var lf = new LoadForm(_coreHolder);

            var dialogResult = lf.ShowDialog();

            if (dialogResult != DialogResult.OK)
                return;

            _schemeID = lf.SchemeID;

            if (_schemeID == null) return;
            try
            {
                _view.OnWaitCursor();
                _goView.ClearDoc();
                ClearLinksGoView(); // тут не нужен
                ClearPointsGoViewPresenterCoreHolderMainForm();
                ClearDictMode();
                
                var point =
                    //Выбрали центр в районе точки 'Lanzhot'
                    new PointF(2820.30444335938f, 1788.36059570313f);

                _goView.SetPosition(point.X, point.Y);

                _coreHolder.LoadSchema((int)_schemeID);
               
                var schema = _coreHolder.GetSchema();
                if (schema.Image != null)
                {
                    Bitmap bitmap;
                    using (var ms = new MemoryStream(schema.Image))
                    {
                        bitmap = new Bitmap(ms);
                    }
                    _goView.SetUnderlayer(bitmap);
                }

                //_coreHolder.CreateNewScenario();
                LoadScheme();
            }
            finally
            {
                _view.OnDefaultCursor();
            }
        }

        /// <summary>
        ///     Выбор и загрузка сценария
        /// </summary>        
        public void OnLoadScenario()
        {
            var lf = new LoadFormScenario();
            lf.Init(this, _errorHandler);

            var dialogResult = lf.ShowDialog();

            if (dialogResult != DialogResult.OK || lf.ScenarioID == null)
                return;
            try
            {
                _view.OnWaitCursor();
                _scenarioID = lf.ScenarioID;
                _coreHolder.LoadScenario((int)_scenarioID);
            }
            finally
            {
                _view.OnDefaultCursor();
            }
        }

        public void GenerateModel()
        {
        }

        public IEnumerable<DTO_BalanceZone> GetBalanceZones()
        {
            return
                _coreHolder.GetBalanceZones().Select(bz => new DTO_BalanceZone { ID = bz.ID, Name = bz.Name }).ToArray();
        }

        public DTO_Schema GetScheme()
        {
            return _coreHolder.GetSchema();
        }

        public IEnumerable<DTO_Node> GetSchemaNodes()
        {
            return _coreHolder.GetSchemaNodes();
        }

        public IEnumerable<DTO_NodeRelation> GetNodesRelations()
        {
            return _coreHolder.LoadNodeRelations();
        }

        public bool IsSchemaLoaded
        {
            get { return _coreHolder.IsSchemaLoaded(); }
        }

        public bool AllNodesSaved()
        {
            return _coreHolder.AllNodesSaved();
        }

        public bool ScenarioHasUnsavedChanges()
        {
            return _coreHolder.ScenarioHasUnsavedChanges();
        }

        public bool SchemaHasChanges()
        {
            return _coreHolder.HasSchemaChanges();
        }

        public DTO_Scenario Scenario
        {
            get { return _coreHolder.GetScenario(); }
        }

        public void UpdateScenarioNameAndComment(string Name, string comm)
        {
            _coreHolder.UpdateScenarioName(Name, comm);
        }

        public void CreateNew(bool useBasic)
        {
            _coreHolder.CreateNewSchema(useBasic);
            if (!useBasic) _goView.ClearDoc();

            ClearLinksGoView();
            ClearPointsGoViewPresenterCoreHolderMainForm();
            ClearDictMode();
        }

        public void OnSave()
        {
            _coreHolder.SaveSchema(_goView.MapUnderlayer);
        }

        public void OnSaveScenario()
        {
            _coreHolder.SaveScenario();
        }

        public void OnRemoveScenario(int IDscenario)
        {
            _coreHolder.RemoveScenario(IDscenario);
        }

        public void OnCreateScenario()
        {
            _coreHolder.CreateNewScenario();
        }

        public void OnSaveAs()
        {
            _coreHolder.SaveSchemaAsNew(_goView.MapUnderlayer);
        }

        public void OnSaveScenarioAs()
        {
            _coreHolder.SaveScenarioAsNew();
        }

        public void Calculate()
        {
            _coreHolder.Calculate();
        }

        public bool TerminalPointsSelected()
        {
            return _coreHolder.TerminalPointsSelected();
        }

        internal void OnLoadMatrix()
        {
        }

        public void OnSaveMatrix(IEnumerable<DTO_NodeRelation> nodeRelations)
        {
            _coreHolder.UpdateNodeRelations(nodeRelations);
        }

        public void OnSaveNodesRelationsToBalZones(IEnumerable<DTO_Node> nodes)
        {
            foreach (var node in nodes)
            {
                //Синхронизация с внутренней структорой
                var pair = _nodeToGoNodeDictionary.FirstOrDefault(p => p.Key.ID == node.ID);
                pair.Key.ZoneId = node.ZoneId;
                pair.Key.ZoneName = node.ZoneName;

                _coreHolder.UpdateNode(node);
            }
        }

        public void UpdateSchemaNameAndComment(string name, string comm)
        {
            var schema = _coreHolder.GetSchema();
            schema.SchemaName = name;
            schema.Comment = comm;
            _coreHolder.UpdateSchema(schema);
        }

        public void ShowCalculated(MasterPathsView pathsView)
        {
            ClearLinksGoView();
            var paths = pathsView;
            var path = paths.Views;

            if (path != null)
            {
                var list = path.Select(p =>
                {
                    var dtoNode = _nodeToGoNodeDictionary.Keys.FirstOrDefault(x => p._Node != null && x.ID == p._Node.ID);

                    if (dtoNode == null) return null;
                    return _nodeToGoNodeDictionary[dtoNode];
                }
                    ).ToList();
                var tuples = list.Zip(list.Skip(1), Tuple.Create).
                    //без петель
                    Where(x => x.Item1 != x.Item2);

                var graphPath = tuples.Select(t =>
                {
                    var link = _diagramFactory.CreateConnection();
                    link.NodeFrom = t.Item1;
                    link.NodeTo = t.Item2;
                    return link;
                }).ToList();

                graphPath.ForEach(link => _goView.AddToNetwork(link));
            }
        }

        public void AddBalanceZone(string zoneName)
        {
            _coreHolder.AddBalanceZone(new DTO_BalanceZone() { Name = zoneName });
        }

        #region IGExDomainCallback

        void IGExDomainCallback.CallGui(string msg)
        {
            throw new NotImplementedException();
        }

        void IGExDomainCallback.BeforeNewSchemaSave(DTO_Schema sch)
        {
            foreach (var cbk in _childCallbackList)
                cbk.BeforeNewSchemaSave(sch);
        }

        void IGExDomainCallback.OnScenarioLoaded()
        {
            // TODO: обновить все контролы сценария
            foreach (var cbk in _childCallbackList)
                cbk.OnScenarioLoaded();
        }

        void IGExDomainCallback.OnScenarioSaved()
        {
        }

        void IGExDomainCallback.OnNodeAdded()
        {
            foreach (var cbk in _childCallbackList)
                cbk.OnNodeAdded();
        }


        void IGExDomainCallback.OnSchemaSaved(SavedEntityInfo schemaInfo, IEnumerable<SavedEntityInfo> nodesInfo)
        {
            //Обновление словарей ID*Узел
            foreach (var savedEntityInfo in nodesInfo)
            {
                var nodeDtoForChange =
                    _nodeToGoNodeDictionary.Keys.FirstOrDefault(nodeDto => nodeDto.ID == savedEntityInfo.IdBeforeSave);
                if (nodeDtoForChange != null)
                    nodeDtoForChange.ID = savedEntityInfo.IdAfterSave;
                foreach (var cbk in _childCallbackList)
                    cbk.OnSchemaSaved(schemaInfo, nodesInfo);
            }
        }

        #endregion

        public void UpdateNodeName(INodeView node)
        {
            _coreHolder.UpdateNodeName(node.NodeDocument.ID, node.Name);
        }

        public void UpdateNodeOutput(INodeView node)
        {
            _coreHolder.UpdateNodeOutput(node.NodeDocument.ID, node.NodeDocument.IsOutput);
        }

        public bool HasCalculatedResults()
        {
            return _coreHolder.GetCalculatedPaths().Count() > 0;
        }

        public void RemoveNode(INodeView node)
        {
            _coreHolder.RemoveNode(node.NodeDocument);
        }

        public void ChangeNodeLocation(INodeView goNode)
        {
            _coreHolder.UpdateNodePosition(goNode.NodeDocument.ID, goNode.X, goNode.Y);
        }

        public void ChangeNodeLabelPosition(DTO_Node node, float labelX, float labelY)
        {
            _coreHolder.UpdateNodeLabelOffset(node.ID, labelX, labelY);
        }

        /// <summary>
        /// Очистка маршрутов (edje) на схеме GoView
        /// </summary>
        public void ClearLinksGoView()
        {
            _goView.ClearLinks();
        }

        /// <summary>
        /// Удалить начальные и конечные точки GoView и Presenter и CoreHolder и MainForm
        /// </summary>
        public void ClearPointsGoViewPresenterCoreHolderMainForm()
        {
            _firstPoints.ForEach(p =>
            {
                _goView.RemoveNodeAsSelectedForPath(p);
            });
            _lastPoints.ForEach(p =>
            {
                _goView.RemoveNodeAsSelectedForPath(p);
            });

            _firstPoints = new List<DTO_Node>();
            _lastPoints = new List<DTO_Node>();

            _coreHolder.ClearPointsPaths();

            _view.FirstPointClear();
            _view.LastPointClear();
        }

        /// <summary>
        /// Очистить словарь с режимами и выбранными точками в презентере
        /// Не вносить в методы очистки точек, так как логика изменения режима 
        /// и логика применения метод очистки разниться
        /// </summary>
        public void ClearDictMode()
        {
            _dictPathNodes.Clear();
            _dictModeResult.Clear();
        }

        public void OnUpdateBalZones(IEnumerable<DTO_BalanceZone> zones)
        {
            _coreHolder.UpdateBalanceZones(zones);
        }

        public void OnSaveOperators()
        {
            _coreHolder.SaveOperators();
        }

        public string GetCaptionAfterNodeChosen()
        {
            var caption = string.Empty;

            if (_coreHolder.GetSchema() != null)
            {
                caption = _coreHolder.GetSchema().SchemaName;
            }

            if (Mode == ModeWork.Single)
            {
                if (_firstPoints.Any()) 
                    caption += string.Format(", Начальная точка: {0}", _firstPoints.First().Name);
                
                if (_lastPoints.Any())
                    caption += string.Format(", Конечная точка:  {0}", _lastPoints.First().Name);
            }
            else
            {
                if (_firstPoints.Any())
                    caption += string.Format(", Начальные точки: {0}", _firstPoints.Select(n=>n.Name).Aggregate((s1, s2)=>s1 + ", " + s2));

                if (_lastPoints.Any())
                    caption += string.Format(", Конечные точки:  {0}", _lastPoints.Select(n=>n.Name).Aggregate((s1, s2)=>s1 + ", " + s2));
            }

            return caption;
        }


        public void OnSchemaInit()
        {
            _view.OnschemaInit();
            foreach (var cbk in _childCallbackList)
                cbk.OnSchemaInit();
        }

        public bool isNewScenario()
        {
            return _coreHolder.ScenarioIsNew();
        }

        public string ScenarioName
        {
            get { return _coreHolder.GetScenario() != null ? _coreHolder.GetScenario().Name : string.Empty; }
        }

        private Dictionary<ModeWork, IGeneralDocResult> _dictModeResult = new Dictionary<ModeWork, IGeneralDocResult>();

        /// <summary>
        /// Key: ModeWork
        /// Value: item1 - начальные точки, item2 - конечные точки
        /// </summary>
        private Dictionary<ModeWork, Tuple<List<DTO_Node>, List<DTO_Node>>> _dictPathNodes = new Dictionary<ModeWork, Tuple<List<DTO_Node>, List<DTO_Node>>>();

        public ModeWork Mode
        {
            get { return _mode; }
            set
            {
                // запомним выбранные точки до смены режима
                if (!_dictPathNodes.ContainsKey(_mode))
                    _dictPathNodes.Add(_mode, new Tuple<List<DTO_Node>, List<DTO_Node>>(_firstPoints, _lastPoints));
                else
                    _dictPathNodes[_mode] = new Tuple<List<DTO_Node>, List<DTO_Node>>(_firstPoints, _lastPoints);

                ClearLinksGoView();
                ClearPointsGoViewPresenterCoreHolderMainForm();

                // изменим режим
                _mode = value;

                _coreHolder.Mode = _mode;

                var result = _coreHolder.DocResult;
                if (result != null)
                {
                    if (!_dictModeResult.ContainsKey(result.Mode))
                        _dictModeResult.Add(result.Mode, result);
                    else
                        _dictModeResult[result.Mode] = result;
                }

                _dictModeResult.TryGetValue(_mode, out result);
                _coreHolder.ShowCalculate(result);

                // покажем выбранные точки для режима
                Tuple<List<DTO_Node>, List<DTO_Node>> tuple;
                _dictPathNodes.TryGetValue(_mode, out tuple);
                if (tuple != null)
                {
                    _firstPoints = tuple.Item1;
                    _lastPoints = tuple.Item2;

                    _firstPoints.ForEach(p =>
                    {
                        _goView.SetNodeAsSelectedForPath(p);
                        _coreHolder.SetFirstPointsForPath(p);
                        _view.AddFirstPoint(p);
                    });
                    _lastPoints.ForEach(p =>
                    {
                        _goView.SetNodeAsSelectedForPath(p);
                        _coreHolder.SetLastPointsForPath(p);
                        _view.AddLastPoint(p);
                    });
                }

                _view.OnPathPointSet();
            }
        }

        public void SaveScenarioAs()
        {
            _coreHolder.SaveScenarioAsNew();
        }

        public void UpdateGazExportObjectsZones()
        {
            _coreHolder.UpdateGazExportObjectsZones();
            _coreHolder.UpdateRelations();
        }

        public void SelectEdgeInPath(SlavePathView nodeFrom, SlavePathView nodeTo)
        {
            var links = _goView.EnumerateLinks();
            foreach (var link in links)
            {
                link.DeSelect();
            }


            var dtoNodeFrom = _nodeToGoNodeDictionary.Keys.FirstOrDefault(x => x.ID == nodeFrom._Node.ID);
            var dtoNodeTo = _nodeToGoNodeDictionary.Keys.FirstOrDefault(x => x.ID == nodeTo._Node.ID);

            foreach (var link in links)
            {
                var linkNodeFrom = link.NodeFrom;
                var linkNodeTo = link.NodeTo;

                if ((linkNodeFrom.NodeDocument.ID == dtoNodeFrom.ID)
                    && (linkNodeTo.NodeDocument.ID == dtoNodeTo.ID))
                {
                    link.Select();
                }

            }

        }

        public void SaveNodeZones(List<KeyValuePair<int, DTO_BalanceZone>> nodeZonesForUpdate)
        {
            foreach (var nz in nodeZonesForUpdate)
            {
                _coreHolder.SetConnections(nz.Key, nz.Value);
            }
        }

        /// <summary>
        /// Вызывается из контрола, добавляем первую точку
        /// </summary>
        /// <param name="node"></param>
        public void AddFirstPoint(DTO_Node node)
        {
            if (node == null) return;

            var result = _coreHolder.SetFirstPointsForPath(node);
            if (!result)
            {
                MessageBox.Show(String.Format("{0} не может быть выбран в качестве первой точки", node.Name), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            _goView.SetNodeAsSelectedForPath(node);
            
            if (Mode == ModeWork.Single) _firstPoints.Clear();
            _firstPoints.Add(node);

            if (Mode == ModeWork.Single) _view.FirstPointClear();
            _view.AddFirstPoint(node);
        }

        /// <summary>
        /// Вызывается из контрола, добавляем последнею точку
        /// </summary>
        public void AddLastPoint(DTO_Node node)
        {
            if (node == null) return;

            if (!node.IsOutput)
            {
                MessageBox.Show(String.Format("{0} не может быть выбран в качестве второй точки", node.Name), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            _coreHolder.SetLastPointsForPath(node);
            _goView.SetNodeAsSelectedForPath(node);
            
            if (Mode == ModeWork.Single) _lastPoints.Clear();
            _lastPoints.Add(node);

            if (Mode == ModeWork.Single) _view.LastPointClear();
            _view.AddLastPoint(node);
        }

        /// <summary>
        /// Вызывается из контрола, удаляем точку
        /// </summary>
        public void RemovePoint(DTO_Node node)
        {
            if (node == null) return;
            
            RemovePointPath(node.ID);
            _coreHolder.RemovePoint(node);
            _goView.RemoveNodeAsSelectedForPath(node);
            _view.RemovePoint(node);
        }
    }
}