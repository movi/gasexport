﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using GEx.Iface;
//using GEx.Domain;

namespace CommonUIControls
{
    public partial class BalanceZonesCtrl : XtraUserControl
    {
        private RepositoryItemLookUpEdit _riLookup;

        public BalanceZonesCtrl()
        {
            InitializeComponent();

        }

        public IEnumerable<DTO_Node> Nodes { get; set; }

        public List<DTO_BalanceZone> BalanceZones { get; set; }

        private BalanceZonesPresenter _presenter;

        public BalanceZonesPresenter Presenter
        {
            get { return _presenter; }
            set { _presenter = value; }
        }

        public void Init()
        {
            if (Nodes == null) return;

            var zones = BalanceZones;

            //_presenter = new BalanceZonesPresenter(Nodes, BalanceZones);
            var masterSource = _presenter.PrepareMasterSource();

            gcBalZones.DataSource = masterSource;
            gvBalZones.Columns["Name"].OptionsColumn.AllowFocus =
            gvBalZones.Columns["Name"].OptionsColumn.AllowEdit = false;
            BalanceZones.Sort((x, y) => x.Name.CompareTo(y.Name));

            gcOperators.DataSource = BalanceZones;
            //настройка вида списка операторов
            gvOperators.Columns["ID"].Visible = false;

            if (zones == null) return;

            PrepareLookUpEdit(zones);

            gvBalZones.MasterRowExpanded += gridView1_MasterRowExpanded;
            gcBalZones.ViewRegistered += gridControl1_ViewRegistered;

            //Включение режима объединенных ячеек (глобально для всего грида)
            gvBalZones.OptionsView.AllowCellMerge = true;
            //gvOperators.OptionsView.AllowCellMerge = false;
            gcBalZones.ContextMenuStrip = GetContextMenuStrip();
        }

        //recommended detailheight for expanded views is 1000 px
        private void gridControl1_ViewRegistered(object sender, DevExpress.XtraGrid.ViewOperationEventArgs e)
        {
            GridView view = e.View as GridView;
            if (view != null)
                view.DetailHeight = 1000;
        }

        private ContextMenuStrip GetContextMenuStrip()
        {
            var m = new ContextMenuStrip();

            var item = new ToolStripButton("Добавить");
            m.Items.Add(item);
            item.Click += AddToBalanceZone;

            item = new ToolStripButton("Удалить");
            m.Items.Add(item);
            item.Click += Remove;

            return m;
        }

        private void Remove(object sender, EventArgs eventArgs)
        {
            var bw = GetFocusedBorderView();
            if (bw != null)
            {
                _presenter.RemoveElementFromBorderViews(bw);
            }
        }

        private void AddToBalanceZone(object sender, EventArgs eventArgs)
        {
            var bw = GetFocusedBorderView();

            if (bw != null)
            {
                var newBw = new BalanceZonesPresenter.BorderView(bw.GetNodeID(), bw.NodeName, -1, bw.GetNode());
                _presenter.AddElementToBorderViews(newBw);
            }

            int level = 0;
            GridView detailView = gvBalZones.GetDetailView(gvBalZones.FocusedRowHandle, level) as GridView;
            detailView.RefreshData();
        }

        private GridView _currentDetailView;
        private BalanceZonesPresenter.BorderView GetFocusedBorderView()
        {
            int level = 0;
            _currentDetailView = gvBalZones.GetDetailView(gvBalZones.FocusedRowHandle, level) as GridView;

            if (_currentDetailView != null)
            {
                var bw = _currentDetailView.GetFocusedRow() as BalanceZonesPresenter.BorderView;
                return bw;
            }
            return null;
        }

        /// <summary>
        /// Подготовка RepositoryItemLookUpEdit для использования в качестве
        /// списка для выбора балансовых зон(операторов)
        /// </summary>
        /// <param name="zones">
        /// DataSource для отображения в списке
        /// </param>
        private void PrepareLookUpEdit(IEnumerable<DTO_BalanceZone> zones)
        {
            _riLookup = new RepositoryItemLookUpEdit
            {
                DataSource = zones,
                DisplayMember = GetPropertyName((DTO_BalanceZone t) => t.Name),
                BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup,
                DropDownRows = zones.Count(),
                SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete,
                AutoSearchColumnIndex = 1
            };
            _riLookup.PopulateColumns();
            _riLookup.Columns["ID"].Visible = false;

            var button = new EditorButton()
            {
                Kind = ButtonPredefines.Delete
            };

            _riLookup.ButtonClick += riLookup_ButtonClick;
            _riLookup.Buttons.Add(button);
        }

        /// <summary>
        /// Событие развертывания строки из Main View
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridView1_MasterRowExpanded(object sender, CustomMasterRowEventArgs e)
        {
            GridView childView = this.gvBalZones.GetDetailView(e.RowHandle, e.RelationIndex) as GridView;
            if (childView == null) return;

            var columns = childView.Columns;

            SetLookupEditorForBalanceZoneColumns(columns);
            SetMergeColumn(columns);

            int index = GetPropertyIndex((BalanceZonesPresenter.BorderView t) => t.NodeName);
            SetSortedColumn(childView, index);
            childView.CellValueChanged -= childView_CellValueChanged;
            childView.CellValueChanged += childView_CellValueChanged;
            childView.CustomDrawCell -= childView_CustomDrawCell;
            childView.CustomDrawCell += childView_CustomDrawCell;
            childView.CellMerge -= childView_CellMerge;
            childView.CellMerge += childView_CellMerge;
            childView.GotFocus -= childView_GotFocus;
            childView.GotFocus += childView_GotFocus;
        }

        private void childView_GotFocus(object sender, EventArgs e)
        {
            gvBalZones.FocusedRowHandle = (sender as GridView).SourceRowHandle;
        }

        private void childView_CellMerge(object sender, CellMergeEventArgs e)
        {
            e.Handled = true;
            var firstRow = (sender as GridView).GetRow(e.RowHandle1) as IBalZoneToNode;
            var secondRow = (sender as GridView).GetRow(e.RowHandle2) as IBalZoneToNode;



            if (e.Column.FieldName != "NodeName" || firstRow == null || secondRow == null ||
                (firstRow.GetNodeID() != secondRow.GetNodeID()) || (firstRow.GetNodeName() != secondRow.GetNodeName()))
            {
                e.Merge = false;
            }
            else
            {
                e.Merge = true;
            }
        }

        private void childView_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            e.Appearance.BackColor = e.RowHandle % 2 == 0 || e.Column.FieldName.Equals("NodeName", StringComparison.InvariantCultureIgnoreCase) ? Color.White : Color.LightCyan;
        }

        private void childView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName.Contains("Zone"))
            {
                var t = (sender as GridView).GetFocusedRow();
                var border = t as BalanceZonesPresenter.BorderView;
                int? nodeid = null;
                DTO_BalanceZone zone = null;
                if (border != null)
                {
                    //TODO:разделение на первого и второго оператора, отделить также первый список с одним оператором
                    zone = border.ZoneFirst;
                    nodeid = border.GetNodeID();
                }
                else
                {
                    var b = t as BalanceZonesPresenter.NodeToBalanceZonesConnectionView;
                    if (b != null)
                    {
                        zone = b.Zone;
                        nodeid = b.GetNodeID();
                    }
                }

                if (zone != null && nodeid != null)
                    _presenter.SetConnections(nodeid.Value, zone);
            }
        }

        private void SetSortedColumn(GridView gv, int index)
        {
            var column = gv.Columns.FirstOrDefault(x => x.AbsoluteIndex == index);
            gv.ClearSorting();

            if (column != null)
                column.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
        }

        private int GetPropertyIndex<TObject, TProperty>(Expression<Func<TObject, TProperty>> expression)
        {
            var props = typeof(TObject).GetProperties();

            var member = expression.Body as MemberExpression;

            int order = -1;

            for (int i = 0; i < props.Length; i++)
            {
                var p = props[i];
                if (p == member.Member)
                {
                    order = i;
                    break;
                }
            }
            return order;
        }

        private MemberInfo GetMemberInfo<TObject, TProperty>(Expression<Func<TObject, TProperty>> expression)
        {
            var member = expression.Body as MemberExpression;
            if (member != null)
            {
                return member.Member;
            }

            throw new ArgumentException("expression");
        }

        string GetPropertyName<TObject, TProperty>(Expression<Func<TObject, TProperty>> expression)
        {
            return GetMemberInfo(expression).Name;
        }

        /// <summary>
        /// Установка объединенных ячеек
        /// </summary>
        /// <param name="columns"></param>
        private void SetMergeColumn(GridColumnCollection columns)
        {
            // колонка с наименованием
            var colFirst =
                columns.FirstOrDefault(c => c.FieldName.Equals("NodeName", StringComparison.InvariantCultureIgnoreCase));

            if (colFirst != null)
            {
                foreach (GridColumn column in columns)
                {
                    column.OptionsColumn.AllowMerge = DefaultBoolean.False;
                }

                //Включаем объединенные ячейки только для наименования
                colFirst.OptionsColumn.AllowMerge = DefaultBoolean.True;

                //TODO:включить, если нужно выравнивание
                // colFirst.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center;
                // colFirst.AppearanceCell.TextOptions.VAlignment = VertAlignment.Center;
            }
        }

        /// <summary>
        /// Назначение RepositoryItemLookUpEdit для полей с балансовой зоной(оператором)
        /// </summary>
        /// <param name="columns"></param>
        private void SetLookupEditorForBalanceZoneColumns(GridColumnCollection columns)
        {
            //Поиск по наименованию среди свойств - не лучший вариант
            var col =
                columns.FirstOrDefault(c => c.FieldName.Equals("Zone", StringComparison.InvariantCultureIgnoreCase));
            var col1 =
                columns.FirstOrDefault(c => c.FieldName.Equals("ZoneFirst", StringComparison.InvariantCultureIgnoreCase));
            var col2 =
                columns.FirstOrDefault(
                    c => c.FieldName.Equals("ZoneSecond", StringComparison.InvariantCultureIgnoreCase));

            if (col != null) col.ColumnEdit = _riLookup;
            if (col1 != null) col1.ColumnEdit = _riLookup;
            if (col2 != null) col2.ColumnEdit = _riLookup;
        }

        /// <summary>
        /// Обработка события нажатия ЛЮБОЙ
        /// кнопки RepositoryItemLookUpEdit
        /// Нас интересует кнопка удаления
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void riLookup_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            var btn = e.Button;
            if (btn.Kind == ButtonPredefines.Delete)
            {
                var r = sender as LookUpEdit;
                var item = r.ItemIndex;
                var bz = r.EditValue as DTO_BalanceZone;
                r.EditValue = null;
                r.ItemIndex = -1;
            }
        }

        public void SaveBorders()
        {
            _presenter.SaveInterconnectorsObject(DTO_Node_type.Border);
            _presenter.SaveInterconnectorsObject(DTO_Node_type.VirtualTradingPoint);
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            e.Appearance.BackColor = e.RowHandle % 2 == 0 || e.Column.FieldName.Equals("NodeName", StringComparison.InvariantCultureIgnoreCase) ? Color.White : Color.LightCyan;

        }

        private void gridView1_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {

        }

        private void sbAddOperator_Click(object sender, EventArgs e)
        {
            var t = gvOperators.GetFocusedRow();
            int i = 1;
            while (BalanceZones.Any(zn => zn.Name == "Новый оператор " + i.ToString()))
            {
                i++;
            }
            int newID = getNewZoneId();
            BalanceZones.Add(new DTO_BalanceZone() { ID = newID, Name = "Новый оператор " + i.ToString() });
            gvOperators.RefreshData();
            var newRowHandle = gvOperators.LocateByValue("ID", newID);
            if (newRowHandle >= 0)
            {
                gvOperators.FocusedRowHandle = newRowHandle;
            }
            //gvOperators.MoveLast();
            //var tt = gvOperators.GetFocusedRow();
            ///gvOperators.SetFocusedRowCellValue("Name", "Новый оператор " + i.ToString());
            ///gvOperators.SetFocusedRowCellValue("ID", i);
            //gvOperators.RefreshData();
            //gvOperators.MoveLast();
        }

        private int getNewZoneId()
        {
            var t = BalanceZones.Min(z => z.ID);
            if (t > 0) t = 0;
            return t - 1;
        }

        private void sbDeleteOperator_Click(object sender, EventArgs e)
        {
            if (gvOperators.FocusedRowHandle >= 0)
            {
                if (gvOperators.ActiveEditor != null) gvOperators.HideEditor();
                DTO_BalanceZone zone = gvOperators.GetFocusedRow() as DTO_BalanceZone;
                if (zone != null)
                {
                    if (_presenter.Holder.ZoneHasRelations(zone.ID))
                    {
                        XtraMessageBox.Show("Невозможно удалить оператора с существующими привязками в базе данных.");
                    }
                    else
                    {
                        BalanceZones.Remove(zone);
                        gvOperators.RefreshData();
                    }
                }

            }
        }

        private void gvOperators_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            if (e.Value.ToString().Length == 0)
            {
                e.ErrorText = "Наименование не может быть пустым";
                e.Valid = false;
            }

            if (BalanceZones.Any(zn => zn.Name == e.Value.ToString()))
            {
                e.ErrorText = "Оператор с таким наименованием уже существует";
                e.Valid = false;
            }
        }

        public List<KeyValuePair<int, DTO_BalanceZone>> NodeZones
        {
            get
            {
                return _presenter.ZonesForUpdate;
            }
        }
    }
}