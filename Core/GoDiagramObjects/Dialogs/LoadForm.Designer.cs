﻿namespace GasExportApp.Dialogs
{
    partial class LoadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.sbLoad = new DevExpress.XtraEditors.SimpleButton();
            this.sbCancel = new DevExpress.XtraEditors.SimpleButton();
            this.netsGridControl = new DevExpress.XtraGrid.GridControl();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiRedaction = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRename = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiComment = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.netsGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.netsGridControl)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.netsGridView)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // sbLoad
            // 
            this.sbLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sbLoad.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.sbLoad.Appearance.Options.UseFont = true;
            this.sbLoad.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.sbLoad.Enabled = false;
            this.sbLoad.Location = new System.Drawing.Point(637, 7);
            this.sbLoad.Name = "sbLoad";
            this.sbLoad.Size = new System.Drawing.Size(86, 32);
            this.sbLoad.TabIndex = 3;
            this.sbLoad.Text = "Загрузить";
            this.sbLoad.Click += new System.EventHandler(this.ldButton_Click);
            // 
            // sbCancel
            // 
            this.sbCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sbCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.sbCancel.Appearance.Options.UseFont = true;
            this.sbCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.sbCancel.Location = new System.Drawing.Point(729, 7);
            this.sbCancel.Name = "sbCancel";
            this.sbCancel.Size = new System.Drawing.Size(86, 32);
            this.sbCancel.TabIndex = 4;
            this.sbCancel.Text = "Отмена";
            this.sbCancel.Click += new System.EventHandler(this.sbCancel_Click);
            // 
            // netsGridControl
            // 
            this.netsGridControl.ContextMenuStrip = this.contextMenuStrip1;
            this.netsGridControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.netsGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.netsGridControl.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.netsGridControl.Location = new System.Drawing.Point(0, 0);
            this.netsGridControl.MainView = this.netsGridView;
            this.netsGridControl.Name = "netsGridControl";
            this.netsGridControl.Size = new System.Drawing.Size(821, 435);
            this.netsGridControl.TabIndex = 5;
            this.netsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.netsGridView});
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiRedaction,
            this.toolStripSeparator1,
            this.tsmiLoad,
            this.tsmiRemove});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.ShowImageMargin = false;
            this.contextMenuStrip1.Size = new System.Drawing.Size(134, 76);
            this.contextMenuStrip1.Text = "Удалить расчет";
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // tsmiRedaction
            // 
            this.tsmiRedaction.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiRename,
            this.tsmiComment});
            this.tsmiRedaction.Name = "tsmiRedaction";
            this.tsmiRedaction.Size = new System.Drawing.Size(133, 22);
            this.tsmiRedaction.Text = "Редактировать";
            // 
            // tsmiRename
            // 
            this.tsmiRename.Name = "tsmiRename";
            this.tsmiRename.Size = new System.Drawing.Size(152, 22);
            this.tsmiRename.Text = "Наименование";
            this.tsmiRename.Click += new System.EventHandler(this.tsmiRename_Click);
            // 
            // tsmiComment
            // 
            this.tsmiComment.Name = "tsmiComment";
            this.tsmiComment.Size = new System.Drawing.Size(152, 22);
            this.tsmiComment.Text = "Комментарий";
            this.tsmiComment.Click += new System.EventHandler(this.tsmiComment_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(130, 6);
            // 
            // tsmiLoad
            // 
            this.tsmiLoad.Name = "tsmiLoad";
            this.tsmiLoad.Size = new System.Drawing.Size(133, 22);
            this.tsmiLoad.Text = "Загрузить схему";
            this.tsmiLoad.ToolTipText = "Удалить все расчеты";
            this.tsmiLoad.Click += new System.EventHandler(this.tsmiLoad_Click);
            // 
            // tsmiRemove
            // 
            this.tsmiRemove.Name = "tsmiRemove";
            this.tsmiRemove.Size = new System.Drawing.Size(133, 22);
            this.tsmiRemove.Text = "Удалить схему";
            this.tsmiRemove.ToolTipText = "Удалить активный расчет";
            this.tsmiRemove.Click += new System.EventHandler(this.tsmiRemove_Click);
            // 
            // netsGridView
            // 
            this.netsGridView.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.netsGridView.Appearance.HeaderPanel.Options.UseFont = true;
            this.netsGridView.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 10F);
            this.netsGridView.Appearance.Row.Options.UseFont = true;
            this.netsGridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.netsGridView.GridControl = this.netsGridControl;
            this.netsGridView.Name = "netsGridView";
            this.netsGridView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.netsGridView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.netsGridView.OptionsBehavior.Editable = false;
            this.netsGridView.OptionsBehavior.ReadOnly = true;
            this.netsGridView.OptionsCustomization.AllowGroup = false;
            this.netsGridView.OptionsCustomization.AllowQuickHideColumns = false;
            this.netsGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.netsGridView.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.netsGridView.OptionsView.ShowGroupPanel = false;
            this.netsGridView.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.netsGridView_RowClick);
            this.netsGridView.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.netsGridView_CustomDrawCell);
            this.netsGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.netsGridView_SelectionChanged);
            this.netsGridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.netsGridView_FocusedRowChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.sbCancel);
            this.panel1.Controls.Add(this.sbLoad);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Font = new System.Drawing.Font("Tahoma", 8F);
            this.panel1.Location = new System.Drawing.Point(0, 435);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(821, 44);
            this.panel1.TabIndex = 6;
            // 
            // LoadForm
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(821, 479);
            this.Controls.Add(this.netsGridControl);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 8F);
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "LoadForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Загрузить схему";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LoadForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.LoadForm_FormClosed);
            this.Load += new System.EventHandler(this.LoadForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.netsGridControl)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.netsGridView)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton sbLoad;
        private DevExpress.XtraEditors.SimpleButton sbCancel;
        private DevExpress.XtraGrid.GridControl netsGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView netsGridView;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmiRedaction;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem tsmiLoad;
        private System.Windows.Forms.ToolStripMenuItem tsmiRemove;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripMenuItem tsmiRename;
        private System.Windows.Forms.ToolStripMenuItem tsmiComment;
    }
}