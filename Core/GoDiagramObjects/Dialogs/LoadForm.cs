﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using GEx.Iface;

namespace GasExportApp.Dialogs
{
    /// <summary>
    /// Форма для загрузки сети
    /// </summary>
    public partial class LoadForm : XtraForm, IDialogForm
    {
        #region Fields

        /// <summary>
        /// Идентификатор загружаемой сети
        /// </summary>
        private int? _netId = null;

        /// <summary>
        /// Наименование загружаемой сети
        /// </summary>
        private String _netName; 

        private DataTable Nets = new DataTable();
        private int? _schemeID;

        /// <summary>
        /// интерфейс загрузчика
        /// </summary>
        private IGExDomain _loader;

        private IMessageFilter _filter;

        #endregion Fields

        #region .ctors

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="myForm">Главная форма</param>
        public LoadForm(IEnumerable<DTO_Schema> schemata)
        {
            InitializeComponent();
            InitNets(schemata);
        }

        public LoadForm(IGExDomain loader)
        {
            _loader = loader;
            InitializeComponent();
            InitNets(_loader.LoadSchemas());
        }
        public int? SchemeID
        {
            get { return _schemeID; }
            set { _schemeID = value; }
        }

        #endregion .ctors

        #region Methods

        /// <summary>
        /// Инициализировать список доступных сетей
        /// </summary>
        private void InitNets(IEnumerable<DTO_Schema> schemata)
        {
            this.netsGridView.BeginUpdate();
            this.netsGridView.BeginDataUpdate();            
            Nets = new DataTable();
            Nets.Columns.Clear();
            Nets.Columns.Add("NetID", typeof(Int32));
            Nets.Columns.Add("Name", typeof(String));
            Nets.Columns.Add("CreationDate", typeof(DateTime));
            Nets.Columns.Add("Author", typeof(string));
            Nets.Columns.Add("Comment", typeof(string));

            //загрузка

            foreach (var schema in schemata)
            {

                Net geNet = new Net
                {
                    Name = schema.SchemaName,
                    NetID = Convert.ToInt32(schema.Id),
                    CreationDate = Convert.ToDateTime(schema.CreationDate),
                    Author = schema.Author,
                    Comment = schema.Comment
                };

                Nets.Rows.Add(geNet.NetID, geNet.Name, geNet.CreationDate, geNet.Author, geNet.Comment);
            }
            this.netsGridControl.DataSource = Nets;

            this.netsGridView.Columns["CreationDate"].SortOrder = ColumnSortOrder.Descending;

            this.netsGridView.Columns["NetID"].Caption = "Id";
            this.netsGridView.Columns["NetID"].Visible = false;
            this.netsGridView.Columns["Name"].Caption = "Наименование";
            this.netsGridView.Columns["CreationDate"].Caption = "Дата последнего редактирования";
            this.netsGridView.Columns["Comment"].Caption = "Комментарий";
            DevExpress.XtraEditors.Repository.RepositoryItemTextEdit cEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            DevExpress.XtraEditors.Repository.RepositoryItemTextEdit comEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            cEdit.DisplayFormat.FormatString = "dd.MM.yy H:mm:ss";
            //cEdit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.netsGridView.Columns["CreationDate"].ColumnEdit = cEdit;
            this.netsGridView.Columns["Comment"].ColumnEdit = comEdit;
            this.netsGridView.Columns["Author"].Caption = "Автор";
            this.netsGridView.Columns["NetID"].OptionsColumn.AllowEdit = false;
            this.netsGridView.Columns["CreationDate"].OptionsColumn.AllowEdit = false;
            this.netsGridView.Columns["Author"].OptionsColumn.AllowEdit = false;
            this.netsGridView.Columns["Comment"].OptionsColumn.AllowEdit = false;
            this.netsGridView.Columns["Comment"].VisibleIndex = 1;
            this.netsGridView.BestFitColumns();
            this.netsGridView.EndDataUpdate();
            this.netsGridView.EndUpdate();
        }

        private IEnumerable<DTO_Schema> GetSchemas()
        {
            return _loader.LoadSchemas();
        }

        private void ldButton_Click(object sender, EventArgs e)
        {
            BeginLoadCurrentNet();
        }

        private void netsLstView_DoubleClick(object sender, EventArgs e)
        {
            BeginLoadCurrentNet();
        }

        private void netsLstView_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                BeginLoadCurrentNet();
            }
        }

        private void BeginLoadCurrentNet()
        {
            int[] selRowsHandlers = netsGridView.GetSelectedRows();
            if (selRowsHandlers.Length > 0)
            {
                DataRowView rowView = netsGridView.GetRow(selRowsHandlers[0]) as DataRowView;

                _netId = Convert.ToInt32(rowView.Row.ItemArray[0]);
                _netName = rowView.Row.ItemArray[1].ToString();
                _schemeID = _netId;
            }
            else 
            {
                _netId = _schemeID = null;
            }
            //this.Close();
        }

        private void LoadForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.RemoveMessageFilter(_filter);           
        }

        private void netsGridView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _netId = (int)netsGridView.GetFocusedRowCellValue(netsGridView.Columns["NetID"]);
        }

        #endregion Methods
        
        #region Nested Types

        /// <summary>
        /// Класс описания рег. данных о сети
        /// </summary>
        private class Net
        {
            internal Int32 NetID { get; set; }

            public String Name { get; set; }

            public DateTime CreationDate { get; set; }

            public string Author { get; set; }

            public string Comment { get; set; }
        }
              

        #endregion

        private void netsGridView_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            sbLoad.Enabled = netsGridView.SelectedRowsCount > 0;

            var focusedRowCellValue = netsGridView.GetFocusedRowCellValue(netsGridView.Columns["NetID"]);
            if(focusedRowCellValue!=null)
                _netId = (int)netsGridView.GetFocusedRowCellValue(netsGridView.Columns["NetID"]);
            netsGridView.Columns["Name"].OptionsColumn.AllowEdit = false;
            netsGridView.Columns["Comment"].OptionsColumn.AllowEdit = false;
            netsGridView.OptionsBehavior.Editable = false;
            netsGridView.OptionsBehavior.ReadOnly = true;
            netsGridView.OptionsSelection.EnableAppearanceFocusedCell = false;           
        }

        private void sbReload_Click(object sender, EventArgs e)
        {
            InitNets(GetSchemas());
        }

        private void sbCancel_Click(object sender, EventArgs e)
        {
            //BeginLoadCurrentNet();
            _schemeID = null;
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void netsGridView_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            if (e.Clicks == 2)
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
        }

        private void tsmiRemove_Click(object sender, EventArgs e)
        {
            if (_netId.HasValue &&
                MessageBox.Show(string.Format("Вы действительно хотите удалить схему '{0}'?", netsGridView.GetFocusedRowCellValue("Name").ToString()), "Удаление схемы", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                var _curs = Cursor;
                try
                {
                    Cursor = Cursors.WaitCursor;
                    _loader.RemoveSchema((int)_netId);
                    _netId = null;
                    netsGridView.DeleteSelectedRows();
                    if (netsGridView.GetFocusedRow() != null)
                        _netId = (int)netsGridView.GetFocusedRowCellValue(netsGridView.Columns["NetID"]);
                }
                finally
                {
                    Cursor = _curs;
                }
            }
        }

        private void tsmiLoad_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void tsmiRename_Click(object sender, EventArgs e)
        {
            if (netsGridView.GetFocusedRow() != null)
            {
                netsGridView.Columns["Name"].OptionsColumn.AllowEdit = true;
                netsGridView.OptionsBehavior.Editable = true;
                netsGridView.OptionsBehavior.ReadOnly = false;
                netsGridView.OptionsSelection.EnableAppearanceFocusedCell = true;
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit cEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                netsGridView.Columns["Name"].ColumnEdit = cEdit;
                netsGridView.Columns["Name"].OptionsColumn.AllowEdit = true;
                cEdit.Validating += cEdit_Validating;
                //cEdit.Modified += cEdit_EditValueChanged;
                cEdit.AllowFocused = true;
                netsGridView.FocusedColumn = netsGridView.Columns["Name"];

                netsGridView.SelectCell(netsGridView.FocusedRowHandle, netsGridView.Columns["Name"]);
                netsGridView.ShowEditor();
                if (netsGridView.ActiveEditor != null)
                    netsGridView.ActiveEditor.Focus();
            }
        }

        private void cEdit_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_netId.HasValue)
            {
                _loader.UpdateSchemaName((int)_netId, (sender as DevExpress.XtraEditors.TextEdit).Text);
                e.Cancel = false;
            }
        }
      

        private void netsGridView_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            e.Appearance.BackColor = e.Appearance.BackColor2 = e.RowHandle % 2 == 0 ? Color.White : Color.LightCyan;
        }

        private void LoadForm_Load(object sender, EventArgs e)
        {
            _filter = new MessageFilter(this);
            Application.AddMessageFilter(_filter);
        }

        private void LoadForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            BeginLoadCurrentNet();
        }

        public void ReactionForEnter()
        {
            if (netsGridView.ActiveEditor != null)
            { 
                netsGridView.CloseEditor();
                netsGridView.UpdateCurrentRow();
            }
            else
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
        }

        public void ReactionForEsc()
        {
            if (netsGridView.ActiveEditor != null)
            {
                netsGridView.HideEditor();
                netsGridView.UpdateCurrentRow();
            }
            else
            {
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                this.Close();
            }
        }

        public Control ReturnMainControl()
        {
            return this;
        }

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            bool isEnabled = false;
            if (netsGridView.GetFocusedRow() != null)
            {
                var focusedRowCellValue = netsGridView.GetFocusedRowCellValue(netsGridView.Columns["Author"]).ToString().ToUpperInvariant();
                isEnabled = focusedRowCellValue.Equals((Environment.UserDomainName + "\\" + Environment.UserName).ToUpperInvariant());
            }
            tsmiRedaction.Enabled = tsmiRemove.Enabled = isEnabled;
        }

        private void tsmiComment_Click(object sender, EventArgs e)
        {
            if (netsGridView.GetFocusedRow() != null)
            {
                netsGridView.Columns["Comment"].OptionsColumn.AllowEdit = true;
                netsGridView.OptionsBehavior.Editable = true;
                netsGridView.OptionsBehavior.ReadOnly = false;
                netsGridView.OptionsSelection.EnableAppearanceFocusedCell = true;
                DevExpress.XtraEditors.Repository.RepositoryItemTextEdit cEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
                netsGridView.Columns["Comment"].ColumnEdit = cEdit;
                netsGridView.Columns["Comment"].OptionsColumn.AllowEdit = true;
                cEdit.Validating += comEdit_Validating;
                //cEdit.Modified += cEdit_EditValueChanged;
                cEdit.AllowFocused = true;
                netsGridView.FocusedColumn = netsGridView.Columns["Comment"];

                netsGridView.SelectCell(netsGridView.FocusedRowHandle, netsGridView.Columns["Comment"]);
                netsGridView.ShowEditor();
                if (netsGridView.ActiveEditor != null)
                    netsGridView.ActiveEditor.Focus();
            }
        }

        private void comEdit_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_netId.HasValue)
            {
                _loader.UpdateSchemaComment((int)_netId, (sender as DevExpress.XtraEditors.TextEdit).Text);
                e.Cancel = false;
            }
        }
    }
}