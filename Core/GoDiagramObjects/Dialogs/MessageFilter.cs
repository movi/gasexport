using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using GEx.Iface;

namespace GasExportApp.Dialogs
{
    /// <summary>
    /// ����� ��� ������� �� Esc � Enter
    /// </summary>
    public class MessageFilter : IMessageFilter
    {
        public IDialogForm _form;

        public MessageFilter(IDialogForm form) { _form = form; }

        public bool PreFilterMessage(ref Message m)
        {
            //_form.Handle
            if (m.Msg == 0x0100)
            {
                if (IsMessageForUs(_form as Control, m.HWnd)) // Check, if the message was sent to _form or to one of its child 
                {
                    if (m.WParam.ToInt32() == 27) // ESC was pressed
                    {
                        _form.ReactionForEsc();                       
                        return true;
                    }
                    else if (m.WParam.ToInt32() == 13) // Enter was pressed
                    {
                        _form.ReactionForEnter();                        
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsMessageForUs(Control form, IntPtr intPtr)
        {
            Func<Control, IntPtr, Boolean> check = null;
            check = (control, handle) =>
            {
                if (control.Handle == handle) return true;
                foreach (Control c in control.Controls)
                {
                    if (check(c, intPtr)) return true;
                }
                return false;
            };

            return check(form, intPtr);
        }
    }
}