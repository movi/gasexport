﻿using System;
using System.Windows.Forms;
using CommonUIControls.ScenarioControls;
using DevExpress.XtraEditors;
using GEx.ErrorHandler;
using GEx.Iface;

namespace GasExportApp.Dialogs
{    
    /// <summary>
    /// Форма для загрузки сценария
    /// </summary>
    public partial class LoadFormScenario : XtraForm, IDialogForm
    {
        #region Fields

        /// <summary>
        /// Наименование загружаемого сценария
        /// </summary>
        private String _netName;

        private int? _scenarioID;

        /// <summary>
        /// интерфейс загрузчика
        /// </summary>
        private IGExDomain _loader;
        private IErrorOutput _errorHandler;
        private MainFormPresenter _presenter;

        private IMessageFilter _filter;

        #endregion Fields

        #region .ctors
        

        public LoadFormScenario()
        {            
            InitializeComponent();            
        }
        public int? ScenarioID
        {
            get { return _scenarioID; }
            set { _scenarioID = value; }
        }

        #endregion .ctors

        #region Methods

        /// <summary>
        /// Инициализировать список доступных сетей
        /// </summary>
        public void Init(MainFormPresenter presenter, IErrorOutput errorHandler)
        {
            _presenter = presenter;
            _errorHandler = errorHandler;
            InitScenariosList();
        }

        private void InitScenariosList()
        {            
            var pres = new ScenariosListPresenter(_presenter.CoreHolder, _errorHandler, ctrlScenarios);
            ctrlScenarios.Init(pres, OnChoiceScenario);
            ctrlScenarios.InitControlData();
        }

        private void OnChoiceScenario(int scenID)
        {
            DialogResult = DialogResult.OK;
            this.Close();
        }               
        
        private void ldButton_Click(object sender, EventArgs e)
        {
            //BeginLoadCurrentScenario();
        }
        
        private void BeginLoadCurrentScenario()
        {
            if (ctrlScenarios.SelectedScenario != null)
            {
                _scenarioID = ctrlScenarios.SelectedScenario;
            }
            //this.Close();
        }


        public void ReactionForEnter()
        {
            if (ctrlScenarios.gvScenarios.ActiveEditor != null)
            {
                ctrlScenarios.gvScenarios.CloseEditor();
                ctrlScenarios.gvScenarios.UpdateCurrentRow();
            }
            else
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
        }

        public void ReactionForEsc()
        {
            if (ctrlScenarios.gvScenarios.ActiveEditor != null)
            {
                ctrlScenarios.gvScenarios.HideEditor();
                ctrlScenarios.gvScenarios.UpdateCurrentRow();
            }
            else
            {
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                this.Close();
            }
        }

        public Control ReturnMainControl()
        {
            return this;
        }

        #endregion Methods

        private void LoadFormScenario_FormClosing(object sender, FormClosingEventArgs e)
        {
            BeginLoadCurrentScenario();
        }

        private void LoadFormScenario_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.RemoveMessageFilter(_filter); 
        }

        private void LoadFormScenario_Load(object sender, EventArgs e)
        {
            _filter = new MessageFilter(this);
            Application.AddMessageFilter(_filter);
        }        
    }
}