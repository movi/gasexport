﻿namespace GasExportApp.Dialogs
{
    partial class LoadFormScenario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sbLoad = new DevExpress.XtraEditors.SimpleButton();
            this.sbCancel = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.ctrlScenarios = new CommonUIControls.ScenarioControls.CtrlScenariosList();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // sbLoad
            // 
            this.sbLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sbLoad.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.sbLoad.Appearance.Options.UseFont = true;
            this.sbLoad.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.sbLoad.Location = new System.Drawing.Point(509, 6);
            this.sbLoad.Name = "sbLoad";
            this.sbLoad.Size = new System.Drawing.Size(86, 32);
            this.sbLoad.TabIndex = 3;
            this.sbLoad.Text = "Загрузить";
            this.sbLoad.Click += new System.EventHandler(this.ldButton_Click);
            // 
            // sbCancel
            // 
            this.sbCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sbCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.sbCancel.Appearance.Options.UseFont = true;
            this.sbCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.sbCancel.Location = new System.Drawing.Point(601, 6);
            this.sbCancel.Name = "sbCancel";
            this.sbCancel.Size = new System.Drawing.Size(86, 32);
            this.sbCancel.TabIndex = 4;
            this.sbCancel.Text = "Отмена";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.sbCancel);
            this.panelControl1.Controls.Add(this.sbLoad);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 435);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(692, 43);
            this.panelControl1.TabIndex = 6;
            // 
            // ctrlScenarios
            // 
            this.ctrlScenarios.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctrlScenarios.Location = new System.Drawing.Point(0, 0);
            this.ctrlScenarios.Name = "ctrlScenarios";
            this.ctrlScenarios.Size = new System.Drawing.Size(692, 435);
            this.ctrlScenarios.TabIndex = 5;
            // 
            // LoadFormScenario
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(692, 478);
            this.Controls.Add(this.ctrlScenarios);
            this.Controls.Add(this.panelControl1);
            this.Font = new System.Drawing.Font("Tahoma", 8F);
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "LoadFormScenario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Загрузка сценария";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LoadFormScenario_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.LoadFormScenario_FormClosed);
            this.Load += new System.EventHandler(this.LoadFormScenario_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton sbLoad;
        private DevExpress.XtraEditors.SimpleButton sbCancel;
        private CommonUIControls.ScenarioControls.CtrlScenariosList ctrlScenarios;
        private DevExpress.XtraEditors.PanelControl panelControl1;
    }
}