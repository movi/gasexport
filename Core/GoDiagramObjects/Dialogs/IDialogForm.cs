using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using GEx.Iface;

namespace GasExportApp.Dialogs
{
    public interface IDialogForm
    {
        void ReactionForEnter();
        void ReactionForEsc();
        Control ReturnMainControl();
    }
}