﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonUIControls.ScenarioControls;
using GEx.ErrorHandler;
using GEx.Iface;

namespace CommonUIControls
{
    public class NominationsViewItem
    {
        //Узел
        [DisplayName("Узел")]
        public string ItemName { get; internal set; }

        //Оператор
        [DisplayName("Оператор")]
        public string OperatorName { get; internal set; }

        //Направление
        internal DTO_Direction DtoDirection { get; set; }
        
        //Направление
        [DisplayName("Вх/Вых")]
        public string Direction { get { return DtoDirection.Description(); }}

        [DisplayName("Номинация, млн м³")]
        public double? NominationValue { get; set; }

        [Browsable(false)]
        public int IdNode { get; set; }

        [Browsable(false)]
        public int IdBalanceZone { get; set; }

        //Является ли узел точкой поступления газа
        [Browsable(false)]
        public bool IsSource { get; set; }
    }

    public class NominationsPresenter : IValuesPresenterDataAccess, IValuesPresenterResultsForControl, IGExDomainCallback
    {
        private IGExDomain _coreHolder;
        private IErrorOutput _errorHandler;
        private IScenarioControl _ctrl;
        private List<NominationsViewItem> _dataSource;

        public NominationsPresenter(IGExDomain coreHolder, IErrorOutput eh, IScenarioControl ctrl)
        {
            _coreHolder = coreHolder;
            _errorHandler = eh;
            _ctrl = ctrl;
        }

        private void PrepareDataForCtrl()
        {
            ////
            var result = new List<NominationsViewItem>();
            var list1 = _coreHolder.LoadNominations() ?? new List<DTO_Nominations>();
            List<DTO_Nominations> list = list1.ToList();

            foreach (var cScen in list)
            {
                var data = new NominationsViewItem
                {
                    ItemName = cScen.BorderName,
                    IdNode = cScen.Id_Border,
                    IdBalanceZone = cScen.Id_BalanceZone,
                    OperatorName = cScen.BalanceZoneName,
                    DtoDirection = cScen.Direction,
//                    isActive = cScen.IsActive,
                    NominationValue = cScen.NominationValue,
                    IsSource = cScen.IsSource
                };

                result.Add(data);
            }

            result.Sort((x, y) =>
            {
//                if (y.isActive.CompareTo(x.isActive) == 0)
//                {
                    if (x.ItemName.CompareTo(y.ItemName) == 0)
                    {
                        if (y.Direction.CompareTo(x.Direction) == 0)
                            return x.OperatorName.CompareTo(y.OperatorName);
                        else
                            return y.Direction.CompareTo(x.Direction);
                    }
                    else
                        return x.ItemName.CompareTo(y.ItemName);
//                }
//                else
//                    return y.isActive.CompareTo(x.isActive);

            });
            _dataSource = result;
        }

        public IList ShowDataForCtrl()
        {
            PrepareDataForCtrl();
            return _dataSource;
        }

        public void CallGui(string msg)
        {
            throw new NotImplementedException();
        }

        void IGExDomainCallback.BeforeNewSchemaSave(DTO_Schema sch)
        {
        }

        void IGExDomainCallback.OnScenarioLoaded()
        {
            _ctrl.UpdateCtrl();
        }

        void IGExDomainCallback.OnScenarioSaved()
        {
        }

        public void SetValue(object val)
        {
        }

        public void OnNodeAdded()
        {
            _ctrl.UpdateCtrl();
        }


        public void OnNodeUpdated(DTO_Node node)
        {
            _ctrl.UpdateCtrl();
        }


        public void OnRemoveNode(int nodeId)
        {
            _ctrl.UpdateCtrl();
        }


        public void OnSchemaSaved(SavedEntityInfo schemaInfo, IEnumerable<SavedEntityInfo> nodesInfo)
        {
            _ctrl.UpdateCtrl();
            // throw new NotImplementedException();
        }


        public void OnCalculated()
        {
            _ctrl.UpdateCtrl();
        }

        public void OnRemoveNodeAsSelectedForPath(DTO_Node node)
        {
            throw new NotImplementedException();
        }


        public void OnSchemaInit()
        {
            _ctrl.UpdateCtrl();
        }


        public bool IsCalculated()
        {
            return _coreHolder.IsCalculated();
        }
    }
}
