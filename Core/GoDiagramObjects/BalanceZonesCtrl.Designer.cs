﻿namespace CommonUIControls
{
    partial class BalanceZonesCtrl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BalanceZonesCtrl));
            this.gcBalZones = new DevExpress.XtraGrid.GridControl();
            this.gvBalZones = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.tcBalZones = new DevExpress.XtraTab.XtraTabControl();
            this.tpOperators = new DevExpress.XtraTab.XtraTabPage();
            this.gcOperators = new DevExpress.XtraGrid.GridControl();
            this.gvOperators = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.sbAddOperator = new DevExpress.XtraEditors.SimpleButton();
            this.ImageCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.sbDeleteOperator = new DevExpress.XtraEditors.SimpleButton();
            this.tpRelations = new DevExpress.XtraTab.XtraTabPage();
            ((System.ComponentModel.ISupportInitialize)(this.gcBalZones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBalZones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcBalZones)).BeginInit();
            this.tcBalZones.SuspendLayout();
            this.tpOperators.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcOperators)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOperators)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ImageCollection)).BeginInit();
            this.tpRelations.SuspendLayout();
            this.SuspendLayout();
            // 
            // gcBalZones
            // 
            this.gcBalZones.Cursor = System.Windows.Forms.Cursors.Default;
            this.gcBalZones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcBalZones.Location = new System.Drawing.Point(0, 0);
            this.gcBalZones.MainView = this.gvBalZones;
            this.gcBalZones.Name = "gcBalZones";
            this.gcBalZones.Size = new System.Drawing.Size(457, 356);
            this.gcBalZones.TabIndex = 0;
            this.gcBalZones.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvBalZones});
            // 
            // gvBalZones
            // 
            this.gvBalZones.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gvBalZones.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvBalZones.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gvBalZones.Appearance.Row.Options.UseFont = true;
            this.gvBalZones.Appearance.ViewCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gvBalZones.Appearance.ViewCaption.Options.UseFont = true;
            this.gvBalZones.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.gvBalZones.GridControl = this.gcBalZones;
            this.gvBalZones.Name = "gvBalZones";
            this.gvBalZones.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvBalZones.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvBalZones.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.gvBalZones.OptionsCustomization.AllowGroup = false;
            this.gvBalZones.OptionsCustomization.AllowQuickHideColumns = false;
            this.gvBalZones.OptionsView.ShowGroupPanel = false;
            this.gvBalZones.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gvBalZones.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView1_ValidatingEditor);
            // 
            // tcBalZones
            // 
            this.tcBalZones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcBalZones.Location = new System.Drawing.Point(0, 0);
            this.tcBalZones.Name = "tcBalZones";
            this.tcBalZones.SelectedTabPage = this.tpOperators;
            this.tcBalZones.Size = new System.Drawing.Size(463, 384);
            this.tcBalZones.TabIndex = 0;
            this.tcBalZones.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tpOperators,
            this.tpRelations});
            // 
            // tpOperators
            // 
            this.tpOperators.Controls.Add(this.gcOperators);
            this.tpOperators.Controls.Add(this.flowLayoutPanel1);
            this.tpOperators.Name = "tpOperators";
            this.tpOperators.Size = new System.Drawing.Size(457, 356);
            this.tpOperators.Text = "Операторы";
            // 
            // gcOperators
            // 
            this.gcOperators.Cursor = System.Windows.Forms.Cursors.Default;
            this.gcOperators.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcOperators.Location = new System.Drawing.Point(0, 36);
            this.gcOperators.MainView = this.gvOperators;
            this.gcOperators.Name = "gcOperators";
            this.gcOperators.Size = new System.Drawing.Size(457, 320);
            this.gcOperators.TabIndex = 1;
            this.gcOperators.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvOperators});
            // 
            // gvOperators
            // 
            this.gvOperators.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gvOperators.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvOperators.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gvOperators.Appearance.Row.Options.UseFont = true;
            this.gvOperators.Appearance.ViewCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gvOperators.Appearance.ViewCaption.Options.UseFont = true;
            this.gvOperators.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.gvOperators.GridControl = this.gcOperators;
            this.gvOperators.Name = "gvOperators";
            this.gvOperators.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
            this.gvOperators.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.True;
            this.gvOperators.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.gvOperators.OptionsCustomization.AllowGroup = false;
            this.gvOperators.OptionsCustomization.AllowQuickHideColumns = false;
            this.gvOperators.OptionsNavigation.AutoFocusNewRow = true;
            this.gvOperators.OptionsView.ShowGroupPanel = false;
            this.gvOperators.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gvOperators_ValidatingEditor);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.sbAddOperator);
            this.flowLayoutPanel1.Controls.Add(this.sbDeleteOperator);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(457, 36);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // sbAddOperator
            // 
            this.sbAddOperator.ImageIndex = 22;
            this.sbAddOperator.ImageList = this.ImageCollection;
            this.sbAddOperator.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbAddOperator.Location = new System.Drawing.Point(3, 3);
            this.sbAddOperator.Name = "sbAddOperator";
            this.sbAddOperator.Size = new System.Drawing.Size(30, 30);
            this.sbAddOperator.TabIndex = 0;
            this.sbAddOperator.ToolTip = "Добавить нового Оператора";
            this.sbAddOperator.Click += new System.EventHandler(this.sbAddOperator_Click);
            // 
            // ImageCollection
            // 
            this.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ImageCollection.ImageStream")));
            this.ImageCollection.Images.SetKeyName(0, "Ribbon_New_16x16.png");
            this.ImageCollection.Images.SetKeyName(1, "Ribbon_Open_16x16.png");
            this.ImageCollection.Images.SetKeyName(2, "Ribbon_Close_16x16.png");
            this.ImageCollection.Images.SetKeyName(3, "Ribbon_Find_16x16.png");
            this.ImageCollection.Images.SetKeyName(4, "Ribbon_Save_16x16.png");
            this.ImageCollection.Images.SetKeyName(5, "Ribbon_SaveAs_16x16.png");
            this.ImageCollection.Images.SetKeyName(6, "Ribbon_Exit_16x16.png");
            this.ImageCollection.Images.SetKeyName(7, "Ribbon_Content_16x16.png");
            this.ImageCollection.Images.SetKeyName(8, "Ribbon_Info_16x16.png");
            this.ImageCollection.Images.SetKeyName(9, "Ribbon_Bold_16x16.png");
            this.ImageCollection.Images.SetKeyName(10, "Ribbon_Italic_16x16.png");
            this.ImageCollection.Images.SetKeyName(11, "Ribbon_Underline_16x16.png");
            this.ImageCollection.Images.SetKeyName(12, "Ribbon_AlignLeft_16x16.png");
            this.ImageCollection.Images.SetKeyName(13, "Ribbon_AlignCenter_16x16.png");
            this.ImageCollection.Images.SetKeyName(14, "Ribbon_AlignRight_16x16.png");
            this.ImageCollection.Images.SetKeyName(15, "Cursor_16x16.png");
            this.ImageCollection.Images.SetKeyName(16, "green.png");
            this.ImageCollection.Images.SetKeyName(17, "orange.png");
            this.ImageCollection.Images.SetKeyName(18, "red.png");
            this.ImageCollection.Images.SetKeyName(19, "violet.png");
            this.ImageCollection.Images.SetKeyName(20, "Play_16x16.png");
            this.ImageCollection.Images.SetKeyName(21, "Refresh_16x16.png");
            this.ImageCollection.Images.SetKeyName(22, "Add_16x16.png");
            // 
            // sbDeleteOperator
            // 
            this.sbDeleteOperator.ImageIndex = 6;
            this.sbDeleteOperator.ImageList = this.ImageCollection;
            this.sbDeleteOperator.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.sbDeleteOperator.Location = new System.Drawing.Point(39, 3);
            this.sbDeleteOperator.Name = "sbDeleteOperator";
            this.sbDeleteOperator.Size = new System.Drawing.Size(30, 30);
            this.sbDeleteOperator.TabIndex = 0;
            this.sbDeleteOperator.ToolTip = "Удатить выбранного Оператора";
            this.sbDeleteOperator.Click += new System.EventHandler(this.sbDeleteOperator_Click);
            // 
            // tpRelations
            // 
            this.tpRelations.Controls.Add(this.gcBalZones);
            this.tpRelations.Name = "tpRelations";
            this.tpRelations.Size = new System.Drawing.Size(457, 356);
            this.tpRelations.Text = "Привязка к операторам";
            // 
            // BalanceZonesCtrl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.tcBalZones);
            this.Name = "BalanceZonesCtrl";
            this.Size = new System.Drawing.Size(463, 384);
            ((System.ComponentModel.ISupportInitialize)(this.gcBalZones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvBalZones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcBalZones)).EndInit();
            this.tcBalZones.ResumeLayout(false);
            this.tpOperators.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcOperators)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvOperators)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ImageCollection)).EndInit();
            this.tpRelations.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraGrid.GridControl gcBalZones;
        public DevExpress.XtraGrid.Views.Grid.GridView gvBalZones;
        private DevExpress.XtraTab.XtraTabControl tcBalZones;
        private DevExpress.XtraTab.XtraTabPage tpRelations;
        private DevExpress.XtraTab.XtraTabPage tpOperators;
        public DevExpress.XtraGrid.GridControl gcOperators;
        public DevExpress.XtraGrid.Views.Grid.GridView gvOperators;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton sbAddOperator;
        private DevExpress.XtraEditors.SimpleButton sbDeleteOperator;
        private DevExpress.Utils.ImageCollection ImageCollection;

    }
}
