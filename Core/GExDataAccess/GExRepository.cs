﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GEx.Domain;
using GEx.Iface;
using GEx.ErrorHandler;
using System.Data.Entity;

namespace GEx.DataAccess
{
    public class GExRepository : IGExRepository
    {
        private IErrorOutput _errorHandler;

        public GExRepository()
        {
        }
        
        public void Init(IErrorOutput eh)
        {
            _errorHandler = eh;
        }

        public Schema Load(int schemaId)
        {
            Schema schema;
            using (var cntxt = new GExDbContext())
            {
                schema = cntxt.Schemas.
                    Include(s => s.Nodes.Select(n => n.RelatedNodes)).
                    Include(s => s.Nodes.Select(n => n.GasExportObject)).
                    FirstOrDefault(s => s.ID == schemaId);
            }
            return schema;
        }

        public void Create(Schema schema)
        {
            using (var cntxt = new GExDbContext())
            {
                cntxt.Schemas.Add(schema);   
                cntxt.SaveChanges();
            }
        }

        public IEnumerable<Schema> LoadAll()
        {
            IEnumerable<Schema> allSchemas = null;
            using (var cntxt = new GExDbContext())
            {
               allSchemas = cntxt.Schemas.ToArray();
            }
            return allSchemas;
        }

        public void Update(Schema schema)
        {
            using (var cntxt = new GExDbContext())
            {
                using (var transcat = cntxt.Database.BeginTransaction())
                {
                    try
                    {
                        List<int> nodeIdForDelete = new List<int>();

                        var originalSchema = cntxt.Schemas.
                            Include(s => s.Nodes.Select(n => n.RelatedNodes)).
                            Include(s => s.Nodes.Select(n => n.GasExportObject)).
                            FirstOrDefault(s => s.ID == schema.ID);

                        cntxt.Entry(originalSchema).CurrentValues.SetValues(schema);

                        foreach (var originalNode in originalSchema.Nodes)
                        {
                            var currNode = schema.Nodes.FirstOrDefault(x => x.ID == originalNode.ID);
                            if (currNode == null)
                            {
                                nodeIdForDelete.Add(originalNode.ID);
                                continue;
                            }

                            List<int> nodeIdForDeleteFromBinding = new List<int>();
                            foreach (var originalBindedNode in originalNode.RelatedNodes)
                            {
                                var nodes = currNode.RelatedNodes.Where(n => n.ID == originalBindedNode.ID).ToArray();
                                if (!nodes.Any())
                                    nodeIdForDeleteFromBinding.Add(originalBindedNode.ID);
                            }
                            foreach (int nodeId in nodeIdForDeleteFromBinding)
                            {
                                var bindedNodeForDelete = originalNode.RelatedNodes.FirstOrDefault(n => n.ID == nodeId);
                                originalNode.RelatedNodes.Remove(bindedNodeForDelete);
                            }
                        }
                        cntxt.SaveChanges();


                        foreach (int nodeId in nodeIdForDelete)
                        {
                            var nodeForDelete = originalSchema.Nodes.FirstOrDefault(n => n.ID == nodeId);
                            originalSchema.Nodes.Remove(nodeForDelete);
                        }
                        cntxt.SaveChanges();
                        transcat.Commit();
                    }
                    catch(Exception ex)
                    {
                        transcat.Rollback();
                        throw ex;
                    }


                }
            }
        }
    }
}
