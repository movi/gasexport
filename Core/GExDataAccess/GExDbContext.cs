﻿using GEx.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace GEx.DataAccess
{
    public class GExDbContext : DbContext
    {
        private const string ConnSection = "GExDbContext";

        public GExDbContext()
            : base(ConnSection) 

        {
           
        }
        static GExDbContext()
        {
            Database.SetInitializer(new GExDbContextInitializer());
            var hack = typeof(System.Data.Entity.SqlServer.SqlProviderServices);
        }

        public GExDbContext(string nameOrConnnectionString)
            : base(nameOrConnnectionString)
        {
        }

        public DbSet<Schema> Schemas { get; set; }

        public DbSet<Node> Nodes { get; set; }

        public DbSet<GasExportObject> GasExportObjects { get; set; }

        public DbSet<BalanceZone> BalanceZones { get; set; }

        public DbSet<CalculationScenario> Scenarios { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
          //  modelBuilder.Entity<BalanceZone>().HasKey<int>(zone => zone.ID);
            modelBuilder.Entity<Schema>().ToTable("Schemas");

            modelBuilder.Entity<Schema>().HasKey<int>(schema => schema.ID);
         //   modelBuilder.Entity<Node>().ToTable("Nodes");
            modelBuilder.Entity<Node>().HasRequired(s => s.Schema);
          //  modelBuilder.Entity<Node>().HasRequired(s => s.GasExportObject);
          //  modelBuilder.Entity<Node>().HasKey<int>(n => n.ID);

            modelBuilder.Entity<Schema>().HasOptional(s => s.SchemaImage).WithRequired().WillCascadeOnDelete(true);

            modelBuilder.Entity<GasExportObject>().ToTable("GazExportObjects");
            modelBuilder.Entity<GasExportObject>().Ignore(o => o.Type);
            modelBuilder.Entity<GasExportObject>().HasKey<int>(o => o.ID);
            modelBuilder.Entity<Output>().ToTable("Outputs");
            modelBuilder.Entity<RussianGasInput>().ToTable("RussianGasInputs");
            modelBuilder.Entity<UGS>().ToTable("UGSes");
            modelBuilder.Entity<Border>().ToTable("Borders");
            modelBuilder.Entity<VTP>().ToTable("VTPs");
            

          //  modelBuilder.Entity<BorderInterconnection>();
          //  modelBuilder.Entity<BorderInterconnection>().HasKey(b => new { b.Id, b.BorderId }).Property(o => o.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<BorderInterconnection>().HasRequired(r => r.Border).WithMany(b => b.Interconnections).HasForeignKey(i => i.BorderId).WillCascadeOnDelete(true);


           // modelBuilder.Entity<NodesRelation>().HasKey(r => new { r.ID, r.SchemaID });
           // modelBuilder.Entity<NodesRelation>().Property(o => o.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<NodesRelation>().HasRequired(r => r.Schema).WithMany(s => s.Relations).HasForeignKey(o => o.SchemaID);
            modelBuilder.Entity<NodesRelation>().HasRequired(x => x.BalanceZone).WithMany();
            modelBuilder.Entity<NodesRelation>().HasRequired(r => r.NodeFirst).WithMany().WillCascadeOnDelete(false);
            modelBuilder.Entity<NodesRelation>().HasRequired(r => r.NodeSecond).WithMany().WillCascadeOnDelete(false);
            //modelBuilder.Entity<NodesRelation>().HasKey(r => new { r.ID, r.SchemaID }).Property(o => o.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);


         //   modelBuilder.Entity<OutputScenarioData>().HasKey(o => new { o.Id, o.ScenarioId });
         //   modelBuilder.Entity<OutputScenarioData>().Property(o => o.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<OutputScenarioData>().HasRequired(o => o.Scenario).WithMany(s => s.OutputScenarioDataSet).HasForeignKey(o => o.ScenarioId).WillCascadeOnDelete(true);
            modelBuilder.Entity<OutputScenarioData>().HasRequired(o => o.Obj).WithMany().WillCascadeOnDelete(true);//.WithMany();

            

         //   modelBuilder.Entity<RussianGasInputScenarioData>().HasKey(o => new { o.Id, o.ScenarioId });
         //   modelBuilder.Entity<RussianGasInputScenarioData>().Property(o => o.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<RussianGasInputScenarioData>().HasRequired(o => o.Scenario).WithMany(s => s.RussianGasInputScenarioDataSet).HasForeignKey(o => o.ScenarioId).WillCascadeOnDelete(true);
            modelBuilder.Entity<RussianGasInputScenarioData>().HasRequired(o => o.Obj).WithMany().WillCascadeOnDelete(true); 

         //   modelBuilder.Entity<UGSScenarioData>().HasKey(o => new { o.Id, o.ScenarioId });
         //   modelBuilder.Entity<UGSScenarioData>().Property(o => o.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<UGSScenarioData>().HasRequired(o => o.Scenario).WithMany(s=>s.UGSScenarioDataSet).HasForeignKey(o=>o.ScenarioId).WillCascadeOnDelete(true);
            modelBuilder.Entity<UGSScenarioData>().HasRequired(o => o.Obj).WithMany().WillCascadeOnDelete(true); 


         //   modelBuilder.Entity<BorderScenarioData>().HasKey(o => new { o.Id, o.ScenarioId });
         //   modelBuilder.Entity<BorderScenarioData>().Property(o => o.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<BorderScenarioData>().HasRequired(o => o.Scenario).WithMany(s => s.BorderScenarioDataSet).HasForeignKey(o => o.ScenarioId);
            modelBuilder.Entity<BorderScenarioData>().HasRequired(o => o.GasExportObject).WithMany().WillCascadeOnDelete(true);
            modelBuilder.Entity<BorderScenarioData>().HasRequired(o => o.Operator).WithMany().WillCascadeOnDelete(true);
        }
    }
}
