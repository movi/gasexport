﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GEx.Domain;
using GEx.ErrorHandler;
using GEx.Iface;
using System.Data.Entity;

namespace GEx.DataAccess
{
    public class GazExportObjectRepository : IGasExportObjectRepository
    {
        IErrorOutput _errorHandler;

        public void Init(IErrorOutput eh)
        {
            _errorHandler = eh;
        }

        public void Update(IEnumerable<GasExportObject> objects)
        {
            using (var cntxt = new GExDbContext())
            {
                var dictTempIdNewIdForInterconnections = new List<Tuple<BorderInterconnection, BorderInterconnection>>();

                foreach (var geo in objects)
                {
                    if (geo.ID < 0)
                        continue;
                    if (geo is Border)
                    {
                        var border = geo as Border;
                        var tmpBorder = new Border() { ID = border.ID };
                        cntxt.Set<Border>().Attach(tmpBorder);

                        var currentConnectionIds = border.Interconnections.Select(i => i.Id).ToArray();
                        var conectionsForRemove = cntxt.Set<BorderInterconnection>().Where(i => i.BorderId == tmpBorder.ID && !currentConnectionIds.Contains(i.Id)).ToArray();
                        var idsOfcoonectionsForRemove = conectionsForRemove.Select(c => c.Id).ToArray();
                        foreach (var id in idsOfcoonectionsForRemove)
                        {
                            var conn = cntxt.Set<BorderInterconnection>().Find(id);
                            cntxt.Set<BorderInterconnection>().Remove(conn);
                        }

                        foreach (var conn in border.Interconnections)
                        {
                            if (conn.Id >= 0) continue;
                            if (conn.ZoneFrom != null && cntxt.Set<BalanceZone>().Local.All(z => z.ID != conn.ZoneFrom.ID))
                                cntxt.Set<BalanceZone>().Attach(new BalanceZone { ID = conn.ZoneFrom.ID });


                            if (conn.ZoneTo != null && cntxt.Set<BalanceZone>().Local.All(z => z.ID != conn.ZoneTo.ID))
                                cntxt.Set<BalanceZone>().Attach(new BalanceZone { ID = conn.ZoneTo.ID });

                            var tmpBorderInterconnection = new BorderInterconnection
                            {
                                Id = conn.Id,
                                Border = tmpBorder,
                                ZoneTo = cntxt.Set<BalanceZone>().First(z => z.ID == conn.ZoneTo.ID),
                                ZoneFrom = cntxt.Set<BalanceZone>().First(z => z.ID == conn.ZoneFrom.ID),
                                BorderId = conn.BorderId
                            };

                            tmpBorder.Interconnections.Add(tmpBorderInterconnection);
                            dictTempIdNewIdForInterconnections.Add(new Tuple<BorderInterconnection, BorderInterconnection>(tmpBorderInterconnection, conn));
                        }
                    }


                    if (geo is IOneZoneNode)
                    {
                        var oneZoneGeo = geo as IOneZoneNode;
                        if (oneZoneGeo.Zone == null)
                            continue;

                        if (cntxt.Set<BalanceZone>().Local.All(z => z.ID != oneZoneGeo.Zone.ID))
                        {
                            var tmpZone = new BalanceZone() { ID = oneZoneGeo.Zone.ID };
                            cntxt.Set<BalanceZone>().Attach(tmpZone);
                        }

                        if (geo is UGS)
                        {

                            var ugs = new UGS { ID = geo.ID, Name = geo.Name, Zone = null };
                            cntxt.Set<UGS>().Attach(ugs);
                            cntxt.Entry(ugs).Reference(p => p.Zone).Load();
                            ugs.Zone = cntxt.Set<BalanceZone>().Find(oneZoneGeo.Zone.ID);
                        }

                        if (geo is RussianGasInput)
                        {
                            var input = new RussianGasInput { ID = geo.ID, Name = geo.Name, Zone = null };
                            cntxt.Set<RussianGasInput>().Attach(input);
                            cntxt.Entry(input).Reference(p => p.Zone).Load();
                            input.Zone = cntxt.Set<BalanceZone>().Find(oneZoneGeo.Zone.ID);
                        }
                    }
                }

                cntxt.SaveChanges();

                foreach (var pair in dictTempIdNewIdForInterconnections)
                {
                    pair.Item2.Id = pair.Item1.Id;
                }

            }
        }
    }
}
