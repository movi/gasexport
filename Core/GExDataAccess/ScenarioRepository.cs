﻿using GEx.Domain;
using GEx.ErrorHandler;
using GEx.Iface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Core.Mapping;

namespace GEx.DataAccess
{
    public class ScenarioRepository : IScenarioRepository
    {
        IErrorOutput _errorHandler;

        public void Init(IErrorOutput eh)
        {
            _errorHandler = eh;
        }

        private CalculationScenario GetScenario(GExDbContext dbContext,int id)
        {
            var scenario =  dbContext.Scenarios.Include(sc => sc.RussianGasInputScenarioDataSet).
                    Include(sc => sc.RussianGasInputScenarioDataSet.Select(mev => mev.Obj)).
                    Include(sc => sc.UGSScenarioDataSet.Select(ugs => ugs.Obj)).
                    Include(sc => sc.OutputScenarioDataSet.Select(cv => cv.Obj)).
                   // Include(sc => sc.PathPointScenarioDataSet.Select(cd => cd.PathPoint)).
                    Include(sc => sc.BorderScenarioDataSet.Select(cd => cd.Operator)).
                    Include(sc => sc.BorderScenarioDataSet.Select(cd => cd.GasExportObject)).
                    FirstOrDefault(s => s.ID == id);
            return FiletrScenario(scenario);
        }

        private static CalculationScenario FiletrScenario(CalculationScenario scenario)
        {
            var forRemove = scenario.OutputScenarioDataSet.GroupBy(o => new {o.Obj.ID})
                .Where(g => g.Count() > 1)
                .SelectMany(g => g.Skip(1))
                .ToArray();
            foreach (var every in forRemove)
            {
               scenario.OutputScenarioDataSet.Remove(every);
            }

            return scenario;
        }

        public IEnumerable<CalculationScenario> LoadAll()
        {
            IEnumerable<CalculationScenario> allScenarios;
            using (var cntxt = new GExDbContext())
            {
                allScenarios = cntxt.Scenarios.AsNoTracking().ToArray();
            }
            return allScenarios;
        }

        public void Create(CalculationScenario scenario)
        {
            var dictTempIdNewIdForOuputScen = new List<Tuple<OutputScenarioData, OutputScenarioData>>();
            var dictTempIdNewIdForRusInputScen = new List<Tuple<RussianGasInputScenarioData, RussianGasInputScenarioData>>();
            var dictTempIdNewIdForUgsScen = new List<Tuple<UGSScenarioData, UGSScenarioData>>();
            var dictTempIdNewIdForBorderScen = new List<Tuple<BorderScenarioData, BorderScenarioData>>();

            using (var cntxt = new GExDbContext())
            {
                var scenTmp = new CalculationScenario
                {
                    ID = scenario.ID,
                    Name = scenario.Name,
                    Author = scenario.Author,
                    Comment = scenario.Comment,
                    CreationDate = scenario.CreationDate
                };

                foreach (var outputScen in scenario.OutputScenarioDataSet)
                {
                    var outputScenTmp = new OutputScenarioData
                    {
                        Id = outputScen.Id,
                        Scenario = scenTmp,
                        ScenarioId = scenTmp.ID,
                        ContractVolume = outputScen.ContractVolume
                    };
                    var tmpBorder = new Border { ID = outputScen.Obj.ID }; 
                    if (cntxt.Set<Border>().Local.All(b => b.ID != tmpBorder.ID))
                        cntxt.Set<Border>().Attach(tmpBorder);

                    outputScenTmp.Obj = cntxt.Set<Border>().Local.First(b => b.ID == outputScen.Obj.ID);
                    scenTmp.OutputScenarioDataSet.Add(outputScenTmp);
                    dictTempIdNewIdForOuputScen.Add(new Tuple<OutputScenarioData, OutputScenarioData>(outputScenTmp, outputScen));
                }

                foreach (var russScen in scenario.RussianGasInputScenarioDataSet)
                {
                    var russScenTmp = new RussianGasInputScenarioData()
                    {
                        Id = russScen.Id,
                        Scenario = scenTmp,
                        ScenarioId = scenTmp.ID,
                        MaxExportVolume = russScen.MaxExportVolume
                    };

                    var tmpInput = new RussianGasInput() { ID = russScen.Obj.ID };
                    if (cntxt.Set<RussianGasInput>().Local.All(b => b.ID != tmpInput.ID))
                        cntxt.Set<RussianGasInput>().Attach(tmpInput);
                    russScenTmp.Obj = cntxt.Set<RussianGasInput>().Local.First(r => r.ID == tmpInput.ID);

                    scenTmp.RussianGasInputScenarioDataSet.Add(russScenTmp);
                    dictTempIdNewIdForRusInputScen.Add(new Tuple<RussianGasInputScenarioData, RussianGasInputScenarioData>(russScenTmp, russScen));
                }

                foreach (var ugsScen in scenario.UGSScenarioDataSet)
                {
                    var ugsScenTmp = new UGSScenarioData()
                    {
                        Id = ugsScen.Id,
                        Scenario = scenTmp,
                        ScenarioId = scenTmp.ID,
                        MaxUGSWithdrawal = ugsScen.MaxUGSWithdrawal
                    };

                    var tmpUGS = new UGS() { ID = ugsScen.Obj.ID };
                    if (cntxt.Set<UGS>().Local.All(b => b.ID != tmpUGS.ID))
                        cntxt.Set<UGS>().Attach(tmpUGS);
                    ugsScenTmp.Obj = cntxt.Set<UGS>().Local.First(r => r.ID == tmpUGS.ID);


                    scenTmp.UGSScenarioDataSet.Add(ugsScenTmp);
                    dictTempIdNewIdForUgsScen.Add(new Tuple<UGSScenarioData, UGSScenarioData>(ugsScenTmp, ugsScen));
                }

                foreach (var borderScen in scenario.BorderScenarioDataSet)
                {
                    var borderScenTmp = new BorderScenarioData()
                    {
                        Id = borderScen.Id,
                        Scenario = scenTmp,
                        ScenarioId = scenTmp.ID,
                        AvailableCapacity = borderScen.AvailableCapacity,
                        BookedCapacity = borderScen.BookedCapacity,
                        Tariff = borderScen.Tariff,
                        GsnValue = borderScen.GsnValue,
                        GsnPercentValue = borderScen.GsnPercentValue,
                        Description = borderScen.Description,
                        Direction = borderScen.Direction
                    };
                    GasExportObject tmpGeo = null;

                    if (borderScen.GasExportObject is Border)
                        tmpGeo = new Border() { ID = borderScen.GasExportObject.ID };
                    if (borderScen.GasExportObject is UGS)
                        tmpGeo = new UGS() { ID = borderScen.GasExportObject.ID };
                    if (borderScen.GasExportObject is RussianGasInput)
                        tmpGeo = new RussianGasInput() { ID = borderScen.GasExportObject.ID };

                    if (!cntxt.Set<GasExportObject>().Local.Any(b => b.ID == tmpGeo.ID))
                        cntxt.Set<GasExportObject>().Attach(tmpGeo);

                    BalanceZone tmpZone;
                    tmpZone = new BalanceZone() { ID = borderScen.Operator.ID };

                    if (cntxt.Set<BalanceZone>().Local.All(o => o.ID != tmpZone.ID))
                        cntxt.Set<BalanceZone>().Attach(tmpZone);
                    
                    borderScenTmp.GasExportObject = cntxt.Set<GasExportObject>().Local.First(r => r.ID == tmpGeo.ID);
                    borderScenTmp.Operator = cntxt.Set<BalanceZone>().Local.First(r => r.ID == borderScen.Operator.ID);

                    scenTmp.BorderScenarioDataSet.Add(borderScenTmp);
                    dictTempIdNewIdForBorderScen.Add(new Tuple<BorderScenarioData, BorderScenarioData>(borderScenTmp, borderScen));
                }

                cntxt.Scenarios.Add(scenTmp);
                cntxt.SaveChanges();

                scenario.ID = scenTmp.ID;
                foreach (var pair in dictTempIdNewIdForBorderScen)
                {
                    pair.Item2.ScenarioId = pair.Item2.Scenario.ID;
                    pair.Item2.Id = pair.Item1.Id;
                }
                foreach (var pair in dictTempIdNewIdForOuputScen)
                {
                    pair.Item2.ScenarioId = pair.Item2.Scenario.ID;
                    pair.Item2.Id = pair.Item1.Id;
                }
                foreach (var pair in dictTempIdNewIdForRusInputScen)
                {
                    pair.Item2.ScenarioId = pair.Item2.Scenario.ID;
                    pair.Item2.Id = pair.Item1.Id;
                }
                foreach (var pair in dictTempIdNewIdForUgsScen)
                {
                    pair.Item2.ScenarioId = pair.Item2.Scenario.ID;
                    pair.Item2.Id = pair.Item1.Id;
                }
            }
        }

        public CalculationScenario Load(int scenarioId)
        {
            CalculationScenario scenario;
            using (var cntxt = new GExDbContext())
            {
                scenario = GetScenario(cntxt,scenarioId);
            }
            return scenario;
        }

        public void Update(CalculationScenario scenario)
        {
            if (scenario == null)
                return;

            using (var cntxt = new GExDbContext())
            {
                var dictTempIdNewIdForOuputScen = new List<Tuple<OutputScenarioData, OutputScenarioData>>();
                var dictTempIdNewIdForRusInputScen = new List<Tuple<RussianGasInputScenarioData, RussianGasInputScenarioData>>();
                var dictTempIdNewIdForUgsScen = new List<Tuple<UGSScenarioData, UGSScenarioData>>();
                var dictTempIdNewIdForBorderScen = new List<Tuple<BorderScenarioData, BorderScenarioData>>();

                CalculationScenario scenTmp = new CalculationScenario() { 
                    ID = scenario.ID ,
                    Name=scenario.Name,
                    Author=scenario.Author,
                    Comment = scenario.Comment,
                    CreationDate=scenario.CreationDate};
           
                cntxt.Entry(scenTmp).State = EntityState.Modified;

                foreach (var outputScen in scenario.OutputScenarioDataSet)
                {
                    var outputScenTmp = new OutputScenarioData()
                    {
                        Id = outputScen.Id,
                        Scenario = scenTmp,
                        ScenarioId = scenTmp.ID,
                        ContractVolume = outputScen.ContractVolume
                    };
                    var tmpBorder = new Border() { ID = outputScen.Obj.ID };
                    if (cntxt.Set<Border>().Local.All(b => b.ID != tmpBorder.ID))
                        cntxt.Set<Border>().Attach(tmpBorder);

                    outputScenTmp.Obj = cntxt.Set<Border>().Local.First(b => b.ID == tmpBorder.ID);
                    
                    if (outputScenTmp.Id > 0)
                    {
                        cntxt.Set<OutputScenarioData>().Attach(outputScenTmp);
                        cntxt.Entry(outputScenTmp).Property(o => o.ContractVolume).IsModified = true;
                    }
                    else
                    {
                        dictTempIdNewIdForOuputScen.Add(new Tuple<OutputScenarioData,OutputScenarioData>(outputScenTmp, outputScen));
                        scenTmp.OutputScenarioDataSet.Add(outputScenTmp);
                    }
                }

                foreach (var russScen in scenario.RussianGasInputScenarioDataSet)
                {
                    var russScenTmp = new RussianGasInputScenarioData()
                    {
                        Id = russScen.Id,
                        Scenario = scenTmp,
                        ScenarioId = scenTmp.ID,
                        MaxExportVolume = russScen.MaxExportVolume
                    };

                    var tmpInput = new RussianGasInput() { ID = russScen.Obj.ID};
                    if (cntxt.Set<RussianGasInput>().Local.All(b => b.ID != tmpInput.ID))
                        cntxt.Set<RussianGasInput>().Attach(tmpInput);
                    russScenTmp.Obj = cntxt.Set<RussianGasInput>().Local.First(r => r.ID == tmpInput.ID);
                    
                    if (russScenTmp.Id > 0)
                    {
                        
                        cntxt.Set<RussianGasInputScenarioData>().Attach(russScenTmp);
                        cntxt.Entry(russScenTmp).Property(o => o.MaxExportVolume).IsModified = true;
                    }
                    else
                    {
                        dictTempIdNewIdForRusInputScen.Add(new Tuple<RussianGasInputScenarioData,RussianGasInputScenarioData>(russScenTmp, russScen));
                        scenTmp.RussianGasInputScenarioDataSet.Add(russScenTmp);
                    }
                }

                foreach (var ugsScen in scenario.UGSScenarioDataSet)
                {
                    var ugsScenTmp = new UGSScenarioData()
                    {
                        Id = ugsScen.Id,
                        Scenario = scenTmp,
                        ScenarioId = scenTmp.ID,
                        MaxUGSWithdrawal = ugsScen.MaxUGSWithdrawal
                    };

                    var tmpUGS = new UGS() { ID = ugsScen.Obj.ID };
                    if (cntxt.Set<UGS>().Local.All(b => b.ID != tmpUGS.ID))
                        cntxt.Set<UGS>().Attach(tmpUGS);
                    ugsScenTmp.Obj = cntxt.Set<UGS>().Local.First(r => r.ID == tmpUGS.ID);
                    

                    if (ugsScenTmp.Id > 0)
                    {
                        cntxt.Set<UGSScenarioData>().Attach(ugsScenTmp);
                        cntxt.Entry(ugsScenTmp).Property(o => o.MaxUGSWithdrawal).IsModified = true;
                    }
                    else
                    {
                        dictTempIdNewIdForUgsScen.Add(new Tuple<UGSScenarioData,UGSScenarioData>(ugsScenTmp, ugsScen));
                        scenTmp.UGSScenarioDataSet.Add(ugsScenTmp);
                    }
                }

                foreach (var borderScen in scenario.BorderScenarioDataSet)
                {
                    var borderScenTmp = new BorderScenarioData()
                    {
                        Id = borderScen.Id,
                        Scenario = scenTmp,
                        ScenarioId = scenTmp.ID,
                        AvailableCapacity = borderScen.AvailableCapacity,
                        BookedCapacity = borderScen.BookedCapacity,
                        Tariff = borderScen.Tariff,
                        GsnValue = borderScen.GsnValue,
                        GsnPercentValue = borderScen.GsnPercentValue,
                        Description = borderScen.Description,
                        Direction = borderScen.Direction
                    };

                    GasExportObject tmpGeo=null;
                    if(borderScen.GasExportObject is Border)
                        tmpGeo = new Border() { ID = borderScen.GasExportObject.ID };
                    if (borderScen.GasExportObject is UGS)
                        tmpGeo = new UGS() { ID = borderScen.GasExportObject.ID };
                    if (borderScen.GasExportObject is RussianGasInput)
                        tmpGeo = new RussianGasInput() { ID = borderScen.GasExportObject.ID };

                    BalanceZone tmpZone = null;
                    tmpZone = new BalanceZone() { ID = borderScen.Operator.ID };

                    if (!cntxt.Set<GasExportObject>().Local.Any(b => b.ID == tmpGeo.ID))
                        cntxt.Set<GasExportObject>().Attach(tmpGeo);

                    if (cntxt.Set<BalanceZone>().Local.All(o => o.ID != tmpZone.ID))
                        cntxt.Set<BalanceZone>().Attach(tmpZone);

                    borderScenTmp.GasExportObject = cntxt.Set<GasExportObject>().Local.First(r => r.ID == tmpGeo.ID);
                    borderScenTmp.Operator = cntxt.Set<BalanceZone>().Local.First(r => r.ID == tmpZone.ID);
                    

                    if (borderScenTmp.Id > 0)
                    {
                        cntxt.Set<BorderScenarioData>().Attach(borderScenTmp);
                        cntxt.Entry(borderScenTmp).Property(o => o.AvailableCapacity).IsModified = true;
                        cntxt.Entry(borderScenTmp).Property(o => o.BookedCapacity).IsModified = true;
                        cntxt.Entry(borderScenTmp).Property(o => o.Tariff).IsModified = true;
                        cntxt.Entry(borderScenTmp).Property(o => o.GsnValue).IsModified = true;
                        cntxt.Entry(borderScenTmp).Property(o => o.GsnPercentValue).IsModified = true;
                        cntxt.Entry(borderScenTmp).Property(o => o.Description).IsModified = true;
                    }
                    else
                    {
                        dictTempIdNewIdForBorderScen.Add(new Tuple<BorderScenarioData,BorderScenarioData>(borderScenTmp, borderScen));
                        scenTmp.BorderScenarioDataSet.Add(borderScenTmp);

                    }
                }

                cntxt.SaveChanges();

                foreach(var pair in dictTempIdNewIdForBorderScen)
                {
                    pair.Item2.Id = pair.Item1.Id;
                }
                foreach (var pair in dictTempIdNewIdForOuputScen)
                {
                    pair.Item2.Id = pair.Item1.Id;
                }
                foreach (var pair in dictTempIdNewIdForRusInputScen)
                {
                    pair.Item2.Id = pair.Item1.Id;
                }
                foreach (var pair in dictTempIdNewIdForUgsScen)
                {
                    pair.Item2.Id = pair.Item1.Id;
                }
            }
        }

        public void Remove(int scenarioId)
        {
            using (var cntxt = new GExDbContext())
            {
                var originScenario = GetScenario(cntxt, scenarioId);
                if(originScenario!=null)
                    cntxt.Scenarios.Remove(originScenario);
                cntxt.SaveChanges();

            }
        }
    }
}
