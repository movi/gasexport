namespace GEx.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Ini : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BalanceZones",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.GazExportObjects",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.BorderInterconnections",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BorderId = c.Int(nullable: false),
                        ZoneFirst_ID = c.Int(),
                        ZoneSecond_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Borders", t => t.BorderId)
                .ForeignKey("dbo.BalanceZones", t => t.ZoneFirst_ID)
                .ForeignKey("dbo.BalanceZones", t => t.ZoneSecond_ID)
                .Index(t => t.BorderId)
                .Index(t => t.ZoneFirst_ID)
                .Index(t => t.ZoneSecond_ID);
            
            CreateTable(
                "dbo.Nodes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        X = c.Double(nullable: false),
                        Y = c.Double(nullable: false),
                        GasExportObject_ID = c.Int(),
                        Schema_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.GazExportObjects", t => t.GasExportObject_ID)
                .ForeignKey("dbo.Schemas", t => t.Schema_ID)
                .Index(t => t.GasExportObject_ID)
                .Index(t => t.Schema_ID);
            
            CreateTable(
                "dbo.Schemas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CreationDate = c.DateTime(nullable: false),
                        Author = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.NodesRelations",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SchemaID = c.Int(nullable: false),
                        BalanceZone_ID = c.Int(nullable: false),
                        NodeFirst_ID = c.Int(nullable: false),
                        NodeSecond_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.BalanceZones", t => t.BalanceZone_ID, cascadeDelete: true)
                .ForeignKey("dbo.Nodes", t => t.NodeFirst_ID)
                .ForeignKey("dbo.Nodes", t => t.NodeSecond_ID)
                .ForeignKey("dbo.Schemas", t => t.SchemaID, cascadeDelete: true)
                .Index(t => t.SchemaID)
                .Index(t => t.BalanceZone_ID)
                .Index(t => t.NodeFirst_ID)
                .Index(t => t.NodeSecond_ID);
            
            CreateTable(
                "dbo.CalculationScenarios",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CreationDate = c.DateTime(nullable: false),
                        Author = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.BorderScenarioDatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ScenarioId = c.Int(nullable: false),
                        Direction = c.Int(nullable: false),
                        BookedCapacity = c.Double(),
                        AvailableCapacity = c.Double(),
                        Tariff = c.Double(),
                        GasExportObject_ID = c.Int(nullable: false),
                        Operator_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GazExportObjects", t => t.GasExportObject_ID, cascadeDelete: true)
                .ForeignKey("dbo.BalanceZones", t => t.Operator_ID, cascadeDelete: true)
                .ForeignKey("dbo.CalculationScenarios", t => t.ScenarioId, cascadeDelete: true)
                .Index(t => t.ScenarioId)
                .Index(t => t.GasExportObject_ID)
                .Index(t => t.Operator_ID);
            
            CreateTable(
                "dbo.OutputScenarioDatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ScenarioId = c.Int(nullable: false),
                        ConctractVolume = c.Double(),
                        Obj_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Outputs", t => t.Obj_ID)
                .ForeignKey("dbo.CalculationScenarios", t => t.ScenarioId, cascadeDelete: true)
                .Index(t => t.ScenarioId)
                .Index(t => t.Obj_ID);
            
            CreateTable(
                "dbo.RussianGasInputScenarioDatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ScenarioId = c.Int(nullable: false),
                        MaxExportVolume = c.Double(),
                        Obj_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RussianGasInputs", t => t.Obj_ID)
                .ForeignKey("dbo.CalculationScenarios", t => t.ScenarioId, cascadeDelete: true)
                .Index(t => t.ScenarioId)
                .Index(t => t.Obj_ID);
            
            CreateTable(
                "dbo.UGSScenarioDatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ScenarioId = c.Int(nullable: false),
                        MaxUGSWithdrawal = c.Double(),
                        Obj_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UGSes", t => t.Obj_ID)
                .ForeignKey("dbo.CalculationScenarios", t => t.ScenarioId, cascadeDelete: true)
                .Index(t => t.ScenarioId)
                .Index(t => t.Obj_ID);
            
            CreateTable(
                "dbo.Outputs",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Zone_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.GazExportObjects", t => t.ID)
                .ForeignKey("dbo.BalanceZones", t => t.Zone_ID)
                .Index(t => t.ID)
                .Index(t => t.Zone_ID);
            
            CreateTable(
                "dbo.RussianGasInputs",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Zone_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.GazExportObjects", t => t.ID)
                .ForeignKey("dbo.BalanceZones", t => t.Zone_ID)
                .Index(t => t.ID)
                .Index(t => t.Zone_ID);
            
            CreateTable(
                "dbo.UGSes",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Zone_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.GazExportObjects", t => t.ID)
                .ForeignKey("dbo.BalanceZones", t => t.Zone_ID)
                .Index(t => t.ID)
                .Index(t => t.Zone_ID);
            
            CreateTable(
                "dbo.Borders",
                c => new
                    {
                        ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.GazExportObjects", t => t.ID)
                .Index(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Borders", "ID", "dbo.GazExportObjects");
            DropForeignKey("dbo.UGSes", "Zone_ID", "dbo.BalanceZones");
            DropForeignKey("dbo.UGSes", "ID", "dbo.GazExportObjects");
            DropForeignKey("dbo.RussianGasInputs", "Zone_ID", "dbo.BalanceZones");
            DropForeignKey("dbo.RussianGasInputs", "ID", "dbo.GazExportObjects");
            DropForeignKey("dbo.Outputs", "Zone_ID", "dbo.BalanceZones");
            DropForeignKey("dbo.Outputs", "ID", "dbo.GazExportObjects");
            DropForeignKey("dbo.UGSScenarioDatas", "ScenarioId", "dbo.CalculationScenarios");
            DropForeignKey("dbo.UGSScenarioDatas", "Obj_ID", "dbo.UGSes");
            DropForeignKey("dbo.RussianGasInputScenarioDatas", "ScenarioId", "dbo.CalculationScenarios");
            DropForeignKey("dbo.RussianGasInputScenarioDatas", "Obj_ID", "dbo.RussianGasInputs");
            DropForeignKey("dbo.OutputScenarioDatas", "ScenarioId", "dbo.CalculationScenarios");
            DropForeignKey("dbo.OutputScenarioDatas", "Obj_ID", "dbo.Outputs");
            DropForeignKey("dbo.BorderScenarioDatas", "ScenarioId", "dbo.CalculationScenarios");
            DropForeignKey("dbo.BorderScenarioDatas", "Operator_ID", "dbo.BalanceZones");
            DropForeignKey("dbo.BorderScenarioDatas", "GasExportObject_ID", "dbo.GazExportObjects");
            DropForeignKey("dbo.NodesRelations", "SchemaID", "dbo.Schemas");
            DropForeignKey("dbo.NodesRelations", "NodeSecond_ID", "dbo.Nodes");
            DropForeignKey("dbo.NodesRelations", "NodeFirst_ID", "dbo.Nodes");
            DropForeignKey("dbo.NodesRelations", "BalanceZone_ID", "dbo.BalanceZones");
            DropForeignKey("dbo.Nodes", "Schema_ID", "dbo.Schemas");
            DropForeignKey("dbo.Nodes", "GasExportObject_ID", "dbo.GazExportObjects");
            DropForeignKey("dbo.BorderInterconnections", "ZoneSecond_ID", "dbo.BalanceZones");
            DropForeignKey("dbo.BorderInterconnections", "ZoneFirst_ID", "dbo.BalanceZones");
            DropForeignKey("dbo.BorderInterconnections", "BorderId", "dbo.Borders");
            DropIndex("dbo.Borders", new[] { "ID" });
            DropIndex("dbo.UGSes", new[] { "Zone_ID" });
            DropIndex("dbo.UGSes", new[] { "ID" });
            DropIndex("dbo.RussianGasInputs", new[] { "Zone_ID" });
            DropIndex("dbo.RussianGasInputs", new[] { "ID" });
            DropIndex("dbo.Outputs", new[] { "Zone_ID" });
            DropIndex("dbo.Outputs", new[] { "ID" });
            DropIndex("dbo.UGSScenarioDatas", new[] { "Obj_ID" });
            DropIndex("dbo.UGSScenarioDatas", new[] { "ScenarioId" });
            DropIndex("dbo.RussianGasInputScenarioDatas", new[] { "Obj_ID" });
            DropIndex("dbo.RussianGasInputScenarioDatas", new[] { "ScenarioId" });
            DropIndex("dbo.OutputScenarioDatas", new[] { "Obj_ID" });
            DropIndex("dbo.OutputScenarioDatas", new[] { "ScenarioId" });
            DropIndex("dbo.BorderScenarioDatas", new[] { "Operator_ID" });
            DropIndex("dbo.BorderScenarioDatas", new[] { "GasExportObject_ID" });
            DropIndex("dbo.BorderScenarioDatas", new[] { "ScenarioId" });
            DropIndex("dbo.NodesRelations", new[] { "NodeSecond_ID" });
            DropIndex("dbo.NodesRelations", new[] { "NodeFirst_ID" });
            DropIndex("dbo.NodesRelations", new[] { "BalanceZone_ID" });
            DropIndex("dbo.NodesRelations", new[] { "SchemaID" });
            DropIndex("dbo.Nodes", new[] { "Schema_ID" });
            DropIndex("dbo.Nodes", new[] { "GasExportObject_ID" });
            DropIndex("dbo.BorderInterconnections", new[] { "ZoneSecond_ID" });
            DropIndex("dbo.BorderInterconnections", new[] { "ZoneFirst_ID" });
            DropIndex("dbo.BorderInterconnections", new[] { "BorderId" });
            DropTable("dbo.Borders");
            DropTable("dbo.UGSes");
            DropTable("dbo.RussianGasInputs");
            DropTable("dbo.Outputs");
            DropTable("dbo.UGSScenarioDatas");
            DropTable("dbo.RussianGasInputScenarioDatas");
            DropTable("dbo.OutputScenarioDatas");
            DropTable("dbo.BorderScenarioDatas");
            DropTable("dbo.CalculationScenarios");
            DropTable("dbo.NodesRelations");
            DropTable("dbo.Schemas");
            DropTable("dbo.Nodes");
            DropTable("dbo.BorderInterconnections");
            DropTable("dbo.GazExportObjects");
            DropTable("dbo.BalanceZones");
        }
    }
}
