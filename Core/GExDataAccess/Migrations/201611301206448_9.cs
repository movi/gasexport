namespace GEx.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _9 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Schemas", "Comment", c => c.String());
            AddColumn("dbo.CalculationScenarios", "Comment", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CalculationScenarios", "Comment");
            DropColumn("dbo.Schemas", "Comment");
        }
    }
}
