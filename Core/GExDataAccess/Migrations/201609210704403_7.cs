namespace GEx.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class _7 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BorderInterconnections", "BorderId", "dbo.Borders");


            AddForeignKey("dbo.BorderInterconnections", "BorderId", "dbo.Borders", "ID", cascadeDelete: true);
        }

        public override void Down()
        {
            DropForeignKey("dbo.BorderInterconnections", "BorderId", "dbo.Borders");


            AddForeignKey("dbo.BorderInterconnections", "BorderId", "dbo.Borders", "ID", cascadeDelete: false);
        }
    }
}
