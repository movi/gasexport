namespace GEx.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _6 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UGSScenarioDatas", "Obj_ID", "dbo.UGSes");
            DropForeignKey("dbo.RussianGasInputScenarioDatas", "Obj_ID", "dbo.RussianGasInputs");
            AddForeignKey("dbo.UGSScenarioDatas", "Obj_ID", "dbo.UGSes", "ID", cascadeDelete: true);
            AddForeignKey("dbo.RussianGasInputScenarioDatas", "Obj_ID", "dbo.RussianGasInputs", "ID", cascadeDelete: true);
        }

        public override void Down()
        {
            DropForeignKey("dbo.UGSScenarioDatas", "Obj_ID", "dbo.UGSes");
            DropForeignKey("dbo.RussianGasInputScenarioDatas", "Obj_ID", "dbo.RussianGasInputs");
            AddForeignKey("dbo.UGSScenarioDatas", "Obj_ID", "dbo.UGSes", "ID", cascadeDelete: false);
            AddForeignKey("dbo.RussianGasInputScenarioDatas", "Obj_ID", "dbo.RussianGasInputs", "ID", cascadeDelete: true);
        }
    }
}


