namespace GEx.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Nodes", "LabelX", c => c.Double());
            AddColumn("dbo.Nodes", "LabelY", c => c.Double());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Nodes", "LabelY");
            DropColumn("dbo.Nodes", "LabelX");
        }
    }
}
