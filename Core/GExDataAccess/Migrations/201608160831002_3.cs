namespace GEx.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _3 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.BorderInterconnections", name: "ZoneFirst_ID", newName: "ZoneFrom_ID");
            RenameColumn(table: "dbo.BorderInterconnections", name: "ZoneSecond_ID", newName: "ZoneTo_ID");
            RenameIndex(table: "dbo.BorderInterconnections", name: "IX_ZoneFirst_ID", newName: "IX_ZoneFrom_ID");
            RenameIndex(table: "dbo.BorderInterconnections", name: "IX_ZoneSecond_ID", newName: "IX_ZoneTo_ID");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.BorderInterconnections", name: "IX_ZoneTo_ID", newName: "IX_ZoneSecond_ID");
            RenameIndex(table: "dbo.BorderInterconnections", name: "IX_ZoneFrom_ID", newName: "IX_ZoneFirst_ID");
            RenameColumn(table: "dbo.BorderInterconnections", name: "ZoneTo_ID", newName: "ZoneSecond_ID");
            RenameColumn(table: "dbo.BorderInterconnections", name: "ZoneFrom_ID", newName: "ZoneFirst_ID");
        }
    }
}
