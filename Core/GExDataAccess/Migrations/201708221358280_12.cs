namespace GEx.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _12 : DbMigration
    {
        public override void Up()
        {
            //  AddColumn("dbo.OutputScenarioDatas", "ContractVolume", c => c.Double());
            //  DropColumn("dbo.OutputScenarioDatas", "ConctractVolume");
            RenameColumn("dbo.OutputScenarioDatas", "ConctractVolume", "ContractVolume");
        }

        public override void Down()
        {
            // AddColumn("dbo.OutputScenarioDatas", "ConctractVolume", c => c.Double());
            // DropColumn("dbo.OutputScenarioDatas", "ContractVolume");
            RenameColumn("dbo.OutputScenarioDatas", "ContractVolume", "ConctractVolume");
        }
    }
}
