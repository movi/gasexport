namespace GEx.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _11 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SchemaImages", "Id", "dbo.Schemas");
            AddForeignKey("dbo.SchemaImages", "Id", "dbo.Schemas", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SchemaImages", "Id", "dbo.Schemas");
            AddForeignKey("dbo.SchemaImages", "Id", "dbo.Schemas", "ID");
        }
    }
}
