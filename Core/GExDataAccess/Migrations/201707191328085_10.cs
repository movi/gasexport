namespace GEx.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _10 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SchemaImages",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Image = c.Binary(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Schemas", t => t.Id)
                .Index(t => t.Id);
            
            AddColumn("dbo.NodesRelations", "Value", c => c.Double(nullable: false));
            AddColumn("dbo.BorderScenarioDatas", "GsnValue", c => c.Double());
            AddColumn("dbo.BorderScenarioDatas", "GsnPercentValue", c => c.Double());
            AddColumn("dbo.BorderScenarioDatas", "Description", c => c.String());
            Sql("Update dbo.[NodesRelations] set value = 100");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SchemaImages", "Id", "dbo.Schemas");
            DropIndex("dbo.SchemaImages", new[] { "Id" });
            DropColumn("dbo.BorderScenarioDatas", "Description");
            DropColumn("dbo.BorderScenarioDatas", "GsnPercentValue");
            DropColumn("dbo.BorderScenarioDatas", "GsnValue");
            DropColumn("dbo.NodesRelations", "Value");
            DropTable("dbo.SchemaImages");
        }
    }
}
