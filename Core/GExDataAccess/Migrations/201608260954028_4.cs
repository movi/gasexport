namespace GEx.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OutputScenarioDatas", "Obj_ID", "dbo.Outputs");
            AddForeignKey("dbo.OutputScenarioDatas", "Obj_ID", "dbo.Borders", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OutputScenarioDatas", "Obj_ID", "dbo.Borders");
            AddForeignKey("dbo.OutputScenarioDatas", "Obj_ID", "dbo.Outputs", "ID", cascadeDelete: true);
            
        }
    }
}
