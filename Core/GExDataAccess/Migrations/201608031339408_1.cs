namespace GEx.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Nodes", "Schema_ID", "dbo.Schemas");
            DropIndex("dbo.Nodes", new[] { "Schema_ID" });
            AlterColumn("dbo.Nodes", "Schema_ID", c => c.Int(nullable: false));
            CreateIndex("dbo.Nodes", "Schema_ID");
            AddForeignKey("dbo.Nodes", "Schema_ID", "dbo.Schemas", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Nodes", "Schema_ID", "dbo.Schemas");
            DropIndex("dbo.Nodes", new[] { "Schema_ID" });
            AlterColumn("dbo.Nodes", "Schema_ID", c => c.Int());
            CreateIndex("dbo.Nodes", "Schema_ID");
            AddForeignKey("dbo.Nodes", "Schema_ID", "dbo.Schemas", "ID");
        }
    }
}
