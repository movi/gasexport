﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GEx.Domain;
using GEx.Iface;
using GEx.ErrorHandler;
using System.Data.Entity;
using System.Diagnostics;

namespace GEx.DataAccess
{
    public class SchemaRepository : ISchemaRepository
    {
        private IErrorOutput _errorHandler;

        public void Init(IErrorOutput eh)
        {
            _errorHandler = eh;
        }

        public Schema Load(int schemaId)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            Schema schema;
            using (var cntxt = new GExDbContext())
            {
                schema = cntxt.Schemas.
                    Include(s => s.Relations.Select(r => r.BalanceZone)).
                    Include(s => s.Nodes.Select(n => n.GasExportObject)).
                    FirstOrDefault(s => s.ID == schemaId);
                cntxt.Set<Schema>().Include(i => i.SchemaImage).Load();
                cntxt.Set<UGS>().Include(z => z.Zone).Load();
                cntxt.Set<RussianGasInput>().Include(z => z.Zone).Load();
                cntxt.Set<Border>().
                    Include(b => b.Interconnections.Select(c => c.ZoneFrom)).
                    Include(b => b.Interconnections.Select(c => c.ZoneTo)).Load();

                //foreach (var node in schema.Nodes)
                //{
                //    Border border = node.GasExportObject as Border;
                //    if (border != null)
                //    {
                //        cntxt.Entry(border).Collection(ic => ic.Interconnections).Load();

                //        foreach (var conn in border.Interconnections)
                //        {
                //            cntxt.Entry(conn).Reference(b => b.ZoneFrom).Load();
                //            cntxt.Entry(conn).Reference(b => b.ZoneTo).Load();
                //        }
                //    }
                //    else
                //    {
                //        IOneZoneNode oneZone = node.GasExportObject as IOneZoneNode;
                //        cntxt.Entry(oneZone).Reference(b => b.Zone).Load();
                //    }
                //}
            }

            sw.Stop();
            _errorHandler.ReportError(eErrClass.EC_TRACE, string.Format("SchemaRepository.Load time {0}", sw.Elapsed.TotalMilliseconds), "SchemaRepository.Load");
            return schema;
        }

        public void Create(Schema schema)
        {
            Stopwatch sw_0 = new Stopwatch();
            sw_0.Start();


            using (var cntxt = new GExDbContext())
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();

                cntxt.Set<BalanceZone>().Load();
                cntxt.Set<GasExportObject>().Load();
                cntxt.Set<UGS>().Include(z => z.Zone).Load();
                cntxt.Set<RussianGasInput>().Include(z => z.Zone).Load();
                cntxt.Set<Border>().
                    Include(b => b.Interconnections.Select(c => c.ZoneFrom)).
                    Include(b => b.Interconnections.Select(c => c.ZoneTo)).Load();


                var dict_id_Node = schema.Nodes.ToDictionary(n => n.ID);
                var dict_id_Zone = cntxt.BalanceZones.Local.ToDictionary(x => x.ID);
                var dict_id_Geos = cntxt.GasExportObjects.Local.ToDictionary(x => x.ID);

                sw.Stop();
                _errorHandler.ReportError(eErrClass.EC_TRACE, string.Format("SchemaRepository.Create preload {0}", sw.Elapsed.TotalMilliseconds), "SchemaRepository.Create");


                foreach (var node in schema.Nodes)
                {
                    Stopwatch sw2 = new Stopwatch();
                    sw2.Start();

                    if (node.Schema != schema)
                        node.Schema = schema;

                    if (node.GasExportObject == null)
                        continue;
                    var geo = node.GasExportObject;
                    var geoType = node.GasExportObject.Type;
                    if (geo.ID > 0)
                    {
                        var origGeo = dict_id_Geos[geo.ID];
                        if (origGeo != null)
                            cntxt.Entry(origGeo).CurrentValues.SetValues(geo);
                        node.GasExportObject = origGeo;
                    }
                    else
                    {
                        if (geo is IOneZoneNode)
                        {
                            var oneZoneNode = geo as IOneZoneNode;
                            if (oneZoneNode.Zone != null && oneZoneNode.Zone.ID > 0)
                                oneZoneNode.Zone = dict_id_Zone[oneZoneNode.Zone.ID];
                        }

                        if (geo is Border)
                        {
                            var border = geo as Border;
                            foreach (var conn in border.Interconnections)
                            {
                                if (conn.ZoneFrom != null)
                                    conn.ZoneFrom = dict_id_Zone[conn.ZoneFrom.ID];
                                if (conn.ZoneTo != null)
                                    conn.ZoneTo = dict_id_Zone[conn.ZoneTo.ID];
                            }
                        }
                    }

                    sw2.Stop();
                    _errorHandler.ReportError(eErrClass.EC_TRACE, string.Format("SchemaRepository.Create node iteration {0}", sw2.Elapsed.TotalMilliseconds), "SchemaRepository.Create");
                }






                foreach (var rel in schema.Relations)
                {
                    Stopwatch sw3 = new Stopwatch();
                    sw3.Start();

                    rel.BalanceZone = dict_id_Zone[rel.BalanceZone.ID];

                    var nodeFirst = dict_id_Node[rel.NodeFirst.ID];
                    var nodeSecond = dict_id_Node[rel.NodeSecond.ID];

                    if (rel.NodeFirst != nodeFirst)
                        rel.NodeFirst = nodeFirst;
                    if (rel.NodeSecond != nodeSecond)
                        rel.NodeSecond = nodeSecond;
                    if (rel.Schema != schema)
                        rel.Schema = schema;

                    sw3.Stop();
                    _errorHandler.ReportError(eErrClass.EC_TRACE, string.Format("SchemaRepository.Create relations iter {0}", sw3.Elapsed.TotalMilliseconds), "SchemaRepository.Create");
                }



                cntxt.Schemas.Add(schema);
                schema.CreationDate = DateTime.Now;
                cntxt.SaveChanges();


            }

            sw_0.Stop();
            _errorHandler.ReportError(eErrClass.EC_TRACE, string.Format("whole time {0}", sw_0.Elapsed.TotalMilliseconds), "SchemaRepository.Create");
        }

        public IEnumerable<Schema> LoadAll()
        {
            IEnumerable<Schema> allSchemas;
            using (var cntxt = new GExDbContext())
            {
                allSchemas = cntxt.Schemas.ToArray();
            }
            return allSchemas;
        }

        public void Update(Schema schema)
        {
            using (var cntxt = new GExDbContext())
            {
                var originalSchema = cntxt.Schemas.
                    Include(s => s.Relations.Select(r => r.BalanceZone)).
                    Include(s => s.Nodes.Select(n => n.GasExportObject)).
                    FirstOrDefault(s => s.ID == schema.ID);

                cntxt.Entry(originalSchema).CurrentValues.SetValues(schema);
                foreach (var curNode in schema.Nodes)
                {
                    if (curNode.ID <= 0)
                    {
                        if (curNode.GasExportObject == null)
                            continue;
                        originalSchema.Nodes.Add(curNode);
                        continue;
                    }

                    var origNode = originalSchema.Nodes.FirstOrDefault(n => n.ID == curNode.ID);
                    if (origNode == null)
                        continue;

                    cntxt.Entry(origNode).CurrentValues.SetValues(curNode);
                    var origGazExportObject = origNode.GasExportObject;
                    var curGazExportObject = curNode.GasExportObject;
                    if (origGazExportObject != null && curNode.GasExportObject != null)
                        cntxt.Entry(origGazExportObject).CurrentValues.SetValues(curGazExportObject);
                }

                cntxt.Entry(originalSchema).CurrentValues.SetValues(schema);
                foreach (var curRel in schema.Relations)
                {
                    if (curRel.ID <= 0)
                    {
                        curRel.Schema = originalSchema;
                        curRel.NodeFirst = originalSchema.Nodes.FirstOrDefault(n => n.ID == curRel.NodeFirst.ID);
                        curRel.NodeSecond = originalSchema.Nodes.FirstOrDefault(n => n.ID == curRel.NodeSecond.ID);
                        curRel.BalanceZone = cntxt.BalanceZones.Find(curRel.BalanceZone.ID);
                        originalSchema.Relations.Add(curRel);
                        continue;
                    }
                    var origRel = originalSchema.Relations.FirstOrDefault(r => r.ID == curRel.ID);
                    origRel.Value = curRel.Value;


                }

                var idsOfOriginalRelations = originalSchema.Relations.Select(r => r.ID).ToArray();
                var idsOfCurrentRelations = schema.Relations.Select(r => r.ID).ToArray();
                var idsOfOriginNodes = originalSchema.Nodes.Select(n => n.ID).ToArray();
                var idsOfCurrentNodes = schema.Nodes.Select(n => n.ID).ToArray();
                var idsOfNodesForRemove = idsOfOriginNodes.Except(idsOfCurrentNodes).ToArray();
                var idsOfRelationsForRemove = idsOfOriginalRelations.Except(idsOfCurrentRelations).Union(
                 originalSchema.Relations.Where(r => idsOfNodesForRemove.Contains(r.NodeFirst.ID) || idsOfNodesForRemove.Contains(r.NodeSecond.ID)).Select(r => r.ID)
                 ).ToArray();

                foreach (var id in idsOfRelationsForRemove)
                {
                    var rel = originalSchema.Relations.FirstOrDefault(r => r.ID == id);
                    if (rel == null) continue;
                    originalSchema.Relations.Remove(rel);
                    cntxt.Set<NodesRelation>().Remove(rel);
                }

                foreach (var id in idsOfNodesForRemove)
                {
                    var nodeForRemove = originalSchema.Nodes.FirstOrDefault(n => n.ID == id);
                    if (nodeForRemove == null) continue;

                    var geo = nodeForRemove.GasExportObject;
                    cntxt.Nodes.Remove(nodeForRemove);
                    originalSchema.Nodes.Remove(nodeForRemove);
                    if (!cntxt.Nodes.Any(n => n.GasExportObject.ID == geo.ID && n.ID != nodeForRemove.ID))
                        cntxt.GasExportObjects.Remove(geo);
                }

                cntxt.SaveChanges();
            }
        }

        public void Remove(int Id)
        {
            using (var cntxt = new GExDbContext())
            {
                var originalSchema = cntxt.Schemas.Find(Id);
                cntxt.Entry(originalSchema).Collection(n => n.Relations).Load();
                var idsForRemove = cntxt.Nodes.Where(n => n.Schema != null && n.Schema.ID == Id).AsNoTracking().Select(n => n.ID).ToArray();
                foreach (var id in idsForRemove)
                {
                    var originNode = cntxt.Nodes.Include(n => n.GasExportObject).FirstOrDefault(n => n.ID == id);
                    var geo = originNode.GasExportObject;
                    cntxt.Nodes.Remove(originNode);
                    if (!cntxt.Nodes.Any(n => n.GasExportObject.ID == geo.ID && n.ID != originNode.ID))
                    {
                        cntxt.GasExportObjects.Remove(geo);
                    }
                }
                cntxt.Schemas.Remove(originalSchema);
                cntxt.SaveChanges();
            }
        }
    }
}
