﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GEx.Domain;
using GEx.Iface;
using GEx.ErrorHandler;
using System.Data.Entity;
using System.Diagnostics;

namespace GEx.DataAccess
{
    public class UpdateRelationsCommand : IUpdateRelationsCommand
    {
        private IErrorOutput _errorHandler;

        public void Init(ErrorHandler.IErrorOutput eh)
        {
            _errorHandler = eh;
        }

        public void Update(Schema schema)
        {
            using (var cntxt = new GExDbContext())
            {
                using (var transcat = cntxt.Database.BeginTransaction())
                {
                    try
                    {
                        var originalSchema = cntxt.Schemas.
                            Include(s => s.Relations.Select(r => r.BalanceZone)).
                            Include(s => s.Nodes.Select(n => n.GasExportObject)).
                            FirstOrDefault(s => s.ID == schema.ID);

                        cntxt.Entry(originalSchema).CurrentValues.SetValues(schema);
                        foreach (var curRel in schema.Relations)
                        {
                            if (curRel.ID <= 0)
                            {
                                curRel.Schema = originalSchema;
                                curRel.NodeFirst = originalSchema.Nodes.FirstOrDefault(n => n.ID == curRel.NodeFirst.ID);
                                curRel.NodeSecond = originalSchema.Nodes.FirstOrDefault(n => n.ID == curRel.NodeSecond.ID);
                                curRel.BalanceZone = cntxt.BalanceZones.Find(curRel.BalanceZone.ID);
                                originalSchema.Relations.Add(curRel);
                            }
                            var origRel = originalSchema.Relations.FirstOrDefault(r => r.ID == curRel.ID);
                            origRel.Value = curRel.Value;
                        }

                        var idsOfOriginalRelations = originalSchema.Relations.Select(r => r.ID).ToArray();
                        var idsOfCurrentRelations = schema.Relations.Select(r => r.ID).ToArray();
                        var idsOfOriginNodes = originalSchema.Nodes.Select(n => n.ID).ToArray();
                        var idsOfCurrentNodes = schema.Nodes.Select(n => n.ID).ToArray();
                        var idsOfNodesForRemove = idsOfOriginNodes.Except(idsOfCurrentNodes).ToArray();
                        var idsOfRelationsForRemove = idsOfOriginalRelations.Except(idsOfCurrentRelations).Union(
                         originalSchema.Relations.Where(r => idsOfNodesForRemove.Contains(r.NodeFirst.ID) || idsOfNodesForRemove.Contains(r.NodeSecond.ID)).Select(r => r.ID)
                         ).ToArray();

                        foreach (var id in idsOfRelationsForRemove)
                        {
                            var rel = originalSchema.Relations.FirstOrDefault(r => r.ID == id);
                            if (rel != null)
                            {
                                originalSchema.Relations.Remove(rel);
                                cntxt.Set<NodesRelation>().Remove(rel);
                            }
                        }

                        foreach (var id in idsOfNodesForRemove)
                        {
                            var nodeForRemove = originalSchema.Nodes.FirstOrDefault(n => n.ID == id);
                            if (nodeForRemove != null)
                            {
                                var geo = nodeForRemove.GasExportObject;
                                cntxt.Nodes.Remove(nodeForRemove);

                                originalSchema.Nodes.Remove(nodeForRemove);
                                if (!cntxt.Nodes.Any(n => n.GasExportObject.ID == geo.ID && n.ID != nodeForRemove.ID))
                                {
                                    cntxt.GasExportObjects.Remove(geo);
                                }
                            }
                        }

                        cntxt.SaveChanges();
                        transcat.Commit();

                    }
                    catch (Exception ex)
                    {
                        transcat.Rollback();
                        throw ex;
                    }
                }
            }


           
        }
    }

}
