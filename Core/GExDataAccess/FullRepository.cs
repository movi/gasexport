﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GEx.Domain;
using GEx.ErrorHandler;
using GEx.Iface;

namespace GEx.DataAccess
{
    /*
    public class FullRepository : IFullRepository
    {
        private GExDbContext cntxt;

        void IFullRepository.Init(IErrorOutput eh)
        {
            cntxt = new GExDbContext();
        }

        IEnumerable<BalanceZone> IFullRepository.LoadBalanceZones()
        {
            IEnumerable<BalanceZone> allZones = null;
            using (var cntxt = new GExDbContext())
            {
                allZones = cntxt.BalanceZones.ToArray();
            }
            return allZones;
        }

        Schema IFullRepository.Load(int schemaId)
        {
            Schema schema;
            using (var cntxt = new GExDbContext())
            {
                schema = cntxt.Schemas.
                     Include(s => s.Nodes.Select(n => n.RelatedNodes)).
                     Include(s => s.Nodes.Select(n => n.GasExportObject)).
                     FirstOrDefault(s => s.ID == schemaId);

                foreach (var node in schema.Nodes)
                {
                    Border border = node.GasExportObject as Border;
                    if (border != null)
                    {
                        cntxt.Entry(border).Reference(b => b.BorderZoneFirst).Load();
                        cntxt.Entry(border).Reference(b => b.BorderZoneSecond).Load();
                        continue;
                    }
                    else
                    {
                        IOneZoneNode oneZone = node.GasExportObject as IOneZoneNode;
                        cntxt.Entry(oneZone).Reference(b => b.Zone).Load();
                    }
                }
            }
            return schema;
        }

        private Schema LoadSchemeWithCreatedContext(int schemaId, GExDbContext cntxt)
        {
            var schema = cntxt.Schemas.
                Include(s => s.Nodes.Select(n => n.RelatedNodes)).
                Include(s => s.Nodes.Select(n => n.GasExportObject)).
                FirstOrDefault(s => s.ID == schemaId);

            foreach (var node in schema.Nodes)
            {
                Border border = node.GasExportObject as Border;
                if (border != null)
                {
                    cntxt.Entry(border).Reference(b => b.BorderZoneFirst).Load();
                    cntxt.Entry(border).Reference(b => b.BorderZoneSecond).Load();
                    continue;
                }
                else
                {
                    IOneZoneNode oneZone = node.GasExportObject as IOneZoneNode;
                    cntxt.Entry(oneZone).Reference(b => b.Zone).Load();
                }
            }
            return schema;
        }

        private IEnumerable<BalanceZone> LoadZonesWithCreatedContext(GExDbContext cntxt)
        {
            var allZones = cntxt.BalanceZones.ToArray();
            
            return allZones;
        }

        //DataContainer IFullRepository.GetAllData(int schemeID)
        //{
        //    DataContainer container = new DataContainer();

        //    using (var cntxt = new GExDbContext())
        //    {
        //        container.Scheme = LoadSchemeWithCreatedContext(schemeID, cntxt);
        //        container.BalanceZones = LoadZonesWithCreatedContext(cntxt);
        //    }

        //    return container;
        //}

        void IFullRepository.LoadAll(int schemaID)
        {
        }
              
        CalculationScenario IFullRepository.LoadScenario(int IdScenario)
        {
            return new CalculationScenario();            
            //TODO оформить загрузку сценария
            //throw new NotImplementedException();
        }
    }
     * */
}

