﻿using GEx.Domain;
using GEx.Iface;
using System.Collections.Generic;
using System.Linq;
using GEx.ErrorHandler;

namespace GEx.DataAccess
{
    public class BalanceZoneRepository : IBalanceZoneRepository
    {
        private IErrorOutput _errorOutput;

        public void Init(IErrorOutput eh)
        {
            _errorOutput = eh;
        }

        public IEnumerable<BalanceZone> LoadAll()
        {
            IEnumerable<BalanceZone> allZones;
            using (var cntxt = new GExDbContext())
            {
                allZones = cntxt.BalanceZones.AsNoTracking().ToArray();
            }
            return allZones;
        }


        public void Create(BalanceZone zone)
        {
            using (var cntxt = new GExDbContext())
            {
                cntxt.BalanceZones.Add(zone);
                cntxt.SaveChanges();
            }
        }

        public void Update(BalanceZone zone)
        {
            using (var cntxt = new GExDbContext())
            {
                var origZone = cntxt.BalanceZones.Find(zone.ID);
                if (origZone == null) return;
                origZone.Name = zone.Name;
                cntxt.SaveChanges();
            }
        }

        public void Remove(int id)
        {
            using (var cntxt = new GExDbContext())
            {
                var origZone = cntxt.BalanceZones.Find(id);
                if (origZone == null) return;
                cntxt.BalanceZones.Remove(origZone);
                cntxt.SaveChanges();
            }
        }

        public void Update(IEnumerable<BalanceZone> zones)
        {
            using(var cntxt = new GExDbContext())
            {
               var idsOfOriginZones = cntxt.BalanceZones.Select(z => z.ID).ToArray();
               var idsOfCurrentZones = zones.Select(z => z.ID).ToArray();
               var idsOfOriginZonesForRemove = idsOfOriginZones.Except(idsOfCurrentZones);
               
                foreach(var id in idsOfOriginZonesForRemove)
                {
                    var zoneForRemove = cntxt.BalanceZones.First(z => z.ID==id);
                    cntxt.BalanceZones.Remove(zoneForRemove);
                }

               foreach(var curZone in zones)
               {
                   if(curZone.ID<0)
                       cntxt.BalanceZones.Add(curZone);
                   else
                       cntxt.BalanceZones.First(s => s.ID == curZone.ID).Name = curZone.Name;
               }
               cntxt.SaveChanges();
            }
        }

        public bool HasRelations(int id)
        {
            using (var cntxt = new GExDbContext())
            {
               var hasRelationsInIntrconnections = cntxt.Set<BorderInterconnection>().Any(b => b.ZoneFrom.ID == id || b.ZoneTo.ID == id);
               var hasRelationsInGazExportObjects = cntxt.Set<UGS>().Any(io => io.Zone.ID == id) || cntxt.Set<RussianGasInput>().Any(io => io.Zone.ID == id);
               var hasRelationsInBorderScenatioData= cntxt.Set<BorderScenarioData>().Any(bsd => bsd.Operator.ID == id);
               return hasRelationsInIntrconnections || hasRelationsInGazExportObjects || hasRelationsInBorderScenatioData;
            }
          
        }
    
    }
}
