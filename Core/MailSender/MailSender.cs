﻿using Mailing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Mailing
{
    public class MailSender : IMailing
    {
        #region Nested types

        private class EmailData
        {
            public String Body { get; set; }
            public String From { get; set; }
            public Int32 Port { get; set; }
            public String SmtpServerName { get; set; }
            public String Subject { get; set; }
            public IList<String> To { get; set; }
            public IList<String> Attachments { get; set; }

            public EmailData(String from, IList<String> to, String subject, String body, String smtpServerName, Int32 port, IList<String> attachments)
            {
                Body = body;
                From = from;
                Port = port;
                SmtpServerName = smtpServerName;
                Subject = subject;
                To = to;
                Attachments = attachments;
            }
        }

        #endregion Nested types

        #region Methods

        private void Check(EmailData data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data", "Данные для e-mail равны null");
            }
            if (String.IsNullOrWhiteSpace(data.SmtpServerName))
            {
                throw new ArgumentException("Не задано имя smtp-сервера");
            }
            if (data.Port <= 0)
            {
                throw new ArgumentException("Не корректно задан номер порта smtp-сервера");
            }
            if (String.IsNullOrWhiteSpace(data.From))
            {
                throw new ArgumentException("Не задано имя отправителя");
            }
            if (data.Attachments != null)
            {
                var sbAtt = new StringBuilder();
                foreach (var att in data.Attachments)
                {
                    if (!File.Exists(att))
                    {
                        sbAtt.AppendLine(att + Environment.NewLine);
                    }
                }

                if (sbAtt.Length > 0)
                {
                    throw new ArgumentException("Не найдены файлы вложения: \n" + sbAtt.ToString());
                } 
            }

            var emailPattern = @"\A[^@]+@([^@\.]+\.)+[^@\.]+\z";
            if (!Regex.IsMatch(data.From, emailPattern))
            {
                throw new ArgumentException("Не корректно задан email отправителя: " + data.From);
            }
            if (data.To == null || data.To.Count == 0)
            {
                throw new ArgumentException("Не задан список получателей");
            }
            var sb = new StringBuilder();
            foreach (var to in data.To)
            {
                if (!Regex.IsMatch(to, emailPattern))
                {
                    sb.AppendLine(to);
                }
            }
            if (sb.Length > 0)
            {
                throw new ArgumentException("Не корректно заданы следующие e-mail'ы получателей: \n" + sb.ToString());
            }
        }
        
        #endregion Methods

        public event EventHandler OnMailingFailed;

        public event EventHandler OnMailingSucceeded;
        
        public void SendMail(string subject, string text, string UserFrom, IList<string> UsersTo, IList<string> attach, int Port = 25, string SmtpServerName = "smtp")
        {
            EmailData emData = new EmailData(UserFrom, UsersTo, subject, text, SmtpServerName, Port, attach);
            if (emData == null) throw new ArgumentNullException("Не удалось создать объект - сообщение");

            // проверка валидности параметров письма
            Check(emData);

            try
            {
                var client = new SmtpClient(emData.SmtpServerName, emData.Port);
                client.Timeout = 1000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Credentials = new NetworkCredential(UserFrom.Split('@')[0], "12345");
                var msg = new MailMessage();
                msg.From = new MailAddress(emData.From);
                foreach (var to in emData.To)
                {
                    msg.To.Add(to);
                }
                msg.Subject = emData.Subject;
                msg.Body = emData.Body;
                //msg.Attachments = new AttachmentCollection();
                foreach (var att in emData.Attachments)
                {
                    msg.Attachments.Add(new Attachment(att));
                }

                SendMail(client, msg);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SendMail(string subject, string text, string UserFrom, IList<string> UsersTo, int Port = 25, string SmtpServerName = "smtp")
        {
            SendMail(subject, text, UserFrom, UsersTo, null, Port, SmtpServerName);
        }

        public void SendMail(SmtpClient clnt, MailMessage msg)
        {
            var task = clnt.SendMailAsync(msg);
            task.ContinueWith(
                t =>
                {
                    if (t.Status == System.Threading.Tasks.TaskStatus.RanToCompletion)
                    {
                        var ev = OnMailingSucceeded;
                        if (ev != null) ev(this, EventArgs.Empty);
                    }
                    else if (t.Status == System.Threading.Tasks.TaskStatus.Faulted)
                    {
                        var ev = OnMailingFailed;
                        if (ev != null) ev(this, EventArgs.Empty);
                    };
                });
        }
    }
}
