// /////////////////////////////////////////////////////////////
// File: 
// Date:					Author: Ildar Batyrshin
// Language: C#				Framework: .NET 1.1
// /////////////////////////////////////////////////////////////

namespace GEx.ErrorHandler
{
    using System;

    public enum eErrClass
    {
        EC_INFO,
        EC_WARN,
        EC_ERROR,
        EC_FATAL,
        EC_TRACE,
        EC_HOT_DEBUG
    };

    public interface IErrorOutput
    {
        bool ReportError(eErrClass cls,
                         string sMsg,
                         string sSource,
                         int nCode
                         );

        bool ReportError(eErrClass cls,
                         string sMsg,
                         string sSource
                         );

        bool ReportError(eErrClass cls,
                         int userId,
                         string sMsg,
                         string sSource
                         );

    }
}
