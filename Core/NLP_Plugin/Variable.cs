﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SolverFoundation.Services;

namespace NLPSolverPlugin
{
    public class Variable
    {
        Decision var;
        double solution = 0;
        bool isNLP = false;

        internal Decision Var
        {
            get { return var; }
        }

        internal bool IsNLP
        {
            set { isNLP = value; }
        }

        #region constructor

        public Variable(Decision var)
        {
            this.var = var;
        }
        
        #endregion

        /// <summary>
        /// записываем решение из модели
        /// </summary>
        /// <param name="res">решение</param>
        internal void SetSolution(double res)
        {
            solution = res;
        }

        #region overrides Результат решения

        public double GetValue()
        {
            return this.ToDouble();
        }

        public double ToDouble()
        {
            if (isNLP)
                return solution;
            else
                return var.ToDouble();
        }

        public double GetDouble()
        {
            return this.ToDouble();
        }
        
        #endregion

        #region binary operators (Var x Var)

        public static Term operator +(Variable var1, Variable var2)
        {
            return var1.var + var2.var;
        }

        public static Term operator -(Variable var1, Variable var2)
        {
            return var1.var - var2.var;
        }

        public static Term operator *(Variable var1, Variable var2)
        {
            return var1.var * var2.var;
        }

        public static Term operator /(Variable var1, Variable var2)
        {
            return var1.var / var2.var;
        }

        public static Term operator >(Variable var1, Variable var2)
        {
            return var1.var > var2.var;
        }

        public static Term operator >=(Variable var1, Variable var2)
        {
            return var1.var >= var2.var;
        }

        public static Term operator <(Variable var1, Variable var2)
        {
            return var1.var < var2.var;
        }

        public static Term operator <=(Variable var1, Variable var2)
        {
            return var1.var <= var2.var;
        }

        public static Term operator ==(Variable var1, Variable var2)
        {
            return var1.var == var2.var;
        }

        public static Term operator !=(Variable var1, Variable var2)
        {
            return var1.var != var2.var;
        }

        #endregion
        #region binary operators (Var x Term)

        public static Term operator +(Variable var, Term term)
        {
            return var.var + term;
        }

        public static Term operator +(Term term, Variable var)
        {
            return term + var.var;
        }

        public static Term operator *(Variable var, Term term)
        {
            return var.var * term;
        }

        public static Term operator *(Term term, Variable var)
        {
            return term * var.var;
        }
        public static Term operator /(Variable var, Term term)
        {
            return var.var / term;
        }

        public static Term operator /(Term term, Variable var)
        {
            return term / var.var;
        }

        public static Term operator -(Variable var, Term term)
        {
            return var.var - term;
        }

        public static Term operator -(Term term, Variable var)
        {
            return term - var.var;
        }

        public static Term operator >(Variable var, Term term)
        {
            return var.var > term;
        }

        public static Term operator >(Term term, Variable var)
        {
            return term > var.var;
        }

        public static Term operator >=(Variable var, Term term)
        {
            return var.var >= term;
        }

        public static Term operator >=(Term term, Variable var)
        {
            return term >= var.var;
        }

        public static Term operator <(Variable var, Term term)
        {
            return var.var < term;
        }

        public static Term operator <(Term term, Variable var)
        {
            return term < var.var;
        }

        public static Term operator <=(Variable var, Term term)
        {
            return var.var <= term;
        }

        public static Term operator <=(Term term, Variable var)
        {
            return term <= var.var;
        }

        public static Term operator ==(Variable var, Term term)
        {
            return var.var == term;
        }

        public static Term operator ==(Term term, Variable var)
        {
            return term == var.var;
        }

        public static Term operator !=(Variable var, Term term)
        {
            return var.var != term;
        }

        public static Term operator !=(Term term, Variable var)
        {
            return term != var.var;
        }

        #endregion


        #region binary operators (Var x double)

        public static Term operator +(Variable var, double coeff)
        {
            return var.var + coeff;
        }

        public static Term operator +(double coeff, Variable var)
        {
            return coeff + var.var;
        }

        public static Term operator /(Variable var, double coeff)
        {
            return var.var / coeff;
        }

        public static Term operator /(double coeff, Variable var)
        {
            return coeff / var.var;
        }

        public static Term operator *(Variable var, double coeff)
        {
            return var.var * coeff;
        }

        public static Term operator *(double coeff, Variable var)
        {
            return coeff * var.var;
        }

        public static Term operator -(Variable var, double coeff)
        {
            return var.var - coeff;
        }

        public static Term operator -(double term, Variable var)
        {
            return term - var.var;
        }

        public static Term operator >(Variable var, double coeff)
        {
            return var.var > coeff;
        }

        public static Term operator >(double coeff, Variable var)
        {
            return coeff > var.var;
        }

        public static Term operator >=(Variable var, double term)
        {
            return var.var >= term;
        }

        public static Term operator >=(double coeff, Variable var)
        {
            return coeff >= var.var;
        }

        public static Term operator <(Variable var, double coeff)
        {
            return var.var < coeff;
        }

        public static Term operator <(double coeff, Variable var)
        {
            return coeff < var.var;
        }

        public static Term operator <=(Variable var, double coeff)
        {
            return var.var <= coeff;
        }

        public static Term operator <=(double coeff, Variable var)
        {
            return coeff <= var.var;
        }

        public static Term operator ==(Variable var, double coeff)
        {
            return var.var == coeff;
        }

        public static Term operator ==(double coeff, Variable var)
        {
            return coeff == var.var;
        }

        public static Term operator !=(Variable var, double coeff)
        {
            return var.var != coeff;
        }

        public static Term operator !=(double coeff, Variable var)
        {
            return coeff != var.var;
        }

        #endregion

        #region unary operators

        public static Term operator -(Variable var)
        {
            return -var.var;
        }

        #endregion

        #region implicit operators

        public static implicit operator Term(Variable var)
        {
            return var.var;
        }

        public static implicit operator Decision(Variable var)
        {
            return var.var;
        }

        #endregion

        #region explicit

        public static explicit operator Variable(Decision var)
        {
            return new Variable(var);
        }

        #endregion

        #region overrides

        public override string ToString()
        {
            return var.ToString();
        }

        public override int GetHashCode()
        {
            return var.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is Variable)
                return var.Equals(((Variable)obj).var);
            else if (obj is Decision)
                return var.Equals((Decision)obj);
            else
                return false;
        }

        #endregion

    }
}
