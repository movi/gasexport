﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SolverFoundation.Services;
using Optimizer;

namespace NLPSolverPlugin
{
    public class NLPModel
    {
        #region fields

        /// <summary>
        /// строковые выражения ограничений в инфиксном формате
        /// </summary>
        List<string> constraints;

        /// <summary>
        /// символьные обозначения типов ограничений (типизировано под фико-модуль)
        /// 'E' - уравнение
        /// 'G' - больше
        /// 'L' - меньше
        /// 'N' - без огр
        /// </summary>
        List<char> constrChars;

        /// <summary>
        /// названия ограничений
        /// </summary>
        List<string> constrNames;

        /// <summary>
        /// модель msf
        /// </summary>
        Model model;

        /// <summary>
        /// модель фико
        /// </summary>
        XNLPprob probe;

        /// <summary>
        /// список расширений переменных из decisionsList
        /// </summary>
        List<Variable> exDecisionsList;

        /// <summary>
        /// список переменных из msf
        /// </summary>
        List<Decision> decisionsList;

        /// <summary>
        /// символьные обозначения типов переменных (типизировано под фико-модуль)
        /// 'B' - бинарная
        /// 'I' - целочисленная
        /// 'C' - непрерывная
        /// </summary>
        List<char> decisionTypes;

        /// <summary>
        /// максимальные значения переменных
        /// </summary>
        List<double> decisionMins;

        /// <summary>
        /// минимальные значения переменных
        /// </summary>
        List<double> decisionMaxs;

        /// <summary>
        /// результаты расчета для переменных
        /// </summary>
        double[] solution;

        /// <summary>
        /// результирующее значение критерия
        /// </summary>
        double criteria;

        /// <summary>
        /// true если задача будет решенас помощью фико (директивы NLP и MINLP)
        /// false иначе
        /// </summary>
        bool isNLP = false;

        #endregion

        #region constructors

        public NLPModel(Model model, XNLPprob probe)
        {
            this.model = model;
            constraints = new List<string>();
            constrChars = new List<char>();
            constrNames = new List<string>();
            this.probe = probe;

            this.exDecisionsList = new List<Variable>();
            this.decisionsList = new List<Decision>();
            this.decisionTypes = new List<char>();
            this.decisionMins = new List<double>();
            this.decisionMaxs = new List<double>();
        }

        public NLPModel(Model model)
        {
            this.model = model;
            constraints = new List<string>();
            constrChars = new List<char>();
            constrNames = new List<string>();

            this.exDecisionsList = new List<Variable>();
            this.decisionsList = new List<Decision>();
            this.decisionTypes = new List<char>();
            this.decisionMins = new List<double>();
            this.decisionMaxs = new List<double>();
        }

        #endregion

        /// <summary>
        /// вызов солвера для задачи, соответстующего директиве
        /// </summary>
        /// <param name="directive">объект директивы</param>
        /// <param name="capability">тип директивы</param>
        /// <returns>true если найдено абсолютный или локальный оптимум</returns>
        public bool SolveCommon(Directive directive, SolverCapability capability)
        {
            bool success = false;

            if (capability == SolverCapability.NLP || capability == SolverCapability.MINLP)
            {
                success = this.Calculate();
            }
            else
            {
                SolverContext context = SolverContext.GetContext();
                Solution s = context.Solve(directive);
                success = (s.Quality == SolverQuality.LocalOptimal) || (s.Quality == SolverQuality.Optimal);
                
            }

            return success;
        }

        #region Variable Init

        /// <summary>
        /// непрерывная
        /// </summary>
        /// <param name="name"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public Variable AddRealDecision(string name, double min, double max)
        {
            Decision d = new Decision(Domain.RealRange(min, max), name);
            this.decisionsList.Add(d);
            this.decisionMaxs.Add(max);
            this.decisionMins.Add(min);
            this.decisionTypes.Add('C');
            Variable ex = new Variable(d);
            this.exDecisionsList.Add(ex);
            return ex;
        }
        public Variable AddRealDecision(double min, double max, string name)
        {
            return this.AddRealDecision(name, min, max);
        }

        /// <summary>
        /// булева
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Variable AddBooleanDecision(string name)
        {
            Decision d = new Decision(Domain.Boolean, name);
            this.decisionsList.Add(d);
            this.decisionMaxs.Add(0);
            this.decisionMins.Add(1);
            this.decisionTypes.Add('B');
            Variable ex = new Variable(d);
            this.exDecisionsList.Add(ex);
            return ex;
        }

        /// <summary>
        /// целочисленная
        /// </summary>
        /// <param name="name"></param>
        /// <param name="values">возможные значения переменный</param>
        /// <returns></returns>
        public Variable AddIntegerDecision(string name, params int[] values)
        {
            if (values == null || values.Length == 0)
                return null;

            Decision d = new Decision(Domain.Set(values), name);
            this.decisionsList.Add(d);
            this.decisionMaxs.Add(values[0]);
            this.decisionMins.Add(values[values.Length - 1]);
            this.decisionTypes.Add('I');
            Variable ex = new Variable(d);
            this.exDecisionsList.Add(ex);
            return ex;
        }

        #endregion

        #region Variable Model Filling

        /// <summary>
        /// Добавление в модель всех переменных из decisions
        /// </summary>
        /// <param name="decisions"></param>
        public void AddDecisions(params Decision[] decisions)
        {
            if (decisions.Length >= decisionsList.Count)
                this.AddDecisions();
            else
            {
                List<Decision> decisionsList1 = new List<Decision>();
                List<char> decisionTypes1 = new List<char>();
                List<double> decisionMins1 = new List<double>();
                List<double> decisionMaxs1 = new List<double>();
                List<Variable> exDecisionsList1 = new List<Variable>();

                for (int i = 0; i < decisions.Length; i++)
                {
                    int index = this.decisionsList.IndexOf(decisions[i]);
                    if (index == -1)
                        throw new Exception("Wrong Creation of Decision: " + decisions[i].Name);
                    decisionsList1.Add(decisionsList[index]);
                    decisionMaxs1.Add(decisionMaxs[index]);
                    decisionMins1.Add(decisionMins[index]);
                    decisionTypes1.Add(decisionTypes[index]);
                    exDecisionsList1.Add(exDecisionsList[index]);
                }

                this.decisionsList = decisionsList1;
                this.decisionMaxs = decisionMaxs1;
                this.decisionMins = decisionMins1;
                this.decisionTypes = decisionTypes1;
                this.exDecisionsList = exDecisionsList1;
            }
        }

        /// <summary>
        /// Добавление в модель всех переменных из decisions
        /// </summary>
        /// <param name="decisions"></param>
        public void AddDecisions(params Variable[] decisions)
        {
            if (decisions.Length >= decisionsList.Count)
                this.AddDecisions();
            else
            {
                List<Decision> decisionsList1 = new List<Decision>();
                List<char> decisionTypes1 = new List<char>();
                List<double> decisionMins1 = new List<double>();
                List<double> decisionMaxs1 = new List<double>();
                List<Variable> exDecisionsList1 = new List<Variable>();

                for (int i = 0; i < decisions.Length; i++)
                {
                    int index = this.decisionsList.IndexOf(decisions[i].Var);
                    if (index == -1)
                        throw new Exception("Wrong Creation of Decision: " + decisions[i].Var.Name);
                    decisionsList1.Add(decisionsList[index]);
                    decisionMaxs1.Add(decisionMaxs[index]);
                    decisionMins1.Add(decisionMins[index]);
                    decisionTypes1.Add(decisionTypes[index]);
                    exDecisionsList1.Add(exDecisionsList[index]);
                }

                this.decisionsList = decisionsList1;
                this.decisionMaxs = decisionMaxs1;
                this.decisionMins = decisionMins1;
                this.decisionTypes = decisionTypes1;
                this.exDecisionsList = exDecisionsList1;
            }
        }

        /// <summary>
        /// Добавление в модель всех объявленных переменных
        /// </summary>
        public void AddDecisions()
        {
            model.AddDecisions(decisionsList.ToArray());
        }

        #endregion

        #region Constraints Init

        public Constraint AddConstraint(string name, Term constraint)
        {
            return model.AddConstraint(name, constraint);
        }

        public Constraint AddConstraints(string name, params Term[] constraints)
        {
            return model.AddConstraints(name, constraints);
        }

        #endregion

        #region Criterium

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="direction">Maximize или Minimize - тип отпитимазции</param>
        /// <param name="goal">критерий</param>
        /// <returns>goal</returns>
        public Goal AddGoal(string name, GoalKind direction, Term goal)
        {
            return model.AddGoal(name, direction, goal);
        }

        public void RemoveGoals()
        {
            model.RemoveGoals(model.Goals.ToArray());
        }

        public void RemoveGoals(params Goal[] goals)
        {
            model.RemoveGoals(goals);
        }

        #endregion

        #region non public methods

        /// <summary>
        /// добавление строковых выражений для ограничений
        /// </summary>
        internal void CreateExpressions()
        {
            foreach (Constraint c in model.Constraints)
            {
                this.AddExpressionConstraint(c.Expression.Replace(" ","").Replace('[','(').Replace(']',')'));
                this.constrNames.Add(c.Name);
            }
        }

        /// <summary>
        /// создание переменных в модели фико
        /// </summary>
        internal void CreateVars()
        {
            int count = this.model.Decisions.Count();
            string[] names = new string[count];
            char[] types = new char[count];
            double[] lb = new double[count];
            double[] ub = new double[count];

            for (int i = 0; i < count; i++)
            {
                Decision d = decisionsList[i];
                names[i] = d.Name;
                types[i] = decisionTypes[i];
                lb[i] = decisionMins[i];
                ub[i] = decisionMaxs[i];
            }

            probe.CreateVars(names, types, lb, ub);

        }

        /// <summary>
        /// разбиение ограничения на левую и правую часть и тип ограничения
        /// </summary>
        /// <param name="expression">ограничение</param>
        private void AddExpressionConstraint(string expression)
        {
            StringBuilder res = new StringBuilder();
            int index = 0;
            int len = 0;
            char op = 'X';
            if (expression.Contains("=="))
            {
                index = expression.IndexOf("==");
                len = 2;
                op = 'E';
            }
            else if (expression.Contains("!="))
            {
                index = expression.IndexOf("!=");
                len = 2;
                op = 'N';
            }
            else if (expression.Contains(">="))
            {
                index = expression.IndexOf(">=");
                len = 2;
                op = 'G';
            }
            else if (expression.Contains("<="))
            {
                index = expression.IndexOf("<=");
                len = 2;
                op = 'L';
            }
            else if (expression.Contains(">"))
            {
                index = expression.IndexOf(">");
                len = 1;
                op = 'G';
            }
            else if (expression.Contains("<"))
            {
                index = expression.IndexOf("<");
                len = 1;
                op = 'L';
            }

            res.Append(expression.Substring(0, index));

            if (len > 1 || expression[index] != '0')
            {
                res.Append("-(");
                res.Append(expression.Substring(index + len));
                res.Append(")");
            }

            constraints.Add(res.ToString());
            constrChars.Add(op);
        }

        /// <summary>
        /// создание модели фико с параметрами, взятыми из модели msf
        /// </summary>
        internal void CreateNewProbe()
        {


            int sense = 0;
            GoalKind goalKind = model.Goals.FirstOrDefault().Kind;
            if (goalKind == GoalKind.Maximize)
                sense = 1;


            XNLPprob.Init();
            probe = new XNLPprob(sense);

            //probe.SetTolerance(XNLPConst.XNLP_OPTIMALITY_TOLERANCE, 1e-3);
            //probe.SetTolerance(XNLPConst.XNLP_INTEGRALITY_TOLERANCE, 1e-3);
            //probe.SetTolerance(XNLPConst.XNLP_FEASIBILITY_TOLERANCE, 1e-3);
        }

        /// <summary>
        /// создание ограничений в модели фико
        /// </summary>
        internal void CreateConstraints()
        {
            probe.CreateConstraints(constrNames.ToArray(), constrChars.ToArray());
        }

        /// <summary>
        /// парсим ограничение или критерий
        /// </summary>
        /// <param name="expr">ограничение или критерий</param>
        /// <param name="rowIndex">индекс ограничения, -1 если критерий</param>
        private void AddRow(string expr, int rowIndex)
        {
            StringBuilder sb = new StringBuilder(expr.Trim());
            string exp = sb.ToString();

            object[] parse = probe.ParseExpression(exp, exp.Length);
            int count = (int)parse[0];
            int[] tokenTypes = ((int[])parse[1]).Take(count).ToArray();
            double[] tokenValues = ((double[])parse[2]).Take(count).ToArray();

            try
            {
                probe.AddTermEquationPostfix(tokenTypes, tokenValues, rowIndex);
            }
            catch (Exception)
            {
                if (rowIndex == -1)
                    System.Diagnostics.Debug.WriteLine("Bad Criterium: " + this.model.Goals.First());
                else
                    System.Diagnostics.Debug.WriteLine("Bad Constraint: " + this.model.Constraints.ToList()[rowIndex].ToString());
            }
        }

        /// <summary>
        /// парсим ограничения
        /// </summary>
        internal void ParseConstraints()
        {
            for (int i = 0; i < constraints.Count; i++)
            {
                string expr = constraints[i];
                this.AddRow(expr, i);
            }
        }

        /// <summary>
        /// парсим критерий
        /// </summary>
        internal void ParseCriteria()
        {
            if (model.Goals.Any())
                this.AddRow(model.Goals.First().Expression, XNLPConst.XNLP_OBJECTIVE_ROW);
        }

        /// <summary>
        /// решение модели фико
        /// </summary>
        /// <returns>статус решения</returns>
        internal XNLPstatus Solve()
        {
            probe.SetSolver(XNLPConst.XNLP_SOLVER_OPTIMIZER);
            probe.SetCriteriumType(!model.Goals.Any() || model.Goals.First().Kind == GoalKind.Minimize);
            XNLPstatus status = probe.Optimize();
            return status;
        }

        /// <summary>
        /// запись решения в solution и criteria
        /// </summary>
        internal void GetSolution()
        {
            Decision[] decisions = model.Decisions.ToArray();
            solution = probe.GetSolution(decisions.Length);
            criteria = probe.GetCriteria();
            for (int i = 0; i < solution.Length; i++)
                exDecisionsList[i].SetSolution(solution[i]);
        }

        /// <summary>
        /// расчет
        /// в зависимости от типа директивы либо решателями msf либо xnlp
        /// </summary>
        /// <param name="filename">имя файла куда записать задачу если нужно</param>
        /// <returns>true если статус решения optimal или localOptimal</returns>
        internal bool Calculate(string filename = null)
        {
            this.SetNLP(true);
            this.CreateExpressions();
            this.CreateNewProbe();
            this.CreateVars();
            this.CreateConstraints();
            this.ParseConstraints();
            this.ParseCriteria();

            if (filename != null)
            {
                try
                {
                    probe.WriteProbe(filename);
                }
                catch (Exception) { }
            }

            XNLPstatus status = this.Solve();
            this.GetSolution();
            probe.Dispose();
            XNLPprob.Free();
            return (status == XNLPstatus.XNLP_STATUS_LOCALLY_OPTIMAL) || (status == XNLPstatus.XNLP_STATUS_OPTIMAL);
        }

        /// <summary>
        /// true если директивы NLP или MINLP
        /// </summary>
        /// <param name="isNLP"></param>
        private void SetNLP(bool isNLP)
        {
            this.isNLP = isNLP;
            foreach (Variable var in this.exDecisionsList)
            {
                var.IsNLP = isNLP;
            }
        }

        #endregion

        #region Terms Operations

        public static Term Product(params Term[] terms)
        {

            return Model.Product(terms);
        }

        public static Term Product(params object[] vars)
        {
            if (vars.Length == 0)
                return 0;

            if (vars.Length == 1 && vars[0] is Variable)
                return ((Variable)vars[0]).Var;

            Term var;
            if (vars[0] is Term)
            {
                var = (Term)vars[0];
                for (int i = 1; i < vars.Length; i++)
                {
                    if (vars[i] is Term)
                        var *= (Term)vars[i];
                    else if (vars[i] is Variable)
                        var *= (Variable)vars[i];
                }
            }
            else if (vars[0] is Variable)
            {
                var = ((Variable)vars[0]).Var;
                for (int i = 1; i < vars.Length; i++)
                {
                    if (vars[i] is Term)
                        var *= (Term)vars[i];
                    else if (vars[i] is Variable)
                        var *= (Variable)vars[i];
                }
            }
            else
                throw new Exception("Unknown Term Type");
            return var;
        }

        public static Term Sum(params Term[] terms)
        {
            //if (terms.Length == 0)
            //    return 0;

            //Term t = terms[0];
            //for (int i = 1; i < terms.Length; i++)
            //    t += terms[i];
            //return t;

            return Model.Sum(terms);
        }

        public static Term Sum(params object[] vars)
        {
            if (vars.Length == 0)
                return 0;

            if (vars.Length == 1 && vars[0] is Variable)
                return ((Variable)vars[0]).Var;

            Term var;
            if (vars[0] is Term)
            {
                var = (Term)vars[0];
                for (int i = 1; i < vars.Length; i++)
                {
                    if (vars[i] is Term)
                        var += (Term)vars[i];
                    else if (vars[i] is Variable)
                        var += (Variable)vars[i];
                }
            }
            else if (vars[0] is Variable)
            {
                var = ((Variable)vars[0]).Var;
                for (int i = 1; i < vars.Length; i++)
                {
                    if (vars[i] is Term)
                        var += (Term)vars[i];
                    else if (vars[i] is Variable)
                        var += (Variable)vars[i];
                }
            }
            else
                throw new Exception("Unknown Term Type");

            return var;
        }

        public static Term Power(Term v1, Term v2)
        {
            return Model.Power(v1 + 0, v2 + 0);
        }

        public static Term Power(Term v1, Variable v2)
        {
            return Model.Power(v1 + 0, v2.Var + 0);
        }

        public static Term Power(Variable v1, Term v2)
        {
            return Model.Power(v1.Var + 0, v2 + 0);
        }

        public static Term Power(Variable v1, Variable v2)
        {
            return Model.Power(v1.Var, v2.Var);
        }

        public static Term Exp(Variable v)
        {
            return Model.Exp(v.Var);
        }

        #endregion

        #region Getting results

        /// <summary>
        /// 
        /// </summary>
        /// <param name="d">переменная</param>
        /// <returns>результат расчета для переменной</returns>
        public double GetValue(Decision d)
        {
            if (isNLP)
            {
                int index = decisionsList.IndexOf(d);
                return solution[index];
            }
            else
                return d.ToDouble();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>результат расчета для критерия</returns>
        public double GetGoalValue()
        {
            if (isNLP)
            {
                return this.criteria;
            }
            else
                return this.model.Goals.FirstOrDefault().ToDouble();
        }

        #endregion
	}
}
