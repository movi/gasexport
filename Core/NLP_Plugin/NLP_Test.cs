﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SolverFoundation.Services;
using NLPSolverPlugin;

namespace NLP_Plugin
{
    public class NLP_Test
    {
        public static void Main()
        {
            SolverCapability solverCap = SolverCapability.NLP;
            
            SolverContext context = SolverContext.GetContext();
            context.ClearModel();
            var mpModel = new NLPModel(context.CreateModel());

            //sqrt(x^6y^6+10)/(x+y)^2
            var goal = mpModel.AddRealDecision(0, 1e6, "z");
            var x = mpModel.AddRealDecision(0, 2, "x");
            var y = mpModel.AddRealDecision(0, 2, "y");
            var u = mpModel.AddRealDecision(0.5, 0.5, "u");
            var u2 = mpModel.AddRealDecision(6, 6, "u2");
            mpModel.AddDecisions(goal, x, y, u, u2);

            Term t2 = NLPModel.Power(x + y, 2) + 1;
            Term t1 = NLPModel.Power(x, 6) * NLPModel.Power(y, 6) + 10.5;
            Term t3 = NLPModel.Power(t1, 0.5);
            Term t = t3/t2;

            mpModel.AddConstraint("eq", goal == t);
            mpModel.AddGoal("min", GoalKind.Minimize, goal);
            bool success = mpModel.SolveCommon(new MixedIntegerProgrammingDirective(), solverCap);
            double value = goal.ToDouble();
            double xVal = x.ToDouble();
            double yVal = y.ToDouble();
        }
    }
}
