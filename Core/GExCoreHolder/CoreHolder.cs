﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

using GEx.ErrorHandler;
using GEx.Domain;
using GEx.Iface;
using GEx.DataAccess;
using GEx.Calculate;
using GEx.Domain.DataPumps;

namespace GEx.CoreHolder
{
    /// <summary>
    /// IMB: Здесь храним все ядро (домен объектная модель коллекции с инстанцированными классами - обертками от уровня DAL
    /// Это сущность хранитель всего. Это фактически синглтон, живет в единственном экземпляре все время жизни проги.
    /// </summary>
    public class CoreHolder : IGExDomain
    {
        private GEx.ErrorHandler.IErrorOutput _errorHandler;

        private int _NewNodeId = -1;
        private int _newUGSscenarioNode = -1;
        private int _newRussianGasInputScenarioNode = -1;
        private int _newOutputScenarioNode = -1;
        private int _NewRelationId = -1;
        private int _NewInterconnectionId = -1;
        private int _newBorderScenarioPoint = -1;
        private int _newZoneId = -1;
        private bool _HasSchemaChanges = false;

        //IMB: Сохраняем колбек сразу при ините, в дальнейшем используем его для обратного вызова гуя
        private GEx.Iface.IGExDomainCallback _guiCallback;

        private ISchemaRepository _schemaRepo;       

        private IBalanceZoneRepository _balanceZoneRepo;

        private IScenarioRepository _scenarioRepo;

        private IGasExportObjectRepository _GasExportOBjectsRepo;

        //IMB: Public методов, кроме конструктора (неизбежное зло, ведь его надо создавать в том месте, где он хранится)
        //методов интерфейсов, здесь быть не должно. Использовать internal.

        #region Коллекции с инстанцированными классами - обертками от уровня DAL

        private Schema _scheme;

        private CalculationScenario _scenario;  //текущий сценарий. В морде на табах отображается содеожимое только этого сценария.

        private List<BalanceZone> _balanceZones;


       

        //IMB: Результаты расчета тоже храним здесь. Передаем только референс для заполнения в другие сборки
        private GExBaseDocResult _calcResult;

        /// <summary>
        /// Первые точки маршрута
        /// </summary>
        private List<Node> _firstPathNodes = new List<Node>();

        /// <summary>
        /// Конечные точки маршрута
        /// </summary>
        private List<Node> _lastPathNodes = new List<Node>();
        #endregion

        public CoreHolder()
        {
           // _balanceZones = new List<BalanceZone>();

            Mode = ModeWork.Multi;
        }

        #region IGExDomain Impl

        /// <summary>
        /// IMB: coreHolder хранится как член-данные в запускающей форме. 
        /// </summary>
        /// <param name="eh"></param>
        void IGExDomain.Init(IErrorOutput eh, GEx.Iface.IGExDomainCallback cbk)
        {
            const string errorSource = "CoreHolder::Init()";
            _errorHandler = eh;
            _guiCallback = cbk;
            try
            {
                

                _schemaRepo = new GEx.DataAccess.SchemaRepository();
                _schemaRepo.Init(eh);

                _balanceZoneRepo = new BalanceZoneRepository();
                _balanceZoneRepo.Init(eh);
                _balanceZones = new List<BalanceZone>();
                _balanceZones.AddRange(_balanceZoneRepo.LoadAll());

                _scenarioRepo = new ScenarioRepository();
                _scenarioRepo.Init(eh);
                


                _GasExportOBjectsRepo = new GazExportObjectRepository();
                _GasExportOBjectsRepo.Init(eh);

                _errorHandler.ReportError(eErrClass.EC_INFO, "Init complete.", errorSource);
                // _fullRep.Init(eh);
            }
            //ошибка доступа на этапе инициализации
            catch (SqlException ex)
            {
                //TODO: разграничить по ex.Number
                //т.к. обязательно может свалиться еще какой SqlException (таблица не найдена, поле отсутствует) здесь же
                string msg = "Init error: DB Access failed.\n" + 
                    "Sql Exception Number: " + ex.Number + "\n" +
                    ex.Source + ": " + ex.Message + "\n" +
                             ex.InnerException + "\n" +
                             ex.StackTrace;

                //весь стектрейс пишем здесь, дальше можно подробно в лог не писать
                _errorHandler.ReportError(eErrClass.EC_FATAL, msg, errorSource);
                
                //ошибка доступа на старте, пока считаем, что все ошибки здесь - ошибка доступа к базе
                throw new Exceptions.DBAccessFatalException(ex.Message);
            }

        }

        #region Schema

        bool IGExDomain.HasSchemaChanges()
        {
            return _HasSchemaChanges;
        }

        void IGExDomain.CreateNewSchema(bool useBasic)
        {
            const string errorSource = "CoreHolder::CreateNewSchema()";
            var scheme = new Schema
            {
                Name = "Новая схема",
                CreationDate = DateTime.Now,
                Nodes = new List<Node>(),
                Author = Environment.UserDomainName + "\\" + Environment.UserName
            };

            if (useBasic)
            {
                foreach (var node in _scheme.Nodes)
                    scheme.Nodes.Add(node);
            }
            _scenario = null;
            _calcResult = null;
            _scheme = scheme;
            //_balanceZones.AddRange(_balanceZoneRepo.LoadAll());
            _guiCallback.OnSchemaInit();
            _errorHandler.ReportError(eErrClass.EC_INFO, "Создана схема '" + scheme.Name + "'.", errorSource);
            _HasSchemaChanges = false;
        }

        IEnumerable<DTO_Schema> IGExDomain.LoadSchemas()
        {
            const string errorSource = "CoreHolder::LoadSchemas()";
            var schemas = _schemaRepo.LoadAll();
            var schemasDto = new List<DTO_Schema>();
            foreach(var schema in schemas)
            {
                schemasDto.Add(GetSchemaDto(schema));
            }
            _errorHandler.ReportError(eErrClass.EC_INFO, "Список схем получен.", errorSource);
            return schemasDto.ToArray();
        }

        void IGExDomain.LoadSchema(int schemaID)
        {
            const string errorSource = "CoreHolder::LoadSchema()";

            _scheme = _schemaRepo.Load(schemaID);
            //var schemaZones = _scheme.GetZones();
            //foreach(var schemaZone in schemaZones)
            //{
            //    var zone = _balanceZones.Find(z => z.ID == schemaZone.ID);
            //    if (zone != null)
            //    {
            //        _balanceZones.Remove(zone);
            //        _balanceZones.Add(schemaZone);
            //    }
            //}
            
            _calcResult = null;
            _scenario = null;
            _guiCallback.OnSchemaInit();
            _errorHandler.ReportError(eErrClass.EC_INFO, string.Format("схема '{0}'(Id = {1}) и операторы загружены", _scheme.Name, _scheme.ID), errorSource);
            _HasSchemaChanges = false;
        }

        //Удалить после перехода на SchemaNodes
        //Schema IGExDomain.GetSchema()
        //{
        //    return _scheme;
        //}

        bool IGExDomain.IsSchemaLoaded()
        {
            return null != _scheme;
        }

        DTO_Schema IGExDomain.GetSchema()
        {
            return GetSchemaDto(_scheme);
        }

        IEnumerable<DTO_Node> IGExDomain.GetSchemaNodes()
        {
            var nodesDto = new List<DTO_Node>();
            foreach (var node in _scheme.Nodes)
            {
                nodesDto.Add(GetNodeDto(node));
            }
            return nodesDto.ToArray();
        }

        void IGExDomain.SaveSchemaAsNew(byte[] mapUnderlayer)
        {
            const string errorSource = "CoreHolder::SaveAsNew()";
            try
            {
                UpdateBalanceZones();
                _guiCallback.BeforeNewSchemaSave(GetSchemaDto(_scheme));
                var nodeAndIdBeforeSaveArr = _scheme.Nodes.Select(node => new { Node = node, IdBeforeSave = node.ID }).ToArray();
                int schemaTempId = _scheme.ID;
                _scheme.CreationDate = DateTime.Now;
                _scheme.Author = Environment.UserDomainName + "\\" + Environment.UserName;
                _scheme.SchemaImage.Image = mapUnderlayer;
                _schemaRepo.Create(_scheme);
                _guiCallback.OnSchemaSaved(new SavedEntityInfo()
                    {
                        IdAfterSave = _scheme.ID,
                        IdBeforeSave = schemaTempId
                    }, nodeAndIdBeforeSaveArr.Select(
                    nodeAndId => new SavedEntityInfo()
                    {
                        IdAfterSave = nodeAndId.Node.ID,
                        IdBeforeSave = nodeAndId.IdBeforeSave
                    }).ToArray());
                _errorHandler.ReportError(eErrClass.EC_INFO, "Cхема  '" + _scheme.Name + "' cохранена  как новая.", errorSource);
                _HasSchemaChanges = false;
            }
            catch (Exception ex)
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, "Ошибка при сохранении схемы   '" + _scheme.Name + "'. " + ex.Message, errorSource);
                throw;
            }
        }

        void IGExDomain.SaveSchema(byte[] mapUnderlayer)
        {
            const string errorSource = "CoreHolder::SaveSchema()";
            try
            {

                var sw1 = new System.Diagnostics.Stopwatch();
                sw1.Start();
                UpdateBalanceZones();
                sw1.Stop();
                _errorHandler.ReportError(eErrClass.EC_TRACE, string.Format("UpdateBalanceZones {0}", sw1.Elapsed.TotalMilliseconds), "CoreHolder.SaveSchema()");


                var sw2 = new System.Diagnostics.Stopwatch();
                sw2.Start();

                var nodeAndIdBeforeSaveArr = _scheme.Nodes.Select(node => new { Node = node, IdBeforeSave = node.ID }).ToArray();
                int schemaTempId = _scheme.ID;                
                _scheme.CreationDate = DateTime.Now;
                _scheme.Author = Environment.UserDomainName + "\\" + Environment.UserName;
                _scheme.SchemaImage.Image = mapUnderlayer;
                _schemaRepo.Update(_scheme);

                sw2.Stop();
                _errorHandler.ReportError(eErrClass.EC_TRACE, string.Format("UpdateSchema {0}", sw2.Elapsed.TotalMilliseconds), "CoreHolder.SaveSchema()");

                var sw3 = new System.Diagnostics.Stopwatch();
                sw3.Start();

                _guiCallback.OnSchemaSaved(new SavedEntityInfo()
                {
                    IdAfterSave = _scheme.ID,
                    IdBeforeSave = schemaTempId
                }, nodeAndIdBeforeSaveArr.Select(
                    nodeAndId => new SavedEntityInfo()
                    {
                        IdAfterSave = nodeAndId.Node.ID,
                        IdBeforeSave = nodeAndId.IdBeforeSave
                    }).ToArray());

                sw3.Stop();
                _errorHandler.ReportError(eErrClass.EC_TRACE, string.Format("After UpdateSchema {0}", sw3.Elapsed.TotalMilliseconds), "CoreHolder.SaveSchema()");

                _errorHandler.ReportError(eErrClass.EC_INFO, "Схема   '" + _scheme.Name + "' сохранена.", errorSource);
                _HasSchemaChanges = false;
            }
            catch (Exception ex)
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, "Ошибка при сохранении схемы   '" + _scheme.Name + "'. " + ex.Message, errorSource);
                throw;
            }
            
        }

        void IGExDomain.UpdateGazExportObjectsZones()
        {
            _GasExportOBjectsRepo.Update(_scheme.Nodes.Select(n => n.GasExportObject));
        }

        public void UpdateRelations(){
            UpdateRelationsCommand updeRelationsCommand = new UpdateRelationsCommand();
            updeRelationsCommand.Update(_scheme);
        }

        void IGExDomain.UpdateSchemaName(int schemaId, string newName)
        {
            const string errorSource = "CoreHolder::UpdateSchemaName()";
            try
            {
                
               // var nodeAndIdBeforeSaveArr = _scheme.Nodes.Select(node => new { Node = node, IdBeforeSave = node.ID }).ToArray();
               // int schemaTempId = _scheme.ID;
                var schemaForUpdate =_schemaRepo.Load(schemaId);
                schemaForUpdate.Name = newName;
                _schemaRepo.Update(schemaForUpdate);
                _guiCallback.OnSchemaSaved(new SavedEntityInfo()
                {
                    IdAfterSave = schemaId,
                    IdBeforeSave = schemaId
                },new SavedEntityInfo [0]);
                _errorHandler.ReportError(eErrClass.EC_INFO, String.Format("Имя схемы Id = {0} изменено на '{1}'.", schemaId, newName), errorSource);

            }
            catch (Exception ex)
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, "Ошибка при изменении имени схемы Id = " + schemaId + " на '" + newName + "'. " + ex.Message, errorSource);
                throw;
            }
        }

        void IGExDomain.UpdateSchemaComment(int schemaId, string newComment)
        {
            const string errorSource = "CoreHolder::UpdateSchemaComment()";
            try
            {

                var schemaForUpdate = _schemaRepo.Load(schemaId);
                schemaForUpdate.Comment = newComment;
                _schemaRepo.Update(schemaForUpdate);
                _guiCallback.OnSchemaSaved(new SavedEntityInfo()
                {
                    IdAfterSave = schemaId,
                    IdBeforeSave = schemaId
                }, new SavedEntityInfo[0]);
                _errorHandler.ReportError(eErrClass.EC_INFO, String.Format("Комментарий схемы Id = {0} изменено на '{1}'.", schemaId, newComment), errorSource);

            }
            catch (Exception ex)
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, "Ошибка при изменении комментария схемы Id = " + schemaId + " на '" + newComment + "'. " + ex.Message, errorSource);
                throw;
            }
        }

        void IGExDomain.UpdateSchema(DTO_Schema schema)
        {
            const string errorSource = "CoreHolder::UpdateSchema()";
            try
            {
                if (_scheme != null)
                {
                    _scheme.Name = schema.SchemaName;
                    _scheme.Comment = schema.Comment;
                    _scheme.CreationDate = schema.CreationDate;

                    if (_scheme.SchemaImage==null)
                        _scheme.SchemaImage = new SchemaImage {Image = schema.Image};
                    else
                        _scheme.SchemaImage.Image = schema.Image;

                    _scheme.Author = schema.Author;
                }                
            }
            catch (Exception ex)
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, "Ошибка при изменении текущей схемы" + ex.Message, errorSource);
                throw;
            }
        }

        void IGExDomain.RemoveSchema(int Id)
        {
            const string errorSource = "CoreHolder::RemoveSchema()";
            try
            {
                _schemaRepo.Remove(Id);
                _errorHandler.ReportError(eErrClass.EC_INFO, String.Format("Схема Id = {0} удалена.", Id), errorSource);
            }
            catch (Exception ex)
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, "Ошибка при удалении схемы Id = " + Id + "." + ex.Message, errorSource);
                throw;
            }
        }

        void UpdateBalanceZones()
        {
            foreach(var zone in _balanceZones)
            {
                if(zone.ID <= 0)
                {
                    _balanceZoneRepo.Create(zone);
                }
            }
        }

        #endregion
        
        //void IGExDomain.AddNode(Node node)
        //{
        //    const string errorSource = "CoreHolder::AddNode()";
        //    _scheme.Nodes.Add(node);
        //    _errorHandler.ReportError(eErrClass.EC_INFO, "Добавлен новый узел '"+ node.ToString() +"'.", errorSource);
        //}

        int IGExDomain.AddNode(DTO_Node node,bool callBack)
        {
            Node newNode = new Node();
            newNode.X = node.X;
            newNode.Y = node.Y;

            newNode.LabelX = node.LabelX;
            newNode.LabelY = node.LabelY;

          //  newNode.Schema = _scheme;
            newNode.ID = _NewNodeId--;

            GasExportObject newGexObj = null;

            switch (node.Type)
            {
                case DTO_Node_type.Output:
                    {
                        newGexObj = new Output();
                        
                        //_scenario.OutoutScenarioDataSet.Add(new OutputScenarioData()
                        //    {
                        //        Id = newNode.ID,
                        //        Obj = (Output)newGexObj,
                        //        ContractVolume = null,
                        //        Scenario = _scenario
                        //    }
                        //    );
                        break;
                    }

                case DTO_Node_type.RussianGasInput:
                    {
                        newGexObj = new RussianGasInput();
                        /*
                        _scenario.RussianGasInputScenarioDataSet.Add(new RussianGasInputScenarioData()
                        {
                            Id = newNode.ID,
                            Obj = (RussianGasInput)newGexObj,
                            MaxExportVolume = null,
                            Scenario = _scenario
                        }
                        );*/
                        break;
                    }

                case DTO_Node_type.UGS:
                    {
                        newGexObj = new UGS();
                        /*
                        _scenario.UGSScenarioDataSet.Add(new UGSScenarioData()
                        {
                            Id = newNode.ID,
                            Obj = (UGS)newGexObj,
                            MaxUGSWithdrawal = null,
                            Scenario = _scenario
                        }
                        );*/
                        break;
                    }

                case DTO_Node_type.Border:
                    {
                        newGexObj = new Border();
                        /*
                        _scenario.OutputScenarioDataSet.Add(new OutputScenarioData()
                        {
                            Id = newNode.ID,
                            Obj = (Border)newGexObj,
                            ContractVolume = null,
                            Scenario = _scenario
                        }
                            );
                        */
                        break;
                    }
                case DTO_Node_type.VirtualTradingPoint:
                    {
                        newGexObj = new VTP();
                        /*
                        _scenario.OutputScenarioDataSet.Add(new OutputScenarioData()
                        {
                            Id = newNode.ID,
                            Obj = (Border)newGexObj,
                            ContractVolume = null,
                            Scenario = _scenario
                        }
                            );
                        */
                        break;
                    }
                
                    
            }
            newGexObj.ID = newNode.ID;
            newGexObj.ID = newNode.ID;
            newGexObj.Name = node.Name;
            newNode.GasExportObject = newGexObj;
                
            
            //newNode.ID = _NewNodeId++;
            _scheme.Nodes.Add(newNode);
            if (callBack)
                _guiCallback.OnNodeAdded();

            _HasSchemaChanges = true;
            return newNode.ID;


            //private Node CreateNodeFromType(GasExportObjectType type)
            //{
            //    Node n = new Node();

            //    GasExportObject gexObj = null;
            //    if (type == GasExportObjectType.Output)
            //        gexObj = new Output();

            //    if (type == GasExportObjectType.RussianGasInput)
            //        gexObj = new RussianGasInput();

            //    if (type == GasExportObjectType.UGS)
            //        gexObj = new UGS();

            //    if (type == GasExportObjectType.Border)
            //        gexObj = new Border();

            //    n.GasExportObject = gexObj;
            //    return n;
            //}
            
        }

        void IGExDomain.UpdateNode(DTO_Node curNode)
        {
          if (curNode == null)
                return;
          var origNode =  _scheme.Nodes.FirstOrDefault(n => n.ID == curNode.ID);
          if (origNode == null)
              return;
          origNode.X = curNode.X;
          origNode.Y = curNode.Y;
          origNode.LabelX = curNode.LabelX;
          origNode.LabelY = curNode.LabelY;
          
          if (origNode.GasExportObject != null)
          {
              origNode.GasExportObject.Name = curNode.Name;
              if(origNode.GasExportObject is IOneZoneNode)
              {
                  var oneZoneObj = origNode.GasExportObject as IOneZoneNode;
                  oneZoneObj.Zone = _balanceZones.FirstOrDefault(z => z.ID == curNode.ZoneId);                  
              }
              if(origNode.GasExportObject is Border)
              {
                  //(origNode.GasExportObject as Border).IsOutput = node.IsOutput;
                  var origBorder = origNode.GasExportObject as Border;
                  origBorder.IsOutput = curNode.IsOutput;
                  if (_scenario != null)
                  {
                      var tmpOutput = _scenario.OutputScenarioDataSet.FirstOrDefault(e => e.Obj.ID == origBorder.ID);
                      if (tmpOutput != null && !tmpOutput.isLoaded) _scenario.OutputScenarioDataSet.Remove(tmpOutput);
                  }

                  List<int> IdsOfInterconnectionsForRemove = new List<int>();
                  foreach (var origCon in origBorder.Interconnections)
                  {
                      if (!curNode.InterConnections.Any(i => i.ZoneTo.ID == origCon.ZoneTo.ID &&
                                                                i.ZoneFrom.ID == origCon.ZoneFrom.ID))
                          IdsOfInterconnectionsForRemove.Add(origCon.Id);
                  }
                  IdsOfInterconnectionsForRemove.ForEach(id =>
                      {
                          
                          //---------------
                          var connectionForRemove = origBorder.Interconnections.FirstOrDefault(i => i.Id == id);
                          var oId = connectionForRemove.BorderId;
                          var z1 = connectionForRemove.ZoneTo.ID;
                          var z2 = connectionForRemove.ZoneFrom.ID;
                          origBorder.Interconnections.Remove(connectionForRemove);
                          var relationIdsForRemove = new List<int>();
                          if (!origBorder.Interconnections.Any( x => x.BorderId == oId && (x.ZoneTo.ID == z1 || x.ZoneFrom.ID == z1)))
                          {
                               var relationIdsForRemved = _scheme.Relations.Where(x => (x.NodeFirst.GasExportObject.ID == oId
                                                                                    || x.NodeSecond.GasExportObject.ID == oId) && x.BalanceZone.ID == z1).Select(x => x.ID).ToArray();
                               relationIdsForRemove.AddRange(relationIdsForRemved);

                          }
                          if (!origBorder.Interconnections.Any(x => x.BorderId== oId && (x.ZoneTo.ID == z2 || x.ZoneFrom.ID == z2)))
                          {
                              var relationIdsForRemved =
                                  _scheme.Relations.Where(x => (x.NodeFirst.GasExportObject.ID == oId
                                                                || x.NodeSecond.GasExportObject.ID == oId) &&
                                                               x.BalanceZone.ID == z2).Select(x => x.ID).ToList();
                              relationIdsForRemove.AddRange(relationIdsForRemved);
                          }

                          relationIdsForRemove.ForEach(relId =>
                          {
                              var rel = _scheme.Relations.FirstOrDefault(x => x.ID == relId);
                              if (rel != null)
                                  _scheme.Relations.Remove(rel);
                          });

                      });

                  
                  foreach (var con in curNode.InterConnections)
                  {
                      if (!origBorder.Interconnections.Any(i => i.ZoneTo.ID == con.ZoneTo.ID &&
                                                                i.ZoneFrom.ID == con.ZoneFrom.ID))
                      {
                          BorderInterconnection newInterconnection = new BorderInterconnection();
                          newInterconnection.BorderId = origBorder.ID;
                          newInterconnection.Id =_NewInterconnectionId--;
                          if(con.ZoneFrom!=null)
                            newInterconnection.ZoneFrom = _balanceZones.FirstOrDefault(z => z.ID == con.ZoneFrom.ID);
                          if (con.ZoneTo != null)
                              newInterconnection.ZoneTo = _balanceZones.FirstOrDefault(z => z.ID == con.ZoneTo.ID);
                          origBorder.Interconnections.Add(newInterconnection);
                          //--------------
                          
                         
                          //--------------
                      }
                  }

                  
              }
          }
          _HasSchemaChanges = true;
          _guiCallback.OnNodeUpdated(curNode);
        }




        void IGExDomain.AddInterconnection(DTO_BorderInterconnection interconnection, int nodeId)
        {
          var origNode =  _scheme.Nodes.FirstOrDefault(n => n.ID == nodeId);
          if (origNode == null)
              return;

          if (origNode.GasExportObject != null)
          {
              if(origNode.GasExportObject is Border)
              {

                  var origBorder = origNode.GasExportObject as Border;
                  BorderInterconnection newInterconnection = new BorderInterconnection();
                  newInterconnection.Id =_NewInterconnectionId--;
                  if(interconnection.ZoneFrom!=null)
                      newInterconnection.ZoneFrom = _balanceZones.FirstOrDefault(z => z.ID == interconnection.ZoneFrom.ID);
                  if(interconnection.ZoneTo != null)
                      newInterconnection.ZoneTo = _balanceZones.FirstOrDefault(z => z.ID == interconnection.ZoneTo.ID);


                  foreach(Node node in _scheme.Nodes)
                  {
                      if(node!=origNode)
                      {
                         if(! _scheme.Relations.Any(rel=>rel.NodeFirst.ID ==node.ID && rel.NodeSecond.ID == origNode.ID && rel.BalanceZone.ID==interconnection.ZoneFrom.ID))
                         {
                             NodesRelation relation = new NodesRelation();
                             relation.NodeFirst = node;
                             relation.NodeSecond = origNode;
                             relation.BalanceZone = newInterconnection.ZoneFrom;
                             _scheme.Relations.Add(relation);
                         }

                         if (!_scheme.Relations.Any(rel => rel.NodeFirst.ID == origNode.ID && rel.NodeSecond.ID == node.ID && rel.BalanceZone.ID == interconnection.ZoneFrom.ID))
                         {
                             NodesRelation relation = new NodesRelation();
                             relation.NodeFirst = origNode;
                             relation.NodeSecond = node;
                             relation.BalanceZone = newInterconnection.ZoneFrom;
                             _scheme.Relations.Add(relation);
                         }

                         if (!_scheme.Relations.Any(rel => rel.NodeFirst.ID == node.ID && rel.NodeSecond.ID == origNode.ID && rel.BalanceZone.ID == interconnection.ZoneTo.ID))
                         {
                             NodesRelation relation = new NodesRelation();
                             relation.NodeFirst = node;
                             relation.NodeSecond = origNode;
                             relation.BalanceZone = newInterconnection.ZoneTo;
                             _scheme.Relations.Add(relation);
                         }

                         if (!_scheme.Relations.Any(rel => rel.NodeFirst.ID == origNode.ID && rel.NodeSecond.ID == node.ID && rel.BalanceZone.ID == interconnection.ZoneTo.ID))
                         {
                             NodesRelation relation = new NodesRelation();
                             relation.NodeFirst = origNode;
                             relation.NodeSecond = node;
                             relation.BalanceZone = newInterconnection.ZoneTo;
                             _scheme.Relations.Add(relation);
                         }

                      }
                  }
                  
                  origBorder.Interconnections.Add(newInterconnection);

              }
          }
          _HasSchemaChanges = true;
         // _guiCallback.OnNodeUpdated(curNode);
        }

        void IGExDomain.UpdateInterconnection(DTO_BorderInterconnection interconnection, int nodeId)
     {
          var origNode =  _scheme.Nodes.FirstOrDefault(n => n.ID == nodeId);
          if (origNode == null)
              return;

          if (origNode.GasExportObject != null)
          {
              if(origNode.GasExportObject is Border)
              {

                  var origBorder = origNode.GasExportObject as Border;
                  var origInterconnection = origBorder.Interconnections.FirstOrDefault(i => i.Id == interconnection.Id);
                  if (interconnection.ZoneFrom != null)
                      origInterconnection.ZoneFrom = _balanceZones.FirstOrDefault(z => z.ID == interconnection.ZoneFrom.ID);
                  if (interconnection.ZoneTo != null)
                      origInterconnection.ZoneTo = _balanceZones.FirstOrDefault(z => z.ID == interconnection.ZoneTo.ID);

                  foreach (Node node in _scheme.Nodes)
                  {
                      if (node != origNode)
                      {
                          if (!_scheme.Relations.Any(rel => rel.NodeFirst.ID == node.ID && rel.NodeSecond.ID == origNode.ID && rel.BalanceZone.ID == origInterconnection.ZoneFrom.ID))
                          {
                              NodesRelation relation = new NodesRelation();
                              relation.NodeFirst = node;
                              relation.NodeSecond = origNode;
                              relation.BalanceZone = origInterconnection.ZoneFrom;
                              _scheme.Relations.Add(relation);
                          }

                          if (!_scheme.Relations.Any(rel => rel.NodeFirst.ID == origNode.ID && rel.NodeSecond.ID == node.ID && rel.BalanceZone.ID == origInterconnection.ZoneFrom.ID))
                          {
                              NodesRelation relation = new NodesRelation();
                              relation.NodeFirst = origNode;
                              relation.NodeSecond = node;
                              relation.BalanceZone = origInterconnection.ZoneFrom;
                              _scheme.Relations.Add(relation);
                          }

                          if (!_scheme.Relations.Any(rel => rel.NodeFirst.ID == node.ID && rel.NodeSecond.ID == origNode.ID && rel.BalanceZone.ID == origInterconnection.ZoneTo.ID))
                          {
                              NodesRelation relation = new NodesRelation();
                              relation.NodeFirst = node;
                              relation.NodeSecond = origNode;
                              relation.BalanceZone = origInterconnection.ZoneTo;
                              _scheme.Relations.Add(relation);
                          }

                          if (!_scheme.Relations.Any(rel => rel.NodeFirst.ID == origNode.ID && rel.NodeSecond.ID == node.ID && rel.BalanceZone.ID == origInterconnection.ZoneTo.ID))
                          {
                              NodesRelation relation = new NodesRelation();
                              relation.NodeFirst = origNode;
                              relation.NodeSecond = node;
                              relation.BalanceZone = origInterconnection.ZoneTo;
                              _scheme.Relations.Add(relation);
                          }

                      }
                  }
              }
          }
          _HasSchemaChanges = true;
        }   
     
        void IGExDomain.RemoveInterconnection(int id,int nodeId)
        {
            var origNode = _scheme.Nodes.FirstOrDefault(n => n.ID == nodeId);
            if (origNode == null)
                return;

            if (origNode.GasExportObject != null)
            {
                if (origNode.GasExportObject is Border)
                {

                    var origBorder = origNode.GasExportObject as Border;
                    var connectionForRemove = origBorder.Interconnections.FirstOrDefault(i => i.Id == id);
                    origBorder.Interconnections.Remove(connectionForRemove);


                    foreach (Node node in _scheme.Nodes)
                    {
                        if (node != origNode)
                        {
                            if (!_scheme.Relations.Any(rel => rel.NodeFirst.ID == node.ID && rel.NodeSecond.ID == origNode.ID && rel.BalanceZone.ID == connectionForRemove.ZoneFrom.ID))
                            {
                               var relForRemove = _scheme.Relations.FirstOrDefault(rel => rel.NodeFirst.ID == node.ID && rel.NodeSecond.ID == origNode.ID && rel.BalanceZone.ID == connectionForRemove.ZoneFrom.ID);
                               _scheme.Relations.Remove(relForRemove);
                               
                            }

                            if (!_scheme.Relations.Any(rel => rel.NodeFirst.ID == origNode.ID && rel.NodeSecond.ID == node.ID && rel.BalanceZone.ID == connectionForRemove.ZoneFrom.ID))
                            {
                                var relForRemove = _scheme.Relations.FirstOrDefault(rel => rel.NodeFirst.ID == origNode.ID && rel.NodeSecond.ID == node.ID && rel.BalanceZone.ID == connectionForRemove.ZoneFrom.ID);
                                _scheme.Relations.Remove(relForRemove);
                            }

                            if (!_scheme.Relations.Any(rel => rel.NodeFirst.ID == node.ID && rel.NodeSecond.ID == origNode.ID && rel.BalanceZone.ID == connectionForRemove.ZoneTo.ID))
                            {
                                var relForRemove = _scheme.Relations.FirstOrDefault(rel => rel.NodeFirst.ID == node.ID && rel.NodeSecond.ID == origNode.ID && rel.BalanceZone.ID == connectionForRemove.ZoneTo.ID);
                                _scheme.Relations.Remove(relForRemove);
                            }

                            if (!_scheme.Relations.Any(rel => rel.NodeFirst.ID == origNode.ID && rel.NodeSecond.ID == node.ID && rel.BalanceZone.ID == connectionForRemove.ZoneTo.ID))
                            {
                                var relForRemove = _scheme.Relations.FirstOrDefault(rel => rel.NodeFirst.ID == origNode.ID && rel.NodeSecond.ID == node.ID && rel.BalanceZone.ID == connectionForRemove.ZoneTo.ID);
                                _scheme.Relations.Remove(relForRemove);
                            }

                        }
                    }
                }
            }
            _HasSchemaChanges = true;
        }

        void IGExDomain.SetBalanceZoneToNode(DTO_BalanceZone zone, int nodeId)
        {
            var origNode = _scheme.Nodes.FirstOrDefault(n => n.ID == nodeId);
            if (origNode == null)
                return;

            if (origNode.GasExportObject != null)
            {
                if (origNode.GasExportObject is IOneZoneNode)
                {
                    var oneZoneObj = origNode.GasExportObject as IOneZoneNode;
                    oneZoneObj.Zone = _balanceZones.FirstOrDefault(z => z.ID == zone.ID);

                    foreach (Node node in _scheme.Nodes)
                    {
                        if (node != origNode)
                        {
                            if (!_scheme.Relations.Any(rel => rel.NodeFirst.ID == node.ID && rel.NodeSecond.ID == origNode.ID && rel.BalanceZone.ID == oneZoneObj.Zone.ID))
                            {
                                var relForRemove = _scheme.Relations.FirstOrDefault(rel => rel.NodeFirst.ID == node.ID && rel.NodeSecond.ID == origNode.ID && rel.BalanceZone.ID == oneZoneObj.Zone.ID);
                                _scheme.Relations.Remove(relForRemove);

                            }

                            if (!_scheme.Relations.Any(rel => rel.NodeFirst.ID == origNode.ID && rel.NodeSecond.ID == node.ID && rel.BalanceZone.ID == oneZoneObj.Zone.ID))
                            {
                                var relForRemove = _scheme.Relations.FirstOrDefault(rel => rel.NodeFirst.ID == origNode.ID && rel.NodeSecond.ID == node.ID && rel.BalanceZone.ID == oneZoneObj.Zone.ID);
                                _scheme.Relations.Remove(relForRemove);
                            }
                        }
                    }


                }
            }
            _HasSchemaChanges = true;
        }

        //private void AddDafaultRelations(Node node)
        //{

        //}

        void IGExDomain.RemoveNode(DTO_Node node)
        {
            var origNode = _scheme.Nodes.FirstOrDefault(n => n.ID == node.ID);
            if (origNode != null)
            {
                if (_scenario != null)
                {
                    var tmpRGI = _scenario.RussianGasInputScenarioDataSet.FirstOrDefault(e => e.Obj.ID == origNode.GasExportObject.ID);
                    if (tmpRGI != null && !tmpRGI.isLoaded) _scenario.RussianGasInputScenarioDataSet.Remove(tmpRGI);

                    var tmpUGS = _scenario.UGSScenarioDataSet.FirstOrDefault(e => e.Obj.ID == origNode.GasExportObject.ID);
                    if (tmpUGS != null && !tmpUGS.isLoaded) _scenario.UGSScenarioDataSet.Remove(tmpUGS);

                    var tmpOutput = _scenario.OutputScenarioDataSet.FirstOrDefault(e => e.Obj.ID == origNode.GasExportObject.ID);
                    if (tmpOutput != null && !tmpOutput.isLoaded) _scenario.OutputScenarioDataSet.Remove(tmpOutput);

                    var Borders = _scenario.BorderScenarioDataSet.FindAll(e => e.GasExportObject.ID == origNode.GasExportObject.ID);
                    foreach (var tmpBorder in Borders)
                    {
                        if (!tmpBorder.isLoaded) _scenario.BorderScenarioDataSet.Remove(tmpBorder);
                    }
                }
                _scheme.Nodes.Remove(origNode);
                var idsOfRelationsForRemove = _scheme.Relations.Where(rel => rel.NodeFirst.ID == origNode.ID || rel.NodeSecond.ID == origNode.ID).Select(rel=>rel.ID).ToArray();
                foreach(var id in idsOfRelationsForRemove)
                {
                  var relForRemove =  _scheme.Relations.FirstOrDefault(rel => rel.ID == id);
                  _scheme.Relations.Remove(relForRemove);
                }

                var firstNode = _firstPathNodes.FirstOrDefault(f=>f.ID == node.ID);
                if (firstNode != null) _firstPathNodes.Remove(firstNode);

                var lastNode = _lastPathNodes.FirstOrDefault(l => l.ID == node.ID);
                if (lastNode != null) _lastPathNodes.Remove(lastNode);

                if (Mode == ModeWork.Single) _calcResult = null;
                else _calcResult = null; //TODO: необходимо очищать все словари везде, в презентере, отображалке, коре холдере, ...
                
                _guiCallback.OnRemoveNode(origNode.ID);
            }
            _HasSchemaChanges = true;
            //throw new NotImplementedException();
        }

        void IGExDomain.UpdateNodePosition(int nodeId, double x, double y)
        {
            var origNode = _scheme.Nodes.FirstOrDefault(n => n.ID == nodeId);
            if (origNode == null)
                return;
            origNode.X = x;
            origNode.Y = y;
            _HasSchemaChanges = true;
        }

        void IGExDomain.UpdateNodeLabelOffset(int nodeId, double x, double y)
        {
            var origNode = _scheme.Nodes.FirstOrDefault(n => n.ID == nodeId);
            if (origNode == null)
                return;
            origNode.LabelX = x;
            origNode.LabelY = y;
            _HasSchemaChanges = true;
        }

        void IGExDomain.UpdateNodeName(int nodeId, string name)
        {
          var origNode = _scheme.Nodes.FirstOrDefault(n => n.ID == nodeId);
          if (origNode == null)
              return;
          if (origNode.GasExportObject != null)
          {
              origNode.GasExportObject.Name = name;
              if (_scenario != null)
              {
                  var Bsd = _scenario.BorderScenarioDataSet.FirstOrDefault(bsd => bsd.GasExportObject.ID == origNode.GasExportObject.ID);
                  if (Bsd != null) Bsd.GasExportObject.Name = name;
                  var Osd = _scenario.OutputScenarioDataSet.FirstOrDefault(osd => osd.Obj.ID == origNode.GasExportObject.ID);
                  if (Osd != null) Osd.Obj.Name = name;
                  var Rgisd = _scenario.RussianGasInputScenarioDataSet.FirstOrDefault(rgisd => rgisd.Obj.ID == origNode.GasExportObject.ID);
                  if (Rgisd != null) Rgisd.Obj.Name = name;
                  var Usd = _scenario.UGSScenarioDataSet.FirstOrDefault(usd => usd.Obj.ID == origNode.GasExportObject.ID);
                  if (Usd != null) Usd.Obj.Name = name;
              }
              _guiCallback.OnNodeUpdated(GetNodeDto(origNode));
          }
        }

        void IGExDomain.UpdateNodeOutput(int nodeId, bool output)
        {
            var origNode = _scheme.Nodes.FirstOrDefault(n =>n.GasExportObject != null && n.ID == nodeId);
            if (origNode == null)
                return;
            if (origNode.GasExportObject != null)
            {
                (origNode.GasExportObject as Border).IsOutput = output;
                _guiCallback.OnNodeUpdated(GetNodeDto(origNode));
            }
        }

        IEnumerable<BalanceZone> IGExDomain.GetBalanceZones()
        {
            return _balanceZones;
           // return _balanceZoneRepo.LoadAll();
        }

        #region Scenario

        IEnumerable<DTO_Scenario> IGExDomain.LoadCalculationScenarios()
        {
            var scenarios = _scenarioRepo.LoadAll();
            List<DTO_Scenario> scenariosDto = new List<DTO_Scenario>();
            foreach (var scen in scenarios)
            {
                var dto = GetScenarioDto(scen);
                if (dto != null)
                    scenariosDto.Add(dto);
            }
            return scenariosDto;
            
        }

        void IGExDomain.LoadScenario(int IdScenario)
        {
            const string errorSource = "CoreHolder::LoadScenario()";
            _errorHandler.ReportError(eErrClass.EC_INFO, "Начало загрузки сценария Id =" + IdScenario.ToString(), errorSource);
            //if (_scenarios == null) _scenarios = new List<CalculationScenario>();
            CalculationScenario res = _scenarioRepo.Load(IdScenario);
            if (res != null)
            {
                //не добавляем узлы схемы сразу в сценарий, а используем "фантомные узлы" для редактирования в GUI
                //CorrelateDefaultScenario(ref res);
                _scenario = res;
                _calcResult = null;
                //string noElements = _scenario.Init(res);
                //if (noElements.Length > 0)
                //    _errorHandler.ReportError(eErrClass.EC_WARN, noElements, errorSource);
                _errorHandler.ReportError(eErrClass.EC_INFO, "Сценарий '" + res.Name + "' загружен.", errorSource);

                var errString = "Дублирующаяся информация в сценарии! [{0}] {1} {2} {3}";

                foreach(var gr1 in res.BorderScenarioDataSet.GroupBy(x=>x.GasExportObject))
                    foreach (var gr2 in gr1.GroupBy(x => x.Operator))
                    {
                        if (gr2.Count(x => x.Direction == Direction.Entry) > 1)
                            _errorHandler.ReportError(eErrClass.EC_ERROR, string.Format(errString, "Тарифы", gr1.Key.Name, gr2.Key.Name, "Entry"), errorSource);

                        if (gr2.Count(x => x.Direction == Direction.Exit) > 1)
                            _errorHandler.ReportError(eErrClass.EC_ERROR, string.Format(errString, "Тарифы", gr1.Key.Name, gr2.Key.Name, "Exit"), errorSource);

                    }

                errString = "Дублирующаяся информация в сценарии! [{0}] {1}";

                foreach (var t in res.OutputScenarioDataSet.GroupBy(x => x.Obj).Where(x => x.Count() > 1))
                {
                    _errorHandler.ReportError(eErrClass.EC_ERROR, string.Format(errString, "Контракты", t.Key.Name), errorSource);
                }

                foreach (var t in res.RussianGasInputScenarioDataSet.GroupBy(x => x.Obj).Where(x => x.Count() > 1))
                {
                    _errorHandler.ReportError(eErrClass.EC_ERROR, string.Format(errString, "Экспорт", t.Key.Name), errorSource);
                }

                foreach (var t in res.UGSScenarioDataSet.GroupBy(x => x.Obj).Where(x => x.Count() > 1))
                {
                    _errorHandler.ReportError(eErrClass.EC_ERROR, string.Format(errString, "ПХГ", t.Key.Name), errorSource);
                }

                _guiCallback.OnScenarioLoaded();
                _errorHandler.ReportError(eErrClass.EC_TRACE, "Уведомления OnScenarioLoaded() разосланы.", errorSource);
            }
            else
                _errorHandler.ReportError(eErrClass.EC_INFO, "Сценарий id = " + IdScenario.ToString() + " не загружен (пуст).", errorSource);
            //return res;
        }

        bool IGExDomain.ScenarioIsNew()
        {
            return _scenario != null && _scenario.ID <= 0;
        }

        private void CorrelateDefaultScenario(ref CalculationScenario res)
        {
            foreach (Node currNode in _scheme.Nodes)
            {
                if (currNode.GasExportObject == null) continue;

                switch (currNode.GasExportObject.Type)
                {
                    case GasExportObjectType.Border:
                        // _scenario.
                        break;
                    case GasExportObjectType.Output:                        
                        //if (!res.OutoutScenarioDataSet.Any(e => e.Obj.ID == currNode.GasExportObject.ID))
                        //    res.OutoutScenarioDataSet.Add(new OutputScenarioData()
                        //    {
                        //        Obj = (currNode.GasExportObject as Output),
                        //        ContractVolume = null
                        //    });
                        break;
                    case GasExportObjectType.UGS:                        
                        if (res.UGSScenarioDataSet.All(e => e.Obj.ID != currNode.GasExportObject.ID))
                            res.UGSScenarioDataSet.Add(new UGSScenarioData()
                            {
                                Obj = (currNode.GasExportObject as UGS),
                                MaxUGSWithdrawal = null
                            });
                        break;
                    case GasExportObjectType.RussianGasInput:                        
                        if (res.RussianGasInputScenarioDataSet.All(e => e.Obj.ID != currNode.GasExportObject.ID))
                            res.RussianGasInputScenarioDataSet.Add(new RussianGasInputScenarioData()
                            {
                                Obj = (currNode.GasExportObject as RussianGasInput),
                                MaxExportVolume = null
                            });
                        break;
                }
            }
        }

        //Удалить
        DTO_Scenario IGExDomain.GetScenario()
        {
            return GetScenarioDto(_scenario);
        }

        void IGExDomain.CreateNewScenario()
        {
            const string errorSource = "CoreHolder::CreateNewScenario()";
            
            _scenario = new CalculationScenario();                   
            //Если есть схема, то инициализируем внутренности сценария по объектам схемы
            /*
            if (_scheme != null)
            {

                foreach (Node currNode in _scheme.Nodes)
                {
                    if (currNode.GasExportObject == null) continue;

                    switch (currNode.GasExportObject.Type)
                    {
                        case GasExportObjectType.Border:
                            // _scenario.
                            break;
                        case GasExportObjectType.Output:
                            //var baseOutoutScenarioData = baseScenario.OutoutScenarioDataSet.FirstOrDefault(e => e.Obj.ID == currNode.GasExportObject.ID);
                            _scenario.OutoutScenarioDataSet.Add(new OutputScenarioData()
                            {
                                Obj = (currNode.GasExportObject as Output),
                                ContractVolume = null
                            });
                            break;
                        case GasExportObjectType.UGS:
                            //var baseUGSScenarioData = baseScenario.UGSScenarioDataSet.FirstOrDefault(e => e.Obj.ID == currNode.GasExportObject.ID);
                            _scenario.UGSScenarioDataSet.Add(new UGSScenarioData()
                            {
                                Obj = (currNode.GasExportObject as UGS),
                                MaxUGSWithdrawal = null
                            });
                            break;
                        case GasExportObjectType.RussianGasInput:
                            //var baseRussianGasInputScenarioData = baseScenario.RussianGasInputScenarioDataSet.FirstOrDefault(e => e.Obj.ID == currNode.GasExportObject.ID);
                            _scenario.RussianGasInputScenarioDataSet.Add(new RussianGasInputScenarioData()
                            {
                                Obj = (currNode.GasExportObject as RussianGasInput),
                                MaxExportVolume = null
                            });
                            break;
                    }
                }
            }
            */
            _calcResult = null;
            _errorHandler.ReportError(eErrClass.EC_INFO, "Создан сценарий '" + _scenario.Name + "'.", errorSource);
            _guiCallback.OnScenarioLoaded();
        }

        /// <summary>
        /// IMB: Визуальность взаимодействует с остальными частями системы через такие методы.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        void IGExDomain.SetScenarioParameterValue(GasExportObjectType type, object value)//Удлить
        {
            switch (type)
            {
                case GasExportObjectType.UGS:
                    var tmpUGS = value as Tuple<int, double?>;
                    if (tmpUGS != null)
                    {
                        var item = _scenario.UGSScenarioDataSet.FirstOrDefault(e => e.Obj.ID == tmpUGS.Item1);
                        if (item != null)
                            item.MaxUGSWithdrawal = tmpUGS.Item2;
                    }
                    break;
                case GasExportObjectType.RussianGasInput:
                    var tmpRussianGasInput = value as Tuple<int, double?>;
                    if (tmpRussianGasInput != null)
                    {
                        var item = _scenario.RussianGasInputScenarioDataSet.FirstOrDefault(e => e.Obj.ID == tmpRussianGasInput.Item1);
                        if (item != null)
                            item.MaxExportVolume = tmpRussianGasInput.Item2;
                    }
                    break;
                case GasExportObjectType.Output:
                    var tmpOutput = value as Tuple<int, double?>;
                    if (tmpOutput != null)
                    {
                        var item = _scenario.OutputScenarioDataSet.FirstOrDefault(e => e.Obj.ID == tmpOutput.Item1);
                        if (item != null)
                            item.ContractVolume = tmpOutput.Item2;
                    }
                    break;
                case GasExportObjectType.Border:
                    break;
            }
        }

        void IGExDomain.SaveScenario()
        {
            const string errorSource = "CoreHolder::SaveScenario()";
            try
            {
                _scenario.CreationDate = DateTime.Now;
                _scenario.Author = Environment.UserDomainName + "\\" + Environment.UserName;
                _scenarioRepo.Update(_scenario);
                //_guiCallback.CallGui(errorSource);
                _errorHandler.ReportError(eErrClass.EC_INFO, "Сценарий   '" + _scenario.Name + "' сохранен.", errorSource);
            }
            catch (Exception ex)
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, "Ошибка при сохранении сценария   '" + _scenario.Name + "'. " + ex.Message, errorSource);
                throw;
            }
        }

        void IGExDomain.SaveScenarioAsNew()
        {
            const string errorSource = "CoreHolder::SaveScenarioAsNew()";
            try
            {
                _scenario.CreationDate = DateTime.Now;
                _scenario.Author = Environment.UserDomainName + "\\" + Environment.UserName;
                _scenarioRepo.Create(_scenario);
                _guiCallback.OnScenarioSaved();
                _errorHandler.ReportError(eErrClass.EC_INFO, "Сценарий '" + _scenario.Name + "' cохранен  как новый.", errorSource);
            }
            catch (Exception ex)
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, "Ошибка при сохранении сценария   '" + _scenario.Name + "'. " + ex.Message, errorSource);
                throw;
            }
        }
        
        void IGExDomain.UpdateScenarioName(int IdScenario, string name)
        {
            const string errorSource = "CoreHolder::UpdateScenarioName()";
            try
            {                            
                var originScenario = _scenarioRepo.Load(IdScenario);
                originScenario.Name = name;
                _scenarioRepo.Update(originScenario);
                _errorHandler.ReportError(eErrClass.EC_INFO, String.Format("Имя сценария Id = {0} изменено на '{1}'.", IdScenario, name), errorSource);              
            }
            catch (Exception ex)
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, "Ошибка при переименовании сценария   '" + name + "'. " + ex.Message, errorSource);
                throw;
            }
        }

        void IGExDomain.UpdateScenarioComment(int IdScenario, string comm)
        {
            const string errorSource = "CoreHolder::UpdateScenarioComment()";
            try
            {
                var originScenario = _scenarioRepo.Load(IdScenario);
                originScenario.Comment = comm;
                _scenarioRepo.Update(originScenario);
                _errorHandler.ReportError(eErrClass.EC_INFO, String.Format("Комментарий сценария Id = {0} изменен на '{1}'.", IdScenario, comm), errorSource);
            }
            catch (Exception ex)
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, "Ошибка при изменении комментария сценария Id = '" + IdScenario.ToString() + "'. " + ex.Message, errorSource);
                throw;
            }
        }

        void IGExDomain.UpdateScenario(DTO_Scenario scenarioDto)
        {
            _scenario.Name = scenarioDto.Name;
        }

        void IGExDomain.UpdateScenarioName(string name, string comm)
        {
            const string errorSource = "CoreHolder::UpdateScenarioName()";
            try
            {
                if (_scenario != null)
                {
                    _scenario.Name = name;
                    _scenario.Comment = comm;
                }
            }
            catch (Exception ex)
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, "Ошибка при переименовании сценария   '" + name + "'. " + ex.Message, errorSource);
                throw;
            }
        }
                
        void IGExDomain.RemoveScenario(int IdScenario)
        {
            const string errorSource = "CoreHolder::RemoveScenario()";
            try
            {
                _scenarioRepo.Remove(IdScenario);
                _errorHandler.ReportError(eErrClass.EC_INFO, String.Format("Сценарий Id = {0} удален.", IdScenario), errorSource); 
            }
            catch (Exception ex)
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, "Ошибка при удалении сценария Id = " + IdScenario + "." + ex.Message, errorSource);
                throw;
            }
        }
        
        #endregion

        bool IGExDomain.Calculate()
        {
            const string errorSource = "CoreHolder::Calculate()";
            _errorHandler.ReportError(eErrClass.EC_INFO, "Расчет начат.", errorSource);

            //((IGExCalculate)Engine.Instance).Start(dp);
            var engine = (IGExCalculate)Engine.Instance;
            engine.Init(_errorHandler);

            if (Mode == ModeWork.Single)
            {
                var dp = new GExRoutesDataPump()
                {
                    CalcScenario = _scenario,
                    Scheme = _scheme,
                    SourcePoint = _firstPathNodes.First().GasExportObject as IOneZoneNode,
                    TargetPoint = _lastPathNodes.First().GasExportObject
                };
                engine.Start(dp, Mode);
            }

            if (Mode == ModeWork.Multi)
            {
                var dp = new GExContractsDataPump()
                {
                    CalcScenario = _scenario,
                    Scheme = _scheme,
                    SourcePoints = _firstPathNodes.Select(f=>f.GasExportObject).OfType<IOneZoneNode>().Distinct().ToList(),
                    TargetPoints = _lastPathNodes.Select(l=>l.GasExportObject).Distinct().ToList()
                };
                engine.Start(dp, Mode);
            }
            
            _calcResult = (GExBaseDocResult) Engine.Instance.Result;

            _errorHandler.ReportError(eErrClass.EC_INFO, "Расчет завершен.", errorSource);
            _guiCallback.OnCalculated();
            return false;
        }

        public void ShowCalculate(IGeneralDocResult result)
        {
            _calcResult = (GExBaseDocResult)result;
            _guiCallback.OnCalculated();
        }

        IGeneralDocResult IGExDomain.DocResult
        {
            get
            {
                return _calcResult;
            }
        }

        public ModeWork Mode { get; set; }

        bool IGExDomain.IsCalculated()
        {
            return _calcResult != null;
        }

        IEnumerable<DTO_Path> IGExDomain.GetCalculatedPaths()
        {
            if (_calcResult == null)
                return new List<DTO_Path>();
            
            if (_calcResult is GExContractsDocResult)
            {
                var dtoPaths = new List<DTO_Path>();
                ((GExContractsDocResult)_calcResult).Routes.ForEach(pair =>
                {
                    var dto = CreateDtoPaths(pair.Value);
                    dto.ForEach(d=>
                    {
                        d.OutputZone = new DTO_BalanceZone
                        {
                            ID = pair.Key.Point.Id,
                            Name = pair.Key.Point.GasExportObject.Name,
                        };
                    });
                    dtoPaths.AddRange(dto);
                });
                return dtoPaths;
            }
            else if (_calcResult is GExRoutesDocResult)
            {
                var result = (GExRoutesDocResult) _calcResult;
                return CreateDtoPaths(result.Routes);
            }
            return new List<DTO_Path>();
        }

        private IEnumerable<DTO_Path> CreateDtoPaths(List<GExGraphRoute> routes)
        {
            List<DTO_Path> res = new List<DTO_Path>();
            if (routes.Any())
            {
                foreach (var curRoute in routes)
                {
                    var vertices = curRoute.Path.Select(v => new DTO_Vertex()
                        {
                            Node =
                                GetNodeDto(_scheme.Nodes.FirstOrDefault(
                                    n => n.GasExportObject.Equals(v.Vertex.Point.GasExportObject))),
                            Capacity = v.Capacity,
                            RelativeFixedCost = v.RelativeFixedCost,
                            RelativeVariableCost = v.RelativeVariableCost,
                            AbsoluteFixedCost = v.AbsoluteFixedCost,
                            AbsoluteVariableCost = v.AbsoluteVariableCost,
                            Zone = GetZoneDTO(v.Vertex.Point.Operator),
                            Direction = (DTO_Direction) ((int) v.Vertex.Point.Direction)
                        })
                        .ToList();

                    res.Add(new DTO_Path()
                        {
                            Vertices = vertices,
                            RelativeFixedCost = curRoute.RelativeFixedCost,
                            RelativeVariableCost = curRoute.RelativeVariableCost,
                            AbsoluteFixedCost = curRoute.AbsoluteFixedCost,
                            AbsoluteVariableCost = curRoute.AbsoluteVariableCost,
                            TotalCapacity = curRoute.TotalCapacity
                        }
                    );
                }
            }
            return res;
        }

        /// <summary>
        /// IMB: Код формирования данных для отображения матрицы здесь
        /// </summary>
        IEnumerable<DTO_NodeRelation> IGExDomain.LoadNodeRelations()
        {
            var zoneToDictionaryNodes = new Dictionary<int, List<int>>();
            foreach (var node in _scheme.Nodes)
            {
                var zoneIds = new List<int>();
                var zoneNode = node.GasExportObject as IOneZoneNode;
                if (zoneNode != null && zoneNode.Zone!=null)
                    zoneIds.Add(zoneNode.Zone.ID);

                var border = node.GasExportObject as Border;
                if (border != null)
                    zoneIds.AddRange(border.Interconnections.Select(x => x.ZoneFrom.ID)
                        .Union(border.Interconnections.Select(x => x.ZoneTo.ID)));

                foreach (var zoneId in zoneIds)
                {
                    List<int> nodeIds ;
                    if (zoneToDictionaryNodes.ContainsKey(zoneId))
                        nodeIds = zoneToDictionaryNodes[zoneId];
                    else
                    {
                    nodeIds = new List<int>();
                    zoneToDictionaryNodes.Add(zoneId, nodeIds);
                    }
                    nodeIds.Add(node.ID);
                }
            }

            var relationsDto = new List<DTO_NodeRelation>();
            int tempId = 0;
            foreach (var zoneWithNodes in zoneToDictionaryNodes)
            {
                var zoneId = zoneWithNodes.Key;
                var nodesIds = zoneWithNodes.Value;
                var nodesPairs = (from n1 in nodesIds from n2 in nodesIds select new {first = n1, second = n2})
                    .ToList();
                foreach (var pair in nodesPairs)
                {
                    var rel =
                        _scheme.Relations.FirstOrDefault(
                            x => x.NodeFirst.ID == pair.first && x.NodeSecond.ID == pair.second &&
                                 x.BalanceZone.ID == zoneId);
                    if (rel != null)
                        relationsDto.Add(new DTO_NodeRelation
                        {
                            ID = rel.ID,
                            ZoneID = rel.BalanceZone.ID,
                            NodeIdFirst = rel.NodeFirst.ID,
                            NodeIdSecond = rel.NodeSecond.ID,
                            Value = rel.Value
                        });
                    else
                    {
                        relationsDto.Add(
                            new DTO_NodeRelation
                            {
                                ID = --tempId,
                                NodeIdFirst = pair.first,
                                NodeIdSecond = pair.second,
                                ZoneID = zoneId
                            });
                    }
                }
                

            }
            return relationsDto;
        }

        public void UpdateNodeRelations(IEnumerable<DTO_NodeRelation> updatedRelations)
        {

            //var idsOfRelations = _scheme.Relations.Select(r => r.ID).ToArray();
            //var idsOfUpdatedRelations = updatedRelations.Select(r => r.ID).ToArray();           
           
            //var idsOfRelationsForRemove = idsOfRelations.Except(idsOfUpdatedRelations).ToArray();
            
            //foreach (var id in idsOfRelationsForRemove)
            //{
            //    var rel = _scheme.Relations.FirstOrDefault(r => r.ID == id);
            //    if( rel!=null )
            //        _scheme.Relations.Remove(rel);
            //}

            //var idsOfRelationsForAdd = idsOfUpdatedRelations.Except(idsOfRelations).ToArray();

            foreach(var relDto in updatedRelations)
            {
                 var rel =  _scheme.Relations.FirstOrDefault(r => r.NodeFirst.ID == relDto.NodeIdFirst &&
                                           r.NodeSecond.ID == relDto.NodeIdSecond &&
                                           r.BalanceZone.ID == relDto.ZoneID);

                if (rel!=null)
                {
                  //  if(Math.Abs(rel.Value - relDto.Value) > 2)
                        rel.Value = relDto.Value;
                }
                else
                {
                    var newRelation = new NodesRelation
                    {
                        ID = _NewRelationId--,
                        Schema = _scheme,
                        SchemaID = _scheme.ID,
                        BalanceZone = _balanceZones.FirstOrDefault(z => z.ID == relDto.ZoneID),
                        NodeFirst = _scheme.Nodes.FirstOrDefault(n => n.ID == relDto.NodeIdFirst),
                        NodeSecond = _scheme.Nodes.FirstOrDefault(n => n.ID == relDto.NodeIdSecond),
                        Value = relDto.Value
                    };
                    _scheme.Relations.Add(newRelation);
                }
            }
            _HasSchemaChanges = true;
        }


        public void AddNodeRelations(IEnumerable<DTO_NodeRelation> newRelations)
        {
            foreach (var rel in newRelations)
            {
                if (!_scheme.Relations.Any(r => (r.NodeFirst.ID == rel.NodeIdFirst &&
                                                 r.NodeSecond.ID == rel.NodeIdSecond &&
                                                 r.BalanceZone.ID == rel.ZoneID)))
                {
                    NodesRelation newRelation = new NodesRelation
                    {
                        ID = _NewRelationId--,
                        Schema = _scheme,
                        SchemaID = _scheme.ID,
                        BalanceZone = _balanceZones.FirstOrDefault(z => z.ID == rel.ZoneID),
                        NodeFirst = _scheme.Nodes.FirstOrDefault(n => n.ID == rel.NodeIdFirst),
                        NodeSecond = _scheme.Nodes.FirstOrDefault(n => n.ID == rel.NodeIdSecond),
                        Value = rel.Value
                    };
                    _scheme.Relations.Add(newRelation);
                }
            }
            _HasSchemaChanges = true;
        }


        void IGExDomain.UnInit()
        {
            const string errorSource = "CoreHolder::UnInit()";
            _guiCallback.CallGui(errorSource);
        }

        Schema IGExDomain.GenerateModel()
        {
            _scheme = new TestSchemeGenerator().GenerateDataModel();
            return _scheme;
        }

        Node GetNodeFromDTONode(DTO_Node dtoNode)
        {
            Node node =
                _scheme.Nodes.FirstOrDefault(n => dtoNode.ID == n.ID);
            return node;
        }

        bool IGExDomain.SetFirstPointsForPath(DTO_Node dtoNode)
        {
            return Mode == ModeWork.Single 
                ? FirstPointForPath(dtoNode)
                : FirstPointsForPath(dtoNode);
        }

        private bool FirstPointsForPath(DTO_Node dtoNode)
        {
            var node = GetNodeFromDTONode(dtoNode);
            if (node == null)
                return false;

            IOneZoneNode oneZoneNode = node.GasExportObject as IOneZoneNode;

            //choose first point as the first for the path
            //...
            if (oneZoneNode == null) return false;

            _firstPathNodes.Add(node);
            return true;
        }

        private bool FirstPointForPath(DTO_Node dtoNode)
        {
            var node = GetNodeFromDTONode(dtoNode);
            if (node == null) return false;

            IOneZoneNode oneZoneNode = node.GasExportObject as IOneZoneNode;

            //choose first point as the first for the path
            //...
            if (oneZoneNode == null) return false;

            if (_firstPathNodes.Any())
            {
                var dto = GetNodeDto(_firstPathNodes.First());
                _guiCallback.OnRemoveNodeAsSelectedForPath(dto);
            }

            _firstPathNodes.Clear();
            _firstPathNodes.Add(node);
            return true;
        }
        
        void IGExDomain.SetLastPointsForPath(DTO_Node dtoNode)
        {
            if (Mode == ModeWork.Single)
                LastPointForPath(dtoNode);
            else
                LastPointsForPath(dtoNode);
        }

        private void LastPointsForPath(DTO_Node dtoNode)
        {
            var node = GetNodeFromDTONode(dtoNode);
            if (node == null) return;

            _lastPathNodes.Add(node);
        }

        private void LastPointForPath(DTO_Node dtoNode)
        {
            var node = GetNodeFromDTONode(dtoNode);
            if (node == null) return;

            var nd = node.GasExportObject;
            //choose second point as the first for the path
            //...
            if (_lastPathNodes.Any())
            {
                var dto = GetNodeDto(_lastPathNodes.First());
                _guiCallback.OnRemoveNodeAsSelectedForPath(dto);
            }

            _lastPathNodes.Clear();
            _lastPathNodes.Add(node);
        }

        /// <summary>
        /// Очистить точки маршрутов
        /// </summary>
        public void ClearPointsPaths()
        {
            _firstPathNodes = new List<Node>();
            _lastPathNodes = new List<Node>();
        }

        /// <summary>
        /// Удалить точку маршрута
        /// </summary>
        /// <param name="node"></param>
        public void RemovePoint(DTO_Node node)
        {
            var firstNode = _firstPathNodes.FirstOrDefault(f => f.ID == node.ID);
            if (firstNode != null) _firstPathNodes.Remove(firstNode);

            var lastNode = _lastPathNodes.FirstOrDefault(l => l.ID == node.ID);
            if (lastNode != null) _lastPathNodes.Remove(lastNode);
        }

        /// <summary>
        /// Установить соединения от узла ко всем другим узлам в данной зоне
        /// </summary>
        /// <param name="nodeid"></param>
        /// <param name="zone"></param>
        void IGExDomain.SetConnections(int nodeid, DTO_BalanceZone zone)
        {
            // 1) получить список узлов зоны
            // 2) установить связи от узла ко всем узлам зоны и от всех узлов зоны к узлу

            var nodes = ((IGExDomain) this).GetSchemaNodes();
            var balanceZones = ((IGExDomain) this).GetBalanceZones()
                    .Select(bz => new DTO_BalanceZone {ID = bz.ID, Name = bz.Name})
                    .ToList();

            // List{ Node, List{Zone>}}
            var nodesWithUnitedZones = nodes.Select(n => new
            {
                n,
                //все-все-все зоны узла - со всех интерконнекторов и полей ZoneID, ZoneName
                Zones1 =
                    (n.InterConnections.Select(x => x.ZoneFrom).Union(
                        n.InterConnections.Select(x => x.ZoneTo)).ToList().Union(
                            new[] {balanceZones.FirstOrDefault(z => z.ID == n.ZoneId)})).Where(elem => (elem != null))
                        .ToList()
            });

            // плющим список в пары
            // List {Node,Zone}
            var nodeZonePairs =
                nodesWithUnitedZones.SelectMany(x => x.Zones1.Select(y => new {Node = x.n, Zone = y})).ToList();

            nodeZonePairs.Sort((x, y) => String.Compare(x.Node.Name, y.Node.Name, StringComparison.Ordinal));

            //объединяем по зонам
            //Dictionary {Zone, List{Node}}
            var zoneNodesDictionary = nodeZonePairs.GroupBy(x => x.Zone)
                .ToDictionary(x => x.Key, y => y.Select(zz => zz.Node).ToList());
            
            if (!zoneNodesDictionary.ContainsKey(zone))
            {
                //TODO: фатально! нужно с этим случаем что-то делать,
                //однако по дизайну такого не должно происходить
                return;
            }

            var nodesInZone = zoneNodesDictionary[zone];

            var relations = new List<DTO_NodeRelation>();

            foreach (var dtoNode in nodesInZone)
            {
                if (dtoNode.ID.Equals(nodeid)) continue;

                relations.Add(new DTO_NodeRelation() {NodeIdFirst = nodeid, NodeIdSecond = dtoNode.ID, ZoneID = zone.ID});
                relations.Add(new DTO_NodeRelation() {NodeIdFirst = dtoNode.ID, NodeIdSecond = nodeid, ZoneID = zone.ID});
            }
          //  UpdateNodeRelations(relations);
              AddNodeRelations(relations);
        }
        
        #endregion

        #region DTO Helpers

        private DTO_BalanceZone GetZoneDTO(BalanceZone zone)
        {
            return new DTO_BalanceZone() 
            {
                ID = zone.ID,
                Name = zone.Name
            };
        }

        private DTO_Node GetNodeDto(Node node)
        {
            if (node == null)
                return null;

            DTO_Node nodeDto = new DTO_Node();
            nodeDto.ID = node.ID;
            nodeDto.X = node.X;
            nodeDto.Y = node.Y;
            nodeDto.LabelX = node.LabelX ?? nodeDto.LabelX;
            nodeDto.LabelY = node.LabelY ?? nodeDto.LabelY;


            if (node.GasExportObject != null)
            {
                nodeDto.Name = node.GasExportObject.Name;
                nodeDto.Type = (DTO_Node_type) node.GasExportObject.Type;


                if (node.GasExportObject is IOneZoneNode)
                {
                    var oneZone = node.GasExportObject as IOneZoneNode;
                    nodeDto.ZoneName = oneZone.Zone != null ? oneZone.Zone.Name : string.Empty;
                    nodeDto.ZoneId = oneZone.Zone != null ? (int?) oneZone.Zone.ID : null;
                }
                else
                {
                    var border = (node.GasExportObject as Border);
                    
                    if (border == null) return nodeDto;

                    nodeDto.IsOutput = border.IsOutput;
                    nodeDto.InterConnections =
                        border.Interconnections.Select(ic => new DTO_BorderInterconnection()
                        {
                            Id = ic.Id,
                            ZoneFrom = new DTO_BalanceZone()
                            {
                                ID = ic.ZoneFrom != null ? ic.ZoneFrom.ID : -1,
                                Name = ic.ZoneFrom != null ? ic.ZoneFrom.Name : string.Empty,
                            },
                            ZoneTo = new DTO_BalanceZone()
                            {
                                ID = ic.ZoneTo != null ? ic.ZoneTo.ID : -1,
                                Name = ic.ZoneTo != null ? ic.ZoneTo.Name : string.Empty,
                            }
                        }
                            ).ToList();
                }
            }
            return nodeDto;
        }

        private DTO_Schema GetSchemaDto(Schema schema)
        {
            if (schema == null)
                return null;
            DTO_Schema schemaDto = new DTO_Schema();
            schemaDto.Id = schema.ID;
            schemaDto.SchemaName = schema.Name;
            schemaDto.Author = schema.Author;
            schemaDto.CreationDate = schema.CreationDate;
            if (schema.SchemaImage!=null)
                schemaDto.Image = schema.SchemaImage.Image;
            schemaDto.Comment = schema.Comment;
            return schemaDto;
            }

        private DTO_Scenario GetScenarioDto(CalculationScenario scen)
        {
            if (scen == null)
                return null;
            DTO_Scenario scenarioDto = new DTO_Scenario();
            scenarioDto.ID = scen.ID;
            scenarioDto.Name = scen.Name;
            scenarioDto.Author = scen.Author;
            scenarioDto.CreationDate = scen.CreationDate;
            scenarioDto.Comment = scen.Comment;
            return scenarioDto;

        }

        #endregion


        IEnumerable<DTO_OutputScenarioData> IGExDomain.LoadOutputScenarioData()
        {
            if (_scenario == null) return null;
            var res = _scenario.OutputScenarioDataSet.Select(o => new DTO_OutputScenarioData()
                {
                    Id = o.Obj == null ? 0: o.Obj.ID,
                    OutputName = o.Obj !=null ? o.Obj.Name : string.Empty,
                    ConctractVolume = o.ContractVolume,
                    IsActive = _scheme.Nodes.FirstOrDefault(n=> n.GasExportObject != null && n.GasExportObject.ID == o.Obj.ID && n.GasExportObject is Border) == null ? false :
                               (_scheme.Nodes.First(n => n.GasExportObject != null && n.GasExportObject.ID == o.Obj.ID && n.GasExportObject is Border).GasExportObject as Border).IsOutput
                    
                }).ToList();

            // добавляем фантомные узлы
            foreach (var currNode in _scheme.Nodes.Where(e => (e.GasExportObject is Border) && (e.GasExportObject as Border).IsOutput))
            {
                var baseOutoutScenarioData = res.FirstOrDefault(e => e.Id == currNode.GasExportObject.ID);
                if (baseOutoutScenarioData == null)
                {
                    res.Add(new DTO_OutputScenarioData()
                    {
                        Id = currNode.GasExportObject == null ? 0 : currNode.GasExportObject.ID,
                        OutputName = currNode.GasExportObject != null ? currNode.GasExportObject.Name : string.Empty,
                        ConctractVolume = null,
                        IsActive = true 
                    });
                }
            }

            return res;
        }

        IEnumerable<DTO_RussianGasInputScenario> IGExDomain.LoadRussianGasInputScenario()
        {
            if (_scenario == null) return null;
            var res = _scenario.RussianGasInputScenarioDataSet.Select(o => new DTO_RussianGasInputScenario()
            {
                Id = o.Obj == null ? 0 : o.Obj.ID,
                RussianGasInputName = o.Obj != null ? o.Obj.Name : string.Empty,
                MaxExportVolume = o.MaxExportVolume,
                IsActive = _scheme.Nodes.Select(n => n.GasExportObject.ID).Contains(o.Obj.ID)

            }).ToList();

            // добавляем фантомные узлы
            foreach (var currNode in _scheme.Nodes.Where(e => e.GasExportObject.Type == GasExportObjectType.RussianGasInput))
            {
                var baseOutoutScenarioData = res.FirstOrDefault(e => e.Id == currNode.GasExportObject.ID);
                if (baseOutoutScenarioData == null)
                {
                    res.Add(new DTO_RussianGasInputScenario()
                    {
                        Id = currNode.GasExportObject == null ? 0 : currNode.GasExportObject.ID,
                        RussianGasInputName = currNode.GasExportObject != null ? currNode.GasExportObject.Name : string.Empty,
                        MaxExportVolume = null,
                        IsActive = true
                    });
                }
            }

            return res;
        }

        IEnumerable<DTO_UGSScenarioData> IGExDomain.LoadUGSScenarioData()
        {
            if (_scenario == null) return null;
            var res = _scenario.UGSScenarioDataSet.Select(o => new DTO_UGSScenarioData()
            {
                Id = o.Obj == null ? 0 : o.Obj.ID,
                UGSName = o.Obj != null ? o.Obj.Name : string.Empty,
                MaxUGSWithdrawal = o.MaxUGSWithdrawal,
                IsActive = _scheme.Nodes.Select(n => n.GasExportObject.ID).Contains(o.Obj.ID)

            }).ToList();

            // добавляем фантомные узлы
            foreach (var currNode in _scheme.Nodes.Where(e => e.GasExportObject.Type == GasExportObjectType.UGS))
            {
                var baseOutoutScenarioData = res.FirstOrDefault(e => e.Id == currNode.GasExportObject.ID);
                if (baseOutoutScenarioData == null)
                {
                    res.Add(new DTO_UGSScenarioData()
                    {
                        Id = currNode.GasExportObject == null ? 0 : currNode.GasExportObject.ID,
                        UGSName = currNode.GasExportObject != null ? currNode.GasExportObject.Name : string.Empty,
                        MaxUGSWithdrawal = null,
                        IsActive = true
                    });
                }
            }

            return res;
        }
        
        IEnumerable<DTO_BorderScenarioData> IGExDomain.LoadBorderScenarioData()
        {
            if (_scenario == null) return null;
            var res = _scenario.BorderScenarioDataSet.Select(o => new DTO_BorderScenarioData()
            {
                Id_Border = o.GasExportObject == null ? 0 : o.GasExportObject.ID,
                BorderName = o.GasExportObject != null ? o.GasExportObject.Name : string.Empty,
                Id_BalanceZone = o.Operator != null ? o.Operator.ID : 0,
                BalanceZoneName = o.Operator != null ? o.Operator.Name : string.Empty,
                Direction = (DTO_Direction)((int)o.Direction),// ? DTO_Direction.Entry : DTO_Direction.Exit,
                AbleVolumeValue = o.AvailableCapacity,
                ReservedVolumeValue = o.BookedCapacity,
                TarifValue = o.Tariff,
                GsnValue = o.GsnValue,
                GsnPercentValue = o.GsnPercentValue,
                Description = o.Description,
                IsActive = _scheme.Nodes.Any(n => n.GasExportObject.ID == o.GasExportObject.ID && n.GasExportObject is Border &&
                                                (
                                                    o.Direction == Direction.Entry && (n.GasExportObject as Border).Interconnections.Any(con => con.ZoneTo.ID == o.Operator.ID) ||
                                                    o.Direction == Direction.Exit && (n.GasExportObject as Border).Interconnections.Any(con => con.ZoneFrom.ID == o.Operator.ID) 
                                                )
                                            )
            }).ToList();

            // добавляем фантомные узлы
            
            foreach (var currNode in _scheme.Nodes.Where(e => e.GasExportObject is Border))
            {                 
                Border tmpBorder = currNode.GasExportObject as Border;
                if (tmpBorder == null || tmpBorder.Interconnections == null || tmpBorder.Interconnections.Count == 0) continue;
                foreach (var connection in tmpBorder.Interconnections)
                {
                    if (connection.ZoneFrom != null)
                    {
                        var baseOutBorder = res.FirstOrDefault(e => e.Id_Border == currNode.GasExportObject.ID &&
                                                               e.Direction == DTO_Direction.Exit &&
                                                               e.Id_BalanceZone == connection.ZoneFrom.ID                                           
                                                              );
                        if (baseOutBorder == null)
                        { 
                            res.Add(new DTO_BorderScenarioData()
                            {
                                Id_Border = tmpBorder.ID,
                                Id_BalanceZone = connection.ZoneFrom.ID,
                                BorderName = tmpBorder.Name,
                                BalanceZoneName = connection.ZoneFrom.Name,
                                Direction = DTO_Direction.Exit,
                                AbleVolumeValue = null,
                                ReservedVolumeValue = null,
                                TarifValue = null,
                                GsnValue = null,
                                GsnPercentValue = null,
                                Description = null,
                                IsActive = true
                            });
                        }
                    }

                    if (connection.ZoneTo != null)
                    {
                        var baseOutBorder = res.FirstOrDefault(e => e.Id_Border == currNode.GasExportObject.ID &&
                                                               e.Direction == DTO_Direction.Entry &&
                                                               e.Id_BalanceZone == connection.ZoneTo.ID
                                                              );
                        if (baseOutBorder == null)
                        {
                            res.Add(new DTO_BorderScenarioData()
                            {
                                Id_Border = tmpBorder.ID,
                                Id_BalanceZone = connection.ZoneTo.ID,
                                BorderName = tmpBorder.Name,
                                BalanceZoneName = connection.ZoneTo.Name,
                                Direction = DTO_Direction.Entry,
                                AbleVolumeValue = null,
                                ReservedVolumeValue = null,
                                TarifValue = null,
                                GsnValue = null,
                                GsnPercentValue = null,
                                Description = null,
                                IsActive = true
                            });
                        }
                    }
                }               
            }
            
            return res;
        }

        public IEnumerable<DTO_Nominations> LoadNominations()
        {
            if (!(_calcResult is GExContractsDocResult))
                return new List<DTO_Nominations>();

            var list = new List<DTO_Nominations>();
            ((GExContractsDocResult)_calcResult).TotalUsedCapacities.ForEach(t =>
            {
                var value = t.Value;
                if (value == 0) return;

                var point = t.Key.Point;
                var dto = new DTO_Nominations
                {
                    BalanceZoneName = point.Operator.Name,
                    Id_BalanceZone = point.Operator.ID,
                    BorderName = point.GasExportObject.Name,
                    Id_Border = point.GasExportObject.ID,
                    Direction = point.Direction == Direction.Entry ? DTO_Direction.Entry : DTO_Direction.Exit,
//                    IsActive = true,
                    NominationValue = value,
                    IsSource = point.GasExportObject.Type == GasExportObjectType.RussianGasInput || point.GasExportObject.Type == GasExportObjectType.UGS 
                };
                list.Add(dto);
            });
            return list;
        }

        public IEnumerable<DTO_Deliveries> LoadDeliveries()
        {
            if (!(_calcResult is GExContractsDocResult))
                return new List<DTO_Deliveries>();

            var list = new List<DTO_Deliveries>();
            ((GExContractsDocResult)_calcResult).UndeliveredVolumes.ForEach(t =>
            {
                var dto = new DTO_Deliveries
                {
                    ItemName = t.Key.Name,
                    DeliveriesValue = t.Value,
                };
                list.Add(dto);
            });
            return list;
        }

        void IGExDomain.UpdateOutputScenario(int id, double? conctractVolume)
        {           
           var outoutScenarioData = _scenario.OutputScenarioDataSet.Find(o => o.Obj.ID == id);
           if (outoutScenarioData != null)
           {
               if (conctractVolume.HasValue)
                   outoutScenarioData.ContractVolume = conctractVolume;
               else
                   _scenario.OutputScenarioDataSet.Remove(outoutScenarioData);

           }
           else
           {
               if (conctractVolume.HasValue)
               {
                   _scenario.OutputScenarioDataSet.Add(
                       new OutputScenarioData() 
                        {
                            ContractVolume = conctractVolume,
                            Scenario = _scenario,
                            Obj = _scheme.Nodes.First(e => e.GasExportObject.ID == id).GasExportObject as Border,
                            isLoaded = false,
                            ScenarioId = _scenario != null? _scenario.ID : 0,
                            Id = _newOutputScenarioNode--
                        }
                   );
               }
           }
        }

        void IGExDomain.UpdateRussianGasInputScenario(int id, double? maxExportVolume)
        {
            var russianGasInputScenarioData = _scenario.RussianGasInputScenarioDataSet.Find(o => o.Obj.ID == id);
            //if (russianGasInputScenarioData != null)
            //    russianGasInputScenarioData.MaxExportVolume = maxExportVolume;
            if (russianGasInputScenarioData != null)
            {
                if (maxExportVolume.HasValue)
                    russianGasInputScenarioData.MaxExportVolume = maxExportVolume;
                else
                    _scenario.RussianGasInputScenarioDataSet.Remove(russianGasInputScenarioData);

            }
            else
            {
                if (maxExportVolume.HasValue)
                {
                    _scenario.RussianGasInputScenarioDataSet.Add(
                        new RussianGasInputScenarioData()
                        {
                            MaxExportVolume = maxExportVolume,
                            Scenario = _scenario,
                            Obj = _scheme.Nodes.First(e => e.GasExportObject.ID == id).GasExportObject as RussianGasInput,
                            isLoaded = false,
                            ScenarioId = _scenario != null ? _scenario.ID : 0,
                            Id = _newRussianGasInputScenarioNode--
                        }
                    );
                }
            }

        }

        void IGExDomain.UpdateUGSScenarioData(int id, double? maxUGSWithdrawal)
        {
            var ugsScenarioData = _scenario.UGSScenarioDataSet.Find(o => o.Obj.ID == id);
           
            if (ugsScenarioData != null)
            {
                if (maxUGSWithdrawal.HasValue)
                    ugsScenarioData.MaxUGSWithdrawal = maxUGSWithdrawal;
                else
                    _scenario.UGSScenarioDataSet.Remove(ugsScenarioData);

            }
            else
            {
                if (maxUGSWithdrawal.HasValue)
                {
                    _scenario.UGSScenarioDataSet.Add(
                        new UGSScenarioData()
                        {
                            MaxUGSWithdrawal = maxUGSWithdrawal,
                            Scenario = _scenario,
                            Obj = _scheme.Nodes.First(e => e.GasExportObject.ID == id).GasExportObject as UGS,
                            isLoaded = false,
                            ScenarioId = _scenario != null ? _scenario.ID : 0,
                            Id = _newUGSscenarioNode--
                        }
                    );
                }
            }
        }

        void IGExDomain.UpdateBorderScenario(int idNode, int idBalanceZone, DTO_Direction direct, double? ableVolumeValue, double? reservedVolumeValue, double? tarifValue, double? gsnValue, double? gsnPercentValue, string description)
        {
            Direction tmpDir = direct == DTO_Direction.Entry ? Direction.Entry : Direction.Exit;
            
            var borderScenarioData = _scenario.BorderScenarioDataSet.Find(o => o.GasExportObject.ID == idNode &&
                                                                            o.Operator.ID == idBalanceZone && o.Direction == tmpDir);
            if (borderScenarioData != null)
            {
                if (ableVolumeValue.HasValue || reservedVolumeValue.HasValue || tarifValue.HasValue || gsnValue.HasValue || gsnPercentValue.HasValue || description != null)
                {
                    borderScenarioData.AvailableCapacity = ableVolumeValue;
                    borderScenarioData.BookedCapacity = reservedVolumeValue;
                    borderScenarioData.Tariff = tarifValue;
                    borderScenarioData.GsnValue = gsnValue;
                    borderScenarioData.GsnPercentValue = gsnPercentValue;
                    borderScenarioData.Description = description;
                }
                else
                    _scenario.BorderScenarioDataSet.Remove(borderScenarioData);

            }
            else
            {
                if (ableVolumeValue.HasValue || reservedVolumeValue.HasValue || tarifValue.HasValue || gsnValue.HasValue || gsnPercentValue.HasValue || description != null)
                {
                    var tmpNode = _scheme.Nodes.First(e => e.GasExportObject.ID == idNode).GasExportObject as Border;
                    var tmpIntercon = tmpNode.Interconnections.First(e => e.ZoneFrom.ID == idBalanceZone || e.ZoneTo.ID == idBalanceZone);
                    var tmpBalZone = tmpIntercon.ZoneFrom.ID == idBalanceZone ? tmpIntercon.ZoneFrom : tmpIntercon.ZoneTo;
                    _scenario.BorderScenarioDataSet.Add(
                        new BorderScenarioData()
                        {
                            AvailableCapacity = ableVolumeValue,
                            BookedCapacity = reservedVolumeValue,
                            Tariff = tarifValue,      
                            GsnValue = gsnValue,
                            GsnPercentValue = gsnPercentValue,
                            Description = description,
                            Scenario = _scenario,
                            GasExportObject = tmpNode,
                            Direction = tmpDir,
                            Operator = tmpBalZone,                            
                            isLoaded = false,
                            ScenarioId = _scenario != null ? _scenario.ID : 0,
                            Id = _newBorderScenarioPoint--
                        }
                    );
                }
            }
        }

        bool IGExDomain.AllNodesSaved()
        {
           return !_scheme.Nodes.Any(n => n.GasExportObject != null && n.GasExportObject.ID < 0);
        }


        public bool ScenarioHasUnsavedChanges()
        {
            //TODO: определить признаки несохраненных изменений для сценария
            return false;
            //throw new NotImplementedException();
        }

        //если понадобится разнести добавлегние и сохранение
        public void AddBalanceZone(DTO_BalanceZone zone)
        {
            BalanceZone newZone = new BalanceZone()
            {
                ID = zone.ID,
                Name = zone.Name
            };
            _balanceZoneRepo.Create(newZone);
            _balanceZones.Add(newZone);
            _HasSchemaChanges = true;
            
        }
        
        void IGExDomain.UpdateBalanceZones(IEnumerable<DTO_BalanceZone> zones)
        {
            var ids = _balanceZones.Select(z => z.ID).ToArray();
           
            var idsOfCurrentZones = zones.Select(z => z.ID).ToArray();
            var idsOfOriginZonesForRemove = ids.Except(idsOfCurrentZones);

            foreach (var id in idsOfOriginZonesForRemove)
            {
                var zoneForRemove = _balanceZones.First(z => z.ID == id);
                _balanceZones.Remove(zoneForRemove);
            }
            
            foreach(var zone in zones)
            {
                var origZone = _balanceZones.Find(z=>z.ID==zone.ID);
                if (origZone == null)
                {
                    _balanceZones.Add(new BalanceZone()
                    {
                        ID = _newZoneId--,
                        Name = zone.Name
                    });
                }
                else
                    origZone.Name = zone.Name;
            } 
            
            _HasSchemaChanges = true;
        }

        void IGExDomain.SaveOperators()
        {
            _balanceZoneRepo.Update(_balanceZones);
        }
        
        bool IGExDomain.ZoneHasRelations(int idZone)
        {
            return _balanceZoneRepo.HasRelations(idZone);
        }

        bool IGExDomain.TerminalPointsSelected()
        {
            return _firstPathNodes.Any() && _lastPathNodes.Any();
        }
    }
}
