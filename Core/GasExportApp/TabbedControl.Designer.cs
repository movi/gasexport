﻿using System.Windows.Forms;
using CommonUIControls;

namespace GasExportApp
{
    partial class TabbedControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TabbedControl));
            this.ribbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.ribbonImageCollectionLarge = new DevExpress.Utils.ImageCollection();
            this.tcProperties = new DevExpress.XtraTab.XtraTabControl();
            this.tpContractsVolume = new DevExpress.XtraTab.XtraTabPage();
            this.tpExportVolume = new DevExpress.XtraTab.XtraTabPage();
            this.tpStorageExtraction = new DevExpress.XtraTab.XtraTabPage();
            this.tpPowerTarif = new DevExpress.XtraTab.XtraTabPage();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollectionLarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcProperties)).BeginInit();
            this.tcProperties.SuspendLayout();
            this.SuspendLayout();
            // 
            // ribbonControl
            // 
            this.ribbonControl.ApplicationButtonText = null;
            this.ribbonControl.ExpandCollapseItem.Id = 0;
            this.ribbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl.ExpandCollapseItem});
            this.ribbonControl.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl.MaxItemId = 1;
            this.ribbonControl.Name = "ribbonControl";
            this.ribbonControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2010;
            this.ribbonControl.Size = new System.Drawing.Size(614, 22);
            // 
            // ribbonImageCollectionLarge
            // 
            this.ribbonImageCollectionLarge.ImageSize = new System.Drawing.Size(32, 32);
            this.ribbonImageCollectionLarge.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ribbonImageCollectionLarge.ImageStream")));
            this.ribbonImageCollectionLarge.Images.SetKeyName(0, "Ribbon_New_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(1, "Ribbon_Open_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(2, "Ribbon_Close_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(3, "Ribbon_Find_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(4, "Ribbon_Save_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(5, "Ribbon_SaveAs_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(6, "Ribbon_Exit_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(7, "Ribbon_Content_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(8, "Ribbon_Info_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(9, "Cursor_16x16.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(10, "gnome-mime-application-msword.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(11, "Play_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(12, "Refresh_32x32.png");
            // 
            // tcProperties
            // 
            this.tcProperties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tcProperties.Appearance.Options.UseFont = true;
            this.tcProperties.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcProperties.Location = new System.Drawing.Point(0, 0);
            this.tcProperties.MultiLine = DevExpress.Utils.DefaultBoolean.True;
            this.tcProperties.Name = "tcProperties";
            this.tcProperties.SelectedTabPage = this.tpContractsVolume;
            this.tcProperties.Size = new System.Drawing.Size(614, 547);
            this.tcProperties.TabIndex = 1;
            this.tcProperties.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tpContractsVolume,
            this.tpExportVolume,
            this.tpStorageExtraction,
            this.tpPowerTarif});
            // 
            // tpContractsVolume
            // 
            this.tpContractsVolume.Appearance.Header.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tpContractsVolume.Appearance.Header.Options.UseFont = true;
            this.tpContractsVolume.Name = "tpContractsVolume";
            this.tpContractsVolume.Size = new System.Drawing.Size(608, 516);
            this.tpContractsVolume.Text = "Контракты";
            // 
            // tpExportVolume
            // 
            this.tpExportVolume.Appearance.Header.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tpExportVolume.Appearance.Header.Options.UseFont = true;
            this.tpExportVolume.Name = "tpExportVolume";
            this.tpExportVolume.Size = new System.Drawing.Size(608, 516);
            this.tpExportVolume.Text = "Экспорт";
            // 
            // tpStorageExtraction
            // 
            this.tpStorageExtraction.Appearance.Header.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tpStorageExtraction.Appearance.Header.Options.UseFont = true;
            this.tpStorageExtraction.Name = "tpStorageExtraction";
            this.tpStorageExtraction.Size = new System.Drawing.Size(608, 516);
            this.tpStorageExtraction.Text = "Отбор из ПХГ";
            // 
            // tpPowerTarif
            // 
            this.tpPowerTarif.Appearance.Header.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tpPowerTarif.Appearance.Header.Options.UseFont = true;
            this.tpPowerTarif.Name = "tpPowerTarif";
            this.tpPowerTarif.Size = new System.Drawing.Size(608, 516);
            this.tpPowerTarif.Text = "Тарифы и тр. мощности";
            // 
            // TabbedControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tcProperties);
            this.Name = "TabbedControl";
            this.Size = new System.Drawing.Size(614, 547);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollectionLarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcProperties)).EndInit();
            this.tcProperties.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl;
        private DevExpress.Utils.ImageCollection ribbonImageCollectionLarge;
        private DevExpress.XtraTab.XtraTabControl tcProperties;
        private DevExpress.XtraTab.XtraTabPage tpContractsVolume;
        private DevExpress.XtraTab.XtraTabPage tpExportVolume;
        private DevExpress.XtraTab.XtraTabPage tpStorageExtraction;
        private DevExpress.XtraTab.XtraTabPage tpPowerTarif;
    }
}
