﻿using System;
using System.Threading;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.Skins;
using DevExpress.UserSkins;
using DevExpress.XtraSplashScreen;
using GEx.ErrorHandler;
using GEx.Log;

namespace GasExportApp
{
    static class Program
    {
        #region Methods
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            const string errorSource = "GEx.Main()";

            try
            {
                Logger.Name = "GEx";
                Logger.Instance.Start();
                Logger.Instance.Log(eErrClass.EC_INFO, "", errorSource, -1);

                //Version vrsn = null;
                //Version.TryParse(ConfigurationManager.AppSettings["Version"], out vrsn);

                //var buildInfo = (vrsn == null ? " - " : (vrsn.MinorRevision < 0 ? vrsn.ToString(3) : vrsn.ToString(4))) +
                //                "(от " + File.GetLastWriteTime(Application.ExecutablePath).ToString("dd.MM.yy hh:mm") + ")";
#if DEBUG
                //Logger.Instance.Log(eErrClass.EC_INFO, "DEBUG version: " + buildInfo, errorSource, -1);
                //Logger.Instance.Log(eErrClass.EC_INFO, "DEBUG version: ", errorSource, -1);
#else              
                //Logger.Instance.Log(eErrClass.EC_INFO,
                //    "Release version: " + buildInfo,
                //    errorSource, -1);
#endif
                Logger.Instance.Log(eErrClass.EC_INFO, Environment.UserDomainName + "\\" + Environment.UserName,
                    errorSource, -1);

                AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
                Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
                Application.ThreadException += Application_ThreadException;
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                Environment.SetEnvironmentVariable("NLS_LANG", "RUSSIAN_RUSSIA.CL8MSWIN1251",
                    EnvironmentVariableTarget.Process);

                //Database.SetInitializer<GExDbContext>(null);

                //BonusSkins.Register();
                //SkinManager.EnableFormSkins();
                //UserLookAndFeel.Default.SetSkinStyle("Default");    

                try
                {
                    AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
                    Application.ApplicationExit += Application_ApplicationExit;
                    //DevExpress.Skins.SkinManager.Default.RegisterAssembly(typeof(DevExpress.UserSkins.KalmarSkin.KalmarSkin).Assembly);
                    //DevExpress.Skins.SkinManager.Default.RegisterAssembly(typeof(DevExpress.UserSkins.KalmarSkinScenario.KalmarSkinScenario).Assembly);
                    DevExpress.Skins.SkinManager.Default.RegisterAssembly(typeof(DevExpress.UserSkins.SkinScenario).Assembly);

                    SplashScreenManager.ShowForm(typeof(MainSplashScreen));                    
                    var form = new MainForm();
                    //AppContext.MainForm = form;
                    Application.Run(form);
                    Logger.Instance.Log(eErrClass.EC_INFO, "Нормальное завершение.", errorSource, -1);
                }
                
                //ошибка доступа к базе при инициализации
                catch (GEx.Exceptions.DBAccessFatalException ex)
                {
                    var msg = "Возникла ошибка при обращении к базе данных со следующим сообщением:\n\n" +
                              ex.Message + "\n\n" +
                              "Продолжение работы невозможно. Обратитесь к администратору системы.";
                    var caption = "Ошибка при получении доступа к базе данных";
                    
                    //в лог должны были подробно записать выше.
                    //Здесь просто показываем упрощенное окно
                    MessageBox.Show(msg,
                        "GazExportApp : " + caption,
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                catch (Exception e)
                {
                    LogException(e);
                }
            }
            catch (Exception ex)
            {
                HandleUnhandledException(ex, errorSource);
            }
            finally
            {
                Logger.Instance.Stop();
                Application.Exit();
            }
        }

        private static void LogException(Exception ex, Boolean showMsgBox = true)
        {
            if (ex != null)
            {
                const string errorSource = "Main()";
                string logmsg = ex.Source + ": " + ex.Message

                                + "\n" +
                                ex.InnerException + "\n" +
                                ex.StackTrace;

                Logger.Instance.Log(eErrClass.EC_FATAL, logmsg, errorSource, -1);

                string usermsg = ex.Source + ": " + ex.Message;
                if (showMsgBox)
                {
                    MessageBox.Show(usermsg,
                        "GazExportApp : Фатальная ошибка приложения",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
        }

        static void Application_ApplicationExit(object sender, EventArgs e)
        {
            //VersionLib.Cleaner.Clean();
        }

        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            //throw e.Exception;
            HandleUnhandledException(e.Exception, null);
            Logger.Instance.Stop();
            Application.Exit();
            //AppContext.MainForm.Hide();
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            HandleUnhandledException(e.ExceptionObject as Exception, null);
            Logger.Instance.Stop();
        }

        private static void HandleUnhandledException(Exception ex, String errSource)
        {
            string exInfo = ex.GetExceptionInfo();
            Logger.Instance.Log(eErrClass.EC_ERROR, exInfo, errSource, -1);
            string usermsg = ex.GetType() + ": " + ex.Message;
            var msg = "Произошло необработанное исключение. Программа будет закрыта. \n" + usermsg;
            MessageBox.Show(msg, "Фатальная ошибка приложения", MessageBoxButtons.OK, MessageBoxIcon.Stop);
        }
        #endregion Methods
    }
}