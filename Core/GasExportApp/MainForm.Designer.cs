﻿using System.Windows.Forms;
using CommonUIControls;
using GEx.Iface;
using GExGoDiagram;
using Northwoods.Go;

namespace GasExportApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem13 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip14 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem14 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip15 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem15 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip16 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem16 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip17 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem17 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip18 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem18 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip19 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem19 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip20 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem20 = new DevExpress.Utils.ToolTipTitleItem();
            this.fileRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.iNew = new DevExpress.XtraBars.BarButtonItem();
            this.iOpen = new DevExpress.XtraBars.BarButtonItem();
            this.iSave = new DevExpress.XtraBars.BarButtonItem();
            this.iSaveAs = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.goView = new GExGoDiagram.RasterLayerGoView();
            this.ribbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.appMenu = new DevExpress.XtraBars.Ribbon.ApplicationMenu(this.components);
            this.iExit = new DevExpress.XtraBars.BarButtonItem();
            this.barAndDockingController = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.ribbonImageCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.iClose = new DevExpress.XtraBars.BarButtonItem();
            this.iFind = new DevExpress.XtraBars.BarButtonItem();
            this.iHelp = new DevExpress.XtraBars.BarButtonItem();
            this.iAbout = new DevExpress.XtraBars.BarButtonItem();
            this.siStatus = new DevExpress.XtraBars.BarStaticItem();
            this.siInfo = new DevExpress.XtraBars.BarStaticItem();
            this.alignButtonGroup = new DevExpress.XtraBars.BarButtonGroup();
            this.iBoldFontStyle = new DevExpress.XtraBars.BarButtonItem();
            this.iItalicFontStyle = new DevExpress.XtraBars.BarButtonItem();
            this.iUnderlinedFontStyle = new DevExpress.XtraBars.BarButtonItem();
            this.fontStyleButtonGroup = new DevExpress.XtraBars.BarButtonGroup();
            this.iLeftTextAlign = new DevExpress.XtraBars.BarButtonItem();
            this.iCenterTextAlign = new DevExpress.XtraBars.BarButtonItem();
            this.iRightTextAlign = new DevExpress.XtraBars.BarButtonItem();
            this.rgbiSkins = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.matrixBI = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonGroup1 = new DevExpress.XtraBars.BarButtonGroup();
            this.bbiBalZones = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCalculate = new DevExpress.XtraBars.BarButtonItem();
            this.btnClearPaths = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddBalZone = new DevExpress.XtraBars.BarButtonItem();
            this.bbiNewScenario = new DevExpress.XtraBars.BarButtonItem();
            this.bbiOpenScenario = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveScenario = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveAsScenario = new DevExpress.XtraBars.BarButtonItem();
            this.zoomInButton = new DevExpress.XtraBars.BarButtonItem();
            this.zoomOutButton = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.beiVersion = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.iExport = new DevExpress.XtraBars.BarButtonItem();
            this.iImportUnderlayer = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonGroup2 = new DevExpress.XtraBars.BarButtonGroup();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemRadioGroup1 = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.ribbonGalleryBarItem1 = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.skinRibbonGalleryBarItem1 = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.barDockingMenuItem1 = new DevExpress.XtraBars.BarDockingMenuItem();
            this.btsMode = new DevExpress.XtraBars.BarToggleSwitchItem();
            this.ribbonImageCollectionLarge = new DevExpress.Utils.ImageCollection(this.components);
            this.homeRibbonPage = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.viewRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpEditSchema = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.exitRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.formatRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpScenario = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgScenarios = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgCalculation = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpMode = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.helpRibbonPage = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.helpRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.skinsRibbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.tcProperties = new GasExportApp.TabbedControl();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dpView = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.pcContainetScenario = new DevExpress.XtraBars.Docking.DockPanel();
            this.dpResults = new DevExpress.XtraBars.Docking.DockPanel();
            this.controlContainer1 = new DevExpress.XtraBars.Docking.ControlContainer();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtpRouts = new DevExpress.XtraTab.XtraTabPage();
            this.ctrlResults = new CommonUIControls.CtrlResults();
            this.xtpNominations = new DevExpress.XtraTab.XtraTabPage();
            this.ctrlNominations = new CommonUIControls.CtrlNominations();
            this.xtpDeliveries = new DevExpress.XtraTab.XtraTabPage();
            this.ctrlDeliveries = new CommonUIControls.CtrlDeliveries();
            this.dpPointsPath = new DevExpress.XtraBars.Docking.DockPanel();
            this.controlContainer2 = new DevExpress.XtraBars.Docking.ControlContainer();
            this.ctrlPointsPaths = new CommonUIControls.CtrlPointsPaths();
            this.dpOptions = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel3_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.appMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollectionLarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dpView.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            this.pcContainetScenario.SuspendLayout();
            this.dpResults.SuspendLayout();
            this.controlContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtpRouts.SuspendLayout();
            this.xtpNominations.SuspendLayout();
            this.xtpDeliveries.SuspendLayout();
            this.dpPointsPath.SuspendLayout();
            this.controlContainer2.SuspendLayout();
            this.dpOptions.SuspendLayout();
            this.dockPanel3_Container.SuspendLayout();
            this.SuspendLayout();
            // 
            // fileRibbonPageGroup
            // 
            this.fileRibbonPageGroup.AllowTextClipping = false;
            this.fileRibbonPageGroup.ItemLinks.Add(this.iNew);
            this.fileRibbonPageGroup.ItemLinks.Add(this.iOpen);
            this.fileRibbonPageGroup.Name = "fileRibbonPageGroup";
            this.fileRibbonPageGroup.ShowCaptionButton = false;
            this.fileRibbonPageGroup.Text = "Схема";
            // 
            // iNew
            // 
            this.iNew.Caption = "Создать";
            this.iNew.Description = "Creates a new, blank file.";
            this.iNew.Hint = "Creates a new, blank file";
            this.iNew.Id = 1;
            this.iNew.ImageIndex = 0;
            this.iNew.ItemAppearance.Disabled.Font = new System.Drawing.Font("Tahoma", 10F);
            this.iNew.ItemAppearance.Disabled.Options.UseFont = true;
            this.iNew.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.iNew.ItemAppearance.Hovered.Options.UseFont = true;
            this.iNew.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.iNew.ItemAppearance.Normal.Options.UseFont = true;
            this.iNew.ItemAppearance.Pressed.Font = new System.Drawing.Font("Tahoma", 10F);
            this.iNew.ItemAppearance.Pressed.Options.UseFont = true;
            this.iNew.LargeImageIndex = 0;
            this.iNew.Name = "iNew";
            toolTipTitleItem1.Text = "Создать новую схему";
            superToolTip1.Items.Add(toolTipTitleItem1);
            this.iNew.SuperTip = superToolTip1;
            this.iNew.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iNew_ItemClick);
            // 
            // iOpen
            // 
            this.iOpen.Caption = "Открыть";
            this.iOpen.Description = "Opens a file.";
            this.iOpen.Hint = "Opens a file";
            this.iOpen.Id = 2;
            this.iOpen.ImageIndex = 1;
            this.iOpen.ItemAppearance.Disabled.Font = new System.Drawing.Font("Tahoma", 10F);
            this.iOpen.ItemAppearance.Disabled.Options.UseFont = true;
            this.iOpen.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.iOpen.ItemAppearance.Hovered.Options.UseFont = true;
            this.iOpen.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.iOpen.ItemAppearance.Normal.Options.UseFont = true;
            this.iOpen.ItemAppearance.Pressed.Font = new System.Drawing.Font("Tahoma", 10F);
            this.iOpen.ItemAppearance.Pressed.Options.UseFont = true;
            this.iOpen.LargeImageIndex = 1;
            this.iOpen.Name = "iOpen";
            this.iOpen.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            toolTipTitleItem2.Text = "Открыть сохраненную ранее схему";
            superToolTip2.Items.Add(toolTipTitleItem2);
            this.iOpen.SuperTip = superToolTip2;
            this.iOpen.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iOpen_ItemClick);
            // 
            // iSave
            // 
            this.iSave.Caption = "Сохранить";
            this.iSave.Description = "Saves the active document.";
            this.iSave.Hint = "Saves the active document";
            this.iSave.Id = 16;
            this.iSave.ImageIndex = 4;
            this.iSave.ItemAppearance.Disabled.Font = new System.Drawing.Font("Tahoma", 10F);
            this.iSave.ItemAppearance.Disabled.Options.UseFont = true;
            this.iSave.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.iSave.ItemAppearance.Hovered.Options.UseFont = true;
            this.iSave.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.iSave.ItemAppearance.Normal.Options.UseFont = true;
            this.iSave.ItemAppearance.Pressed.Font = new System.Drawing.Font("Tahoma", 10F);
            this.iSave.ItemAppearance.Pressed.Options.UseFont = true;
            this.iSave.LargeImageIndex = 4;
            this.iSave.Name = "iSave";
            toolTipTitleItem3.Text = "Сохранить схему (обновить)";
            superToolTip3.Items.Add(toolTipTitleItem3);
            this.iSave.SuperTip = superToolTip3;
            this.iSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iSave_ItemClick);
            // 
            // iSaveAs
            // 
            this.iSaveAs.Caption = "Сохранить как";
            this.iSaveAs.Description = "Saves the active document in a different location.";
            this.iSaveAs.Hint = "Saves the active document in a different location";
            this.iSaveAs.Id = 17;
            this.iSaveAs.ImageIndex = 5;
            this.iSaveAs.ItemAppearance.Disabled.Font = new System.Drawing.Font("Tahoma", 10F);
            this.iSaveAs.ItemAppearance.Disabled.Options.UseFont = true;
            this.iSaveAs.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.iSaveAs.ItemAppearance.Hovered.Options.UseFont = true;
            this.iSaveAs.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.iSaveAs.ItemAppearance.Normal.Options.UseFont = true;
            this.iSaveAs.ItemAppearance.Pressed.Font = new System.Drawing.Font("Tahoma", 10F);
            this.iSaveAs.ItemAppearance.Pressed.Options.UseFont = true;
            this.iSaveAs.LargeImageIndex = 5;
            this.iSaveAs.Name = "iSaveAs";
            toolTipTitleItem4.Text = "Сохранить схему как новую";
            superToolTip4.Items.Add(toolTipTitleItem4);
            this.iSaveAs.SuperTip = superToolTip4;
            this.iSaveAs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iSaveAs_ItemClick);
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // goView
            // 
            this.goView.ArrowMoveLarge = 10F;
            this.goView.ArrowMoveSmall = 1F;
            this.goView.BackColor = System.Drawing.Color.White;
            this.goView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.goView.DragsRealtime = true;
            this.goView.ExternalDragDropsOnEnter = true;
            this.goView.Location = new System.Drawing.Point(0, 0);
            this.goView.MapUnderlayer = null;
            this.goView.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.goView.Name = "goView";
            this.goView.Size = new System.Drawing.Size(1042, 657);
            this.goView.TabIndex = 2;
            // 
            // ribbonControl
            // 
            this.ribbonControl.ApplicationButtonDropDownControl = this.appMenu;
            this.ribbonControl.ApplicationButtonText = null;
            this.ribbonControl.Controller = this.barAndDockingController;
            this.ribbonControl.ExpandCollapseItem.Id = 0;
            this.ribbonControl.Images = this.ribbonImageCollection;
            this.ribbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl.ExpandCollapseItem,
            this.iNew,
            this.iOpen,
            this.iClose,
            this.iFind,
            this.iSave,
            this.iSaveAs,
            this.iExit,
            this.iHelp,
            this.iAbout,
            this.siStatus,
            this.siInfo,
            this.alignButtonGroup,
            this.iBoldFontStyle,
            this.iItalicFontStyle,
            this.iUnderlinedFontStyle,
            this.fontStyleButtonGroup,
            this.iLeftTextAlign,
            this.iCenterTextAlign,
            this.iRightTextAlign,
            this.rgbiSkins,
            this.matrixBI,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5,
            this.barButtonItem6,
            this.barButtonGroup1,
            this.bbiBalZones,
            this.bbiCalculate,
            this.btnClearPaths,
            this.bbiAddBalZone,
            this.bbiNewScenario,
            this.bbiOpenScenario,
            this.bbiSaveScenario,
            this.bbiSaveAsScenario,
            this.zoomInButton,
            this.zoomOutButton,
            this.barButtonItem7,
            this.beiVersion,
            this.iExport,
            this.iImportUnderlayer,
            this.barButtonItem8,
            this.barButtonGroup2,
            this.barEditItem1,
            this.ribbonGalleryBarItem1,
            this.skinRibbonGalleryBarItem1,
            this.barDockingMenuItem1,
            this.btsMode});
            this.ribbonControl.LargeImages = this.ribbonImageCollectionLarge;
            this.ribbonControl.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.ribbonControl.MaxItemId = 15;
            this.ribbonControl.Name = "ribbonControl";
            this.ribbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.homeRibbonPage,
            this.rpEditSchema,
            this.rpScenario,
            this.helpRibbonPage});
            this.ribbonControl.QuickToolbarItemLinks.Add(this.beiVersion);
            this.ribbonControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemRadioGroup1});
            this.ribbonControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2010;
            this.ribbonControl.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl.ShowDisplayOptionsMenuButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.True;
            this.ribbonControl.ShowToolbarCustomizeItem = false;
            this.ribbonControl.Size = new System.Drawing.Size(1283, 146);
            this.ribbonControl.StatusBar = this.ribbonStatusBar;
            this.ribbonControl.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl.SelectedPageChanging += new DevExpress.XtraBars.Ribbon.RibbonPageChangingEventHandler(this.ribbonControl_SelectedPageChanging);
            this.ribbonControl.SelectedPageChanged += new System.EventHandler(this.ribbonControl_SelectedPageChanged);
            // 
            // appMenu
            // 
            this.appMenu.ItemLinks.Add(this.iNew);
            this.appMenu.ItemLinks.Add(this.iOpen);
            this.appMenu.ItemLinks.Add(this.iSave);
            this.appMenu.ItemLinks.Add(this.iSaveAs);
            this.appMenu.ItemLinks.Add(this.iExit);
            this.appMenu.Name = "appMenu";
            this.appMenu.Ribbon = this.ribbonControl;
            // 
            // iExit
            // 
            this.iExit.Caption = "Exit";
            this.iExit.Description = "Closes this program after prompting you to save unsaved data.";
            this.iExit.Hint = "Closes this program after prompting you to save unsaved data";
            this.iExit.Id = 20;
            this.iExit.ImageIndex = 6;
            this.iExit.LargeImageIndex = 6;
            this.iExit.Name = "iExit";
            // 
            // barAndDockingController
            // 
            this.barAndDockingController.AppearancesDocking.ActiveTab.Font = new System.Drawing.Font("Tahoma", 10F);
            this.barAndDockingController.AppearancesDocking.ActiveTab.Options.UseFont = true;
            this.barAndDockingController.AppearancesDocking.FloatFormCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.barAndDockingController.AppearancesDocking.FloatFormCaption.Options.UseFont = true;
            this.barAndDockingController.AppearancesDocking.FloatFormCaptionActive.Font = new System.Drawing.Font("Tahoma", 10F);
            this.barAndDockingController.AppearancesDocking.FloatFormCaptionActive.Options.UseFont = true;
            this.barAndDockingController.AppearancesDocking.Panel.Font = new System.Drawing.Font("Tahoma", 10F);
            this.barAndDockingController.AppearancesDocking.Panel.Options.UseFont = true;
            this.barAndDockingController.AppearancesDocking.PanelCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.barAndDockingController.AppearancesDocking.PanelCaption.Options.UseFont = true;
            this.barAndDockingController.AppearancesDocking.Tabs.Font = new System.Drawing.Font("Tahoma", 10F);
            this.barAndDockingController.AppearancesDocking.Tabs.Options.UseFont = true;
            this.barAndDockingController.LookAndFeel.SkinName = "SkinWorkSchema";
            this.barAndDockingController.LookAndFeel.UseDefaultLookAndFeel = false;
            this.barAndDockingController.PropertiesBar.AllowLinkLighting = false;
            this.barAndDockingController.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.barAndDockingController.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // ribbonImageCollection
            // 
            this.ribbonImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ribbonImageCollection.ImageStream")));
            this.ribbonImageCollection.Images.SetKeyName(0, "Ribbon_New_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(1, "Ribbon_Open_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(2, "Ribbon_Close_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(3, "Ribbon_Find_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(4, "Ribbon_Save_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(5, "Ribbon_SaveAs_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(6, "Ribbon_Exit_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(7, "Ribbon_Content_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(8, "Ribbon_Info_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(9, "Ribbon_Bold_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(10, "Ribbon_Italic_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(11, "Ribbon_Underline_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(12, "Ribbon_AlignLeft_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(13, "Ribbon_AlignCenter_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(14, "Ribbon_AlignRight_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(15, "Cursor_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(16, "green.png");
            this.ribbonImageCollection.Images.SetKeyName(17, "orange.png");
            this.ribbonImageCollection.Images.SetKeyName(18, "red.png");
            this.ribbonImageCollection.Images.SetKeyName(19, "violet.png");
            this.ribbonImageCollection.Images.SetKeyName(20, "Play_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(21, "Refresh_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(22, "new-icon16Scenario.png");
            this.ribbonImageCollection.Images.SetKeyName(23, "Open-icon16Scenario.png");
            this.ribbonImageCollection.Images.SetKeyName(24, "saveAs-icon16Scenario.png");
            this.ribbonImageCollection.Images.SetKeyName(25, "save-icon16Scenario.png");
            this.ribbonImageCollection.Images.SetKeyName(26, "aqua.png");
            this.ribbonImageCollection.Images.SetKeyName(27, "lightcoral-aqua.png");
            this.ribbonImageCollection.Images.SetKeyName(28, "violet-green.png");
            // 
            // iClose
            // 
            this.iClose.Caption = "Закрыть";
            this.iClose.Description = "Closes the active document.";
            this.iClose.Hint = "Closes the active document";
            this.iClose.Id = 3;
            this.iClose.ImageIndex = 2;
            this.iClose.LargeImageIndex = 2;
            this.iClose.Name = "iClose";
            this.iClose.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)((DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // iFind
            // 
            this.iFind.Caption = "Найти";
            this.iFind.Description = "Searches for the specified info.";
            this.iFind.Hint = "Searches for the specified info";
            this.iFind.Id = 15;
            this.iFind.ImageIndex = 3;
            this.iFind.LargeImageIndex = 3;
            this.iFind.Name = "iFind";
            this.iFind.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)((DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // iHelp
            // 
            this.iHelp.Caption = "Help";
            this.iHelp.Description = "Start the program help system.";
            this.iHelp.Hint = "Start the program help system";
            this.iHelp.Id = 22;
            this.iHelp.ImageIndex = 7;
            this.iHelp.LargeImageIndex = 7;
            this.iHelp.Name = "iHelp";
            this.iHelp.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // iAbout
            // 
            this.iAbout.Caption = "About";
            this.iAbout.Description = "Displays general program information.";
            this.iAbout.Hint = "Displays general program information";
            this.iAbout.Id = 24;
            this.iAbout.ImageIndex = 8;
            this.iAbout.LargeImageIndex = 8;
            this.iAbout.Name = "iAbout";
            this.iAbout.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.iAbout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iAbout_ItemClick);
            // 
            // siStatus
            // 
            this.siStatus.Id = 31;
            this.siStatus.Name = "siStatus";
            this.siStatus.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // siInfo
            // 
            this.siInfo.Id = 32;
            this.siInfo.Name = "siInfo";
            this.siInfo.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // alignButtonGroup
            // 
            this.alignButtonGroup.Caption = "Align Commands";
            this.alignButtonGroup.Id = 52;
            this.alignButtonGroup.ItemLinks.Add(this.iBoldFontStyle);
            this.alignButtonGroup.ItemLinks.Add(this.iItalicFontStyle);
            this.alignButtonGroup.ItemLinks.Add(this.iUnderlinedFontStyle);
            this.alignButtonGroup.Name = "alignButtonGroup";
            // 
            // iBoldFontStyle
            // 
            this.iBoldFontStyle.Caption = "Bold";
            this.iBoldFontStyle.Id = 53;
            this.iBoldFontStyle.ImageIndex = 9;
            this.iBoldFontStyle.Name = "iBoldFontStyle";
            // 
            // iItalicFontStyle
            // 
            this.iItalicFontStyle.Caption = "Italic";
            this.iItalicFontStyle.Id = 54;
            this.iItalicFontStyle.ImageIndex = 10;
            this.iItalicFontStyle.Name = "iItalicFontStyle";
            // 
            // iUnderlinedFontStyle
            // 
            this.iUnderlinedFontStyle.Caption = "Underlined";
            this.iUnderlinedFontStyle.Id = 55;
            this.iUnderlinedFontStyle.ImageIndex = 11;
            this.iUnderlinedFontStyle.Name = "iUnderlinedFontStyle";
            // 
            // fontStyleButtonGroup
            // 
            this.fontStyleButtonGroup.Caption = "Font Style";
            this.fontStyleButtonGroup.Id = 56;
            this.fontStyleButtonGroup.ItemLinks.Add(this.iLeftTextAlign);
            this.fontStyleButtonGroup.ItemLinks.Add(this.iCenterTextAlign);
            this.fontStyleButtonGroup.ItemLinks.Add(this.iRightTextAlign);
            this.fontStyleButtonGroup.Name = "fontStyleButtonGroup";
            // 
            // iLeftTextAlign
            // 
            this.iLeftTextAlign.Caption = "Left";
            this.iLeftTextAlign.Id = 57;
            this.iLeftTextAlign.ImageIndex = 12;
            this.iLeftTextAlign.Name = "iLeftTextAlign";
            // 
            // iCenterTextAlign
            // 
            this.iCenterTextAlign.Caption = "Center";
            this.iCenterTextAlign.Id = 58;
            this.iCenterTextAlign.ImageIndex = 13;
            this.iCenterTextAlign.Name = "iCenterTextAlign";
            // 
            // iRightTextAlign
            // 
            this.iRightTextAlign.Caption = "Right";
            this.iRightTextAlign.Id = 59;
            this.iRightTextAlign.ImageIndex = 14;
            this.iRightTextAlign.Name = "iRightTextAlign";
            // 
            // rgbiSkins
            // 
            this.rgbiSkins.Caption = "Skins";
            // 
            // 
            // 
            this.rgbiSkins.Gallery.AllowHoverImages = true;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseFont = true;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseTextOptions = true;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.rgbiSkins.Gallery.ColumnCount = 4;
            this.rgbiSkins.Gallery.FixedHoverImageSize = false;
            this.rgbiSkins.Gallery.ImageSize = new System.Drawing.Size(32, 17);
            this.rgbiSkins.Gallery.ItemImageLocation = DevExpress.Utils.Locations.Top;
            this.rgbiSkins.Gallery.RowCount = 4;
            this.rgbiSkins.Id = 60;
            this.rgbiSkins.Name = "rgbiSkins";
            // 
            // matrixBI
            // 
            this.matrixBI.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.matrixBI.Caption = "Матрица";
            this.matrixBI.Id = 62;
            this.matrixBI.ImageIndex = 7;
            this.matrixBI.LargeImageIndex = 7;
            this.matrixBI.Name = "matrixBI";
            this.matrixBI.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.matrixBI.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.matrixBI_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Матрица";
            this.barButtonItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.Glyph")));
            this.barButtonItem1.Id = 63;
            this.barButtonItem1.ItemAppearance.Disabled.Font = new System.Drawing.Font("Tahoma", 10F);
            this.barButtonItem1.ItemAppearance.Disabled.Options.UseFont = true;
            this.barButtonItem1.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.barButtonItem1.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem1.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.barButtonItem1.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem1.ItemAppearance.Pressed.Font = new System.Drawing.Font("Tahoma", 10F);
            this.barButtonItem1.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem1.LargeImageIndex = 18;
            this.barButtonItem1.Name = "barButtonItem1";
            toolTipTitleItem5.Text = "Матрица связей узлов сети";
            superToolTip5.Items.Add(toolTipTitleItem5);
            this.barButtonItem1.SuperTip = superToolTip5;
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Output";
            this.barButtonItem2.Id = 64;
            this.barButtonItem2.ImageIndex = 16;
            this.barButtonItem2.ItemAppearance.Disabled.Font = new System.Drawing.Font("Tahoma", 10F);
            this.barButtonItem2.ItemAppearance.Disabled.Options.UseFont = true;
            this.barButtonItem2.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.barButtonItem2.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem2.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.barButtonItem2.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem2.Name = "barButtonItem2";
            toolTipTitleItem6.Text = "Создать пункт сдачи";
            superToolTip6.Items.Add(toolTipTitleItem6);
            this.barButtonItem2.SuperTip = superToolTip6;
            this.barButtonItem2.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Граница России";
            this.barButtonItem3.Id = 65;
            this.barButtonItem3.ImageIndex = 17;
            this.barButtonItem3.ItemAppearance.Disabled.Font = new System.Drawing.Font("Tahoma", 10F);
            this.barButtonItem3.ItemAppearance.Disabled.Options.UseFont = true;
            this.barButtonItem3.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.barButtonItem3.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem3.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.barButtonItem3.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem3.Name = "barButtonItem3";
            toolTipTitleItem7.Text = "Создать пункт экспорта\r\n";
            superToolTip7.Items.Add(toolTipTitleItem7);
            this.barButtonItem3.SuperTip = superToolTip7;
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Обычный";
            this.barButtonItem4.Id = 66;
            this.barButtonItem4.ImageIndex = 15;
            this.barButtonItem4.LargeImageIndex = 9;
            this.barButtonItem4.Name = "barButtonItem4";
            this.barButtonItem4.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItem4.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem4_ItemClick);
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "ПХГ";
            this.barButtonItem5.Id = 67;
            this.barButtonItem5.ImageIndex = 18;
            this.barButtonItem5.ItemAppearance.Disabled.Font = new System.Drawing.Font("Tahoma", 10F);
            this.barButtonItem5.ItemAppearance.Disabled.Options.UseFont = true;
            this.barButtonItem5.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.barButtonItem5.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem5.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.barButtonItem5.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem5.Name = "barButtonItem5";
            toolTipTitleItem8.Text = "Создать ПХГ";
            superToolTip8.Items.Add(toolTipTitleItem8);
            this.barButtonItem5.SuperTip = superToolTip8;
            this.barButtonItem5.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem5_ItemClick);
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Интерконнектор";
            this.barButtonItem6.Id = 68;
            this.barButtonItem6.ImageIndex = 28;
            this.barButtonItem6.ItemAppearance.Disabled.Font = new System.Drawing.Font("Tahoma", 10F);
            this.barButtonItem6.ItemAppearance.Disabled.Options.UseFont = true;
            this.barButtonItem6.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.barButtonItem6.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem6.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.barButtonItem6.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem6.Name = "barButtonItem6";
            toolTipTitleItem9.Text = "Создать виртуальную площадку";
            superToolTip9.Items.Add(toolTipTitleItem9);
            this.barButtonItem6.SuperTip = superToolTip9;
            this.barButtonItem6.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem6_ItemClick);
            // 
            // barButtonGroup1
            // 
            this.barButtonGroup1.Caption = "barButtonGroup1";
            this.barButtonGroup1.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.barButtonGroup1.Id = 1;
            this.barButtonGroup1.Name = "barButtonGroup1";
            // 
            // bbiBalZones
            // 
            this.bbiBalZones.Caption = "Операторы";
            this.bbiBalZones.Id = 2;
            this.bbiBalZones.ItemAppearance.Disabled.Font = new System.Drawing.Font("Tahoma", 10F);
            this.bbiBalZones.ItemAppearance.Disabled.Options.UseFont = true;
            this.bbiBalZones.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.bbiBalZones.ItemAppearance.Hovered.Options.UseFont = true;
            this.bbiBalZones.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.bbiBalZones.ItemAppearance.Normal.Options.UseFont = true;
            this.bbiBalZones.ItemAppearance.Pressed.Font = new System.Drawing.Font("Tahoma", 10F);
            this.bbiBalZones.ItemAppearance.Pressed.Options.UseFont = true;
            this.bbiBalZones.LargeImageIndex = 19;
            this.bbiBalZones.Name = "bbiBalZones";
            toolTipTitleItem10.Text = "Привязка узлов сети к операторам";
            superToolTip10.Items.Add(toolTipTitleItem10);
            this.bbiBalZones.SuperTip = superToolTip10;
            this.bbiBalZones.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiBalZones_ItemClick);
            // 
            // bbiCalculate
            // 
            this.bbiCalculate.Caption = "Расчет";
            this.bbiCalculate.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.bbiCalculate.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCalculate.Glyph")));
            this.bbiCalculate.Id = 4;
            this.bbiCalculate.ImageIndex = 20;
            this.bbiCalculate.ItemAppearance.Disabled.Font = new System.Drawing.Font("Tahoma", 10F);
            this.bbiCalculate.ItemAppearance.Disabled.Options.UseFont = true;
            this.bbiCalculate.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.bbiCalculate.ItemAppearance.Hovered.Options.UseFont = true;
            this.bbiCalculate.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.bbiCalculate.ItemAppearance.Normal.Options.UseFont = true;
            this.bbiCalculate.ItemAppearance.Pressed.Font = new System.Drawing.Font("Tahoma", 10F);
            this.bbiCalculate.ItemAppearance.Pressed.Options.UseFont = true;
            this.bbiCalculate.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiCalculate.LargeGlyph")));
            this.bbiCalculate.LargeImageIndex = 11;
            this.bbiCalculate.Name = "bbiCalculate";
            toolTipTitleItem11.Text = "Произвести расчет на заданных условиях";
            superToolTip11.Items.Add(toolTipTitleItem11);
            this.bbiCalculate.SuperTip = superToolTip11;
            this.bbiCalculate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCalculate_ItemClick);
            // 
            // btnClearPaths
            // 
            this.btnClearPaths.Caption = "Убрать маршрут";
            this.btnClearPaths.Id = 1;
            this.btnClearPaths.ImageIndex = 0;
            this.btnClearPaths.ItemAppearance.Disabled.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnClearPaths.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnClearPaths.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnClearPaths.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnClearPaths.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnClearPaths.ItemAppearance.Normal.Options.UseFont = true;
            this.btnClearPaths.ItemAppearance.Pressed.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnClearPaths.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnClearPaths.LargeImageIndex = 25;
            this.btnClearPaths.Name = "btnClearPaths";
            toolTipTitleItem12.Text = "Скрыть маршрут на схеме";
            superToolTip12.Items.Add(toolTipTitleItem12);
            this.btnClearPaths.SuperTip = superToolTip12;
            this.btnClearPaths.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClearPaths_ItemClick);
            // 
            // bbiAddBalZone
            // 
            this.bbiAddBalZone.Caption = "Добавить оператора";
            this.bbiAddBalZone.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.bbiAddBalZone.Id = 2;
            this.bbiAddBalZone.Name = "bbiAddBalZone";
            // 
            // bbiNewScenario
            // 
            this.bbiNewScenario.Caption = "Создать";
            this.bbiNewScenario.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.bbiNewScenario.Id = 3;
            this.bbiNewScenario.ImageIndex = 22;
            this.bbiNewScenario.ItemAppearance.Disabled.Font = new System.Drawing.Font("Tahoma", 10F);
            this.bbiNewScenario.ItemAppearance.Disabled.Options.UseFont = true;
            this.bbiNewScenario.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.bbiNewScenario.ItemAppearance.Hovered.Options.UseFont = true;
            this.bbiNewScenario.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.bbiNewScenario.ItemAppearance.Normal.Options.UseFont = true;
            this.bbiNewScenario.ItemAppearance.Pressed.Font = new System.Drawing.Font("Tahoma", 10F);
            this.bbiNewScenario.ItemAppearance.Pressed.Options.UseFont = true;
            this.bbiNewScenario.LargeImageIndex = 13;
            this.bbiNewScenario.Name = "bbiNewScenario";
            toolTipTitleItem13.Text = "Создать новый сценарий";
            superToolTip13.Items.Add(toolTipTitleItem13);
            this.bbiNewScenario.SuperTip = superToolTip13;
            this.bbiNewScenario.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiNewScenario_ItemClick);
            // 
            // bbiOpenScenario
            // 
            this.bbiOpenScenario.Caption = "Открыть";
            this.bbiOpenScenario.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.bbiOpenScenario.Id = 4;
            this.bbiOpenScenario.ImageIndex = 23;
            this.bbiOpenScenario.ItemAppearance.Disabled.Font = new System.Drawing.Font("Tahoma", 10F);
            this.bbiOpenScenario.ItemAppearance.Disabled.Options.UseFont = true;
            this.bbiOpenScenario.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.bbiOpenScenario.ItemAppearance.Hovered.Options.UseFont = true;
            this.bbiOpenScenario.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.bbiOpenScenario.ItemAppearance.Normal.Options.UseFont = true;
            this.bbiOpenScenario.ItemAppearance.Pressed.Font = new System.Drawing.Font("Tahoma", 10F);
            this.bbiOpenScenario.ItemAppearance.Pressed.Options.UseFont = true;
            this.bbiOpenScenario.LargeImageIndex = 14;
            this.bbiOpenScenario.Name = "bbiOpenScenario";
            toolTipTitleItem14.Text = "Открыть сохраненный сценарий";
            superToolTip14.Items.Add(toolTipTitleItem14);
            this.bbiOpenScenario.SuperTip = superToolTip14;
            this.bbiOpenScenario.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiOpenScenario_ItemClick);
            // 
            // bbiSaveScenario
            // 
            this.bbiSaveScenario.Caption = "Сохранить";
            this.bbiSaveScenario.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.bbiSaveScenario.Id = 5;
            this.bbiSaveScenario.ImageIndex = 4;
            this.bbiSaveScenario.ItemAppearance.Disabled.Font = new System.Drawing.Font("Tahoma", 10F);
            this.bbiSaveScenario.ItemAppearance.Disabled.Options.UseFont = true;
            this.bbiSaveScenario.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.bbiSaveScenario.ItemAppearance.Hovered.Options.UseFont = true;
            this.bbiSaveScenario.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.bbiSaveScenario.ItemAppearance.Normal.Options.UseFont = true;
            this.bbiSaveScenario.ItemAppearance.Pressed.Font = new System.Drawing.Font("Tahoma", 10F);
            this.bbiSaveScenario.ItemAppearance.Pressed.Options.UseFont = true;
            this.bbiSaveScenario.LargeImageIndex = 15;
            this.bbiSaveScenario.Name = "bbiSaveScenario";
            toolTipTitleItem15.Text = "Сохранить сценарий (обновить)";
            superToolTip15.Items.Add(toolTipTitleItem15);
            this.bbiSaveScenario.SuperTip = superToolTip15;
            this.bbiSaveScenario.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSaveScenario_ItemClick);
            // 
            // bbiSaveAsScenario
            // 
            this.bbiSaveAsScenario.Caption = "Сохранить как";
            this.bbiSaveAsScenario.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.bbiSaveAsScenario.Id = 6;
            this.bbiSaveAsScenario.ImageIndex = 5;
            this.bbiSaveAsScenario.ItemAppearance.Disabled.Font = new System.Drawing.Font("Tahoma", 10F);
            this.bbiSaveAsScenario.ItemAppearance.Disabled.Options.UseFont = true;
            this.bbiSaveAsScenario.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.bbiSaveAsScenario.ItemAppearance.Hovered.Options.UseFont = true;
            this.bbiSaveAsScenario.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.bbiSaveAsScenario.ItemAppearance.Normal.Options.UseFont = true;
            this.bbiSaveAsScenario.ItemAppearance.Pressed.Font = new System.Drawing.Font("Tahoma", 10F);
            this.bbiSaveAsScenario.ItemAppearance.Pressed.Options.UseFont = true;
            this.bbiSaveAsScenario.LargeImageIndex = 16;
            this.bbiSaveAsScenario.Name = "bbiSaveAsScenario";
            toolTipTitleItem16.Text = "Сохранить сценарий как новый";
            superToolTip16.Items.Add(toolTipTitleItem16);
            this.bbiSaveAsScenario.SuperTip = superToolTip16;
            this.bbiSaveAsScenario.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSaveAsScenario_ItemClick);
            // 
            // zoomInButton
            // 
            this.zoomInButton.Caption = "Увеличить";
            this.zoomInButton.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.zoomInButton.Id = 1;
            this.zoomInButton.ItemAppearance.Disabled.Font = new System.Drawing.Font("Tahoma", 10F);
            this.zoomInButton.ItemAppearance.Disabled.Options.UseFont = true;
            this.zoomInButton.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.zoomInButton.ItemAppearance.Hovered.Options.UseFont = true;
            this.zoomInButton.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.zoomInButton.ItemAppearance.Normal.Options.UseFont = true;
            this.zoomInButton.ItemAppearance.Pressed.Font = new System.Drawing.Font("Tahoma", 10F);
            this.zoomInButton.ItemAppearance.Pressed.Options.UseFont = true;
            this.zoomInButton.LargeImageIndex = 22;
            this.zoomInButton.Name = "zoomInButton";
            this.zoomInButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.zoomInButton_ItemClick);
            // 
            // zoomOutButton
            // 
            this.zoomOutButton.Caption = "Уменьшить";
            this.zoomOutButton.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.zoomOutButton.Id = 2;
            this.zoomOutButton.ItemAppearance.Disabled.Font = new System.Drawing.Font("Tahoma", 10F);
            this.zoomOutButton.ItemAppearance.Disabled.Options.UseFont = true;
            this.zoomOutButton.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 10F);
            this.zoomOutButton.ItemAppearance.Hovered.Options.UseFont = true;
            this.zoomOutButton.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.zoomOutButton.ItemAppearance.Normal.Options.UseFont = true;
            this.zoomOutButton.ItemAppearance.Pressed.Font = new System.Drawing.Font("Tahoma", 10F);
            this.zoomOutButton.ItemAppearance.Pressed.Options.UseFont = true;
            this.zoomOutButton.LargeImageIndex = 23;
            this.zoomOutButton.Name = "zoomOutButton";
            this.zoomOutButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.zoomOutButton_ItemClick);
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "ВТП";
            this.barButtonItem7.Id = 3;
            this.barButtonItem7.ImageIndex = 27;
            this.barButtonItem7.Name = "barButtonItem7";
            toolTipTitleItem17.Text = "Создать виртуальную торговую площадку";
            superToolTip17.Items.Add(toolTipTitleItem17);
            this.barButtonItem7.SuperTip = superToolTip17;
            this.barButtonItem7.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem7_ItemClick_1);
            // 
            // beiVersion
            // 
            this.beiVersion.CategoryGuid = new System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537");
            this.beiVersion.Description = "Версия программы";
            this.beiVersion.Edit = this.repositoryItemTextEdit1;
            this.beiVersion.EditWidth = 120;
            this.beiVersion.Enabled = false;
            this.beiVersion.Id = 4;
            this.beiVersion.ItemAppearance.Disabled.BackColor = System.Drawing.Color.Transparent;
            this.beiVersion.ItemAppearance.Disabled.Options.UseBackColor = true;
            this.beiVersion.ItemInMenuAppearance.Disabled.BackColor = System.Drawing.Color.Transparent;
            this.beiVersion.ItemInMenuAppearance.Disabled.Options.UseBackColor = true;
            this.beiVersion.Name = "beiVersion";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemTextEdit1.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.repositoryItemTextEdit1.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.repositoryItemTextEdit1.Appearance.Options.UseBackColor = true;
            this.repositoryItemTextEdit1.Appearance.Options.UseBorderColor = true;
            this.repositoryItemTextEdit1.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemTextEdit1.AppearanceDisabled.BackColor2 = System.Drawing.Color.Transparent;
            this.repositoryItemTextEdit1.AppearanceDisabled.BorderColor = System.Drawing.Color.Transparent;
            this.repositoryItemTextEdit1.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemTextEdit1.AppearanceDisabled.Options.UseBorderColor = true;
            this.repositoryItemTextEdit1.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black;
            this.repositoryItemTextEdit1.AppearanceReadOnly.Options.UseForeColor = true;
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            this.repositoryItemTextEdit1.ReadOnly = true;
            // 
            // iExport
            // 
            this.iExport.Caption = "Экспорт";
            this.iExport.Id = 5;
            this.iExport.LargeImageIndex = 26;
            this.iExport.Name = "iExport";
            toolTipTitleItem18.Text = "Экспорт в графический формат";
            superToolTip18.Items.Add(toolTipTitleItem18);
            this.iExport.SuperTip = superToolTip18;
            this.iExport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iExport_ItemClick);
            // 
            // iImportUnderlayer
            // 
            this.iImportUnderlayer.Caption = "Импорт подложки";
            this.iImportUnderlayer.Id = 6;
            this.iImportUnderlayer.LargeImageIndex = 27;
            this.iImportUnderlayer.Name = "iImportUnderlayer";
            toolTipTitleItem19.Text = "Импорт графической подложки";
            superToolTip19.Items.Add(toolTipTitleItem19);
            this.iImportUnderlayer.SuperTip = superToolTip19;
            this.iImportUnderlayer.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iImportUnderlayer_ItemClick);
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "barButtonItem8";
            this.barButtonItem8.Id = 7;
            this.barButtonItem8.Name = "barButtonItem8";
            // 
            // barButtonGroup2
            // 
            this.barButtonGroup2.Id = 9;
            this.barButtonGroup2.Name = "barButtonGroup2";
            this.barButtonGroup2.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "barEditItem1";
            this.barEditItem1.Edit = this.repositoryItemRadioGroup1;
            this.barEditItem1.Id = 10;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemRadioGroup1
            // 
            this.repositoryItemRadioGroup1.Name = "repositoryItemRadioGroup1";
            // 
            // ribbonGalleryBarItem1
            // 
            this.ribbonGalleryBarItem1.Caption = "ribbonGalleryBarItem1";
            this.ribbonGalleryBarItem1.Id = 11;
            this.ribbonGalleryBarItem1.Name = "ribbonGalleryBarItem1";
            // 
            // skinRibbonGalleryBarItem1
            // 
            this.skinRibbonGalleryBarItem1.Caption = "skinRibbonGalleryBarItem1";
            this.skinRibbonGalleryBarItem1.Id = 12;
            this.skinRibbonGalleryBarItem1.Name = "skinRibbonGalleryBarItem1";
            // 
            // barDockingMenuItem1
            // 
            this.barDockingMenuItem1.Caption = "barDockingMenuItem1";
            this.barDockingMenuItem1.Id = 13;
            this.barDockingMenuItem1.Name = "barDockingMenuItem1";
            // 
            // btsMode
            // 
            this.btsMode.BindableChecked = true;
            this.btsMode.Caption = "Маршруты/Контракты";
            this.btsMode.Checked = true;
            this.btsMode.Id = 14;
            this.btsMode.Name = "btsMode";
            toolTipTitleItem20.Text = "Выбор режима";
            superToolTip20.Items.Add(toolTipTitleItem20);
            this.btsMode.SuperTip = superToolTip20;
            this.btsMode.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barToggleSwitchItem1_CheckedChanged);
            // 
            // ribbonImageCollectionLarge
            // 
            this.ribbonImageCollectionLarge.ImageSize = new System.Drawing.Size(32, 32);
            this.ribbonImageCollectionLarge.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ribbonImageCollectionLarge.ImageStream")));
            this.ribbonImageCollectionLarge.Images.SetKeyName(0, "Ribbon_New_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(1, "Ribbon_Open_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(2, "Ribbon_Close_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(3, "Ribbon_Find_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(4, "Ribbon_Save_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(5, "Ribbon_SaveAs_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(6, "Ribbon_Exit_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(7, "Ribbon_Content_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(8, "Ribbon_Info_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(9, "Cursor_16x16.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(10, "gnome-mime-application-msword.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(11, "Play_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(12, "Refresh_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(13, "Ribbon_New_Calc_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(14, "Ribbon_Open_Calc_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(15, "Ribbon_Save_Calc_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(16, "Ribbon_SaveAs_Calc_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(17, "Routes_32_32.dib");
            this.ribbonImageCollectionLarge.Images.SetKeyName(18, "BalZones_32_32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(19, "List_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(20, "Zoom In.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(21, "Zoom Out.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(22, "Zoom In_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(23, "Zoom Out_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(24, "clear32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(25, "clear32_color.png");
            this.ribbonImageCollectionLarge.InsertGalleryImage("importmap_32x32.png", "images/maps/importmap_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/maps/importmap_32x32.png"), 26);
            this.ribbonImageCollectionLarge.Images.SetKeyName(26, "importmap_32x32.png");
            this.ribbonImageCollectionLarge.InsertGalleryImage("defaultmap_32x32.png", "images/maps/defaultmap_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/maps/defaultmap_32x32.png"), 27);
            this.ribbonImageCollectionLarge.Images.SetKeyName(27, "defaultmap_32x32.png");
            // 
            // homeRibbonPage
            // 
            this.homeRibbonPage.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.homeRibbonPage.Appearance.Options.UseFont = true;
            this.homeRibbonPage.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.fileRibbonPageGroup,
            this.ribbonPageGroup2,
            this.ribbonPageGroup5,
            this.viewRibbonPageGroup});
            this.homeRibbonPage.Name = "homeRibbonPage";
            this.homeRibbonPage.Text = "Схемы";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.iSave);
            this.ribbonPageGroup2.ItemLinks.Add(this.iSaveAs);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "Сохранить";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.iExport);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.Text = "Экспорт";
            // 
            // viewRibbonPageGroup
            // 
            this.viewRibbonPageGroup.ItemLinks.Add(this.zoomInButton);
            this.viewRibbonPageGroup.ItemLinks.Add(this.zoomOutButton);
            this.viewRibbonPageGroup.Name = "viewRibbonPageGroup";
            this.viewRibbonPageGroup.Text = "Вид";
            // 
            // rpEditSchema
            // 
            this.rpEditSchema.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.rpEditSchema.Appearance.Options.UseFont = true;
            this.rpEditSchema.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.exitRibbonPageGroup,
            this.formatRibbonPageGroup,
            this.ribbonPageGroup6,
            this.ribbonPageGroup1,
            this.ribbonPageGroup4});
            this.rpEditSchema.Name = "rpEditSchema";
            this.rpEditSchema.Text = "Редактор схем";
            // 
            // exitRibbonPageGroup
            // 
            this.exitRibbonPageGroup.AllowTextClipping = false;
            this.exitRibbonPageGroup.ItemLinks.Add(this.barButtonItem1);
            this.exitRibbonPageGroup.Name = "exitRibbonPageGroup";
            this.exitRibbonPageGroup.Text = "Матрица";
            // 
            // formatRibbonPageGroup
            // 
            this.formatRibbonPageGroup.ItemLinks.Add(this.barButtonItem2);
            this.formatRibbonPageGroup.ItemLinks.Add(this.barButtonItem3);
            this.formatRibbonPageGroup.ItemLinks.Add(this.barButtonItem5);
            this.formatRibbonPageGroup.ItemLinks.Add(this.barButtonItem6);
            this.formatRibbonPageGroup.ItemLinks.Add(this.barButtonItem7);
            this.formatRibbonPageGroup.Name = "formatRibbonPageGroup";
            this.formatRibbonPageGroup.Text = "Создать...";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ItemLinks.Add(this.iImportUnderlayer);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.Text = "Импорт";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.bbiBalZones);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Операторы";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.zoomInButton);
            this.ribbonPageGroup4.ItemLinks.Add(this.zoomOutButton);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "Вид";
            // 
            // rpScenario
            // 
            this.rpScenario.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.rpScenario.Appearance.Options.UseFont = true;
            this.rpScenario.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgScenarios,
            this.rpgCalculation,
            this.ribbonPageGroup3,
            this.rpMode});
            this.rpScenario.Name = "rpScenario";
            this.rpScenario.Text = "Расчеты";
            // 
            // rpgScenarios
            // 
            this.rpgScenarios.AllowTextClipping = false;
            this.rpgScenarios.ItemLinks.Add(this.bbiNewScenario);
            this.rpgScenarios.ItemLinks.Add(this.bbiOpenScenario);
            this.rpgScenarios.ItemLinks.Add(this.bbiSaveScenario);
            this.rpgScenarios.ItemLinks.Add(this.bbiSaveAsScenario);
            this.rpgScenarios.Name = "rpgScenarios";
            this.rpgScenarios.ShowCaptionButton = false;
            this.rpgScenarios.Text = "Сценарий";
            // 
            // rpgCalculation
            // 
            this.rpgCalculation.ItemLinks.Add(this.bbiCalculate);
            this.rpgCalculation.ItemLinks.Add(this.btnClearPaths);
            this.rpgCalculation.Name = "rpgCalculation";
            this.rpgCalculation.Text = "Расчет";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.zoomInButton);
            this.ribbonPageGroup3.ItemLinks.Add(this.zoomOutButton);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Вид";
            // 
            // rpMode
            // 
            this.rpMode.ItemLinks.Add(this.btsMode);
            this.rpMode.Name = "rpMode";
            this.rpMode.Text = "Режимы";
            // 
            // helpRibbonPage
            // 
            this.helpRibbonPage.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.helpRibbonPage.Appearance.Options.UseFont = true;
            this.helpRibbonPage.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.helpRibbonPageGroup});
            this.helpRibbonPage.Name = "helpRibbonPage";
            this.helpRibbonPage.Text = "Помощь";
            this.helpRibbonPage.Visible = false;
            // 
            // helpRibbonPageGroup
            // 
            this.helpRibbonPageGroup.ItemLinks.Add(this.iHelp);
            this.helpRibbonPageGroup.ItemLinks.Add(this.iAbout);
            this.helpRibbonPageGroup.Name = "helpRibbonPageGroup";
            this.helpRibbonPageGroup.Text = "Help";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.siStatus);
            this.ribbonStatusBar.ItemLinks.Add(this.siInfo);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 831);
            this.ribbonStatusBar.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbonControl;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1283, 31);
            // 
            // skinsRibbonPageGroup
            // 
            this.skinsRibbonPageGroup.ItemLinks.Add(this.rgbiSkins);
            this.skinsRibbonPageGroup.Name = "skinsRibbonPageGroup";
            this.skinsRibbonPageGroup.ShowCaptionButton = false;
            this.skinsRibbonPageGroup.Text = "Skins";
            // 
            // tcProperties
            // 
            this.tcProperties.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcProperties.Location = new System.Drawing.Point(0, 0);
            this.tcProperties.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.tcProperties.Name = "tcProperties";
            this.tcProperties.Size = new System.Drawing.Size(223, 627);
            this.tcProperties.TabIndex = 0;
            // 
            // dockManager1
            // 
            this.dockManager1.Controller = this.barAndDockingController;
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dpView,
            this.pcContainetScenario});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
            // 
            // dpView
            // 
            this.dpView.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.dpView.Appearance.Options.UseFont = true;
            this.dpView.Controls.Add(this.dockPanel1_Container);
            this.dpView.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dpView.FloatVertical = true;
            this.dpView.ID = new System.Guid("c5ae9222-8dda-4baa-971e-3664f3bb88d1");
            this.dpView.Location = new System.Drawing.Point(0, 146);
            this.dpView.Margin = new System.Windows.Forms.Padding(3, 17, 3, 17);
            this.dpView.Name = "dpView";
            this.dpView.Options.AllowFloating = false;
            this.dpView.Options.FloatOnDblClick = false;
            this.dpView.Options.ShowAutoHideButton = false;
            this.dpView.Options.ShowCloseButton = false;
            this.dpView.OriginalSize = new System.Drawing.Size(1051, 200);
            this.dpView.Size = new System.Drawing.Size(1051, 685);
            this.dpView.TabsPosition = DevExpress.XtraBars.Docking.TabsPosition.Top;
            this.dpView.Text = "Схема";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.goView);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 24);
            this.dockPanel1_Container.Margin = new System.Windows.Forms.Padding(3, 21, 3, 21);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(1042, 657);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // pcContainetScenario
            // 
            this.pcContainetScenario.ActiveChild = this.dpResults;
            this.pcContainetScenario.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.pcContainetScenario.Appearance.Options.UseFont = true;
            this.pcContainetScenario.Controls.Add(this.dpPointsPath);
            this.pcContainetScenario.Controls.Add(this.dpOptions);
            this.pcContainetScenario.Controls.Add(this.dpResults);
            this.pcContainetScenario.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.pcContainetScenario.FloatVertical = true;
            this.pcContainetScenario.ID = new System.Guid("f975d19e-d829-44b9-bf08-10f6b511d742");
            this.pcContainetScenario.Location = new System.Drawing.Point(1051, 146);
            this.pcContainetScenario.Margin = new System.Windows.Forms.Padding(3, 17, 3, 17);
            this.pcContainetScenario.Name = "pcContainetScenario";
            this.pcContainetScenario.OriginalSize = new System.Drawing.Size(232, 200);
            this.pcContainetScenario.Size = new System.Drawing.Size(232, 685);
            this.pcContainetScenario.Tabbed = true;
            this.pcContainetScenario.TabsPosition = DevExpress.XtraBars.Docking.TabsPosition.Top;
            this.pcContainetScenario.Text = "panelContainer1";
            // 
            // dpResults
            // 
            this.dpResults.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.dpResults.Appearance.Options.UseFont = true;
            this.dpResults.Controls.Add(this.controlContainer1);
            this.dpResults.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.dpResults.FloatVertical = true;
            this.dpResults.ID = new System.Guid("b382cbb0-b75a-4d73-bb3e-d47254aa1733");
            this.dpResults.Location = new System.Drawing.Point(5, 54);
            this.dpResults.Margin = new System.Windows.Forms.Padding(3, 59, 3, 59);
            this.dpResults.Name = "dpResults";
            this.dpResults.Options.FloatOnDblClick = false;
            this.dpResults.Options.ShowAutoHideButton = false;
            this.dpResults.Options.ShowCloseButton = false;
            this.dpResults.OriginalSize = new System.Drawing.Size(225, 627);
            this.dpResults.Size = new System.Drawing.Size(223, 627);
            this.dpResults.TabsPosition = DevExpress.XtraBars.Docking.TabsPosition.Top;
            this.dpResults.TabText = "Расчет";
            this.dpResults.Text = "Расчет";
            this.dpResults.Click += new System.EventHandler(this.dpResults_Click);
            // 
            // controlContainer1
            // 
            this.controlContainer1.Controls.Add(this.xtraTabControl1);
            this.controlContainer1.Location = new System.Drawing.Point(0, 0);
            this.controlContainer1.Margin = new System.Windows.Forms.Padding(3, 73, 3, 73);
            this.controlContainer1.Name = "controlContainer1";
            this.controlContainer1.Size = new System.Drawing.Size(223, 627);
            this.controlContainer1.TabIndex = 0;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.xtraTabControl1.Appearance.Options.UseFont = true;
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtpRouts;
            this.xtraTabControl1.Size = new System.Drawing.Size(223, 627);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtpRouts,
            this.xtpNominations,
            this.xtpDeliveries});
            // 
            // xtpRouts
            // 
            this.xtpRouts.Appearance.Header.Font = new System.Drawing.Font("Tahoma", 10F);
            this.xtpRouts.Appearance.Header.Options.UseFont = true;
            this.xtpRouts.Controls.Add(this.ctrlResults);
            this.xtpRouts.Margin = new System.Windows.Forms.Padding(3, 39, 3, 39);
            this.xtpRouts.Name = "xtpRouts";
            this.xtpRouts.Size = new System.Drawing.Size(217, 596);
            this.xtpRouts.Text = "Маршруты";
            // 
            // ctrlResults
            // 
            this.ctrlResults.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctrlResults.Location = new System.Drawing.Point(0, 0);
            this.ctrlResults.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.ctrlResults.Name = "ctrlResults";
            this.ctrlResults.Size = new System.Drawing.Size(217, 596);
            this.ctrlResults.TabIndex = 1;
            // 
            // xtpNominations
            // 
            this.xtpNominations.Controls.Add(this.ctrlNominations);
            this.xtpNominations.Name = "xtpNominations";
            this.xtpNominations.Size = new System.Drawing.Size(217, 596);
            this.xtpNominations.Text = "Номинации";
            // 
            // ctrlNominations
            // 
            this.ctrlNominations.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctrlNominations.Location = new System.Drawing.Point(0, 0);
            this.ctrlNominations.Name = "ctrlNominations";
            this.ctrlNominations.Size = new System.Drawing.Size(217, 596);
            this.ctrlNominations.TabIndex = 0;
            // 
            // xtpDeliveries
            // 
            this.xtpDeliveries.Controls.Add(this.ctrlDeliveries);
            this.xtpDeliveries.Name = "xtpDeliveries";
            this.xtpDeliveries.Size = new System.Drawing.Size(217, 596);
            this.xtpDeliveries.Text = "Недопоставки";
            // 
            // ctrlDeliveries
            // 
            this.ctrlDeliveries.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctrlDeliveries.Location = new System.Drawing.Point(0, 0);
            this.ctrlDeliveries.Name = "ctrlDeliveries";
            this.ctrlDeliveries.Size = new System.Drawing.Size(217, 596);
            this.ctrlDeliveries.TabIndex = 0;
            // 
            // dpPointsPath
            // 
            this.dpPointsPath.Controls.Add(this.controlContainer2);
            this.dpPointsPath.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.dpPointsPath.ID = new System.Guid("256326a4-a86e-4151-acf5-d0a94b9a7c5b");
            this.dpPointsPath.Location = new System.Drawing.Point(5, 54);
            this.dpPointsPath.Margin = new System.Windows.Forms.Padding(3, 59, 3, 59);
            this.dpPointsPath.Name = "dpPointsPath";
            this.dpPointsPath.OriginalSize = new System.Drawing.Size(200, 200);
            this.dpPointsPath.Size = new System.Drawing.Size(223, 627);
            this.dpPointsPath.Text = "Точки маршрутов";
            // 
            // controlContainer2
            // 
            this.controlContainer2.Controls.Add(this.ctrlPointsPaths);
            this.controlContainer2.Location = new System.Drawing.Point(0, 0);
            this.controlContainer2.Name = "controlContainer2";
            this.controlContainer2.Size = new System.Drawing.Size(223, 627);
            this.controlContainer2.TabIndex = 0;
            // 
            // ctrlPointsPaths
            // 
            this.ctrlPointsPaths.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctrlPointsPaths.Location = new System.Drawing.Point(0, 0);
            this.ctrlPointsPaths.Name = "ctrlPointsPaths";
            this.ctrlPointsPaths.Size = new System.Drawing.Size(223, 627);
            this.ctrlPointsPaths.TabIndex = 0;
            // 
            // dpOptions
            // 
            this.dpOptions.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.dpOptions.Appearance.Options.UseFont = true;
            this.dpOptions.Controls.Add(this.dockPanel3_Container);
            this.dpOptions.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.dpOptions.FloatVertical = true;
            this.dpOptions.ID = new System.Guid("bc507763-a277-4ed0-b0b3-f941f8a40bf7");
            this.dpOptions.Location = new System.Drawing.Point(5, 54);
            this.dpOptions.Margin = new System.Windows.Forms.Padding(3, 59, 3, 59);
            this.dpOptions.Name = "dpOptions";
            this.dpOptions.Options.FloatOnDblClick = false;
            this.dpOptions.Options.ShowAutoHideButton = false;
            this.dpOptions.Options.ShowCloseButton = false;
            this.dpOptions.OriginalSize = new System.Drawing.Size(225, 627);
            this.dpOptions.Size = new System.Drawing.Size(223, 627);
            this.dpOptions.TabsPosition = DevExpress.XtraBars.Docking.TabsPosition.Top;
            this.dpOptions.TabText = "Сценарий";
            this.dpOptions.Text = "Сценарий";
            // 
            // dockPanel3_Container
            // 
            this.dockPanel3_Container.Controls.Add(this.tcProperties);
            this.dockPanel3_Container.Location = new System.Drawing.Point(0, 0);
            this.dockPanel3_Container.Margin = new System.Windows.Forms.Padding(3, 73, 3, 73);
            this.dockPanel3_Container.Name = "dockPanel3_Container";
            this.dockPanel3_Container.Size = new System.Drawing.Size(223, 627);
            this.dockPanel3_Container.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.False;
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1283, 862);
            this.Controls.Add(this.pcContainetScenario);
            this.Controls.Add(this.dpView);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbonControl);
            this.Font = new System.Drawing.Font("Tahoma", 10F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "MainForm";
            this.Ribbon = this.ribbonControl;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.appMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollectionLarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dpView.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            this.pcContainetScenario.ResumeLayout(false);
            this.dpResults.ResumeLayout(false);
            this.controlContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtpRouts.ResumeLayout(false);
            this.xtpNominations.ResumeLayout(false);
            this.xtpDeliveries.ResumeLayout(false);
            this.dpPointsPath.ResumeLayout(false);
            this.controlContainer2.ResumeLayout(false);
            this.dpOptions.ResumeLayout(false);
            this.dockPanel3_Container.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl;
        private DevExpress.XtraBars.BarButtonItem iNew;
        private DevExpress.XtraBars.BarButtonItem iOpen;
        private DevExpress.XtraBars.BarButtonItem iClose;
        private DevExpress.XtraBars.BarButtonItem iFind;
        private DevExpress.XtraBars.BarButtonItem iSave;
        private DevExpress.XtraBars.BarButtonItem iSaveAs;
        private DevExpress.XtraBars.BarButtonItem iExit;
        private DevExpress.XtraBars.BarButtonItem iHelp;
        private DevExpress.XtraBars.BarButtonItem iAbout;
        private DevExpress.XtraBars.BarStaticItem siStatus;
        private DevExpress.XtraBars.BarStaticItem siInfo;
        private DevExpress.XtraBars.BarButtonGroup alignButtonGroup;
        private DevExpress.XtraBars.BarButtonItem iBoldFontStyle;
        private DevExpress.XtraBars.BarButtonItem iItalicFontStyle;
        private DevExpress.XtraBars.BarButtonItem iUnderlinedFontStyle;
        private DevExpress.XtraBars.BarButtonGroup fontStyleButtonGroup;
        private DevExpress.XtraBars.BarButtonItem iLeftTextAlign;
        private DevExpress.XtraBars.BarButtonItem iCenterTextAlign;
        private DevExpress.XtraBars.BarButtonItem iRightTextAlign;
        private DevExpress.XtraBars.RibbonGalleryBarItem rgbiSkins;
        private DevExpress.XtraBars.Ribbon.RibbonPage homeRibbonPage;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup fileRibbonPageGroup;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup formatRibbonPageGroup;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup exitRibbonPageGroup;
        private DevExpress.XtraBars.Ribbon.RibbonPage helpRibbonPage;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup helpRibbonPageGroup;
        private DevExpress.XtraBars.Ribbon.ApplicationMenu appMenu;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.Utils.ImageCollection ribbonImageCollection;
        private DevExpress.Utils.ImageCollection ribbonImageCollectionLarge;
        private RasterLayerGoView goView;
        private DevExpress.XtraBars.BarButtonItem matrixBI;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup skinsRibbonPageGroup;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonGroup barButtonGroup1;
        private DevExpress.XtraBars.BarButtonItem bbiBalZones;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem bbiCalculate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private TabbedControl tcProperties;
        private DevExpress.XtraBars.BarButtonItem btnClearPaths;
        private DevExpress.XtraBars.Docking.DockPanel dpOptions;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel3_Container;
        private DevExpress.XtraBars.Docking.DockPanel dpView;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraBars.BarButtonItem bbiAddBalZone;
        private DevExpress.XtraBars.BarButtonItem bbiNewScenario;
        private DevExpress.XtraBars.BarButtonItem bbiOpenScenario;
        private DevExpress.XtraBars.BarButtonItem bbiSaveScenario;
        private DevExpress.XtraBars.BarButtonItem bbiSaveAsScenario;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpEditSchema;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpScenario;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgScenarios;
        private DevExpress.XtraBars.Docking.DockPanel dpResults;
        private DevExpress.XtraBars.Docking.ControlContainer controlContainer1;
        private DevExpress.XtraBars.Docking.DockPanel pcContainetScenario;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtpRouts;
        private CtrlResults ctrlResults;
        private DevExpress.XtraBars.BarButtonItem zoomInButton;
        private DevExpress.XtraBars.BarButtonItem zoomOutButton;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup viewRibbonPageGroup;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgCalculation;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarAndDockingController barAndDockingController;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.BarEditItem beiVersion;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.BarButtonItem iExport;
        private DevExpress.XtraBars.BarButtonItem iImportUnderlayer;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.BarButtonGroup barButtonGroup2;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup repositoryItemRadioGroup1;
        private DevExpress.XtraBars.RibbonGalleryBarItem ribbonGalleryBarItem1;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem1;
        private DevExpress.XtraBars.BarDockingMenuItem barDockingMenuItem1;
        private DevExpress.XtraBars.Docking.DockPanel dpPointsPath;
        private DevExpress.XtraBars.Docking.ControlContainer controlContainer2;
        private CtrlPointsPaths ctrlPointsPaths;
        private DevExpress.XtraTab.XtraTabPage xtpNominations;
        private CtrlNominations ctrlNominations;
        private DevExpress.XtraTab.XtraTabPage xtpDeliveries;
        private CtrlDeliveries ctrlDeliveries;
        private DevExpress.XtraBars.BarToggleSwitchItem btsMode;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpMode;
    }
}
