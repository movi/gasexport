﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace GasExportApp
{
    public static class ExceptionExtension
    {
        public static String GetExceptionInfo(this Exception ex, Boolean includeReflectionTypeLoadExceptionInformation = true)
        {
            if (ex == null) return null;
            var info = ex.ToString();
            if (includeReflectionTypeLoadExceptionInformation)
            {
                var exceptions = FindException<ReflectionTypeLoadException>(ex);
                if (exceptions.Count > 0)
                {
                    var sb = new StringBuilder(info.Length * 2);
                    sb.AppendLine(info);
                    sb.AppendLine("-------------------------");
                    sb.AppendLine("Loader exceptions information:");
                    for (var i = 0; i < exceptions.Count; i++)
                    {
                        if (exceptions[i].LoaderExceptions != null && exceptions[i].LoaderExceptions.Length > 0)
                        {
                            foreach (var e1 in exceptions[i].LoaderExceptions)
                            {
                                sb.AppendLine(e1.ToString());
                            }
                            if (i < exceptions.Count - 1) sb.AppendLine("=========================");
                        }
                    }
                    info = sb.ToString();
                }
            }
            return info;
        }

        private static IList<T> FindException<T>(Exception ex) where T: Exception
        {
            var exceptions = new List<T>();
            if (ex != null) 
            {
                if (ex is T) exceptions.Add((T)ex);
                exceptions.AddRange(FindException<T>(ex.InnerException));
            }
            return exceptions;
        }
    }
}
