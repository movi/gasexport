﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars.Docking;
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraBars.Docking2010.Views.Tabbed;
using System.ComponentModel;
using DevExpress.XtraBars.Docking2010.Views;
using DevExpress.Utils;
using System.Drawing;
using DevExpress.XtraBars;

namespace GasExportApp.Helpers
{
    /// <summary>
    /// Объект, помогающий создавать DockPanel'и и размещать на них контролы, производить dock-операции на ними  
    /// </summary>
    public class DockAssistant
    {
        #region Fields

        /// <summary>
        /// Флаг, показывающий, можно ли возбуждать события
        /// </summary>
        /// <remarks>По умолчанию имеет значение true</remarks>
        private Boolean _canRaiseEvents;

        /// <summary>
        /// Контрол, для которого текущий объект будет создавать dock-окна
        /// </summary>
        private readonly ContainerControl _containerControl;

        /// <summary>
        /// Словарь, содержащий контролы, для которых были созданны DockPanel'и
        /// Ключ - контрол, для которого была создана DockPanel, на которой он был размещён
        /// Значение - DockPanel, которая была создана для размещения на ней контрола
        /// </summary>
        private readonly Dictionary<Control, DockPanel> _controls;

        /// <summary>
        /// Словарь, содержащий DockPanel'и, которые были созданы для размещения на них контролов
        /// Ключ - DockPanel, которая была создана для размещения на ней контрола
        /// Значение - контрол, для которого была создана DockPanel, на которой он был размещён
        /// </summary>
        private readonly Dictionary<DockPanel, Control> _controlsByPanels;

        /// <summary>
        /// Объект, управляющий DockPanel'ями
        /// </summary>
        private readonly DockManager _dockManager;

        /// <summary>
        /// Объект, позволяющий создавать DockPanel'и в виде закладок
        /// </summary>
        private readonly DocumentManager _documentManager;        

        /// <summary>
        /// Представление для _documentManager, отображающее созданные DockPanel'и в виде закладок
        /// </summary>
        private readonly TabbedView _tabbedView;

        private Boolean _updating;

        #endregion Fields

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public Boolean CanRaiseEvents 
        {
            get { return _canRaiseEvents; }
            set { _canRaiseEvents = value; }
        }

        /// <summary>
        /// Активная DockPanel
        /// </summary>
        public DockPanel ActivePanel 
        {
            get 
            {
                if (_dockManager.ActivePanel != null)
                {
                    return _dockManager.ActivePanel;
                } 
                if (_documentManager.View.ActiveDocument != null && _documentManager.View.ActiveDocument.IsDockPanel)
                {
                    return _documentManager.View.ActiveDocument.Control as DockPanel;
                }
                return null;
            }
            set { _dockManager.ActivePanel = value; }
        }

        /// <summary>
        /// Активный контрол, т.е. контрол, находящийся на активной DockPanel'и
        /// </summary>
        public Control ActiveControl 
        {
            get { return GetControlByDockPanel(this.ActivePanel); }
            set 
            { 
                this.ActivePanel = GetDockPanelByControl(value); 
            }
        } 

        /// <summary>
        /// Контролы, которые были добавлены через этот объект на DockPanel'и
        /// </summary>
        public IEnumerable<Control> Controls { get { return _controls.Keys; } }

        /// <summary>
        /// Панели, на которые были добавлены контролы
        /// </summary>
        public IEnumerable<DockPanel> Panels { get { return _controls.Values; } }

        #endregion Properties

        #region .ctors

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="containerControl">Контрол, на котором будут создаваться Dock-панели</param>
        public DockAssistant(ContainerControl containerControl)
        {                
            if (containerControl == null)
            {
                throw new ArgumentNullException("containerControl", "Контрол, для которого создаётся DockAssistant, равен null");
            }

            _containerControl = containerControl;

            _controls = new Dictionary<Control, DockPanel>();
            _controlsByPanels = new Dictionary<DockPanel, Control>();

            _canRaiseEvents = true;

            _documentManager = new DocumentManager();            
            _tabbedView = new TabbedView();            
            _dockManager = new DockManager();

            ((ISupportInitialize)(_documentManager)).BeginInit();
            ((ISupportInitialize)(_tabbedView)).BeginInit();                        
            ((ISupportInitialize)(_dockManager)).BeginInit();

            _documentManager.ShowToolTips = DefaultBoolean.True;
            _documentManager.ContainerControl = _containerControl;
            _documentManager.View = _tabbedView;
            _documentManager.ViewCollection.Add(_tabbedView);

            _dockManager.Form = _containerControl;            
            _dockManager.DockingOptions.ShowCaptionImage = true;
            _dockManager.TopZIndexControls.AddRange(new String[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
            _dockManager.ClosingPanel += DockManagerClosingPanelEventHandlerEvent;
            _dockManager.ClosedPanel += DockManagerClosedPanelEventHandlerEvent;
            _dockManager.ActivePanelChanged += DockManagerActivePanelChanged;
            
            _tabbedView.UseLoadingIndicator = DefaultBoolean.True;
            _tabbedView.FloatingDocumentContainer = FloatingDocumentContainer.SingleDocument;            
            _tabbedView.DocumentActivated += TabbedViewDocumentActivatedEventHandler;

            ((ISupportInitialize)(_documentManager)).EndInit();
            ((ISupportInitialize)(_tabbedView)).EndInit();
            ((ISupportInitialize)(_dockManager)).EndInit();            
        }

        private void TabbedViewDocumentActivatedEventHandler(object sender, DocumentEventArgs e)
        {
            var args = new ActivePanelChangedEventArgs(e.Document.Control as DockPanel, null);
            RaiseActiveDockPanelChangedEvent(args);            
        }

        #endregion .ctors

        #region Methods

        public void BeginUpdate()
        {
            if (!_updating)
            {
                _updating = true;
                _dockManager.BeginUpdate();
            }
        }

        public void EndUpdate()
        {
            if (_updating)
            {
                _dockManager.EndUpdate();
                _updating = false;
            }
        }

        /// <summary>
        /// Изменить видимость панели
        /// </summary>
        /// <param name="panel">Панель</param>
        /// <param name="visible">Видимость</param>
        public void ChangeVisibility(DockPanel panel, Boolean visible)
        {
            var doc = _documentManager.View.Documents.FirstOrDefault(d => d.Control == panel);
            if (doc == null)
            {
                panel.Visibility = visible ? DockVisibility.Visible : DockVisibility.Hidden;
            }
            else
            {                
                _tabbedView.SetGroupsVisibility(new DocumentGroup[] { _tabbedView.DocumentGroups[0] }, visible);
            }            
        }

        /// <summary>
        /// Удалить все DockPanel'и, созданные при помощи этого объекта
        /// </summary>
        public void Clear()
        {
            var args = RaiseOperationProcessingEvent(_controls.Values, OperationType.Clear, _controls.Keys);
            if (args != null && !args.Cancel)
            {
                var dockPanels = new List<DockPanel>(_controls.Values);
                var controls = new List<Control>(_controls.Keys);

                _controls.Clear();
                _controlsByPanels.Clear();
                _documentManager.View.Documents.Clear();
                _dockManager.Clear();                

                RaiseOperationCompletedEvent(dockPanels, OperationType.Clear, controls);
            }            
        }

        /// <summary>
        /// Удалить все DockPanel'и, созданные при помощи этого объекта
        /// </summary>
        /// <param name="type">Тип контролов, которые содержат DockPanel'и, подлежащие удалению</param>
        public void Clear(Type type)
        {
            if (type == null)
            {
                return;
            }

            var controls = _controls.Where(c => type.IsAssignableFrom(c.Key.GetType())).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            if (controls.Count > 0)
            {               
                var args = RaiseOperationProcessingEvent(controls.Values, OperationType.Clear, controls.Keys);
                if (args != null && !args.Cancel)
                {
                    var ctrls = new List<Control>(controls.Keys);
                    var panels = new List<DockPanel>(controls.Values);

                    BeginUpdate();

                    _canRaiseEvents = false;
                    while (controls.Count > 0)
                    {
                        Remove(controls.Keys.First());
                        controls.Remove(controls.Keys.First());
                    }
                    _canRaiseEvents = true;
                                       
                    RaiseOperationCompletedEvent(panels, OperationType.Clear, ctrls);

                    EndUpdate();
                }
            }
        }

        public DockPanel Dock(Control contentControl, DockingStyle style)
        {            
            return Dock(contentControl, style, null);
        }

        public DockPanel Dock(Control contentControl, DockingStyle style, Control neighbourControl)
        {
            if (contentControl == null)
            {
                return null;
            }           

            if (style == DockingStyle.Fill)
            {
                throw new ArgumentException("Значение параметра типа DockingStyle не может быть равно значению Fill", "style");
            }

            BeginUpdate();

            var panel = _dockManager.AddPanel(Point.Empty);
            contentControl.Dock = DockStyle.Fill;
            contentControl.Parent = panel;

            _controls.Add(contentControl, panel);
            _controlsByPanels.Add(panel, contentControl);

            DockPanel neighbourPanel = null;
            if (neighbourControl != null)
            {
                if (_controls.ContainsKey(neighbourControl))
                {
                    neighbourPanel = _controls[neighbourControl];                    
                }
            }
            if (neighbourPanel != null) panel.DockTo(neighbourPanel, style);
            else panel.DockTo(style);                      

            EndUpdate();

            RaiseOperationCompletedEvent(panel, OperationType.Add, contentControl);

            return panel;
        }

        public DockPanel DockAsTab(Control contentControl, Control neighbourControl)
        {
            if (contentControl == null)
            {
                return null;
            }
            if (neighbourControl == null)
            {
                return null;
            }
            if (!_controls.ContainsKey(neighbourControl))
            {
                return null;
            }

            BeginUpdate();

            var panel = _dockManager.AddPanel(Point.Empty);
            contentControl.Dock = DockStyle.Fill;
            contentControl.Parent = panel;

            _controls.Add(contentControl, panel);
            _controlsByPanels.Add(panel, contentControl);

            var neighbourPanel = _controls[neighbourControl];
            panel.DockAsTab(neighbourPanel);
            panel.Index = neighbourPanel.Index + 1;

            EndUpdate();

            RaiseOperationCompletedEvent(panel, OperationType.Add, contentControl);

            return panel;
        }

        public DockPanel DockAsTabbedDocument(Control contentControl)
        {
            if (contentControl == null)
            {
                return null;
            }

            BeginUpdate();

            var panel = _dockManager.AddPanel(DockingStyle.Left);
            panel.Text = null;
            panel.DockAsMdiDocument();
            contentControl.Dock = DockStyle.Fill;
            contentControl.Parent = panel;

            _controls.Add(contentControl, panel);
            _controlsByPanels.Add(panel, contentControl);

            EndUpdate();

            RaiseOperationCompletedEvent(panel, OperationType.Add, contentControl);

            return panel;
        }

        /// <summary>
        /// Получить контрол, который располагается на заданной DockPanel'и
        /// </summary>
        /// <param name="dockPanel">DockPanel</param>
        /// <returns>Контрол, который располагается на заданной DockPanel'и</returns>
        public Control GetControlByDockPanel(DockPanel dockPanel)
        {
            if (dockPanel == null || !_controlsByPanels.ContainsKey(dockPanel))
            {
                return null;
            }

            return _controlsByPanels[dockPanel];
        }

        /// <summary>
        /// Получить DockPanel, на которой располагается заданнsй контрол
        /// </summary>
        /// <param name="contentControl">Контрол</param>
        /// <returns>DockPanel, на которой располагается заданнsй контрол</returns>
        public DockPanel GetDockPanelByControl(Control contentControl)
        {
            if (contentControl == null || !_controls.ContainsKey(contentControl))
            {
                return null;
            }

            return _controls[contentControl];
        }

        public BaseDocument GetDocumentByDockPanel(DockPanel panel)
        {
            if (panel != null)
            {
                foreach (var doc in _documentManager.View.Documents)
                {
                    if (doc.Control == panel)
                    {
                        return doc;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Инициализировать
        /// </summary>
        /// <param name="barAndDockingController"></param>
        public void Init(BarAndDockingController barAndDockingController)
        {                        
            _documentManager.BarAndDockingController = barAndDockingController;
        }

        /// <summary>
        /// Вызвать событие OnOperationCompleted
        /// </summary>        
        /// <param name="e">Аргумент события</param>
        private void RaiseActiveDockPanelChangedEvent(ActivePanelChangedEventArgs e)
        {            
            if (_canRaiseEvents && OnActiveDockPanelChanged != null)
            {
                OnActiveDockPanelChanged(this, e);
            }            
        }

        /// <summary>
        /// Вызвать событие OnOperationCompleted
        /// </summary>        
        /// <param name="dockPanel">DockPanel, над которой была произведена операция</param>
        /// <param name="opType">Тип операции, которая была произведена над dockPanel'ью</param>
        /// <param name="control">Контрол, который располагается на DockPanel'и, над которой была произведена операция</param>
        /// <returns>Аргумент события</returns>
        private OperationEventArgs RaiseOperationCompletedEvent(DockPanel dockPanel, OperationType opType, Control control)
        {
            OperationEventArgs args = null;
            if (_canRaiseEvents && OnOperationCompleted != null)
            {
                args = new OperationEventArgs(dockPanel, opType, control);
                OnOperationCompleted(this, args);
            }
            return args;
        }

        /// <summary>
        /// Вызвать событие OnOperationCompleted
        /// </summary>        
        /// <param name="dockPanels">DockPanel'и, над которыми была произведена операция</param>
        /// <param name="opType">Тип операции, которая была произведена над dockPanel'ями</param>
        /// <param name="controls">Контролы, которые располагаются на DockPanel'ях, над которыми была произведена операция</param>
        /// <returns>Аргумент события</returns>
        private OperationEventArgs RaiseOperationCompletedEvent(IEnumerable<DockPanel> dockPanels, OperationType opType, IEnumerable<Control> controls)
        {
            OperationEventArgs args = null;
            if (_canRaiseEvents && OnOperationCompleted != null)
            {
                args = new OperationEventArgs(dockPanels, opType, controls);
                OnOperationCompleted(this, args);
            }
            return args;
        }

        /// <summary>
        /// Вызвать событие RaiseOperationProcessingEvent
        /// </summary>        
        /// <param name="dockPanel">DockPanel, над которой производится операция</param>
        /// <param name="opType">Тип операции, которая произведится над dockPanel'ью</param>
        /// <param name="control">Контрол, который располагается на DockPanel'и, над которой производится операция</param>
        /// <returns>Аргумент события</returns>
        private OperationCancelEventArgs RaiseOperationProcessingEvent(DockPanel dockPanel, OperationType opType, Control control)
        {
            OperationCancelEventArgs args = null;
            if (_canRaiseEvents && OnOperationProcessing != null)
            {
                args = new OperationCancelEventArgs(new OperationEventArgs(dockPanel, opType, control));
                OnOperationProcessing(this, args);
            }
            return args;
        }

        /// <summary>
        /// Вызвать событие RaiseOperationProcessingEvent
        /// </summary>        
        /// <param name="dockPanels">DockPanel's, над которыми производится операция</param>
        /// <param name="opType">Тип операции, которая произведится над dockPanel'ью</param>
        /// <param name="controls">Контролы, которые располагаются на DockPanel'ях, над которыми производится операция</param>
        /// <returns>Аргумент события</returns>
        private OperationCancelEventArgs RaiseOperationProcessingEvent(IEnumerable<DockPanel> dockPanels, OperationType opType, IEnumerable<Control> controls)
        {
            OperationCancelEventArgs args = null;
            if (_canRaiseEvents && OnOperationProcessing != null)
            {
                args = new OperationCancelEventArgs(new OperationEventArgs(dockPanels, opType, controls));
                OnOperationProcessing(this, args);
            }
            return args;
        }

        ///// <summary>
        ///// Закрыть DockPanel вместе с содержащимся на ней контролом
        ///// </summary>
        ///// <param name="contentControl">Контрол, который содержится на DockPanel'и, которую требуется закрыть</param>
        ///// <returns></returns>
        //public Boolean Close(Control contentControl)
        //{
        //    if (contentControl == null)
        //    {
        //        return false;
        //    }

        //    var removedPanel = _controls[contentControl];
        //    return Close(removedPanel);
        //}

        /// <summary>
        /// Удалить DockPanel вместе с содержащимся на ней контролом
        /// </summary>
        /// <param name="contentControl">Контрол, который содержится на DockPanel'и, которую требуется удалить</param>
        /// <returns><c>true</c> - Контрол был удалён, <c>false</c> контрол удалён не был</returns>
        public Boolean Remove(Control contentControl)
        {
            if (contentControl == null || !_controls.ContainsKey(contentControl))
            {
                return false;
            }

            var removedPanel = _controls[contentControl];
            return Remove(removedPanel);
        }

        ///// <summary>
        ///// Закрыть DockPanel вместе с содержащимся на ней контролом
        ///// </summary>
        ///// <param name="panel">DockPanel, которую требуется закрыть</param>
        ///// <returns></returns>
        //public Boolean Close(DockPanel panel)
        //{
        //    if (panel == null || !_dockManager.Panels.Contains(panel))
        //    {
        //        return false;
        //    }

        //    panel.Close();           
        //    return true;
        //}

        /// <summary>
        /// Удаляит DockPanel вместе с содержащимся на ней контролом
        /// </summary>
        /// <param name="panel">DockPanel, которую требуется удалить</param>
        /// <returns><c>true</c> - Панель была удалена, <c>false</c> панель удалена не была</returns>
        public Boolean Remove(DockPanel panel)
        {
            if (panel == null || !_controlsByPanels.ContainsKey(panel))
            {
                return false;
            }
            
            var removedControl = _controlsByPanels[panel];
            _controls.Remove(removedControl);
            _controlsByPanels.Remove(panel);
            _documentManager.View.RemoveDocument(panel);
            _dockManager.RemovePanel(panel);
            RaiseOperationCompletedEvent(panel, OperationType.Remove, removedControl);

            return true;
        }

        #region Events handlers

        /// <summary>
        /// Обработчик события изменения активной DockPanel'и
        /// </summary>
        /// <param name="sender">Источник события</param>
        /// <param name="e">Аргумент события</param>
        private void DockManagerActivePanelChanged(object sender, ActivePanelChangedEventArgs e)
        {
            RaiseActiveDockPanelChangedEvent(e);
        }

        /// <summary>
        /// Обработчик события, которое возникает после закрытия DockPanel'и
        /// </summary>
        /// <param name="sender">Источник события</param>
        /// <param name="e">Аргумент события</param>
        private void DockManagerClosedPanelEventHandlerEvent(object sender, DockPanelEventArgs e)
        {
            Remove(e.Panel);
            //var removedControl = _controlsByPanels[e.Panel];
            //_controls.Remove(removedControl);
            //_controlsByPanels.Remove(e.Panel);
            //_documentManager.View.RemoveDocument(removedControl);
            //_dockManager.RemovePanel(e.Panel);
            //RaiseOperationCompletedEvent(e.Panel, OperationType.Remove, removedControl);
        }

        /// <summary>
        /// Обработчик события закрытия DockPanel'и
        /// </summary>
        /// <param name="sender">Источник события</param>
        /// <param name="e">Аргумент события</param>
        private void DockManagerClosingPanelEventHandlerEvent(object sender, DockPanelCancelEventArgs e)
        {
            var args = RaiseOperationProcessingEvent(e.Panel, OperationType.Remove, _controlsByPanels[e.Panel]);
            if (args != null)
            {
                e.Cancel = args.Cancel;                 
            }
        }

        ///// <summary>
        ///// Обработчик события, которое возникает после закрытия DockPanel'и в виде закладки
        ///// </summary>
        ///// <param name="sender">Источник события</param>
        ///// <param name="e">Аргумент события</param>
        //private void ViewDocumentClosedEventHandler(object sender, DocumentEventArgs e)
        //{
        //    var dockPanel = e.Document.Control as DockPanel;
        //    RaiseOperationCompletedEvent(dockPanel, OperationType.Remove, _controlsByPanels[dockPanel]);
        //}

        ///// <summary>
        ///// Обработчик события закрытия DockPanel'и в виде закладки
        ///// </summary>
        ///// <param name="sender">Источник события</param>
        ///// <param name="e">Аргумент события</param>
        //private void ViewDocumentClosingEventHandler(object sender, DocumentCancelEventArgs e)
        //{
        //    var dockPanel = e.Document.Control as DockPanel;
        //    var args = RaiseOperationProcessingEvent(dockPanel, OperationType.Remove, _controlsByPanels[dockPanel]);
        //    if (args != null)
        //    {
        //        e.Cancel = args.Cancel;
        //    }
        //}

        #endregion Events handlers

        #endregion Methods

        #region Events

        /// <summary>
        /// Событие, возникающее при смене активной DockPanel'и
        /// </summary>
        public event ActivePanelChangedEventHandler OnActiveDockPanelChanged;        

        /// <summary>
        /// Событие, которое возникает в процессе выполнения добавления или удаления DockPanel'и
        /// </summary>
        public event EventHandler<OperationCancelEventArgs> OnOperationProcessing;

        /// <summary>
        /// Событие, которое возникает в случае окончания выполнения добавления или удаления DockPanel'и
        /// </summary>
        public event EventHandler<OperationEventArgs> OnOperationCompleted;

        #endregion Events
    }

    /// <summary>
    /// Тип операции
    /// </summary>
    public enum OperationType
    {
        /// <summary>
        /// Добавить
        /// </summary>
        Add = 1,
        /// <summary>
        /// Удалить
        /// </summary>
        Remove = 2,
        /// <summary>
        /// Очистить
        /// </summary>
        Clear = 3
    }

    public class OperationEventArgs : EventArgs
    {
        #region Fields

        #endregion Fields

        #region Properties

        public DockPanel Panel { get; private set; }

        public IEnumerable<DockPanel> Panels { get; private set; }

        public OperationType OpType { get; private set; }

        public Control ContentControl { get; private set; }

        public IEnumerable<Control> ContentControls { get; private set; }

        #endregion Properties

        #region .ctors

        internal OperationEventArgs(DockPanel panel, OperationType opType, Control contentControl)
        {
            Panel = panel;
            Panels = new DockPanel[] { panel };
            OpType = opType;
            ContentControl = contentControl;
            ContentControls = new Control[] { contentControl };
        }

        internal OperationEventArgs(IEnumerable<DockPanel> panels, OperationType opType, IEnumerable<Control> contentControls)
        {
            Panels = panels;
            this.Panel = panels.Count() == 1 ? panels.First() : null;
            OpType = opType;
            ContentControls = contentControls;
            ContentControl = contentControls.Count() == 1 ? contentControls.First() : null;
        }

        #endregion .ctors
    }

    public class OperationCancelEventArgs : CancelEventArgs
    {
        #region Fields

        #endregion Fields

        #region Properties

        /// <summary>
        /// Объект, содержащий в себе всю необходимую информацию о событии
        /// </summary>
        public OperationEventArgs Args { get; private set; }

        #endregion Properties

        #region .ctors

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="args">Объект, содержащий в себе всю необходимую информацию о событии</param>
        internal OperationCancelEventArgs(OperationEventArgs args)
        {
            Args = args;
        }

        #endregion .ctors
    }
}

