﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using CommonUIControls;
using CommonUIControls.MatrixControl;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Docking;
using DevExpress.XtraBars.Helpers;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraSplashScreen;
using GasExportApp.Helpers;
using GasExportApp.Properties;
using GEx.ErrorHandler;
using GEx.Iface;
using GExGoDiagram;
using GExGoDiagram.GoTools;
using Northwoods.Go;
using Settings = GExGoDiagram.Settings;
using Mailing;

namespace GasExportApp
{
    public partial class MainForm : RibbonForm, IGExDomainCallback, IMainForm
    {
        private const string MDIFileConfigSection = "MDIfile";
        private IErrorOutput _errorHandler = new ErrorHandlerImpl();
        private MainFormPresenter _presenter;
        private CreateTool _createTool;
        private DragViewTool _dragTool;
        private IGoTool _tempTool;

        /// <summary>
        /// Объект для помощи в Docking'е окон
        /// </summary>
        protected readonly DockAssistant _dockAssistant;
        private IGoTool _goTool;
        private Cursor _defaultCursor;

        public MainForm()
        {
            InitializeComponent();

            xtpNominations.PageVisible = false;
            xtpDeliveries.PageVisible = false;

            //_dockAssistant = new DockAssistant(this);
            //var view = _dockAssistant.DockAsTabbedDocument(goView);  

            //try
            //{
            //    string mdifilename = GetMDIFileName();
            //    clearOldMDIFileVersions(mdifilename);

            //    if (File.Exists(mdifilename))
            //        dockManager1.RestoreLayoutFromXml(mdifilename);
            //    else
            //    {
            //        dpView.DockAsMdiDocument();
            //        dpView.Dock = DockingStyle.Fill;                    
            //    }
            //}
            //catch 
            //{
                /*Если файла нет, то все окна будут установлены по умолчанию*/
                _errorHandler.ReportError(eErrClass.EC_WARN, "Положение MDI окон будет установлено по умолчанию.", "MainForm::MainForm()");
                dpView.DockAsMdiDocument();
                dpView.Dock = DockingStyle.Fill;                
            //}
            dpView.Text = "Схема";
            InitSkinGallery();            
            //создание новой схемы при запуске
            //splitContainerControl1.PanelVisibility = DevExpress.XtraEditors.SplitPanelVisibility.Panel1;
            ExtraUIInit();            
            InitMainMenu();
            BindEventHandlers();
            SetMode(_presenter.Mode);
        }

        private void BindEventHandlers()
        {
            dockManager1.StartDocking += (s, e) => e.Cancel = true;
        }

        private void clearOldMDIFileVersions(string mdifilename)
        {
            if (mdifilename.Length < 13) return;
            string s = mdifilename.Substring(0, mdifilename.Length - 7) + "*.xml";
            var olaFiles = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, s);
            foreach (var strOldFile in olaFiles)
            {
                if (strOldFile != AppDomain.CurrentDomain.BaseDirectory + mdifilename)
                    File.Delete(strOldFile);
            }
        }

        private string GetMDIFileName()
        {
            return ConfigurationManager.AppSettings[MDIFileConfigSection];
        }

        private void InitMainMenu()
        {
            formatRibbonPageGroup.Enabled = 
            exitRibbonPageGroup.Enabled = 
            ribbonPageGroup1.Enabled = 
            iSaveAs.Enabled = 
            rpgScenarios.Enabled =
            _presenter.IsSchemaLoaded;

            var scheme = _presenter.GetScheme();
            
            iSave.Enabled = _presenter.IsSchemaLoaded && scheme.Author != null && scheme.Author.ToUpperInvariant().Equals((Environment.UserDomainName + "\\" + Environment.UserName).ToUpperInvariant());

            rpgCalculation .Enabled = bbiSaveAsScenario.Enabled = _presenter.IsSchemaLoaded && _presenter.Scenario != null;

            bbiSaveScenario.Enabled = _presenter.IsSchemaLoaded && _presenter.Scenario != null && 
                _presenter.Scenario.Author != null &&
                _presenter.Scenario.Author.ToUpperInvariant().Equals((Environment.UserDomainName + "\\" + Environment.UserName).ToUpperInvariant()); ;
        }

        private void InitSkinGallery()
        {
            SkinHelper.InitSkinGallery(rgbiSkins, true);
        }

        private void ExtraUIInit()
        {
            var f = new GoDiagramFactory();

            goView.SelectionMoved += goView_SelectionMoved;
            goView.DocumentChanged += GoDocumentChanged;            
            goView.SelectionDeleting += goView_SelectionDeleting;
            _presenter = new MainFormPresenter(_errorHandler, goView, f, this);
            _presenter.Init();
            _presenter.AddChildPresenter(this);

            CalcResultsInit();
            
            PointsPathsInit();

            tcProperties.Init(_presenter, _errorHandler);

            _createTool = new CreateTool(goView);
            _createTool.OnCreateNewNode += CreateNewNode;
            _createTool.ExitTool += SetDefaultTool;

            _dragTool = new DragViewTool(goView);
            
            _tempTool = goView.Tool;
            _defaultCursor = Cursor;

            WindowState = FormWindowState.Maximized;
            
            Text = Resources.ApplicationName;

            goView.DocScale = 1/2.5f;
            
            goView.SheetStyle = GoViewSheetStyle.Sheet;

            StartZoomAndDrag();
        }

        void goView_SelectionDeleting(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //TODO:обработка возможности удаления (условное удаление по желанию пользователя,
            //т.к. результаты расчета будут неконсистентны при удалении узла, попавшего в маршрут
            if (goView.Selection.OfType<INodeView>().Count() > 0 && 
                _presenter.HasCalculatedResults() && MessageBox.Show("При удалении расчеты будут очищены. Продолжить?", "Удаление объекта", MessageBoxButtons.YesNo) != System.Windows.Forms.DialogResult.Yes)
            {
                e.Cancel = true; 
                return;
            }            
        }

        private void CalcResultsInit()
        {
            //Инициализация контрола с результатами расчета
            var ctrl = (CtrlResults) xtpRouts.Controls[0];
            var childPresenter = new ResultsPresenter(_presenter.CoreHolder, _errorHandler, ctrl);
            ctrl.OnPathSelect += _presenter.ShowCalculated;
            ctrl.OnEdgeSelect += _presenter.SelectEdgeInPath;
            ctrl.SetPresenter(childPresenter);
            ctrl.SetCoreHolder(_presenter.CoreHolder);
            _presenter.AddChildPresenter(childPresenter);

            //Инициализация контрола с номинациями
            var ctrlNom = (CtrlNominations) xtpNominations.Controls[0];
            var nPresenter = new NominationsPresenter(_presenter.CoreHolder, _errorHandler, ctrlNom);
            ctrlNom.SetPresenter(nPresenter);
            _presenter.AddChildPresenter(nPresenter);

            //Инициализация контрола с недопоставками
            var ctrlDev = (CtrlDeliveries)xtpDeliveries.Controls[0];
            var dPresenter = new DeliveriesPresenter(_presenter.CoreHolder, _errorHandler, ctrlDev);
            ctrlDev.SetPresenter(dPresenter);
            _presenter.AddChildPresenter(dPresenter);
        }

        private void PointsPathsInit()
        {
            ctrlPointsPaths.ChoseFirstPoint += _presenter.AddFirstPoint;
            ctrlPointsPaths.RemoveChoseFirstPoint += _presenter.RemovePoint;
            ctrlPointsPaths.ChoseLastPoint += _presenter.AddLastPoint;
            ctrlPointsPaths.RemoveChoseLastPoint += _presenter.RemovePoint;

            ctrlPointsPaths.Init();
        }

        private void GoDocumentChanged(object sender, GoChangedEventArgs e)
        {
            if (e.GoObject != null &&
                (e.GoObject is GazExportGoNode || e.GoObject.TopLevelObject is GazExportGoNode && e.GoObject is GoText))
            {
                INodeView goNode = e.GoObject.TopLevelObject as GazExportGoNode;

                switch (e.Hint)
                {
                    case GoLayer.ChangedObject:
                    {
                        if (e.SubHint == GoText.ChangedText)
                        {
                            _presenter.UpdateNodeName(goNode);
                        }
                        if (e.SubHint == Settings.IsOutputChangedSubHint /*при изменении Output*/)
                        {
                            _presenter.UpdateNodeOutput(goNode);
                        }
                        break;
                    }
                    case GoLayer.RemovedObject:
                    {
                        _presenter.RemoveNode(goNode);                        
                        break;
                    }
                }
            }
        }


        private void goView_SelectionMoved(object sender, EventArgs e)
        {
            foreach (var goNode in goView.Selection.OfType<INodeView>())
            {
                _presenter.ChangeNodeLocation(goNode);
            }
            
            foreach (var label in goView.Selection.OfType<ILabelView>())
            {
                var node = label.Parent;
                _presenter.ChangeNodeLabelPosition(node.NodeDocument, node.LabelX, node.LabelY);
            }
        }

        private void CreateNewNode(GazExportGoNode node)
        {
            _presenter.AddCreatedVisualNodeToScheme(node);            
            SetDefaultTool();            
        }

        private void OnLoad()
        {
            _presenter.OnLoad();
            
            goView.IsBackGroundImageVisible = _presenter.IsSchemaLoaded;

            tcProperties.InitTabCtrls(_presenter.Scenario != null);
            InitMainMenu();            
            UpdateFormCaption();
        }

        private void OnLoadScenario()
        {
            _presenter.OnLoadScenario();            
            tcProperties.InitTabCtrls(_presenter.Scenario != null);
            dpOptions.Text = _presenter.ScenarioName.Length > 0 ? "Сценарий: " + _presenter.ScenarioName : "Сценарий";
            InitMainMenu();
            
            _presenter.ClearLinksGoView();
            _presenter.ClearPointsGoViewPresenterCoreHolderMainForm();
            _presenter.ClearDictMode();
        }

        private void OnSave()
        {
            try
            {
                OnWaitCursor();
                _presenter.OnSave();
                UpdateFormCaption();
            }
            finally
            {
                OnDefaultCursor();
            }
        }
       
        
        /// <summary>
        /// Установка GoDiagram-тула для
        /// добавления новых узлов
        /// </summary>
        /// <param name="gasExportObjectType">
        /// Тип узла, который мы будем добавлять
        /// </param>
        private void SetAddTool(DTO_Node_type type)
        {
            goView.Tool = _createTool;
            _createTool.NodeType = type;
        }
        
        /// <summary>
        /// Возврат к GoDiagram-tool по умолчанию.
        /// Обыкновенно, это курсор-стрелка для Selection
        /// </summary>
        private void SetDefaultTool()
        {
            goView.Tool = _tempTool;
        }
        
        /// <summary>
        /// Обработчик кнопки "Новая схема"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void iNew_ItemClick(object sender, ItemClickEventArgs e)
        {
            const string errorSource = "MainForm::iNew_ItemClick()";
            try
            {
                //Проверка на имеющиеся несохраненные изменения в текущем сценарии
                if (_presenter.ScenarioHasUnsavedChanges() && 
                    MessageBox.Show("В текущем сценарии есть несохраненные изменения, хотите их сохранить?", "Изменения в сценарии", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    return;
                }

                _presenter.CreateNew(_presenter.GetScheme() != null && _presenter.GetSchemaNodes().Any() && MessageBox.Show("Создать схему на основе текущей?", "Создание новой схемы", MessageBoxButtons.YesNo) == DialogResult.Yes);
                //teCurrentScenarioName.Text = _presenter.GetScenario().Name;
                //_ctrlScenariosList.InitControlData(_presenter.GetCoreHolder().LoadCalculationScenarios(), _presenter.GetCoreHolder().GetScenario());
                //splitContainerControl1.PanelVisibility = DevExpress.XtraEditors.SplitPanelVisibility.Both;
                //InitTabCtrls(true);
                goView.IsBackGroundImageVisible = true;
                tcProperties.InitTabCtrls(_presenter.Scenario != null);
                InitMainMenu();                
                UpdateFormCaption();
            }
            catch (Exception ex)
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, ex.Message, errorSource);
            }            
        }
        
        /// <summary>
        /// Обработчик кнопки "Открыть"
        /// (открытие схемы из базы)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void iOpen_ItemClick(object sender, ItemClickEventArgs e)
        {
            const string errorSource = "MainForm::iOpen_ItemClick()";
            try
            {
                //Проверка на имеющиеся несохраненные изменения в текущем сценарии
                if (_presenter.ScenarioHasUnsavedChanges() &&
                    MessageBox.Show("В текущем сценарии есть несохраненные изменения, хотите их сохранить?", "Изменения в сценарии", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    return;
                }

                OnLoad();                
            }
            catch (Exception ex)
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, ex.Message, errorSource);
            }  
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void matrixBI_ItemClick(object sender, ItemClickEventArgs e)
        {
      
        }
        
        /// <summary>
        /// Отображение матрицы связей
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            var form = new MatrixForm();
            form.Init(_errorHandler,_presenter.GetSchemaNodes(),_presenter.GetNodesRelations(), _presenter.GetBalanceZones());
            //_presenter.OnLoadMatrix();
            if (DialogResult.OK == form.ShowDialog())
            {
                var relations = form.NodesRelations;
                _presenter.OnSaveMatrix(relations);
                //_presenter.OnUpdateBalZones(form.BalansZones);
            }
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
           // SetAddTool(DTO_Node_type.Output);
            CreateNode();
        }

        private void CreateNode()
        {
            INodeView gn = new GazExportGoNode();
            var dto = new DTO_Node();
            gn.NodeDocument = dto;
            gn.Name = "новый";
            gn.LabelY = 0;
            gn.LabelX = 0;
            gn.X = 100;
            gn.Y = 100;

            goView.AddToNetwork(gn);
        }

        private void barButtonItem4_ItemClick(object sender, ItemClickEventArgs e)
        {
            SetDefaultTool();
        }

        private void barButtonItem3_ItemClick(object sender, ItemClickEventArgs e)
        {
            SetAddTool(DTO_Node_type.RussianGasInput);
        }

        private void barButtonItem5_ItemClick(object sender, ItemClickEventArgs e)
        {
            SetAddTool(DTO_Node_type.UGS);
        }

        private void barButtonItem6_ItemClick(object sender, ItemClickEventArgs e)
        {
            SetAddTool(DTO_Node_type.Border);
        }
        
        /// <summary>
        /// Обработчик кнопки "Сохранить как"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void iSaveAs_ItemClick(object sender, ItemClickEventArgs e)
        {
            //string newName = _presenter.GetScheme().Name;
            InputString frm = new InputString("Сохранить схему как новую", "Введите название схемы", _presenter.GetScheme().SchemaName, "Комментарий", _presenter.GetScheme().Comment);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                const string errorSource = "MainForm::iSaveAs_ItemClick()";
                try
                {
                    OnWaitCursor();
                    _presenter.UpdateSchemaNameAndComment(frm.Value, frm.Comment);
                    OnSaveAs();
                    UpdateFormCaption();
                    
                    InitMainMenu();
                }
                catch (Exception ex)
                {
                    _errorHandler.ReportError(eErrClass.EC_ERROR, ex.Message, errorSource);
                }
                finally
                {
                    OnDefaultCursor();
                }

                
            }
        }

        private void UpdateFormCaption()
        {
            var sc = _presenter.GetScheme();
            if (sc != null)
            {
                Text = String.Format("{0} ( {1} )", Resources.ApplicationName, sc.SchemaName);
                dpView.Text = _presenter.GetCaptionAfterNodeChosen();
            }
            else 
            {
                dpView.Text = Text = Resources.ApplicationName;                 
            }
            dpOptions.Text = _presenter.ScenarioName.Length > 0 ? "Сценарий: " + _presenter.ScenarioName : "Сценарий";
        }


        private void OnSaveAs()
        {
            _presenter.OnSaveAs();            
        }

        /// <summary>
        /// Обработчик кнопки "Сохранить"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void iSave_ItemClick(object sender, ItemClickEventArgs e)
        {
            const string errorSource = "MainForm::iSave_ItemClick()";
            try
            {
                if (_presenter.GetScheme().Id <= 0)
                {
                    iSaveAs_ItemClick(sender, e);
                    return;
                }
                OnSave();
                /*
                if (_presenter.Scenario != null && _presenter.Scenario.ID > 0)
                {
                    _presenter.CoreHolder.LoadScenario(_presenter.Scenario.ID);
                    if (_presenter.CoreHolder.GetScenario() != null)
                    {                        
                        tcProperties.InitTabCtrls(true);
                    }
                }
                */ 
                InitMainMenu();
            }
            catch (Exception ex)
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, ex.Message, errorSource);
            }
        }

        private void bbiBalZones_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!_presenter.AllNodesSaved())
            {
                MessageBox.Show("В схеме есть новые элементы, сначала сохраните схему.", "Операторы");
                return;
            }

            var form = new FrmBalanceZones();
            form.Init(_presenter.GetSchemaNodes(), _presenter.GetBalanceZones(), _presenter.CoreHolder);
            if (DialogResult.OK == form.ShowDialog())
            {
                var nodes = form.Nodes;
                //Сохраняем изменения только по кнопке ОК
                _presenter.OnUpdateBalZones(form.Zones);
                _presenter.OnSaveNodesRelationsToBalZones(nodes);
                //TODO: сделать отдельный метод для сохранения привязок к зонам отдельно от схемы.
                _presenter.OnSaveOperators();
                _presenter.UpdateGazExportObjectsZones();

                _presenter.SaveNodeZones(form.NodeZones);
                //if (_presenter.GetScheme().Id > 0)
                //    _presenter.OnSave();
            }
        }

        private void barButtonItem7_ItemClick(object sender, ItemClickEventArgs e)
        {
            const string errorSource = "MainForm::barButtonItem7_ItemClick()";
            try
            {
                _presenter.GenerateModel();
            }
            catch (Exception ex)
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, ex.Message, errorSource);
            }
        }
       
        private void bbiCalculate_ItemClick(object sender, ItemClickEventArgs e)
        {
            const string errorSource = "MainForm::bbiCalculate_ItemClick()";
            if (!_presenter.TerminalPointsSelected())
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, "Не заданы терминальные точки маршрута, расчет не запущен", errorSource);
                MessageBox.Show("Задайте терминальные точки маршрута", "Ошибка расчета");
                return;
            }
            
            try
            {                
                OnWaitCursor();
                _presenter.Calculate();
                dpResults.Select();
                dpResults.Show();

                if (_presenter.CoreHolder.Mode == ModeWork.Multi && _presenter.CoreHolder.LoadDeliveries().Any())
                        MessageBox.Show(@"Контрактные обязательства выполнены не полностью");
            }
            catch (Exception ex)
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, ex.Message, errorSource);
                MessageBox.Show("Во время расчета произошла ошибка.", "Ошибка расчета");
            }
            finally
            {
                OnDefaultCursor();
            }
           
        }

        private void sbSave_Click(object sender, EventArgs e)
        {
            const string errorSource = "MainForm::sbSave_Click()";
            try
            {
                if (!_presenter.AllNodesSaved())
                {
                    MessageBox.Show("В схеме есть новые элементы, сначала сохраните схему.", "Ошибка сохранения");
                    return;
                }

                if (_presenter.Scenario.ID > 0)
                    _presenter.OnSaveScenario();
                else
                {
                    btSaveAsNew_Click(sender, e);
                    return;
                }
                    //_presenter.OnSaveScenarioAs();
                //todo: вернуть
                //if (_ctrlScenariosList != null)
                //    _ctrlScenariosList.InitControlData();
                siStatus.Caption = "Cохранен сценарий '" + _presenter.Scenario.Name + "'";
            }
            catch (Exception ex)
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, ex.Message, errorSource);
            }
            
        }

        private void btSaveAsNew_Click(object sender, EventArgs e)
        {
            InputString frm = new InputString("Сохранить сценарий как новый", "Введите название сценария", _presenter.Scenario.Name, "Комментарий", _presenter.Scenario.Comment);
             if (frm.ShowDialog() == DialogResult.OK)
             {
                 const string errorSource = "MainForm::btSaveAsNew_Click()";
                 try
                 {
                     if (!_presenter.AllNodesSaved())
                     {
                         MessageBox.Show("В схеме есть новые элементы, сначала сохраните схему.", "Ошибка сохранения");
                         return;
                     }
                     _presenter.UpdateScenarioNameAndComment(frm.Value, frm.Comment);
                     _presenter.OnSaveScenarioAs();
                     //todo: вернуть
                     //if (_ctrlScenariosList != null)
                     //    _ctrlScenariosList.InitControlData();
                     siStatus.Caption = "Cохранен сценарий '" + _presenter.Scenario.Name + "'";
                 }
                 catch (Exception ex)
                 {
                     _errorHandler.ReportError(eErrClass.EC_ERROR, ex.Message, errorSource);
                 }
             }
        }

        private void SetDefaultScenario()
        { 
            
        }

        #region IGExDomainCallback

        void IGExDomainCallback.CallGui(string msg)
        {
            //throw new NotImplementedException();
        }

        void IGExDomainCallback.BeforeNewSchemaSave(DTO_Schema sch)
        {
            //if (System.Windows.Forms.DialogResult.Yes == MessageBox.Show("Дайте новой схеме имя !!", "", MessageBoxButtons.YesNo))
            {
                //заглушка
            }
        }

        void IGExDomainCallback.OnScenarioLoaded()
        {
            var tmp = _presenter.CoreHolder.GetScenario();
            if (tmp != null)
            {
                tcProperties.InitTabCtrls(true);
                InitMainMenu();
                siStatus.Caption = "Загружен сценарий '" + _presenter.Scenario.Name + "'";
            }
            else
            {
                siStatus.Caption = "Не удалось загрузить сценарий";
            }
        }

        void IGExDomainCallback.OnScenarioSaved()
        {
            siStatus.Caption = "Сохранен сценарий '" + _presenter.Scenario.Name + "'";
            //todo: вернуть
            //_ctrlScenariosList.InitControlData();
        }

        #endregion
        
        public void OnNodeAdded()
        {
            //throw new NotImplementedException();
        }
        
        public void OnNodeUpdated(DTO_Node node)
        {
            //throw new NotImplementedException();
        }


        public void OnRemoveNode(int nodeId)
        {
            //throw new NotImplementedException();
        }


        public void OnSchemaSaved(SavedEntityInfo schemaInfo, IEnumerable<SavedEntityInfo> nodesInfo)
        {
            siStatus.Caption = "Схема сохранена";
        }

        protected virtual PopupMenu CreateContextMenu(GoInputEventArgs e)
        {
            var menu = new PopupMenu();

            return menu;
        }


        public void OnCalculated()
        {
            siStatus.Caption = "Расчет завершен";
            //throw new NotImplementedException();
        }

        public void OnRemoveNodeAsSelectedForPath(DTO_Node node)
        {
            throw new NotImplementedException();
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {            
            var version = ConfigurationManager.AppSettings["Version"];
            if (String.IsNullOrWhiteSpace(version)) version = "1.0.0.0";
            beiVersion.EditValue = "Версия: " + version;            
            SplashScreenManager.CloseForm();
            //По умолчанию при запуске приложения выдаем список схем для загрузки.
            OnLoad();
        }

        /// <summary>
        /// Убрать маршрут
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClearPaths_ItemClick(object sender, ItemClickEventArgs e)
        {
            _presenter.ClearLinksGoView();
        }  

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            //try
            //{
            //    dockManager1.SaveLayoutToXml(GetMDIFileName());
            //}
            //catch (Exception ex)
            //{
            //    // ignored
            //}
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            int schemaChanges = _presenter.SchemaHasChanges() ? 1 : 0;
            int scenarioChanges = _presenter.ScenarioHasUnsavedChanges() ? 1 : 0;

            int result = schemaChanges*2 + scenarioChanges;
            if (result == 0) return;

            string[] messages =
            {
                "-",
                "в сценарии",
                "в схеме",
                "в схеме и сценарии"
            };

            var msgWhereChanges = messages[result];

            var question = String.Format("{0} имеются несохраненные изменения, выйти без сохранения?", msgWhereChanges);
            var caption = "Изменения " + msgWhereChanges;
            var dr = MessageBox.Show(question, caption, MessageBoxButtons.OKCancel);
            if (dr == DialogResult.Cancel)
            {
                e.Cancel = true;
            }
        }


        /// <summary>
        /// Только в случае успешного назначения точки
        /// </summary>
        void IMainForm.OnPathPointSet()
        {
            //прописать в заголовке схемы выбранные точки.
             dpView.Text = _presenter.GetCaptionAfterNodeChosen();
        }

        private void bbiOpenScenario_ItemClick(object sender, ItemClickEventArgs e)
        {
            const string errorSource = "MainForm::bbiOpenScenario_ItemClick()";
            try
            {
                //Проверка на имеющиеся несохраненные изменения в текущем сценарии
                if (_presenter.ScenarioHasUnsavedChanges() &&
                    MessageBox.Show("В текущем сценарии есть несохраненные изменения, хотите их сохранить?", "Изменения в сценарии", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    return;
                }
                OnLoadScenario();
            }
            catch (Exception ex)
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, ex.Message, errorSource);
            }  
        }


        public void OnSchemaInit()
        {
            
        }

        private void bbiNewScenario_ItemClick(object sender, ItemClickEventArgs e)
        {
            const string errorSource = "MainForm::bbiNewScenario_ItemClick()";
            try
            {
                _presenter.OnCreateScenario();
                tcProperties.InitTabCtrls(_presenter.Scenario != null);
                _presenter.ClearLinksGoView();
                dpOptions.Text = _presenter.ScenarioName.Length > 0 ? "Сценарий: " + _presenter.ScenarioName : "Сценарий";
            }
            catch (Exception ex)
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, ex.Message, errorSource);
            }  
        }

        /// <summary>
        /// Обработчик кнопки сценария "Сохранить"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bbiSaveScenario_ItemClick(object sender, ItemClickEventArgs e)
        {
            const string errorSource = "MainForm::bbiSaveScenario_ItemClick()";
            try
            {
                if (!_presenter.AllNodesSaved())
                {
                    MessageBox.Show("В схеме есть новые элементы, сначала сохраните схему.", "Ошибка сохранения");
                    return;
                }

                if (!_presenter.isNewScenario())
                {
                    try
                    {
                        OnWaitCursor();
                        _presenter.OnSaveScenario();
                    }
                    finally
                    {
                        OnDefaultCursor(); 
                    }
                }
                else
                {
                    bbiSaveAsScenario_ItemClick(sender, e);
                    return;
                }
                dpOptions.Text = _presenter.ScenarioName.Length > 0 ? "Сценарий: " + _presenter.ScenarioName : "Сценарий";
                siStatus.Caption = "Cохранен сценарий '" + _presenter.ScenarioName + "'";
            }
            catch (Exception ex)
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, ex.Message, errorSource);
            }
        }

        /// <summary>
        /// Обработчик кнопки сценария "Сохранить как"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bbiSaveAsScenario_ItemClick(object sender, ItemClickEventArgs e)
        {
            InputString frm = new InputString("Сохранить сценарий как новый", "Введите название сценария", _presenter.ScenarioName, "Комментарий", _presenter.GetScheme().Comment);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                const string errorSource = "CtrlScenariosList::iSaveAs_ItemClick()";
                try
                {
                    if (!_presenter.AllNodesSaved())
                    {
                        MessageBox.Show("В схеме есть новые элементы, сначала сохраните схему.", "Ошибка сохранения");
                        return;
                    }
                    OnWaitCursor();
                    _presenter.UpdateScenarioNameAndComment(frm.Value, frm.Comment);
                    _presenter.SaveScenarioAs();
                    dpOptions.Text = _presenter.ScenarioName.Length > 0 ? "Сценарий: " + _presenter.ScenarioName : "Сценарий";
                    siStatus.Caption = "Cохранен сценарий '" + _presenter.ScenarioName + "'";
                }
                catch (Exception ex)
                {
                    _errorHandler.ReportError(eErrClass.EC_ERROR, ex.Message, errorSource);
                }
                finally
                {
                    OnDefaultCursor();
                }
            }
        }

        /// <summary>
        /// Включаем перетаскивание и масштабирование схемы
        /// </summary>
        public void StartZoomAndDrag()
        {
            var w = goView;
            // Отвечает за перетаскивание и масштабирование
            w.MouseDownTools.Insert(0, _dragTool);

            // Отвечает за колёсико
            w.DefaultTool = new GExToolManager(goView);
            w.Tool = w.DefaultTool;
        }
        
        public void OnWaitCursor()
        {
            _defaultCursor = Cursor;
            Cursor = Cursors.WaitCursor;
        }

        public void OnDefaultCursor()
        {
            Cursor = _defaultCursor;
        }

        private void dpResults_Click(object sender, EventArgs e)
        {

        }

        private void zoomInButton_ItemClick(object sender, ItemClickEventArgs e)
        {
            goView.DocScale *= 1.5f;
        }

        private void zoomOutButton_ItemClick(object sender, ItemClickEventArgs e)
        {
            goView.DocScale /= 1.5f;
        }
        
        public void OnschemaInit()
        {
            ribbonControl.SelectedPage = ribbonControl.Pages["Расчеты"];
        }

        public void RefreshSomeCaptions()
        {
            UpdateFormCaption();
        }

        private void ribbonControl_SelectedPageChanged(object sender, EventArgs e)
        {
            
        }

        private void ribbonControl_SelectedPageChanging(object sender, RibbonPageChangingEventArgs e)
        {
            if (e.Page == homeRibbonPage)
            {
                barAndDockingController.LookAndFeel.SetSkinStyle("SkinWorkSchema");
            }
            else if (e.Page == rpScenario)
            {
                barAndDockingController.LookAndFeel.SetSkinStyle("SkinScenario");
            }
            else if (e.Page == rpEditSchema)
            {
                barAndDockingController.LookAndFeel.SetSkinStyle("SkinSchemeEditor");
            }
        }

        private void barButtonItem7_ItemClick_1(object sender, ItemClickEventArgs e)
        {
            SetAddTool(DTO_Node_type.VirtualTradingPoint);
        }

        #region Points paths

        /// <summary>
        /// Добавить начальную точку
        /// </summary>
        /// <param name="point"></param>
        public void AddFirstPoint(DTO_Node point)
        {
            if (point == null) return;

            var control = (ListBoxControl)ctrlPointsPaths.FirstSelectControl;
            control.Items.Add(point);

            var all = (ListBoxControl)ctrlPointsPaths.FirstAllControl;
            all.Items.Remove(point);

            ((IMainForm) this).OnPathPointSet();
        }

        /// <summary>
        /// Добавить конечную точку
        /// </summary>
        /// <param name="point"></param>
        public void AddLastPoint(DTO_Node point)
        {
            if (point == null) return;

            var control = (ListBoxControl)ctrlPointsPaths.LastSelectControl;
            control.Items.Add(point);

            var all = (ListBoxControl)ctrlPointsPaths.LastAllControl;
            all.Items.Remove(point);

            ((IMainForm)this).OnPathPointSet();
        }

        /// <summary>
        /// Удалить точку из маршрута
        /// </summary>
        /// <param name="point"></param>
        public void RemovePoint(DTO_Node point)
        {
            if (point == null) return;

            var first = (ListBoxControl)ctrlPointsPaths.FirstSelectControl;
            var last = (ListBoxControl)ctrlPointsPaths.LastSelectControl;

            var firstPoint =  first.Items.OfType<DTO_Node>().FirstOrDefault(d => d.ID == point.ID);
            if (firstPoint != null)
            {
                first.Items.Remove(firstPoint);

                var all = (ListBoxControl)ctrlPointsPaths.FirstAllControl;
                all.Items.Add(point);
            }

            var lastPoint = last.Items.OfType<DTO_Node>().FirstOrDefault(d => d.ID == point.ID);
            if (lastPoint != null)
            {
                last.Items.Remove(lastPoint);

                var all = (ListBoxControl)ctrlPointsPaths.LastAllControl;
                all.Items.Add(point);
            }

            ((IMainForm)this).OnPathPointSet();
        }

        /// <summary>
        /// Добавить все точки
        /// </summary>
        /// <param name="points"></param>
        public void AddAllPoints(IEnumerable<DTO_Node> points)
        {
            var first = (ListBoxControl)ctrlPointsPaths.FirstAllControl;
            var last = (ListBoxControl)ctrlPointsPaths.LastAllControl;
            
            first.Items.Clear();
            last.Items.Clear();
            
            if (points == null) return;

            first.Items.AddRange(points.ToArray());
            last.Items.AddRange(points.ToArray());
        }

        /// <summary>
        /// Удалить выбранные начальные точки
        /// </summary>
        public void FirstPointClear()
        {
            var control = (ListBoxControl)ctrlPointsPaths.FirstSelectControl;
            var all = (ListBoxControl)ctrlPointsPaths.FirstAllControl;
            control.Items.OfType<DTO_Node>().ForEach(n => { all.Items.Add(n); });
            control.Items.Clear();
        }

        /// <summary>
        /// Удалить выбранные конечные точки
        /// </summary>
        public void LastPointClear()
        {
            var control = (ListBoxControl)ctrlPointsPaths.LastSelectControl;
            var all = (ListBoxControl)ctrlPointsPaths.LastAllControl;
            control.Items.OfType<DTO_Node>().ForEach(n => { all.Items.Add(n); });
            control.Items.Clear();
        }

        #endregion //Points paths

        #region Export Map

        private static readonly Dictionary<ImageFormat, string> ExportImageFormats = new Dictionary<ImageFormat, string>
        {
            {ImageFormat.Png, ImageFormat.Png.ToString()},
            {ImageFormat.Bmp, ImageFormat.Bmp.ToString()},
            {ImageFormat.Jpeg, ImageFormat.Jpeg.ToString()},
//            {ImageFormat.Emf, ImageFormat.Emf.ToString()},
//            {ImageFormat.Exif, ImageFormat.Exif.ToString()},
//            {ImageFormat.Gif, ImageFormat.Gif.ToString()},
//            {ImageFormat.Tiff, ImageFormat.Tiff.ToString()},
//            {ImageFormat.Wmf, ImageFormat.Wmf.ToString()},
//            {ImageFormat.Icon, ImageFormat.Icon.ToString()}
        };

        private void iExport_ItemClick(object sender, ItemClickEventArgs e)
        {
            var filter = ExportImageFormats.Values.Aggregate(string.Empty, (current, s) => current + (s + " files (*." + s.ToLower() + ")|*." + s.ToLower() + "|"));
            filter = filter.Substring(0, filter.Length - 1);
            var saveFileDialog = new SaveFileDialog
            {
                DefaultExt = ExportImageFormats.First().Value.ToLower(),
                Filter = filter,
//                AddExtension = true
            };
            if (saveFileDialog.ShowDialog(this) != DialogResult.OK) return;

            const string errorSource = "MainForm::iExport_ItemClick()";
            try
            {
                OnWaitCursor();

                var strExt = saveFileDialog.FileName.Split('.')[1];
                var ext = ExportImageFormats.FirstOrDefault(s => s.Value.ToLower() == strExt).Key;

                if (ext == null)
                {
                    MessageBox.Show(string.Format("Разрешение файла {0} не соответствует ожидаемому формату {1}", strExt, ExportImageFormats.Values.ToArray()[saveFileDialog.FilterIndex - 1]), @"Ошибки экспорта");
                    return;
                }

                var bitmap = goView.GetBitmap();
                bitmap.Save(saveFileDialog.FileName, ext);
            }
            catch (Exception ex)
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, ex.Message, errorSource);
            }
            finally
            {
                OnDefaultCursor();
            }
        }

        #endregion //Export Map
        
        #region Import Underlayer

        private static readonly Dictionary<ImageFormat, string> ImportImageFormats = new Dictionary<ImageFormat, string>
        {
            {ImageFormat.Png, ImageFormat.Png.ToString()},
            {ImageFormat.Bmp, ImageFormat.Bmp.ToString()},
            {ImageFormat.Jpeg, ImageFormat.Jpeg.ToString()},
//            {ImageFormat.Emf, ImageFormat.Emf.ToString()},
//            {ImageFormat.Exif, ImageFormat.Exif.ToString()},
//            {ImageFormat.Gif, ImageFormat.Gif.ToString()},
//            {ImageFormat.Tiff, ImageFormat.Tiff.ToString()},
//            {ImageFormat.Wmf, ImageFormat.Wmf.ToString()},
//            {ImageFormat.Icon, ImageFormat.Icon.ToString()}
        };
        
        private void iImportUnderlayer_ItemClick(object sender, ItemClickEventArgs e)
        {
            var filter = ImportImageFormats.Values.Aggregate(string.Empty, (current, s) => current + (s + " files (*." + s.ToLower() + ")|*." + s.ToLower() + "|"));
            filter = filter.Substring(0, filter.Length - 1);
            var openFileDialog = new OpenFileDialog()
            {
                DefaultExt = ImportImageFormats.First().Value.ToLower(),
                Filter = filter,
//                AddExtension = true
            };
            if (openFileDialog.ShowDialog(this) != DialogResult.OK) return;

            const string errorSource = "MainForm::iImportUnderlayer_ItemClick()";
            try
            {
                OnWaitCursor();
                
                var bitmap = new Bitmap(openFileDialog.FileName);
                goView.SetUnderlayer(bitmap);
            }
            catch (Exception ex)
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, ex.Message, errorSource);
            }
            finally
            {
                OnDefaultCursor();
            }
        }

        #endregion //Import Underlayer

        #region Mode
        
        private void barToggleSwitchItem1_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            var barToggleSwitchItem = (BarToggleSwitchItem) sender;
            var mode = barToggleSwitchItem.Checked ? ModeWork.Multi : ModeWork.Single;

            SetMode(mode);
        }

        void SetMode(ModeWork mode)
        {
            xtpNominations.PageVisible = xtpDeliveries.PageVisible = mode == ModeWork.Multi;

            _presenter.Mode = mode;
        }

        #endregion //Mode

        private void iAbout_ItemClick(object sender, ItemClickEventArgs e)
        {
            IMailing mSender = new MailSender();
            try
            {
                mSender.OnMailingFailed += mSender_OnMailingFailed;
                mSender.OnMailingSucceeded += mSender_OnMailingSucceeded; ;
                var mailers = new List<String>();
                mailers.Add("R.Semin@econom.gazprom.ru");
                mSender.SendMail("Test message", "Простое сообщение", "R.Semin@econom.gazprom.ru", mailers, null);
            }
            catch
            { }
        }

        private void mSender_OnMailingSucceeded(object sender, EventArgs e)
        {
            MessageBox.Show("Письмо отправлено!");
        }

        private void mSender_OnMailingFailed(object sender, EventArgs e)
        {
            MessageBox.Show("Не получилось!");
        }

        
    }
}