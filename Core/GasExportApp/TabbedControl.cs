﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommonUIControls;
using CommonUIControls.ScenarioControls;
using DevExpress.XtraEditors;
using GEx.ErrorHandler;

namespace GasExportApp
{
    public partial class TabbedControl : DevExpress.XtraEditors.XtraUserControl
    {
        public TabbedControl()
        {
         
            InitializeComponent();
        }

        private CtrlScenariosList _ctrlScenariosList;
        private MainFormPresenter _presenter;
        private IErrorOutput _errorHandler;

        public void Init(MainFormPresenter presenter, IErrorOutput errorHandler)
        {
            _presenter = presenter;
            _errorHandler = errorHandler;
            //InitScenariosList();
            InitTabCtrls(false);
            
        }
        
        internal void InitTabCtrls(Boolean isVisible)
        { 
            tpContractsVolume.PageVisible = isVisible;
            if (tpContractsVolume.Controls.Count == 0 || (tpContractsVolume.Controls[0] as CtrlContractsVolume) == null)
            {
                CtrlContractsVolume cCV = new CtrlContractsVolume();
                var childPresenter = new ContractsVolumePresenter(_presenter.CoreHolder, _errorHandler, cCV);
                cCV.SetPresenter(childPresenter);
                _presenter.AddChildPresenter(childPresenter);
                cCV.Dock = DockStyle.Fill;
                tpContractsVolume.Controls.Add(cCV);
            }
            //(tpContractsVolume.Controls[0] as CtrlContractsVolume).InitControlData();

            tpExportVolume.PageVisible = isVisible;
            if (tpExportVolume.Controls.Count == 0 || (tpExportVolume.Controls[0] as CtrlExportVolume) == null)
            {

                CtrlExportVolume cEV = new CtrlExportVolume();
                ExportVolumePresenter pres = new ExportVolumePresenter(_presenter.CoreHolder, _errorHandler, cEV);
                cEV.SetPresenter(pres);
                _presenter.AddChildPresenter(pres);
                cEV.Dock = DockStyle.Fill;
                tpExportVolume.Controls.Add(cEV);
            }
            //(tpExportVolume.Controls[0] as CtrlExportVolume).InitControlData();


            tpStorageExtraction.PageVisible = isVisible;
            if (tpStorageExtraction.Controls.Count == 0 || (tpStorageExtraction.Controls[0] as CtrlStorageExtraction) == null)
            {
                CtrlStorageExtraction cSE = new CtrlStorageExtraction();
                var childPresenter = new StorageExtractionPresenter(_presenter.CoreHolder, _errorHandler, cSE);
                cSE.SetPresenter(childPresenter);
                _presenter.AddChildPresenter(childPresenter);
                cSE.Dock = DockStyle.Fill;
                tpStorageExtraction.Controls.Add(cSE);
            }
            //(tpStorageExtraction.Controls[0] as CtrlStorageExtraction).InitControlData();

            tpPowerTarif.PageVisible = isVisible;

            if (tpPowerTarif.Controls.Count == 0 || (tpPowerTarif.Controls[0] as CtrlTransPowerAndTarif) == null)
            {
                CtrlTransPowerAndTarif cPandT = new CtrlTransPowerAndTarif();
                var childPresenter = new TransPowerAndTarifPresenter(_presenter.CoreHolder, _errorHandler, cPandT);
                cPandT.SetPresenter(childPresenter);
                _presenter.AddChildPresenter(childPresenter);
                cPandT.Dock = DockStyle.Fill;
                tpPowerTarif.Controls.Add(cPandT);
            }
            //(tpPowerTarif.Controls[0] as CtrlTransPowerAndTarif).InitControlData();

            if (isVisible)
            {
                (tpContractsVolume.Controls[0] as CtrlContractsVolume).InitControlData();
                (tpExportVolume.Controls[0] as CtrlExportVolume).InitControlData();
                (tpStorageExtraction.Controls[0] as CtrlStorageExtraction).InitControlData();
                (tpPowerTarif.Controls[0] as CtrlTransPowerAndTarif).InitControlData();
            }

            //_dockAssistant.EndUpdate();
        }
        private void sbRemoveScenario_Click(object sender, EventArgs e)
        {
            const string errorSource = "MainForm::sbRemoveScenario_Click()";
            try
            {
                if (_ctrlScenariosList.SelectedScenario.HasValue &&
                MessageBox.Show("Вы действительно хотите удалить выбранный сценарий?", "Удаление сценария", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    _presenter.OnRemoveScenario((int)_ctrlScenariosList.SelectedScenario);
                    if (_ctrlScenariosList != null)
                        _ctrlScenariosList.InitControlData();
                    //siStatus.Caption = "Cценарий удален";
                }
            }
            catch (Exception ex)
            {
                _errorHandler.ReportError(eErrClass.EC_ERROR, ex.Message, errorSource);
            }
        }
    }
}
