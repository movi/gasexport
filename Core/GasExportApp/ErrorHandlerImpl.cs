// /////////////////////////////////////////////////////////////
// File: 
// Date:			Author: Ildar Batyrshin
// Language: C#		Framework: .NET 2.0
// /////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Threading;
using GEx.ErrorHandler;
using GEx.Log;

namespace GasExportApp
{
    //public class CReportErrorItem
    //{
    //    public bool ShowTime;
    //    public bool ShowOrigin;
    //    public string Text;
    //    public string Source;
    //    public eErrClass Class;
    //    public DateTime Time;
    //    public int ThreadID;

    //    public override string ToString()
    //    {
    //        string source = this.Source.Length > 0 ? this.Source : "origin unknown";
    //        string s = "";
    //        if (ShowTime)
    //            s += this.Time.ToString() + " | " + ThreadID.ToString() + " | ";
    //        else
    //            s += "                                  ";
    //        s += this.Text;
    //        if (ShowOrigin)
    //            s += " ; <" + source + ">";
    //        return s;
    //    }
    //};

	/// <summary>
	/// Summary description for ErrorHandlerImpl.
	/// </summary>
    public class ErrorHandlerImpl : IErrorOutput
    {
        //private System.Windows.Forms.ListBox listBoxJournal;
        //private System.Windows.Forms.Form _owner;
        private delegate void OwnerFormDelegate(object o);

        public ErrorHandlerImpl()
        {
        }

        //public void SetListBox(System.Windows.Forms.ListBox listBox)
        //{
        //    listBoxJournal = listBox;
        //    _owner = (System.Windows.Forms.Form)listBox.FindForm();
        //}

        private bool ReportErrorImpl(eErrClass cls, string msg, string source, int code)
        {
            Logger.Instance.Log(cls, msg, source,code);

            //string s = msg;
            //ArrayList list = new ArrayList();
            //int pos = -1;
            //while (-1 != (pos = s.IndexOf("\r\n")))
            //{
            //    string p = s.Substring(0, pos);
            //    list.Add(p);
            //    s = s.Substring(pos + 2, s.Length - pos - 2);
            //}
            //if (list.Count > 0)
            //{
            //    int i = 0;
            //    foreach (string p in list)
            //    {
            //        CReportErrorItem item = new CReportErrorItem();
            //        item.Text = p;
            //        item.Class = cls;
            //        item.Time = DateTime.Now;
            //        item.Source = source;
            //        item.ShowTime = (0 == i) ? true : false;
            //        item.ShowOrigin = (list.Count - 1 == i) ? true : false;
            //        item.ThreadID = Thread.CurrentThread.GetHashCode();
            //        _owner.Invoke(new OwnerFormDelegate(Helper), item);
            //        //listBoxJournal.Items.Add(item);
            //        //listBoxJournal.SetSelected(listBoxJournal.SelectedIndex + 1, true);
            //        i++;
            //    }
            //}
            //else
            //{
            //    CReportErrorItem item = new CReportErrorItem();
            //    item.Text = msg;
            //    item.Class = cls;
            //    item.Time = DateTime.Now;
            //    item.Source = source;
            //    item.ShowTime = true;
            //    item.ShowOrigin = true;
            //    item.ThreadID = Thread.CurrentThread.GetHashCode();
            //    _owner.Invoke(new OwnerFormDelegate(Helper), item);
            //    //listBoxJournal.Items.Add(item);
            //    //listBoxJournal.SetSelected(listBoxJournal.SelectedIndex + 1, true);
            //}
            return true;
        }

        //private void Helper(object o)
        //{
        //    listBoxJournal.Items.Add(o);
        //    listBoxJournal.SetSelected(listBoxJournal.SelectedIndex + 1, true);
        //}

        bool IErrorOutput.ReportError(eErrClass cls, string sMsg, string sSource, int nCode)
        {
            return ReportErrorImpl(cls, sMsg, sSource, nCode);
        }

        bool IErrorOutput.ReportError(eErrClass cls, string sMsg, string sSource)
        {
            return ReportErrorImpl(cls, sMsg, sSource, 0);
        }

        bool IErrorOutput.ReportError(eErrClass cls, int userId, string sMsg, string sSource)
        {
            return ReportErrorImpl(cls, sMsg, sSource, 0);
        }
    }
	
}
