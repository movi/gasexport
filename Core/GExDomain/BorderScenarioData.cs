﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Domain
{
    public class BorderScenarioData
    {
        public int Id { get; set; }

        public int ScenarioId { get; set; }

        public CalculationScenario Scenario { get; set; }

        //public PathPoint PathPoint { get; set; }
        /// <summary>
        /// Узел
        /// </summary>
        public GasExportObject GasExportObject { get; set; }

        /// <summary>
        /// Оператор
        /// </summary>
        public BalanceZone Operator { get; set; }

        /// <summary>
        /// Направление
        /// </summary>
        public Direction Direction { get; set; }
        /// <summary>
        /// Уже забронированный объем, млн. м3
        /// </summary>
        public Nullable<double> BookedCapacity { get; set; }

        /// <summary>
        /// Доступные для бронирования объем, млн. м3
        /// </summary>
        public Nullable<double> AvailableCapacity { get; set; }

        /// <summary>
        /// Фиксированная часть тарифа, Евро/млн. м3
        /// </summary>
        public Nullable<double> Tariff { get; set; }

        /// <summary>
        /// Стоимость ГСН, €/млн м³
        /// </summary>
        public Nullable<double> GsnValue { get; set; }

        /// <summary>
        /// % от стоимости  ГСН
        /// </summary>
        public Nullable<double> GsnPercentValue { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Признак того, что данные были загружены из БД
        /// по сути нужен только при удалении элемента со схемы
        /// </summary>
        public bool isLoaded = true;

    }
}
