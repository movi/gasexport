﻿namespace GEx.Domain
{
    public class SchemaImage
    {
        public int Id { get; set; }

        public byte[] Image { get; set; }
    }
}
