﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Domain
{
    public class BorderInterconnection
    {
        public int Id { get; set; }

        public int BorderId { get; set; }

        public Border Border { get; set; }

        public BalanceZone ZoneFrom { get; set; }

        public BalanceZone ZoneTo { get; set; }
    }
}
