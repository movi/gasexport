﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Domain
{
    public class Output : GasExportObject, IOneZoneNode
    {
        public override GasExportObjectType Type
        {
            get { return GasExportObjectType.Output; }
        }

        public BalanceZone Zone
        {
            get;
            set;
        }
    }
}
