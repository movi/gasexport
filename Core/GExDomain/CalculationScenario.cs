﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace GEx.Domain
{
    /// <summary>
    /// Класс представляющий сценарий расчета.
    /// Под сценарием подразумевается необходимый для расчета набор исходных данных в привязке к объектам схемы.
    /// </summary>
    public class CalculationScenario
    {
        /// <summary>
        /// Идентификатор сценария в БД
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Название сценария
        /// </summary>
        public string Name { get; set; }

        public string Comment { get; set; }

        public DateTime CreationDate { get; set; }

        public string Author { get; set; }

        /// <summary>
        /// Объемы контрактов по пунктам сдачи, млн. м3
        /// </summary>       
        public List<OutputScenarioData> OutputScenarioDataSet { get; set; }
        
        /// <summary>
        /// Максимальные объемы поступления российского газа на экспорт по экспортным пунктам, млн. м3
        /// </summary>
        //public Dictionary<RussianGasInput, Nullable<double>> MaxExportVolumes { get; set; }
        
        public List<RussianGasInputScenarioData> RussianGasInputScenarioDataSet { get; set; }
        
        /// <summary>
        /// Максимальные объемы отбора газа из арендуемых зарубежных ПХГ, млн. м3
        /// </summary>
        //public Dictionary<UGS, Nullable<double>> MaxUGSWithdrawal { get; set; }        
        public List<UGSScenarioData> UGSScenarioDataSet { get; set; }
        
        /// <summary>
        /// Мощности и тарифы по точкам и операторам
        /// </summary>
        //public Dictionary<PathPoint, PathPointCapacitiesData> CapacitiesData { get; set; }

        public List<BorderScenarioData> BorderScenarioDataSet { get; set; }

        public CalculationScenario()
        {
            ID = 0;
            Name = "Новый сценарий";
            CreationDate = DateTime.Now;
            Author = Environment.UserDomainName + "\\" + Environment.UserName;
            BorderScenarioDataSet = new List<BorderScenarioData>();
            OutputScenarioDataSet = new List<OutputScenarioData>();
            RussianGasInputScenarioDataSet = new List<RussianGasInputScenarioData>();
            UGSScenarioDataSet = new List<UGSScenarioData>();
        }

        public string Init(CalculationScenario res)
        {
            StringBuilder elements = new StringBuilder();
            ID = res.ID;
            Name = res.Name;
            CreationDate = res.CreationDate;
            Author = res.Author;
            //сопоставить списки объектов.
            foreach (var curNode in res.BorderScenarioDataSet)
            {
                var relateNode = this.BorderScenarioDataSet.Find(e => e.Direction == curNode.Direction &&
                                                    e.Operator.ID == curNode.Operator.ID &&
                                                    e.GasExportObject.ID == curNode.GasExportObject.ID);
                if (relateNode != null)
                {
                    relateNode.AvailableCapacity = curNode.AvailableCapacity;
                    relateNode.BookedCapacity = curNode.BookedCapacity;
                    relateNode.Tariff = curNode.Tariff;
                    relateNode.GsnValue = curNode.GsnValue;
                    relateNode.GsnPercentValue = curNode.GsnPercentValue;
                    relateNode.Description = curNode.Description;
                }
                else 
                {
                    elements.Append(string.Format("Узел сценария '{0}' (Id={1}), направление '{2}', оператор '{3}'  отсутствует на схеме.",
                                                curNode.GasExportObject.Name, curNode.GasExportObject.ID, curNode.Direction, curNode.Operator.Name));
                    elements.Append(Environment.NewLine);
                }
            }

            foreach (var curNode in res.OutputScenarioDataSet)
            {
                var relateNode = this.OutputScenarioDataSet.Find(e => e.Obj.ID == curNode.Obj.ID);
                if (relateNode != null)
                {
                    relateNode.ContractVolume = curNode.ContractVolume;                    
                }
                else
                {
                    elements.Append(string.Format("Узел сценария '{0}' (Id={1}) отсутствует на схеме.",
                                                    curNode.Obj.Name, curNode.Obj.ID));
                    elements.Append(Environment.NewLine);
                }
            }
            foreach (var curNode in res.RussianGasInputScenarioDataSet)
            {
                var relateNode = this.RussianGasInputScenarioDataSet.Find(e => e.Obj.ID == curNode.Obj.ID);
                if (relateNode != null)
                {
                    relateNode.MaxExportVolume = curNode.MaxExportVolume;
                }
                else
                {
                    elements.Append(string.Format("Узел сценария '{0}' (Id={1}) отсутствует на схеме.",
                                                    curNode.Obj.Name, curNode.Obj.ID));
                    elements.Append(Environment.NewLine);
                }
            }
            foreach (var curNode in res.UGSScenarioDataSet)
            {
                var relateNode = this.UGSScenarioDataSet.Find(e => e.Obj.ID == curNode.Obj.ID);
                if (relateNode != null)
                {
                    relateNode.MaxUGSWithdrawal = curNode.MaxUGSWithdrawal;
                }
                else
                {
                    elements.Append(string.Format("Узел сценария '{0}' (Id={1}) отсутствует на схеме.",
                                                    curNode.Obj.Name, curNode.Obj.ID));
                    elements.Append(Environment.NewLine);
                }
            }

            return elements.ToString();
        }
    }
}
