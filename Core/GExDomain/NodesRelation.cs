﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Domain
{
    public class NodesRelation
    {
        public int ID {get; set;}

        public int SchemaID { get; set;}

        public Schema Schema { get; set; }

        public Node NodeFirst { get; set; }

        public Node NodeSecond { get; set; }

        public BalanceZone BalanceZone { get; set; }

        public double Value { get; set; }

    }
}
