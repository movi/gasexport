﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GEx.Domain
{
    public class TestSchemeGenerator
    {
        public Schema GenerateDataModel()
        {
            var s = new Schema();
            s.CreationDate = DateTime.Now;
            for (int ii = 0; ii < r.Next(2, 8); ii++)
            {
                var zone = new BalanceZone();
                zone.Name = GenerateToken((byte)r.Next(2, 10));

                int nodesCount = r.Next(1, 9);
                var nodes = new List<Node>();
                for (int i = 0; i < nodesCount; i++)
                {
                    var node = new Node
                    {
                        X = r.Next(0, 1000),
                        Y = r.Next(0, 1000),
                        GasExportObject = new Output()
                        {
                            Name=GenerateToken((byte)r.Next(3, 5)),
                            Zone = zone
                        },
                      //  RelatedNodes = new List<Node>(),
                        Schema = s
                    };
                    nodes.Add(node);
                    s.Nodes.Add(node);
                }
            }
            var n = s.Nodes.Count;
            int edgesmax = (n - 1)*(n)/2;
            
            for (int j = 0; j < r.Next(edgesmax); j++)
            {
                int f = r.Next(n);
                var elem = s.Nodes.ElementAt(f);

                int e = r.Next(n);
                var elem2= s.Nodes.ElementAt(e);
                //if (!elem.RelatedNodes.Contains(elem2))
                //{
                //    elem.RelatedNodes.Add(elem2);
                //    elem2.RelatedNodes.Add(elem);
                //}
            }
            return s;
        }

        private Random r = new Random();
        public string GenerateToken(int length)
        {

            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".ToLower();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[r.Next(s.Length)]).ToArray());
        }
    }
}
