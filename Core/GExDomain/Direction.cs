﻿using System.ComponentModel;

namespace GEx.Domain
{
    /// <summary>
    /// Направление в модели вход-выход
    /// </summary>
    public enum Direction
    {
        [Description("Вход")]
        Entry,
        [Description("Выход")]
        Exit
    }
}
