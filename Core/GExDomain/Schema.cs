﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Domain
{
    public class Schema
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Comment { get; set; }

        public DateTime CreationDate { get; set; }

        public string Author { get; set; }

        public ICollection<Node> Nodes { get; set; }

        public ICollection<NodesRelation> Relations { get; set; }

        public SchemaImage SchemaImage { get; set; }

      //  public byte[] Image { get; set; }

        public Schema()
        {
            Nodes = new ObservableCollection<Node>();
            Relations = new List<NodesRelation>();
            //INotifyCollectionChanged notify = Nodes as INotifyCollectionChanged;
            //notify.CollectionChanged += (sender, args) =>
            //    {
            //        if (args.Action == NotifyCollectionChangedAction.Remove)
            //        {
            //            var nodeForDelete = args.OldItems[0] as Node;
            //            foreach (var node in Nodes)
            //            {
            //                var relNodeForDelete = node.RelatedNodes.Where(rn => rn.ID == nodeForDelete.ID).FirstOrDefault();
            //                if (relNodeForDelete != null)
            //                    node.RelatedNodes.Remove(relNodeForDelete);
            //            }

            //        }
            //    };

        }

        
        public IEnumerable<BalanceZone> GetZones()
        {
            var coll1 = this.Nodes.Select(n=>n.GasExportObject).OfType<Border>().SelectMany(b => b.Interconnections).Select(i => i.ZoneFrom);
            var coll2 = this.Nodes.Select(n=>n.GasExportObject).OfType<Border>().SelectMany(b => b.Interconnections).Select(i => i.ZoneTo);
            var coll3 = this.Nodes.Select(n=>n.GasExportObject).OfType<IOneZoneNode>().Select(oz => oz.Zone).Where(i => i != null);

            var result = coll1.Union(coll2).Union(coll3);

            return result.Distinct();
        }
    }
}
