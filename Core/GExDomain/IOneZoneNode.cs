﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GEx.Domain
{
    //TODO change IOneZoneNode to IOneZoneObject
    public interface IOneZoneNode
    {
        BalanceZone Zone { get; set; }
    }
}
