﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Domain
{
    public enum GasExportObjectType
    {
        [Description("Пункт поступления российского газа")]
        RussianGasInput, //      пункты поступления российского газа;
        [Description("ПХГ")]
        UGS, //Underground gas storage -- ПХГ
        [Description("Граница")]
        Border, //	граничные узлы;
        [Description("Пункт сдачи")]
        Output, // пункты сдачи. 
        [Description("Витртуальная торговая площадка")]
        VTP
    }
}
