﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Domain
{
    public class Node
    {
        public int ID { get; set; }

        public double X { get; set; }

        public double Y { get; set; }

       
        /// <summary>
        /// Координата по оси X позиции надписи 
        /// </summary>
        
        public double? LabelX { get; set; }

        /// <summary>
        /// Координата по оси Y позиции надписи
        /// </summary>
        public double? LabelY { get; set; }

        //public List<Node> RelatedNodes { get; set; }

        public Schema Schema { get; set; }

        public GasExportObject GasExportObject { get; set; }

        public Node()
        {
          //  RelatedNodes = new List<Node>();
        }
    }
}
