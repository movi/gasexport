﻿namespace GEx.Domain
{
    /// <summary>
    /// Представление точки маршрута в модели вход-выход
    /// </summary>
    public class PathPoint
    {
        public int Id { get; set; }

        /// <summary>
        /// Узел
        /// </summary>
        public GasExportObject GasExportObject { get; set; }
        
        /// <summary>
        /// Оператор
        /// </summary>
        public BalanceZone Operator { get; set; }
        
        /// <summary>
        /// Направление
        /// </summary>
        public Direction Direction { get; set; }

        public PathPoint(int _Id, GasExportObject _GasExportObject, BalanceZone _Operator, Direction _Direction)
        {
            Id = _Id;
            GasExportObject = _GasExportObject;
            Operator = _Operator;
            Direction = _Direction;
        }

        //TODO Возможно, тут следует перекрыть разные equals и сравнения

        public override bool Equals(object obj)
        {
            if (obj is PathPoint)
            {
                return Id.Equals((obj as PathPoint).Id);
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override string ToString()
        {
            return GasExportObject.Name.ToString() +  " [" + Operator.ToString() +  "] " + Direction.ToString();
        }
    }
}
