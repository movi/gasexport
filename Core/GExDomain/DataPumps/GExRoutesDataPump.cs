﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GEx.Domain;

namespace GEx.Domain.DataPumps
{
    /// <summary>
    /// Интерфейс DataPump маршрутной задачи GasExport
    /// </summary>
    public class GExRoutesDataPump : IGExBaseDataPump
    {
        /// <summary>
        /// Схема GasExport
        /// </summary>
        public Schema Scheme { get; set;  }

        /// <summary>
        /// Сценарий с данными GasExport
        /// </summary>
        public CalculationScenario CalcScenario { get; set; }

        /// <summary>
        /// Точка входа российского газа - исходная точка маршрутов
        /// </summary>
        public IOneZoneNode SourcePoint { get; set; }

        /// <summary>
        /// Граница (или ПХГ), выход на которую с любой балансовой зоны(оператора) -- финальная точка маршрутов
        /// </summary>
        public GasExportObject TargetPoint { get; set; }
    }
}