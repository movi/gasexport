﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GEx.Domain;

namespace GEx.Domain.DataPumps
{
    /// <summary>
    /// Интерфейс DataPump маршрутно-контрактной задачи GasExport
    /// </summary>
    public class GExContractsDataPump : IGExBaseDataPump
    {
        /// <summary>
        /// Схема GasExport
        /// </summary>
        public Schema Scheme { get; set; }

        /// <summary>
        /// Сценарий с данными GasExport
        /// </summary>
        public CalculationScenario CalcScenario { get; set; }

        /// <summary>
        /// Точки входа российского газа - исходные точки маршрутов
        /// </summary>
        public List<IOneZoneNode> SourcePoints { get; set; }

        /// <summary>
        /// Границы (или ПХГ) -- финальная точка маршрутов
        /// </summary>
        public List<GasExportObject> TargetPoints { get; set; }
    }
}