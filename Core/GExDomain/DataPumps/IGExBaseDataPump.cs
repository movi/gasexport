﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GEx.Domain;

namespace GEx.Domain.DataPumps
{
    /// <summary>
    /// Интерфейс общего DataPump для задач GasExport
    /// </summary>
    public interface IGExBaseDataPump : IGeneralDataPump
    {
        /// <summary>
        /// Схема GasExport
        /// </summary>
        Schema Scheme { get; }
        
        /// <summary>
        /// Сценарий с данными GasExport
        /// </summary>
        CalculationScenario CalcScenario { get; }
    }
}
