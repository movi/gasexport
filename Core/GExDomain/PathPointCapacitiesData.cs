﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Domain
{
    /// <summary>
    /// Доступные мощности и тарифы по маршрутной точке
    /// </summary>
    public class PathPointCapacitiesData
    {
        /// <summary>
        /// Уже забронированный объем, млн. м3
        /// </summary>
        public Nullable<double> BookedCapacity { get; set; }

        /// <summary>
        /// Доступные для бронирования объем, млн. м3
        /// </summary>
        public Nullable<double> AvailableCapacity { get; set; }

        /// <summary>
        /// Тариф на использование, Евро/млн. м3
        /// </summary>
        public Nullable<double> Tariff { get; set; }
    }
}
