﻿namespace GEx.Domain
{
    public class BalanceZone
    {
        public int ID { get; set; }

        public string Name { get; set;}

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var zone = obj as BalanceZone;
            if (zone != null)
            {
                return (ID == zone.ID);
            }
            return false;
        }
    }
}