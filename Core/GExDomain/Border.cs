﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Domain
{
    public class Border : GasExportObject
    {
        ///// <summary>
        ///// Первая балансовая зона (принадлежность к балансовой зоне для границы)
        ///// </summary>
        //public BalanceZone BorderZoneFirst
        //{
        //    get;
        //    set;
        //}

        ///// <summary>
        ///// Вторая балансовая зона (принадлежность к балансовой зоне для границы)
        ///// </summary>
        //public BalanceZone BorderZoneSecond
        //{
        //    get;
        //    set;
        //}
        public bool IsOutput { get; set; }

        public List<BorderInterconnection> Interconnections { get; set; }

        public override GasExportObjectType Type
        {
            get { return GasExportObjectType.Border; }
        }

        public Border()
        {
            //IsOutput = true;
            Interconnections = new List<BorderInterconnection>();
        }
    }
}
