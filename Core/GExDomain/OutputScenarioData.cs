﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Domain
{
    public class OutputScenarioData
    {
        public int Id { get; set; }

        public int ScenarioId { get; set; }

        public CalculationScenario Scenario { get; set; }
        
        //public Output Obj { get; set; } 

        // принято решение, что в контрактах определены границы с установленным признаком экспорта.
        //TODO (AlxB) : пункитом сдачи может быть и ПХГ, и у него тоже может быть контрактный объём!!! Переписать
        public Border Obj  { get; set; } 

        public Nullable<double> ContractVolume { get; set; }

        /// <summary>
        /// Признак того, что данные были загружены из БД
        /// по сути нужен только при удалении элемента со схемы
        /// </summary>
        public bool isLoaded = true;
    }
}
