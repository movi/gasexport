﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Domain
{
    public class RussianGasInputScenarioData
    {
        public int Id { get; set; }

        public int ScenarioId { get; set; }

        public CalculationScenario Scenario { get; set; }

        public RussianGasInput Obj { get; set; }

        public Nullable<double> MaxExportVolume { get; set; }

        /// <summary>
        /// Признак того, что данные были загружены из БД
        /// по сути нужен только при удалении элемента со схемы
        /// </summary>
        public bool isLoaded = true;
    }
}
