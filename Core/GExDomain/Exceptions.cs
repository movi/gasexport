﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Exceptions
{
    /// <summary>
    /// Ошибка доступа к базе данных при запуске приложения
    /// </summary>
    public class DBAccessFatalException : Exception
    {
        public DBAccessFatalException(string empty) : base(empty)
        {
        }
    }
}
