﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Domain
{
    public class RussianGasInput : GasExportObject, IOneZoneNode
    {

        public BalanceZone Zone
        {
            get;
            set;
        }

        public override GasExportObjectType Type
        {
            get { return GasExportObjectType.RussianGasInput; }
        }
    }
}
