﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Domain
{

    public abstract class GasExportObject
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public abstract  GasExportObjectType Type
        {
            get;
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var gasExportObject = obj as GasExportObject;
            if (gasExportObject != null)
            {
                return (ID == gasExportObject.ID);
            }
            return false;
        }
    }
}
