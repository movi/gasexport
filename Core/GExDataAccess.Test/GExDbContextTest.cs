﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GEx.DataAccess;
using GEx.Domain;
using System.Collections.Generic;
using System.Linq;

namespace GExDataAccess.Test
{

    public class NodeTraverser
    {
        private int Index { get; set; }

        private List<Tuple<Node, Node>> Edges { get; set; }

        public NodeTraverser()
        {
            Traverse = (node) =>
            {
                EveryObjectCallback(node,Index);
                Index = Index + 1;
                var relatedNodes =   node.RelatedNodes.OrderBy(n => n.ID).ToArray();
                foreach (var relNode in relatedNodes)
                {
                    if (!Edges.Exists(pair => pair.Item1.ID == node.ID && pair.Item2.ID == relNode.ID))
                    {
                        Edges.Add(new Tuple<Node, Node>(node, relNode));
                        Traverse(relNode);
                        
                    }
                }
            };
        }

        private Action<Node> Traverse;

        private Action<Node,int> EveryObjectCallback;

        public void TraverseObject(Node node, Action<Node,int> everyObjectCallback)
        {
            Edges = new List<Tuple<Node, Node>>();
            Index = 0;
            EveryObjectCallback = everyObjectCallback;
            Traverse(node);
        }
    }


    [TestClass]
    public class GExDbContextTest
    {
        #region Equality helpers

        public void TestSchemaEquality(Schema schemaRt, Schema schemaLft)
        {
            Assert.AreEqual(schemaRt.ID, schemaLft.ID);
            Assert.AreEqual(schemaRt.Name, schemaLft.Name);
            Assert.AreEqual(schemaRt.Nodes.Count, schemaLft.Nodes.Count);
           // TestIEnumerableNodesEuality(schemaRt.Nodes, schemaLft.Nodes);

            List<Node> nodes1 = new List<Node>();
            foreach(var n in schemaRt.Nodes.OrderBy(n=>n.ID))
            {
                
                NodeTraverser traverser = new NodeTraverser();
                Action<Node,int> equalTest =  (node,index)=>
                {
                    nodes1.Add(node);
                };
                traverser.TraverseObject(n,equalTest);
            }

            List<Node> nodes2 = new List<Node>();
            foreach (var n in schemaLft.Nodes.OrderBy(n => n.ID))
            {

                NodeTraverser traverser = new NodeTraverser();
                Action<Node, int> equalTest = (node, index) =>
                {
                    nodes2.Add(node);
                };
                traverser.TraverseObject(n, equalTest);
            }

            if(nodes1.Count==nodes2.Count)
            {
                for(int i = 0; i < nodes1.Count;i++)
                {
                   var n1= nodes1[i];
                   var n2 = nodes2[i];
                   TestNodeEqualityStrait(n1,n2);

                }
            }
            
        }

        //public void TestIEnumerableNodesEuality(IEnumerable<Node> nodes1, IEnumerable<Node> nodes2)
        //{
        //    var nodes1Ordered = nodes1.OrderBy(n => n.ID);
        //    var nodes2Ordered = nodes2.OrderBy(n => n.ID);

        //    for (int i = 0; i < nodes1.Count(); i++)
        //    {
        //        var node1 = nodes1Ordered.ElementAt(i);
        //        var node2 = nodes2Ordered.ElementAt(i);
        //        TestNodeEquality(node1, node2);

        //    }
        //}

        public void TestNodeEqualityStrait(Node node1, Node node2)
        {
            Assert.AreEqual(node1.ID, node2.ID);
            Assert.AreEqual(node1.RelatedNodes.Count, node2.RelatedNodes.Count);
            TestGasExportObjectEquality(node1.GasExportObject, node2.GasExportObject);
        }



        //public void TestNodeEquality(Node node1, Node node2)
        //{
        //    Assert.AreEqual(node1.ID, node2.ID);
        //    Assert.AreEqual(node1.RelatedNodes.Count, node2.RelatedNodes.Count);
        //    TestGasExportObjectEquality(node1.GasExportObject, node2.GasExportObject);
        //    TestIEnumerableNodesEuality(node1.RelatedNodes, node2.RelatedNodes);

        //}

        public void TestGasExportObjectEquality(GasExportObject netObj1, GasExportObject netObj2)
        {
            Assert.AreEqual(netObj1.ID, netObj2.ID);
            Assert.AreEqual(netObj1.Name, netObj2.Name);
        }



        public void TesBalanceZoneEquality(BalanceZone zone1, BalanceZone zone2)
        {
            Assert.AreEqual(zone1.ID, zone2.ID);
            Assert.AreEqual(zone1.Name, zone2.Name);
        }
        #endregion

        public Schema ComposeSimpleSchema()
        {
            Schema newSchema = new Schema() { Name = "Schema", CreationDate = DateTime.Now, Author = "UnitTest.ComposeSchema" };
            var node1 = new Node() { X=1,Y=1,GasExportObject = new Border() { Name = "Border1" } };
            newSchema.Nodes.Add(node1);
            return newSchema;

        }


        public Schema ComposeSchema()
        {
            BalanceZone zone1 = new BalanceZone ();
            BalanceZone zone2 = new BalanceZone ();
            Schema newSchema = new Schema() { Name = "Schema", CreationDate = DateTime.Now, Author = "UnitTest.ComposeSchema" };
            var node1 = new Node() { GasExportObject = new Border() { Name = "Border", BorderZoneFirst = zone1, BorderZoneSecond = zone2 } };
            var node2 = new Node() { GasExportObject = new RussianGasInput() { Name = "RussianGasInput",Zone=zone2 } };
            var node3 = new Node() { GasExportObject = new Output() { Name = "Output",Zone=zone2 } };
            newSchema.Nodes.Add(node1);
            newSchema.Nodes.Add(node2);
            newSchema.Nodes.Add(node3);
            node1.RelatedNodes.Add(node2);
            node1.RelatedNodes.Add(node3);
            return newSchema;

        }


        public Schema ComposeSchemaWithoutRelations()
        {
            BalanceZone zone1 = new BalanceZone();
            BalanceZone zone2 = new BalanceZone();
            Schema newSchema = new Schema() { Name = "Schema", CreationDate = DateTime.Now, Author = "UnitTest.ComposeSchema" };
            var node1 = new Node() { GasExportObject = new Border() { Name = "Border", BorderZoneFirst = zone1, BorderZoneSecond = zone2 } };
            var node2 = new Node() { GasExportObject = new RussianGasInput() { Name = "RussianGasInput", Zone = zone2 } };

            newSchema.Nodes.Add(node1);
            newSchema.Nodes.Add(node2);
     

            return newSchema;

        }


        public Schema ComposeSchemaWithZones()
        {
            Schema newSchema = new Schema() { Name = "Schema_With_Zone", CreationDate = DateTime.Now, Author = "UnitTest.ComposeSchema" };
            var node1 = new Node()
            {
                GasExportObject = new Border()
                {
                    Name = "Border",
                    BorderZoneFirst = new BalanceZone() { Name="z1"},
                    BorderZoneSecond = new BalanceZone() { Name = "z2" }
                }
            };
            var node2 = new Node()
            {
                GasExportObject = new RussianGasInput()
                {
                    Name = "RGI",
                    Zone = new BalanceZone() { Name = "RGI" }
                }
            };
            
            newSchema.Nodes.Add(node1);
            newSchema.Nodes.Add(node2);
            return newSchema;

        }

        [TestMethod]
        public void CreateSchemaAndLoadSchema()
        {
            var newSchema = ComposeSchema();
            var repo = new SchemaRepository();
            repo.Create(newSchema);
            var loadedSchema = repo.Load(newSchema.ID);
            TestSchemaEquality(newSchema, loadedSchema);
        }

        [TestMethod]
        public void CreateSchemaWithZonesAndLoadSchema()
        {
            var newSchema = ComposeSchemaWithZones();
            var repo = new SchemaRepository();
            repo.Create(newSchema);
            var loadedSchema = repo.Load(newSchema.ID);
            TestSchemaEquality(newSchema, loadedSchema);
        }

        [TestMethod]
        public void UpdateSchema_AddNewNodesWithoutRelations()
        {
            var newSchema = ComposeSchema();
            var repo = new SchemaRepository();
            repo.Create(newSchema);
            var loadedSchema = repo.Load(newSchema.ID);
            TestSchemaEquality(newSchema, loadedSchema);

            loadedSchema.Name = "Add 3 Nodes";
            loadedSchema.CreationDate = DateTime.Now;
            var newNode1 = new Node() { GasExportObject = new Border() { Name = "Added-Border" } };
            var newNode2 = new Node() { GasExportObject = new Output() { Name = "Added-Output" } };
            var newNode3 = new Node() { GasExportObject = new UGS() { Name = "Added-UGS" } };

            //var first = loadedSchema.Nodes.FirstOrDefault();
            //first.RelatedNodes.RemoveAt(0);
            //var last = loadedSchema.Nodes.LastOrDefault();
            //  loadedSchema.Nodes.Remove(last);
            loadedSchema.Nodes.Add(newNode1);
            loadedSchema.Nodes.Add(newNode2);
            loadedSchema.Nodes.Add(newNode3);
            // newNode.RelatedNodes.Add(first);//cause error

            repo.Update(loadedSchema);
            var updatedSchema = repo.Load(loadedSchema.ID);

            TestSchemaEquality(updatedSchema, loadedSchema);
        }

        [TestMethod]
        public void UpdateSchema_RemoveExistNodes()
        {
            var newSchema = ComposeSchema();
            var repo = new SchemaRepository();
            repo.Create(newSchema);
            var loadedSchema = repo.Load(newSchema.ID);
            TestSchemaEquality(newSchema, loadedSchema);


            var last = loadedSchema.Nodes.LastOrDefault();
            loadedSchema.Nodes.Remove(last);
            var first = loadedSchema.Nodes.FirstOrDefault();
            loadedSchema.Nodes.Remove(first);
            // newNode.RelatedNodes.Add(first);//cause error

            repo.Update(loadedSchema);
            var updatedSchema = repo.Load(loadedSchema.ID);
            Assert.AreEqual(newSchema.ID, updatedSchema.ID);
            TestSchemaEquality(updatedSchema, loadedSchema);
        }

        [TestMethod]
        public void UpdateSchema_RemoveExistRelations()
        {
            var newSchema = ComposeSchema();
            var repo = new SchemaRepository();
            repo.Create(newSchema);
            var loadedSchema = repo.Load(newSchema.ID);
            TestSchemaEquality(newSchema, loadedSchema);

            loadedSchema.Name = "Modified";
            loadedSchema.CreationDate = DateTime.Now;

            foreach (var node in loadedSchema.Nodes)
            {
                node.RelatedNodes.Clear();
            }
            repo.Update(loadedSchema);
            var updatedSchema = repo.Load(loadedSchema.ID);
            TestSchemaEquality(updatedSchema, loadedSchema);

        }

        [TestMethod]
        public void UpdateSchema_AddNewNodesWithoutRelations_RemoveExistNodes_RemoveExistRelations()
        {
            var newSchema = ComposeSchema();
            var repo = new SchemaRepository();
            repo.Create(newSchema);
            var loadedSchema = repo.Load(newSchema.ID);
            TestSchemaEquality(newSchema, loadedSchema);


            var first = loadedSchema.Nodes.FirstOrDefault();
            var last = loadedSchema.Nodes.LastOrDefault();
            first.RelatedNodes.RemoveAt(0);
            loadedSchema.Nodes.Remove(last);

            repo.Update(loadedSchema);

            var updatedSchema = repo.Load(loadedSchema.ID);
            Assert.AreEqual(newSchema.ID, updatedSchema.ID);
            TestSchemaEquality(updatedSchema, loadedSchema);
        }

        [TestMethod]
        public void UpdateSchema_AddNewNodeRelationOnly()
        {
            var newSchema = ComposeSchemaWithoutRelations();
            var repo = new SchemaRepository();
            repo.Create(newSchema);
            var loadedSchema = repo.Load(newSchema.ID);
            TestSchemaEquality(newSchema, loadedSchema);

            var first = loadedSchema.Nodes.FirstOrDefault();
            var last = loadedSchema.Nodes.LastOrDefault();
            last.RelatedNodes.Add(first);//cause error

            repo.Update(loadedSchema);
            var updatedSchema = repo.Load(loadedSchema.ID);
            Assert.AreEqual(newSchema.ID, updatedSchema.ID);
            TestSchemaEquality(updatedSchema, loadedSchema);
        }


        [TestMethod]
        public void UpdateSchema_AddNewNodeWithRelationToExistNode()
        {
            var newSchema = ComposeSimpleSchema();
            var repo = new SchemaRepository();
            repo.Create(newSchema);
            var loadedSchema = repo.Load(newSchema.ID);
            TestSchemaEquality(newSchema, loadedSchema);


            var first = loadedSchema.Nodes.FirstOrDefault();
            var newNode1 = new Node() { GasExportObject = new Border() { Name = "test" } };
            loadedSchema.Nodes.Add(newNode1);
            newNode1.RelatedNodes.Add(first);

            repo.Update(loadedSchema);
            var updatedSchema = repo.Load(loadedSchema.ID);
            Assert.AreEqual(newSchema.ID, updatedSchema.ID);
            TestSchemaEquality(updatedSchema, loadedSchema);
        }

        [TestMethod]
        public void UpdateSchema_AddNewNodeWithRelationToNewNode()
        {
            var newSchema = ComposeSimpleSchema();
            var repo = new SchemaRepository();
            repo.Create(newSchema);
            var loadedSchema = repo.Load(newSchema.ID);
            TestSchemaEquality(newSchema, loadedSchema);


            var first = loadedSchema.Nodes.FirstOrDefault();
            var newNode1 = new Node() { GasExportObject = new Border() { Name = "b1" } };
            var newNode2 = new Node() { GasExportObject = new Output() { Name = "o1" } };
            //var newNode3 = new Node() { GasExportObject = new UGS() { Name = "Added-UGS" } };

            loadedSchema.Nodes.Add(newNode1);
            loadedSchema.Nodes.Add(newNode2);
            newNode1.RelatedNodes.Add(newNode2);

            repo.Update(loadedSchema);
            var updatedSchema = repo.Load(loadedSchema.ID);
            TestSchemaEquality(updatedSchema, loadedSchema);
        }


        [TestMethod]
        public void UpdateSchema_AddNewNodeWithRelationToNewNodeAndExistNode()
        {
            var newSchema = ComposeSimpleSchema();
            var repo = new SchemaRepository();
            repo.Create(newSchema);
            var loadedSchema = repo.Load(newSchema.ID);
            TestSchemaEquality(newSchema, loadedSchema);

            var zoneRepo = new BalanceZoneRepository();
            var zones =   zoneRepo.LoadAll();

            
            var first = loadedSchema.Nodes.FirstOrDefault();
            var newNode1 = new Node() {X=2,Y=2, GasExportObject = new Border() { Name = "Border2" } };
            var newNode2 = new Node() {X=3,Y=3, GasExportObject = new Output() { Name = "Output3" } };
            //var newNode3 = new Node() { GasExportObject = new UGS() { Name = "Added-UGS" } };

            loadedSchema.Nodes.Add(newNode1);
            loadedSchema.Nodes.Add(newNode2);
            newNode1.RelatedNodes.Add(newNode2);
            newNode1.RelatedNodes.Add(first);


            repo.Update(loadedSchema);
            var updatedSchema = repo.Load(loadedSchema.ID);
            TestSchemaEquality(updatedSchema, loadedSchema);
        }


    }
}
