// /////////////////////////////////////////////////////////////
// File: 
// Date:					Author: Ildar Batyrshin
// Language: C#				Framework: .NET 2.0
// /////////////////////////////////////////////////////////////

using System;
using System.IO;
using System.Text;

namespace GEx.Log
{
    /// <summary>
    /// Summary description for DailyFile.
    /// </summary>
    public class DailyFile
    {
        FileStream _fout = null;
        int _nSeriesIndex;
        private const int HOUR_DAY_BEGIN = 6;
        private const int CODEPAGE = 1251;
        string _strPath;
        string _strNameBase;
        string _strExt = "log";
        string _strNameFullOld;
        int _nDay;
        string _strHeader = "";

        private enum eCloseOpenReason
        {
            COR_DATE_CHANGED,
            COR_SIZE_LIM_REACHED
        }

        public DailyFile()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public string Header
        {
            set
            {
                _strHeader = value;
            }
        }

        void GetNameFull(ref string strName)
        {
            string strDay = _nDay.ToString("00");
            string strSeries = "";
            if (_nSeriesIndex > 0)
                strSeries = _nSeriesIndex.ToString();
            strName = _strNameBase + "_" + strDay + strSeries + "." + _strExt;
        }

        public bool Writeln(string param)
        {
            string strErrSource = "DailyFile::Writeln()";
            bool bRes = true;
            if (_fout.CanWrite)
            {
                try
                {
                    Encoding enc = Encoding.GetEncoding(CODEPAGE);
                    byte[] strTempBytes = enc.GetBytes(param);
                    int strTempBytesCount = enc.GetByteCount(param);
                    _fout.Write(strTempBytes, 0, strTempBytesCount);
                    _fout.WriteByte(0xD);
                    _fout.WriteByte(0xA);
                    _fout.Flush();
                }
                catch
                {
                    //strTemp = "empty line, failed to convert korean characters"; 
                }
                //_fout.Write( << strTemp.c_str() << std::endl << std::flush;
                if (!_fout.CanWrite)
                {
                    bRes = false;
                }
            }
            return bRes;
        }

        public bool Open(string strFileName)
        {
            if (string.IsNullOrEmpty(strFileName))
                return false;

            DateTime now = DateTime.Now;
            _nDay = now.Day;
            if (0 <= now.Hour && now.Hour < HOUR_DAY_BEGIN)
                _nDay = Yesterday(now);

            _strPath = Path.GetDirectoryName(strFileName) + @"\";
            string strFileNameExt = Path.GetFileName(strFileName);
            _strNameBase = Path.GetFileNameWithoutExtension(strFileName);
            GetNameFull(ref _strNameFullOld);
            string strFilePathToOpen = _strPath + _strNameFullOld;
            FileMode openMode = FileMode.Create;
            if (File.Exists(strFilePathToOpen))
            {
                FileInfo fi = new FileInfo(strFilePathToOpen);
                DateTime dt = fi.LastWriteTime;
                //if today
                if (dt.Year == now.Year &&
                    dt.Month == now.Month &&
                    dt.Day == now.Day)
                    openMode = FileMode.Append;
            }
            _fout = new FileStream(strFilePathToOpen, openMode, FileAccess.Write, FileShare.ReadWrite);
            if (!_fout.CanWrite)
            {
                return false;
            }
            Writeln(_strHeader);
            Writeln(String.Format("������ {0}", DateTime.Now.ToString("F")));
            return true;
        }

        public bool Close()
        {
            if (_fout.CanWrite)
            {
                _fout.Flush();
                _fout.Close();
            }
            return true;
        }

        public bool CloseAndOpenNew()
        {
            string strErrSource = "CLogBase::CloseAndOpenNew";
            if (0 == _strPath.Length)
                return false;
            string strNameFull = "";
            GetNameFull(ref strNameFull);
            string strPathFileCopy = _strPath + strNameFull;
            Close();

            try
            {
                File.Delete(strPathFileCopy);
            }
            catch
            {
                _nSeriesIndex++;
                string strMsg = "Failed to delete ";
                strMsg += strPathFileCopy;
                //ReportError(EC_ERROR,strMsg,strErrSource,dwError);
                GetNameFull(ref strNameFull);
                strPathFileCopy = _strPath + strNameFull;
            }

            _fout = new FileStream(strPathFileCopy, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
            Writeln(_strHeader);
            Writeln(String.Format("������ {0}", DateTime.Now.ToString("F")));
            return _fout.CanWrite;
        }

        int Yesterday(DateTime dt)
        {
            DateTime yesterday = dt.AddDays(-1);
            return yesterday.Day;
        }

        public bool IsDateChanged()
        {
            DateTime now = DateTime.Now;
            if (now.Day != _nDay && now.Hour >= HOUR_DAY_BEGIN)
            {
                _nDay = now.Day;
                return true;
            }
            return false;
        }
    }
}
