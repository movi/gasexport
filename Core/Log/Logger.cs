// /////////////////////////////////////////////////////////////
// File: 
// Date:					Author: Ildar Batyrshin
// Language: C#				Framework: .NET 2.0
// /////////////////////////////////////////////////////////////

namespace GEx.Log
{
    using System;
    using System.Collections;
    using System.Diagnostics;
    using System.IO;
    using System.Security.Permissions;
    using System.Security.Principal;
    using System.Threading;
    using GEx.ErrorHandler;

    public class Logger
    {
        private Thread _thread = null;
        private Queue _queue = null;
        private bool _exit = false;
        private static Logger instance = null;
        private static string _nameBase = string.Empty;

        private class QueueItem
        {
            public eErrClass _errorClass;
            public string _msg;
            public string _source;
            public int _threadId;
            public int _userId;
            public QueueItem(eErrClass errorClass, string msg, string source, int userId)
            {
                _errorClass = errorClass;
                _msg = msg;
                _source = source;
                _threadId = Thread.CurrentThread.ManagedThreadId;
                _userId = userId;
            }
        }

        public static Logger Instance
        {
            get
            {
                if (instance == null)
                    instance = new Logger();
                return instance;
            }
        }

        public static string Name
        {
            set
            {
                if (instance == null)
                    instance = new Logger();
                _nameBase = value;
            }
        }

        private char GetMark(eErrClass errorClass)
        {
            switch (errorClass)
            {
                case eErrClass.EC_TRACE:
                    return 't';
                case eErrClass.EC_INFO:
                    return 'i';
                case eErrClass.EC_WARN:
                    return '!';
                case eErrClass.EC_ERROR:
                    return 'e';
                case eErrClass.EC_FATAL:
                    return 'x';
            }
            return '?';
        }

        private string GetTimeMs(DateTime dt)
        {
            return dt.ToString("T") + ":" + dt.Millisecond.ToString("000");
        }

        public void Start()
        {
            if (null == _thread)
            {
                _queue = new Queue();
                _thread = new Thread(new ThreadStart(LogThread));
                _thread.Name = "Log thread";
                _thread.Start();
            }
        }

        // UI or other thread (differs from LogThread)
        public void Log(eErrClass errorClass, string msg, string source, int userId)
        {
            lock (_queue.SyncRoot)
            {
                _queue.Enqueue(new QueueItem(errorClass, msg, source, userId));
            }
        }

        // Worker thread, _queue is used from multiple threads, sync is a must
        private void LogThread()
        {
            DailyFile file = null;
            string path = AppDomain.CurrentDomain.BaseDirectory;
            try
            {
                try
                {
                    new FileIOPermission(FileIOPermissionAccess.AllAccess, path).Demand();
                    path += @"\Logs\";
                }
                catch (Exception ex)
                {
                    path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                    path += @"\HydroCalc\Logs\";
                }

                //WindowsPrincipal pricipal = new WindowsPrincipal(WindowsIdentity.GetCurrent());
                //bool hasAdministrativeRight = pricipal.IsInRole(WindowsBuiltInRole.Administrator);
                //if (hasAdministrativeRight)                
                //    path = AppDomain.CurrentDomain.BaseDirectory;
                //else
                //    path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);                
                //new FileIOPermission(FileIOPermissionAccess.AllAccess, path).Demand();
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                file = new DailyFile();
                //file.Header = String.Format("Created {0}",DateTime.Now.ToString("F"));
                string fileName = string.IsNullOrEmpty(_nameBase) ? AppDomain.CurrentDomain.FriendlyName : _nameBase;
                fileName = Path.GetFileNameWithoutExtension(fileName);
                file.Open(path + fileName);
                while (!_exit)
                {
                    try
                    {
                        if (file.IsDateChanged())
                        {
                            file.CloseAndOpenNew();
                        }

                        //DO NOT USE IEnumerators 
                        //lock(_queue.SyncRoot)
                        //{
                        //	IEnumerator i = _queue.GetEnumerator();
                        //	while (i.MoveNext())
                        //	{
                        //		QueueItem item = (QueueItem)i.Current;
                        //		string line = String.Format("[{0}] [{1}] {2}, {3}",Helpers.GetTimeMs(),GetMark(item._errorClass),item._msg,item._source);
                        //		file.Writeln(line);
                        //		_queue.Dequeue();
                        //	}
                        //}

                        lock (_queue.SyncRoot)
                        {
                            while (_queue.Count > 0)
                            {
                                QueueItem item = (QueueItem)_queue.Dequeue();
                                string uid = (-1 == item._userId) ? "  " : item._userId.ToString("00");
                                string line = String.Format("{0} [{1}] {2} : {5} {3}, {4}", GetTimeMs(DateTime.Now), GetMark(item._errorClass), item._threadId.ToString("00"), item._msg, item._source, uid);
                                file.Writeln(line);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message + e.Source + " LogThread");
                    }
                    finally
                    {
                        System.Threading.Thread.Sleep(100);
                    }
                }
            }
            catch
            {
            }
            finally
            {
                lock (_queue.SyncRoot)
                {
                    while (_queue.Count > 0)
                    {
                        QueueItem item = (QueueItem)_queue.Dequeue();
                        string line = String.Format("{0} [{1}] {2} : {3}, {4}", GetTimeMs(DateTime.Now), GetMark(item._errorClass), item._threadId.ToString("00"), item._msg, item._source);
                        file.Writeln(line);
                    }
                }
                file.Close();
            }
        }

        public void Stop()
        {
            _exit = true;
        }

    }
}
