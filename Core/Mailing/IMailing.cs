﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Mailing
{
    public interface IMailing
    {
        void SendMail(String subject, String text, String UserFrom,  IList<String> UsersTo, IList<string> attach, Int32 Port = 25, String SmtpServerName = "smtp");
        void SendMail(String subject, String text, String UserFrom, IList<String> UsersTo, Int32 Port = 25, String SmtpServerName = "smtp");
        void SendMail(SmtpClient clnt, MailMessage msg);

        event EventHandler OnMailingFailed;
        event EventHandler OnMailingSucceeded;
    }
}
