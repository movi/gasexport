﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SolverFoundation.Services;
using SolverFoundation.Plugin.XpressMP;

namespace GEx.Calculate
{
    /// <summary>
    /// Класс решателя модели маршрутной задачи GasExport
    /// </summary>
    public class GExContractsModelSolver : GExBaseModelSolver
    {
        #region Коэффициенты критерия

        public const double CoeffUndelivery = 1e15;
        public const double CoeffNewBooking = 1e8;
        public const double CoeffInterruptibleTransport = 1e3;
        public const double CoeffGuaranteedTransport = 1;

        #endregion //Коэффициенты критерия

        #region Fields

        protected NLPSolverPlugin.NLPModel MpModel;
        protected SolverContext Context;
        protected SolverCapability solverType;

        private GEx.ErrorHandler.IErrorOutput _errorHandler = null;

        /// <summary>
        /// Все переменные (поток, отбор и т.д.) создаются с таким верхним ограничением
        /// Некоторые отдельные переменные могут создаваться с другим ограничением, которое отталкивается от данного базового
        /// </summary>
        protected double variablesRange = 1e10;

        #endregion

        private new GExContractsModel Model
        {
            get { return base.Model as GExContractsModel; }
        }

        private new GExContractsVariablesPool VariablesPool
        {
            get { return base.VariablesPool as GExContractsVariablesPool; }
        }

        /// <summary>
        /// тип задачи мат.программирования
        /// по умолчанию ЛП
        /// </summary>
        public SolverCapability SolverType
        {
            get { return solverType; }
            set { solverType = value; }
        }

        /// <summary>
        /// Строка с сообщением об ошибке
        /// </summary>
        public string ErrorString { get; private set; }

        public GExContractsModelSolver()
        {
            Settings = new GExBaseModelSettings();
            solverType = SolverCapability.LP;
            ErrorString = "";
        }

        public void Init(GEx.ErrorHandler.IErrorOutput eh)
        {
            _errorHandler = eh;
        }

        public override bool Solve(GExBaseModel mdl)
        {
            try
            {
                return SolveInternal(mdl);
            }
            catch (Exception e)
            {
                _errorHandler.ReportError(ErrorHandler.eErrClass.EC_ERROR, e.ToString(), "GEx.Calculate.GExContractsModelSolver.Solve()");
                throw e;
            }
        }


        public bool SolveInternal(GExBaseModel mdl)
        {
            if (mdl is GExContractsModel)
            {
                base.Model = mdl;
                base.VariablesPool = new GExContractsVariablesPool();

                _errorHandler.ReportError(ErrorHandler.eErrClass.EC_INFO, "Начало расчёта потоков", "GEx.Calculate.GExContractsModelSolver.SolveInternal()");

                Context = SolverContext.GetContext();
                Context.ClearModel();
                MpModel = new NLPSolverPlugin.NLPModel(Context.CreateModel());

                _errorHandler.ReportError(ErrorHandler.eErrClass.EC_INFO, "Модель создана", "GEx.Calculate.GExContractsModelSolver.SolveInternal()");
               
                CreateVariables();
                AddConstraints();
                AddCriterion();

                var result = CallSolver(Context, solverType);

                if (!result)
                {
                    _errorHandler.ReportError(ErrorHandler.eErrClass.EC_INFO, "Ошибка при решении задачи: " + ErrorString, "GEx.Calculate.GExContractsModelSolver.SolveInternal()");
                }                 

                return result;                
            }
            else
            {
                return false;
            }
        }

        #region Protected methods

        #region Добавление переменных

        /// <summary>
        /// Создание переменных
        /// </summary>
        protected virtual void CreateVariables()
        {
            //Объявляем переменные и указываем их типы
            CreateEdgeVariables();
            CreateVertexVariables();

            AddVariablesToMpModel();
        }

        protected virtual void CreateEdgeVariables()
        {
            foreach (var edge in Model.Graph.Edges)
            {
                long edgeID = edge.GetID();
                VariablesPool.EdgeFlows[edge] = MpModel.AddRealDecision(GetStringForMsf(edge.GetID(), "EdgeFlow"), 0, variablesRange);
                VariablesPool.EdgeAdditionalBookingFlows[edge] = MpModel.AddRealDecision(GetStringForMsf(edge.GetID(), "EdgeFlowAdditionalBookingFlow"), 0, variablesRange); 
            }
        }

        protected virtual void CreateVertexVariables()
        {
            foreach (var vertex in Model.Graph.Vertices)
            {
                VariablesPool.VertexInFlows[vertex] = MpModel.AddRealDecision(GetStringForMsf(vertex.GetID(), "VertexInFlow"), 0, variablesRange);
                VariablesPool.VertexOutFlows[vertex] = MpModel.AddRealDecision(GetStringForMsf(vertex.GetID(), "VertexOutFlow"), 0, variablesRange);
                VariablesPool.VertexDisbalancesPlus[vertex] = MpModel.AddRealDecision(GetStringForMsf(vertex.GetID(), "VertexDisbalancesPlus"), 0, variablesRange);
                VariablesPool.VertexDisbalancesMinus[vertex] = MpModel.AddRealDecision(GetStringForMsf(vertex.GetID(), "VertexDisbalancesMinus"), 0, variablesRange);
                VariablesPool.AdditionalCapacityBookings[vertex] = MpModel.AddRealDecision(GetStringForMsf(vertex.GetID(), "AdditionalCapacityBookings"), 0, variablesRange);                
            }
        }

        /// <summary>
        /// Добавление переменных в модель MSF
        /// </summary>
        protected virtual void AddVariablesToMpModel()
        {
            MpModel.AddDecisions(VariablesPool.GetAllVariables().ToArray());
        }

        #endregion //Добавление переменных

        #region Добавление ограничений

        protected virtual void AddConstraints()
        {
            AddInAndOutFlowConstraints();
            AddEdgeFlowsConstraints();
            AddVertexBalanceConstraints();
            AddVertexDisbalanceConstraints();
            AddCapacityConstraints();
            AddAdditionalCapacityBoundConstraints();
        }

        /// <summary>
        /// Уравнения связи потоковых переменных по дуге
        /// </summary>
        protected virtual void AddEdgeFlowsConstraints()
        {
            foreach (var edge in Model.Graph.Edges)
                MpModel.AddConstraint(GetStringForMsf(edge.GetID(), "EdgeVarsLink"),
                        VariablesPool.EdgeAdditionalBookingFlows[edge] <= VariablesPool.EdgeFlows[edge]);
        }

        /// <summary>
        /// Уравнения, определяющие входящие и исходящие потоки по узлам
        /// </summary>
        protected virtual void AddInAndOutFlowConstraints()
        {
            //По узлам
            foreach (var vertex in Model.Graph.Vertices)
            {
                var inEdges = Model.Graph.Connections[vertex].Item2.Cast<GExGraphEdge>().ToArray();
                var outEdges = Model.Graph.Connections[vertex].Item1.Cast<GExGraphEdge>().ToArray();

                Term tIn = 0, tOut = 0;
                inEdges.ForEach(e => tIn += VariablesPool.EdgeFlows[e]);
                outEdges.ForEach(e => tOut += VariablesPool.EdgeFlows[e]);
                tIn -= VariablesPool.VertexInFlows[vertex];
                tOut -= VariablesPool.VertexOutFlows[vertex];

                MpModel.AddConstraint(GetStringForMsf(vertex.GetID(), "VertexInFlowEq"),
                        tIn == 0);
                MpModel.AddConstraint(GetStringForMsf(vertex.GetID(), "VertexOutFlowEq"),
                        tOut == 0);
            }
        }

        /// <summary>
        /// Балансы в узлах 
        /// </summary>
        protected virtual void AddVertexBalanceConstraints()
        {
            foreach (var vertex in Model.Graph.Vertices)
            {
                Term t = VariablesPool.VertexInFlows[vertex] - VariablesPool.VertexOutFlows[vertex];
                t -= (VariablesPool.VertexDisbalancesPlus[vertex] - VariablesPool.VertexDisbalancesMinus[vertex]);

                double rhs = -(Model.Graph.AvailableVolumes.ContainsKey(vertex) ? Model.Graph.AvailableVolumes[vertex] : 0);

                MpModel.AddConstraint(GetStringForMsf(vertex.GetID(), "VertexBalance"),
                    t == rhs);
            }           
        }
        
        /// <summary>
        /// Ограничения на дисбалансы в точках приёма и сдачи газа, в остальных вершинах - 0 
        /// </summary>
        protected virtual void AddVertexDisbalanceConstraints()
        {            
            // По обычным узлам и источникам
            // -------------------------------------------------------------------
            foreach (var vertex in Model.Graph.Vertices)
            {
                if (!Model.TargetVertices.Contains(vertex))
                {
                    double maxVertexDisbalancePlus = 0,
                           maxVertexDisbalanceMinus = 0;

                    if (Model.Graph.AvailableVolumes.ContainsKey(vertex))
                    {
                        maxVertexDisbalancePlus = Model.Graph.AvailableVolumes[vertex];
                    }                   

                    Term disbalancePlus = VariablesPool.VertexDisbalancesPlus[vertex];
                    Term disbalanceMinus = VariablesPool.VertexDisbalancesMinus[vertex];

                    MpModel.AddConstraint(GetStringForMsf(vertex.GetID(), "VertexDisbalancePlusBound"),
                            disbalancePlus <= maxVertexDisbalancePlus);
                    MpModel.AddConstraint(GetStringForMsf(vertex.GetID(), "VertexDisbalanceMinusBound"),
                            disbalanceMinus <= maxVertexDisbalanceMinus);
                }
            }

            //По целевым узлам, принадлежащим агрегированным точкам сдачи
            // -------------------------------------------------------------------
            
            // отрицательный дисбаланс в точках сдачи - 0
            foreach (var vertex in Model.TargetVertices)
            {
                double maxVertexDisbalanceMinus = 0;

                Term disbalanceMinus = VariablesPool.VertexDisbalancesMinus[vertex];                

                MpModel.AddConstraint(GetStringForMsf(vertex.GetID(), "VertexDisbalanceMinusBound"),
                        disbalanceMinus <= maxVertexDisbalanceMinus);
            }

            // положительный дисбаланс - суммарный спрос по агрегату
            foreach (Domain.GasExportObject geObj in Model.Graph.ContractVolumes.Keys)
            {
                bool isTargetAggregate = false;

                Term t = 0;
                double rhs = 0;
                foreach (var vertex in Model.Graph.Vertices.Where(v => v.Point.GasExportObject == geObj && Model.TargetVertices.Contains(v)))
                {                    
                    t += VariablesPool.VertexDisbalancesPlus[vertex];                   

                    isTargetAggregate = true;
                }

                if (isTargetAggregate)
                {
                    rhs = Model.Graph.ContractVolumes[geObj];

                    MpModel.AddConstraint(GetStringForMsf(geObj.ID, "AggrVertexDisbalancePlusBound"),
                            t <= rhs);
                }
            }           
        }

        /// <summary>
        /// Ограничения на потоки через узлы и дуги графа с учётом уже забронированных и потенциально доступных мощностей
        /// </summary>
        protected void AddCapacityConstraints()
        {
            foreach (var vertex in Model.Graph.Vertices)
            {
                var inEdges = Model.Graph.Connections[vertex].Item2.Cast<GExGraphEdge>().ToArray();
                Term tIn = 0;
                inEdges.ForEach(e => tIn += VariablesPool.EdgeAdditionalBookingFlows[e]);
                tIn -= VariablesPool.AdditionalCapacityBookings[vertex];

                MpModel.AddConstraint(GetStringForMsf(vertex.GetID(), "EdgesToVertexCapacityLink"), tIn == 0);
                
                
                if (!Model.SourceVertices.Contains(vertex))
                {
                    Term t = VariablesPool.VertexInFlows[vertex] - VariablesPool.AdditionalCapacityBookings[vertex];
                    
                    MpModel.AddConstraint(GetStringForMsf(vertex.GetID(), "VertexCapacityBound"),
                            t <= Model.Graph.BookedCapacities[vertex]);                    
                }
            }
        }

        /// <summary>
        /// Ограничения на доступные для потенциального бронирования мощности
        /// </summary>
        protected void AddAdditionalCapacityBoundConstraints()
        {
            foreach (var vertex in Model.Graph.Vertices)
            {
                Term t = VariablesPool.AdditionalCapacityBookings[vertex];

                MpModel.AddConstraint(GetStringForMsf(vertex.GetID(), "VertexAdditionalCapacityBound"),
                        t <= Model.Graph.AvailableCapacities[vertex]);
            }
        }

        #endregion // Добавление ограничений

        #region Составление критерия

        /// <summary>
        /// Добавление критерия оптимизации
        /// </summary>
        protected void AddCriterion()
        {
            MpModel.AddGoal("Criteria", GoalKind.Minimize, CreateCriterion());
        }

        /// <summary>
        /// Создание критерия оптимизации
        /// </summary>
        /// <returns></returns>
        protected virtual Term CreateCriterion()
        {
            Term goal = 0;

            foreach (var vertex in Model.TargetVertices)
            {
                // Недопоставки по контрактам
                goal -= CoeffUndelivery * VariablesPool.VertexDisbalancesPlus[vertex];
            }

            // по узлам
            foreach (var vertex in Model.Graph.Vertices)
            {
                // Бронирование дополнительных мощностей (постоянная часть тарифа)
                goal += CoeffNewBooking * VariablesPool.AdditionalCapacityBookings[vertex] * (Model.Graph.BookingCosts[vertex] + 0.0000001);          
            }

            
            // по дугам (поток)
            foreach (var edge in Model.Graph.Edges)
            {
                if (Model.Graph.FirmDeliveryProbability.ContainsKey(edge))
                {
                    // Транспорт (переменная часть тарифа).
                    // С каждой обычной дуги берём половину тарифа от каждой точки, от точек в начале и в конце маршрута - по полному тарифу.
                    double cSource = Model.SourceVertices.Contains(edge.Source) ? 1 : 0.5;
                    double cTarget = Model.TargetVertices.Contains(edge.Target) ? 1 : 0.5;

                    double tariff = cSource * (Model.Graph.FlowCosts[edge.Source] + 0.0000001) + cTarget * (Model.Graph.FlowCosts[edge.Target] + 0.0000001);

                    double cFirm = CoeffGuaranteedTransport;
                    double cInterruptible = CoeffInterruptibleTransport * (100 - Model.Graph.FirmDeliveryProbability[edge]) / 10;

                    double cBooked = 0;
                    if (Model.Graph.FirmDeliveryProbability[edge] > 99.99) // Firm - приоритет
                    {
                        cBooked = cFirm;
                    }
                    else // Прерываемые мощности
                    {
                        cBooked = cInterruptible;
                    }

                    //goal += (cBooked * VariablesPool.EdgeFlows[edge] + (cFirm - cBooked) * VariablesPool.AdditionalCapacityBookings[edge.Target]) * tariff;
                    goal += cBooked * (VariablesPool.EdgeFlows[edge] - VariablesPool.EdgeAdditionalBookingFlows[edge]) * tariff;
                    
                    goal += cFirm * VariablesPool.EdgeAdditionalBookingFlows[edge] * tariff;
                }
            }          

            return goal;
        }

        #endregion // Составление критерия

        #region Методы инициализации и вызова xpress солвера

        /// <summary>
        /// Вызываем солвер
        /// </summary>
        /// <param name="context"></param>
        /// <param name="directive"></param>
        /// <returns></returns>
        protected virtual bool CallSolver(SolverContext context, SolverCapability directiveType)
        {
            _errorHandler.ReportError(ErrorHandler.eErrClass.EC_ERROR, "Вызов решателя", "GEx.Calculate.GExContractsModelSolver");

            try
            {
                XpressMPDirective directive = this.GetDirective(directiveType);
                if (directiveType == SolverCapability.NLP || directiveType == SolverCapability.MINLP)
                {
                    bool success = MpModel.SolveCommon(directive, directiveType);
                    if (!success)
                    {
                        FinishCalculation("Решения нет! ");
                        return false;
                    }
                }
                else
                {
                    Solution solution = context.Solve(directive);
                    if (solution.Quality != SolverQuality.Optimal)
                    {
                        FinishCalculation("Решения нет! " + solution.Quality);
                        return false;
                    }


                    directive.CrossOver = AlwaysNeverAutomatic.Always;
                    solution = context.Solve(directive);

                    if (solution.Quality != SolverQuality.Optimal)
                    {
                        FinishCalculation("Ошибка при решении с crossover! " + solution.Quality);
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                FinishCalculation(ex.Message);
                _errorHandler.ReportError(ErrorHandler.eErrClass.EC_ERROR, ex.Message, "GEx.Calculate.GExContractsModelSolver");

                return false;
            }

            FinishCalculation();
            return true;
        }

        /// <summary>
        /// Метод завершения расчета. Отпускает Xpress
        /// </summary>
        /// <param name="errorString"></param>
        protected void FinishCalculation(string errorString = null)
        {
            XpressMPLicense.ReleaseXpress();
            ErrorString = errorString;
        }

        /// <summary>
        /// Возвращает директиву для расчета в зависимости от типа задачи МП
        /// </summary>
        /// <param name="capability"></param>
        /// <returns></returns>
        protected virtual XpressMPDirective GetDirective(SolverCapability capability)
        {
            XpressMPDirective directive;
            switch (capability)
            {
                case SolverCapability.QP:
                    directive = new XpressMPDirective
                    {
                        DefaultAlg = DefaultAlg.Barrier,
                        FeasTol = 1e-5,
                        OptimalityTol = 1e-12,
                        BarCrash = 0,
                        BarGapStop = 1e-15,
                        BarDualStop = 1e-12,
                        BarPrimalStop = 1e-12,
                        BarIterLimit = 100000,
                        BarStepStop = 1e-10,
                        CrossOver = AlwaysNeverAutomatic.Never,
                        PresolveType = Presolve.NoPrimalInfeasibility,
                        BarOrder = BarrierOrdering.MinDegree,
                        xpparams = { BarStart = 2, Trace = 1 }
                    };
                    break;
                case SolverCapability.LP:
                    directive = new XpressMPDirective
                    {
                        MIPTol = 1e-6,
                        MIPRelStop = 1e-5,
                        MIPRelCutoff = 1e-5,
                        FeasTol = 1e-8,
                        OptimalityTol = 1e-8,
                        BarGapStop = 1e-9,
                        BarIterLimit = 20000,
                        BarPrimalStop = 0,
                        BarDualStop = 0,
                        BarCrash = 0,                     
                    };
                    break;

                default:
                    directive = new XpressMPDirective();
                    break;
            }
            return directive;
        }

        #endregion //Методы инициализации и вызова xpress солвера

        #region Auxiliary

        protected string GetStringForMsf(long id, string val)
        {
            return String.Format("_{0}_{1}", id, val);
        }

        #endregion //Auxiliary

        #endregion
    }

    public static class Helpers
    {
        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (T item in source)
            {
                action(item);
            }
        }
    }
}
