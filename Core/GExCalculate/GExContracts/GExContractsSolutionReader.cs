﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Calculate
{
    /// <summary>
    /// Класс чтения решения оптимизационной маршрутно-контрактной задачи GasExport
    /// </summary>
    public class GExContractsSolutionReader : GExBaseSolutionReader
    {
        private new GExContractsModel Model
        {
            get { return base.Model as GExContractsModel; }
        }

        private new GExContractsVariablesPool VariablesPool
        {
            get { return base.VariablesPool as GExContractsVariablesPool; }
        }

        private new GExContractsDocResult DocResult
        {
            get { return base.DocResult as GExContractsDocResult; }
        }

        public GExContractsSolutionReader()
        {
        }

        public override GExBaseDocResult ReadSolution(GExBaseModel model, GExBaseVariablesPool variablesPool)
        {
            base.DocResult = new GExContractsDocResult();
            base.VariablesPool = variablesPool;
            base.Model = model;

            DocResult.Routes = new Dictionary<GExGraphVertex, List<GExGraphRoute>>();
            DocResult.TotalUsedCapacities = new Dictionary<GExGraphVertex, double>();

            // заполняем словарь номинаций по точкам   
            // ---------------------------------------------------------------------------------------------------------
            foreach (GExGraphVertex v in VariablesPool.VertexInFlows.Keys)
            {               
                if (!Model.SourceVertices.Contains(v))
                {
                    DocResult.TotalUsedCapacities.Add(v, VariablesPool.VertexInFlows[v].GetValue());
                }
            }

            foreach (GExGraphVertex v in VariablesPool.VertexOutFlows.Keys)
            {
                if (Model.SourceVertices.Contains(v))
                {
                    DocResult.TotalUsedCapacities.Add(v, VariablesPool.VertexOutFlows[v].GetValue());
                }
            }

            // заполняем словарь недопоставок  
            // ---------------------------------------------------------------------------------------------------------
            Dictionary<Domain.GasExportObject, double> deliveredVolumes = new Dictionary<Domain.GasExportObject, double>();

            foreach (GExGraphVertex v in VariablesPool.VertexInFlows.Keys)
            {
                if (Model.TargetVertices.Contains(v))
                {
                    if (!deliveredVolumes.ContainsKey(v.Point.GasExportObject))
                    {
                        deliveredVolumes.Add(v.Point.GasExportObject, VariablesPool.VertexInFlows[v].GetValue());
                    }
                    else
                    {
                        deliveredVolumes[v.Point.GasExportObject] += VariablesPool.VertexInFlows[v].GetValue();
                    }                
                }
            }   
         
            DocResult.UndeliveredVolumes = new Dictionary<Domain.GasExportObject,double>();
            foreach (Domain.GasExportObject g in Model.Graph.ContractVolumes.Keys)
            {
                double undVol = Model.Graph.ContractVolumes[g];
                if (deliveredVolumes.ContainsKey(g))
                {
                    undVol -= deliveredVolumes[g];
                }
                
                if (undVol > 0.0001)
                {
                    DocResult.UndeliveredVolumes.Add(g, undVol);
                }
            }

            // формируем список маршрутов         
            // ---------------------------------------------------------------------------------------------------------

            Dictionary<GExGraphEdge, double> unconsideredFlows = new Dictionary<GExGraphEdge, double>(); 
            foreach (var e in VariablesPool.EdgeFlows.Keys)
            {
                unconsideredFlows.Add(e, VariablesPool.EdgeFlows[e].GetValue());
            }

            // разбираем совокупность потоков на ниточные маршруты (на основе минимального потока) и проставляем соответствующие мощности по маршрутам            
            while (true)
            {
                // ищем ребро с минимальным потоком
                bool minNonZeroFlowFound = false;
                double minNonZeroFlow = double.MaxValue;
                GExGraphEdge edgeWithMinNonZeroFlow = null;
                foreach (var e in unconsideredFlows.Keys)
                {
                    if (unconsideredFlows[e] > 0 && unconsideredFlows[e] < minNonZeroFlow)
                    {
                        minNonZeroFlow = unconsideredFlows[e];
                        minNonZeroFlowFound = true;
                        edgeWithMinNonZeroFlow = e;
                    }                   
                }

                if (!minNonZeroFlowFound)
                {
                    // поток исчерпан, прекращаем поиск маршрутов
                    break;
                }
                else
                {
                    // вычитаем поток по выбранному ребру как уже рассмотренный
                    unconsideredFlows[edgeWithMinNonZeroFlow] -= minNonZeroFlow;

                    // признак оборванного маршрута
                    bool brokenRoute = false;

                    // проходим маршрут вперёд до какой-то целевой вершины
                    List<GExGraphEdge> nextEdges = new List<GExGraphEdge>();
                    GExGraphVertex routeTargetVertex = null;
                    GExGraphEdge edge = edgeWithMinNonZeroFlow;
                    while (true)
                    {
                        if (Model.TargetVertices.Contains(edge.Target))
                        {
                            routeTargetVertex = edge.Target;
                            break;
                        }                       

                        // из рёбер, которые исходят из вершины, куда приходит рассматриваемое ребро, выбираем то, у которого наименьший поток
                        GExGraphEdge nextEdgeWithMinFlow = null;
                        double minNextFlow = double.MaxValue;
                        foreach (GExGraphEdge nextEdge in Model.Graph.Connections[edge.Target].Item1)
                        {
                            if (unconsideredFlows[nextEdge] < minNextFlow && unconsideredFlows[nextEdge] > 0) 
                            {
                                minNextFlow = unconsideredFlows[nextEdge];
                                nextEdgeWithMinFlow = nextEdge;
                            }
                        }
                        if (nextEdgeWithMinFlow != null)
                        {
                            edge = nextEdgeWithMinFlow;
                        }
                        else
                        {
                            brokenRoute = true;
                            break;
                            // TODO (Alx) - это нештатная ситуация; какой-то из маршрутов оборвался
                        }

                        // вычитаем поток по выбранному ребру как уже рассмотренный
                        unconsideredFlows[edge] -= minNonZeroFlow;
                        nextEdges.Add(edge);
                    }

                    // проходим маршрут назад до какой-то из вершин-источников
                    List<GExGraphEdge> prevEdges = new List<GExGraphEdge>();                    
                    edge = edgeWithMinNonZeroFlow;                    
                    while (true)
                    {
                        if (Model.SourceVertices.Contains(edge.Source))
                        {                            
                            break;
                        }

                        // из рёбер, которые входят в вершину, откуда исходит рассматриваемое ребро, выбираем то, у которого наименьший поток
                        GExGraphEdge prevEdgeWithMinFlow = null;
                        double minPrevFlow = double.MaxValue;
                        foreach (GExGraphEdge prevEdge in Model.Graph.Connections[edge.Source].Item2)
                        {
                            if (unconsideredFlows[prevEdge] < minPrevFlow && unconsideredFlows[prevEdge] > 0)
                            {
                                minPrevFlow = unconsideredFlows[prevEdge];
                                prevEdgeWithMinFlow = prevEdge;
                            }
                        }
                        if (prevEdgeWithMinFlow != null)
                        {
                            edge = prevEdgeWithMinFlow;
                        }
                        else
                        {
                            brokenRoute = true;
                            break;
                            // TODO (Alx) - это нештатная ситуация; какой-то из маршрутов оборвался
                        }

                        // вычитаем поток по выбранному ребру как уже рассмотренный
                        unconsideredFlows[edge] -= minNonZeroFlow;
                        prevEdges.Add(edge);
                    }
                    
                    // собираем маршрут из части до и после рассмотренного ребра, и самого ребра
                    List<GExGraphVertex> path = new List<GExGraphVertex>();
                    for (int k = prevEdges.Count - 1; k >= 0; k--)
                    {
                        path.Add(prevEdges[k].Source);
                    }
                    path.Add(edgeWithMinNonZeroFlow.Source);
                    path.Add(edgeWithMinNonZeroFlow.Target);
                    for (int k = 0; k < nextEdges.Count; k++)
                    {
                        path.Add(nextEdges[k].Target);
                    }
                    
                    // проставляем мощности
                    GExGraphRoute route = new GExGraphRoute(path);
                    route.TotalCapacity = minNonZeroFlow;
                    foreach (GExGraphRoutePoint p in route.Path)
                    {
                        p.Capacity = minNonZeroFlow;
                    }

                    if (routeTargetVertex != null && !brokenRoute)
                    {
                        if (!DocResult.Routes.ContainsKey(routeTargetVertex))
                        {
                            DocResult.Routes.Add(routeTargetVertex, new List<GExGraphRoute>());
                        }
                        DocResult.Routes[routeTargetVertex].Add(route);
                    }
                }
            }

            // вычисляем фиксированную и стоимости для маршрутов и их звеньев; 
            // приоритет по использованию уже забронированных мощностей отдаётся маршрутам с большим объёмом поставок
            // ---------------------------------------------------------------------------------------------------------            
            
            // сортируем все маршруты по мощности по убыванию
            List<GExGraphRoute> allRoutes = new List<GExGraphRoute>();
            foreach (GExGraphVertex tv in DocResult.Routes.Keys)
            {
                foreach (GExGraphRoute route in DocResult.Routes[tv])
                {
                    allRoutes.Add(route);
                }
            }
            allRoutes.Sort((r1, r2) => -r1.TotalCapacity.CompareTo(r2.TotalCapacity));

            // вычисляем стоимости по точкам, сначала используя для более мощных маршрутов уже забронированные мощности
            Dictionary<GExGraphVertex, double> usedAlreadyBookedCapacity = new Dictionary<GExGraphVertex, double>();
            for (int j = 0; j < allRoutes.Count; j++)
            {
                for (int k = 0; k < allRoutes[j].Path.Count; k++)
                {
                    // рассматриваем точку маршрута
                    GExGraphRoutePoint p = allRoutes[j].Path[k];

                    if (!usedAlreadyBookedCapacity.ContainsKey(p.Vertex))
                    {
                        usedAlreadyBookedCapacity.Add(p.Vertex, 0);
                    }               

                    // фиксированная составляющая тарифа: берём, если исчерпаны уже забронированные мощности
                    double bookedCapToUse = Math.Min(allRoutes[j].TotalCapacity, Model.Graph.BookedCapacities[p.Vertex] - usedAlreadyBookedCapacity[p.Vertex]);
                    usedAlreadyBookedCapacity[p.Vertex] += bookedCapToUse;
                    p.AbsoluteFixedCost = (allRoutes[j].TotalCapacity - bookedCapToUse) * Model.Graph.BookingCosts[p.Vertex];

                    // переменная составляющая тарифа
                    p.AbsoluteVariableCost = allRoutes[j].TotalCapacity * Model.Graph.FlowCosts[p.Vertex];
                }
            }

            // считаем общие фиксированные и переменные стоимости по маршрутам
            foreach (GExGraphVertex tv in DocResult.Routes.Keys)
            {
                foreach (GExGraphRoute route in DocResult.Routes[tv])
                {
                    route.AbsoluteFixedCost = 0;
                    route.AbsoluteVariableCost = 0;
                    // без начальной точки
                    for (int k = 1; k < route.Path.Count; k++)
                    {
                        route.AbsoluteFixedCost += route.Path[k].AbsoluteFixedCost;
                        route.AbsoluteVariableCost += route.Path[k].AbsoluteVariableCost;
                    }
                }
            }           

            return DocResult;
        }
    }
}

