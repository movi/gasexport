﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GEx.Calculate
{
    /// <summary>
    /// Класс модели оптимизационной маршрутно-контрактной задачи GasExport
    /// </summary>
    public class GExContractsModel : GExBaseModel
    {
        /// <summary>
        /// Расчётный граф контрактной модели
        /// </summary>
        public GExContractsGraph Graph {get; set; }

        /// <summary>
        /// Вершины, откуда идут поставки
        /// </summary>
        public List<GExGraphVertex> SourceVertices { get; set; }

        /// <summary>
        /// Список вершин, куда ведутся поставки
        /// </summary>
        public List<GExGraphVertex> TargetVertices { get; set; }

        public GExContractsModel()
        {
            Graph = new GExContractsGraph();

            SourceVertices = new List<GExGraphVertex>();
            TargetVertices = new List<GExGraphVertex>();
        }
    }
}
