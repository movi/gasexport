﻿using System.Text;
using System.Threading.Tasks;

namespace GEx.Calculate
{
    public class GExContractsCalculation :
        GExBaseCalculation<GExContractsModelBuilder, GExContractsModelSolver, GExContractsSolutionReader>
    {
        private GEx.ErrorHandler.IErrorOutput _errorHandler = null;

        public GExContractsCalculation()
        {
            Settings = new GExBaseModelSettings();
            ModelBuilder.Settings = Settings;
            ModelSolver.Settings = Settings;
            this.SolutionReader.Settings = Settings;
        }

        public void Init(GEx.ErrorHandler.IErrorOutput eh)
        {
            _errorHandler = eh;
            this.ModelBuilder.Init(_errorHandler);
            this.ModelSolver.Init(_errorHandler);
        }
    }
}
