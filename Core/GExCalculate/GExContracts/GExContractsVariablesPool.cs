﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SolverFoundation.Services;
using NLPSolverPlugin;

namespace GEx.Calculate
{
    /// <summary>
    /// Класс хранилища переменных оптимизационной маршрутно-контрактной задачи GasExport
    /// </summary>
    public class GExContractsVariablesPool : GExBaseVariablesPool
    {
        #region Properties

        /// <summary>
        /// Суммарные потоки по дугам расчётного графа в её направлении 
        /// </summary>
        public Dictionary<GExGraphEdge, Variable> EdgeFlows { get; protected set; }

        /// <summary>
        /// Потоки по дугам расчётного графа, отвечающие за дополнительно забронированные объемы   
        /// </summary>
        public Dictionary<GExGraphEdge, Variable> EdgeAdditionalBookingFlows { get; protected set; }

        /// <summary>
        /// Входящие в узлы потоки  
        /// </summary>
        public Dictionary<GExGraphVertex, Variable> VertexInFlows { get; protected set; }

        /// <summary>
        /// Исходящие из узлов потоки  
        /// </summary>
        public Dictionary<GExGraphVertex, Variable> VertexOutFlows { get; protected set; }

        /// <summary>
        /// Положительные части дисбалансов на узлах  
        /// </summary>
        public Dictionary<GExGraphVertex, Variable> VertexDisbalancesPlus { get; protected set; }

        /// <summary>
        /// Отрицательные части дисбалансов на узлах  
        /// </summary>
        public Dictionary<GExGraphVertex, Variable> VertexDisbalancesMinus { get; protected set; }

        /// <summary>
        /// Дополнительный объём бронирования мощностей в узле 
        /// </summary>
        public Dictionary<GExGraphVertex, Variable> AdditionalCapacityBookings  { get; protected set; }    

        #endregion

        #region Constructor

        public GExContractsVariablesPool()
        {
            EdgeFlows = new Dictionary<GExGraphEdge, Variable>();
            EdgeAdditionalBookingFlows = new Dictionary<GExGraphEdge, Variable>();
            VertexInFlows = new Dictionary<GExGraphVertex, Variable>();
            VertexOutFlows = new Dictionary<GExGraphVertex, Variable>();
            VertexDisbalancesPlus = new Dictionary<GExGraphVertex, Variable>();
            VertexDisbalancesMinus = new Dictionary<GExGraphVertex, Variable>();
            AdditionalCapacityBookings = new Dictionary<GExGraphVertex, Variable>();
        }

        #endregion Constructor

        #region Public methods

        /// <summary>
        /// Инициализация по заданному набору дуг и узлов
        /// </summary>
        /// <param name="edges"></param>
        /// <param name="vertices"></param>
        public virtual void Initialize(IEnumerable<GExGraphEdge> edges, IEnumerable<GExGraphVertex> vertices)
        {
            InitializeForEdges(edges);
            InitializeForVertices(vertices);
        }

        /// <summary>
        /// Возвращает все имеющиеся переменные
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<Variable> GetAllVariables()
        {
            return EdgeFlows.Values
                .Union(EdgeAdditionalBookingFlows.Values)
                .Union(VertexInFlows.Values)
                .Union(VertexOutFlows.Values)
                .Union(VertexDisbalancesPlus.Values)
                .Union(VertexDisbalancesMinus.Values)
                .Union(AdditionalCapacityBookings.Values);
        }        

        #endregion //Public methods

        #region Protected methods

        /// <summary>
        /// Инициализация по узлам
        /// </summary>
        /// <param name="nodes"></param>
        protected virtual void InitializeForVertices(IEnumerable<GExGraphVertex> vertices)
        {
            
        }

        /// <summary>
        /// Инициализация по дугам
        /// </summary>
        /// <param name="edges"></param>
        protected virtual void InitializeForEdges(IEnumerable<GExGraphEdge> edges)
        {
            
        }

        #endregion //Protected methods
    }
}

