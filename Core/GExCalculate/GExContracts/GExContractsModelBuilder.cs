﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using GEx.Domain;
using GEx.Domain.DataPumps;
using GEx.ErrorHandler;

namespace GEx.Calculate
{
    /// <summary>
    /// Класс конструктора модели маршрутно-контрактной задачи GasExport
    /// </summary>
    public class GExContractsModelBuilder : GExBaseModelBuilder
    {
        private GEx.ErrorHandler.IErrorOutput _errorHandler = null;

        private new GExContractsDataPump DataPump
        {
            get { return base.DataPump as GExContractsDataPump; }
        }

        private new GExContractsModel Model
        {
            get { return base.Model as GExContractsModel; }
        }

        public GExContractsModelBuilder()
        {
        }

        public void Init(GEx.ErrorHandler.IErrorOutput eh)
        {
            _errorHandler = eh;
        }

        /// <summary>
        /// Генерируем уникальный ID тройки - точки  расчётного графа
        /// </summary>
        /// <param name="obj">Точка</param>
        /// <param name="op">Оператор</param>
        /// <param name="dir">Направление</param>
        /// <returns></returns>
        protected int GenerateVertexID(GasExportObject obj, BalanceZone op, Direction dir)
        {
            return 1000000 * op.ID + 10 * obj.ID + (int)dir;
        }

        public override GExBaseModel BuildModel(IGExBaseDataPump _datapump)
        {
            try
            {
                return BuildModelInternal(_datapump);
            }
            catch (Exception e)
            {
                _errorHandler.ReportError(ErrorHandler.eErrClass.EC_ERROR,
                    e.ToString(),
                    "GEx.Calculate.GExContractsModelBuilder");
                throw e;
            }
        }

        public GExBaseModel BuildModelInternal(IGExBaseDataPump _dataPump)
        {
            base.Model = new GExContractsModel();

            GExContractsDataPump dataPump = _dataPump as GExContractsDataPump;

            // словарь для хранения сценарных данных по граничным точкам
            Dictionary<Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction>, BorderScenarioData> dicBorderScenarioData =
                new Dictionary<Tuple<GasExportObject, BalanceZone, Direction>, BorderScenarioData>();
            foreach (var borderData in dataPump.CalcScenario.BorderScenarioDataSet)
            {
                dicBorderScenarioData.Add(new Tuple<GasExportObject, BalanceZone, Direction>(borderData.GasExportObject, borderData.Operator, borderData.Direction), borderData);
            }
            // словарь для хранения сценарных данных по входам российского газа
            Dictionary<Domain.GasExportObject, RussianGasInputScenarioData> dicInputScenarioData =
                new Dictionary<Domain.GasExportObject, RussianGasInputScenarioData>();
            foreach (var inputData in dataPump.CalcScenario.RussianGasInputScenarioDataSet)
            {
                dicInputScenarioData.Add(inputData.Obj, inputData);
            }
            // словарь для хранения сценарных данных по ПХГ
            Dictionary<Domain.GasExportObject, UGSScenarioData> dicUGSScenarioData =
                new Dictionary<Domain.GasExportObject, UGSScenarioData>();
            foreach (var ugsData in dataPump.CalcScenario.UGSScenarioDataSet)
            {
                dicUGSScenarioData.Add(ugsData.Obj, ugsData);
            }
            // словарь для хранения данных по пунктам сдачи
            Dictionary<Domain.GasExportObject, OutputScenarioData> dicOutputScenarioData =
                new Dictionary<GasExportObject, OutputScenarioData>();
            foreach (var outputData in dataPump.CalcScenario.OutputScenarioDataSet)
            {
                if (!dicOutputScenarioData.ContainsKey(outputData.Obj))
                {
                    dicOutputScenarioData.Add(outputData.Obj as GasExportObject, outputData);
                }
            }
            
            // создаём расчётный граф
            Model.Graph = new GExContractsGraph();
            Dictionary<Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction>, GExGraphVertex> bordersVertexDict =
                new Dictionary<Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction>, GExGraphVertex>();
            GasExportObject geObj;
            foreach (var node in dataPump.Scheme.Nodes)
            {
                if (node.GasExportObject is Domain.Border)
                {
                    Domain.Border brd = node.GasExportObject as Domain.Border;
                    foreach (var conn in brd.Interconnections)
                    {
                        // формируем индексы
                        Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction> indExitFirst =
                            new Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction>(brd, conn.ZoneFrom, Domain.Direction.Exit);
                        Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction> indEntrySecond =
                            new Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction>(brd, conn.ZoneTo, Domain.Direction.Entry);

                        // создаём путевые точки                            
                        Domain.PathPoint exitPointFirst = new PathPoint(GenerateVertexID(brd, conn.ZoneFrom, Direction.Exit), brd, conn.ZoneFrom, Direction.Exit);
                        Domain.PathPoint entryPointSecond = new PathPoint(GenerateVertexID(brd, conn.ZoneTo, Direction.Entry), brd, conn.ZoneTo, Direction.Entry);

                        // создаём вершины расчётного графа
                        GExGraphVertex exitFirst = new GExGraphVertex(exitPointFirst);
                        GExGraphVertex entrySecond = new GExGraphVertex(entryPointSecond);

                        // добавляем их в словарь, если их там ещё нет
                        if (!bordersVertexDict.ContainsKey(indExitFirst))
                        {
                            bordersVertexDict.Add(indExitFirst, exitFirst);
                        }
                        if (!bordersVertexDict.ContainsKey(indEntrySecond))
                        {
                            bordersVertexDict.Add(indEntrySecond, entrySecond);
                        }

                        // если надо, добавляем в граф узлы
                        if (!Model.Graph.Connections.ContainsKey(exitFirst))
                        {
                            Model.Graph.Connections.Add(exitFirst, new Tuple<List<GExGraphEdge>, List<GExGraphEdge>>(new List<GExGraphEdge>(), new List<GExGraphEdge>()));
                        }
                        if (!Model.Graph.Connections.ContainsKey(entrySecond))
                        {
                            Model.Graph.Connections.Add(entrySecond, new Tuple<List<GExGraphEdge>, List<GExGraphEdge>>(new List<GExGraphEdge>(), new List<GExGraphEdge>()));
                        }

                        // Создаём дугу между узлами соединения и добавляем её в граф
                        GExGraphEdge edge1 = new GExGraphEdge(exitFirst, entrySecond);
                        if (!Model.Graph.Connections[exitFirst].Item1.Contains(edge1))
                        {
                            Model.Graph.Connections[exitFirst].Item1.Add(edge1);
                        }
                        if (!Model.Graph.Connections[entrySecond].Item2.Contains(edge1))
                        {
                            Model.Graph.Connections[entrySecond].Item2.Add(edge1);
                        }

                        // Проставляем значения мощностей и тарифов по точкам                        
                        if (dicBorderScenarioData.ContainsKey(indExitFirst) && dicBorderScenarioData[indExitFirst] != null)
                        {
                            Model.Graph.BookedCapacities[exitFirst] =
                                (dicBorderScenarioData[indExitFirst].BookedCapacity.HasValue ? dicBorderScenarioData[indExitFirst].BookedCapacity.Value : 0);
                            Model.Graph.AvailableCapacities[exitFirst] = 
                                (dicBorderScenarioData[indExitFirst].AvailableCapacity.HasValue ? dicBorderScenarioData[indExitFirst].AvailableCapacity.Value : 0);
                            Model.Graph.BookingCosts[exitFirst] =
                                dicBorderScenarioData[indExitFirst].Tariff.HasValue ? dicBorderScenarioData[indExitFirst].Tariff.Value : 0;
                            Model.Graph.FlowCosts[exitFirst] =
                                (dicBorderScenarioData[indExitFirst].GsnValue.HasValue ? dicBorderScenarioData[indExitFirst].GsnValue.Value : 0) *
                                (dicBorderScenarioData[indExitFirst].GsnPercentValue.HasValue ? dicBorderScenarioData[indExitFirst].GsnPercentValue.Value : 0) / 100.0;                       
                        }
                        else
                        {
                            Model.Graph.BookedCapacities[exitFirst] = 0;
                            Model.Graph.AvailableCapacities[exitFirst] = 0;
                            Model.Graph.BookingCosts[exitFirst] = 0;
                            Model.Graph.FlowCosts[exitFirst] = 0;
                        }

                        if (dicBorderScenarioData.ContainsKey(indEntrySecond) && dicBorderScenarioData[indEntrySecond] != null)
                        {
                            Model.Graph.BookedCapacities[entrySecond] =
                                (dicBorderScenarioData[indEntrySecond].BookedCapacity.HasValue ? dicBorderScenarioData[indEntrySecond].BookedCapacity.Value : 0);
                            Model.Graph.AvailableCapacities[entrySecond] =
                                (dicBorderScenarioData[indEntrySecond].AvailableCapacity.HasValue ? dicBorderScenarioData[indEntrySecond].AvailableCapacity.Value : 0);
                            Model.Graph.BookingCosts[entrySecond] =
                                dicBorderScenarioData[indEntrySecond].Tariff.HasValue ? dicBorderScenarioData[indEntrySecond].Tariff.Value : 0;
                            Model.Graph.FlowCosts[entrySecond] =
                                (dicBorderScenarioData[indEntrySecond].GsnValue.HasValue ? dicBorderScenarioData[indEntrySecond].GsnValue.Value : 0) *
                                (dicBorderScenarioData[indEntrySecond].GsnPercentValue.HasValue ? dicBorderScenarioData[indEntrySecond].GsnPercentValue.Value : 0) / 100.0;
                        }
                        else
                        {
                            Model.Graph.BookedCapacities[entrySecond] = 0;
                            Model.Graph.AvailableCapacities[entrySecond] = 0;
                            Model.Graph.BookingCosts[entrySecond] = 0;
                            Model.Graph.FlowCosts[entrySecond] = 0;
                        }

                        // Проставляем значения контрактных обязательств по пункту сдачи                        
                        geObj = brd as GasExportObject;
                        if (dicOutputScenarioData.ContainsKey(geObj) && dicOutputScenarioData[geObj] != null && dataPump.TargetPoints.Contains(geObj))
                        {
                            Model.Graph.ContractVolumes[geObj] = dicOutputScenarioData[geObj].ContractVolume.HasValue ? dicOutputScenarioData[geObj].ContractVolume.Value : 0;
                        }

                        // Устанавливаем вероятность непрерываемости мощности по ребру
                        Model.Graph.FirmDeliveryProbability[new GExGraphEdge(exitFirst, entrySecond)] = 100.0;
                    }

                }
            }

            //Блок кода для выдачи в дебаге матриц связности в удобной форме
            // -----------------------------------------------------------------------------------------------
            Dictionary<BalanceZone, List<string>> ptsByOps = new Dictionary<BalanceZone, List<string>>();
            foreach (NodesRelation relation in dataPump.Scheme.Relations)
            {
                if (!ptsByOps.ContainsKey(relation.BalanceZone))
                {
                    ptsByOps.Add(relation.BalanceZone, new List<string>());
                }

                if (!ptsByOps[relation.BalanceZone].Contains(relation.NodeFirst.GasExportObject.Name))
                {
                    ptsByOps[relation.BalanceZone].Add(relation.NodeFirst.GasExportObject.Name);
                }

                if (!ptsByOps[relation.BalanceZone].Contains(relation.NodeSecond.GasExportObject.Name))
                {
                    ptsByOps[relation.BalanceZone].Add(relation.NodeSecond.GasExportObject.Name);
                }
            }

            foreach (BalanceZone bz in ptsByOps.Keys)
            {
                ptsByOps[bz].Sort();
            }

            Dictionary<BalanceZone, Dictionary<string, Dictionary<string, double>>> rels = new Dictionary<BalanceZone, Dictionary<string, Dictionary<string, double>>>();
            foreach (BalanceZone bz in ptsByOps.Keys)
            {
                rels.Add(bz, new Dictionary<string, Dictionary<string, double>>());

                foreach (string p1 in ptsByOps[bz])
                {
                    rels[bz].Add(p1, new Dictionary<string, double>());

                    foreach (string p2 in ptsByOps[bz])
                    {
                        rels[bz][p1].Add(p2, 0);
                    }
                }
            }

            foreach (NodesRelation relation in dataPump.Scheme.Relations)
            {
                rels[relation.BalanceZone][relation.NodeFirst.GasExportObject.Name][relation.NodeSecond.GasExportObject.Name] = 100;
            }

            foreach (BalanceZone bz in rels.Keys.OrderBy(b => b.Name))
            {
                System.Diagnostics.Debug.WriteLine(bz.Name);
                System.Diagnostics.Debug.WriteLine("");

                System.Diagnostics.Debug.Write("\t");
                foreach (string p1 in rels[bz].Keys)
                {
                    System.Diagnostics.Debug.Write(p1 + "\t");
                }
                System.Diagnostics.Debug.WriteLine("");

                foreach (string p1 in rels[bz].Keys)
                {
                    System.Diagnostics.Debug.Write(p1 + "\t");
                    foreach (string p2 in rels[bz].Keys)
                    {
                        if (p1 == p2)
                        {
                            System.Diagnostics.Debug.Write(" " + "\t");
                        }
                        else 
                        {
                            System.Diagnostics.Debug.Write(Math.Round(rels[bz][p1][p2]).ToString() + "\t");
                        }
                    }
                    System.Diagnostics.Debug.WriteLine("");
                }

                System.Diagnostics.Debug.WriteLine("");
                System.Diagnostics.Debug.WriteLine("");
            }
            // -----------------------------------------------------------------------------------------------

            // добавляем дуги графа внутри оператора: связи между его "входами" и "выходами"
            foreach (NodesRelation relation in dataPump.Scheme.Relations)
            {
                if (relation.NodeFirst.GasExportObject is Domain.Border && relation.NodeSecond.GasExportObject is Domain.Border && relation.Value > 0)
                {
                    // формируем индексы
                    Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction> indOperatorEntry =
                        new Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction>(relation.NodeFirst.GasExportObject, relation.BalanceZone, Domain.Direction.Entry);
                    Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction> indOperatorExit =
                        new Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction>(relation.NodeSecond.GasExportObject, relation.BalanceZone, Domain.Direction.Exit);

                    // создаём путевые точки                            
                    Domain.PathPoint operatorEntry = new PathPoint(GenerateVertexID(relation.NodeFirst.GasExportObject, relation.BalanceZone, Domain.Direction.Entry), relation.NodeFirst.GasExportObject, relation.BalanceZone, Domain.Direction.Entry);
                    Domain.PathPoint operatorExit = new PathPoint(GenerateVertexID(relation.NodeSecond.GasExportObject, relation.BalanceZone, Domain.Direction.Exit), relation.NodeSecond.GasExportObject, relation.BalanceZone, Domain.Direction.Exit);

                    // создаём вершины расчётного графа
                    GExGraphVertex entry = new GExGraphVertex(operatorEntry);
                    GExGraphVertex exit = new GExGraphVertex(operatorExit);

                    // добавляем их в словарь, если их там ещё нет
                    if (!bordersVertexDict.ContainsKey(indOperatorEntry))
                    {
                        bordersVertexDict.Add(indOperatorEntry, entry);
                    }
                    if (!bordersVertexDict.ContainsKey(indOperatorExit))
                    {
                        bordersVertexDict.Add(indOperatorExit, exit);
                    }

                    // если надо, добавляем в граф узлы
                    if (!Model.Graph.Connections.ContainsKey(entry))
                    {
                        Model.Graph.Connections.Add(entry, new Tuple<List<GExGraphEdge>, List<GExGraphEdge>>(new List<GExGraphEdge>(), new List<GExGraphEdge>()));
                    }
                    if (!Model.Graph.Connections.ContainsKey(exit))
                    {
                        Model.Graph.Connections.Add(exit, new Tuple<List<GExGraphEdge>, List<GExGraphEdge>>(new List<GExGraphEdge>(), new List<GExGraphEdge>()));
                    }

                    // Создаём дугу между узлами соединения и добавляем её в граф
                    GExGraphEdge edgeInsideOperator = new GExGraphEdge(entry, exit);
                    if (!Model.Graph.Connections[entry].Item1.Contains(edgeInsideOperator))
                    {
                        Model.Graph.Connections[entry].Item1.Add(edgeInsideOperator);
                    }
                    if (!Model.Graph.Connections[exit].Item2.Contains(edgeInsideOperator))
                    {
                        Model.Graph.Connections[exit].Item2.Add(edgeInsideOperator);
                    }

                    // Проставляем значения мощностей и тарифов по точкам                        
                    if (dicBorderScenarioData.ContainsKey(indOperatorEntry) && dicBorderScenarioData[indOperatorEntry] != null)
                    {
                        Model.Graph.BookedCapacities[entry] =
                                (dicBorderScenarioData[indOperatorEntry].BookedCapacity.HasValue ? dicBorderScenarioData[indOperatorEntry].BookedCapacity.Value : 0);
                        Model.Graph.AvailableCapacities[entry] =
                            (dicBorderScenarioData[indOperatorEntry].AvailableCapacity.HasValue ? dicBorderScenarioData[indOperatorEntry].AvailableCapacity.Value : 0);
                        Model.Graph.BookingCosts[entry] =
                            dicBorderScenarioData[indOperatorEntry].Tariff.HasValue ? dicBorderScenarioData[indOperatorEntry].Tariff.Value : 0;
                        Model.Graph.FlowCosts[entry] =
                            (dicBorderScenarioData[indOperatorEntry].GsnValue.HasValue ? dicBorderScenarioData[indOperatorEntry].GsnValue.Value : 0) *
                            (dicBorderScenarioData[indOperatorEntry].GsnPercentValue.HasValue ? dicBorderScenarioData[indOperatorEntry].GsnPercentValue.Value : 0) / 100.0; 
                    }
                    else
                    {
                        Model.Graph.BookedCapacities[entry] = 0;
                        Model.Graph.AvailableCapacities[entry] = 0;
                        Model.Graph.BookingCosts[entry] = 0;
                        Model.Graph.FlowCosts[entry] = 0;
                    }

                    if (dicBorderScenarioData.ContainsKey(indOperatorExit) && dicBorderScenarioData[indOperatorExit] != null)
                    {
                        Model.Graph.BookedCapacities[exit] =
                               (dicBorderScenarioData[indOperatorExit].BookedCapacity.HasValue ? dicBorderScenarioData[indOperatorExit].BookedCapacity.Value : 0);
                        Model.Graph.AvailableCapacities[exit] =
                            (dicBorderScenarioData[indOperatorExit].AvailableCapacity.HasValue ? dicBorderScenarioData[indOperatorExit].AvailableCapacity.Value : 0);
                        Model.Graph.BookingCosts[exit] =
                            dicBorderScenarioData[indOperatorExit].Tariff.HasValue ? dicBorderScenarioData[indOperatorExit].Tariff.Value : 0;
                        Model.Graph.FlowCosts[exit] =
                            (dicBorderScenarioData[indOperatorExit].GsnValue.HasValue ? dicBorderScenarioData[indOperatorExit].GsnValue.Value : 0) *
                            (dicBorderScenarioData[indOperatorExit].GsnPercentValue.HasValue ? dicBorderScenarioData[indOperatorExit].GsnPercentValue.Value : 0) / 100.0;
                    }
                    else
                    {
                        Model.Graph.BookedCapacities[exit] = 0;
                        Model.Graph.AvailableCapacities[exit] = 0;
                        Model.Graph.BookingCosts[exit] = 0;
                        Model.Graph.FlowCosts[exit] = 0;
                    }

                    // Проставляем значения контрактных обязательств по пункту сдачи                        
                    geObj = relation.NodeFirst.GasExportObject;
                    if (dicOutputScenarioData.ContainsKey(geObj) && dicOutputScenarioData[geObj] != null && !Model.Graph.ContractVolumes.ContainsKey(geObj) && dataPump.TargetPoints.Contains(geObj))
                    {
                        Model.Graph.ContractVolumes[geObj] = dicOutputScenarioData[geObj].ContractVolume.HasValue ? dicOutputScenarioData[geObj].ContractVolume.Value : 0;
                    }
                    geObj = relation.NodeSecond.GasExportObject;
                    if (dicOutputScenarioData.ContainsKey(geObj) && dicOutputScenarioData[geObj] != null && !Model.Graph.ContractVolumes.ContainsKey(geObj) && dataPump.TargetPoints.Contains(geObj))
                    {
                        Model.Graph.ContractVolumes[geObj] = dicOutputScenarioData[geObj].ContractVolume.HasValue ? dicOutputScenarioData[geObj].ContractVolume.Value : 0;
                    }

                    // Устанавливаем вероятность непрерываемости мощности по ребру
                    Model.Graph.FirmDeliveryProbability[new GExGraphEdge(entry, exit)] = relation.Value;
                }
            }

            // добавляем дуги "внутри виртуальных операторов"
            foreach (var node in dataPump.Scheme.Nodes)
            {
                if (node.GasExportObject is Domain.VTP)
                {
                    Domain.VTP vtp = node.GasExportObject as Domain.VTP;

                    Dictionary<BalanceZone, int> bordCnt = new Dictionary<BalanceZone, int>();
                    foreach (var ic in vtp.Interconnections)
                    {
                        if (!bordCnt.ContainsKey(ic.ZoneTo))
                        {
                            bordCnt.Add(ic.ZoneTo, 1);
                        }
                        else
                        {
                            bordCnt[ic.ZoneTo]++;
                        }
                    }
                    BalanceZone vtpZone = bordCnt.Keys.First();
                    int maxCnt = bordCnt[vtpZone];
                    foreach (BalanceZone bz in bordCnt.Keys)
                    {
                        if (bordCnt[bz] > maxCnt)
                        {
                            vtpZone = bz;
                            maxCnt = bordCnt[bz];
                        }
                    }

                    // формируем индексы
                    Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction> indOperatorEntry =
                        new Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction>(node.GasExportObject, vtpZone, Domain.Direction.Entry);
                    Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction> indOperatorExit =
                        new Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction>(node.GasExportObject, vtpZone, Domain.Direction.Exit);


                    // создаём путевые точки                            
                    Domain.PathPoint operatorEntry = new PathPoint(GenerateVertexID(node.GasExportObject, vtpZone, Domain.Direction.Entry), node.GasExportObject, vtpZone, Domain.Direction.Entry);
                    Domain.PathPoint operatorExit = new PathPoint(GenerateVertexID(node.GasExportObject, vtpZone, Domain.Direction.Exit), node.GasExportObject, vtpZone, Domain.Direction.Exit);

                    // создаём вершины расчётного графа
                    GExGraphVertex entry = new GExGraphVertex(operatorEntry);
                    GExGraphVertex exit = new GExGraphVertex(operatorExit);

                    // добавляем их в словарь, если их там ещё нет
                    if (!bordersVertexDict.ContainsKey(indOperatorEntry))
                    {
                        bordersVertexDict.Add(indOperatorEntry, entry);
                    }
                    if (!bordersVertexDict.ContainsKey(indOperatorExit))
                    {
                        bordersVertexDict.Add(indOperatorExit, exit);
                    }

                    // если надо, добавляем в граф узлы
                    if (!Model.Graph.Connections.ContainsKey(entry))
                    {
                        Model.Graph.Connections.Add(entry, new Tuple<List<GExGraphEdge>, List<GExGraphEdge>>(new List<GExGraphEdge>(), new List<GExGraphEdge>()));
                    }
                    if (!Model.Graph.Connections.ContainsKey(exit))
                    {
                        Model.Graph.Connections.Add(exit, new Tuple<List<GExGraphEdge>, List<GExGraphEdge>>(new List<GExGraphEdge>(), new List<GExGraphEdge>()));
                    }

                    // Создаём дугу между узлами соединения и добавляем её в граф
                    GExGraphEdge edgeInsideOperator = new GExGraphEdge(entry, exit);
                    if (!Model.Graph.Connections[entry].Item1.Contains(edgeInsideOperator))
                    {
                        Model.Graph.Connections[entry].Item1.Add(edgeInsideOperator);
                    }
                    if (!Model.Graph.Connections[exit].Item2.Contains(edgeInsideOperator))
                    {
                        Model.Graph.Connections[exit].Item2.Add(edgeInsideOperator);
                    }

                    // Проставляем значения мощностей и тарифов по точкам                        
                    if (dicBorderScenarioData.ContainsKey(indOperatorEntry) && dicBorderScenarioData[indOperatorEntry] != null)
                    {
                        Model.Graph.BookedCapacities[entry] =
                                (dicBorderScenarioData[indOperatorEntry].BookedCapacity.HasValue ? dicBorderScenarioData[indOperatorEntry].BookedCapacity.Value : 0);
                        Model.Graph.AvailableCapacities[entry] =
                            (dicBorderScenarioData[indOperatorEntry].AvailableCapacity.HasValue ? dicBorderScenarioData[indOperatorEntry].AvailableCapacity.Value : 0);
                        Model.Graph.BookingCosts[entry] =
                            dicBorderScenarioData[indOperatorEntry].Tariff.HasValue ? dicBorderScenarioData[indOperatorEntry].Tariff.Value : 0;
                        Model.Graph.FlowCosts[entry] =
                            (dicBorderScenarioData[indOperatorEntry].GsnValue.HasValue ? dicBorderScenarioData[indOperatorEntry].GsnValue.Value : 0) *
                            (dicBorderScenarioData[indOperatorEntry].GsnPercentValue.HasValue ? dicBorderScenarioData[indOperatorEntry].GsnPercentValue.Value : 0) / 100.0;
                    }
                    else
                    {
                        Model.Graph.BookedCapacities[entry] = 0;
                        Model.Graph.AvailableCapacities[entry] = 0;
                        Model.Graph.BookingCosts[entry] = 0;
                        Model.Graph.FlowCosts[entry] = 0;
                    }

                    if (dicBorderScenarioData.ContainsKey(indOperatorExit) && dicBorderScenarioData[indOperatorExit] != null)
                    {
                        Model.Graph.BookedCapacities[exit] =
                                (dicBorderScenarioData[indOperatorExit].BookedCapacity.HasValue ? dicBorderScenarioData[indOperatorExit].BookedCapacity.Value : 0);
                        Model.Graph.AvailableCapacities[exit] =
                            (dicBorderScenarioData[indOperatorExit].AvailableCapacity.HasValue ? dicBorderScenarioData[indOperatorExit].AvailableCapacity.Value : 0);
                        Model.Graph.BookingCosts[exit] =
                            dicBorderScenarioData[indOperatorExit].Tariff.HasValue ? dicBorderScenarioData[indOperatorExit].Tariff.Value : 0;
                        Model.Graph.FlowCosts[exit] =
                            (dicBorderScenarioData[indOperatorExit].GsnValue.HasValue ? dicBorderScenarioData[indOperatorExit].GsnValue.Value : 0) *
                            (dicBorderScenarioData[indOperatorExit].GsnPercentValue.HasValue ? dicBorderScenarioData[indOperatorExit].GsnPercentValue.Value : 0) / 100.0;
                    }
                    else
                    {
                        Model.Graph.BookedCapacities[exit] = 0;
                        Model.Graph.AvailableCapacities[exit] = 0;
                        Model.Graph.BookingCosts[exit] = 0;
                        Model.Graph.FlowCosts[exit] = 0;
                    }
                    
                    // Проставляем значения контрактных обязательств по пункту сдачи                        
                    geObj = node.GasExportObject;
                    if (dicOutputScenarioData.ContainsKey(geObj) && dicOutputScenarioData[geObj] != null && !Model.Graph.ContractVolumes.ContainsKey(geObj) && dataPump.TargetPoints.Contains(geObj))
                    {
                        Model.Graph.ContractVolumes[geObj] = dicOutputScenarioData[geObj].ContractVolume.HasValue ? dicOutputScenarioData[geObj].ContractVolume.Value : 0;
                    }                   

                    // Устанавливаем вероятность непрерываемости мощности по ребру
                    Model.Graph.FirmDeliveryProbability[new GExGraphEdge(entry, exit)] = 100.0;
                }
            }

            // добавляем в граф источники 
            if (dataPump.SourcePoints == null || dataPump.SourcePoints.Count == 0)
            {
                throw new Exception("Не задана начальная точка");
            }
            else
            {
                Model.SourceVertices = new List<GExGraphVertex>();
                foreach (var src in dataPump.SourcePoints)
                {
                    PathPoint sp = null;
                    double capSource = 0;
                    int idSource = -1;
                    Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction> indSource = null;

                    if (src is RussianGasInput)
                    {
                        RussianGasInput s = src as RussianGasInput;
                        sp = new PathPoint(GenerateVertexID(s, s.Zone, Direction.Entry), s, s.Zone, Direction.Entry);
                        capSource = (dicInputScenarioData.ContainsKey(s) && dicInputScenarioData[s].MaxExportVolume != null) ? dicInputScenarioData[s].MaxExportVolume.Value : 0;
                        idSource = s.ID;
                        indSource = new Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction>(s, s.Zone, Direction.Entry);
                    }
                    else if (src is UGS)
                    {
                        UGS s = src as UGS;
                        sp = new PathPoint(GenerateVertexID(s, s.Zone, Direction.Entry), s, s.Zone, Direction.Entry);
                        capSource = (dicUGSScenarioData.ContainsKey(s) && dicUGSScenarioData[s].MaxUGSWithdrawal != null) ? dicUGSScenarioData[s].MaxUGSWithdrawal.Value : 0;
                        idSource = s.ID;
                        indSource = new Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction>(s, s.Zone, Direction.Entry);
                    }
                    GExGraphVertex sourcePoint = new GExGraphVertex(sp);
                    bordersVertexDict[indSource] = sourcePoint;
                    Model.Graph.Connections.Add(sourcePoint, new Tuple<List<GExGraphEdge>, List<GExGraphEdge>>(new List<GExGraphEdge>(), new List<GExGraphEdge>()));

                    // прописываем для источника заданный макс объём экспорта или отбора из ПХГ
                    Model.Graph.BookedCapacities[sourcePoint] = capSource;
                    Model.Graph.AvailableCapacities[sourcePoint] = 0;
                    Model.Graph.BookingCosts[sourcePoint] = 0;
                    Model.Graph.FlowCosts[sourcePoint] = 0;

                    Model.Graph.AvailableVolumes[sourcePoint] = capSource;

                    // добавляем дуги от источника
                    foreach (NodesRelation relation in dataPump.Scheme.Relations)
                    {
                        if (relation.NodeFirst.GasExportObject.ID == idSource && relation.NodeSecond.GasExportObject is Domain.Border)
                        {
                            // формируем индексы
                            Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction> indOperatorEntry =
                                new Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction>(relation.NodeFirst.GasExportObject, relation.BalanceZone, Domain.Direction.Entry);
                            Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction> indOperatorExit =
                                new Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction>(relation.NodeSecond.GasExportObject, relation.BalanceZone, Domain.Direction.Exit);

                            GExGraphVertex vert1 = null, vert2 = null;

                            bool connectVertices = true;

                            if (bordersVertexDict.ContainsKey(indOperatorEntry))
                            {
                                vert1 = bordersVertexDict[indOperatorEntry];
                            }
                            else
                            {
                                _errorHandler.ReportError(ErrorHandler.eErrClass.EC_ERROR,
                                    "В словаре вершин графа отсутствует " + indOperatorEntry.ToString(),
                                    "GEx.Calculate.GExRoutesModelBuilder");
                                connectVertices = false;
                            }

                            if (bordersVertexDict.ContainsKey(indOperatorExit))
                            {
                                vert2 = bordersVertexDict[indOperatorExit];
                            }
                            else
                            {
                                _errorHandler.ReportError(ErrorHandler.eErrClass.EC_ERROR,
                                    "В словаре вершин графа отсутствует " + indOperatorExit.ToString(),
                                    "GEx.Calculate.GExRoutesModelBuilder");
                                connectVertices = false;
                            }

                            if (connectVertices)
                            {
                                GExGraphEdge edgeInsideOperator = new GExGraphEdge(vert1, vert2);

                                Model.Graph.Connections[vert1].Item1.Add(edgeInsideOperator);
                                Model.Graph.Connections[vert2].Item2.Add(edgeInsideOperator);                                

                                Model.Graph.FirmDeliveryProbability[edgeInsideOperator] = relation.Value;
                            }
                            else
                            {
                                _errorHandler.ReportError(ErrorHandler.eErrClass.EC_ERROR,
                                    "Невозможно связывание узлов внутри оператора " + relation.BalanceZone.Name + ". Некорректное задание одного из узлов: " + relation.NodeFirst.GasExportObject.Name + " ; " + relation.NodeSecond.GasExportObject.Name,
                                    "GEx.Calculate.GExRoutesModelBuilder");
                            }
                        }
                    }

                    // Прописываем стартовую точку маршрутов по графу
                    Model.SourceVertices.Add(sourcePoint);
                }
            }

            // добавляем в граф целевые точки
            if (dataPump.TargetPoints == null || dataPump.TargetPoints.Count == 0)
            {
                throw new Exception("Не задана целевая точка");
            }
            else
            {
                Model.TargetVertices = new List<GExGraphVertex>();
                foreach (var target in dataPump.TargetPoints)
                {
                    if (!Model.Graph.ContractVolumes.ContainsKey(target))
                    {
                        //throw new Exception("По целевой точке " + target.Name + " не заданы контрактные обязательства");
                        Model.Graph.ContractVolumes.Add(target, 0);
                    }
                    
                    // Прописываем целевые точки маршрутов по графу (выходы из разных операторов на интересующую границу или входы в ПХГ)
                    int idTarget = -1;
                    Direction dirTarget = Direction.Exit;
                    if (target is Border)
                    {
                        Border t = target as Border;
                        idTarget = t.ID;
                        dirTarget = Direction.Exit;
                    }
                    else if (target is UGS)
                    {
                        UGS t = target as UGS;
                        idTarget = t.ID;
                        dirTarget = Direction.Entry;

                        // добавляем в граф целевое ПХГ 
                        PathPoint tp = new PathPoint(GenerateVertexID(t, t.Zone, Direction.Entry), t, t.Zone, Direction.Entry);
                        double capTarget = double.MaxValue;
                        idTarget = t.ID;
                        Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction> indTarget = new Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction>((GasExportObject)t, t.Zone, Direction.Exit);

                        GExGraphVertex targetPoint = new GExGraphVertex(tp);
                        bordersVertexDict[indTarget] = targetPoint;
                        Model.Graph.Connections.Add(targetPoint, new Tuple<List<GExGraphEdge>, List<GExGraphEdge>>(new List<GExGraphEdge>(), new List<GExGraphEdge>()));

                        // прописываем для целевого ПХГ заданный макс объём экспорта или отбора из ПХГ
                        Model.Graph.BookedCapacities[targetPoint] = capTarget;
                        Model.Graph.AvailableCapacities[targetPoint] = 0;
                        Model.Graph.BookingCosts[targetPoint] = 0;
                        Model.Graph.FlowCosts[targetPoint] = 0;                        

                        // добавляем дуги от целевого ПХГ (до границ, не до других ПХГ!)
                        foreach (NodesRelation relation in dataPump.Scheme.Relations)
                        {
                            if (relation.NodeSecond.GasExportObject.ID == idTarget && relation.NodeFirst.GasExportObject.Type != GasExportObjectType.UGS)
                            {
                                // формируем индексы
                                Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction> indOperatorEntry =
                                    new Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction>(relation.NodeFirst.GasExportObject, relation.BalanceZone, Domain.Direction.Entry);
                                Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction> indOperatorExit =
                                    new Tuple<Domain.GasExportObject, Domain.BalanceZone, Domain.Direction>(relation.NodeSecond.GasExportObject, relation.BalanceZone, Domain.Direction.Exit);

                                GExGraphVertex vert1 = null, vert2 = null;

                                bool connectVertices = true;

                                if (bordersVertexDict.ContainsKey(indOperatorEntry))
                                {
                                    vert1 = bordersVertexDict[indOperatorEntry];
                                }
                                else
                                {
                                    _errorHandler.ReportError(ErrorHandler.eErrClass.EC_ERROR,
                                        "В словаре вершин графа отсутствует " + indOperatorEntry.ToString(),
                                        "GEx.Calculate.GExRoutesModelBuilder");
                                    connectVertices = false;
                                }

                                if (bordersVertexDict.ContainsKey(indOperatorExit))
                                {
                                    vert2 = bordersVertexDict[indOperatorExit];
                                }
                                else
                                {
                                    _errorHandler.ReportError(ErrorHandler.eErrClass.EC_ERROR,
                                        "В словаре вершин графа отсутствует " + indOperatorExit.ToString(),
                                        "GEx.Calculate.GExRoutesModelBuilder");
                                    connectVertices = false;
                                }

                                if (connectVertices)
                                {
                                    GExGraphEdge edgeInsideOperator = new GExGraphEdge(vert1, vert2);

                                    Model.Graph.Connections[vert1].Item1.Add(edgeInsideOperator);
                                    Model.Graph.Connections[vert2].Item2.Add(edgeInsideOperator);

                                    Model.Graph.FirmDeliveryProbability[edgeInsideOperator] = relation.Value;
                                }
                                else
                                {
                                    _errorHandler.ReportError(ErrorHandler.eErrClass.EC_ERROR,
                                        "Невозможно связывание узлов внутри оператора " + relation.BalanceZone.Name + ". Некорректное задание одного из узлов: " + relation.NodeFirst.GasExportObject.Name + " ; " + relation.NodeSecond.GasExportObject.Name,
                                        "GEx.Calculate.GExRoutesModelBuilder");
                                }
                            }
                        }
                    }

                    foreach (GExGraphVertex v in Model.Graph.Connections.Keys)
                    {
                        if (v.Point.GasExportObject.ID == idTarget && v.Point.Direction == dirTarget)
                        {
                            Model.TargetVertices.Add(v);
                        }
                    }
                }
            }              

            return Model;
        }
    }
}