﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Calculate
{
    /// <summary>
    /// Документ результата оптимизационной маршрутно-контрактной задачи GasExport
    /// </summary>
    public class GExContractsDocResult : GExBaseDocResult
    {
        /// <summary>
        /// Словарь совокупностей маршрутов поставок по точкам сдачи
        /// </summary>
        public Dictionary<GExGraphVertex, List<GExGraphRoute>> Routes { get; set; }

        /// <summary>
        /// Итоговые суммарные номинации по точкам схемы
        /// </summary>
        public Dictionary<GExGraphVertex, double> TotalUsedCapacities { get; set; }

        /// <summary>
        /// Объёмы невыполненных контрактных обязательств по пунктам сдачи
        /// </summary>
        public Dictionary<Domain.GasExportObject, double> UndeliveredVolumes { get; set; }

        public GExContractsDocResult()
        {
            Routes = new Dictionary<GExGraphVertex, List<GExGraphRoute>>();

            TotalUsedCapacities = new Dictionary<GExGraphVertex, double>();

            UndeliveredVolumes = new Dictionary<Domain.GasExportObject, double>();
        }
    }
}