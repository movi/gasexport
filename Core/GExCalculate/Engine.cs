﻿// /////////////////////////////////////////////////////////////
// File: 
// Date:					Author: Ildar Batyrshin
// Language: C#				Framework: .NET 4.0
// /////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Xml;
using GEx.ErrorHandler;
using GEx.Domain;
using GEx.Domain.DataPumps;
using GEx.Iface;

namespace GEx.Calculate
{ 
    public sealed class Engine : GEx.Iface.IGExCalculate
    {
        public static readonly Engine Instance = new Engine();
        private GEx.ErrorHandler.IErrorOutput _errorHandler = null;

        private Engine()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        //public void Run(GEx.ErrorHandler.IErrorOutput eh, string fileIn, string fileOut, string fileInitVals)
        //{
        //    _errorHandler = eh;
        //    //_pathFileXmlIn = fileIn;
        //    //_pathFileXmlOut = fileOut;
        //    //_pathFileXmlInitVals = fileInitVals;
        //    EnginePrologue();
        //    EngineBody();
        //    EngineEpilogue();
        //}

        private void EnginePrologue()
        {
            const string errorSource = "EnginePrologue()";
            string path = AppDomain.CurrentDomain.BaseDirectory;
            _errorHandler.ReportError(ErrorHandler.eErrClass.EC_INFO,
                "Base directory : " + path,errorSource);

            //_errorHandler.ReportError(ErrorHandler.eErrClass.EC_INFO,
            //    "Configuration loaded : " + _configFileName,
            //    "");
            try
            {
                //IMB: Инит расчетного блока
                //ModelsFactory.GetFactory().CurrentModel = new SimpleModel();
                //_doc = new HydroCalcDocument();

            //    RemotingConfiguration.Configure(_configFileName);

            //    if (RequestProcessor.Instance.Init(_errorHandler))
            //    {
            //        _errorHandler.ReportError(ErrorHandler.eErrClass.EC_INFO,
            //            "Init complete.",
            //            errorSource);
            //        string hostName = "<DNS Error. Failed to resolve the local computer name.>";
            //        try
            //        {
            //            hostName = Dns.GetHostName();
            //        }
            //        catch
            //        {
            //        }
            //        finally
            //        {
            //            _errorHandler.ReportError(ErrorHandler.eErrClass.EC_INFO,
            //                "This computer name : { " + hostName + " }",
            //                "");
            //        }
            //        IChannel[] channels = ChannelServices.RegisteredChannels;
            //        for (int i = 0; i < channels.Length; i++)
            //        {
            //            IChannelReceiver ch = (IChannelReceiver)channels[i];
            //            ChannelDataStore data = (ChannelDataStore)ch.ChannelData;
            //            string str = "{ ";
            //            foreach (string s in data.ChannelUris)
            //                str += s + " ";
            //            str += "}";
            //            _errorHandler.ReportError(ErrorHandler.eErrClass.EC_INFO,
            //                "This Engine URL(s) : " + str,
            //                "");
            //        }
            //    }
            //    else
            //    {
            //        _errorHandler.ReportError(ErrorHandler.eErrClass.EC_ERROR,
            //            "Failed to init data Engine (TaskManager). Press Ctrl-C to exit.",
            //            errorSource);
            //    }
            }
            catch (Exception e)
            {
                _errorHandler.ReportError(ErrorHandler.eErrClass.EC_ERROR,
                    "Failed to init data Engine. " + e.Message + ". Press Ctrl-C to exit.",
                    errorSource);
            }
        }

        private void EngineEpilogue()
        {
            const string errorSource = "EngineEpilogue()";

            //if (!RequestProcessor.Instance.UnInit())
            //{
            //    _errorHandler.ReportError(ErrorHandler.eErrClass.EC_ERROR,
            //        "Failed to uninit TaskRecMgr",
            //        errorSource);
            //}

        }

        private void EngineBody()
        {
            const string errorSource = "EngineBody()";
            try
            {
//                //IMB: В Creature() вызываются методы загрузки, которые в свою очередь вызывают методы Loader'a, 
//                //которые нужно реализовать
//                var loader = new ILoaderImpl();
//                loader.Init(_errorHandler, _pathFileXmlIn, _pathFileXmlInitVals);
//                LoadersFactory.GetFactory().CurrentLoader = loader;

//                var saver = new ISaverImp();
//                saver.Init(_errorHandler, _pathFileXmlOut);
//                SaverFactory.GetFactory().CurrentSaver = saver;

//                //ModelsFactory.GetFactory().CurrentModel = new MaxFlowModelImpl();
//               // ModelsFactory.GetFactory().CurrentModel = new SimpleModelImpl();
//               ModelsFactory.GetFactory().CurrentModel = new StraightforwardModelImpl();
//               // ModelsFactory.GetFactory().CurrentModel = new TemperatureModel();
//                ConverterFactory.GetFactory().CurrentConverter = new FisValConverter();
////                ConverterFactory.GetFactory().CurrentConverter = new FisValConverter_ONTP();

//                _doc.Creature();
//                _doc.CompileAndCalculate();
//                _doc.GetResults();

                //PLACE PROCESSING HERE!!!!!

                //while (true)
                //{
                //    System.Threading.Thread.Sleep(100);
                //}
                //_errorHandler.ReportError(eErrClass.EC_INFO, "Расчет завершен. " + _pathFileXmlIn + "->" + _pathFileXmlOut, errorSource);
            }
            catch (Exception e)
            {
                _errorHandler.ReportError(ErrorHandler.eErrClass.EC_ERROR,
                    e.ToString(),
                    errorSource);
            }
        }

        //public string ConfigFileName
        //{
        //    get
        //    {
        //        return _configFileName;
        //    }
        //    set
        //    {
        //        _configFileName = value;
        //    }
        //}

        #region IGExCalculate

        void IGExCalculate.Init(GEx.ErrorHandler.IErrorOutput eh)
        {
            _errorHandler = eh;
        }

        void IGExCalculate.Start(GExRoutesDataPump data, ModeWork mode)
        {
            GExRoutesCalculation routesCalc = new GExRoutesCalculation();
            routesCalc.Init(_errorHandler);
            bool res = routesCalc.Calculate(data);
            // Здесь далее анализируем поле routesCalc.DocResult.Routes. В каждй точке списка внутри объект PathPoint
            _docResult = routesCalc.DocResult;
            _docResult.Mode = mode;
        }

        void IGExCalculate.Start(GExContractsDataPump data, ModeWork mode)
        {
            GExContractsCalculation contractsCalc = new GExContractsCalculation();
            contractsCalc.Init(_errorHandler);
            bool res = contractsCalc.Calculate(data);
            // Здесь далее анализируем поле routesCalc.DocResult.Routes. В каждй точке списка внутри объект PathPoint
            _docResult = contractsCalc.DocResult;
            _docResult.Mode = mode;
        }


        void IGExCalculate.Stop()
        {
        }

        #endregion

        private IGeneralDocResult _docResult;
        
        public IGeneralDocResult Result
        {
            get
            {
                return _docResult;
            }
        }
    }
}
