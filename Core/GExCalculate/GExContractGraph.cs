﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GEx.Domain;

namespace GEx.Calculate
{
    /// <summary>
    /// Класс расчётного графа для задач GasExport с постоянной и переменной частями тарифа и прерываемостью поставок 
    /// </summary>
    public class GExContractsGraph : GExGraph
    {
        /// <summary>
        /// Вероятность непрерываемости поставок по данному ребру графа
        /// </summary>
        public Dictionary<GExGraphEdge, double> FirmDeliveryProbability;

        /// <summary>
        /// Контрактные объёмы, которые необходимо суммарно поставить в точки графа, соответствующие заданному объекту
        /// </summary>
        public Dictionary<GasExportObject, double> ContractVolumes;

        /// <summary>
        /// Доступные для поставки от источников объёмы
        /// </summary>
        public Dictionary<GExGraphVertex, double> AvailableVolumes;        
        

        public GExContractsGraph() :
            base()
        {
            FirmDeliveryProbability = new Dictionary<GExGraphEdge, double>();

            ContractVolumes = new Dictionary<GasExportObject, double>();

            AvailableVolumes = new Dictionary<GExGraphVertex, double>();
        }
    }
}

