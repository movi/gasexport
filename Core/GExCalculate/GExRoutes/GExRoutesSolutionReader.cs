﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Calculate
{
    /// <summary>
    /// Класс чтения решения маршрутной задачи GasExport
    /// </summary>
    public class GExRoutesSolutionReader : GExBaseSolutionReader
    {
        private new GExRoutesModel Model
        {
            get { return base.Model as GExRoutesModel; }
        }        

        private new GExRoutesVariablesPool VariablesPool
        {
            get { return base.VariablesPool as GExRoutesVariablesPool; }
        }

        private new GExRoutesDocResult DocResult
        {
            get { return base.DocResult as GExRoutesDocResult; }
        }
        
        public GExRoutesSolutionReader()
        {
        }

        public override GExBaseDocResult ReadSolution(GExBaseModel model, GExBaseVariablesPool variablesPool)
        {
            base.DocResult = new GExRoutesDocResult();
            base.VariablesPool = variablesPool;
            DocResult.Routes = VariablesPool.Routes;
            return DocResult;
        }
    }
}
