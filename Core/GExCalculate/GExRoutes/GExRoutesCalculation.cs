﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Calculate
{
    public class GExRoutesCalculation :
        GExBaseCalculation<GExRoutesModelBuilder, GExRoutesModelSolver, GExRoutesSolutionReader>
    {
        private GEx.ErrorHandler.IErrorOutput _errorHandler = null;        
        
        public GExRoutesCalculation()
        {
            Settings = new GExBaseModelSettings();
            ModelBuilder.Settings = Settings;
            ModelSolver.Settings = Settings;
            this.SolutionReader.Settings = Settings;
        }

        public void Init(GEx.ErrorHandler.IErrorOutput eh)
        {
            _errorHandler = eh;
            this.ModelBuilder.Init(_errorHandler);
            this.ModelSolver.Init(_errorHandler);
        }
    }
}
