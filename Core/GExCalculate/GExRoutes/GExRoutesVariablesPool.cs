﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GEx.Calculate
{
    /// <summary>
    /// Класс хранилища переменных маршрутной задачи GasExport
    /// </summary>
    public class GExRoutesVariablesPool : GExBaseVariablesPool
    {
        public List<GExGraphRoute> Routes;

        public GExRoutesVariablesPool()
        {
            Routes = new List<GExGraphRoute>();
        }
    }
}
