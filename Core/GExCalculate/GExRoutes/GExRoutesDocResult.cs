﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Calculate
{
    /// <summary>
    /// Документ результата маршрутной задачи GasExport
    /// </summary>
    public class GExRoutesDocResult : GExBaseDocResult
    {
        public List<GExGraphRoute> Routes;
        
        public GExRoutesDocResult()
        {
            Routes = new List<GExGraphRoute>();
        }
    }
}
