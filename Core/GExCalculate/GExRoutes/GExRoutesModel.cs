﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GEx.Calculate
{
    /// <summary>
    /// Класс модели маршрутной задачи GasExport
    /// </summary>
    public class GExRoutesModel : GExBaseModel
    {
        /// <summary>
        /// Расчётный граф
        /// </summary>
        public GExGraph Graph;

        /// <summary>
        /// Вершина, откуда идут маршруты
        /// </summary>
        public GExGraphVertex startVertex;

        /// <summary>
        /// Список вершин, до которых должны доходить маршруты
        /// </summary>
        public List<GExGraphVertex> targetVertices;
        
        public GExRoutesModel()
        {
            Graph = new GExGraph();
        }
    }
}
