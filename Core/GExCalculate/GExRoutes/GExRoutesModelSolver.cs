﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Calculate
{
    /// <summary>
    /// Класс решателя модели маршрутной задачи GasExport
    /// </summary>
    public class GExRoutesModelSolver : GExBaseModelSolver
    {
        private GEx.ErrorHandler.IErrorOutput _errorHandler = null;
        
        private new GExRoutesModel Model
        {
            get { return base.Model as GExRoutesModel; }
        }

        private new GExRoutesVariablesPool VariablesPool
        {
            get { return base.VariablesPool as GExRoutesVariablesPool; }
        }
        
        public GExRoutesModelSolver()
        {
            Settings = new GExBaseModelSettings();
        }

        public void Init(GEx.ErrorHandler.IErrorOutput eh)
        {
            _errorHandler = eh;
        }

        public override bool Solve(GExBaseModel mdl)
        {
            try
            {
                return SolveInternal(mdl);
            }
            catch (Exception e)
            {
                _errorHandler.ReportError(ErrorHandler.eErrClass.EC_ERROR,
                    e.ToString(),
                    "GEx.Calculate.GExRoutesModelSolver");
                throw e;
            }
        }


        public bool SolveInternal(GExBaseModel mdl)
        {
            if (mdl is GExRoutesModel)
            {
                base.Model = mdl;
                base.VariablesPool = new GExRoutesVariablesPool();
                VariablesPool.Routes = new List<GExGraphRoute>();

                foreach (GExGraphVertex targetVertex in Model.targetVertices)
                {
                    // получаем все маршруты до данной требуемой точки выхода
                    List<GExGraphRoute> allPaths = FindAllPaths(Model.startVertex, targetVertex);

                    // проставляем мощности и стоимости
                    foreach (GExGraphRoute route in allPaths)
                    {                        
                        route.TotalCapacity = double.MaxValue;
                        for (int k = 0; k < route.Path.Count; k++)
                        {
                            route.Path[k].Capacity = 0;

                            if (Model.Graph.BookedCapacities.ContainsKey(route.Path[k].Vertex))
                            {
                                route.Path[k].Capacity += Model.Graph.BookedCapacities[route.Path[k].Vertex];
                            }
                            else
                            {
                                _errorHandler.ReportError(ErrorHandler.eErrClass.EC_ERROR,
                                    "Отсутствуют данные об уже забронированной мощности по точке " + route.Path[k].Vertex.ToString(),
                                    "GEx.Calculate.GExRoutesModelSolver");                                
                            }

                            if (Model.Graph.AvailableCapacities.ContainsKey(route.Path[k].Vertex))
                            {
                                route.Path[k].Capacity += Model.Graph.AvailableCapacities[route.Path[k].Vertex];
                            }
                            else
                            {
                                _errorHandler.ReportError(ErrorHandler.eErrClass.EC_ERROR,
                                    "Отсутствуют данные о доступной для бронирования мощности по точке " + route.Path[k].Vertex.ToString(),
                                    "GEx.Calculate.GExRoutesModelSolver");                                
                            }
                            
                            if (route.Path[k].Capacity < route.TotalCapacity)
                            {
                                route.TotalCapacity = route.Path[k].Capacity;
                            }

                            if (route.TotalCapacity == 0)
                            {
                                break;
                            }
                        }
                                                
                        route.RelativeFixedCost = 0;
                        route.RelativeVariableCost = 0;
                        for (int k = 0; k < route.Path.Count; k++)
                        {
                            if (Model.Graph.BookingCosts.ContainsKey(route.Path[k].Vertex))
                            {
                                route.Path[k].RelativeVariableCost = Model.Graph.FlowCosts[route.Path[k].Vertex];
                            }
                            else
                            {
                                _errorHandler.ReportError(ErrorHandler.eErrClass.EC_ERROR,
                                    "Отсутствуют данные по переменному тарифу на точке " + route.Path[k].Vertex.ToString(),
                                    "GEx.Calculate.GExRoutesModelSolver");
                                route.Path[k].RelativeVariableCost = 0;
                            }
                            
                            if (Model.Graph.BookingCosts.ContainsKey(route.Path[k].Vertex))
                            {
                                if (route.TotalCapacity <= Model.Graph.BookedCapacities[route.Path[k].Vertex])
                                {
                                    route.Path[k].RelativeFixedCost = 0;
                                }
                                else
                                {
                                    route.Path[k].RelativeFixedCost = Model.Graph.BookingCosts[route.Path[k].Vertex] * (route.TotalCapacity - Model.Graph.BookedCapacities[route.Path[k].Vertex]) / route.TotalCapacity;
                                }                                
                            }
                            else
                            {
                                _errorHandler.ReportError(ErrorHandler.eErrClass.EC_ERROR,
                                    "Отсутствуют данные по фиксированному тарифу на точке " + route.Path[k].Vertex.ToString(),
                                    "GEx.Calculate.GExRoutesModelSolver");
                                route.Path[k].RelativeFixedCost = 0;
                            }

                            route.RelativeFixedCost += route.Path[k].RelativeFixedCost;
                            route.RelativeVariableCost += route.Path[k].RelativeVariableCost;
                        }

                        if (route.TotalCapacity > 0)
                        {
                            VariablesPool.Routes.Add(route);
                        }
                    }
                }
                return true;
            }
            else
            {
                return false;
            }            
        }

        protected List<GExGraphRoute> FindAllPaths(GExGraphVertex from, GExGraphVertex to)
        {
            Queue<Tuple<GExGraphVertex, List<GExGraphVertex>>> nodes = new Queue<Tuple<GExGraphVertex, List<GExGraphVertex>>>();
            nodes.Enqueue(new Tuple<GExGraphVertex, List<GExGraphVertex>>(from, new List<GExGraphVertex>()));
            List<GExGraphRoute> paths = new List<GExGraphRoute>();

            while (nodes.Any())
            {
                var current = nodes.Dequeue();
                GExGraphVertex currentNode = current.Item1;                

                bool omitPath = false;
                
                // запрещаем циклические пути, которые проходят по крайней мере дважды через одного оператора в одной точке (кроме ВТП)
                bool isPathCyclic = false;               
                int cnt = 0;
                foreach (GExGraphVertex v in current.Item2)
                {
                    if (v.Point.Operator.ID == currentNode.Point.Operator.ID && v.Point.GasExportObject.ID == currentNode.Point.GasExportObject.ID)
                    {
                        cnt++;
                    }
                }
                if (cnt > (currentNode.Point.GasExportObject.Type == Domain.GasExportObjectType.VTP ? 1 : 0))
                {
                    isPathCyclic = true;
                }
                if (isPathCyclic)
                {
                    omitPath = true;
                }              

                // не рассматриваем пути, заходящие как на промежуточную на целевую точку (на отличном от целевого операторе)
                if (currentNode.Point.GasExportObject.ID == to.Point.GasExportObject.ID && currentNode.Point.Operator.ID != to.Point.Operator.ID)
                {
                    omitPath = true;
                } 
                // не рассматриваем пути с длиной большей, чем заданное разумное значение                
                if (current.Item2.Count > 16)
                {
                    omitPath = true;
                }
                

                if (omitPath)
                {
                    continue;
                }

                current.Item2.Add(currentNode);

                if (currentNode.Equals(to))
                {
                    paths.Add(new GExGraphRoute(current.Item2));
                    continue;
                }

                foreach (var edge in Model.Graph.Connections[current.Item1].Item1)
                {
                    nodes.Enqueue(new Tuple<GExGraphVertex, List<GExGraphVertex>>(edge.Target, new List<GExGraphVertex>(current.Item2)));
                }
            }

            return paths;
        } 
    }
}
