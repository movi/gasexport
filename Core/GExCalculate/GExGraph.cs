﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Calculate
{
    /// <summary>
    /// Класс расчётного графа для задач GasExport
    /// </summary>
    public class GExGraph
    {
        /// <summary>
        /// Связи (словарь по узлам; первая компонента значения - выходящие из узла дуги; вторая - входящие)
        /// </summary>
        public Dictionary<GExGraphVertex, Tuple<List<GExGraphEdge>, List<GExGraphEdge>>> Connections;

        // Все узлы графа
        public IEnumerable<GExGraphVertex> Vertices
        {
            get
            {
                return getVertices();
            }
        }

        // Все дуги графа
        public IEnumerable<GExGraphEdge> Edges
        {
            get
            {
                return getEdges();
            }
        }       

        /// <summary>
        /// Уже забронированные мощности по точкам
        /// </summary>
        public Dictionary<GExGraphVertex, double> BookedCapacities;

        /// <summary>
        /// Доступные для бронирования мощности по точкам
        /// </summary>
        public Dictionary<GExGraphVertex, double> AvailableCapacities;

        /// <summary>
        /// Фиксированная составляющая тарифа по точкам (оплата факта бронирования)
        /// </summary>
        public Dictionary<GExGraphVertex, double> BookingCosts;

        /// <summary>
        /// Переменная составляющая тарифа по точкам (оплата фактического потока)         
        /// </summary>
        public Dictionary<GExGraphVertex, double> FlowCosts;
        
        public GExGraph()
        {
            Connections = new Dictionary<GExGraphVertex, Tuple<List<GExGraphEdge>, List<GExGraphEdge>>>();

            BookedCapacities = new Dictionary<GExGraphVertex, double>();

            AvailableCapacities = new Dictionary<GExGraphVertex, double>();

            BookingCosts = new Dictionary<GExGraphVertex, double>();

            FlowCosts = new Dictionary<GExGraphVertex, double>();           
        }

        /// <summary>
        /// Возвращает список вершин в графе
        /// </summary>
        /// <param name="graph"></param>
        /// <returns></returns>
        private GExGraphVertex[] getVertices()
        {
            GExGraphVertex[] vertices = Connections.Keys.ToArray();
            return vertices;
        }

        /// <summary>
        /// Возвращает список дуг в графе
        /// </summary>
        /// <param name="graph"></param>
        /// <returns></returns>
        private GExGraphEdge[] getEdges()
        {
            //GExGraphEdge[] edges = Connections.Values.SelectMany(x => x.Item1.Union(x.Item2)).Distinct().ToArray();

            List<GExGraphEdge> edges = new List<GExGraphEdge>();
            HashSet<long> occupiedIDs = new HashSet<long>();

            foreach (var eColl in Connections.Values)
            {
                var inEdges = eColl.Item1;
                var outEdges = eColl.Item2;

                foreach (GExGraphEdge e in inEdges)
                {
                    long id = e.GetID();
                    if (!occupiedIDs.Contains(id))
                    {
                        edges.Add(e);
                        occupiedIDs.Add(id);
                    }
                }
                foreach (GExGraphEdge e in outEdges)
                {
                    long id = e.GetID();
                    if (!occupiedIDs.Contains(id))
                    {
                        edges.Add(e);
                        occupiedIDs.Add(id);
                    }
                }
            }

            return edges.ToArray();
        }
    }
}
