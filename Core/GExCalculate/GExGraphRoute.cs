﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Calculate
{
    /// <summary>
    /// Структура, описывающая точку маршрута по расчётному графу
    /// </summary>
    public class GExGraphRoutePoint
    {
        /// <summary>
        /// Вершина расчётного графа
        /// </summary>
        public GExGraphVertex Vertex;
        
        /// <summary>
        /// Удельная стоимость транспорта через точку маршрута в рамках фиксированной части тарифа (для маршрутной задачи) 
        /// </summary>
        public double RelativeFixedCost;

        /// <summary>
        /// Удельная стоимость транспорта через точку маршрута в рамках переменной части тарифа (для маршрутной задачи) 
        /// </summary>
        public double RelativeVariableCost;
        
        /// <summary>
        /// Пропускная спообность или объём поставок (в зависимости от задачи)
        /// </summary>
        public double Capacity;

        /// <summary>
        /// Абсолютная стоимость транспорта через точку маршрута в рамках фиксированной части тарифа (для контрактной задачи)
        /// </summary>
        public double AbsoluteFixedCost;

        /// <summary>
        /// Абсолютная стоимость транспорта через точку маршрута в рамках переменной части тарифа (для контрактной задачи)
        /// </summary>
        public double AbsoluteVariableCost;

        public GExGraphRoutePoint()
        {
        }
    }
    
    /// <summary>
    /// Класс маршрута по расчётному графу
    /// </summary>
    public class GExGraphRoute
    {
        /// <summary>
        /// Последовательность вершин расчётного графа 
        /// </summary>
        public List<GExGraphRoutePoint> Path;

        /// <summary>
        /// Удельная стоимость транспорта через точку маршрута в рамках фиксированной части тарифа (для маршрутной задачи) 
        /// </summary>
        public double RelativeFixedCost;

        /// <summary>
        /// Удельная стоимость транспорта через точку маршрута в рамках переменной части тарифа (для маршрутной задачи) 
        /// </summary>
        public double RelativeVariableCost;

        /// <summary>
        /// Общая пропускная способность маршрута
        /// </summary>
        public double TotalCapacity;

        /// <summary>
        /// Абсолютная стоимость транспорта по маршруту в рамках фиксированной части тарифа (для контрактной задачи)
        /// </summary>
        public double AbsoluteFixedCost;

        /// <summary>
        /// Абсолютная стоимость транспорта по маршруту в рамках переменной части тарифа (для контрактной задачи)
        /// </summary>
        public double AbsoluteVariableCost;

        public GExGraphRoute(List<GExGraphVertex> _path)
        {
            Path = new List<GExGraphRoutePoint>();
            foreach (GExGraphVertex v in _path)
            {
                GExGraphRoutePoint p = new GExGraphRoutePoint();
                p.Vertex = v;
                // заполнять позже 
                p.RelativeFixedCost = 0;
                p.RelativeVariableCost = 0;
                p.Capacity = 0;
                
                // для контрактной задачи
                p.AbsoluteFixedCost = 0;
                p.AbsoluteVariableCost = 0;

                Path.Add(p);
            }
            // заполнять позже 
            RelativeFixedCost = 0;
            RelativeVariableCost = 0;
            TotalCapacity = 0;

            // для контрактной задачи
            AbsoluteFixedCost = 0;
            AbsoluteVariableCost = 0;
        }
    }
}
