﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GEx.Calculate
{
    public class GeneralModelSettings
    {
        /// <summary>
        /// Параметры критерия
        /// </summary>
        public IGeneralCriterionInfo CriterionInfo { get; protected set; }

        public GeneralModelSettings()
        {
        }
    }
}
