﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GEx.Calculate
{
    /// <summary>
    /// Интерфейс решателя общей задачи
    /// </summary>
    public abstract class GeneralModelSolver<TModel, TModelSettings, TVariablesPool>
        where TModel : class //IGeneralModel
        where TModelSettings : GeneralModelSettings, new()
        where TVariablesPool : GeneralVariablesPool        
    {       
        /// <summary>
        /// Модель
        /// </summary>
        protected TModel Model { get; set; }

        /// <summary>
        /// Настройки
        /// </summary>
        public TModelSettings Settings { get; set; }

        /// <summary>
        /// Хранилище переменных
        /// </summary>
        public TVariablesPool VariablesPool { get; set; }     

        protected GeneralModelSolver()
        {
        }

        /// <summary>
        /// Строит и решает оптимизационную задачу по заданной модели
        /// </summary>
        /// <param name="mdl">Модель</param>
        /// <returns>Успешность решения задачи</returns>
        public virtual bool Solve(TModel mdl)
        {
            return false;
        }
    }
}
