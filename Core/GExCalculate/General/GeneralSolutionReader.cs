﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GEx.Calculate
{
    /// <summary>
    /// Класс читалки результата
    /// </summary>
    public abstract class GeneralSolutionReader<TModel, TModelSettings, TVariablesPool, TDocResult>
        where TModel : class //IGeneralModel
        where TModelSettings : GeneralModelSettings, new()
        where TVariablesPool : GeneralVariablesPool        
        where TDocResult : class //IGeneralDocResult
    {       
        /// <summary>
        /// Модель
        /// </summary>
        protected TModel Model { get; set; }        
        
        /// <summary>
        /// Настройки
        /// </summary>
        public TModelSettings Settings { get; set; }

        /// <summary>
        /// Хранилище переменных
        /// </summary>
        public TVariablesPool VariablesPool { get; set; }

        /// <summary>
        /// Документ результата
        /// </summary>
        protected TDocResult DocResult { get; set; }
   
        protected GeneralSolutionReader()
        {
        }

        /// <summary>
        /// Чтение результата решения
        /// </summary>
        /// <param name="model">Модель</param>
        /// <param name="variablesPool">Хранилище переменных из решателя</param>
        /// <returns>Документ результата</returns>
        public virtual TDocResult ReadSolution(TModel model, TVariablesPool variablesPool)
        {
            return default(TDocResult);
        }
    }
}
