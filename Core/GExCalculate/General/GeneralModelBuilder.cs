﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using GEx.Domain.DataPumps;

namespace GEx.Calculate
{
    /// <summary>
    /// Общий абстрактный класс конструктора модели
    /// </summary>
    public abstract class GeneralModelBuilder<TModel, TModelSettings, TDataPump>
        where TModel : class //IGeneralModel
        where TModelSettings : GeneralModelSettings, new()
        where TDataPump : IGeneralDataPump         
    {
        /// <summary>
        /// Модель
        /// </summary>
        protected TModel Model { get; set; }

        /// <summary>
        /// Датапамп
        /// </summary>
        protected TDataPump DataPump { get; set; }        

        /// <summary>
        /// Настройки
        /// </summary>
        public TModelSettings Settings { get; set; }
        
        protected GeneralModelBuilder()
        {
        }

        /// <summary>
        /// Построение модели по исходным данным
        /// </summary>
        /// <param name="dataPump">Контейнер с исходными данными</param>
        /// <returns>Заполненную данными модель</returns>
        public virtual TModel BuildModel(TDataPump dataPump)
        {
            return default(TModel);
        }
    }
}
