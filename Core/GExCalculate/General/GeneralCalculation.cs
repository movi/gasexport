﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GEx.Domain.DataPumps;

namespace GEx.Calculate
{
    public abstract class GeneralCalculation<TModel, TModelSettings, TDataPump, TVariablesPool, TDocResult, TModelBuilder, TModelSolver, TSolutionReader>
        where TModel : class //IGeneralModel
        where TModelSettings : GeneralModelSettings, new()
        where TDataPump : IGeneralDataPump
        where TVariablesPool : GeneralVariablesPool
        where TDocResult : class //IGeneralDocResult
        where TModelBuilder : GeneralModelBuilder<TModel, TModelSettings, TDataPump>, new()
        where TModelSolver : GeneralModelSolver<TModel, TModelSettings, TVariablesPool>, new()
        where TSolutionReader : GeneralSolutionReader<TModel, TModelSettings, TVariablesPool, TDocResult>, new()
    {
        #region Fields

        /// <summary>
        /// Расчетная модель
        /// </summary>
        protected TModel Model;

        /// <summary>
        /// Класс - строитель модели
        /// </summary>
        protected TModelBuilder ModelBuilder;

        /// <summary>
        /// Класс - решатель(оптимизатор) модели
        /// </summary>
        protected TModelSolver ModelSolver;

        /// <summary>
        /// Класс чтения результатов
        /// </summary>
        protected TSolutionReader SolutionReader;

        /// <summary>
        /// Хранилище исходных данных
        /// </summary>
        protected TDataPump DataPump;

        /// <summary>
        /// Настройки расчёта и модели
        /// </summary>
        protected TModelSettings Settings;

        #endregion //Fields

        #region Properties

        /// <summary>
        /// Результаты расчета
        /// </summary>
        public TDocResult DocResult { get; protected set; }

        #endregion //Properties

        #region Constructor

        protected GeneralCalculation()
        {
            Settings = new TModelSettings();

            ModelBuilder = new TModelBuilder();
            ModelBuilder.Settings = Settings;

            ModelSolver = new TModelSolver();
            ModelSolver.Settings = Settings;

            SolutionReader = new TSolutionReader();
            SolutionReader.Settings = Settings;
        }

        #endregion //Constructor

        #region Public methods

        /// <summary>
        /// Запуск расчета 
        /// </summary>
        /// <returns></returns>
        public virtual bool Calculate(IGeneralDataPump dataPump)
        {
            if (dataPump == null)
                return false;

            DataPump = (TDataPump)dataPump;
           
            PreCalculate();            
            CreateModel();            
            bool res = SolveModel();

            if (res)
            {                
                ReadSolution();
            }
            
            AfterCalculate();

            return res;
        }

        #endregion //Public methods

        #region Protected methods

        /// <summary>
        /// Общие действия, которые необходимо провести до начала расчета
        /// </summary>
        protected virtual void PreCalculate()
        {
        }

        /// <summary>
        /// Общие действия, которые необходимо провести после начала расчета
        /// </summary>
        protected virtual void AfterCalculate()
        {
        }

        /// <summary>
        /// Создание расчетной потоковой модели
        /// </summary>
        protected virtual void CreateModel()
        {
            Model = ModelBuilder.BuildModel(DataPump);
        }

        /// <summary>
        /// Проведение оптимизационных расчетов на модели
        /// </summary>
        /// <returns>Успешность расчёта</returns>
        protected virtual bool SolveModel()
        {
            return ModelSolver.Solve(Model);
        }

        /// <summary>
        /// Чтение результатов расчета
        /// </summary>
        protected virtual void ReadSolution()
        {
            DocResult = SolutionReader.ReadSolution(Model, ModelSolver.VariablesPool);
        }

        #endregion //Protected methods
    }
}
