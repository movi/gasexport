﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GEx.Domain;

namespace GEx.Calculate
{
    /// <summary>
    /// Вершина расчётного графа задач GasExport
    /// </summary>
    public class GExGraphVertex : IEquatable<GExGraphVertex>
    {
        /// <summary>
        /// Точка входа/выхода - совокупность объекта, балансовой зоны(оператора) и направления
        /// </summary>
        protected PathPoint point;
        
        /// <summary>
        /// Точка входа/выхода - совокупность объекта, балансовой зоны(оператора) и направления
        /// </summary>
        public PathPoint Point 
        { 
            get
            {
                return point;
            } 
        }
        
        public GExGraphVertex(PathPoint _point)
        {
            point = _point;
        }

        public override bool Equals(object obj)
        {
            if (obj is GExGraphVertex)
            {
                return Point.Equals((obj as GExGraphVertex).Point);
            }
            else
            {
                return false;
            }
        }

        public bool Equals(GExGraphVertex other)
        {
            return this.GetID() == other.GetID();
        }

        public override int GetHashCode()
        {
            //return Point.GetHashCode();
            return GetID().GetHashCode();
        }

        public override string ToString()
        {
            return Point.ToString();
        }

        /// <summary>
        /// Возвращаем уникальный идентификатор тройки
        /// </summary>
        /// <returns></returns>
        public long GetID()
        {
            return 1000000 * Point.Operator.ID + 10 * Point.GasExportObject.ID + (int)Point.Direction;
        }
    }
}
