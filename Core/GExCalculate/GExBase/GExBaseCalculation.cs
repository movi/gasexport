﻿using System.Collections.Generic;
using System.Linq;
using GEx.Domain.DataPumps;

namespace GEx.Calculate
{
    /// <summary>
    /// Базовый класс расчёта задачи на графе GasExport
    /// </summary>
    /// <typeparam name="TModelBuilder">Тип конструктора модели</typeparam>
    /// <typeparam name="TModelSolver">Тип солвера</typeparam>
    /// <typeparam name="TSolutionReader">Тип читалки результата</typeparam>
    public abstract class GExBaseCalculation<TModelBuilder, TModelSolver, TSolutionReader> :
                            GeneralCalculation<GExBaseModel, GExBaseModelSettings, IGExBaseDataPump, GExBaseVariablesPool, GExBaseDocResult,
                                                TModelBuilder, TModelSolver, TSolutionReader>
        where TModelBuilder : GExBaseModelBuilder, new()
        where TModelSolver : GExBaseModelSolver, new()
        where TSolutionReader : GExBaseSolutionReader, new()
    {
    }
}