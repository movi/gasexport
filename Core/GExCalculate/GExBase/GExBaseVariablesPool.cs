﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GEx.Calculate
{
    /// <summary>
    /// Базовый абстрактный класс хранилища переменных задач GasExport
    /// </summary>
    public abstract class GExBaseVariablesPool : GeneralVariablesPool
    {
        protected GExBaseVariablesPool()
        {
        }
    }
}
