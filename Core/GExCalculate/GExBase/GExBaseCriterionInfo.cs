﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GEx.Calculate
{
    /// <summary>
    /// Абстрактный класс информации о критерии общей задачи GasExport
    /// </summary>
    public abstract class GExBaseCriterionInfo : IGeneralCriterionInfo
    {
    }
}
