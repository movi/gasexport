﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Calculate
{
    /// <summary>
    /// Абстрактный класс решателя модели общей задачи GasExport
    /// </summary>
    public abstract class GExBaseModelSolver : GeneralModelSolver<GExBaseModel, GExBaseModelSettings, GExBaseVariablesPool>
    {
        protected GExBaseModelSolver()
        {
        }
    }
}
