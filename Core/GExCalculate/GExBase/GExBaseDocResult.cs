﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GEx.Iface;

namespace GEx.Calculate
{
    /// <summary>
    /// Абстрактный класс результата решения задачи GasExport
    /// </summary>
    public abstract class GExBaseDocResult : IGeneralDocResult
    {
        public ModeWork Mode { get; set; }
    }
}
