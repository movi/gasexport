﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using GEx.Domain.DataPumps;

namespace GEx.Calculate
{
    /// <summary>
    /// Абстрактный класс конструктора модели общей задачи GasExport
    /// </summary>
    public abstract class GExBaseModelBuilder : GeneralModelBuilder<GExBaseModel, GExBaseModelSettings, IGExBaseDataPump>
    {
        protected GExBaseModelBuilder()
        {
        }
    }
}