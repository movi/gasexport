﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Calculate
{
    /// <summary>
    /// Дуга расчётного графа задач GasExport
    /// </summary>
    public class GExGraphEdge : IEquatable<GExGraphEdge>
    {
        /// <summary>
        /// Вершина - источник 
        /// </summary>
        protected GExGraphVertex source;

        /// <summary>
        /// Целевая вершина
        /// </summary>
        protected GExGraphVertex target;

        public GExGraphEdge(GExGraphVertex _source, GExGraphVertex _target)
        {
            source = _source;
            target = _target;
        }

        /// <summary>
        /// Вершина - источник 
        /// </summary>
        public GExGraphVertex Source 
        { 
            get
            {
                return source;
            }      
        }

        /// <summary>
        /// Целевая вершина 
        /// </summary>
        public GExGraphVertex Target
        {
            get
            {
                return target;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is GExGraphEdge)
            {
                return Source.Equals((obj as GExGraphEdge).Source) && Source.Equals((obj as GExGraphEdge).Target); 
            }
            else
            {
                return false;
            }
        }

        public bool Equals(GExGraphEdge other)
        {
            return this.GetID() == other.GetID();
        }

        public override int GetHashCode()
        {
            //return (new Tuple<GExGraphVertex, GExGraphVertex>(Source, Target)).GetHashCode(); 
            return GetID().GetHashCode();
        }

        public override string ToString()
        {
            return Source.ToString() + " - " + Target.ToString();
        }


        /// <summary>
        /// Возвращаем уникальный идентификатор дуги
        /// </summary>
        /// <returns></returns>
        public long GetID()
        {
            return 1000000000 * Source.GetID() + Target.GetID();
        }
    }
}
