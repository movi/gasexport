﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Iface
{
    public class DTO_NodeRelation
    {
        public int ID { get; set; }

        public int NodeIdFirst { get; set; }

        public int NodeIdSecond { get; set; }

        public int ZoneID { get; set; }

        public double Value { get; set; }
    }
}
