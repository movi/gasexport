﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Iface
{
    public class DTO_Scenario
    {
        public int ID { get; set; }

        /// <summary>
        /// Название сценария
        /// </summary>
        public string Name { get; set; }

        public DateTime CreationDate { get; set; }

        public string Author { get; set; }

        public string Comment { get; set; }
    }
}
