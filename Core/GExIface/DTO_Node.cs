﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GEx.Iface
{
    public enum DTO_Node_type
    {
        [Description("Пункт поступления российского газа")]
        RussianGasInput, //      пункты поступления российского газа;
        [Description("ПХГ")]
        UGS, //Underground gas storage -- ПХГ
        [Description("Граница")]
        Border, //	граничные узлы;
        [Description("Пункт сдачи")]
        Output, // пункты сдачи. 
        [Description("Виртуальная торговая площадка")]
        VirtualTradingPoint
    }

    /// <summary>
    /// DTO (транспортная структура для передачи данных по значению) для узла.
    /// В морду мы передаем данные в таких структурах
    /// </summary>
    public class DTO_Node
    {
        public DTO_Node_type Type { get; set; }

        public int ID { get; set; }

        public double X { get; set; }

        public double Y { get; set; }
        
        /// <summary>
        /// Координата по оси X позиции надписи 
        /// </summary>
        public double? LabelX { get; set; }

        /// <summary>
        /// Координата по оси Y позиции надписи
        /// </summary>
        public double? LabelY { get; set; }

        public string Name { get; set; }

        public string ZoneName { get; set; }

        public int? ZoneId { get; set; }

        public ICollection<DTO_BorderInterconnection> InterConnections { get; set; }

        public bool IsOutput { get; set; }

        public DTO_Node()
        {
            InterConnections = new List<DTO_BorderInterconnection>();            
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
