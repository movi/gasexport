﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Iface
{
    public enum DTO_Direction
    {
        [Description("Вход")]            
        Entry,
        [Description("Выход")]            
        Exit      
    }
}
