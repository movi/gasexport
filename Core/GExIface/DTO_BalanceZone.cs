﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GEx.Domain;

namespace GEx.Iface
{
    public class DTO_BalanceZone
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var bzone = obj as DTO_BalanceZone;
            if (bzone != null)
            {
                return (ID == bzone.ID);
            }
            return false;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
