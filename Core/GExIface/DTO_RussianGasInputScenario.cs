﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Iface
{
    public class DTO_RussianGasInputScenario
    {
        public int Id { get; set; }

        public  string RussianGasInputName { get; set; }

        public Nullable<double> MaxExportVolume { get; set; }

        public bool IsActive { get; set; }
    }
}
