﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Iface
{
    public class DTO_BorderInterconnection
    {
        public int Id { get; set; }

        public DTO_BalanceZone ZoneFrom {  get; set;  }

        public DTO_BalanceZone ZoneTo { get; set;  }
    }
}
