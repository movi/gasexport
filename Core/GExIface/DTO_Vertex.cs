﻿namespace GEx.Iface
{
    public class DTO_Vertex
    {      
        public DTO_Node Node { get; set; }

        /// <summary>
        /// сделано по аналогии с PathPoint
        /// </summary>
        public DTO_BalanceZone Zone { get; set; }

        /// <summary>
        /// Направление
        /// TODO: (пока неясно, нужно ли)
        /// </summary>
        public DTO_Direction Direction { get; set; }

        /// <summary>
        /// Удельная фиксированная cтоимость транспортировки, € / млн м³
        /// </summary>
        public double RelativeFixedCost{ get; set; }

        /// <summary>
        /// Удельная переменная cтоимость транспортировки, € / млн м³
        /// </summary>
        public double RelativeVariableCost { get; set; }

        /// <summary>
        /// Фиксированная часть стоимости, € / млн м³
        /// </summary>
        public double AbsoluteFixedCost { get; set; }

        /// <summary>
        /// Переменная часть стоимости, € / млн м³
        /// </summary>
        public double AbsoluteVariableCost { get; set; }

        /// <summary>
        /// Пропускная способность маршрута
        /// </summary>
        public double Capacity{ get; set; }
    }
}