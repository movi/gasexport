﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Iface
{
    public class DTO_Nominations
    {
        //информация о границе
        public int Id_Border { get; set; }
        public string BorderName { get; set; }

        // информация о балансовой зоне(операторе)
        public int Id_BalanceZone { get; set; }
        public string BalanceZoneName { get; set; }

        // направление
        public DTO_Direction Direction { get; set; }

        public double? NominationValue { get; set; }

        public bool IsActive { get; set; }

        //Является ли источником
        public bool IsSource { get; set; }
    }
}
