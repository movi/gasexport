﻿using System.Collections.Generic;

namespace GEx.Iface
{
    public class DTO_Path
    {
        /// <summary>
        /// Точка выхода для группировки в режиме контрактов
        /// Multi режим выбора начальных и конечных точек
        /// </summary>
        public DTO_BalanceZone OutputZone { get; set; }

        public List<DTO_Vertex> Vertices { get; set; }

        /// <summary>
        /// Удельная фиксированная cтоимость транспортировки, € / млн м³
        /// </summary>
        public double RelativeFixedCost{ get; set; }

        /// <summary>
        /// Удельная переменная cтоимость транспортировки, € / млн м³
        /// </summary>
        public double RelativeVariableCost { get; set; }

        /// <summary>
        /// Фиксированная часть стоимости, € / млн м³
        /// </summary>
        public double AbsoluteFixedCost { get; set; }

        /// <summary>
        /// Переменная часть стоимости, € / млн м³
        /// </summary>
        public double AbsoluteVariableCost { get; set; }

        /// <summary>
        /// Общая пропускная способность маршрута
        /// </summary>
        public double TotalCapacity{ get; set; }
    }
}