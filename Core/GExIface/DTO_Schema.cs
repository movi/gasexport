﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Iface
{
    public class DTO_Schema
    {
        public int Id { get; set; }

        public string SchemaName { get; set; }

        public DateTime CreationDate { get; set; }

        public string Author { get; set; }

        public string Comment { get; set; }

        public byte[] Image { get; set;  }
    }
}
