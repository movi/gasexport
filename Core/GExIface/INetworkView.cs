﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Iface
{
    /// <summary>
    /// Интерфейс для GoView
    /// (для GUI)
    /// </summary>
    public interface INetworkView
    {
        void AddToNetwork(INodeView nodeView);
        void AddToNetwork(IConnectionView connectionView);
        IEnumerable<INodeView> EnumerateNodes();
        IEnumerable<IConnectionView> EnumerateLinks();
        void ClearDoc();
        void ClearLinks();
        void RemoveNodeAsSelectedForPath(DTO_Node node);
        void SetNodeAsSelectedForPath(DTO_Node node);
        void SetPosition(float x, float y);
        void SetUnderlayer(Bitmap bitmap);
        byte[] MapUnderlayer { get; set; }
    }

    /// <summary>
    /// Связь двух узлов
    /// </summary>
    public interface IConnectionView
    {
        INodeView NodeFrom { get; set; }
        INodeView NodeTo { get; set; }
        void Select();
        void DeSelect();
    }

    /// <summary>
    /// Представление узла для GoView
    /// 
    /// (в конечном счете это те же GoNode в случае GoDiagram)
    /// </summary>
    public interface INodeView
    {
        /// <summary>
        /// Положение по оси X
        /// </summary>
        float X { get; set; }

        /// <summary>
        /// Положение по оси Y
        /// </summary>
        float Y { get; set; }

        /// <summary>
        /// Свойство для порта - объекта, к которому крепится connection
        /// </summary>
        /// <returns></returns>
        IPort GetPort();

        string Name { get; set; }

        DTO_Node NodeDocument { get; set; }

        DTO_Node_type NodeType { get; set; }

        /// <summary>
        /// Процесс выбора точки маршрута
        /// </summary>
        event Func<DTO_Node, bool> OnChoosingFirstPoint;
        
        /// <summary>
        /// После успешного выбора точки маршрута
        /// </summary>
        event Action<DTO_Node> OnChosenFirstPoint;
        event Action<DTO_Node> ChooseSecondPoint;
        event Action<DTO_Node> RemovePoint;

        void SetLabelFontSize(float size);

        float LabelX { get; set; }
        float LabelY { get; set; }
    }
    
    public interface ILabelView
    {
        INodeView Parent { get; }
    }

    /// <summary>
    /// Абстрактный порт.
    /// Не имеет ничего.
    /// 
    /// (Возможно, будет иметь свою позицию)
    /// </summary>
    public interface IPort
    {
    }

    public class GenericDiagramFactory<TNode, TConnection, TCanvas> : IDiagramFactory
        where TNode : INodeView, new()
        where TConnection : IConnectionView, new()
        where TCanvas : INetworkView, new()
    {
        INodeView IDiagramFactory.CreateNode()
        {
            return new TNode();
        }

        IConnectionView IDiagramFactory.CreateConnection()
        {
            return new TConnection();
        }

        INetworkView IDiagramFactory.CreateCanvas()
        {
            return new TCanvas();
        }
    }
}
