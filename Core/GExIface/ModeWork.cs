﻿namespace GEx.Iface
{
    /// <summary>
    /// Режим работы приложения
    /// </summary>
    public enum ModeWork
    {
        Single = 0,
        Multi = 1
    }
}