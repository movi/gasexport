﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Iface
{
    public class DTO_UGSScenarioData
    {
        public int Id { get; set; }

        public string UGSName { get; set; }

        public Nullable<double> MaxUGSWithdrawal { get; set; }

        public bool IsActive { get; set; }
    }
}
