﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Iface
{
    public class DTO_OutputScenarioData
    {
        public int Id { get; set; }

        public string OutputName { get; set; }

        public Nullable<double> ConctractVolume { get; set; }

        public bool IsActive { get; set; }

    }
}
