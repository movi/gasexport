﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Iface
{
    public class DTO_BorderScenarioData
    {
        //информация о границе
        public int Id_Border { get; set; }
        public string BorderName { get; set; }

        // информация о балансовой зоне(операторе)
        public int Id_BalanceZone { get; set; }
        public string BalanceZoneName { get; set; }

        // направление
        public DTO_Direction Direction { get; set; }
        
        //Уже забронированный объём, млн м3 - это входное поле, редактируется вручную 
        public double? ReservedVolumeValue { get; set; }

        //Доступный объём, млн м3 - это входное поле, редактируется вручную        
        public double? AbleVolumeValue { get; set; }

        /// <summary>
        /// Фиксированная часть тарифа, € / млн м3 - это входное поле, редактируется вручную    
        /// </summary>     
        public double? TarifValue { get; set; }

        /// <summary>
        /// Стоимость ГСН, €/млн м³
        /// </summary>
        public double? GsnValue { get; set; }

        /// <summary>
        /// % от стоимости  ГСН
        /// </summary>
        public double? GsnPercentValue { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        public string Description { get; set; }

        public bool IsActive { get; set; }

    }
}
