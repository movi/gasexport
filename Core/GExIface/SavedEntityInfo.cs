﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEx.Iface
{
    public class SavedEntityInfo
    {
        public int IdBeforeSave { get; set; }

        public int IdAfterSave { get; set; }

    }
}
