﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GEx.ErrorHandler;
using GEx.Domain;
using GEx.Domain.DataPumps;

namespace GEx.Iface
{
    /// <summary>
    /// IMB: Интерфейс, о котором знает морда (и только его), чтобы получить функционал от нижележащих слоев (BL, DAL).
    /// Главная задача этой пары интрефейсов - снизить зацепление между слоями
    /// Данные в морду передаются по запросу, в виде наборов DTO. Морда напрямую в объектную модель, хранимую в BL (CoreHolder), не ходит, состояние не изменяет.
    /// </summary>
    public interface IGExDomain
    {
        void Init(GEx.ErrorHandler.IErrorOutput eh, GEx.Iface.IGExDomainCallback cbk);

        #region Schema

        /// <summary>
        /// Создает новую пустую схему.
        /// </summary>
        void CreateNewSchema(bool onCurrent);

        /// <summary>
        /// Загружает схему с данным ид из БД.
        /// Возвращает референс на загруженную схему.
        /// </summary>
        void LoadSchema(int schemaId);

        /// <summary>
        /// Возвращает список DTO схем, имеющихся в БД 
        /// </summary>
        /// <returns></returns>
        IEnumerable<DTO_Schema> LoadSchemas();

        /// <summary>
        /// Загружена ли схема (любая) в главное окно. Важный признак для логики работы морды 
        /// </summary>
        /// <returns></returns>
        bool IsSchemaLoaded();

        DTO_Schema GetSchema();

        /// <summary>
        /// Через этот метод рисуем схему. Получаем набор DTO Для отрисовки схемы.
        /// </summary>
        IEnumerable<DTO_Node> GetSchemaNodes();

        void SaveSchema(byte[] mapUnderlayer);

        void SaveSchemaAsNew(byte[] mapUnderlayer);

        void RemoveSchema(int Id);

        void UpdateSchemaName(int IdSchema, string Name);

        void UpdateSchemaComment(int IdSchema, string comment);

        void UpdateSchema(DTO_Schema schema);

        bool HasSchemaChanges();

        #endregion

        #region Scenario

        /// <summary>
        /// Получает список сценариев из БД
        /// </summary>
        /// <returns></returns>
        /// 
        IEnumerable<DTO_Scenario> LoadCalculationScenarios();

        IEnumerable<DTO_OutputScenarioData> LoadOutputScenarioData();

        IEnumerable<DTO_RussianGasInputScenario> LoadRussianGasInputScenario();

        IEnumerable<DTO_UGSScenarioData> LoadUGSScenarioData();

        IEnumerable<DTO_BorderScenarioData> LoadBorderScenarioData();

        IEnumerable<DTO_Nominations> LoadNominations();

        IEnumerable<DTO_Deliveries> LoadDeliveries();

        //IEnumerable<DTO_ScenarioData> LoadUGSScenarioData();

        void UpdateOutputScenario(int id, Nullable<double> ConctractVolume);

        void UpdateRussianGasInputScenario(int id, Nullable<double> MaxExportVolume);

        void UpdateUGSScenarioData(int id, Nullable<double> MaxUGSWithdrawal);

        bool ScenarioIsNew();

        void UpdateBorderScenario(int idNode, int IdBalanceZone, DTO_Direction direct, Nullable<double> AbleVolumeValue, Nullable<double> ReservedVolumeValue, Nullable<double> TarifValue, Nullable<double> gsnValue, Nullable<double> gsnPercentValue, string descrioption);

        void UpdateBalanceZones(IEnumerable<DTO_BalanceZone> zones);

        void SaveOperators();

        void UpdateGazExportObjectsZones();

        void UpdateRelations();

        /// <summary>
        /// Создает новый пустой сценарий.
        /// </summary>
        void CreateNewScenario();

        //TODO: сделать возвращаемый тип void.  Использовать всегда GetScenario()
        void LoadScenario(int IdScenario);

        void UpdateScenarioName(int IdScenario, string Name);
        void UpdateScenarioComment(int IdScenario, string comm);

        void UpdateScenarioName(string Name, string comm);

        void UpdateScenario(DTO_Scenario scenarioDto);

        void RemoveScenario(int IdScenario);

        void SaveScenario();

        void SaveScenarioAsNew();

        void SetScenarioParameterValue(GasExportObjectType type, object value);

        DTO_Scenario GetScenario();

        /// <summary>
        /// Можно ли сохранять сценарий. Если есть несохраненные ноды для данной схемы (объекты справочников), то возвращает false.
        /// </summary>
        /// <returns></returns>
        bool AllNodesSaved();

        bool ZoneHasRelations(int idZone);

        /// <summary>
        /// Имеются ли в текущем сценарии несохраненные изменения(true), если нет, то возвращает false.
        /// </summary>
        /// <returns></returns>
        bool ScenarioHasUnsavedChanges();

        #endregion

        /// <summary>
        /// Добавляет новый узел на текущую схему (текущая схема хранится в CoreHolder) и возвращает Id узла 
        /// </summary>
        /// <param name="node"></param>
        int AddNode(DTO_Node node, bool callBack);

        /// <summary>
        /// Обновляет данный узел в объектной модели
        /// </summary>
        /// <param name="node"></param>
        void UpdateNode(DTO_Node node);

        void UpdateNodePosition(int nodeId, double x, double y);
        
        void UpdateNodeLabelOffset(int nodeId, double x, double y);

        void UpdateNodeName(int nodeId, string name);

        void UpdateNodeOutput(int nodeId, bool output);

        /// <summary>
        /// Удаляем данный узел из объектной модели
        /// </summary>
        /// <param name="node"></param>
        void RemoveNode(DTO_Node node);
        /// <summary>
        /// Сохраняет новую балансовую зону(оператора)
        /// </summary>
        /// <returns></returns>
        void AddBalanceZone(DTO_BalanceZone zone);


        void AddInterconnection(DTO_BorderInterconnection interconnection, int nodeId);

        void UpdateInterconnection(DTO_BorderInterconnection interconnection, int nodeId);

        void RemoveInterconnection(int id, int nodeId);

        void SetBalanceZoneToNode(DTO_BalanceZone zone, int nodeId);

        bool TerminalPointsSelected();

        /// <summary>
        /// Возвращает список всех балансовых зон(операторов)
        /// </summary>
        /// <returns></returns>
        IEnumerable<BalanceZone> GetBalanceZones();

        /// <summary>
        /// Метод загрузки матрицы в морду. Метод возвращает множество пар ДТО узлов. Вызываем по открытию диалога с матрицей.
        /// </summary>
        IEnumerable<DTO_NodeRelation> LoadNodeRelations();

        /// <summary>
        /// Метод обновления матрицы. Изменения из диалоговой формы сохраняем этим методом (по кнопке ОК).
        /// </summary>
        void UpdateNodeRelations(IEnumerable<DTO_NodeRelation> relations);

        /// <summary>
        /// Вызов расчета. Входные и выходные данные находятся в CoreHolder
        /// </summary>
        /// <returns></returns>
        bool Calculate();

        IGeneralDocResult DocResult { get; }

        ModeWork Mode { get; set; }

        IEnumerable<DTO_Path> GetCalculatedPaths();

        bool IsCalculated();

        /// <summary>
        /// Вызывать перед завержением проги
        /// </summary>
        void UnInit();

        /// <summary>
        /// Генерирует новую схему. Для дебага на ранних стадиях разработки.
        /// </summary>
        Schema GenerateModel();// 4delete
        
        /// <summary>
        /// Выбор первых точек маршрута (используется в расчете)
        /// </summary>
        /// <param name="node"></param>
        bool SetFirstPointsForPath(DTO_Node node);

        /// <summary>
        /// Выбор конечных точек маршрута (используется в расчете)
        /// </summary>
        /// <param name="node"></param>
        void SetLastPointsForPath(DTO_Node node);
        
        /// <summary>
        /// Установить соединения к другим узлам при выборе зоны
        /// </summary>
        /// <param name="nodeid"></param>
        /// <param name="zone"></param>
        void SetConnections(int nodeid, DTO_BalanceZone zone);

        /// <summary>
        /// Показать уже рассчитаный результат
        /// </summary>
        /// <param name="result"></param>
        void ShowCalculate(IGeneralDocResult result);

        /// <summary>
        /// Очистить точки маршрутов
        /// </summary>
        void ClearPointsPaths();

        /// <summary>
        /// Удалить точку маршрута
        /// </summary>
        /// <param name="node"></param>
        void RemovePoint(DTO_Node node);
    }

    /// <summary>
    /// IMB: Интерфейс, через который нижние слои вызывают морду (и только через него).
    /// </summary>
    public interface IGExDomainCallback
    {
        void CallGui(string msg);
        void BeforeNewSchemaSave(DTO_Schema sch);
        void OnScenarioLoaded();
        void OnScenarioSaved();
        void OnNodeAdded();
        void OnNodeUpdated(DTO_Node node);
        void OnRemoveNode(int nodeId);
        void OnSchemaSaved(SavedEntityInfo schemaInfo, IEnumerable<SavedEntityInfo> nodesInfo);
        void OnSchemaInit();
        void OnCalculated();
        void OnRemoveNodeAsSelectedForPath(DTO_Node node);
    }

    /// <summary>
    /// По сути, будет правильным определить интерфейс результатов расчета в этой сборке.
    /// Использовать и реализовывать уже в Calculate-сборке
    /// </summary>
    public interface IGeneralDocResult
    {
        ModeWork Mode { get; set; }
    }

    public interface IGExCalculate
    {
        void Init(IErrorOutput eh);
        void Start(GExRoutesDataPump data, ModeWork mode);
        void Start(GExContractsDataPump data, ModeWork mode);
        void Stop();

        IGeneralDocResult Result { get;}
    }

    public interface ISchemaRepository
    {
        void Init(GEx.ErrorHandler.IErrorOutput eh);

        Schema Load(int schemaId);
        
        void Create(Schema schema);

        void Update(Schema schema);

        IEnumerable<Schema> LoadAll();

        void Remove(int Id);
    }

    public interface IBalanceZoneRepository
    {
        void Init(GEx.ErrorHandler.IErrorOutput eh);

        void Create(BalanceZone zone);

        void Update(BalanceZone zone);

        void Remove(int id);

        IEnumerable<BalanceZone> LoadAll();

        void Update(IEnumerable<BalanceZone> zones);

        bool HasRelations(int id);

    }

    public interface IScenarioRepository
    {
        void Init(GEx.ErrorHandler.IErrorOutput eh);

        IEnumerable<CalculationScenario> LoadAll();

        void Create(CalculationScenario scenario);

        void Update(CalculationScenario scenario);

        CalculationScenario Load(int scenarioId);

        void Remove(int scenarioId);
    }

    public interface IGasExportObjectRepository
    {
       void Init(GEx.ErrorHandler.IErrorOutput eh);

       void Update(IEnumerable<GasExportObject> objects);
    }

    public interface IUpdateRelationsCommand
    {
        void Init(GEx.ErrorHandler.IErrorOutput eh);

        void Update(Schema objects);
    }
    
    //public interface IFullRepository
    //{
    //    void Init(GEx.ErrorHandler.IErrorOutput eh);

    //    IEnumerable<BalanceZone> LoadBalanceZones();

    //    Schema Load(int schemaId);

    //    CalculationScenario LoadScenario(int IdScenario);

    //    void LoadAll(int schemaID);
    //}

    /// <summary>
    /// Порождающий интерфейс для поддержки
    /// визуального отображения графа
    /// </summary>
    public interface IDiagramFactory
    {
        INodeView CreateNode();
        IConnectionView CreateConnection();
        INetworkView CreateCanvas();
    }


}
