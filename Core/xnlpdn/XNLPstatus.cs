﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Optimizer
{
    public enum XNLPstatus
    {
        XNLP_STATUS_UNSTARTED,
        XNLP_STATUS_LOCALLY_OPTIMAL,
        XNLP_STATUS_OPTIMAL,
        XNLP_STATUS_LOCALLY_INFEASIBLE,
        XNLP_STATUS_INFEASIBLE,
        XNLP_STATUS_UNBOUNDED,
        XNLP_STATUS_UNSOLVED

    }
}
