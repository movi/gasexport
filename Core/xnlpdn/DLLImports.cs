using System;
using System.Runtime.InteropServices;
namespace Optimizer
{
	public class DLLImports
	{
		[DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
		public static extern int XNLPinit(string s);
		[DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
		public static extern int XNLPfree();
		[DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
		public static extern int XNLPcreateprob(out IntPtr _probholder, int optimization_sense);
		[DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
		public static extern int XNLPcreatecolumns(IntPtr prob, int col_count, string[] col_names, char[] col_types, double[] lb, double[] ub);
		[DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
		public static extern int XNLPcreaterows(IntPtr prob, int row_count, string[] row_names, char[] row_types);
		[DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
		public static extern int XNLPaddtokenizedterms(IntPtr prob, int format, int count, int[] row_index, int[] row_offset, int[] token_type, double[] token_value);
		[DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
		public static extern int XNLPtokenizeexpression(IntPtr prob, string expr, int max_size, out int size, int[] token_type, double[] token_value);
		[DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
		public static extern int XNLPaddquadraticterms(IntPtr prob, int count, int[] row_index, int[] row_offset, int[] col_index1, int[] col_index2, double[] value);
		[DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
		public static extern int XNLPaddconstantterms(IntPtr prob, int count, int[] row_index, double[] value);
		[DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
		public static extern int XNLPaddrhsterms(IntPtr prob, int count, int[] row_index, double[] value);
		[DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
		public static extern int XNLPaddlinearterms(IntPtr prob, int count, int[] row_index, int[] row_offset, int[] col_index, double[] value);
		[DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
		public static extern int XNLPoptimize(IntPtr prob, out int status);
		[DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
		public static extern int XNLPsetinitialvalues(IntPtr prob, int count, int[] col_index, double[] value);
		[DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
		public static extern int XNLPgetsolution(IntPtr prob, int count, int[] col_indices, double[] value);
		[DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
		public static extern int XNLPgetlasterror(IntPtr prob, string msg, int size, out int source, out int code);
		[DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
		public static extern int XNLPsetintcontrol(IntPtr prob, int control, int val);
		[DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
		public static extern int XNLPgetintcontrol(IntPtr prob, int control, out int val);
		[DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
		public static extern int XNLPgetsolverintcontrol(IntPtr prob, int control, out int val);
		[DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
		public static extern int XNLPgetintattribute(IntPtr prob, int control, out int val);
		[DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
		public static extern int XNLPgetdoubleattribute(IntPtr prob, int control, out double val);
		[DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
		public static extern int XNLPsetdoublecontrol(IntPtr prob, int control, double val);
		[DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
		public static extern int XNLPsetbounds(IntPtr prob, int count, int[] col_indices, double[] lower, double[] upper);
		[DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
		public static extern int XNLPsetcolumntype(IntPtr prob, int count, int[] col_indices, char[] colTypes);
		[DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
		public static extern int XNLPsetrowtype(IntPtr prob, int count, int[] row_indices, char[] rowTypes);
        [DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
        public static extern int XNLPwriteprob(IntPtr prob, string file, int format, int options);
        [DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
        public static extern int XNLPsetoptimizationsense(IntPtr prob, int sence);
        [DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
        public static extern int XNLPgetoptimizationsense(IntPtr prob, out int sence);

        [DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
        public static extern int XNLPenableconsoleoutput(IntPtr prob);
        [DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
        public static extern int XNLPdisableconsoleoutput(IntPtr prob);
        [DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
        public static extern int XNLPopenlogfile(IntPtr prob, string fileName);
        [DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
        public static extern int XNLPappendlogfile(IntPtr prob, string fileName);
        [DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
        public static extern int XNLPcloselogfile(IntPtr prob, string fileName);
        [DllImport("xnlp.dll", CharSet = CharSet.Ansi)]
        public static extern int XNLPcloselogfiles(IntPtr prob);
    }
}
