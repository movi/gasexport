using System;
namespace Optimizer
{
	public class XNLPConst
	{
		public const int XNLP_OBJECTIVE_ROW = -1;
		public const int XNLP_MINIMIZE = 0;
		public const int XNLP_MAXIMIZE = 1;
		public const int XNLP_TOKEN_FORMAT_INFIX = 0;
		public const int XNLP_TOKEN_FORMAT_POSTFIX = 1;
		public const int XNLP_OK = 0;
		public const int XNLP_ERROR = 32;
		public const int XNLP_FEASIBILITY_TOLERANCE = 15000;
		public const int XNLP_OPTIMALITY_TOLERANCE = 15001;
		public const int XNLP_INTEGRALITY_TOLERANCE = 15002;
		public const int XNLP_SOLVER = 15003;
		public const int XNLP_SOLVER_AUTO = -1;
		public const int XNLP_SOLVER_SLP = 0;
		public const int XNLP_SOLVER_KNITRO = 1;
		public const int XNLP_SOLVER_OPTIMIZER = 2;
		public const int XNLP_OBJECTIVE_VALUE = 17503;
		public const int XNLP_SOLVER_SELECTED = 17505;
		public const int XNLP_SOURCE_NONE = 0;
		public const int XNLP_SOURCE_XNLP = 1;
		public const int XNLP_SOURCE_OS = 2;
		public const int XNLP_SOURCE_OPTIMIZER = 20;
		public const int XNLP_SOURCE_SLP = 21;
		public const int XNLP_PARAMETER_ERROR = 1;
		public const int XNLP_MEMORY_ERROR = 2;
		public const int XNLP_ACCESS_ERROR = 3;
		public const int XNLP_INTERNAL_ERROR = 4;
		public const int XNLP_OS_ERROR = 5;
		public const int XNLP_TOKEN_ERROR = 6;
		public const int XNLP_STRING_ERROR = 7;
		public const int XNLP_BAD_PROB_ERROR = 8;
		public const int XNLP_FILE_FORMAT_MPS = 0;
		public const int XNLP_FILE_FORMAT_LP = 1;
	}
}
