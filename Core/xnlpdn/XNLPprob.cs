﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using XSLPdnet;
using System.Reflection;

namespace Optimizer
{
    public class XNLPprob : IDisposable
    {
        private IntPtr objPtr;

        public static bool Init()
        {
            int num = DLLImports.XNLPinit(null);
            //if (num != XNLPConst.XNLP_OK)
            //    throw new XPRSException("XNLPInit failed, returned " + num.ToString());
            return num == XNLPConst.XNLP_OK;
        }

        public static void Free()
        {
            int num = DLLImports.XNLPfree();
            if (num != XNLPConst.XNLP_OK)
                throw new XPRSException("XNLPFree failed, returned " + num.ToString());
        }

        public XNLPprob(int sense)
        {
            int num = DLLImports.XNLPcreateprob(out this.objPtr, sense);
            if (this.objPtr.Equals(IntPtr.Zero) || num != 0)
            {
                if (num != 0)
                {
                    string name = MethodBase.GetCurrentMethod().Name;
                    this.ThrowError(name);
                }
            }
        }

        public XNLPprob()
        {
            int num = DLLImports.XNLPcreateprob(out objPtr, XNLPConst.XNLP_MINIMIZE);
            if (objPtr.Equals(IntPtr.Zero) || num != XNLPConst.XNLP_OK)
            {
                var name = MethodBase.GetCurrentMethod().Name;
                this.ThrowError(name);
            }
        }


        public void CreateVars(string[] columnNames, char[] columnTypes, double[] lb, double[] ub)
        {
            int num = DLLImports.XNLPcreatecolumns(this.objPtr, columnNames.Length, columnNames, columnTypes, lb, ub);
            if (num != XNLPConst.XNLP_OK)
            {
                var name = MethodBase.GetCurrentMethod().Name;
                this.ThrowError(name);
            }
        }

        public void CreateConstraints(string[] rowNames, char[] rowTypes)
        {
            int num = DLLImports.XNLPcreaterows(this.objPtr, rowNames.Length, rowNames, rowTypes);
            if (num != XNLPConst.XNLP_OK)
            {
                var name = MethodBase.GetCurrentMethod().Name;
                this.ThrowError(name);
            }
        }

        public void AddLinearMatrix(double[,] matrix, int rowCount, int colCount)
        {

            List<int> colsArr = new List<int>();
            List<double> coeffsArr = new List<double>();
            List<int> rowsArr = new List<int>();
            List<int> offsetArr = new List<int>();

            for (int row = 0; row < rowCount; row++)
            {
                int offsetVal = -1;
                for (int col = 0; col < colCount; col++)
                {
                    double val = matrix[col, row];
                    if (val != 0)
                    {
                        if (offsetVal == -1)
                            offsetVal = colsArr.Count;
                        colsArr.Add(col);
                        coeffsArr.Add(val);
                    }
                }
                if (offsetVal != -1)
                {
                    rowsArr.Add(row);
                    offsetArr.Add(offsetVal);
                }
            }
            offsetArr.Add(colsArr.Count);


            int num = DLLImports.XNLPaddlinearterms(this.objPtr, rowsArr.Count, rowsArr.ToArray(), offsetArr.ToArray(), colsArr.ToArray(), coeffsArr.ToArray());
            if (num != XNLPConst.XNLP_OK)
            {
                var name = MethodBase.GetCurrentMethod().Name;
                this.ThrowError(name);
            }
        }

        public void AddLinearMatrix(int[] equationIndexes, int[] varsOffsetsInEquations, int[] varsIndexesInEquations, double[] varsCoeffsInEquations)
        {
            int num = DLLImports.XNLPaddlinearterms(this.objPtr, equationIndexes.Length, equationIndexes, varsOffsetsInEquations, varsIndexesInEquations, varsCoeffsInEquations);
            if (num != XNLPConst.XNLP_OK)
            {
                var name = MethodBase.GetCurrentMethod().Name;
                this.ThrowError(name);
            }
        }


        public void AddRhs(double[] rhs, int[] indexes)
        {
            int num = DLLImports.XNLPaddrhsterms(this.objPtr, indexes.Length, indexes, rhs);
            if (num != XNLPConst.XNLP_OK)
            {
                var name = MethodBase.GetCurrentMethod().Name;
                this.ThrowError(name);
            }
        }

        public void AddLinearCriteria(double[] coeffs, int[] indexes)
        {
            int varCount = indexes.Length;

            int num = DLLImports.XNLPaddlinearterms(this.objPtr, 1, new int[] { XNLPConst.XNLP_OBJECTIVE_ROW }, new int[] { 0, varCount }, indexes, coeffs);
            if (num != XNLPConst.XNLP_OK)
            {
                var name = MethodBase.GetCurrentMethod().Name;
                this.ThrowError(name);
            }
        }

        public void AddVarsIndicator(int[] var1Indexes, int[] var2Indexes, int[] eqIndexes)
        {
            int num = var1Indexes.Length;
            if (num != XNLPConst.XNLP_OK)
            {
                int num2 = 4;
                for (int i = 0; i < num; i++)
                {
                    int num3 = 0;
                    int[] array = new int[num2];
                    double[] array2 = new double[num2];
                    array[num3] = 10;
                    array2[num3++] = (double)var1Indexes[i];
                    array[num3] = 31;
                    array2[num3++] = 3.0;
                    array[num3] = 10;
                    array2[num3++] = (double)var2Indexes[i];
                    array[num3] = 0;
                    array2[num3++] = 0.0;
                    int num4 = DLLImports.XNLPaddtokenizedterms(this.objPtr, 0, 1, new int[]
					{
						eqIndexes[i]
					}, new int[]
					{
						0, 
						num2
					}, array, array2);
                    if (num4 != 0)
                    {
                        string name = MethodBase.GetCurrentMethod().Name;
                        this.ThrowError(name);
                    }
                }
            }
        }

        public void AddVarsIndicator2(int[] var1Indexes, int[] var2Indexes, int[] eqIndexes)
        {
            int num = var1Indexes.Length;
            if (num != XNLPConst.XNLP_OK)
            {
                int num2 = 4;
                int[] array = new int[num2 * num];
                double[] array2 = new double[num2 * num];
                int[] array3 = new int[num + 1];
                array3[0] = 0;
                for (int i = 0; i < num; i++)
                {
                    int num3 = 0;
                    array[num2 * i + num3] = 10;
                    array2[num2 * i + num3++] = (double)var1Indexes[i];
                    array[num2 * i + num3] = 31;
                    array2[num2 * i + num3++] = 3.0;
                    array[num2 * i + num3] = 10;
                    array2[num2 * i + num3++] = (double)var2Indexes[i];
                    array[num2 * i + num3] = 0;
                    array2[num2 * i + num3++] = 0.0;
                    array3[i + 1] = array3[i] + num2;
                }
                int num4 = DLLImports.XNLPaddtokenizedterms(this.objPtr, 0, num, eqIndexes, array3, array, array2);
                if (num4 != 0)
                {
                    string name = MethodBase.GetCurrentMethod().Name;
                    this.ThrowError(name);
                }
            }
        }

        public void SetInitialValues(int[] varIndexes, double[] varValues)
        {
            int num = DLLImports.XNLPsetinitialvalues(this.objPtr, varIndexes.Length, varIndexes, varValues);
            if (num != XNLPConst.XNLP_OK)
            {
                string name = MethodBase.GetCurrentMethod().Name;
                this.ThrowError(name);
            }
        }

        public string GetLastError()
        {
            int size = 100;
            string msg = new string(new char[size], 0, size);
            int source;
            int code;
            int num = DLLImports.XNLPgetlasterror(this.objPtr, msg, size, out source, out code);
            string srcMsg = "";
            switch (source)
            {
                case 0:
                    {
                        srcMsg = "No component signalled an error.";
                        break;
                    }
                case 1:
                    {
                        srcMsg = "Xpress-NLP signalled an error.";
                        break;
                    }
                case 2:
                    {
                        srcMsg = "The operating system signalled an error.";
                        break;
                    }
                default:
                    {
                        switch (source)
                        {
                            case 20:
                                {
                                    srcMsg = "Xpress Optimizer signalled an error.";
                                    break;
                                }
                            case 21:
                                {
                                    srcMsg = "Xpress-SLP signalled an error.";
                                    break;
                                }
                        }
                        break;
                    }
            }
            string codeMsg = "";
            switch (code)
            {
                case 1:
                    {
                        codeMsg = "An invalid parameter was passed to the previous Xpress-NLP call.";
                        break;
                    }
                case 2:
                    {
                        codeMsg = "Xpress-NLP failed to allocate working memory during the previous call.";
                        break;
                    }
                case 3:
                    {
                        codeMsg = "An invalid attempt was made to modify the Xpress-NLP problem, for example during a solve.";
                        break;
                    }
                case 4:
                    {
                        codeMsg = "An unexpected internal error occurred in Xpress-NLP. This may be caused by mismatched versions of Xpress-NLP, Xpress Optimizer and Xpress-SLP.";
                        break;
                    }
                case 6:
                    {
                        codeMsg = "An error occurred during prevalidation of a tokenized expression.";
                        break;
                    }
                case 7:
                    {
                        codeMsg = "An error occurred during prevalidation of a string expression.";
                        break;
                    }
                case 8:
                    {
                        codeMsg = "Bad probe error.";
                        break;
                    }
            }
            if (num != XNLPConst.XNLP_OK)
            {
                throw new XPRSException("XNLPgetlasterror failed, returned " + num);
            }
            return srcMsg + '\n' + codeMsg;
        }

        public Tuple<int, int[], double[]> AddExpression(string expression, int maxSize = 100)
        {
            int[] array = new int[maxSize];
            double[] array2 = new double[maxSize];
            int item;
            DLLImports.XNLPtokenizeexpression(this.objPtr, expression, maxSize, out item, array, array2);
            return new Tuple<int, int[], double[]>(item, array, array2);
        }

        public object[] ParseExpression(string expression, int maxSize = 100)
        {
            int[] array = new int[maxSize];
            double[] array2 = new double[maxSize];
            int num;
            
            DLLImports.XNLPtokenizeexpression(this.objPtr, expression, maxSize, out num, array, array2);
            return new object[]
			{
				num, 
				array, 
				array2
			};
        }

        public void AddVarIndicatorToCriteria(int[] var1Indexes, int[] var2Indexes, double[] coeffs)
        {
            int num = var1Indexes.Length;
            if (num != 0 && var2Indexes.Length == var1Indexes.Length)
            {
                int num2 = 8;
                int num3 = num2 * num;
                int[] array = new int[num3];
                double[] array2 = new double[num3];
                int[] row_index = new int[]
				{
					-1
				};
                int[] row_offset = new int[]
				{
					0, 
					num3
				};
                for (int i = 0; i < num; i++)
                {
                    int num4 = 0;
                    array[num2 * i + num4] = 1;
                    array2[num2 * i + num4++] = coeffs[i];
                    array[num2 * i + num4] = 31;
                    array2[num2 * i + num4++] = 3.0;
                    array[num2 * i + num4] = 21;
                    array2[num2 * i + num4++] = 0.0;
                    array[num2 * i + num4] = 10;
                    array2[num2 * i + num4++] = (double)var1Indexes[i];
                    array[num2 * i + num4] = 31;
                    array2[num2 * i + num4++] = 3.0;
                    array[num2 * i + num4] = 10;
                    array2[num2 * i + num4++] = (double)var2Indexes[i];
                    array[num2 * i + num4] = 22;
                    array2[num2 * i + num4++] = 0.0;
                    if (i < num - 1)
                    {
                        array[num2 * i + num4] = 31;
                        array2[num2 * i + num4++] = 5.0;
                    }
                }
                array[num3 - 1] = 0;
                array2[num3 - 1] = 0.0;
                int num5 = DLLImports.XNLPaddtokenizedterms(this.objPtr, 0, 1, row_index, row_offset, array, array2);
                if (num5 != XNLPConst.XNLP_OK)
                {
                    string name = MethodBase.GetCurrentMethod().Name;
                    this.ThrowError(name);
                }
            }
        }

        public void AddIsolatedPolynomialCriteria(int[] varIndexes, double[] coeffs, int[] degrees)
        {
            int varCount = coeffs.Length;

            if (varCount == 0)
                return;

            int exprLength = 8;
            int tokenCount = exprLength * varCount;
            int[] tokenType = new int[tokenCount];
            double[] tokenValue = new double[tokenCount];
            int[] rowInd = { XNLPConst.XNLP_OBJECTIVE_ROW };
            int[] offset = { 0, tokenCount };

            for (int i = 0; i < coeffs.Length; i++)
            {
                int j = 0;

                tokenType[exprLength * i + j] = XSLPconst.CON;
                tokenValue[exprLength * i + j++] = coeffs[i];

                tokenType[exprLength * i + j] = XSLPconst.OP;
                tokenValue[exprLength * i + j++] = XSLPconst.MULTIPLY;

                tokenType[exprLength * i + j] = XSLPconst.LB;
                tokenValue[exprLength * i + j++] = 0;

                tokenType[exprLength * i + j] = XSLPconst.COL;
                tokenValue[exprLength * i + j++] = varIndexes[i];

                tokenType[exprLength * i + j] = XSLPconst.OP;
                tokenValue[exprLength * i + j++] = XSLPconst.EXPONENT;

                tokenType[exprLength * i + j] = XSLPconst.CON;
                tokenValue[exprLength * i + j++] = degrees[i];

                tokenType[exprLength * i + j] = XSLPconst.RB;
                tokenValue[exprLength * i + j++] = 0;

                if (i < coeffs.Length - 1)
                {
                    tokenType[exprLength * i + j] = XSLPconst.OP;
                    tokenValue[exprLength * i + j++] = XSLPconst.PLUS;
                }
            }

            int num = DLLImports.XNLPaddtokenizedterms(this.objPtr, XNLPConst.XNLP_TOKEN_FORMAT_INFIX, 1, rowInd, offset, tokenType, tokenValue);
            if (num != XNLPConst.XNLP_OK)
                throw new XPRSException("XNLPaddtokenizedterms failed, returned " + num.ToString());
        }

        public void AddConstantTerm(double constant, int rowIndex, int constantOffset)
        {
            int num = DLLImports.XNLPaddtokenizedterms(this.objPtr, 0, 1, new int[]
			{
				rowIndex
			}, new int[]
			{
				constantOffset, 
				constantOffset + 1
			}, new int[]
			{
				1
			}, new double[]
			{
				constant
			});
            if (num != 0)
            {
                string name = MethodBase.GetCurrentMethod().Name;
                this.ThrowError(name);
            }
        }

        public void AddOperationTerm(int operation, int rowIndex, int operationOffset)
        {
            int num = DLLImports.XNLPaddtokenizedterms(this.objPtr, 1, 1, new int[]
			{
				rowIndex
			}, new int[]
			{
				operationOffset, 
				operationOffset + 1
			}, new int[]
			{
				31
			}, new double[]
			{
				(double)operation
			});
            if (num != XNLPConst.XNLP_OK)
            {
                string name = MethodBase.GetCurrentMethod().Name;
                this.ThrowError(name);
            }
        }

        public void AddTermEquationPostfix(int[] tokenTypes, double[] tokenValues, int rowIndex)
        {
            int num = DLLImports.XNLPaddtokenizedterms(this.objPtr, 1, 1, new int[]
			{
				rowIndex
			}, new int[]
			{
				0, 
				tokenTypes.Length
			}, tokenTypes, tokenValues);
            if (num != XNLPConst.XNLP_OK)
            {
                string name = MethodBase.GetCurrentMethod().Name;
                this.ThrowError(name);
            }
        }

        public void AddTermEquationInfix(int[] tokenTypes, double[] tokenValues, int rowIndex)
        {
            int num = DLLImports.XNLPaddtokenizedterms(this.objPtr, 0, 1, new int[]
			{
				rowIndex
			}, new int[]
			{
				0, 
				tokenTypes.Length
			}, tokenTypes, tokenValues);
            if (num != XNLPConst.XNLP_OK)
            {
                string name = MethodBase.GetCurrentMethod().Name;
                this.ThrowError(name);
            }
        }

        public XNLPstatus Optimize()
        {
            int status;
            int num = DLLImports.XNLPoptimize(this.objPtr, out status);
            if (num != XNLPConst.XNLP_OK)
                throw new XPRSException("XNLPoptimize failed, returned " + num.ToString());
            return (XNLPstatus)status;
        }

        public double[] GetSolution(int length)
        {
            double[] varValues = new double[length];
            int num = DLLImports.XNLPgetsolution(this.objPtr, length, null, varValues);
            if (num != XNLPConst.XNLP_OK)
                throw new XPRSException("XNLPgetsolution failed, returned " + num.ToString());
            return varValues;
        }

        public void Dispose()
        {
            this.objPtr = IntPtr.Zero;
        }

        public void SetSolver(int solver)
        {
            int num = DLLImports.XNLPsetintcontrol(this.objPtr, XNLPConst.XNLP_SOLVER, solver);
            if (num != XNLPConst.XNLP_OK)
                throw new XPRSException("XNLPsetintcontrol failed, returned " + num.ToString());
        }

        public int GetSolver()
        {
            int res;
            int num = DLLImports.XNLPgetintcontrol(this.objPtr, XNLPConst.XNLP_SOLVER, out res);
            if (num != XNLPConst.XNLP_OK)
                throw new XPRSException("XNLPgetintcontrol failed, returned " + num.ToString());
            return res;
        }

        public int GetSolverSelected()
        {
            int res;
            int num = DLLImports.XNLPgetintattribute(this.objPtr, XNLPConst.XNLP_SOLVER_SELECTED, out res);
            if (num != XNLPConst.XNLP_OK)
                throw new XPRSException("XNLPgetintattribute failed, returned " + num.ToString());
            return res;
        }

        public double GetCriteria()
        {
            double res;
            int num = DLLImports.XNLPgetdoubleattribute(this.objPtr, XNLPConst.XNLP_OBJECTIVE_VALUE, out res);
            if (num != XNLPConst.XNLP_OK)
                throw new XPRSException("XNLPgetdoubleattribute failed, returned " + num.ToString());
            return res;
        }

        public void SetTolerance(int param, double tol)
        {
            int num = DLLImports.XNLPsetdoublecontrol(this.objPtr, param, tol);
            if (num != XNLPConst.XNLP_OK)
                throw new XPRSException("XNLPsetdoublecontrol failed, returned " + num.ToString());
        }

        public int GetIntControl(int param)
        {
            int res;
            int num = DLLImports.XNLPgetintcontrol(this.objPtr, param, out res);
            if (num != XNLPConst.XNLP_OK)
                throw new XPRSException("XNLPgetintcontrol failed, returned " + num.ToString());
            return res;
        }

        public int GetIntSolverControl(int param)
        {
            int res;
            int num = DLLImports.XNLPgetsolverintcontrol(this.objPtr, param, out res);
            if (num != XNLPConst.XNLP_OK)
                throw new XPRSException("XNLPgetintcontrol failed, returned " + num.ToString());
            return res;
        }

        public int GetIntAttribute(int param)
        {
            int res;
            int num = DLLImports.XNLPgetintattribute(this.objPtr, param, out res);
            if (num != XNLPConst.XNLP_OK)
                throw new XPRSException("XNLPgetintcontrol failed, returned " + num.ToString());
            return res;
        }

        public void SetBounds(int[] columnIndexes, double[] lb, double[] ub)
        {
            int num = DLLImports.XNLPsetbounds(this.objPtr, columnIndexes.Length, columnIndexes, lb, ub);
            if (num != XNLPConst.XNLP_OK)
            {
                string name = MethodBase.GetCurrentMethod().Name;
                this.ThrowError(name);
            }
        }

        public void SetVarTypes(int[] columnIndexes, char[] varTypes)
        {
            int num = DLLImports.XNLPsetcolumntype(this.objPtr, columnIndexes.Length, columnIndexes, varTypes);
            if (num != XNLPConst.XNLP_OK)
            {
                string name = MethodBase.GetCurrentMethod().Name;
                this.ThrowError(name);
            }
        }

        public void SetEquationTypes(int[] columnIndexes, char[] eqTypes)
        {
            int num = DLLImports.XNLPsetrowtype(this.objPtr, columnIndexes.Length, columnIndexes, eqTypes);
            if (num != XNLPConst.XNLP_OK)
            {
                string name = MethodBase.GetCurrentMethod().Name;
                this.ThrowError(name);
            }
        }

        public void SetCriteriumType(bool minimum)
        {
            int num = DLLImports.XNLPsetoptimizationsense(this.objPtr, minimum ? XNLPConst.XNLP_MINIMIZE : XNLPConst.XNLP_MAXIMIZE);
            if (num != XNLPConst.XNLP_OK)
            {
                string name = MethodBase.GetCurrentMethod().Name;
                this.ThrowError(name);
            }
        }

        public bool IsMinimum()
        {
            int type;
            int num = DLLImports.XNLPgetoptimizationsense(this.objPtr, out type);
            if (num != XNLPConst.XNLP_OK)
            {
                string name = MethodBase.GetCurrentMethod().Name;
                this.ThrowError(name);
            }
            return type == XNLPConst.XNLP_MINIMIZE;
        }

        public void WriteProbe(string fileName)
        {
            int num = DLLImports.XNLPwriteprob(this.objPtr, fileName, 1, 0);
            if (num != XNLPConst.XNLP_OK)
            {
                string name = MethodBase.GetCurrentMethod().Name;
                this.ThrowError(name);
            }
        }

        private void ThrowError(string methodName)
        {
            string s = methodName + " failed\n" + this.GetLastError();
            throw new XPRSException(s);
        }

        public void SetLog(bool output, string filename = null)
        {
            if (filename != null && File.Exists(filename))
            {
                int num = DLLImports.XNLPopenlogfile(this.objPtr, filename);
                if (num != XNLPConst.XNLP_OK)
                {
                    string name = MethodBase.GetCurrentMethod().Name;
                    this.ThrowError(name);
                }
            }
            if (output)
            {
                int num = DLLImports.XNLPenableconsoleoutput(this.objPtr);
                if (num != XNLPConst.XNLP_OK)
                {
                    string name = MethodBase.GetCurrentMethod().Name;
                    this.ThrowError(name);
                }
            }
        }

        public void CloseLogs()
        {
            int num = DLLImports.XNLPcloselogfiles(this.objPtr);
            if (num != XNLPConst.XNLP_OK)
            {
                string name = MethodBase.GetCurrentMethod().Name;
                this.ThrowError(name);
            }
            num = DLLImports.XNLPdisableconsoleoutput(this.objPtr);
            if (num != XNLPConst.XNLP_OK)
            {
                string name = MethodBase.GetCurrentMethod().Name;
                this.ThrowError(name);
            }
            
        }
    }
}
