﻿using System;
using System.Linq;
using GasExportApp;
using GEx.ErrorHandler;
using GEx.Iface;
using GExGoDiagram;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GExUnitTests
{
    [TestClass]
    public class GoDiagramTests : AbstractDiagramTests
    {
        public GoDiagramTests()
        {
            Init();
        }

        void Init()
        {
            _eh = new ErrorOutputMock();
            _diagramFactory = new GoDiagramFactory();
        }

    }
}
