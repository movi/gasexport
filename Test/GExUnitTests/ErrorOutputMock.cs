﻿using GEx.ErrorHandler;

namespace GExUnitTests
{
    internal class ErrorOutputMock : IErrorOutput

    {
        public bool ReportError(eErrClass cls, string sMsg, string sSource, int nCode)
        {
            return true;
        }

        public bool ReportError(eErrClass cls, string sMsg, string sSource)
        {
            return true;
        }

        public bool ReportError(eErrClass cls, int userId, string sMsg, string sSource)
        {
            return true;
        }
    }
}