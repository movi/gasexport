﻿using System.Linq;
using GasExportApp;
using GEx.ErrorHandler;
using GEx.Iface;
using GExGoDiagram;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GExUnitTests
{
    public class AbstractDiagramTests
    {
        protected IErrorOutput _eh;
        protected INetworkView _goView;
        protected IDiagramFactory _diagramFactory;

        [TestMethod]
        public void TestLinksCreation()
        {
            var p = new MainFormPresenter(_eh, _goView, _diagramFactory);
            INetworkView nw = _diagramFactory.CreateCanvas();
            int count = 100;
            for (int i = 0; i < count; i++)
            {
                var link = _diagramFactory.CreateConnection();
                nw.AddToNetwork(link);
            }

            var links = nw.EnumerateLinks();
            var linksCount = links.Count();

            Assert.AreEqual(linksCount, count);
        }

        [TestMethod]
        public void TestClearing()
        {
            var p = new MainFormPresenter(_eh, _goView, _diagramFactory);

            //IDiagramFactory factory = new GoDiagramFactory();

            INetworkView nw = _diagramFactory.CreateCanvas();
            int count = 100;
            for (int i = 0; i < count; i++)
            {
                var link = _diagramFactory.CreateConnection();
                nw.AddToNetwork(link);
            }
            //суть удаления - не должно остаться ни одного линка
            nw.ClearLinks();

            var linksCount = nw.EnumerateLinks().Count();

            Assert.AreEqual(linksCount, 0);
        }

        [TestMethod]
        public void TestClearing1()
        {
            var p = new MainFormPresenter(_eh, _goView, _diagramFactory);

            //IDiagramFactory factory = new GoDiagramFactory();

            INetworkView nw = _diagramFactory.CreateCanvas();
            int count = 100;
            for (int i = 0; i < count; i++)
            {
                var link = _diagramFactory.CreateConnection();
                nw.AddToNetwork(link);
            }
            //суть удаления - не должно остаться ни одного линка
            nw.ClearLinks();

            var linksCount = nw.EnumerateLinks().Count();

            Assert.AreEqual(linksCount, 0);
        }         
    }
}