﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using CommonUIControls;
using GEx.Iface;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GExUnitTests
{
    [TestClass]
    public class BalanceZonesPresenterTests
    {
        private List<DTO_Node> _nodes;
        private List<DTO_BalanceZone> _zones;

        public BalanceZonesPresenterTests()
        {
            GenerateTestData();
        }
        
        /// <summary>
        /// Подготовка данных для тестов
        /// </summary>
        private void GenerateTestData()
        {
            _zones = new List<DTO_BalanceZone>()
            {
                new DTO_BalanceZone()
                {
                    ID = 1,
                    Name = "first"
                },
                new DTO_BalanceZone()
                {
                    ID = 2,
                    Name = "Second"
                },
            };
            var zone = new DTO_BalanceZone()
            {
                ID = 3,
                Name = "3rd"
            };

            _zones.Add(zone);
            _nodes = new List<DTO_Node>()
            {
                new DTO_Node()
                {
                    Name = "Aude"
                },
                new DTO_Node()
                {
                    Name = "Steinitz"
                },
                new DTO_Node()
                {
                    Name = "f",
                    Type = DTO_Node_type.Border,
                    InterConnections = new List<DTO_BorderInterconnection>()
                    {
                        new DTO_BorderInterconnection()
                        {
                            Id = 1,
                            ZoneFrom = zone,
                        }
                    }
                }
            };
        }

        [TestMethod]
        public void IsPresenterStable()
        {
            var presenter = new BalanceZonesPresenter(null, null);
        }
        
        [TestMethod]
        public void IsGettingDatasourceStable()
        {
            var presenter = new BalanceZonesPresenter(new List<DTO_Node>(), new List<DTO_BalanceZone>());
            var ds = presenter.PrepareMasterSource();
        }
        
        [TestMethod]
        public void IsGettingDatasourceWithNullsStable()
        {
            var source = "IsGettingDatasourceWithNullsStable()";
            var presenter = new BalanceZonesPresenter(null, null);
            try
            {
                var ds = presenter.PrepareMasterSource();
            }
            //catch (AssertFailedException ex)
            //{
            //    System.Diagnostics.Trace.WriteLine(source + " is getting an error. However we continue\n" + ex.Message);
            //}
            catch (ArgumentNullException ex)
            {
                System.Diagnostics.Trace.WriteLine(source + " is getting an error. However we continue\n" + ex.Message);
            }
            finally
            {
                //Assert.IsTrue(true);
            }
        }

        bool ViewIsPrepared(BalanceZonesPresenter presenter, List<DTO_Node> nodes)
        {
            var bw = new BalanceZonesPresenter.BorderView(1, "2");
            BalanceZonesPresenter.BalanceZonesMasterView[] source = presenter.PrepareMasterSource();

            Assert.IsTrue(source != null);
            //презентер должен отдать ТОЛЬКО два элемента верхнего уровня
            Assert.IsTrue(source.Length == 2);

            var borders = nodes.Where(n => n.Type == DTO_Node_type.Border).ToList();

            //презентер во втором уровне должен отдать список В ЛЮБОМ случае непустой - такова логика (даже если интерконнекторов не было в помине)
            Assert.IsTrue(source[1].Views.Count >= borders.Count());
            if (borders.Any())
            {
                return (source[1].Views.Count > 0);
            }
            return true;
            //presenter.AddElementToBorderViews(bw);
        }


        [TestMethod]
        public void ViewIsPreparedEmptyData()
        {
            var nodes = new List<DTO_Node>();
            var presenter = new BalanceZonesPresenter(nodes, new List<DTO_BalanceZone>());
            bool viewIsPrepared = ViewIsPrepared(presenter, nodes);
            Assert.IsTrue(viewIsPrepared);
        }
        
        [TestMethod]
        public void ViewIsPrepared()
        {
            var presenter = new BalanceZonesPresenter(_nodes, _zones);
            bool viewIsPreapred = ViewIsPrepared(presenter, _nodes);
            Assert.IsTrue(viewIsPreapred);
        }
        
        /// <summary>
        /// Если тест валится, то мы смогли
        /// удалить все строки в гриде, что неправильно
        /// </summary>
        [TestMethod]
        public void WeCannotRemoveAll()
        {
            var presenter = new BalanceZonesPresenter(_nodes, _zones);
            var bw = new BalanceZonesPresenter.BorderView(1, "2");
            var source = presenter.PrepareMasterSource();
            var secondLevel = source[1].Views;
            var borders = _nodes.Where(n => n.Type == DTO_Node_type.Border).ToList();

            var sl = ((BindingList<BalanceZonesPresenter.BorderView>) secondLevel).ToList();

            if (borders.Any())
            {
                sl.ForEach(
                    viewElem => presenter.RemoveElementFromBorderViews(viewElem));
                Assert.IsTrue(source[1].Views.Count == borders.Count);
            }
            else
            {
                //кстати, это фигня какая-то. нет границ, а интерконнекторы есть
                Assert.Fail("ни одной границы. Сделайте что-то с массивом данных для теста");
            }
        }

        /// <summary>
        /// Никакой логики не тестируем,
        /// тестируем входной набор данных
        /// </summary>
        [TestMethod]
        public void TestData()
        {
            var test = _nodes.Any(n => n.InterConnections.Count > 0 && n.Type != DTO_Node_type.Border);
            if (test)
                Assert.Fail("Ошибка входных данных - не должно быть интерконнекторов у узлов, не являющимися границей");
        }
    }
}
