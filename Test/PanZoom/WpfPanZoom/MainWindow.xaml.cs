﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Windows;

namespace PanAndZoom
{
    public partial class MainWindow : Window
    {
            public BitmapImage Convert(Bitmap src)
            {
                MemoryStream ms = new MemoryStream();
                src.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                BitmapImage image = new BitmapImage();
                image.BeginInit();
                ms.Seek(0, SeekOrigin.Begin);
                image.StreamSource = ms;
                image.EndInit();
                return image;
            }

        public MainWindow()
        {
            InitializeComponent();
            Bitmap bmp = PanZoomResources.Resources.europe_map2;

            //чтоб поюзать единый файл из ресурсов
            Img.Source = Convert(bmp);
        }
    }
}