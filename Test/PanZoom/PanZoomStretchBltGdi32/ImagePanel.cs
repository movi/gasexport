using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace PanZoomStretchBltgdi32
{
    [StructLayout(LayoutKind.Sequential)]
    public struct RECT
    {
        public int Left, Top, Right, Bottom;

        public RECT(int left, int top, int right, int bottom)
        {
            Left = left;
            Top = top;
            Right = right;
            Bottom = bottom;
        }

        public RECT(System.Drawing.Rectangle r) : this(r.Left, r.Top, r.Right, r.Bottom) { }

        public int X
        {
            get { return Left; }
            set { Right -= (Left - value); Left = value; }
        }

        public int Y
        {
            get { return Top; }
            set { Bottom -= (Top - value); Top = value; }
        }

        public int Height
        {
            get { return Bottom - Top; }
            set { Bottom = value + Top; }
        }

        public int Width
        {
            get { return Right - Left; }
            set { Right = value + Left; }
        }

        public System.Drawing.Point Location
        {
            get { return new System.Drawing.Point(Left, Top); }
            set { X = value.X; Y = value.Y; }
        }

        public System.Drawing.Size Size
        {
            get { return new System.Drawing.Size(Width, Height); }
            set { Width = value.Width; Height = value.Height; }
        }

        public static implicit operator System.Drawing.Rectangle(RECT r)
        {
            return new System.Drawing.Rectangle(r.Left, r.Top, r.Width, r.Height);
        }

        public static implicit operator RECT(System.Drawing.Rectangle r)
        {
            return new RECT(r);
        }

        public static bool operator ==(RECT r1, RECT r2)
        {
            return r1.Equals(r2);
        }

        public static bool operator !=(RECT r1, RECT r2)
        {
            return !r1.Equals(r2);
        }

        public bool Equals(RECT r)
        {
            return r.Left == Left && r.Top == Top && r.Right == Right && r.Bottom == Bottom;
        }

        public override bool Equals(object obj)
        {
            if (obj is RECT)
                return Equals((RECT)obj);
            else if (obj is System.Drawing.Rectangle)
                return Equals(new RECT((System.Drawing.Rectangle)obj));
            return false;
        }

        public override int GetHashCode()
        {
            return ((System.Drawing.Rectangle)this).GetHashCode();
        }

        public override string ToString()
        {
            return string.Format(System.Globalization.CultureInfo.CurrentCulture, "{{Left={0},Top={1},Right={2},Bottom={3}}}", Left, Top, Right, Bottom);
        }
    }

    public partial class ImagePanel : Control
    {
        private Bitmap map;
        private Graphics bmpGfx;
        private IntPtr hBitmap;

        public void DrawStretch(IntPtr hBitmap, Graphics srcGfx, Graphics destGfx,
            Rectangle srcRect, Rectangle destRect)
        {
            IntPtr pTarget = destGfx.GetHdc();
            IntPtr pSource = CreateCompatibleDC(pTarget);
            IntPtr pOrig = SelectObject(pSource, hBitmap);
            SetStretchBltMode(pTarget, 0x04);
            if (!StretchBlt(pTarget, destRect.X, destRect.Y, destRect.Width, destRect.Height,
                pSource, srcRect.X, srcRect.Y, srcRect.Width, srcRect.Height,
                TernaryRasterOperations.SRCCOPY))
                throw new Win32Exception(Marshal.GetLastWin32Error());

            IntPtr pNew = SelectObject(pSource, pOrig);
            DeleteDC(pSource);
            destGfx.ReleaseHdc(pTarget);
        }

        public void DrawStretch(IntPtr hBitmap, Graphics srcGfx, Graphics destGfx,
            RectangleF srcRect, RectangleF destRect)
        {
            IntPtr pTarget = destGfx.GetHdc();
            IntPtr pSource = CreateCompatibleDC(pTarget);
            IntPtr pOrig = SelectObject(pSource, hBitmap);
            if (!StretchBlt(pTarget, (int)destRect.X, (int)destRect.Y, (int)destRect.Width, (int)
                destRect.Height,
                pSource, (int)srcRect.X, (int)srcRect.Y, (int)srcRect.Width, (int)srcRect.Height,
                TernaryRasterOperations.SRCCOPY))
                throw new Win32Exception(Marshal.GetLastWin32Error());
            IntPtr pNew = SelectObject(pSource, pOrig);
            DeleteDC(pSource);
            destGfx.ReleaseHdc(pTarget);
        }

        [DllImport("gdi32.dll", EntryPoint = "SelectObject")]
        public static extern System.IntPtr SelectObject(
            [In()] System.IntPtr hdc,
            [In()] System.IntPtr h);

        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        private static extern bool DeleteDC(IntPtr hdc);

        [DllImport("gdi32.dll", EntryPoint = "DeleteObject")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DeleteObject(
            [In()] System.IntPtr ho);

        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        private static extern IntPtr CreateCompatibleDC(IntPtr hdc);

        [DllImport("gdi32.dll")]
        private static extern bool StretchBlt(IntPtr hdcDest, int nXOriginDest, int nYOriginDest,
            int nWidthDest, int nHeightDest,
            IntPtr hdcSrc, int nXOriginSrc, int nYOriginSrc, int nWidthSrc, int nHeightSrc,
            TernaryRasterOperations dwRop);

        [DllImport("gdi32.dll")]
        private static extern int ExcludeClipRect(
            IntPtr hdc, int nLeftRect, int nTopRect, int nRightRect, int nBottomRect);

        [DllImport("user32.dll")]
        private static extern int FillRect(IntPtr hDC, [In] ref RECT lprc, IntPtr hbr);

        [DllImport("gdi32.dll")]
        public static extern IntPtr CreatePen(int fnPenStyle, int nWidth, int crColor);


          /// <summary>
    /// SetStretchBltMode
    /// </summary>
    [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
    public static extern bool SetStretchBltMode(IntPtr hObject, int nStretchMode);


        public enum TernaryRasterOperations : uint
        {
            SRCCOPY = 0x00CC0020
            //there are many others but we don't need them for this purpose, omitted for brevity
        }
        public ImagePanel()
        {
            InitializeComponent();

            // Set the value of the double-buffering style bits to true.
            this.SetStyle(ControlStyles.AllPaintingInWmPaint |
              ControlStyles.UserPaint | ControlStyles.ResizeRedraw |
              ControlStyles.UserPaint | ControlStyles.DoubleBuffer, true);

            Image = PanZoomResources.Resources.europe_map2;
            displayScrollbar();
            setScrollbarValues();
        }

        int viewRectWidth, viewRectHeight; // view window width and height

        float zoom = 1.0f;
        public float Zoom
        {
            get { return zoom; }
            set
            {
                if (value < 0.001f) value = 0.001f;
                zoom = value;

                displayScrollbar();
                setScrollbarValues();
                Invalidate();
            }
        }

        Size canvasSize = new Size(60, 40);
        public Size CanvasSize
        {
            get { return canvasSize; }
            set
            {
                canvasSize = value;
                displayScrollbar();
                setScrollbarValues();
                Invalidate();
            }
        }

        public Bitmap Image
        {
            get { return map; }
            set
            {
                if (value != null)
                {
                    var img = value;
                    var size = img.Size;
                    var bmp = new Bitmap(size.Width, size.Height, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
                    using (var gr = Graphics.FromImage(bmp))
                    {
                        gr.DrawImage(img, new Rectangle(Point.Empty, size));
                    }
                    map = bmp;

                    map = value;

                    //dispose/delete any previous caches
                    if (bmpGfx != null) bmpGfx.Dispose();
                    if (hBitmap != null) DeleteObject(hBitmap);

                    //cache the new HBitmap and Graphics.
                    bmpGfx = Graphics.FromImage(map);
                    hBitmap = map.GetHbitmap();
                }

                displayScrollbar();
                setScrollbarValues();
                Invalidate();
            }
        }

        InterpolationMode interMode = InterpolationMode.HighQualityBilinear;
        private Point start;
        private bool drag;

        public InterpolationMode InterpolationMode
        {
            get{return interMode;}
            set{interMode=value;}
        }

        protected override void OnResize(EventArgs e)
        {
            displayScrollbar();
            setScrollbarValues();
            base.OnResize(e);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                start.X = start.X + e.X;
                start.Y = start.Y + e.Y;
                
                this.Cursor = Cursors.Hand;
                drag = true;
            }
            base.OnMouseDown(e);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            drag = false;
            base.OnMouseUp(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (drag)
            {  
                //new Point((int) (hScrollBar1.Value/zoom), (int) (vScrollBar1.Value/zoom));
                pt = new Point(start.X - e.X, start.Y - e.Y);
                Invalidate();
              
                Debug.WriteLine(pt);
            }
            base.OnMouseMove(e);
        }

        Point pt;
        
        [DllImport("gdi32.dll")]
        public static extern IntPtr CreateSolidBrush(int crColor);

        protected override void OnPaint(PaintEventArgs e)
        {
            if(map==null) return;

            Rectangle srcRect, distRect;
            if (canvasSize.Width * zoom < viewRectWidth && canvasSize.Height * zoom < viewRectHeight)
                srcRect = new Rectangle(0, 0, canvasSize.Width, canvasSize.Height);  // view all image
            else srcRect = new Rectangle(pt, new Size((int)(viewRectWidth / zoom), (int)(viewRectHeight / zoom))); // view a portion of image

            Graphics g = e.Graphics;

            Rectangle mapRect = srcRect;//whatever zoom/pan logic you implemented.

            Rectangle windowRect = new Rectangle(0, 0, viewRectWidth, viewRectHeight); // view window width and height


            int left = -mapRect.Left;
            int right = -mapRect.Right;
            int top = mapRect.Top + mapRect.Bottom;
            int bottom = mapRect.Bottom;

            ;// size.Width

            DrawStretch(
                hBitmap,
                bmpGfx,
                g,
                mapRect,
                windowRect);

            var drawnRect = new RECT();
            var winRect = new RECT(windowRect);
            
            bool needToBleach = false;
            if (pt.X < 0 || pt.Y < 0)
            {
                drawnRect.Left = -pt.X;
                drawnRect.Top = -pt.Y;
                drawnRect.Right = windowRect.Right;
                drawnRect.Bottom = windowRect.Bottom;

                needToBleach = true;
            }

            if (pt.X > (CanvasSize.Width - windowRect.Width) || pt.Y > (CanvasSize.Height - windowRect.Height))
            {
                drawnRect.Left = 0;
                drawnRect.Top = 0;
                drawnRect.Right = CanvasSize.Width - pt.X;
                drawnRect.Bottom = CanvasSize.Height - pt.Y;
                needToBleach = true;
            }

            if (needToBleach)
            {
                IntPtr ptrBrush = CreateSolidBrush(ColorTranslator.ToWin32(Color.White));
                IntPtr pTarget = g.GetHdc();
                ExcludeClipRect(pTarget, drawnRect.Left, drawnRect.Top, drawnRect.Right, drawnRect.Bottom);
                FillRect(pTarget, ref winRect, ptrBrush);
                g.ReleaseHdc(pTarget);
            }
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            float c = 100;
            float coeff = e.Delta > 0 ? e.Delta / c : (1f / (-e.Delta / c));

            Zoom *= coeff;
        }

        private void displayScrollbar()
        {
            viewRectWidth = this.Width;
            viewRectHeight = this.Height;

            if (map != null) canvasSize = map.Size;

            // If the zoomed image is wider than view window, show the HScrollBar and adjust the view window
            if (viewRectWidth > canvasSize.Width*zoom)
            {
                hScrollBar1.Visible = false;
                viewRectHeight = Height;
            }
            else
            {
                hScrollBar1.Visible = true;
                viewRectHeight = Height - hScrollBar1.Height;
            }

            // If the zoomed image is taller than view window, show the VScrollBar and adjust the view window
            if (viewRectHeight > canvasSize.Height*zoom)
            {
                vScrollBar1.Visible = false;
                viewRectWidth = Width;
            }
            else
            {
                vScrollBar1.Visible = true;
                viewRectWidth = Width - vScrollBar1.Width;
            }

            // Set up scrollbars
            hScrollBar1.Location = new Point(0, Height - hScrollBar1.Height);
            hScrollBar1.Width = viewRectWidth;
            vScrollBar1.Location = new Point(Width - vScrollBar1.Width, 0);
            vScrollBar1.Height = viewRectHeight;
        }

        private void setScrollbarValues()
        {
            // Set the Maximum, Minimum, LargeChange and SmallChange properties.
            this.vScrollBar1.Minimum = 0;
            this.hScrollBar1.Minimum = 0;

            // If the offset does not make the Maximum less than zero, set its value. 
            if ((canvasSize.Width * zoom - viewRectWidth) > 0)
            {
                this.hScrollBar1.Maximum =(int)( canvasSize.Width * zoom) - viewRectWidth;
            }
            // If the VScrollBar is visible, adjust the Maximum of the 
            // HSCrollBar to account for the width of the VScrollBar.  
            if (this.vScrollBar1.Visible)
            {
                this.hScrollBar1.Maximum += this.vScrollBar1.Width;
            }
            this.hScrollBar1.LargeChange = this.hScrollBar1.Maximum / 10;
            this.hScrollBar1.SmallChange = this.hScrollBar1.Maximum / 20;

            // Adjust the Maximum value to make the raw Maximum value 
            // attainable by user interaction.
            this.hScrollBar1.Maximum += this.hScrollBar1.LargeChange;

            // If the offset does not make the Maximum less than zero, set its value.    
            if ((canvasSize.Height * zoom - viewRectHeight) > 0)
            {
                this.vScrollBar1.Maximum = (int)(canvasSize.Height * zoom) - viewRectHeight;
            }

            // If the HScrollBar is visible, adjust the Maximum of the 
            // VSCrollBar to account for the width of the HScrollBar.
            if (this.hScrollBar1.Visible)
            {
                this.vScrollBar1.Maximum += this.hScrollBar1.Height;
            }
            this.vScrollBar1.LargeChange = this.vScrollBar1.Maximum / 10;
            this.vScrollBar1.SmallChange = this.vScrollBar1.Maximum / 20;

            // Adjust the Maximum value to make the raw Maximum value 
            // attainable by user interaction.
            this.vScrollBar1.Maximum += this.vScrollBar1.LargeChange;
        }

        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            this.Invalidate();
        }
    }
}
